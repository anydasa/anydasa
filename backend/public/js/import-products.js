$(function () {
    $(document).on('click', '#submit-upload-products-form', function (e) {
        e.preventDefault();
        var formData = new FormData($('#upload-products-form')[0]);
        $.ajax({
            url: '/products/product/ajax-action/import-upload-images-excel/upload',
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function (response) {
                var selects = $('.connect-select');
                $('#select-filename').val(response.filename);
                selects.find('option')
                    .remove();
                selects.each(function () {
                    $(this).append('<option value="0">Не соотносить</option>');
                });
                response.columns.forEach(function (column) {
                    selects.each(function () {
                        $(this).append('<option value="' + column['code'] + '">' + column['value'] + '</option>');
                    });
                });
                $('.match-fields-wrap').show();
            }
        });
        return false;
    });

    $(document).on('click', '#submit-products-map-fields', function (e) {
        e.preventDefault();
        var formDataToSend = $('#match-fields-form').serialize();
        $.ajax({
            url: '/products/product/ajax-action/import-upload-submit-images-excel/upload',
            type: 'POST',
            data: formDataToSend,
            success: function (response) {
                promtResult(response);
                $('.match-fields-wrap').hide();
            }
        });
    });

    $(document).on('click', '.addfile', function (e) {
        e.preventDefault();
        $('#xls').click();
    });

    function promtResult(responce)
    {
        var str = 'Найдено товаров: ' + responce.data['countFoundCodes'] + '</br>';
        str+= 'Не найдено товаров: ' + responce.data['countNotFoundCodes'] + '</br><ul>';
        for (var code in responce.data['notFoundCodes']) {
            str+= '<li>"' + responce.data['notFoundCodes'][code] + '"</li>';
        }
        str+= '</ul>';
        str+= 'Загружено данных: <ul>';
        str+= '<li>Фотографий загружено: ' + responce.data['imagesUploaded'] + '</li>';
        str+= 'Ошибки: <ul>';
        for (var error in responce.data['errors']) {
            str+= '<li>"' + responce.data['errors'][error] + '"</li>';
        }
        str+= '</ul>';

        $.prompt(str);
    }
})();

function getFilename(elem) {
    var fn = $(elem).val();
    var filename = fn.match(/[^\\/]*$/)[0];
    $('.filename').text(filename);
}
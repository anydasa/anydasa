$("form#ProductsAccessories").on('click', '.removeRelatedProduct', function () {
    $(this).parents('tr:first').remove();
});

$("form#ProductsAccessories").on('click', "#addRelatedProductButton", function () {
    window.chosen = {};
    window.chosenIsArray = true;
    window.saveChosenProducts = function (products) {
        populateRelatedProducts(products);
        closeModalWindow();
    };
    go2Choice();
});

$("form#ProductsAccessories").on('click', "#addOwnerProductButton", function () {
    window.chosen = {};
    window.chosenIsArray = true;
    window.saveChosenProducts = function (products) {
        populateOwnerProducts(products);
        closeModalWindow();
    };
    go2Choice('id_set=null');
});

$("form#ProductsAccessories").on('click', '.removeOwnerProduct', function () {
    $(this).parents('tr:first').remove();
});

var go2Choice = function (query) {
    if ('string' !== typeof query) {
        query = '';
    }
    $('form#ProductsAccessories input[type="hidden"].id_product').each(function () {
        query += '&excludeid[]=' + $(this).val();
    });
    loadModalWindow('/products/choice/list', getTargetIndex('blank'), query);
};

var populateOwnerProducts = function (products) {
    $.each(products, function (key, product) {
        $("form#ProductsAccessories .ownerProductsContainer").append(
            '<tr>' +
            '<td><input type="hidden" class="id_product" name="ownerProducts[]" value="' + product.id + '">' + product.id + '</td>' +
            '<td style="width: 300px;">' + product.title + '</td>' +
            '<td><span class="btn btn-xs fa fa-times removeOwnerProduct"></span></td>' +
            '</tr>'
        );
    });
};

var populateRelatedProducts = function (products) {
    $.each(products, function (key, product) {
        $("form#ProductsAccessories .relatedProductsContainer").append(
            '<tr data-id_product="'+product.id+'">' +
                '<td>' +
                '<input type="hidden" class="id_product" name="relatedProducts[]" value="' + product.id + '">' +
                product.id +
                '</td>' +
                '<td style="width: 300px;">' + product.title + '</td>' +
                '<td><span class="btn btn-xs fa fa-times removeRelatedProduct"></span></td>' +
            '</tr>'
        );
    });
};

$(function () {
    $("form#ProductsAccessories .title").parents('tr:first').after(
        '<tr>' +
            '<th>Товары</th>' +
            '<td>' +
                '<table class="ownerProductsContainer table-striped table-hover table-condensed" style="border: 0;"></table>' +
                '<i id="addOwnerProductButton" class="fa fa-lg fa-plus btn"></i>' +
            '</td>' +
        '</tr>' +
        '<tr>' +
            '<th>Аксессуары</th>' +
            '<td>' +
                '<table class="relatedProductsContainer table-striped table-hover table-condensed" style="border: 0;"></table>' +
                '<i id="addRelatedProductButton" class="fa fa-lg fa-plus btn"></i>' +
            '</td>' +
        '</tr>'
    );
});
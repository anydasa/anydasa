var go2Choice = function (key) {
    var exclude = [];

    $('form#SeriesProductsForm tr[data-product-id]').each(function () {
        exclude.push($(this).attr('data-product-id'));
    });

    var params = {
        'excludeid[]' : exclude,
        'k' : key,
        'has_series' : 0
    };

    $('body').addClass('wait');
    $.get('/products/choice/list', params).success(
        function (data) {
            openModalWindow(data, this.url, getTargetIndex('blank'))
        }
    )
};

var populateProducts = function (products) {
    $.each(products, function (key, product) {
        populateOneProduct(product, []);
    });
};

var populateOneProduct = function (product, values) {
    var tpl = Handlebars.compile( $('script#SeriesProductsRowTpl').html() );
    $("#SeriesProductsForm table tbody").append( tpl(product) );

    $.each(values, function (key, val) {
        var selector = '#param-'+product.id+'-'+key;
        $(selector).val(val);
    });
};

$(document).on('click', ".delete-series-product", function () {
    $(this).closest('tr').remove();
});

$(document).on('click', ".add-series-product", function () {
    window.chosen = {};
    window.chosenIsArray = true;
    window.saveChosenProducts = function (products) {
        populateProducts(products);
        closeModalWindow();
    };

    go2Choice($(this).attr('data-key'));
});
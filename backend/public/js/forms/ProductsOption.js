$("form#ProductsOption [data-presets]").each(function () {
    var $element = $(this);
    $element.autocomplete({
        source: $.parseJSON( $element.attr("data-presets") ),
        appendTo: $element.parent(),
        minLength: 0
    }).focusin(function() {
        $(this).autocomplete("search");
    });
});

$("#ProductsOption h1").before(
    $("<span>")
        .attr('title', 'Скопировать')
        .attr('data-toggle', 'tooltip')
        .css({cursor: 'pointer', marginRight: '10px'})
        .html('<i class="fa fa-2x fa-copy">')
        .on('click', function () {
            var params = {};
            $("form#ProductsOption input[data-param-id]").each(function () {
                params[$(this).attr('data-param-id')] = $(this).val();
            });
            $.cookie('ProductsOption', JSON.stringify(params));
        })
);


if ( undefined !== $.cookie('ProductsOption') ) {
    $("#ProductsOption h1").after(
        $("<span>")
            .attr('title', 'Вставить')
            .attr('data-toggle', 'tooltip')
            .css({cursor: 'pointer', marginRight: '10px'})
            .html('<i class="fa fa-2x fa-paste">')
            .on('click', function () {
                $.each(JSON.parse($.cookie('ProductsOption')), function (k,v) {
                    $("form#ProductsOption input[data-param-id="+k+"]").val(v);
                });
            })
    );
}


$("form#ProductsSets").on('click', '#save', function () {
    $('form#ProductsSets tr.group').each(function (index) {
        var group_num = index + 1;
        $(this).find('.productsContainer tr').each(function () {
            $("form#ProductsSets").append(
                $('<input type="hidden">')
                    .attr('name', 'products[' + $(this).attr('data-id_product') + '][group_num]')
                    .val(group_num)
            )
        });
    });
});

$("form#ProductsSets").on('update', function () {
    var group_count = $('form#ProductsSets tr.group').length;

    $(".productsContainer").each(function () {
        $(this).find(".discounts").each(function () {
            var discounts_count = $(this).find("input").length;
            var id_product  = $(this).parents('tr:first').find("input.id_product").val();

            if ( discounts_count < group_count ) {
                for (var i = 0; i < (group_count - discounts_count); i++) {
                    var $input = $('<input type="text">')
                        .attr('name', 'products[' + id_product + '][discount]['+(discounts_count+i)+']')
                        .css({width: '25px', marginRight: '2px'})
                        .val('');

                    $(this).append($input);

                    $input.rules("add", {number: true, required: true, max: 100, min: 0});
                }
            } else if ( discounts_count > group_count ) {
                for (var i = 0; i < (discounts_count - group_count); i++) {
                    $(this).find('input:last').remove();
                }
            }

        });
    });
});

$("form#ProductsSets").on('click', "#addGroupButton", function () {
    $("#save").parents('tr').before(
        '<tr class="group">' +
            '<th class="sort-handle">Группа<span class="btn fa fa-times removeGroup"></span></th>' +
            '<td>' +
                '<table class="productsContainer table-striped table-hover table-condensed" style="border: 0;"></table>' +
                '<i id="addProduct2GroupButton" class="fa fa-lg fa-plus btn"></i>' +
            '</td>' +
        '</tr>'
    );
});

$("form#ProductsSets").on('click', '.removeGroup', function () {
    var $parent = $(this).parents('tr:first');
    $parent.find('.removeProduct').click();
    $parent.remove();
});

$("form#ProductsSets").on('click', '.productsContainer .removeProduct', function () {
    var $productRow = $(this).parents('tr:first');
    var $groupRow   = $productRow.parents('tr:first');
    $productRow.remove();

    if ( !$groupRow.find('.productsContainer tr').length ) {
        $groupRow.find(".removeGroup").click();
    }

    $("form#ProductsSets").trigger('update');
});

$("form#ProductsSets").on('click', "#addProduct2GroupButton", function () {
    window.chosen = {};
    window.chosenIsArray = true;
    window.saveChosenProducts = function (products) {
        populateProductsGroup(products, $chosenProductsContainerGroup);
        closeModalWindow();
    };
    window.$chosenProductsContainerGroup = $(this).prev();
    go2Choice();
});

$("form#ProductsSets").on('click', "#addOwnerProductButton", function () {
    window.chosen = {};
    window.chosenIsArray = true;
    window.saveChosenProducts = function (products) {
        populateOwners(products);
        closeModalWindow();
    };
    go2Choice('id_set=null');
});

$("form#ProductsSets").on('click', '.removeOwnerProduct', function () {
    $(this).parents('tr:first').remove();
});

var go2Choice = function (query) {
    if ('string' !== typeof query) {
        query = '';
    }
    $('form#ProductsSets input[type="hidden"].id_product').each(function () {
        query += '&excludeid[]=' + $(this).val();
    });
    loadModalWindow('/products/choice/list', getTargetIndex('blank'), query);
};

var populateOwners = function (products) {
    $.each(products, function (key, product) {
        $row = $(
            '<tr>' +
            '<td><input type="hidden" class="id_product" name="ownerProducts[]" value="' + product.id + '"></td>' +
            '<td style="width: 300px;">' + product.title + '</td>' +
            '<td><strong style="font-size: 11px;">' + product.status_title + '</strong></td>' +
            '<td>' + product.price_retail + '</td>' +
            '<td><span class="btn btn-xs fa fa-times removeOwnerProduct"></span></td>' +
            '</tr>'
        );

        $row.find('td:first').prepend(getShowProductButton(product.code, product.id ))

        $(".ownerProductsContainer").append($row);
    });
};

var populateProductsGroup = function (products, container) {
    $.each(products, function (key, product) {
        $row = $('<tr data-id_product="'+product.id+'">' +
                    '<td>' +
                    '<input type="hidden" class="id_product" name="products[' + product.id + '][id_product]" value="' + product.id + '">' +
                    '</td>' +
                    '<td class="sort-handle" style="width: 300px;">' + product.title + '</td>' +
                    '<td><strong style="font-size: 11px;">' + product.status_title + '</strong></td>' +
                    '<td>' + product.price_retail + '</td>' +
                    '<td><span class="discounts"></span>%</td>' +
                    '<td><span class="btn btn-xs fa fa-times removeProduct"></span></td>' +
                '</tr>');

        $row.find('td:first').prepend(getShowProductButton(product.code, product.id ))

        container.append($row);
    });



    $("#form_ProductsSets tbody").sortable({
        items: '.group',
        handle: ".sort-handle",
        axis: 'y',
        cursor: 'move',
        opacity: 0.6,
        update: function(event, ui) {
            $("form#ProductsSets").trigger('update');
        }
    });

    $(".productsContainer tbody").sortable({
        items: 'tr',
        handle: ".sort-handle",
        axis: 'y',
        cursor: 'move',
        opacity: 0.6,
        update: function(event, ui) {
            $("form#ProductsSets").trigger('update');
        }
    });

    $("form#ProductsSets").trigger('update');
};

var populateProductsDiscount = function (discount) {
    $.each(discount, function (id_product, discounts) {
        $.each(discounts, function (discount_key, discount_item) {
            $('input[name="products[' + id_product + '][discount]['+discount_key+']"]').val(discount_item);
        });
    });
};


var getShowProductButton = function (button_title, id_product) {
    return $("<button>")
        .addClass('btn btn-xs')
        .attr({
            "data-access" : "allow",
            "data-modal-route-name" : "products-product-view",
            "data-toggle" : "tooltip",
            "title" : "Просмотр Товара",
            "data-modal-link" : "http://admin.megabite.loc/products/product/view/" + id_product
        })
        .html(button_title);
}

$(function () {
    $("#save").parent().before('<th>Группа <i id="addGroupButton" class="fa fa-lg fa-plus btn"></i></th>');

    $("form#ProductsSets .title").parents('tr:first').after(
        '<tr>' +
            '<th>Товары</th>' +
            '<td>' +
                '<table class="ownerProductsContainer table-striped table-hover table-condensed" style="border: 0;"></table>' +
                '<i id="addOwnerProductButton" class="fa fa-lg fa-plus btn"></i>' +
            '</td>' +
        '</tr>'
    );
});
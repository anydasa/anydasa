$(function () {

    $("#advancedSearchProductButton").on('click', function () {
        $("#advancedSearchProductContainer").toggle();
    });

    $("input[name='id[]']").shiftClick();

    promtNotFoundCodes();

    $("#select-all-products").click(function() {
        var checkBoxes = $("input[name='id[]']");
        checkBoxes.prop("checked", $(this).prop("checked"));
        checkBoxes.change();
    });

    $(document).on('change', "input[name='id[]']", function () {
        if ( $("input[name='id[]']:checked").length ) {
            $("#multiChangeButtons").show();

            $("#multiChangeButtons button[data-modal-link]").each(function () {
                var link = $(this).attr('data-modal-link');
                link = updateQueryStringParameter(link, 'id_products', $("input[name='id[]']:checked").map(function() {return $( this ).val();}).get().join(","));
                $(this).attr('data-modal-link', link)
            });

        } else {
            $("#multiChangeButtons").hide();
        }
    })

    $(document).on('click', '#multiChangeButtons button', function (e) {

        if ( undefined !== $(this).attr('data-modal-link') ) {
            return;
        }

        if ( 'prices' == $(this).attr('data-action') && $("input[name='id[]']:checked").length > 20) {
            alert('Можно парсить не более 20 цен');
            e.preventDefault();
            return;
        }

        $("[name=submit-action]").val( $(this).attr('data-action') )
        $("[name=submit-value]").val( $(this).attr('data-value') )

        $(this).parents('form').submit();
    });


    $(".goods-list a[data-action][data-value]").each(function () { $(this).addDataClassInRow() })



  $("a[data-action][data-value]").click(function() {
        el = this;
        $("body").addClass('wait')

        $.get(this.href, function(data) {
            $(el).attr('data-value', data).addDataClassInRow()
            $("body").removeClass('wait')
        });

        return false;
    });

    $("tbody [name='set-sort']").change(function () {
        var $el = $(this);
        $el.prop('disabled', true);
        $.post('/products/product/edit/attr/'+$(this).attr('data-id'), {name: 'sort', value: $(this).val()})
            .error(function(data) {
                alert('Ошибка, повторите снова');
            })
            .always(function () {
                $el.prop('disabled', false);
            })
    }).keypress(function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    });
});


$.fn.addDataClassInRow = function () {
    el = this

    $.each( $(el).parents('tr').classes() , function(index, item) {
        if ( item.match(new RegExp("^" + $(el).attr('data-action') + ".+$")) ) {
            $(el).parents('tr').removeClass(item)
        }
    });

    $(this).parents('tr').addClass(
        $(this).attr('data-action') + '_' + $(this).attr('data-value')
    )
};

$.fn.classes = function (callback) {
    var classes = [];
    $.each(this, function (i, v) {
        var splitClassName = v.className.split(/\s+/);
        for (var j in splitClassName) {
            var className = splitClassName[j];
            if (-1 === classes.indexOf(className)) {
                if ('function' !== typeof className) {
                    classes.push(className);
                }
            }
        }
    });
    return classes;
};


Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};

function promtNotFoundCodes()
{
    if ( '' == getQueryParam('code').trim() ) {
        return
    }

    var codes_request = getQueryParam('code').split("\n").map(Function.prototype.call, String.prototype.trim);
    var codes_found   = new Array();
    $("[data-product-code]").each(function () {
        codes_found.push($(this).attr('data-product-code').trim())
    })
    var not_found_codes = codes_request.diff(codes_found);
    if ( not_found_codes.length > 0 ) {
        var str = 'Не найдены товары с кодами: <ul>'
        $.each(not_found_codes, function (index) {
            str += '<li>"' + not_found_codes[index] + '"</li>'
        })
        str += '</ul>'
        $.prompt(str)
    }
}

function processProductsMarginsForm() {
    window.currentProductStickerCodes = [];
    var pricePm = $('#price_pm').val();
    var partialMonthVal = $('#id_partial_payments').val();
    var currentPartialMonthPercent = 0;
    var priceRetail = $('#price_retail').val();
    var calculatedPrice = 0;

    if (partialMonthVal !== '' && partialMonthVal !== 0) {

    }

    if (typeof window.currentProductStickers === 'object') {
        for (var code in window.currentProductStickers) {
            window.currentProductStickerCodes.push(window.currentProductStickers[code]['id']);
        }
        if (window.currentProductStickerCodes.indexOf(25) === -1) {
            document.getElementById('id_partial_payments').setAttribute('disabled','disabled');
        }
    }

    processPartialMonthPricePmChanges();

    $(document).on('input', '#price_retail', function () {
        processPartialMonthPricePmChanges();
    });

    $(document).on('input', '#id_partial_payments', function () {
        processPartialMonthPricePmChanges();
    });

    $(document).on('input', '#price_pm', function () {
        processPartialMonthPricePmChanges();
    });

    function processPartialMonthPricePmChanges() {
        pricePm = $('#price_pm').val();
        partialMonthVal = $('#id_partial_payments').val();
        priceRetail = window.currentProductPrice;
        currentPartialMonthPercent = 0;

        if (partialMonthVal !== '' && partialMonthVal !== 0) {
            for (var code in window.partialPaymentsVars) {
                if (partialMonthVal == window.partialPaymentsVars[code]['id']) {
                    currentPartialMonthPercent = window.partialPaymentsVars[code]['percent_value'];
                }
            }
        }

        if (window.currentProductStickerCodes.indexOf(25) === -1) {
            if (isNaN(parseFloat(pricePm)) || parseFloat(pricePm) == 0) {
                calculatedPrice = parseFloat(priceRetail) + parseFloat(priceRetail) * 0.0275;
                $('.price_pm_helper').text('цена розничная + (2,75%): ' + calculatedPrice);
            } else  {
                $('.price_pm_helper').text('');
            }
        } else {
            if (isNaN(parseFloat(pricePm)) || parseFloat(pricePm) == 0) {
                calculatedPrice = parseFloat(priceRetail) + parseFloat(priceRetail) * 0.0275 + parseFloat(priceRetail) * parseFloat(currentPartialMonthPercent)/100;
                $('.price_pm_helper').text('цена розничная + (2,75% + %ОтВыбранногоЗначенияК-воМесяцев): ' + calculatedPrice);
            } else  {
                $('.price_pm_helper').text('');
            }
        }
    }
}
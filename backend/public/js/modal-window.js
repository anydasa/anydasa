$(function () {
    $(document).on('click', ".mw-close", closeModalWindow);
    $(document).bind('keydown', function (event) {if (27 == event.which) closeModalWindow()});

    $(document).on('click', '[data-modal-link]', function() {
        var targetIndex = getTargetIndex($(this).attr('data-target'), 'blank')

        if ( targetIndex < 1 ) {
            $('body').addClass('wait')
            document.location = $(this).attr('data-modal-link');
            return false;
        }

        loadModalWindow($(this).attr('data-modal-link'), targetIndex, $(this).attr('data-post'), $(this).attr('data-method'));
        return false;
    });

    $(document).on('submit', '.mw-content form', function (e) {
        $form = $(this)

        $(this).find('[type=submit]').each(function () {
            $(this)
                .attr('data-text', $(this).html())
                .css({height: $(this).outerHeight(true), width: $(this).outerWidth(true)})
                .html('<i class="fa fa-refresh fa-spin"></i>');
        });


        var type     =  'POST' == $form.attr('method').toUpperCase() ? 'POST' : 'GET';
        var formData = ('POST' == type) ? new FormData(this) : $form.serialize() ;

        $.ajax({
            url:         $form.attr('action'),
            type:        type,
            data:        formData,
            mimeType:    $form.attr('enctype'),
            contentType: false,
            cache:       false,
            processData: false
        }).error(function(jqXHR) {
            if ( 500 == jqXHR.status ) {
                openModalWindow(jqXHR.responseText, this.url, getTargetIndex('blank', 'blank'))
                $(".mw-content [type=submit][data-text]").each(function () {
                    $(this).html( $(this).attr('data-text')).removeAttr('style');
                });
            } else {
                openModalWindow(jqXHR.responseText, this.url, getTargetIndex('self', 'self'))
            }
        }).success(function(data, status, xhr) {

            if ( checkAndEval(data) ) {
                closeModalWindow()
                return;
            }

            var redirectUrl = getUrlIfRedirect(data);
            var targetIndex = getTargetIndex($form.attr('data-target'), 'prev');
            if ( targetIndex < 1 && redirectUrl ) {
                document.location = redirectUrl;
                return;
            }

            if ( targetIndex < 1 ) {
                targetIndex = 1;
            }

            if ( redirectUrl ) {
                loadModalWindow(redirectUrl, targetIndex)
            } else {
                openModalWindow(data, this.url, targetIndex)
            }
            $(".mw-content [type=submit][data-text]").each(function () {
                $(this).html( $(this).attr('data-text')).removeAttr('style');
            });

        });

        return false;
    })
})

function checkAndEval(data)
{
    if ( 'Eval:' == data.substr(0,5) ) {
        eval(data.substr(5));
        return true;
    }
    return false;
}

function getTargetIndex(target, def)
{
    if ( undefined == target ) {
        target = def
    }
    if ( 'self' == target ) {
        var targetIndex = $(".mw-wrap").length;
    } else if ( 'prev' == target ) {
        var targetIndex = $(".mw-wrap").length - 1;
    } else {
        var targetIndex = $(".mw-wrap").length + 1;
    }

    return targetIndex;
}

function getUrlIfRedirect(data)
{
    if ( 'Location:' == data.substr(0,9) ) {
        return data.substr(9)
    }
    return false
}


function loadModalWindow(url, targetIndex, params, method)
{
    if ( undefined == targetIndex ) {
        targetIndex = getTargetIndex('blank');
    }

    if ( undefined == method ) {
        method = 'GET';
    }

    if ( undefined == params || method == 'GET') {
        params = '';
    }



    $('body').addClass('wait');
    $.ajax({
        url:         url,
        type:        method,
        data:        params,
        cache:       false,
        processData: false
    }).error(function(data) {
        openModalWindow(data.responseText, this.url, targetIndex);
    }).success(function (data) {
        var redirectUrl = getUrlIfRedirect(data);
        if ( redirectUrl ) {
            if ( targetIndex < 1 ) {
                document.location = redirectUrl;
                return;
            } else {
                loadModalWindow(redirectUrl, targetIndex, params)
            }
        } else {
            openModalWindow(data, this.url, targetIndex)
        }
    });
}

function openModalWindow(data, url, targetIndex)
{
    $('body').removeClass('wait')

    var lastIndex = $(".mw-wrap").length + 1;

    if ( 0 == $("#mw-fade").length ) {//Если нет полупрозрачного слоя, показываем
        $("<div />").attr('id', 'mw-fade').appendTo('body')
        $("body").css({overflow: 'hidden'})
    }
    for ($i=lastIndex-targetIndex; $i>0; $i--) {
        $('.mw-wrap.mIndex'+$(".mw-wrap").length).remove();
    }

    $("<div />").addClass('mw-wrap mIndex'+targetIndex).attr('data-url', url).appendTo('body')
    $("<div />").addClass('mw-content').hide().appendTo('.mw-wrap.mIndex'+targetIndex)
    $("<div />").addClass('mw-move').appendTo('.mw-wrap.mIndex'+targetIndex+' .mw-content')
    $("<div />").addClass('mw-close').html('&times;').appendTo('.mw-wrap.mIndex'+targetIndex+' .mw-content')
    $('<div>'+data+'</div>').appendTo('.mw-wrap.mIndex'+targetIndex+' .mw-content')

    $('.mw-wrap.mIndex'+targetIndex+' .mw-content').css({position:'relative', top: ((targetIndex-1)*40), left: ((targetIndex-1)*20)}).show()
    $('.mw-wrap.mIndex'+targetIndex+' .mw-content').draggable({handle: '.mw-move'});

    $('.mw-wrap').removeClass('last').last().addClass('last')

    $("form input[type=submit]").click(function() {
        $("<input />")
            .attr('type', 'hidden')
            .attr('name', $(this).attr('name'))
            .attr('value', $(this).attr('value'))
            .appendTo(this)
    });

    setupPage();
}


function closeModalWindow()
{
    var onclose = $('.mw-wrap.mIndex'+$(".mw-wrap").length).data('onclose');

    $('.mw-wrap.mIndex'+$(".mw-wrap").length).remove();
    $('.mw-wrap').removeClass('last').last().addClass('last')

    if ( 0 == $(".mw-wrap").length ) {
        $("#mw-fade").remove();
        $("body").css({overflow: 'auto'})
    }

    if ( 'reload-prev' == onclose ) {
        reloadModalWindow( getTargetIndex('self') )
    }
}

function reloadModalWindow(index)
{
    if ( undefined == index ) {
        index = $(".mw-wrap").length;
    }

    if ( index < 1 ) {
        window.location.reload();
    } else {
        var url = $('.mw-wrap.mIndex'+index).data('url')

        $('.mw-wrap.mIndex'+index).remove();
        loadModalWindow(url, index)
        $('.mw-wrap').removeClass('last').last().addClass('last')
    }
}

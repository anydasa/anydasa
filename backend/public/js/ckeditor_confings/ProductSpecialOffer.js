
CKEDITOR.editorConfig = function( config ) {

    config.contentsCss = [
        '//' +location.hostname.replace('adm.', '')+ '/css/ck.css',
        '//' +location.hostname.replace('adm.', '')+ '/css/pages/product.css'
    ];
	config.bodyClass   = 'special_offer_block';

	config.language = 'ru';

	config.toolbar = [
		{ name: 'lists', items: [
			'NumberedList',
			'BulletedList',
			'Outdent',
			'Indent','Link','Unlink','FontSize','Bold',
			'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'
		] }
	];

	config.height = '200px';
	config.width = '250px';

    config.scayt_autoStartup = false;
    config.disableNativeSpellChecker = false;
};
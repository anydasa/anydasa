﻿/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

function getTplContent(name) {
    var strUrl = "/js/ckeditor_confings/templates/article/templates/"+name;
    var strReturn = "";

    jQuery.ajax({
        url: strUrl,
        success: function(html) {
            strReturn = html;
        },
        async:false
    });

    return strReturn;
}

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default', {
    // The name of sub folder which hold the shortcut preview images of the
    // templates.
    imagesPath: "/js/ckeditor_confings/templates/article/images/",

    // The templates definitions.
    templates: [
        {
            title: 'Страница 1',
            image: '',
            description: 'Вариант 1',
            html: getTplContent("1.html")
        }
    ]
} );
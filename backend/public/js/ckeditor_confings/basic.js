/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.language = 'ru';

	// The toolbar groups arrangement, optimized for a single toolbar row.
	config.toolbarGroups = [
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'tools' },
		{ name: 'others' }
	];

	// The default plugins included in the basic setup define some buttons that
	// are not needed in a basic editor. They are removed here.
	config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Strike,Subscript,Superscript,Print';
    config.allowedContent = true;
	// Dialog windows are also simplified.
	config.removeDialogTabs = 'link:advanced';
	
	config.filebrowserUploadUrl  =  "base64";
	
    config.scayt_autoStartup = false;
    config.disableNativeSpellChecker = false;


    config.filebrowserBrowseUrl         = '/ckfinder/ckfinder.html&currentFolder=/ddd/';
    config.filebrowserImageBrowseUrl    = '/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl    = '/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl         = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&currentFolder=/aaa/';
    config.filebrowserImageUploadUrl    = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&currentFolder=/aaa/';
    config.filebrowserFlashUploadUrl    = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash&currentFolder=/asd/';
};
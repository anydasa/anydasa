/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

	config.language = 'ru';
	config.toolbarGroups = [
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools', 'tools', 'Aave' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'  ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'styles' },
		{ name: 'colors' }
	];

	config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Underline,Strike,Subscript,Superscript,Print,SpecialChar,Blockquote';

	config.removeDialogTabs = 'link:advanced';
	config.height = '500px';
	config.width = '970px';
    config.allowedContent = true;
    CKEDITOR.config.protectedSource.push(/\{%\s.+\s%\}/g);

    config.filebrowserBrowseUrl         = '/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl    = '/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl    = '/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl         = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl    = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl    = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    
    config.autoParagraph = false;
    config.startupOutlineBlocks = true;  
    
    config.scayt_autoStartup = false;
    config.disableNativeSpellChecker = false;
};
CKEDITOR.editorConfig = function( config ) {

	config.contentsCss = [
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css',
		'http://all.dev/css/all.css'
	];

	config.language = 'ru';
	config.toolbarGroups = [
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',	groups: [ 'list', 'indent', 'blocks', 'align' ] },
		{ name: 'links'},
		{ name: 'insert'},
		{ name: 'styles',   groups: [ 'image', 'table']},
		{ name: 'colors' },
		{ name: 'tools' },
		{ name: 'others' }
	];

	config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Underline,Strike,Subscript,Superscript,Print,Flash,SpecialChar,Smiley,PageBreak,Iframe';
    config.allowedContent = true;

	config.scayt_autoStartup = false;
    config.disableNativeSpellChecker = false;


	config.extraPlugins = 'bootstrapTabs,btgrid';


	config.startupOutlineBlocks = false;

	config.filebrowserBrowseUrl         = '/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl    = '/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl    = '/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl         = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl    = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl    = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};

CKEDITOR.on('instanceReady', loadBootstrap);
CKEDITOR.on('mode', loadBootstrap);

function loadBootstrap(event) {
	if (event.name == 'mode' && event.editor.mode == 'source')
		return; // Skip loading jQuery and Bootstrap when switching to source mode.

	var jQueryScriptTag = document.createElement('script');
	var bootstrapScriptTag = document.createElement('script');

	jQueryScriptTag.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
	bootstrapScriptTag.src = 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js';

	var editorHead = event.editor.document.$.head;

	editorHead.appendChild(jQueryScriptTag);
	jQueryScriptTag.onload = function() {
		editorHead.appendChild(bootstrapScriptTag);
	};
}

CKEDITOR.addTemplates( 'fet', {
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
	imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

	// The templates definitions.
	templates: [ {
		title: 'Image and Title1111',
		image: 'template4.gif',
		description: 'One main image with a title and text that surround the image.',
		html: '<h3>' +
			// Use src=" " so image is not filtered out by the editor as incorrect (src is required).
		'<img src=" " alt="" style="margin-right: 10px" height="100" width="100" align="left" />' +
		'Type the title here' +
		'</h3>' +
		'<p>' +
		'Type the text here' +
		'</p>'
	} ]
} );
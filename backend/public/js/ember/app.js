App = Ember.Application.create();

App.Router.map(function() {

});

App.IndexRoute = Ember.Route.extend({
    model: function () {
        var orders = [];

        orders.pushObject(Ember.Object.create({
            id : 1,
            email: 'anydasa@gmail.com'
        }));

        orders.pushObject(Ember.Object.create({
            id : 2,
            email: 'anydasa2@gmail.com'
        }));

        orders.pushObject(Ember.Object.create({
            id : 3,
            email: 'anydasa3@gmail.com'
        }));

        orders.pushObject(Ember.Object.create({
            id : 4,
            email: 'anydasa4@gmail.com'
        }));

        return orders;
    }
});

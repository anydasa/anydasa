$(document).bind('keydown', kbDescribeEvent);

$(function() {
    menuSetup();
    setupPage();

    function toggleCollectionElement($el)
    {
        var id = $el.attr('data-id').toInt()
        var cookieList = $.fn.cookieList($el.attr('name'));

        if ( cookieList.contain(id) ) {
            $("button[data-toggle='CollectionRowToggle'][data-id='"+id+"']").val(0)
            cookieList.remove(id);
            if ( $el.parents('#mw-wrap').length ) {
                if ( 0 == cookieList.length() ) {
                    $el.parents('#mw-wrap').find('#mw-close').click()
                } else {
                    $el.parents('tr').remove()
                }
            }
        } else {
            $("button[data-toggle='CollectionRowToggle'][data-id='"+id+"']").val(1)
            cookieList.add(id);
        }

        $("[data-CollectionRowCount]").attr('data-CollectionRowCount', cookieList.length())
    }

    var $chkboxes = $("button[data-toggle*='CollectionRowToggle']")
    var firstChecked = null
    $(document).on('click', "button[data-toggle*='CollectionRowToggle']", function(event) {
        if( event.shiftKey && null !== firstChecked) {
            var start = $chkboxes.index(firstChecked);
            var end   = $chkboxes.index($(this));
            $chkboxes
                .slice(Math.min(start,end), Math.max(start,end))
                .not(firstChecked)
                .add($(this))
                .filter(function () {
                    return $(this).val() != $(firstChecked).val()
                })
                .each(function () {
                    toggleCollectionElement($(this))
                })
        } else {
            firstChecked = this;
            toggleCollectionElement($(this))
        }
        return false;
    });

//----------------------------------------------------------------------------

    $(".TableMultiChange .TMC__SelectAllCheckbox").click(function() {
        var checkBoxes = $(this).closest('.TableMultiChange').find(".TMC__ItemCheckbox");
        checkBoxes.prop("checked", $(this).prop("checked"));
        checkBoxes.change();
    });

    $(".TableMultiChange .TMC__ItemCheckbox").shiftClick();

    $(document).on('change', ".TableMultiChange .TMC__ItemCheckbox", function () {
        var $TMC = $(this).closest('.TableMultiChange');
        var $TMC_ButtonsBlock = $TMC.find(".TMC__ButtonOperationBlock");
        var $TMC_SelectAllCheckbox = $TMC.find(".TMC__SelectAllCheckbox");

        if ( $TMC.find(".TMC__ItemCheckbox:checked").length ) {
            $TMC_ButtonsBlock.show();

            $TMC_ButtonsBlock.find("button[data-modal-link]").each(function () {
                var link = $(this).attr('data-modal-link');
                link = updateQueryStringParameter(
                    link,
                    $TMC_SelectAllCheckbox.attr('name'),
                    $TMC.find(".TMC__ItemCheckbox:checked")
                        .map(function() {
                            return $( this ).val();
                        })
                        .get()
                        .join(",")
                );
                $(this).attr('data-modal-link', link)
            });

        } else {
            $TMC_ButtonsBlock.hide();
        }
    });

//----------------------------------------------------------------------------

    $(document).on('click', "button[data-edit-attr-url][name][value]", function(event) {
        if ( 1 == $(this).attr('value') )  {
            $val = 0;
        } else {
            $val = 1;
        }
        editAttr($(this), $val)
        return false;
    });

    $(document).on('change', "select[data-edit-attr-url][name]", function(event) {
        editAttr($(this), $(this).val())
    });

    function editAttr($el, $val)
    {
        $el.blur().animate({opacity: 0.5}, 300);
        $.ajax({
            type: "POST",
            url: $el.attr('data-edit-attr-url'),
            data: {name: $el.attr('name'), value: $val},
            dataType: 'json'
        }).done(function(data) {
            $el.stop().animate({opacity: 1}, 10);
            $el.val(data['value'])

            $el.parents('tr').attr('data-'+$el.attr('name'), data['value']) //установить значение в рядок

            if ( 0 != data['error']) {
                alert(data['error'])
            }
        })
    }

    //----------------------------------------------------------------------------
    $(document).keydown(function(event){
        if (event.keyCode == 27 && $(".jqibox").length) {
            $.prompt.close()
        }
    });
    //----------------------------------------------------------------------------

    $(document).on('click', "[data-toggle*=rtm]", function(event) {
        $("body").addClass('wait');
        $.get(this.href, function(data) {
            openModalWindow( data );
            $("body").removeClass('wait')
        });
        return false;
    });
//----------------------------------------------------------------------------

})

function filterMultiCheckbox(inputSelector, MultiCheckboxContainer)
{
    $(inputSelector).on('keyup', function () {
        $el = $(this)
        $(MultiCheckboxContainer).find('label').hide()
            .filter(function () {
                return $(this).text().toLowerCase().indexOf($el.val().toLowerCase()) >= 0 || '' == $el.val();
            }).show()

    })
}

jQuery.fn.extend({
    filterMultiCheckbox: function() {
        this.each(function() {
            $(this).wrapAll('<div class="filterMultiCheckboxContainer"></div>')

            var input = $("<input type='text' placeholder='фильтр'>")
                .attr('id', $(this).attr('id')+'1')
                .width( $(this).width() );
            $(this).before(input);

            filterMultiCheckbox('#'+$(this).attr('id')+'1', this);

        });
    }
});

function filterSelect(inputSelector, selectSelector)
{
    $(selectSelector).find('option:disabled').attr('data-disabled', 'disabled').css({fontWeight: 'bold', color: 'black'});

    $(inputSelector).on('keyup', function () {
        $el = $(this)

        $(selectSelector).find('option').hide().prop('disabled', true)
            .filter(function () {
                return $(this).html().toLowerCase().indexOf($el.val().toLowerCase()) >= 0 || '' == $el.val();
            })
            .prop('disabled', false).show()
            .filter(function () {
                return $(this).attr('data-disabled') == 'disabled';
            })
            .prop('disabled', true)

        $(selectSelector).find('optgroup').each(function () {
            $(this).find("option:enabled").length ? $(this).show() : $(this).hide()
        })
    })
}

function setupPage(jqXHR, textStatus)
{
    accessSetup();
    confirmSetup();
    tooltipSetup();

    $("input[type=text][slug]").each(function () {
        $(this).before(
            $("<div>")
                .addClass("input-action slug-action")
                .attr('data-slug-element', "#"+$(this).attr("id"))
                .html('slug')
        );
    })

    $('.table-sorted tbody').sortable({
        axis: 'y',
        update: function(event, ui) {
            $form = $(this).parent().find(".sortForm")

            $form.show()
            $form.find("input[name='sort\\[\\]']").remove()

            $(this).find('> tr').each(function () {
                $('<input/>').attr({
                    name: 'sort[]',
                    type: 'hidden',
                    value: $(this).attr('data-id')
                }).appendTo($form)
            })
        }
    });

    $("form [data-toggle*='autocomplete']").each(function () {
        $element = $(this);
        $element.autocomplete({
            source: function (request, response) {
                var field = $(this.element).attr('name');
                $.getJSON(
                    $(this.element).closest("form").attr('action'),
                    $(this.element).closest("form").serialize() + '&__field=' + field,
                    function( data, status, xhr ) {
                        response($.map(data, function (item) {return item[field]}) );
                    }
                );
            },
            appendTo: $element.parent(),
            minLength: 0
        }).focusin(function() {
            $(this).autocomplete("search");
        });
    });

    $('[data-popnote-label]').hover(function () {
        $('[data-popnote-text="'+$(this).attr('data-popnote-label')+'"]').show()
    }, function () {
        $('[data-popnote-text="'+$(this).attr('data-popnote-label')+'"]').hide()
    })
}

function confirmSetup()
{
    $("[data-toggle*=confirm]").off('click').on('click', function(event) {
        if ( !$.isEmptyObject($(this).data('confirm-title')) ) {
            var title = $(this).data('confirm-title')
        } else if ( !$.isEmptyObject($(this).data('original-title')) ) {
            var title = $(this).data('original-title')
        } else if ( !$.isEmptyObject($(this).attr('title')) ) {
            var title = $(this).attr('title')
        } else {
            var title = 'Уверенны'
        }
        if ( confirm(title + '?') ) {
            return true;
        } else {
            event.stopImmediatePropagation();
            event.stopPropagation();
            return false;
        }
    });
}

function menuSetup()
{
    $('#ddmenu li').hover(function (event) {

        clearTimeout($.data(this,'timer'));
        $('>ul', this).stop(true,true).slideDown(200).css({left: $(event.currentTarget).position().left + $(event.currentTarget).width() + 'px'})
    }, function () {
        $.data(this, 'timer', setTimeout($.proxy(function() {
            $('>ul', this).stop(true,true).slideUp(0);
        }, this), 100));
    });
}
function tooltipSetup()
{
    $(document).find("[data-toggle*=tooltip]").tooltip({html: true});

    $(document).find('.tooltip-image').tooltip({
        tooltipClass: "tooltip_message",
        position: {my: "left+20 top-5"},
        items: "img",
        content: function() {
            return '<img src="'+ $(this).attr('src') +'" style="max-width: 300px; max-height: 300px;">';
        }
    })
}
function accessSetup()
{
    $(document).find('[data-access=deny]').addClass('tooltip-error');
    $(document).find('[data-access=deny]').attr('title', 'Нет доступа');
    $(document).find('[data-access=deny]').tooltip();

    $('[data-access=deny]').off('click').on('click', function(event) {
        event.stopImmediatePropagation();
        return false;
    });
}


function kbDescribeEvent(event) {

    if (event.ctrlKey && 13 == event.which)
        $(".standart-form input[type='submit']").click();
    if (event.ctrlKey && 37 == event.which && $('#prev').length)
        location = $('#prev').attr('href');
    if (event.ctrlKey && 39 == event.which && $('#next').length)
        location = $('#next').attr('href');
    if (event.ctrlKey && 78 == event.which && event.altKey)
        location = $(".standart-list a.addlink").attr('href');
}

function parseUrl( url ) {
    var a = document.createElement('a');
    a.href = url;
    return a;
}

function getQueryParam(name, search)
{
    search = typeof search !== 'undefined' ? search : location.search;

    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex   = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(search);

    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

String.prototype.toInt = function() {
    return parseInt(this);
}
String.prototype.toFloat = function() {
    return parseFloat(this);
}

jQuery.fn.extend({
    renameAttr: function( name, newName, removeData ) {
        var val;
        return this.each(function() {
            val = jQuery.attr( this, name );
            jQuery.attr( this, newName, val );
            jQuery.removeAttr( this, name );
            // remove original data
            if (removeData !== false){
                jQuery.removeData( this, name.replace('data-','') );
            }
        });
    }
});

$.fn.shiftClick = function() {
    var lastSelected; // Firefox error: LastSelected is undefined
    var checkBoxes = $(this);
    this.each(function () {
        $(this).click(function (ev) {
            if (ev.shiftKey) {
                if (!lastSelected) {
                    alert("Ouch")
                    return falee;
                }
                var last = checkBoxes.index(lastSelected);
                var first = checkBoxes.index(this);
                var start = Math.min(first, last);
                var end = Math.max(first, last);
                var chk = lastSelected.checked;
                for (var i = start; i <= end; i++) {
                    checkBoxes[i].checked = chk;
                }
            } else {
                lastSelected = this;
            }
        })
    });
};

function detectIE() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // IE 12 => return version number
        return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

function SetCaretAtEnd(elem) {
    var elemLen = elem.value.length;
    // For IE Only
    if (document.selection) {
        // Set focus
        elem.focus();
        // Use IE Ranges
        var oSel = document.selection.createRange();
        // Reset position to 0 & then set at end
        oSel.moveStart('character', -elemLen);
        oSel.moveStart('character', elemLen);
        oSel.moveEnd('character', 0);
        oSel.select();
    }
    else if (elem.selectionStart || elem.selectionStart == '0') {
        // Firefox/Chrome
        elem.selectionStart = elemLen;
        elem.selectionEnd = elemLen;
        elem.focus();
    } // if
}

$(document).on('click', '.slug-action', function () {
    var $element = $( $(this).attr('data-slug-element') );
    $.post('/utils/slug/', {string:$element.val()}, function (data) {
        $element.val(data).focusout();
    }).error(function () {
        alert('Ошибка!! Возможно нет прав');
    });

});

$(document).on('click', '#select-who-created', function () {
    who_created = $(this).data('user-id');
    $('#who_created option[value="' + who_created + '"]').attr('selected', 'selected');
    $('#submit_filters').click();
});

$(document).on('click', '#filter_answer', function () {
    answer = $(this).data('value');
    $('#sent_answer option[value="' + answer + '"]').attr('selected', 'selected');
    $('input[type="submit"]').click();
});

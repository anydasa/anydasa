$("#credit-block").hide();

$(document).on('change', '#payment_type', function () {
    addPaymentToCart();
    if($(this).val() == 'credit' ){
        $("#credit-block").show();
    }else{
        $("#credit-block").hide();
    }
});

function addPaymentToCart() {
    current_payment = $('#Orders select[name=payment_type]').val();
    payments = $.parseJSON( $('#payments').attr('data-payments') );
    cart_total =  $('.cart-total').attr('data-total');


    $.each(payments, function(i, v) {
        if (v.id == current_payment) {
            if(v.commission != 0){
                $('.cart-total .payment-cost').html( v.title + ' +' + Math.round(cart_total*v.commission/100));
            }else{
                $('.cart-total .payment-cost').html('');
            }
        }
    });
}
$(function () {

    var $form = $('#Orders');
    var $cityElement = $form.find("#city");
    var $deliveryTypeElements = $form.find('select[name=delivery_type]');

    $form.find('#novaposhta-block').hide();

    $cityElement.autocomplete({
        autoFocus: true,
        minLength: 2,
        messages: {
            noResults: '',
            results: function() {}
        },
        source: function( request, response ) {
            $.getJSON('/ajax/np/get-cities/', {q:request.term}, function( data, status, xhr ) {
                response($.map(data, function (value, key) {
                    return {
                        value: value.description_ru + ' ' + value.NovaposhtaAreas.description_ru,
                        code: value.code
                    };
                }));
            });
        },
        change: function (event, ui) {
            if( !ui.item ){
                $cityElement.val("");
                $form.find("#city_code").val("").change();
            }
        },
        select: function (event, ui) {
            $form.find("#city_code").val(ui.item.code).change();
            getNpAdresses();
        },
        appendTo: $element.parent(),
    });

    $cityElement.on('keyup', function () {
        if ( $(this).val().length == 0 ) {
            $form.find("#city_code").val("").change();
        }
    });


    $deliveryTypeElements.on('change', function () {
        getNpAdresses();
        addDeliveryToCart();
    });

    function getNpAdresses(){
        if ( 'novaposhta' === $deliveryTypeElements.val()) {
            $form.find('#novaposhta-block').show();
            $form.find('#warehouse option[value!=""]').remove();

            $.get('/ajax/np/get-warehouses/', { city_code: $form.find("#city_code").val() }, function (data) {
                $.each( data, function( key, val ) {
                    $form.find('#warehouse').append(
                        $("<option />")
                            .val(val.code)
                            .text(val.description_ru)
                    );
                });
            }, 'json');
        }else{
            $form.find('#novaposhta-block').hide();
        }
    }

});

function addDeliveryToCart() {
    current_delivery = $('#Orders select[name=delivery_type]').val();
    deliveries = $.parseJSON( $('#deliveries').attr('data-deliveries') );
    cart_total =  $('.cart-total').attr('data-total');


    $.each(deliveries, function(i, v) {
        if (v.id == current_delivery) {
            if(cart_total < v.free_from_price){
                $('.cart-total .delivery-cost').html( v.title + ' +' + v.cost);
            }else{
                $('.cart-total .delivery-cost').html( '' );
            }

        }
    });
}
<?

class Site_View_Helper_ModalLinkHelp
{

    public function ModalLinkHelp($id, $buttonText = '<i class="fa fa-lg fa-question-circle"></i>')
    {
        $Note = Doctrine::getTable('HelpNote')->find($id);

        return Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
            'routeName'   => 'help-note-view',
            'routeParams' => array('id'=> $Note->id),
            'buttonText'  => $buttonText,
            'buttonTitle' => 'Подсказка: ' . $Note->title,
        ));
    }

}
<?

class Site_View_Helper_linkAccess
{

    public function linkAccess($route_name)
    {
        if ( Zend_Registry::get('User')->isAllowedRoute($route_name) ) {
            return 'allow';
        }
        return 'deny';
    }

}
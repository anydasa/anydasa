<?

class Site_View_Helper_ButtonResetSearch
{

    public function ButtonResetSearch()
    {
        return Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
            'routeName'   => Zend_Registry::get('myCurrentRoute')->name,
            'routeParams' => Zend_Controller_Front::getInstance()->getRequest()->getParams(),
            'buttonText'  => '<img src="/img/broom.png" />',
            'buttonTitle' => 'Сбросить поиск',
            'buttonAttr' => array('data-target'=>'self'),
        ));
    }

}
<?

class Site_View_Helper_itemListDataParams
{

    public function itemListDataParams($Obj)
    {
        $return = array();

        if ( !empty($Obj->id) ) {
           $return ['id']= $Obj->id;
        }

        if ( isset($Obj->is_visible) ) {
           $return ['is_visible']= $Obj->is_visible ? 1 : 0;
        }

        if ( isset($Obj->is_enabled) ) {
           $return ['is_enabled']= $Obj->is_enabled ? 1 : 0;
        }

        if ( isset($Obj->status) ) {
           $return ['status']= $Obj->status;
        }

        if ( isset($Obj->is_readed) ) {
           $return ['is_readed']= $Obj->is_readed ? 1 : 0;
        }

        if ( isset($Obj->is_checked) ) {
            $return ['is_checked']= $Obj->is_checked ? 1 : 0;
        }

        foreach ($return as $key=>$val) {
            $return[$key] = 'data-'.$key.'="'.$val.'"';
        }

    	return ' '.implode(' ', $return).' ';
    }

}
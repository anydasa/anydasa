<?

class Site_View_Helper_Array2String
{

    public function Array2String($array, $method = 'ul')
    {
        $dataAttributes = array_map(function($value, $key) {
            return '<strong>' . $key.'</strong>: '.$value.'';
        }, array_values($array), array_keys($array));

        return '<ul><li>' . implode('</li><li>', $dataAttributes) . '</li></ul>';
    }

}
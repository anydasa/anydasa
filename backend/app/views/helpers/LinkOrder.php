<?

class Site_View_Helper_linkOrder
{

    public function linkOrder($name)
    {
    	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());

        list($field, $type) = explode('-', $uri->getQueryAsArray()['sort']);

        if ( empty($type) || $field != $name) {
            $sort_str = $name.'-asc';
        } elseif('asc' == $type) {
            $sort_str = $name.'-desc';
        } elseif('desc' == $type) {
            $sort_str = null;
        }

        $uri->addReplaceQueryParameters( array('sort'=>$sort_str) );

    	return $uri->getUri();
    }

}
<?

class Site_View_Helper_PromotionRemaining extends Zend_View_Helper_Abstract
{

    public function PromotionRemaining(Promotions $Promotion)
    {
        $remainingDays = $Promotion->getRemainingDays();

        if ( 0 == $remainingDays ) {
            $str = 'Акция заканчивается сегодня';
        } elseif( $remainingDays > 0 ) {
            $str = 'Акция закончится через '.$remainingDays . ' '. SF::numberof($remainingDays, '', array('день', 'дня', 'дней'));
        } else {
            $remainingDays = abs($remainingDays);
            $str = 'Акция закончилась '.$remainingDays . ' '. SF::numberof($remainingDays, '', array('день', 'дня', 'дней')) . ' назад';
        }

        return $str;
    }


}
<?

class Site_View_Helper_ModalLinkBotton
{

    public function ModalLinkBotton($params = array())
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $view    = Zend_Layout::getMvcInstance()->getView();

        if ( !isset($params['routeName']) ) {
            throw new Exception('ModalLinkBotton: routerName required');
        }

        if ( !isset($params['routeParams']) ) {
            $params['routeParams'] = array();
        }
        if ( !isset($params['queryParams']) ) {
            $params['queryParams'] = array();
        }
        if ( 'here' == $params['queryParams']['redirect'] ) {
            $params['queryParams']['redirect'] = urlencode($request->getRequestUri());
        }
        if ( !isset($params['buttonTitle']) ) {
            $params['buttonTitle'] = AdminRoutes::getOne($params['routeName'], 'name')->title;
        }
        if ( !isset($params['buttonAttr']['class']) ) {
            $params['buttonAttr']['class'] = 'btn btn-xs';
        }
        if ( !isset($params['buttonAttr']['type']) ) {
            $params['buttonAttr']['type'] = 'button';
        }
        if ( !isset($params['buttonAttr']['data-toggle']) ) {
            $params['buttonAttr']['data-toggle'] = 'tooltip';
        } else {
            $params['buttonAttr']['data-toggle'] .= ' tooltip ';
        }
        $params['buttonAttr']['data-route-name'] = $params['routeName'];

//---------------------------------------------------------------------------------------------------------------------------
        $url = $view->url($params['routeParams'], $params['routeName']);
        $uri = Zend_Uri::factory($request->getScheme() . '://' . $request->getHttpHost() . $url);
        $uri->addReplaceQueryParameters($params['queryParams']);
        $url = $uri->getUri();

        $htmlParams =
                implode(' ', array_map(function($a, $b) { return "$a=\"$b\""; },array_keys($params['buttonAttr']),array_values($params['buttonAttr'])))
                .' title="'.$params['buttonTitle'].'"'
                .' data-modal-route-name="'.$params['routeName'].'"'
                .' data-access="'. $view->linkAccess($params['routeName']);

        if ( 'a' == $params['buttonType'] ) {
            return '<a href="'.$url.'" '. $htmlParams .'">'.$params['buttonText'].'</a>';
        }

        return '<button data-modal-link="'.$url.'" '. $htmlParams .'">'.$params['buttonText'].'</button>';
    }

}
<?

class Site_View_Helper_linkOrderHtml
{

    public function linkOrderHtml($name, $label)
    {
        Zend_Uri_Http::setConfig(array('allow_unwise' => true));
    	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost() . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());

        @list($field, $type) = explode('-', $uri->getQueryAsArray()['sort'] );

        if ( empty($type) || $field != $name) {
            $sort_str = $name.'-asc';
        } elseif('asc' == $type) {
            $sort_str = $name.'-desc';
        } elseif('desc' == $type) {
            $sort_str = null;
        }

        $uri->addReplaceQueryParameters( array('sort'=>$sort_str) );
        $uri->addReplaceQueryParameters( array('page'=>null) );

        $str = '<a href="'.$uri->getUri().'">'.$label.' <i class="fa fa-sort'. (($field == $name) ? '-'.$type : '').'"></i></a>';

        if ( Zend_Controller_Front::getInstance()->getRequest()->isXmlHttpRequest() ) {
            $str = str_replace('href', 'data-target="self" data-modal-link', $str);
        }

        return $str;
    }

}
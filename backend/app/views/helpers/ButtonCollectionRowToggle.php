<?

class Site_View_Helper_ButtonCollectionRowToggle
{
    public function ButtonCollectionRowToggle($modelName, $id)
    {
        return '<button
                class="btn btn-xs"
                data-toggle="CollectionRowToggle"
                name="CollectionRowToggle_'. $modelName .'"
                data-id="'. $id .'"
                value="'. (false !== array_search($id, explode(',', $_COOKIE['CollectionRowToggle_'.$modelName])) ? 1 : 0) .'"
                ><i class="fa fa-lg"></i>
            </button>';
    }
}
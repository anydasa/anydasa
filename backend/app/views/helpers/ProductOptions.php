<?

class Site_View_Helper_ProductOptions extends Zend_View_Helper_Abstract
{

    public function productOptions(Products $Product)
    {
        $str = '';

        $options = $Product->getProps(', ', false);
        $options = array_filter($options, function ($option) {
            return '' != $option['value'];
        });

        if ( !empty($options) ) {
            $str .= '<table class="table table-bordered table-condensed table-striped table-hover">';
            $prevOption = [];

            foreach ($options as $option) {

                if ( $prevOption['group'] != $option['group'] ) {
                    $str .= '<tr class="group">';
                    $str .= '<th colspan="2"><span>'. $option['group'] .'</span></th>';
                    $str .= '</tr>';
                }

                $str .= '<tr>';
                $str .= '<th><span>'. $option['param'] .'</span></th>';
                $str .= '<td><span>'. $option['value'] .' '.$option['unit'].'</span></td>';
                $str .= '</tr>';

                $prevOption = $option;
            }

            $str .= '</table>';
        }

        return $str;
    }
}
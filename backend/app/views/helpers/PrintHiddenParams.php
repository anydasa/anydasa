<?

class Site_View_Helper_PrintHiddenParams
{

    public function PrintHiddenParams($params, $belongsTo = null)
    {
        $str = '';
        foreach($params as $name=>$value) {
            if (null !== $belongsTo) {
                $name = $belongsTo . '[' . $name . ']';
            }

            if (is_array($value)) {
                $str .= $this->PrintHiddenParams($value, $name);
            } else {
                $str .= '<input type="hidden" name="'.$name.'" value="'.$value.'">';
            }
        }
        return $str;
    }
}
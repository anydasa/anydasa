<?

class Site_View_Helper_route
{

    public function route($route_reverse, $route_map)
    {
        $str = str_replace('%s', '<span>%s</span>', $route_reverse);
        $str = vsprintf($str, Site_Db_Function::pg_array_parse($route_map));

        return $str;
    }

}
<?

class Site_View_Helper_ButtonDelete
{

    public function ButtonDelete($routeName, $routeParams)
    {
        return Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
            'routeName'   => $routeName,
            'routeParams' => $routeParams,
            'buttonText'  => '<i class="fa fa-lg fa-trash-o"></i>',
            'buttonAttr'  => array('data-target' => 'self', 'data-toggle' => 'confirm')
        ));
    }

}
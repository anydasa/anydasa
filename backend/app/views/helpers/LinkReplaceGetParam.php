<?

class Site_View_Helper_linkReplaceGetParam
{

    public function linkReplaceGetParam($name, $value = null)
    {
    	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
    	$uri->addReplaceQueryParameters(array($name=>$value));
    	
    	return $uri->getUri();
    }

}
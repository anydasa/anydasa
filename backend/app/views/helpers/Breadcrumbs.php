<?

class Site_View_Helper_Breadcrumbs
{

    public function Breadcrumbs()
    {
        $currentPage = Zend_Registry::getInstance()->myCurrentRoute->AdminPages->getFirst();

        if ( empty($currentPage) ) {
            return '';
        }

        $view    = Zend_Layout::getMvcInstance()->getView();
        $parents = $currentPage->getNode()->getAncestors();

        if ( empty($parents) ) {
            $parents = new Doctrine_Collection('AdminPages');
        }

        $parents->add(Zend_Registry::getInstance()->myCurrentRoute->AdminPages->getFirst());

        $str = '<ul class="breadcrumb">';
        foreach ($parents as $item) {
            if ( $item->route_id ) {
                $str .= '<li><a href="'.$view->url(Site_Db_Function::pg_array_parse($item->params), $item->AdminRoutes->name).'">' . $item->title . '</a></li>';
            } else {
                $str .= '<li><a style="color: gray" href="#">' . $item->title . '</a></li>';
            }
        }
        $str .= '</ul>';

        return $str;
    }

}
<?

class Site_View_Helper_showPager
{

    public function showPager(Doctrine_Pager_Range $pagerRange)
    {
        $uri = Zend_Uri::factory(REQUEST_SCHEME . '://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
        $str = '';

        $pager = $pagerRange->getPager();

        if ($pager->getLastPage() > 1) {
            $str .= '<div class="page-nav"><ul id="nav-pages">';
            if (1 != $pager->getPage()) {
                $uri->addReplaceQueryParameters(array('page'=>null));
                //$str .= '<li><a class="btn btn-xs btn-default" href="'.$uri->getUri().'">« первая</a></li>';
                $uri->addReplaceQueryParameters(array('page'=> 1<$pager->getPreviousPage() ? $pager->getPreviousPage() : null ));
                $str .= '<li><a class="btn btn-default" title="На страницу назад (Alt + Shift + ←)" id="prev" href="'.$uri->getUri().'">сюда <i class="fa fa-long-arrow-left"></i></a></li>';
            }
            if (reset($pagerRange->rangeAroundPage()) != 1) {
                $str .= '<li>...</li>';
            }
            foreach ($pagerRange->rangeAroundPage() as $key => $page) {
                $str .= '<li>';
                if ($page == $pager->getPage()) {
                    $str .= '<em class="btn btn-xs">' . $page . '</em>';
                } elseif (1 == $page) {
                    $uri->addReplaceQueryParameters(array('page'=>null));
                    $str .= '<a class="btn btn-xs btn-default" href="'.$uri->getUri().'">' . $page . '</a>';
                } else {
                    $uri->addReplaceQueryParameters(array('page'=>$page));
                    $str .= '<a class="btn btn-xs btn-default" href="'.$uri->getUri().'">' . $page . '</a>';
                }
                $str .= '</li>';
            }
            if (end($pagerRange->rangeAroundPage()) != $pager->getLastPage()) {
                $str .= '<li>...</li>';
            }
            if ($pager->getPage() != $pager->getLastPage()) {
                $uri->addReplaceQueryParameters(array('page'=>$pager->getNextPage()));
                $str .= '<li><a class="btn btn-default" title="На страницу вперед (Alt + Shift + →)" id="next" href="'.$uri->getUri().'">туда <i class="fa fa-long-arrow-right"></i></a></li>';
                $uri->addReplaceQueryParameters(array('page'=>$pager->getLastPage()));
                //$str .= '<li><a class="btn btn-xs btn-default" href="'.$uri->getUri(). '"><i class="fa fa-fast-forward"></i></a></li>';
            }
            $str .= '</ul></div>';
        }

        if ( Zend_Controller_Front::getInstance()->getRequest()->isXmlHttpRequest() ) {
            $str = str_replace('href', 'data-target="self" data-modal-link', $str);
        }

        return $str;
    }

}
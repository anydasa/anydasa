<?

class Site_View_Helper_ProductLinksPrices extends Zend_View_Helper_Abstract
{

    public function ProductLinksPrices(Products $Product)
    {
        if ( $Product->LinksPrices->count() ) {
            $view = Zend_Layout::getMvcInstance()->getView();

            $str = '';
            $position = ProductsLinksPrices::getOurPosition($Product->id);

            if ( $position > 1 ) {
                $First = $Product->LinksPrices->getFirst();
                $str .= '<span title="'.$view->time($First->Link->prices_updated_at).'" data-toggle="tooltip" style="color: Red">'.$First->company_name.' №1 - '.Currency::format($First->price_uah, 'UAH').'</span><br>';
            }

            if ( false !== $position ) {
                $Our = $Product->LinksPrices->get($position-1);

                if ( $Product->getPrice('retail', 'UAH') != Currency::round($Our->price_uah, 'UAH') ) {
                    $str .= '<strong style="color: Orange;" title="Цена на сайте ' . $Product->getFormatedPrice('retail', 'UAH', true).'" data-toggle="tooltip" >Нужно залить прайс</strong><br>';
                }


                $str .= '<strong title="'.$view->time($Our->Link->prices_updated_at).'" data-toggle="tooltip" >Мы №'.$position.' - '.Currency::format($Our->price_uah, 'UAH').'</strong><br>';

                if ( $Product->LinksPrices->count() > $position ) {
                    $Next = $Product->LinksPrices->get($position);
//                    $deviation = round(($our_price - $price_min) / $price_min * 100, 2).'%';
                    $str .= '<span title="'.$view->time($Next->Link->prices_updated_at).'" data-toggle="tooltip" style="color: Green">'.$Next->company_name.' №'.($position+1).' - '.Currency::format($Next->price_uah, 'UAH').'</span>';
                }
            } else {
                $str .= '<h3>Нас нет</h3>';
            }
            return $str;
        }

    }


}
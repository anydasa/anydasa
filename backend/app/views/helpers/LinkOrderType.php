<?

class Site_View_Helper_linkOrderType
{

    public function linkOrderType($name)
    {
    	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
    	
        list($field, $type) = explode('-', $uri->getQueryAsArray()['sort']);
        
        if ( !empty($type) && $field == $name) {
           return $type;
        } else {
            return ' ';
        }
        
    }

}
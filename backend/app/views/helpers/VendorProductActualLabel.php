<?

class Site_View_Helper_VendorProductActualLabel
{

    public function VendorProductActualLabel(VendorsProducts $VendorProduct)
    {
        if ( $VendorProduct->isActual() ) {
            return '<i style="color: Green; cursor: default;" class="fa fa-lg fa-check" data-toggle="tooltip" title="Актуален."></i>';
        } else {
            return '<i style="color: Red; cursor: default;" class="fa fa-lg fa-exclamation" data-toggle="tooltip" title="НЕ АКТУАЛЕН. <br><br>'. htmlentities($VendorProduct->getCauseOfIrrelevanceMessage()) .'"></i>';
        }
    }
}
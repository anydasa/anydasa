<?php

class Service_ProductImage
{
    public static function saveRemoteDescriptionImagesLocally(Products $product)
    {
        preg_match_all('/<img(.*?)src=("|\'|)(.*?)("|\'| )(.*?)>/s', $product->description, $images);
        foreach ($images[3] as $imageUrl) {
            if (self::isRemoteImage($imageUrl)) {
                $newImageName = self::makeImageFilename($product, $imageUrl);
                $newImageUrl = self::getDescriptionImageUrlDir($product) . $newImageName;
                $newImagePath = self::getDescriptionImagePathDir($product) . $newImageName;

                if (!is_dir(dirname($newImagePath))) {
                    mkdir(dirname($newImagePath), 0755, true);
                }
                $product->description = str_replace($imageUrl, $newImageUrl, $product->description);
                copy($imageUrl, $newImagePath);
            }
        }

        if ($product->isModified()) {
            $product->save();
        }
    }

    public static function getDescriptionImagePathDir(Products $product)
    {
        return Zend_Registry::get('config')->path->img_product_description . '/' . $product->id . '/';
    }

    public static function getDescriptionImageUrlDir(Products $product)
    {
        return Zend_Registry::get('config')->url->img->products_description . '/' . $product->id . '/';
    }

    private static function isRemoteImage($imageUrl)
    {
        $baseUrlProductDescription = Zend_Registry::get('config')->url->img->products_description;

        $localHost = parse_url($baseUrlProductDescription, PHP_URL_HOST);
        $imgHost = parse_url($imageUrl, PHP_URL_HOST);

        return $localHost != $imgHost;
    }

    private static function makeImageFilename(Products $product, $remoteUrl): string
    {
        $imgExt = pathinfo(strtok($remoteUrl, '?'), PATHINFO_EXTENSION);
        return $product->alias . '_' . rand(100000000, 999999999) . '.' . $imgExt;
    }
}

<?php

class Service_SeriesImage
{
    public static function saveRemoteDescriptionImagesLocally(Series $series)
    {
        preg_match_all('/<img(.*?)src=("|\'|)(.*?)("|\'| )(.*?)>/s', $series->description, $images);
        foreach ($images[3] as $imageUrl) {
            if (self::isRemoteImage($imageUrl)) {
                $newImageName = self::makeImageFilename($series, $imageUrl);
                $newImageUrl = self::getDescriptionImageUrlDir($series) . $newImageName;
                $newImagePath = self::getDescriptionImagePathDir($series) . $newImageName;

                if (!is_dir(dirname($newImagePath))) {
                    mkdir(dirname($newImagePath), 0755, true);
                }
                $series->description = str_replace($imageUrl, $newImageUrl, $series->description);
                copy($imageUrl, $newImagePath);
            }
        }

        if ($series->isModified()) {
            $series->save();
        }
    }

    public static function getDescriptionImagePathDir(Series $series)
    {
        return Zend_Registry::get('config')->path->img_series_description . '/' . $series->id . '/';
    }

    public static function getDescriptionImageUrlDir(Series $series)
    {
        return Zend_Registry::get('config')->url->img->series_description . '/' . $series->id . '/';
    }

    private static function isRemoteImage($imageUrl)
    {
        $baseUrlSeriesDescription = Zend_Registry::get('config')->url->img->series_description;

        $localHost = parse_url($baseUrlSeriesDescription, PHP_URL_HOST);
        $imgHost = parse_url($imageUrl, PHP_URL_HOST);

        return $localHost != $imgHost;
    }

    private static function makeImageFilename(Series $series, $remoteUrl): string
    {
        $imgExt = pathinfo(strtok($remoteUrl, '?'), PATHINFO_EXTENSION);
        return rand(100000000, 999999999) . '.' . $imgExt;
    }
}

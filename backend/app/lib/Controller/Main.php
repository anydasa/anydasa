<?

abstract class Controller_Main extends Zend_Controller_Action
{

    public function init()
    {

        $this->sess = Zend_Auth::getInstance()->getIdentity();

        $this->view->params = $this->params = $this->getAllParams();
        $this->view->pix = Zend_Registry::getInstance()->config->url->pix;
        Zend_Registry::getInstance()->spelling = $this->_getSpelling();

        $this->view->headTitle(Zend_Registry::getInstance()->config->siteName);
        $this->view->headTitle('-админ');
        $this->setUpCurrency();

        $myCurrentRoute = Doctrine::getTable('AdminRoutes')->findOneByName(Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName());
        Zend_Registry::set('myCurrentRoute', $myCurrentRoute);

        if ($myCurrentRoute) {
            $this->view->headTitle(' / ' . $myCurrentRoute->title);
        }

        if ( $this->getRequest()->isXmlHttpRequest() ) {
            $this->_helper->layout->disableLayout();
        }

    }

    /**
     * @param string $modelName
     * @param string $val
     * @param string $method
     * @return Doctrine_Record
     * @throws Exception
     */
    public function findRecordOrException($modelName, $val, $method = 'find')
    {
        if ( !$Record = Doctrine::getTable($modelName)->{$method}($val) ) {
            throw new Exception("Не найдена запись $val в $modelName");
        }
        return $Record;
    }

    public function redirect($url, $options = array())
    {
        if ( $this->_request->isXmlHttpRequest() ) {
            echo 'Location:'.$url;
            exit;
        }
        parent::redirect($url, $options);
    }

    private function _getSpelling()
    {
        $key_cache = 'spelling_list_key_value';
        if ( !$return = Site_Cache::load($key_cache) ) {
            $return = Doctrine::getTable('Spelling')->findAll()->toKeyValueArray('key', 'value');
            Site_Cache::save($return, $key_cache);
        }

        return $return;
    }

    private function setUpCurrency()
    {
        $Currency = Doctrine_Query::create()->from('Currency')->where('is_visible = 1')->orderBy('sort')->execute();
        if ( empty($_SESSION['currency']) ) {
            $_SESSION['currency'] = Currency::getByISO('UAH')->iso;
        }
        foreach ($Currency as $Curr) {
            if ( $Curr->iso == $_SESSION['currency'] ) {
                Zend_Registry::getInstance()->currency_current = $Curr;
            }
        }
        Zend_Registry::getInstance()->currency_list    = $Currency;
    }

}
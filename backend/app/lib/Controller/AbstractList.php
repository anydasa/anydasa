<?

abstract class Controller_AbstractList extends Controller_Account
{

    public function preDispatch()
    {
        parent::preDispatch();

        $this->view->routePrefix = static::ROUTE_PREFIX;
        $this->view->modelName   = static::MODEL_NAME;

        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->_request->isXmlHttpRequest() ) {
            $this->_helper->layout->disableLayout();
        }

    }

    public function listAction($Query = null)
    {
        if ( is_null($Query) ) {
            $Query = $this->getQuery($this->_request->getQuery());
        }

        if ( $this->_request->isXmlHttpRequest() && strstr($this->_request->getHeader('Accept'), 'application/json')) {
            $__field = $this->_request->getQuery()['__field'];
            $__field_escape = str_replace('|', '.', $__field);
            if ( !empty($__field) ) {
                $Query->select("DISTINCT ($__field_escape) as $__field_escape");
                $Query->removeDqlQueryPart('orderby');
            }

            $result = $Query->limit(20)->execute()->toArray();

            foreach ($result as & $item) {
                $item[$__field] = $item[$__field_escape];
                unset($item[$__field_escape]);
            }

            return $this->_helper->json($result);
        }

        $this->_helper->viewRenderer->setNoRender(false);

        $pager = new Doctrine_Pager($Query, $this->params['page'], static::COUNT_ON_PAGE);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function editAttrAction()
    {
        $response = array(
            'status'=>0,
            'error'=>0
        );

        if ( $node = $this->_getNode($this->params[Zend_Controller_Front::getInstance()->getRouter()->getCurrentRoute()->getVariables()[0]]) ) {
            $post = $this->_request->getPost();
            if ( Doctrine::getTable(static::MODEL_NAME)->hasColumn($post['name']) ) {
                $node->{$post['name']} = $post['value'];
                $node->save();
                $response['status'] = 1;
                $response['value'] = $node->{$post['name']};
            } else {
                $response['error'] = 'Нет такого атрибута "'.$this->params['name'].'"';
            }
        } else {
            $response['error'] = 'Запись не найдена';
        }

        echo Zend_Json::encode($response);
    }

    /*
     * return Doctrine_Query
     */
    protected function getQuery($params, $modelName = null)
    {
        if ( null === $modelName ) {
            $modelName = static::MODEL_NAME;
        }
        //проверка параметров
        //Если параметр является колонкой в таблице то применяем фильтрацию
        $Query = Doctrine_Query::create()->from($modelName);

        $Query = $this->setOrderQuery($Query, $params, $modelName);
        $i = 1;
        foreach ($params as $key=>$val) {
            if ('' === trim($val)) continue;

            $key = explode('|', $key);

            if ( 2 == count($key) ) {
                $_alias = '_alias_' . $i++;
                $Query->innerJoin($modelName.'.'.$key[0] . ' ' . $_alias);
                $Query = $this->addParamToQuery($Query, $key[0], $_alias, $key[1], $val);
            } else {
                $Query = $this->addParamToQuery($Query, $modelName, $modelName, $key[0], $val);
            }
        }



        // ------- подружить запрос с сортировкой по функции
        $Query->buildSqlQuery(false);
        $map = $Query->getRootDeclaration();
        $table = $map['table'];
        $table->setAttribute(Doctrine_Core::ATTR_QUERY_LIMIT, Doctrine_Core::LIMIT_ROWS);
        //--------

        return $Query;
    }

    /*
     * return Doctrine_Query
     */
    private function setOrderQuery(Doctrine_Query $Query, $params, $model)
    {
        if ( !empty($params['sort']) ) {
            $column = $params['sort'];

            if ( strstr($column, '-') ) {
                list($column, $type) = explode('-', $column);
            }

            if ( strstr($column, '|') ) {
                list($model2, $column) = explode('|', $column);
                $Query->innerJoin($model.'.'.$model2);
                $Query->orderBy($model.'.'.$model2.'.'.$column.' '.$type);
            } else {
                $Query->orderBy($model.'.'.$column.' '.$type);
            }

        }

        $sort = Doctrine::getTable($model)->getIdentifier();

        if (is_array($sort)) {
            $sort = implode(',', $sort);
        }

        $Query->addOrderBy($sort . ' DESC');
        return $Query;
    }

    protected function addParamToQuery(Doctrine_Query $Query, $modelName, $tableAlias, $key, $val)
    {

        if ( 'NULL' == $val ) {
            $Query->addWhere($tableAlias.'.'.$key.' IS NULL');
        } elseif ( 'NOTNULL' == $val ) {
            $Query->addWhere($tableAlias.'.'.$key.' IS NOT NULL');
        } elseif ( 'EXISTS' == $val && class_exists($key) && ((new $key) instanceof Doctrine_Record) ) {
            $lk = Doctrine::getTable($modelName)->getRelation($key)->getLocal();
            $fk = Doctrine::getTable($modelName)->getRelation($key)->getForeign();
            $SubQuery = $Query->createSubquery()->from($key)->where("$modelName.$lk = $key.$fk");
            $Query->addWhere('EXISTS (' . $SubQuery->getDql() . ')');
        } elseif ( 'NOTEXISTS' == $val && class_exists($key) && ((new $key) instanceof Doctrine_Record) ) {
            $lk = Doctrine::getTable($modelName)->getRelation($key)->getLocal();
            $fk = Doctrine::getTable($modelName)->getRelation($key)->getForeign();
            $SubQuery = $Query->createSubquery()->from($key)->where("$modelName.$lk = $key.$fk");
            $Query->addWhere('NOT EXISTS (' . $SubQuery->getDql() . ')');
        } elseif ( '>' == substr($val,0,1) ) {
            $Query->addWhere($tableAlias.'.'.$key.' > ?', substr($val,1));
        } elseif ( '<' == substr($val,0,1) ) {
            $Query->addWhere($tableAlias.'.'.$key.' < ?', substr($val,1));
        } elseif ( '~' == substr($key,0,1) ) {
            $Query->addWhere($tableAlias.'.'.substr($key,1).' ~ ?', $val);
        } elseif ( 'fts-or' == $key ) {
            $val = str_replace(array("\n", "\r", "\t"), '', addcslashes((new Doctrine_Formatter)->quote($val), '.'));
            $Query->select("*, ts_headline(title, plainto_or_tsquery('russian', translate(" .       $val . ",'ё','е'))) as title");
            $Query->addWhere($tableAlias.".fts @@@ plainto_or_tsquery('russian', translate(" .      $val . ",'ё','е'))");
            $Query->orderBy('ts_rank('.$tableAlias.".fts, plainto_or_tsquery('russian', translate(".$val . ",'ё','е')), 32) DESC");
        } elseif ( 'fts' == $key ) {
            $val = str_replace(array("\n", "\r", "\t"), '', addcslashes((new Doctrine_Formatter)->quote($val), '.'));
            $Query->select("*, ts_headline(title, plainto_tsquery('russian', translate(" .          $val . ",'ё','е'))) as title");
            $Query->addWhere($tableAlias.".fts @@@ plainto_tsquery('russian', translate(" .         $val . ",'ё','е'))");
            $Query->addOrderBy('ts_rank('.$tableAlias.".fts, plainto_tsquery('russian', translate(".$val . ",'ё','е')), 32) DESC");
        } elseif ( preg_match('/^(.+)-range$/', $key, $res) ) {
            $field = $res[1];
            list($from, $to) = explode('~', $val);

            if ( 'timestamp' == Doctrine::getTable($modelName)->getTypeOfColumn($field) ) {
                $Query->addWhere('DATE('.$tableAlias.'.'.$field.') >= ?', $from);
                $Query->addWhere('DATE('.$tableAlias.'.'.$field.') <= ?', $to);
            } else {

            }

        } elseif ( preg_match('/^(.+)-array/', $key, $res) ) {
            $val = preg_split("/,|\n/is", $val);
            $val = array_map('trim', $val);
            $val = array_filter($val);
            $Query->whereIn($tableAlias.'.'.$res[1], $val);
        } elseif ( preg_match('/fts-(.+)/', $key, $res) ) {
            $val = str_replace(array("\n", "\r", "\t"), '', addcslashes((new Doctrine_Formatter)->quote($val), '.'));
            $field = $res[1];
            $Query->select("*, ts_headline($field, plainto_tsquery('russian', translate(" .                    $val . ",'ё','е'))) as $field");
            $Query->addWhere("to_tsvector('russian', $field) @@ plainto_tsquery('russian', translate(" .       $val . ",'ё','е'))");
            $Query->addOrderBy("ts_rank(to_tsvector('russian', $field), plainto_tsquery('russian', translate(".$val . ",'ё','е')), 32) DESC");
        } elseif ( preg_match('/ftsor-(.+)/', $key, $res) ) {
            $val = str_replace(array("\n", "\r", "\t"), '', addcslashes((new Doctrine_Formatter)->quote($val), '.'));
            $field = $res[1];
            $Query->select("*, ts_headline($field, plainto_or_tsquery('russian', translate(" .                      $val . ",'ё','е'))) as $field");
            $Query->addWhere("to_tsvector('russian', $field) @@@ plainto_or_tsquery('russian', translate(" .        $val . ",'ё','е'))");
            $Query->addOrderBy("ts_rank(to_tsvector('russian', $field), plainto_or_tsquery('russian', translate(".  $val . ",'ё','е')), 32) DESC");
        } elseif ( 'string' == Doctrine::getTable($modelName)->getTypeOfColumn($key) && !empty($val) ) {
            $Query->addWhere($tableAlias.'.'.$key.' ILIKE ?', '%'.$val.'%');
        } elseif ( 'integer' == Doctrine::getTable($modelName)->getTypeOfColumn($key) && is_numeric($val) ) {
            $Query->addWhere($tableAlias.'.'.$key.' = ?', $val);
        } elseif ( 'decimal' == Doctrine::getTable($modelName)->getTypeOfColumn($key) && is_numeric($val) ) {
            $Query->addWhere($tableAlias.'.'.$key.' = ?', $val);
        } elseif ( 'boolean' == Doctrine::getTable($modelName)->getTypeOfColumn($key) ) {
            $Query->addWhere($tableAlias.'.'.$key.' = ?', $val);
        }

        return $Query;
    }

    public function editAction()
    {
        $node = $this->_getNode($this->params['id']);
        $form = $this->getForm();

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form);
                $node->save();
                $this->_postEdit($node, $form);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        echo $form;
    }

    public function addAction()
    {
        $modelName = static::MODEL_NAME;
        $form = $this->getForm();

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $node = new $modelName;
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form, true);
                $node->save();
                $this->_postSave($node, $form, true);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate($this->_request->getQuery());
        }

        echo $form;
    }

    public function copyAction()
    {
        $copy = $this->_getNode($this->params['id'])->toArray();

        $form = $this->getForm();
        $form->setAction($this->view->url(array(), static::ROUTE_PREFIX.'add'));

        unset($copy['id']);
        $form->populate($copy);

        echo $form;
    }

    /**
     * @return Form_Abstract
     *
     * @throws Zend_Exception
     */
    protected function getForm()
    {
        $formName = static::FORM_NAME;
        $form = new $formName(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        $form->addTechFields( $this->getAllParams() );

        return $form;
    }

    public function deleteAction()
    {
        if ($Node = $this->_getNode($this->params['id'])) {
            $this->_preDelete($Node);
            $Node->delete();
            $this->_redirectToList();
        }
    }

    /**
     * @return Doctrine_Record
     */
    protected function _getNode($id = null)
    {
        if ($node = Doctrine::getTable(static::MODEL_NAME)->find($id)) {
            return $node;
        }
        $this->_redirectToList();
    }

    protected function _redirectToList()
    {
        if ( $this->_request->getQuery()['redirect'] ) {
            $redirectUri = urldecode($this->_request->getQuery()['redirect']);
        } elseif ( !empty($_SESSION['REDIRECT_URI']) ) {
            $redirectUri = $_SESSION['REDIRECT_URI'];
        } else {
            $redirectUri = $this->view->url(array(), Zend_Registry::get('myCurrentRoute')->name);
        }

        $this->redirect($redirectUri);
    }

    protected function _postSave(Doctrine_Record $node, Zend_Form $form)
    {
        if ( !empty($form->getValue('afterSaveCallbackWithJsonResult')) ) {
            echo 'Eval: ' . $form->getValue('afterSaveCallbackWithJsonResult') . '(' . Zend_Json_Encoder::encode($node->toArray()) . ')';
            exit;
        }
    }

    protected function _preDelete(Doctrine_Record $node)
    {

    }

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {

    }

    protected function _postEdit(Doctrine_Record $node, Zend_Form $form)
    {

    }

    public function goBack()
    {
        $redirect = urldecode($this->_request->getQuery()['redirect']);
        if ( empty($redirect) ) {
            $redirect = $_SERVER['HTTP_REFERER'];
        }
        $this->redirect($redirect);
    }

    public function selectedListAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->view->favoriteRocordList = Doctrine_Query::create()->from(static::MODEL_NAME)->whereIn('id', explode(',', $_COOKIE['CollectionRowToggle_'.static::MODEL_NAME]))->execute();
        $_COOKIE['CollectionRowToggle_'.static::MODEL_NAME] = implode(',', array_values($this->view->favoriteRocordList->toKeyValueArray('id', 'id')));
    }

}

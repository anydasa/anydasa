<?

abstract class Controller_Account extends Controller_Main
{

    public function preDispatch()
    {
        parent::preDispatch();

        if (!$this->sess->user) {
            $this->forward('login', 'auth', 'default');
            return;
        }

        $this->user = $this->view->user = Doctrine::getTable('AdminUsers')->find($this->sess->user->id);

        if ( empty($this->user) ) {
            $this->redirect($this->view->url(array(), 'logout'));
        }

        $this->user->assignAllowedRoutes();

        Zend_Registry::set('User', $this->user);
        Zend_Registry::set('CurrentAdmin', $this->user);

        if (!$this->user->isAllowedRoute(Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName())) {
            throw new Site_Exception_AccessDenied;
        }

        $this->view->menu = Doctrine_Core::getTable('AdminPages')->getTree()->fetchTree();

        if ( Zend_Registry::get('myCurrentRoute')->is_registered_in_log ) {
            $Log = new AdminLogs;
            $Log->id_user  = $this->user->id;
            $Log->id_route = Doctrine::getTable('AdminRoutes')->findOneByName(Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName())->id;
            $Log->method   = $this->_request->getMethod();
            $Log->url   = $_SERVER['REQUEST_URI'];
            if ( 'POST' == $Log->method ) {
                $Log->params = var_export($this->_request->getPost(), true);
            }
            $Log->save();
        }

    }
}


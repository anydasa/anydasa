<?

class Form_Currency extends Form_Abstract
{
    public function init()
    {
        $this->setAttrib('style', 'min-width: 600px;');
        $this->addIdElement();
        $this->addTitleElement();
        $this->addSymbolElement();
        $this->addRateElement();
        $this->addIsoElement();
        $this->addFormatElement();
        $this->addDecimalElement();
        $this->addThousandElement();
        $this->addPrecisionElement();
        $this->addIsVisibleElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addSymbolElement()
    {
        $element = new Zend_Form_Element_Text('symbol');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addRateElement()
    {
        $element = new Zend_Form_Element_Text('rate');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Float', true, array('locale' => 'en_US', 'messages' => array('notFloat'=>  'Только число')));
        $this->addElement($element);
    }

    public function addDecimalElement()
    {
        $element = new Zend_Form_Element_Text('decimal');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addThousandElement()
    {
        $element = new Zend_Form_Element_Text('thousand');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addPrecisionElement()
    {
        $element = new Zend_Form_Element_Text('precision');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Int', true, array('messages' => array('notInt' => 'Только числовое значение')));
        $this->addElement($element);
    }

    public function addIsoElement()
    {
        $element = new Zend_Form_Element_Text('iso');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addValidator('Regex', false, array(
                    'pattern' => '/[a-z]{3}/i',
                    'messages' => '"'.$element->getLabel().'" должно содержать 3 латинские буквы'
                )
        );

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $element->addFilter(new Zend_Filter_StringToUpper());

        $this->addElement($element);
    }

    public function addFormatElement()
    {
        $element = new Zend_Form_Element_Textarea('format');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttrib('style', 'height: 50px;');
        $this->addElement($element);
    }

    public function addIsVisibleElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_visible');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

}
<?

class Form_User_Diff extends Form_Abstract
{
    public function init()
    {
        $this->addFileElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addFileElement()
    {
        $element = new Zend_Form_Element_File('file');
        $element->setLabel('Из файла:')
                ->setDescription('Допустимы Excel файлы.');

        $sizeValidator = new Zend_Validate_File_Size(array('max'=>(1024*1024)));
        $sizeValidator->setMessage('Максимальный размер файла %max%');
        $element->addValidator($sizeValidator);

        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->save->setLabel('Загрузить');
    }

    protected function _setDecorsTable()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'standart-form')),
            array('Description', array('tag' => 'h2', 'placement' => 'prepend')),
            'Form',
        ));

        $this->setElementDecorators(array(
            'ViewHelper',
            array('Description', array('class' => 'descr-element', 'escape' => false)),
            array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
            array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
            array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span class="tooltip" title="Обязательно для заполнения">*</span>')),
            array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
        ));

        $this->getElement('file')->setDecorators(
            array(
                'File',
                array('Description', array('class' => 'descr-element', 'escape' => false)),
                array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
            )
        );

        $this->getElement('save')->setDecorators(array(
            array(
                'decorator' => 'ViewHelper',
                'options' => array('helper' => 'formSubmit')),
            array(
                'decorator' => array('td' => 'HtmlTag'),
                'options' => array('tag' => 'td', 'colspan' => 2)),
            array(
                'decorator' => array('tr' => 'HtmlTag'),
                'options' => array('tag' => 'tr')),
        ));
    }
}
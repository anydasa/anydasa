<?

class Form_User_Email extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addEmailElement();
        $this->addNameElement();
        $this->addCommentElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
            ->addValidator('EmailBool', true, array(
                'messages' => array(
                    'emailNotValid'=> "{$element->getLabel()} указан не корректно"
                )));

        $this->addElement($element);
    }

    public function addCommentElement()
    {
        $element = new Zend_Form_Element_Text('comment');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
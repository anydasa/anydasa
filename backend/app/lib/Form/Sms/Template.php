<?

class Form_Sms_Template extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            .image-preview {position: relative; display: inline-block;}
            .image-preview img {max-width: 200px; max-height: 200px;}
            form#'.$this->getId().' textarea {
                height: 100px;
                width: 300px;
            }
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addCodeElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }

    public function addCodeElement()
    {
        $element = new Zend_Form_Element_Text('code');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }



}
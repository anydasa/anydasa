<?

class Form_Log_ReportTasks extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {width: 900px;}
            #'.$this->getId().' #emails,
            #'.$this->getId().' #phones {height: 100px;}
            #'.$this->getId().' .group-element {float: left; width: 150px; margin: 0 3px;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addCodeElement();
        $this->addExecIntervalElement();
        $this->addEmailsElement();
        $this->addPhonesElement();
        $this->addSubmitElement();
    }

    public function addCodeElement()
    {
        $element = new Zend_Form_Element_Text('code');
        $element->setAttrib('placeholder', Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addEmailsElement()
    {
        $element = new Zend_Form_Element_Textarea('emails');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addPhonesElement()
    {
        $element = new Zend_Form_Element_Textarea('phones');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addExecIntervalElement()
    {
        $element = new Zend_Form_Element_Select('exec_interval');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array(''=>'---'));
        $element->addMultiOptions(LogReportTasks::$EXEC_INTERVAL);
        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        $values['emails'] = implode("\n", (array)$values['emails']);
        $values['phones'] = implode("\n", (array)$values['phones']);

        return parent::populate($values);
    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);

        $values['emails'] = array_filter(array_map(function ($item) { return trim($item); }, explode("\n", $values['emails'])));
        $values['phones'] = array_filter(array_map(function ($item) { return trim($item); }, explode("\n", $values['phones'])));

        return $values;
    }
}
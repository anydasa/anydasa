<?

class Form_Mail_Template extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("body_html", { customConfig: "/js/ckeditor_confings/MailTemplate.js"});
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addCodeElement();
        $this->addSubjectElement();
        $this->addFromElement();
        $this->addIdSmtpElement();
        $this->addBodyElement();
        $this->addSubmitElement();
    }

    public function addCodeElement()
    {
        $element = new Zend_Form_Element_Text('code');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }
    public function addSubjectElement()
    {
        $element = new Zend_Form_Element_Text('subject');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
    public function addFromElement()
    {
        $element = new Zend_Form_Element_Text('from_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
    public function addIdSmtpElement()
    {
        $element = new Zend_Form_Element_Select('id_smtp');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $values = Doctrine_Query::create()->from('MailSmtp')->setHydrationMode(Doctrine::HYDRATE_ARRAY)->execute();
        $element->addMultiOption('', '--Выберите Email--');
        foreach ($values as $item) {
        	$element->addMultiOption($item['id'], $item['username']);
        }
        $element->addValidator('InArray', true, array(array_keys($element->getMultiOptions()), 'messages' => array('notInArray' => 'Не допустимое значение')));
        $this->addElement($element);
    }
    public function addBodyElement()
    {
        $element = new Zend_Form_Element_Textarea('body_html');
        $element->setAttribs(array('width'=> 800, 'height' => 300));
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
/*
        $element = new Zend_Form_Element_Textarea('body_text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 800px; height: 200px"));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);*/
    }



}
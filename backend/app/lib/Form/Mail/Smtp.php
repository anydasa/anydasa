<?

class Form_Mail_Smtp extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addHostElement();
        $this->addAuthElement();
        $this->addUsernameElement();
        $this->addPasswordElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addHostElement()
    {
        $element = new Zend_Form_Element_Text('host');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Hostname', true, array('messages' => 'Hе верно (пример: smtp.yandex.ru)'));
        $this->addElement($element);
    }

    public function addAuthElement()
    {
        $element = new Zend_Form_Element_Select('auth');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');

        $element->addMultiOptions(array(
            'login'     => 'login',
            'plain'     => 'plain',
            'cram-md5'  =>'cram-md5'
        ));

        $this->addElement($element);
    }

    public function addUsernameElement()
    {
        $element = new Zend_Form_Element_Text('username');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('EmailBool', true, array('messages' => array('emailNotValid'=>'Hе верно (пример: email@yandex.ru)')));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
                ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }

    public function addPasswordElement()
    {
        $element = new Zend_Form_Element_Text('password');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
}
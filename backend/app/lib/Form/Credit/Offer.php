<?

class Form_Credit_Offer extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptLink('/ckeditor/ckeditor.js');
        $this->addJavaScriptLink('/ckfinder/ckfinder.js');
        $this->addJavaScriptLink('/ckeditor/adapters/jquery.js');

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find("#description").ckeditor({customConfig: "../js/ckeditor_confings/basic.js"});
            CKFinder.setupCKEditor( $("form#'.$this->getId().'").find("#description").ckeditorGet(), "/ckfinder/" );
            $("form#'.$this->getId().' #description-label").next().attr("colspan", 2).prev().remove();
            $("form#'.$this->getId().'").find("#first_payment_is_first_month").change(function () { $("#fieldset-first_payment input[type=text]").toggle( !$(this).prop("checked") ); }).change()

        ');


        $this->setStyleText('
            #'.$this->getId().' { width: 800px;}
            #'.$this->getId().' .group-element {float: left; margin-right: 3px; position: relative;}

            #'.$this->getId().' .MultiCheckbox {resize: vertical; max-height: 100px; overflow-y: scroll; background: white; border: 1px solid Gray; width: 300px; padding: 5px;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0; padding: 1px 2px; line-height: 12px; font-weight: normal; font-size: 11px; float: left; width: 100%;}
            #'.$this->getId().' .MultiCheckbox label input {margin: 0; float: left;}

            #'.$this->getId().' #rate_on_initial_sum {float: left;}
            #'.$this->getId().' #rate_on_initial_sum+.hint {color: gray; font-size: 11px;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addCompanyElement();
        $this->addTitleElement();
        $this->addSumElement();
        $this->addFirstPaymentElement();
        $this->addPeriodElement();
        $this->addInterestRateElement();
        $this->addCommissionElement();
        $this->addProfileFieldsElement();
        $this->addIsEnabledElement();
        $this->addDescriptionElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addProfileFieldsElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('profile_fields');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setMultiOptions(Credit::$profile_fields);
        $element->setSeparator('');
        $this->addElement($element);
    }

    public function addCompanyElement()
    {
        $element = new Zend_Form_Element_Hidden('id_company');
        $this->addElement($element);
    }

    public function addSumElement()
    {
        $element = new Zend_Form_Element_Text('min_sum');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('max_sum');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

//        $this->addDisplayGroup(array('min_sum_percent', 'max_sum_percent'), 'sum', array('legend'=>'Сумма:'));

    }

    public function addPeriodElement()
    {
        $element = new Zend_Form_Element_Text('min_month');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Int', true, array('messages' => array('notInt' => 'Только числовое значение')));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('GreaterThan', true, array(
            'messages' => ['notGreaterThan' => "минимум 1мес."],
            'min' => 0,
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('max_month');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Int', true, array('messages' => array('notInt' => 'Только числовое значение')));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('GreaterThan', true, array(
            'messages' => ['notGreaterThan' => "минимум 1мес."],
            'min' => 1,
        ));
        $this->addElement($element);

//        $this->addDisplayGroup(array('min_month', 'max_month'), 'period', array('legend'=>'Период:'));
    }

    public function addFirstPaymentElement()
    {
        $element = new Zend_Form_Element_Text('min_first_payment_percent');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('max_first_payment_percent');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('first_payment_is_first_month');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);

        $this->addDisplayGroup(array('min_first_payment_percent', 'max_first_payment_percent', 'first_payment_is_first_month'), 'first_payment', array('legend' => 'Первоначальный взнос:'));

    }

    public function addInterestRateElement()
    {
        $element = new Zend_Form_Element_Text('yearly_interest_rate_percent');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('monthly_interest_rate_percent');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

//        $this->addDisplayGroup(array('yearly_interest_rate_percent', 'monthly_interest_rate_percent'), 'interest_rate', array('legend'=>'Процентная ставка:'));
    }

    public function addCommissionElement()
    {
        $element = new Zend_Form_Element_Text('onetime_commission');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('onetime_commission_type');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(['percent' => '% от суммы', 'fix' => 'Фиксировано']);
        $this->addElement($element);

        $this->addDisplayGroup(array('onetime_commission', 'onetime_commission_type'), '_onetime_commission', array('legend'=> Doctrine::getTable($this->modelName)->getDefinitionOf('onetime_commission')['comment'].':' ));

        $element = new Zend_Form_Element_Text('monthly_commission');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('monthly_commission_type');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(['percent_total' => '% от суммы кредита', 'percent_rest' => '% от остатка', 'fix' => 'Фиксировано']);
        $this->addElement($element);

        $this->addDisplayGroup(array('monthly_commission', 'monthly_commission_type'), '_monthly_commission', array('legend'=> Doctrine::getTable($this->modelName)->getDefinitionOf('monthly_commission')['comment'].':' ));

        $element = new Zend_Form_Element_Checkbox('rate_on_initial_sum');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setDescription('Расчет будет произведен на всю сумму, в независимости от того сколько будет внесено изначально.');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
<?

class Form_Series_Option extends Form_Abstract
{
    private Catalog $catalog;

    public function __construct($formName, Catalog $catalog, $options = null)
    {
        $this->catalog = $catalog;

        parent::__construct($formName, $options);
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        return parent::render($view);
    }

    public function init()
    {
        $this->addProductOptionElement();
        $this->addIdCatalogElement();
        $this->addDisplayTypeElement();
        $this->addSubmitElement();
    }

    public function addProductOptionElement()
    {
        $options = $this->catalog->ProductsOption->toKeyValueArray('id', 'title');

        $element = new Zend_Form_Element_Select('id_product_option');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(array('' => '---'));
        $element->addMultiOptions($options);
        $this->addElement($element);
    }

    public function addDisplayTypeElement()
    {
        $element = new Zend_Form_Element_Select('display_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(array('' => '---'));
        $element->addMultiOptions(SeriesOptions::DISPLAY_TYPES);
        $this->addElement($element);
    }

    public function addIdCatalogElement()
    {
        $element = new Zend_Form_Element_Hidden('id_catalog');
        $element->setValue($this->catalog->id);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
}

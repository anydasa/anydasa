<?

class Form_Series_Group extends Form_Abstract
{
    public function init()
    {
        $this->addTitleElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttrib('style', 'width: 400px;');
        $this->addElement($element);
    }

}
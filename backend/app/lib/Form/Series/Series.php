<?

class Form_Series_Series extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            $(".image-preview").append("<div id=\"removeImg\" class=\"fa fa-times\" />");
            $("#removeImg").click(function () {
                $("#image_delete").val(1)
                $(".image-preview").remove()
                $("#image").show()
            })

            if ( $(".image-preview").length ) {
                $("#image").hide()
            }
        ');

        $this->setStyleText('
            #'.$this->getId().' .image-preview {position: relative; display: inline-block;}
            #'.$this->getId().' #removeImg {position: absolute; top: 0; right: 0; cursor: pointer; background: white;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addTitleElement();
        $this->addGroupElement();
        $this->addImageElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttrib('style', 'width: 400px;');
        $this->addElement($element);
    }

    public function addGroupElement()
    {
        $element = new Zend_Form_Element_Select('id_group');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 400px;');
        $element->addMultiOptions(array('' => '---'));
        $element->addMultiOptions(Doctrine::getTable('SeriesGroups')->findAll()->toKeyValueArray('id', 'title'));
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setValueDisabled(true); // отключение авто receive

        $element->setLabel('Картинка:');

        $Validator = new Zend_Validate_File_IsImage(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        parent::populate($values);

        if ( isset($values['id']) ) {
            $Node = Doctrine::getTable($this->modelName)->find($values['id']);

            if ( $url = $Node->getImageUrl() ) {
                $this->getElement('image')->setDescription('<img style="max-height: 200px; max-width: 200px;" src="'.$url.'?'.rand(1, 1000000).'">');
            }
        }
    }

    protected function _setDecorsTable()
    {
        parent::_setDecorsTable();

        $this->getElement('image')->setDecorators(
            array(
                'File',
                array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                array('Description', array('class' => 'image-preview', 'escape' => false)),
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
            )
        );
    }

}
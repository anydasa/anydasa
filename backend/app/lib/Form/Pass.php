<?

class Form_Pass extends Form_Abstract
{
    public function init()
    {
        $this->addPasswordElement();
        $this->addPasswordConfirmElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addPasswordElement()
    {
        $element = new Zend_Form_Element_Password('password');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addFilter(new Zend_Filter_Callback('md5'));
        $this->addElement($element);
    }

    public function addPasswordConfirmElement()
    {
        $element = new Zend_Form_Element_Password('password_confirm');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf('password')['comment'].' (повторно):');

        $element->setRequired(true)
                ->addValidator('NotEmpty', true, array(
                    'messages' => array(
                        'isEmpty' => 'Обязательно для заполнения'
        )));

        $element->addValidator('Identical', true, array(
                    'password', 'messages' => array(
                        'notSame' => 'Неверно указан пароль повторно'
        )));

        $this->addElement($element);
    }
}
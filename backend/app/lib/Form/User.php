<?

class Form_User extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addNameElement();
        $this->addEmailElement();
        $this->addPhoneElement();
        $this->addCompanyElement();
        $this->addPriceElement();
        $this->addIsEnabledElement();
        $this->addDisabledReasonElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addPriceElement()
    {
        $element = new Zend_Form_Element_Select('price');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array(
            'price_trade'   => Doctrine::getTable('Products')->getDefinitionOf('price_trade')['comment'],
            'price_partner' => Doctrine::getTable('Products')->getDefinitionOf('price_partner')['comment'],
            'price_min'     => Doctrine::getTable('Products')->getDefinitionOf('price_min')['comment'],
        ));
        $this->addElement($element);
    }

    public function addDisabledReasonElement()
    {
        $element = new Zend_Form_Element_Text('disabled_reason');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        //$element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addPhoneElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('disabled', 'disabled');
        $this->addElement($element);
    }

    public function addCompanyElement()
    {
        $element = new Zend_Form_Element_Text('company_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
<?

class Form_Products_Image extends Form_Abstract
{
    public function init()
    {

        $this->addByLinkElement();
        $this->addImageElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->addJavaScriptText('
            setTimeout(function() {
                $("#byLink").focus();
            }, 0);
        ');
    }

    public function addByLinkElement()
    {
        $element = new Zend_Form_Element_Textarea('byLink');
        $element->setLabel('По ссылке:');
        $element->setAttrib('style', 'width: 300px; height: 100px;');
        $this->addElement($element);
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setAttrib('multiple', 'multiple');
        $element->setIsArray(true);
        $element->setLabel('Файл:');

        $Validator = new Zend_Validate_File_MimeType(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);


        $sizeValidator = new Site_Validate_File_ImageSize( array('minside'=>300) );
        $sizeValidator->setMessage("Минимальная допустимая сторона для рисунка '%minside%px' но у рисунка %value% ширина=%width%px и высота=%height%px");
        $element->addValidator($sizeValidator);


        $this->addElement($element);
    }

}
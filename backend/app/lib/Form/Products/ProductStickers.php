<?

class Form_Products_ProductStickers extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' .MultiCheckbox {resize: vertical; margin: 10px 0; max-height: 100px; overflow-y: scroll; background: white; border: 1px solid Gray; width: 300px;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0; padding: 2px 2px; line-height: 12px; font-weight: normal; font-size: 11px; float: left; width: 100%;}
            #'.$this->getId().' .MultiCheckbox label input {margin: 0; float: left;}
            #'.$this->getId().' select option {background: White;}
            #'.$this->getId().' select.selected {background: #009966;}
            #'.$this->getId().' .filterMultiCheckboxContainer {float: left;}
        ');

        $this->addJavaScriptText('
           $(function () {
                $("form#'.$this->getId().' .MultiCheckbox").filterMultiCheckbox();
           });
        ');

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( this.checked ) {
                    $(this).closest("label").css({background: "Green"});
                    $(this).parents("tr").find(">th").append(
                        $("<div />")
                            .attr("id", "l"+$(this).attr("id"))
                            .css({fontWeight:"normal", color: "Green", fontSize: "11px", lineHeight: "12px"})
                            .html($(this).parent().text())
                    )
                } else {
                    $("#l"+$(this).attr("id")).remove();
                    $(this).closest("label").css({background: "Transparent"});
                }
            }).change()

            $("form#'.$this->getId().' select").on("change", function() {
                if ( "" != $(this).val() ) {
                    $(this).addClass("selected");
                } else {
                    $(this).removeClass("selected");
                }
            }).change();
        ');


        return parent::render($view);
    }

    public function init()
    {

        $element = new Zend_Form_Element_MultiCheckbox('Stickers');
        $element->setLabel('Стикеры:');
        $element->addMultiOptions(Doctrine_Query::create()->from('ProductsStickers')->where('is_private = false')->execute()->toKeyValueArray('id', 'title'));

        $this->addElement($element);

        $this->addSubmitElement();

    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);

        $values['Stickers'] = (array) $values['Stickers'];

        return $values;
    }

}
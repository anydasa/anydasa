<?

class Form_Products_Product extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            function afterSaveBrand(data) {
                $("form#'.$this->getId().'").find("#id_brand").append("<option value="+data["id"]+" selected>"+ data["title"] +"</option>")
            }

            $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( this.checked ) {
                    $("#Catalogs-label").append(
                        $("<div />")
                            .attr("id", "l"+$(this).attr("id"))
                            .css({fontWeight:"normal", color: "gray", fontSize: "11px", lineHeight: "13px"})
                            .html($(this).parent().text())
                    )
                } else {
                    $("#l"+$(this).attr("id")).remove()
                }
            }).change()

            $("form#'.$this->getId().'").find("#format_title_button").on("click", function() {
                var $title_input = $("form#'.$this->getId().'").find("#title");
                $.get("/products/product/ajax-action/get-formatted-title/1", {title:$title_input.val()}, function (data) {
                    if ( data == $title_input.val() ) {
                        alert("Название уже отформатировано");
                        return;
                    }
                    if ( confirm($title_input.val()+"\n"+data+"") ) {
                        $title_input.val(data);
                    }
                }, "json");
            });


        ');

        $this->setStyleText('
            .group-element {float: left; width: 150px; margin: 0 3px;}
            #id_brand {float: left; width: 120px; margin-right: 5px;}
            #id_brand+p {float: left;}
            #id_brand option:first-letter {font-weight: bold;}
            #title-label+td {position: relative;}
            #title-label+td p {float: right; position: absolute; right: 10px; top:0;}

            #'.$this->getId().' .MultiCheckbox {max-height: 200px; overflow-y: scroll; background: white; padding: 5px; border: 1px solid Gray;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0; line-height: 0.1em; font-weight: normal; font-size: 11px;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addHtmlHeadersElement();
        $this->addTitleElement();
        $this->addSoldPhrasesHotlineElement();
        $this->addAliasElement();
        $this->addTypeElement();
        $this->addManufacturerCodeElement();
        $this->addArticulElement();
        $this->addBrandElement();
        $this->addStateElement();

//        $this->addStatusElement();
//        $this->addBalanceElement();
        $this->addIsVisibleElement();

        $this->addDisplayGroup(array('id_state', 'manufacturer_code', 'code', 'type', 'id_brand', 'create_brand', 'status', 'balance', 'is_visible'), 'group', array('legend' => '&nbsp;', 'escape'=>false));
        $this->addPriceElement();
//        $this->addMarkdownElement();

        $this->addGuaranteeElement();
        //$this->addDiscountElement();
        $this->addFeaturesElement();
        $this->addCatalogElement();
//        $this->addDescriptionElement();

        $this->addSubmitElement();
    }

    public function addHtmlHeadersElement()
    {
        $element = new Zend_Form_Element_Text('html_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('html_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addUrlElement()
    {
        $element = new Zend_Form_Element_Text('url');
        $element->setLabel('Урл:');
        $element->setBelongsTo('ProductsLinks[0]');
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Text('type');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addMarkdownElement()
    {
        $element = new Zend_Form_Element_Text('markdown_percent');
        $element->setLabel('Сколько, %');
        $element->addValidator('Int', true, array('messages' => array('notInt'=>'Не число')));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('markdown_note');
        $element->setAttrib('style', 'width: 900px;');
        $element->setLabel('Почему');
        $this->addElement($element);

        $this->addDisplayGroup(array('markdown_percent', 'markdown_note'), 'markdown', array('legend' => 'Уценка:'));
    }

    public function addDiscountElement()
    {
        $element = new Zend_Form_Element_Text('discount');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Int', true, array('messages' => array('notInt'=>'Не число')));
        $this->addElement($element);
    }
    public function addBalanceElement()
    {
        $element = new Zend_Form_Element_Text('balance');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addValidator('Int', true, array('messages' => array('notInt'=>'Не число')));
        $this->addElement($element);
    }
    public function addPriceElement()
    {
        $element = new Zend_Form_Element_Text('price_retail');
        $element->setLabel('РРЦ:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addValidator('Float', true, array('locale' => 'en_US', 'messages' => array('notFloat'=>'Не число')));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('id_currency');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array('' => '---'));
        $element->addMultiOptions(Doctrine::getTable('Currency')->findAll()->toKeyValueArray('id', 'iso'));
        $this->addElement($element);

        $this->addDisplayGroup(array('price_retail', 'id_currency'), 'price', array('legend' => 'Цены', 'class' => 'prices'));
    }
    public function addIsVisibleElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_visible');
        $element->setLabel('Показывать:');
        $this->addElement($element);
    }
    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->setDescription('<span id="format_title_button" class="btn btn-xs"><strong>F</strong></span>');
        $this->addElement($element);
    }
    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('width' => 800, 'height' => 200));
        $this->addElement($element);
    }

    public function addSoldPhrasesHotlineElement()
    {
        $element = new Zend_Form_Element_Textarea('sold_phrases_hotline');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('rows' => 4));
        $this->addElement($element);
    }

    public function addFeaturesElement()
    {
        $element = new Zend_Form_Element_Textarea('features');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('rows' => 5));
        $this->addElement($element);
    }
    public function addStatusElement()
    {
        $element = new Zend_Form_Element_Select('status');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addMultiOptions(array('' => '---'));
        $element->addMultiOptions(Products::getStatusList());
        $this->addElement($element);
    }

    public function addCatalogElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('Catalogs');
        $element->setLabel('Каталог:');
//        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->setMultiOptions(Doctrine_Query::create()->from('Catalog')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $element->setSeparator('');
        $this->addElement($element);
    }

    public function addBrandElement()
    {
        $element = new Zend_Form_Element_Select('id_brand');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setDescription(
            Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
                'routeName'   => 'products-brand-add',
                'buttonText'  => '<i class="fa fa-plus"></i>',
                'buttonAttr'  => array('class' => 'btn btn-xs btn-success'),
                'queryParams' => array('afterSaveCallbackWithJsonResult' => 'afterSaveBrand')
            )));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addMultiOptions(
            array('' => '---') +
            Doctrine_Query::create()->from('ProductsBrands')->orderBy('title')->execute()->toKeyValueArray('id', 'title')
        );
        $this->addElement($element);
    }

    public function addStateElement()
    {
        $element = new Zend_Form_Element_Select('id_state');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(
            //array('' => '---') +
            Doctrine_Query::create()->from('ProductsState')->orderBy('sort')->execute()->toKeyValueArray('id', 'title')
        );
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addGuaranteeElement()
    {
        $element = new Zend_Form_Element_Select('id_guarantee');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(
            array('' => '---') +
            Doctrine::getTable('ProductsGuarantee')->findAll()->toKeyValueArray('id', 'title')
        );
        $element->addFilter(new Zend_Filter_Null('string'));
        $this->addElement($element);
    }

    public function addArticulElement()
    {
        $element = new Zend_Form_Element_Text('code');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));
        $this->addElement($element);
    }

    public function addManufacturerCodeElement()
    {
        $element = new Zend_Form_Element_Text('manufacturer_code');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Site_Filter_Slugify);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }
}
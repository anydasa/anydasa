<?

class Form_Products_Brand extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $ckFinderUrl = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Brand&responseType=json';

        $this->addJavaScriptText("
            $(function () {
                CKEDITOR.replace( 'description', {
                    language: 'ru',
                    uiColor: '#AADC6E',
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserUploadUrl: '$ckFinderUrl',
                    filebrowserWindowWidth: '1000',
                    filebrowserWindowHeight: '700'
                });
            });
        ");


        $this->addJavaScriptText('
            $(".image-preview").append("<div id=\"removeImg\" class=\"fa fa-times\" />")
            $("#removeImg").click(function () {
                alert("Для сохранения изминений сохраните форму")
                $("#image_delete").val(1)
                $(".image-preview").remove()
                $("#image").show()
            })
            if ( $(".image-preview").length ) {
                $("#image").hide()
            }
        ');

        $this->setStyleText('
            .image-preview {position: relative; display: inline-block;}
            #removeImg {position: absolute; top: 0; right: 0; cursor: pointer;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addImageElement();
        $this->addAliasElement();
        $this->addTitleElement();
        $this->addIsVisibleElement();
        $this->addDescriptionElement();
        $this->addSubmitElement();
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');

        $element->setLabel('Картинка:');

        $Validator = new Zend_Validate_File_MimeType(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $sizeValidator = new Zend_Validate_File_Size(array('max'=>(1024*1024)));
        $sizeValidator->setMessage('Максимальный размер файла "Фотография" %max%');
        $element->addValidator($sizeValidator);

        $element->setDestination(Zend_Registry::get('config')->path->pix.'img/brands/');

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsVisibleElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_visible');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    protected function _setDecorsTable()
    {
        parent::_setDecorsTable();

        $this->getElement('image')->setDecorators(
            array(
                'File',
                array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                array('Description', array('class' => 'image-preview', 'escape' => false)),
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
            )
        );
    }

    public function populate($data)
    {
        parent::populate($data);

        if ( isset($data['id']) ) {
            $image_path = Zend_Registry::get('config')->path->pix.'img/brands/'.$data['id'].'.jpg';
            if ( Site_Image::isImage($image_path) ) {
                $this->getElement('image')->setDescription('<img src="'.Zend_Registry::get('config')->url->pix.'img/brands/'.$data['id'].'.jpg'.'">');
            }
        }

    }
}
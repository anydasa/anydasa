<?

class Form_Products_Option extends Form_Abstract
{

    public function init()
    {
        $this->addIdElement();
        $this->addCatalogElement();
        $this->addGroupElement();
        $this->addTitleElement();
        $this->addTypeElement();
        $this->addIsPrimeElement();
        $this->addIsRequireElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addCatalogElement()
    {
        $element = new Zend_Form_Element_Hidden('id_catalog');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Select('type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setMultiOptions([
            'text'      => 'Текст',
            'numeric'   => 'Числовой',
            'multiselect' => 'multiselect',
            'select' => 'select'
        ]);
        $element->setAttrib('style', 'width: 100px');
        $this->addElement($element);
    }

    public function addGroupElement()
    {
        $element = new Zend_Form_Element_Text('group_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null());
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('group_name_brief');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null());
        $element->addValidator('StringLength', false, array(
            0,
            8,
            'messages' => array(
                'stringLengthTooLong' =>  "Max %max% символов",
            )
        ));
        $this->addElement($element);
    }
    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttrib('style', 'width: 400px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('title_brief');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null());
        $element->addValidator('StringLength', false, array(
                0,
                8,
                'messages' => array(
                    'stringLengthTooLong' =>  "Max %max% символов",
                )
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('unit');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null());
        $element->addValidator('StringLength', false, array(
            0,
            8,
            'messages' => array(
                'stringLengthTooLong' =>  "Max %max% символов",
            )
        ));
        $this->addElement($element);
    }
    public function addIsPrimeElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_prime');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addIsRequireElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_require');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
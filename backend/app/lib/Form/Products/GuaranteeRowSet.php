<?

class Form_Products_GuaranteeRowSet extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$(element).closest("td").append(error);}'),
        ]);

        $this->setStyleText('
            #'.$this->getId().' { width: 600px;}
            #'.$this->getId().' td label { display: block; font-weight: normal; padding: 0; margin: 0;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addGuaranteeElement();
        $this->addSubmitElement();
    }

    public function addGuaranteeElement()
    {
        $element = new Zend_Form_Element_Select('id_guarantee');
        $element->setMultiOptions(['' => '---Гарантия---'] + Doctrine_Query::create()->from('ProductsGuarantee')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $element->setLabel('Гарантия:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }


}
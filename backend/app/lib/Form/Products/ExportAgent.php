<?

class Form_Products_ExportAgent extends Form_Products_Search
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        /*$this->addJavaScriptLink('/ckeditor/ckeditor.js');
        $this->addJavaScriptLink('/ckeditor/adapters/jquery.js');

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find("#'.$this->append_text->getId().'").ckeditor({customConfig: "../js/ckeditor_confings/Products_ExportAgent.js"})
        ');*/

        return parent::render($view);
    }

    public function init()
    {
        parent::init();

        $this->setMethod('POST');

        $order = 100;

        foreach ($this->getDisplayGroups() as $group) {
            $group->setOrder( $order++ );
        }

        foreach ($this->getElements() as $element) {
            $element->setOrder( $order++ );
            $element->setBelongsTo('params');
        }

        self::addTitleElement();
        self::addIntergalGenerateElement();
        self::addTemplateElement();
        self::addPriceTypeElement();
        self::addCurrencyElement();
        self::addSoldPhrasesElement();
        self::addPmStock();
        self::addPartsPayment();
        self::addIncreasePercent();
        self::addAppendTextElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel('Название агента:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addIntergalGenerateElement()
    {
        $element = new Zend_Form_Element_Select('interval_generate');
        $element->setLabel('Интервал генереции:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(array('' => '---') + ProductsExportAgents::$IntervalsGenerate);
        $this->addElement($element);
    }

    public function addTemplateElement()
    {
        $element = new Zend_Form_Element_Select('template');
        $element->setLabel('Шаблон:');
        $element->addMultiOptions(array(
            ''                            => '---',
            'yml.xml'                     => 'Старый yml',
            'hotline.xml'                 => 'HotLine формат',
            'price.xlsx'                  => 'Прайс (EXCEL)',
            'target.my.com.xml'           => 'target.my.com (XML)',
            'mi6.mediatraffic.com.ua.xml' => 'mi6.mediatraffic.com.ua (XML)',
            'yandex.market.xml'           => 'Yandex Market (YML)',
            'facebook.atom.xml'           => 'Facebook Atom (XML)',
            'csv.csv'                     => 'CSV',
            'privat.csv'                  => 'ПриватМаркет CSV',
            'google-merchant.xml'         => 'Google merchant',
        ));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setBelongsTo('params');
        $this->addElement($element);
    }

    public function addPriceTypeElement()
    {
        $element = new Zend_Form_Element_Select('price_type');
        $element->setLabel('Цена:');
        $element->addMultiOptions( [''=>'---'] + Products::$PRICE_TYPES);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setBelongsTo('params');
        $this->addElement($element);
    }

    public function addCurrencyElement()
    {
        $element = new Zend_Form_Element_Select('base_currency');
        $element->setLabel('Базовая валюта:');
        $element->addMultiOptions( [''=>'---'] + Currency::getList()->toKeyValueArray('iso', 'title'));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setBelongsTo('params');
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('currency');
        $element->setLabel('Выводить в валюте:');
        $element->addMultiOptions( [''=>'---'] + Currency::getList()->toKeyValueArray('iso', 'title'));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setBelongsTo('params');
        $this->addElement($element);
    }

    public function addSoldPhrasesElement()
    {
        $element = new Zend_Form_Element_Checkbox('sold_phrases');
        $element->setLabel('Спец. фраза в описание:');
        $element->setBelongsTo('params');
        $this->addElement($element);
    }

    public function addPmStock()
    {
        $element = new Zend_Form_Element_Checkbox('pm_stock');
        $element->setLabel('Добавлять pm_stock:');
        $element->setBelongsTo('params');
        $this->addElement($element);
    }

    public function addPartsPayment()
    {
        $element = new Zend_Form_Element_Checkbox('parts_payment');
        $element->setLabel('Добавлять Оплату частями:');
        $element->setBelongsTo('params');
        $this->addElement($element);
    }

    public function addAppendTextElement()
    {
        $element = new Zend_Form_Element_Textarea('append_text');
        $element->setLabel('Текст в конце описания:');
        $element->setAttrib('style', 'height: 50px; width: 300px;');
        $element->setBelongsTo('params');
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);
    }

    public function addIncreasePercent()
    {
        $element = new Zend_Form_Element_Text('increase_percent');
        $element->setLabel('Увеличить цены на процент:');
        $element->setAttrib('style', 'width: 300px;');
        $element->setBelongsTo('params');
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);
    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);
        $values['params'] = array_filter($values['params'], function ($var) {return ($var !== '');});
        return $values;
    }
}

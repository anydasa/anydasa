<?

class Form_Products_Guarantee extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$(element).closest("td").append(error);}'),
        ]);

        $this->setStyleText('
            #'.$this->getId().' { width: 600px;}
            #'.$this->getId().' td label { display: block; font-weight: normal; padding: 0; margin: 0;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();

        $this->addTitleElement();
        $this->addTermElement();
        $this->addTypeElement();
        $this->addUnitElement();


        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTermElement()
    {
        $element = new Zend_Form_Element_Text('term');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Int', true, array('messages' => array('notInt' => 'Только числовое значение')));
        $element->addValidator('GreaterThan', true, array(
            'messages' => ['notGreaterThan' => "минимум 1"],
            'min' => 0,
        ));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Select('type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setMultiOptions([
           'shop'           => 'от магазина',
           'manufacturer'   => 'от производителя'
        ]);
        $this->addElement($element);
    }

    public function addUnitElement()
    {
        $element = new Zend_Form_Element_Select('unit');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setMultiOptions([
            'days'   => 'дней',
            'months' => 'месяцев'
        ]);
        $this->addElement($element);
    }
}
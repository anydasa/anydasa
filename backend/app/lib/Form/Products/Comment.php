<?


class Form_Products_Comment extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addStyleText('
            form#'.$this->getId().' textarea {
                height: 100px;
            }
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdParentElement();
        $this->addIsQuestionElement();
        $this->addIsCheckedElement();

        $this->addRatingElement();
        $this->addCreatedElement();
        $this->addNameElement();
        $this->addEmailElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }

    public function addIdParentElement()
    {
        $element = new Zend_Form_Element_Text('id_parent');
        $element->setLabel('ID родителького комента:');
        $element->addValidator('Int', true, array('messages' => 'Число'));
        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));
        $this->addElement($element);
    }

    public function addIsCheckedElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_checked');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_admin');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsQuestionElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_question');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addRatingElement()
    {
        $element = new Zend_Form_Element_Text('rating');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');

        $element->addValidator('Int', true, array('messages' => 'Число от 0 до 5'));
        $element->addValidator('Between', true, array(
            'messages' => ['notBetween' => "От 0 до 5"],
            'max' => 5,
            'min' => 0,
        ));

        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));

        $this->addElement($element);
    }

    public function addCreatedElement()
    {
        $element = new Zend_Form_Element_Text('created');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('author_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->addValidator('StringLength', false, array(3, 50, 'messages' => "От 3 до 50 символов"));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('author_email');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');

        $element->addFilter(new Zend_Filter_StringToLower());

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "указан не корректно"
        )));

        $this->addElement($element);
    }

    public function addTextElement()
    {

        $element = new Zend_Form_Element_Textarea('dignity');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('shortcoming');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

}
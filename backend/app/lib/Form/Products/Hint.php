<?

class Form_Products_Hint extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("text", { customConfig: "/js/ckeditor_confings/light.js"});
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addTitleElement();
        $this->addIsEnabledElement();
        $this->addParamsElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }

	public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addParamsElement()
    {
        $element = new Zend_Form_Element_Text('product_title_contains');
        $element->setBelongsTo('params');
        $element->setLabel('Есть в названии товара:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('product_title_notcontains');
        $element->setBelongsTo('params');
        $element->setLabel('Нет в названии товара:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('product_id_brand');
        $element->setBelongsTo('params');
        $element->setLabel('Бренд товара:');

        $element->addMultiOptions(
            array('' => '---') +
            Doctrine_Query::create()->from('ProductsBrands')->orderBy('title')->execute()->toKeyValueArray('id', 'title')
        );

        $this->addElement($element);
    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);

        $values['params'] = json_encode(array_filter($values['params'], function ($var) {return ($var !== '');}));

        return $values;
    }

    public function populate($data)
    {
        $data['params'] = json_decode($data['params'], true);

        return parent::populate($data);
    }

}
<?

class Form_Products_PartialPayments extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addStyleText('
            form#' . $this->getId() . ' #text {height: 100px;}
        ');

        return parent::render();
    }


    public function init()
    {
        $this->addIdElement();

        $this->addMonthElement();
        $this->addPercentElement();

        $this->addSubmitElement();

    }

    public function addMonthElement()
    {
        $element = new Zend_Form_Element_Text('month_count');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'] . ':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addPercentElement()
    {
        $element = new Zend_Form_Element_Text('percent_value');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'] . ':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));

        $this->addElement($element);
    }
}
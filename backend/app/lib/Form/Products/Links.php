<?

class Form_Products_Links extends Form_Abstract
{

    public function init()
    {
        $this->addIdElement();

        $this->addIdProductElement();
        $this->addHtmlElement();
        $this->addIntervalUpdatePriceElement();
        $this->addIsInexactElement();

        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->addJavaScriptText('
            setTimeout(function() {
                $("#url").focus();
            }, 0);
        ');
    }

    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('url');
        $element->setAttrib('style', 'width: 400px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Callback', true, array(
                array('callback' => function($value) {return Zend_Uri::check($value);}),
                'messages' => array(Zend_Validate_Callback::INVALID_VALUE => 'Это не URL'))
        );
        $this->addElement($element);
    }

    public function addHtmlElement()
    {
        $element = new Zend_Form_Element_Textarea('html');
        $element->setAttrib('rows', '8');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    
    public function addIsInexactElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_inexact');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

    }

    public function addIntervalUpdatePriceElement()
    {
        $element = new Zend_Form_Element_Select('interval_update_price');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array(''=>'---'));
        $element->addMultiOptions(ProductsLinks::$INTERVAL_UPDATE_PRICE_LIST);
        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));
        $this->addElement($element);
    }


}
<?

class Form_Products_ProductsOption extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addStyleText('
            #ProductsOption h1, #ProductsOption h4 {display: inline-block; margin-right: 10px;}
            #form_ProductsOption {width: 650px; margin-left: 410px;}
            #form_ProductsOption th {text-align: left;}
            #form_ProductsOption td {width: 400px;}
            #form_ProductsOption .group-element {min-width: 200px;}
            #form_ProductsOption .group-element span {display: none;}
            #form_ProductsOption .group_name {color: Gray; font-size: 11px;}
            #form_ProductsOption .group_name:after {content: "]";}
            #form_ProductsOption .group_name:before {content: "[";}
            #form_ProductsOption .unit {font-size: 11px; color: Gray; font-weight: normal;}

            .ui-autocomplete {
              position: absolute;
              top: 0;
              left: 0;
              cursor: default;
              background: white;
              border: 1px solid #EEEEEE;
              border-top: 0;
              box-shadow: 0 2px 5px #000;
            }
            .ui-autocomplete, .ui-autocomplete > li {
              list-style: none;
              margin: 0;
              padding: 0;
            }
            .ui-autocomplete a {
                text-decoration: none;
            }
            .ui-menu-item {
              padding: 2px 5px !important;
              border-bottom: 1px dotted #CCC;
            }
            .ui-menu-item:hover {
              cursor: pointer;
              background: #EEEEEE;
            }
            .ui-menu-item:last-child {
              border: 0;
            }
        ');

        $this->addStandartJavaScriptValidationForm();
        $this->addJavaScriptLink('/js/forms/ProductsOption.js');

        return parent::render($view);
    }

    public function init()
    {
        $this->addSubmitElement();
    }

    public function addOptionsElements(Products $Product, $id_catalog)
    {
        $Values = $Product->ProductsRefOption->toKeyValueArray('id_option', 'values');

        $Options = Doctrine_Query::create()
            ->from('ProductsOption t1')
            ->where('t1.id_catalog = ?', $id_catalog)
            ->orderBy('t1.sort')
            ->execute();
        $i = 1;
        foreach ($Options as $Option) {
            $element_name = '['. $Option->id .']';
            if ( 'text' == $Option->type ) {
                $element = new Zend_Form_Element_Text($element_name);
                $element->setAttrib('data-presets', Zend_Json_Encoder::encode($Option->getPresets()));
            } elseif ( 'numeric' == $Option->type ) {
                $element = new Zend_Form_Element_Text($element_name);
                $element
                    ->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                    ->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
            } elseif ( 'multiselect' == $Option->type ) {
                $element = new Zend_Form_Element_Multiselect($element_name);
                $presets = $Option->getPresets();
                $element->setMultiOptions(array_combine($presets, $presets));
                $element->setRegisterInArrayValidator(false);
            } elseif ( 'select' == $Option->type ) {
                $element = new Zend_Form_Element_Select($element_name);
                $presets = $Option->getPresets();
                $element->setMultiOptions(array_combine($presets, $presets));
                $element->setRegisterInArrayValidator(false);
            } else {

                throw new Site_Exception('Тип поля "'.$Option->type.' не поддерживается');
            }

            if ( $element->isArray() ) {
                $element->setValue($Values[$Option->id]);
            } else {
                $element->setValue(@reset($Values[$Option->id]));
            }


            $element->addFilter(new Site_Filter_TrimAndDoubleSpace);
            $element->addFilter(new Site_Filter_Comma2NumericPoint);

            $element->setBelongsTo('Options[values]');

            $element->setAttrib('data-param-id', $Option->id);
            $element->setAttrib('data-type', $Option->type);
            $element->setAttrib('tabindex', $i++);

            $label = '';
            if ( !empty($Option->group_name) ) {
                $label .= ' <span class="group_name">'.$Option->group_name.'</span> ';
            }
            $label .= '<span class="param_title">' . $Option->title. '</span>';
            if ( !empty($Option->unit) ) {
                $label .= ', <i class="unit">' . $Option->unit. '</i>';
            }
            if ( $Option->is_require ) {
                $label .= ' <span style="color: Red;">*</span>';
            }
//            $element->setLabel( $label );
            $this->addElement($element);

            $group = array($element);
            /*if ( $Param['is_show_text'] ) {
                $element2 = new Zend_Form_Element_Text('['.$Param['id'].']');
                $element2->setValue( $CustomParams[$Param['id']] );
                $element2->setBelongsTo('_CustomParams');
                $element2->setAttrib('placeholder', 'текст');
                $element2->setAttrib('style', 'color: green;');
                $this->addElement($element2);
                $group []= $element2;
            }*/
            $this->addDisplayGroup($group, 'group'.$Option->id, array('legend' => $label, 'escape'=>false));

        }
    }

}
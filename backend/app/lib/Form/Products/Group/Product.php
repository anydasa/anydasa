<?

class Form_Products_Group_Product extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();
        return parent::render();
    }

    public function init()
    {
        $this->addIdProductElement();
        $this->addIdGroupElement();
        $this->addTitleElement();
        $this->addImageElement();
        $this->addSubmitElement();

        $this->addStyleText('
            form#ProductsRefGroups .hint img {
                margin-top: 10px;
            }
        ');
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addIdGroupElement()
    {
        $element = new Zend_Form_Element_Hidden('id_group');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id_type');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setLabel('Картинка:');
        $element->setValueDisabled(true); // отключение авто receive
        $Validator = new Zend_Validate_File_MimeType(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $sizeValidator = new Zend_Validate_File_Size(array('max'=>(1024*1024)));
        $sizeValidator->setMessage('Максимальный размер файла "Фотография" %max%');
        $element->addValidator($sizeValidator);

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_copy');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('crop_params');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        parent::populate($values);

        $node = Doctrine_Query::create()
            ->from('ProductsRefGroups')
            ->where('id_product = ?', $values['id_product'])
            ->addWhere('id_group = ?', $values['id_group'])
            ->fetchOne();

        if ( $node && $node->issetImg() ) {
            $this->getElement('image')->setDescription('<img src="'.$node->getImgUrl().'?'.rand().'">');
        }

        $this->getElement('image')->setDescription(
            $this->getElement('image')->getDescription()
            . '<div>'
            . Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
                    'routeName'   => 'products-group-icons',
                    'routeParams' => array('id_group' => $values['id_group']),
                    'queryParams' => array(),
                    'buttonText'  => '<i class="fa fa-lg fa-image"> <span style="font-size: 13px; position: relative; top: -2px;">Выбрать из существующих</span></i>',
               ))
            . '</div>');

        $this->getElement('image')->setDescription(
            $this->getElement('image')->getDescription()
            . '<div>'
            . Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
                'routeName'   => 'products-group-icons-from-product',
                'routeParams' => array('id_product' => $values['id_product']),
                'queryParams' => array(),
                'buttonText'  => '<i class="fa fa-lg fa-image"> <span style="font-size: 13px; position: relative; top: -2px;">Выбрать из товара</span></i>',
            ))
            . '</div>');
    }

    public function receive(ProductsRefGroups $node)
    {
        $target = $node->getImgPath();

        if ( !is_dir(dirname($target)) ) {
            mkdir(dirname($target));
        }

        if ( ($source = $this->image_copy->getValue()) && Site_Image::isImage($source) ) {
            copy($source, $target);
            $im = new Imagick($target);

            if ( $crop_params = $this->crop_params->getValue() ) {
                $crop_params = Zend_Json::decode($crop_params);
                $im->resizeImage($crop_params['oH'], $crop_params['oW'], imagick::FILTER_LANCZOS, 0.9, true);
                $im->cropImage($crop_params['w'], $crop_params['h'], $crop_params['x'], $crop_params['y']);
            }

            $im->setImageFormat('jpg');
            $im->resizeImage(100, 100, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();
        }

        if ( $this->image->getValue() ) {
            $this->image->addFilter('Rename', array('target' => $target, 'overwrite'  => true));
            $this->image->receive();

            $im = new Imagick($target);
            $im->setImageFormat('jpg');
            $im->resizeImage(100, 100, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();
        }



    }
}
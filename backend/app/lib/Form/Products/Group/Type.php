<?

class Form_Products_Group_Type extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();
        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addIsShowImageElement();
        $this->addCodeElement();
        $this->addTitleElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 400px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addCodeElement()
    {
        $element = new Zend_Form_Element_Text('code');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 400px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addIsShowImageElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_show_image');
        $element->setValue(1);
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
<?

class Form_Products_Group_Group extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();
        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addTypeElement();
        $this->addSubmitElement();

    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 400px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Select('id_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(['' => '---'] + ProductsGroups::getTypes()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }
}
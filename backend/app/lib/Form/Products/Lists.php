<?

class Form_Products_Lists extends Form_Abstract
{
    public function render()
    {

        $this->setCKFinderBasePath('product-custom-list');

        $this->_setDecorsTable();

        $this->addStyleText('
            form#'.$this->getId().' #text {height: 100px;}
        ');

        $this->addJavaScriptText('
            CKEDITOR.replace("header", { customConfig: "/js/ckeditor_confings/basic.js"});
        ');

        return parent::render();
    }


    public function init()
    {
        $this->addIdElement();

        $this->addAliasElement();
        $this->addTitleElement();
        $this->addHeaderElement();

        $this->addSubmitElement();

    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $this->addElement($element);
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Site_Filter_Slugify);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addHeaderElement()
    {
        $element = new Zend_Form_Element_Textarea('header');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

}
<?

class Form_Products_LetMeKnow extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();
        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();

        $this->addNameElement();
        $this->addProcessedElement();

        $this->addSubmitElement();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addProcessedElement()
    {
        $element = new Zend_Form_Element_Checkbox('processed');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
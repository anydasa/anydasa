<?

class Form_Products_Margin extends Form_Abstract
{
    protected $productStickers;
    protected $productStickersCodes;
    protected $partialPayments;
    protected $currentProductPrice;

    public function init()
    {
        $this->addIdProductElement();
        $this->addUseRetailElement();
        $this->addPriceMinElement();
        $this->addPricePartnerElement();
        $this->addPriceTradeElement();
        $this->addPriceRetailElement();
        $this->addCurrencyElement();
        $this->addPricePartialElement();

        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->setStyleText('
            #ProductsMargins input {width: 100px;}
            #ProductsMargins input.round {width: 25px; text-align: center;}
            #ProductsMargins select {width: 100px;}
            #ProductsMargins label.error {position: absolute; left: 210px; top: 0; width: 100px;}
            #ProductsMargins #id_currency {float: left; margin-right: 5px;}
            #ProductsMargins #id_currency+p {padding: 3px; font-style: italic; font-size: 11px;}

            #ProductsMargins .group-element {float: left; margin-right: 3px; position: relative;}
            #ProductsMargins #use_retail {width: auto;}
        ');
    }

    public function addUseRetailElement()
    {
        $element = new Zend_Form_Element_Checkbox('use_retail');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('Float', true, array('messages' => array('notFloat' => "Не число")));

        $this->addElement($element);
    }

    public function addPriceMinElement()
    {
        $element = new Zend_Form_Element_Text('price_min');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('type_min');
        $element->addMultiOptions(array('fix' => 'Фикс.', 'numeric'=>'+', 'present'=>'+ %'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('round_min');
        $element->setAttrib('class', 'round');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('Int', true, array('messages' => array('notInt' => "Не число")));
        $this->addElement($element);

        $this->addDisplayGroup(array('price_min', 'type_min', 'round_min'), 'min', array('legend'=>'Минимальная:'));
    }

    public function addPricePartnerElement()
    {
        $element = new Zend_Form_Element_Text('price_partner');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('type_partner');
        $element->addMultiOptions(array('fix' => 'Фикс.', 'numeric'=>'+', 'present'=>'+ %'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('round_partner');
        $element->setAttrib('class', 'round');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('Int', true, array('messages' => array('notInt' => "Не число")));
        $this->addElement($element);

        $this->addDisplayGroup(array('price_partner', 'type_partner', 'round_partner'), 'partner', array('legend'=>'Партнерская:'));
    }

    public function addPriceTradeElement()
    {
        $element = new Zend_Form_Element_Text('price_trade');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('type_trade');
        $element->addMultiOptions(array('fix' => 'Фикс.', 'numeric'=>'+', 'present'=>'+ %'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('round_trade');
        $element->setAttrib('class', 'round');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('Int', true, array('messages' => array('notInt' => "Не число")));
        $this->addElement($element);

        $this->addDisplayGroup(array('price_trade', 'type_trade', 'round_trade'), 'trade', array('legend'=>'Опт:'));
    }

    public function addPriceRetailElement()
    {
        $element = new Zend_Form_Element_Text('price_retail');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('type_retail');
        $element->addMultiOptions(array('fix' => 'Фикс.', 'numeric'=>'+', 'present'=>'+ %'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('round_retail');
        $element->setAttrib('class', 'round');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('Int', true, array('messages' => array('notInt' => "Не число")));
        $this->addElement($element);

        $this->addDisplayGroup(array('price_retail', 'type_retail', 'round_retail'), 'retail', array('legend'=>'Розница:'));
    }

    public function addPricePartialElement()
    {
        $element = new Zend_Form_Element_Select('id_partial_payments');
        $element->setLabel('Рассрочка (мес.):');
        $element->addMultiOptions(['' => '---'] +  Doctrine_Query::create()->from('PartialPayments')->execute()->toKeyValueArray('id', 'month_count'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('price_pm');
        $element->setLabel('Цена ПМ:');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $this->addDisplayGroup(array('id_partial_payments', 'price_pm'), 'partial', array('legend'=>'Рассрочка и цена ПМ:'));
    }

    public function addCurrencyElement()
    {
        $element = new Zend_Form_Element_Select('id_currency');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setDescription('Не учавствует при %');
        $element->addMultiOptions(Doctrine::getTable('Currency')->findAll()->toKeyValueArray('id', 'iso'));
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        $poppulate =  parent::populate($values);
        $this->setProductStickersInForm();
        $this->setPartialPaymentsInForm();
        if ($this->getValue('id_product')) {
            $this->setCurrentProductPrice();
        }
        return $poppulate;
    }

    private function setProductStickersInForm()
    {
        $q = Doctrine_Manager::getInstance()->getCurrentConnection();
        $productId = $this->getValue('id_product');
        $foundCodes = $q->execute('SELECT unnest(stickers) FROM products WHERE id = ?', array($productId))->fetchAll(PDO::FETCH_COLUMN);
        if (is_array($foundCodes) && !empty($foundCodes)) {
            $this->productStickersCodes = $foundCodes;
            $this->productStickers = Doctrine_Query::create()->select('title')->from('ProductsStickers')
                ->whereIn('id', $foundCodes)->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
        } else {
            $this->productStickers = false;
            $this->productStickersCodes = false;
        }
    }

    private function setCurrentProductPrice()
    {
        $q = Doctrine_Manager::getInstance()->getCurrentConnection();
        $productId = $this->getValue('id_product');
        $foundPrice = $q->execute('SELECT price_retail, id_currency FROM products WHERE id = ?', array($productId))->fetchAll(PDO::FETCH_ASSOC);
        $this->currentProductPrice = $this->convertPriceRetail($foundPrice[0]['price_retail'], $foundPrice[0]['id_currency']);
    }

    private function setPartialPaymentsInForm()
    {
        $this->partialPayments = Doctrine_Query::create()->from('PartialPayments')->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
    }

    private function convertPriceRetail($price, $currency)
    {
        $price = Currency::conv(
            $price,
            Currency::getById($currency)->iso,
            'UAH'
        );

        return $price;
    }

    public function render(Zend_View_Interface $view = null)
    {
        $content = parent::render($view);
        $content .= ' <script>window.currentProductStickers = ' . json_encode($this->productStickers) . '</script>';
        $content .= ' <script>window.partialPaymentsVars    = ' . json_encode($this->partialPayments) . '</script>';
        $content .= ' <script>window.currentProductPrice    = ' . json_encode($this->currentProductPrice) . '</script>';
        $content .= '<script>$(\'#price_pm-label\').parent().prepend(\'<div class="price_pm_helper" style="color:red;"></div>\');</script>';
        $content .= '<script type="text/javascript" src="/js/products-list.js"></script>';
        $content .= '<script>processProductsMarginsForm();</script>';
        return $content;
    }
}
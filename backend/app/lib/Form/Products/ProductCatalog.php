<?

class Form_Products_ProductCatalog extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();
        $this->addJavaScriptText('filterSelect(\'#filter_catalogs\', \'#id_catalogs\')');

        $this->setStyleText('
            .MultiCheckbox {max-height: 200px; overflow-y: scroll; font-size: 11px; width: 300px; margin: 10px;}
            .MultiCheckbox label {font-weight: normal; cursor: pointer; line-height: 20px; margin: 0; vertical-align: middle;}
            .MultiCheckbox label input {position: relative; top: 2px;}
        ');


        return parent::render($view);
    }

    public function init()
    {
        $element = new Zend_Form_Element_Text('filter_catalogs');
        $element->setLabel('Фильтр:');
        $this->addElement($element);

//      -------------------------------------

        $element = new Zend_Form_Element_Multiselect('id_catalogs');
        $element->setLabel('Каталог:');
        $element->setAttrib('style', 'height: 350px; width: 500px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        foreach (Doctrine_Query::create()->from('Catalog')->orderBy('title')->execute() as $item) {
            $element->addMultiOption($item->id, $item->title);
        }

        $this->addElement($element);

//        -----------------------------------------------

        $this->addSubmitElement();
    }
}
<?

class Form_Products_Set extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptLink('/js/forms/products-set.js');

        if ( $id_set = $this->getElement('id')->getValue() ) {
            $ProductSet = Doctrine::getTable($this->modelName)->find($id_set);
            $this->addJavaScriptText('$(function () {
                populateOwners('.Zend_Json::encode($ProductSet->Products->toArray()).');
            });');
            $ProductSet->ProductsSetsProduct->loadRelated('Products');

            $productGroups = [];
            $productDiscounts = [];
            foreach ($ProductSet->ProductsSetsProduct as $ProductsSetsProduct) {
                $productDiscounts[$ProductsSetsProduct->id_product] = $ProductsSetsProduct->discount;
                $productGroups[$ProductsSetsProduct->group_num][] = $ProductsSetsProduct->Products->toArray();
            }
            foreach ($productGroups as $group) {
                $this->addJavaScriptText('$(function () {
                    $("#addGroupButton").click();
                    var $container = $("form#ProductsSets tr.group:last .productsContainer");
                    populateProductsGroup('.Zend_Json::encode($group).', $container);
                });');
            }
            $this->addJavaScriptText('$(function () {
                populateProductsDiscount('.Zend_Json::encode($productDiscounts).');
            });');

        }

        $this->addStyleText('
            form#'.$this->getId().' .discounts input.error {border-color: Red; background: #FFB3B3;}
            form#'.$this->getId().' .discounts label.error {display: none !important;}
            form#'.$this->getId().' .sort-handle {cursor: move;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 400px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $this->addElement($element);
    }
}
<?

class Form_Products_Description extends Form_Abstract
{
    /**
     * @var Products
     */
    private $product;

    public function __construct($formName, Products $product, $options = null)
    {
        $this->product = $product;

        parent::__construct($formName, $options);
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $ckFinderUrl = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Products&responseType=json&id='.$this->product->id;

        $this->addJavaScriptText("
            $(function () {
                CKEDITOR.replace( 'description', {
                    language: 'ru',
                    uiColor: '#AADC6E',
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserUploadUrl: '$ckFinderUrl',
                    filebrowserWindowWidth: '1000',
                    filebrowserWindowHeight: '700'
                });
            });
        ");

        return parent::render($view);
    }

    public function init()
    {
        $this->addDescriptionElement();
        $this->addSubmitElement();
    }

    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $this->addElement($element);
    }
}

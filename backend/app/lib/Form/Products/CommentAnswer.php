<?


class Form_Products_CommentAnswer extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addStyleText('
            form#'.$this->getId().' textarea {
                height: 100px;
            }
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdParentElement();
        $this->addIsCheckedElement();
        $this->addNameElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }

    public function addIdParentElement()
    {
        $element = new Zend_Form_Element_Hidden('id_parent');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id_product');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('is_admin');
        $element->setValue(1);
        $this->addElement($element);
    }

    public function addIsCheckedElement()
    {
        $element = new Zend_Form_Element_Hidden('is_checked');
        $element->setValue(1);
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('author_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setValue('Megabite');
        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel('Ответ:');
        $this->addElement($element);
    }

}
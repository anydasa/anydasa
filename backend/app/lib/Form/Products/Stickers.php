<?

class Form_Products_Stickers extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addStyleText('
            form#'.$this->getId().' #text {height: 100px;}
        ');

        return parent::render();
    }


    public function init()
    {
        $this->addIdElement();

        $this->addIsShowElement();
        $this->addIsPrivateElement();
        $this->addAliasElement();
        $this->addBackgroundElement();
        $this->addTitleElement();
        $this->addTextElement();

        $this->addSubmitElement();

    }

    public function addBackgroundElement()
    {
        $element = new Zend_Form_Element_Text('color');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter('Null');
        $element->setDescription('<a target="_blank" href="http://www.w3schools.com/tags/ref_colorpicker.asp">выбор цвета</a>');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('background');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter('Null');
        $element->setDescription('<a target="_blank" href="http://www.w3schools.com/tags/ref_colorpicker.asp">выбор цвета</a>');
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }
    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Site_Filter_Slugify);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsShowElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_show_in_filter');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_show_in_product');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addIsPrivateElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_private');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
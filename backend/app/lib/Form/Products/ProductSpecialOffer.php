<?

class Form_Products_ProductSpecialOffer extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("special_offer_text", { customConfig: "/js/ckeditor_confings/ProductSpecialOffer.js"});
            
            $("form#'.$this->getId().'")
                .find("#special_offer_expiry")
                .datetimepicker({
                    dateFormat: \'yy-mm-dd\',
                    timeFormat: \'HH:mm\',
                    minDate: \'now\',
                    maxDate: \'+30D\'
                }).change();
        ');

        $this->setStyleText('
            #'.$this->getId().' #special_offer_text {width: 300px;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $element = new Zend_Form_Element_Text('special_offer_expiry');
        $element->setLabel('Скрыть:');
        $element->addFilter(new Zend_Filter_Null());
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('special_offer_text');
        $element->addFilter(new Zend_Filter_Null());
        $this->addElement($element);

//        -----------------------------------------------

        $this->addSubmitElement();
    }

    public function populate($data)
    {
        if (!empty($data['special_offer_expiry'])) {
            $data['special_offer_expiry'] = (new \DateTime($data['special_offer_expiry']))->format('Y-m-d H:i');
        }

        return parent::populate($data);
    }
}
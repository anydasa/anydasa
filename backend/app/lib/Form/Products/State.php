<?

class Form_Products_State extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addHotlineElement();

        $this->addSubmitElement();
    }

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Text('id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z0-9]{0,}$/i',
                'messages' => 'Только латинские буквы и цифры'
            )
        );
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addHotlineElement()
    {
        $element = new Zend_Form_Element_Select('hotline_value');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array(0 => 'новый', 1 => 'востановленный', 2 => 'Б/У'));
        $this->addElement($element);
    }
}
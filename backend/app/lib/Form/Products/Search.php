<?

class Form_Products_Search extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {
                width: 1100px;
            }
            #'.$this->getId().' #'.$this->code->getId().',
            #'.$this->getId().' #'.$this->excludeid->getId().',
            #'.$this->getId().' #'.$this->id->getId().' {
                height: 20px;
                width: 100px;
                resize:none;
            }
            #'.$this->getId().' #'.$this->code->getId().':focus,
            #'.$this->getId().' #'.$this->excludeid->getId().':focus,
            #'.$this->getId().' #'.$this->id->getId().':focus {
                height: 300px;
                width: 100px;
                position: absolute;
            }
            #'.$this->getId().' .group-element {float: left; margin-right: 3px;}
            #'.$this->getId().' .group-element span {display:none;}
            #'.$this->getId().' #fieldset-additional td select {width: 190px;}
            #'.$this->getId().' #fieldset-balance .group-element,
            #'.$this->getId().' #fieldset-expected .group-element {width: 50px; }
            #'.$this->getId().' .MultiCheckbox {resize: vertical; max-height: 100px; overflow-y: scroll; background: white; border: 1px solid Gray; width: 300px;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0; padding: 1px 2px; line-height: 12px; font-weight: normal; font-size: 11px; float: left; width: 100%;}
            #'.$this->getId().' .MultiCheckbox label input {margin: 0; float: left;}
            #'.$this->getId().' select option {background: White;}
            #'.$this->getId().' select.selected {background: #009966;}
            #'.$this->getId().' .filterMultiCheckboxContainer {float: left;}

            #'.$this->getId().' #fieldset-codes .group-element {width: 100px; display: block; border: solid 1px transparent;}
        ');

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( this.checked ) {
                    $(this).closest("label").css({background: "Green"});
                    $(this).parents("tr").find(">th").append(
                        $("<div />")
                            .attr("id", "l"+$(this).attr("id"))
                            .css({fontWeight:"normal", color: "Green", fontSize: "11px", lineHeight: "12px"})
                            .html($(this).parent().text())
                    )
                } else {
                    $("#l"+$(this).attr("id")).remove();
                    $(this).closest("label").css({background: "Transparent"});
                }
            }).change()

            $("form#'.$this->getId().' select").on("change", function() {
                if ( "" != $(this).val() ) {
                    $(this).addClass("selected");
                } else {
                    $(this).removeClass("selected");
                }
            }).change();
        ');

        $this->addJavaScriptText('
           $(function () {
                $("form#'.$this->getId().' .MultiCheckbox").filterMultiCheckbox();
           });
        ');


        return parent::render($view);
    }

    public function init()
    {
        $this->setMethod('GET');

        $this->addIdElement();
        $this->addTitleElement();
        $this->addOptionElement();
        $this->addVendorElement();
        $this->addCatalogElement();
        $this->addBrandElement();
        $this->addStatusElement();
        $this->addGroupTypeElement();
        $this->addStickersElement();

//        $this->addBalanceGroup();
        $this->addStateElement();
        $this->addAdditionalGroup();
        $this->addViewGroup();
        $this->addOrderElement();

        $this->addSubmitElement();
    }

    /*public function populate(array $values)
    {
        if ( is_array($values['excludeid']) ) {
            $values['excludeid'] = implode("\n", $values['excludeid']);
        }
        if ( is_array($values['id']) ) {
            $values['id'] = implode("\n", $values['id']);
        }
        if ( is_array($values['code']) ) {
            $values['code'] = implode("\n", $values['code']);
        }
        return parent::populate($values);
    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);

        $values['excludeid']    = explode("\n", $values['excludeid']);
        $values['id']           = explode("\n", $values['id']);
        $values['code']         = explode("\n", $values['code']);

        return $values;
    }*/

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Textarea('id');
        $element->setAttrib('placeholder', 'ID');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('excludeid');
        $element->setAttrib('placeholder', 'Исключить ID');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('code');
        $element->setAttrib('placeholder', 'Код');
        $element->setAttrib('wrap', 'off');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('alias');
        $element->setAttrib('style', 'width: 300px;');
        $element->setAttrib('placeholder', 'Алиас');
        $this->addElement($element);

        $this->addDisplayGroup(array('id', 'excludeid', 'code', 'alias'), 'codes', array('legend'=>'Код:'));
    }

    public function addBalanceGroup()
    {
        $element = new Zend_Form_Element_Text('balance_from');
        $element->setAttrib('placeholder', "от");
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('balance_to');
        $element->setAttrib('placeholder', "до");
        $this->addElement($element);

        $this->addDisplayGroup(array('balance_from', 'balance_to'), 'balance', array('legend'=>'Баланс:'));

        $element = new Zend_Form_Element_Text('expected_balance_from');
        $element->setAttrib('placeholder', "от");
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('expected_balance_to');
        $element->setAttrib('placeholder', "до");
        $this->addElement($element);

        $this->addDisplayGroup(array('expected_balance_from', 'expected_balance_to'), 'expected', array('legend'=>'Ожидается:'));
    }


    public function addDiscountElement()
    {
        $element = new Zend_Form_Element_Text('discount');
        $element->setLabel('Скидка:');
        $this->addElement($element);
    }
    public function addPriceElement()
    {
        $element = new Zend_Form_Element_Text('price');
        $element->setLabel('Цена:');
        $this->addElement($element);
    }

    public function addOptionElement()
    {
        $element = new Zend_Form_Element_Text('option_param');
        $element->setAttrib('placeholder', 'параметр');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('option_value');
        $element->setAttrib('placeholder', 'значение (REGEXP)');
        $this->addElement($element);

        $this->addDisplayGroup(array('option_param', 'option_value'), 'option', array('legend'=>'Опции:'));
    }

    public function addStateElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('states');
        $element->setLabel('Состояние товара:');
        $element->addMultiOptions(Doctrine_Query::create()->from('ProductsState')->orderBy('sort')->execute()->toKeyValueArray('id', 'title'));
        $element->setSeparator('');
        $this->addElement($element);
    }

    public function addAdditionalGroup()
    {
        $element = new Zend_Form_Element_Select('is_visible');
        $element->addMultiOptions(array('' => '--- Доступен ---', 0=> 'Не доступен', 1=>'Доступен'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_margin');
        $element->addMultiOptions(array('' => '--- Маржа ---', 0=> 'Маржа не установленна', 1=>'Маржа установленна'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_links');
        $element->addMultiOptions(array('' => '--- Внешние ссылки ---', 0 => 'Нет внешних ссылок', 1 => 'Есть внешние ссылки' ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('is_link_checked');
        $element->addMultiOptions(array('' => '--- Внешние ссылки ---', 1=> 'Внешние ссылки проверенные', 0=> 'Внешние ссылки не проверенные'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('is_spy_prices');
        $element->addMultiOptions(array('' => '--- Цены ---', 0=> 'Цены не отслеживаются', 1=> 'Цены отслеживаются'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('is_img');
        $element->addMultiOptions(array('' => '--- Рисунки ---', 0 => 'Рисунков нет', 1 => 'Есть рисунки'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('filled_require_option');
        $element->addMultiOptions(array('' => '--- Обязательные поля ---', 0 => 'Обязательные поля не заполнены', 1 => 'Обязательные поля заполнены'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_vendor_products');
        $element->addMultiOptions(array('' => '--- Связанность с поставщиком ---', 0 => 'Не связан с поставщиком', 1 => 'Связан с поставщиком'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_catalog');
        $element->addMultiOptions(array('' => '--- Наличие каталогов ---', 0 => 'Нет ни в одном каталоге', 1 => 'Есть в каталоге'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_vendor_owner');
        $element->addMultiOptions(array('' => '--- Актуальный поставщик ---', 1 => 'Есть актуальный поставщик', 0 => 'Нет актуального поставщика'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_groups');
        $element->addMultiOptions(array('' => '--- Группы ---', 1 => 'Есть группа', 0 => 'Нет групп'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_accessories');
        $element->addMultiOptions(array('' => '--- Аксессуары ---', 1 => 'Есть аксессуары', 0 => 'Нет аксессуаров'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_set');
        $element->addMultiOptions(array('' => '--- Комлект ---', 1 => 'Есть Комлект', 0 => 'Нет Комлект'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('id_guarantee');
        $element->addMultiOptions(['' => '----Гарантия-----', 'null' => 'без гарантии'] + Doctrine::getTable('ProductsGuarantee')->findAll()->toKeyValueArray('id', 'title'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('is_empty_price');
        $element->addMultiOptions(['' => '----Цена-----', '0' => 'Нет цены', 1 => 'Есть цена']);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_others_links');
        $element->addMultiOptions(['' => '----Левые ссылки-----', 1 => 'Есть левые ссылки']);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('is_pricing_hotline');
        $element->addMultiOptions(['' => '--Хотлайн--', 1 => 'Есть', 0 => 'Нет']);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_very_small_orig_img');
        $element->addMultiOptions(['' => '----Мелкие рисунки-----', 1 => 'Есть мелкие рисунки']);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('has_series');
        $element->addMultiOptions(['' => '----Серии-----', 1 => 'В сериях', 0 => 'Нет в сериях']);
        $this->addElement($element);

        $this->addDisplayGroup(array('has_series', 'is_pricing_hotline', 'has_others_links', 'is_empty_price', 'id_guarantee', 'has_set', 'has_accessories', 'has_groups', 'has_catalog', 'is_visible', 'has_margin', 'has_links', 'is_link_checked', 'is_spy_prices', 'is_img', 'has_very_small_orig_img', 'filled_require_option', 'has_vendor_products', 'has_vendor_owner'), 'additional', array('legend'=>'Дополнительно:'));
    }

    public function addOrderElement()
    {
        $element = new Zend_Form_Element_Select('sort');
        $element->setLabel('Сортировать:');
        $element->addMultiOptions([
            ''=> '---Сортировка---',
            'sort-desc'     => 'по популярности',
            'price-asc'     => 'от дешевых',
            'price-desc'    => 'от дорогих',
            'created-desc'  => 'новые поступления',
            'rating-desc'   => 'по рейтингу',
            'title-desc'    => 'Названию',
        ]);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('on_page');
        $element->addMultiOptions(array(5 => 'По 5 на странице', 10 => 'По 10 на странице', 20 => 'По 20 на странице', 50 => 'по 50 на странице', 100 => 'по 100 на странице'));
        $element->setLabel('Показывать:');
        $element->setValue(20);
        $this->addElement($element);

        $this->addDisplayGroup(array('sort', 'on_page'), 's', array('legend'=>'-'));

    }

    public function addViewGroup()
    {
       /* $element = new Zend_Form_Element_MultiCheckbox('hide_elements');
        $element->setLabel('Скрыть елементы:');
        $element->addMultiOptions([
            'img'       => 'Рисунок',
            'options'   => 'Опции',
            'buttons'   => 'Кнопки',
            'detail'    => 'Детали',
            'price'     => 'Цена'
        ]);
        $this->addElement($element);*/

        $element = new Zend_Form_Element_Select('on_page');
        $element->addMultiOptions(array(5 => 'По 5 на странице', 10 => 'По 10 на странице', 20 => 'По 20 на странице', 50 => 'по 50 на странице', 100 => 'по 100 на странице'));
        $element->setLabel('Показывать:');
        $element->setValue(20);
        $this->addElement($element);
    }

    public function addHitElement()
    {
        $element = new Zend_Form_Element_Select('is_hit');
        $element->setLabel('Хит:');
        $element->addMultiOptions(array('' => '---', 0=> 'нет', 1=>'да'));
        $this->addElement($element);
    }

    public function addStatusElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('status');
        $element->setLabel('Статус:');
        $element->addMultiOptions( Products::getStatusList() );
        $element->setSeparator('');
        $this->addElement($element);
    }
    public function addStickersElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('stickers');
        $element->setLabel('Стикеры:');
        $element->addMultiOptions( Doctrine::getTable('ProductsStickers')->findAll()->toKeyValueArray('alias', 'title') );
        $element->setSeparator('');
        $element->setBelongsTo('filter');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('notid_sticker');
        $element->setLabel('Исключть стикеры:');
        $element->addMultiOptions( Doctrine::getTable('ProductsStickers')->findAll()->toKeyValueArray('id', 'title') );
        $element->setSeparator('');
        $this->addElement($element);
    }
    public function addBrandElement()
    {
        $values = Doctrine_Query::create()->from('ProductsBrands')->addOrderBy('title')->execute()->toKeyValueArray('id', 'title');
        $element = new Zend_Form_Element_MultiCheckbox('id_brand');
        $element->setLabel('Бренд:');
        $element->addMultiOptions($values);
        $element->setSeparator('');
        $this->addElement($element);
    }

    public function addCatalogElement()
    {
        $values = Doctrine_Query::create()->from('Catalog')->addOrderBy('title')->execute()->toKeyValueArray('id', 'title');

        $element = new Zend_Form_Element_MultiCheckbox('id_catalog');
        $element->setLabel('Каталог:');
        $element->addMultiOptions($values);
        $element->setSeparator('');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('notid_catalog');
        $element->setLabel('Исключить каталог:');
        $element->addMultiOptions($values);
        $element->setSeparator('');
        $this->addElement($element);
    }

    public function addGroupTypeElement()
    {
        $values = Doctrine_Query::create()->from('ProductsGroupsTypes')->addOrderBy('sort')->execute()->toKeyValueArray('id', 'title');

        $element = new Zend_Form_Element_MultiCheckbox('id_group_type');
        $element->setLabel('Группа:');
        $element->addMultiOptions($values);
        $element->setSeparator('');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('notid_group_type');
        $element->setLabel('Исключить группу:');
        $element->addMultiOptions($values);
        $element->setSeparator('');
        $this->addElement($element);
    }

    public function addVendorElement()
    {
        $values = Doctrine_Query::create()->from('Vendors')->addOrderBy('name')->execute()->toKeyValueArray('id', 'name');

        $element = new Zend_Form_Element_MultiCheckbox('id_vendor_owner');
        $element->setSeparator('');
        $element->setLabel('Владелец:');
        $element->addMultiOptions($values);
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('id_vendor');
        $element->setSeparator('');
        $element->setLabel('Поставщики:');
        $element->addMultiOptions($values);
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('vendors_product_more_than');
        $element->setLabel('Кол-во поставщиков:');
        $element->setAttrib('placeholder', 'Количество привязанных товаров-поставщиков более');
        $element->setAttrib('style', 'width: 400px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('notid_vendor');
        $element->setSeparator('');
        $element->setLabel('Нет поставщика:');
        $element->addMultiOptions($values);
        $this->addElement($element);

    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setAttrib('style', 'width: 300px;');
        $element->setAttrib('placeholder', 'Подстрока в названии');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('k');
        $element->setAttrib('style', 'width: 300px;');
        $element->setAttrib('placeholder', "Морфология (все слова)");
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('orkey');
        $element->setAttrib('style', 'width: 300px;');
        $element->setAttrib('placeholder', "Морфология (любые слова)");
        $this->addElement($element);

        $this->addDisplayGroup(array('title', 'k', 'orkey'), 'title_', array('legend'=>'Название:'));

    }

    /*public function addExtendElement()
    {
        $element = new Zend_Form_Element_Checkbox('img_has_dead');
        $element->setLabel('Показать с нарушеной структурой рисунков:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('img_has_dublicate');
        $element->setLabel('Показать с дубликатами рисунков:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('has_bad_price');
        $element->setLabel('Показать нарушенные цены:');
        $this->addElement($element);



    }*/

    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->getElement('save')->setLabel('Искать');
    }
}

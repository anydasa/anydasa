<?

class Form_Products_Accessory extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptLink('/js/forms/products-accessories.js');

        if ( $id_accessory = $this->getElement('id')->getValue() ) {
            $Products = Doctrine::getTable($this->modelName)->find($id_accessory);

            $this->addJavaScriptText('$(function () {
                populateOwnerProducts('.Zend_Json::encode($Products->Products->toArray()).');
            });');

            $Products->ProductsRefAccessories->loadRelated('Products');
            $relatedProducts = [];
            foreach ($Products->ProductsRefAccessories as $Rel) {
                $relatedProducts []= $Rel->Products->toArray();
            }
            $this->addJavaScriptText('$(function () {
                populateRelatedProducts('.Zend_Json::encode($relatedProducts).');
            });');

        }

        $this->addStyleText('
            form#'.$this->getId().' .discounts input.error {border-color: Red; background: #FFB3B3;}
            form#'.$this->getId().' .discounts label.error {display: none !important;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 400px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $this->addElement($element);
    }
}
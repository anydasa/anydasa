<?

class Form_Products_PricingHotline extends Form_Abstract
{
    public function init()
    {
//        $this->addIdProductElement();
        $this->addIsEnabledElement();
        $this->addPriceElement();

        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' label.error {position: absolute; left: 210px; top: 0; width: 100px;}
            #'.$this->getId().' .group-element {float: left; margin-right: 3px; position: relative; width: 40px;}
            #'.$this->getId().' #is_enabled {width: auto;}
        ');
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('Float', true, array('messages' => array('notFloat' => "Не число")));

        $this->addElement($element);
    }

    public function addPriceElement()
    {
        $element = new Zend_Form_Element_Text('price_change_uah');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('max_devaluation');
        $element->setAttrib('placeholder', 'min');
        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('max_revaluation');
        $element->setAttrib('placeholder', 'max');
        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);


        $this->addDisplayGroup(array('max_devaluation', 'max_revaluation'), 'price', array('legend'=>'Допустимое отклонение в %:'));
    }

}
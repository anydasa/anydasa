<?

class Form_Vendor extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();

        $this->addNameElement();
        $this->addIntervalProcessElement();
        $this->addIntervalActualElement();
        $this->addSiteElement();
        $this->addDescriptionElement();
        $this->addUrlFileElement();
        $this->addIsAutoUploadElement();
        $this->addIsEnabledElement();

        $this->addSubmitElement();
        $this->_setDecorsTable();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addIntervalProcessElement()
    {
        $element = new Zend_Form_Element_Select('interval_process');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(array(''=>'---'));
        $element->addMultiOptions(Vendors::$ProcessIntervals);
        $this->addElement($element);
    }

    public function addIntervalActualElement()
    {
        $element = new Zend_Form_Element_Select('interval_actual');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(array(''=>'---'));
        $element->addMultiOptions(Vendors::$ActualIntervals);
        $this->addElement($element);
    }

    public function addSiteElement()
    {
        $element = new Zend_Form_Element_Text('site');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setAttrib('style', 'height: 200px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addUrlFileElement()
    {
        $view    = Zend_Layout::getMvcInstance()->getView();

        $element = new Zend_Form_Element_Text('url_file');
        $element->setLabel(
            Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].
            $view->ModalLinkBotton(array(
                'routeName'   => 'help-note-view',
                'routeParams' => array('id'=> 1),
                'buttonText'  => '<i class="fa fa-lg fa-question-circle"></i>',
            )) .
            ':'
        );
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('google_drive_key');
        $element->setLabel(
            Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].
            ':'
        );
        $this->addElement($element);
    }

    public function addIsAutoUploadElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_auto_upload');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
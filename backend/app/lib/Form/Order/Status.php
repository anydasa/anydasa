<?

class Form_Order_Status extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptLink('/js/moment.min.js');
        $this->addJavaScriptLink('/js/readable-range.js');

        $this->setStyleText('
            #'.$this->getId().' {width: 900px;}
            #'.$this->getId().' #fieldset-checks .group-element span {width: 180px; display: inline-block;}
            #'.$this->getId().' #background {width: 200px; display: inline-block; margin-right: 10px;}
            #'.$this->getId().' #minutes_limit {width: 200px;}
            #'.$this->getId().' .time { display: inline-block; margin-left: 10px; font-size: 11px; color: Gray; }
        ');


        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find("#background option").each(function () { $(this).css({background: $(this).val() }) })
            $("form#'.$this->getId().'").find("#background").keyup(function () { $(this).css({background: $(this).val() }) }).keyup()
            $("form#'.$this->getId().'").find("#is_send_user_mail").change(function () { $("#mail_subject-label").parent().toggle($(this).prop("checked")); }).change()

            $("form#'.$this->getId().' #minutes_limit").after("<span class=time></span>")
            $("form#'.$this->getId().' #minutes_limit").on("keyup", function () {
                var minutes = $(this).val();
                var m1 = moment(moment().subtract(minutes, "minutes"), "YYYY-MM-DD HH:mm");
                var diff = moment.preciseDiff(m1);
                $("span.time").html( diff );
            }).keyup();
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addTimeLimitElement();
        $this->addOthersElement();

        $this->addSubmitElement();
    }

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Text('id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z0-9]{0,}$/i',
                'messages' => 'Только латинские буквы и цифры'
            )
        );
        $this->addElement($element);
    }

    public function addTimeLimitElement()
    {
        $element = new Zend_Form_Element_Text('minutes_limit');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Int', true, array('messages' => array('notInt' => 'Только числовое значение')));
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addOthersElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_send_user_sms');
        $element->setLabel('Отправлять SMS');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_send_user_mail');
        $element->setLabel('Отправлять Email');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_require_user_msg');
        $element->setLabel('Нужен комментарий');
        $this->addElement($element);

        $this->addDisplayGroup(array('is_send_user_sms', 'is_send_user_mail', 'is_require_user_msg'), 'user', array('legend'=>'Пользователю', 'escape' => false));

        $element = new Zend_Form_Element_Checkbox('is_send_admin_sms');
        $element->setLabel('Отправлять SMS');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_send_admin_mail');
        $element->setLabel('Отправлять Email');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_require_admin_msg');
        $element->setLabel('Нужен комментарий');
        $this->addElement($element);

        $this->addDisplayGroup(array('is_send_admin_sms', 'is_send_admin_mail', 'is_require_admin_msg'), 'admin', array('legend'=>'Админу', 'escape' => false));


        $element = new Zend_Form_Element_Text('mail_subject');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter('Null');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('background');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter('Null');
        $element->setDescription('<a target="_blank" href="http://www.w3schools.com/tags/ref_colorpicker.asp">выбор цвета</a>');
        $this->addElement($element);


    }


}
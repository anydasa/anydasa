<?

class Form_Order_Delivery extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('style', 'min-width: 600px;');

        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' .MultiCheckbox {float: left; width: 100%; background: white; border: 1px solid Gray; width: 300px;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0; padding: 1px 2px; line-height: 12px; font-weight: normal; font-size: 11px; float: left; width: 100%;}
            #'.$this->getId().' .MultiCheckbox label input {margin: 0; float: left;}
        ');

        return parent::render($view);
    }


    public function init()
    {


        $this->addIdElement();
        $this->addTitleElement();
        $this->addCostElement();
        $this->addFreeFromPriceElement();
        $this->addPaymentsElement();
        $this->addIsEnabledElement();
        $this->addSubmitElement();
    }

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Text('id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z0-9]{0,}$/',
                'messages' => 'Только латинские буквы и цифры'
            )
        );
        $this->addElement($element);
    }

    public function addPaymentsElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('Payments');
        $element->setLabel('Оплата:');
        $element->setMultiOptions(Doctrine_Query::create()->from('OrdersPayment')->orderBy('sort')->execute()->toKeyValueArray('id', 'title'));
        $element->setSeparator('');
        $this->addElement($element);
    }



    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('title_in_product');
        $element->setAttrib('style', 'height: 50px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addCostElement()
    {
        $element = new Zend_Form_Element_Text('cost');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);
    }

    public function addFreeFromPriceElement()
    {
        $element = new Zend_Form_Element_Text('free_from_price');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $element->addFilter(new Zend_Filter_Null(Zend_Filter_Null::STRING));
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        parent::populate($values);

        $this->Payments->setValue(array_column($values['Payments'], 'id'));

//        Zend_Debug::dump($values); exit;
    }

}
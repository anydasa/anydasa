<?

class Form_Order_InvoiceMail extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("body_html", { customConfig: "/js/ckeditor_confings/light.js"});
        ');

        $this->save->setLabel('Отправить');

        return parent::render($view);
    }

    public function init()
    {
        $this->addEmailElement();
        $this->addSubEmailsElement();
        $this->addNameElement();
        $this->addSubjectElement();
        $this->addBodyElement();
        $this->addSubmitElement();
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
    public function addSubEmailsElement()
    {
        $element = new Zend_Form_Element_Text('emails');
        $element->setLabel('Доп. Email:');
        $element->setDescription('Если больше 1 доп. email, указывать через ; (точка с запятой)');
        $this->addElement($element);
    }
    public function addSubjectElement()
    {
        $element = new Zend_Form_Element_Text('subject');
        $element->setLabel('Тема:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addBodyElement()
    {
        $element = new Zend_Form_Element_Textarea('body_html');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
}
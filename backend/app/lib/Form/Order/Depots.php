<?

class Form_Order_Depots extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("description", { customConfig: "/js/ckeditor_confings/light.js"});
            $("form#'.$this->getId().' #description-label").next().attr("colspan", 2).prev().remove()
        ');


        $this->setStyleText('
            #'.$this->getId().' { width: 800px;}
            #'.$this->getId().' .group-element {float: left; margin-right: 3px; position: relative;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addDescriptionElement();
        $this->addIsEnabledElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

}
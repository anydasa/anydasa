<?

class Form_Adwords_Campaign extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' { width: 800px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addNameElement();
        $this->addStatusElement();
        $this->addBudgetAmountElement();
        $this->addAdServingOptimizationStatusElement();
        $this->addSubmitElement();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addStatusElement()
    {
        $element = new Zend_Form_Element_Select('status');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOption('', '---');
        $element->addMultiOptions(AdwordsCampaign::getStatusList());
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addAdServingOptimizationStatusElement()
    {
        $element = new Zend_Form_Element_Select('ad_serving_optimization_status');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOption('', '---');
        $element->addMultiOptions(AdwordsCampaign::getOptimizationStatusList());
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addBudgetAmountElement()
    {
        $element = new Zend_Form_Element_Text('budget_amount');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);
    }

}
<?

class Form_Adwords_ForbiddenWord extends Form_Abstract
{
    public function render()
    {
        $this->addSubmitElement();
        
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' { width: 600px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        
        $element = new Zend_Form_Element_Text('key');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
        
        $element = new Zend_Form_Element_Text('replacement');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
        
        
    }

}
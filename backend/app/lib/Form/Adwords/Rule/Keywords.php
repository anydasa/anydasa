<?

class Form_Adwords_Rule_Keywords extends Form_Abstract
{

    function __construct()
    {
        parent::__construct('AdwordsRuleKeywords');
    }

    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( this.checked ) {
                    $(this).closest("label").css({background: "#CADDAC"});
                } else {
                    $(this).closest("label").css({background: "Transparent"});
                }
            }).change()

            $("form#'.$this->getId().'").find("#is_iterative_decrease").on("change", function() {
                $(this).closest("tr").find("#iterative_min_word,#iterative_count").closest(".group-element").toggle(this.checked)

            }).change()
        ');

        $this->setStyleText('
            #'.$this->getId().' { width: 670px;}
            #'.$this->getId().' .MultiCheckbox { background: white; border: 1px solid Gray; width: 100%; padding: 10px 0; float: left;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0 0 1px 0; padding: 1px 10px; line-height: 12px; font-weight: normal; font-size: 11px; float: left; width: 100%;}
            #'.$this->getId().' .MultiCheckbox label input {margin: 0; float: left;}
            #'.$this->getId().' textarea {
                height: 50px;
                resize: none;
                overflow: hidden;
                border-radius:2px;
                padding: 4px;
                line-height: 15px;
            }
            #'.$this->getId().' textarea:focus {
                height: 200px;
                box-shadow: 0 0 3px Green;

            }
            #fieldset-iterative_decrease .group-element {float: left; min-width: 50px; padding: 5px;}
            #fieldset-iterative_decrease .group-element label {font-weight: normal; font-size: 11px;}
            #fieldset-iterative_decrease .group-element input[type=text] {width: 50px; margin-right: 5px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('keys_prepend');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('keys_append');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('keys_negative');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('keys_remove');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('options');
        $element->setLabel('Опции:');
        $element->setSeparator('');
        $element->addMultiOptions([
            'include_without_additives'  => 'Создать вариант без добавочных ключей',
            'remove_cyrillic'            => 'Удалить кирилицу "а-я" в исходной строке',
            'remove_brackets'            => 'Удалить содержимое в скобках "(...)"',
        ]);
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_iterative_decrease');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('iterative_count');
        $element->setLabel('Количество уменьшений:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('iterative_min_word');
        $element->setLabel('Минимально слов:');
        $this->addElement($element);

        $this->addDisplayGroup(array('is_iterative_decrease', 'iterative_min_word', 'iterative_count'), 'iterative_decrease', array('legend'=>'Итеративно уменьшать:'));

        $this->addSubmitElement();
    }

    public function setProducts(Doctrine_Collection $Collection)
    {
        $formDescription = $this->getDescription();
        $formDescription .= '<ul style="font-size: 11px; margin-top: 5px; max-height: 400px; overflow-y: scroll">';
        foreach ($Collection as $Record) {
            $formDescription .= "<li>{$Record->Products->title} <strong>(".count(parse_words($Record->Products->title))." слов; ".mb_strlen($Record->Products->title)." символов)</strong></li>";
        }
        $formDescription .= '</ul>';
        $this->setDescription($formDescription);
    }

}
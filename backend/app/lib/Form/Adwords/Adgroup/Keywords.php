<?

class Form_Adwords_Adgroup_Keywords extends Form_Adwords_Rule_Keywords
{
    public function init()
    {
        parent::init();

        $this->removeElement('title');
        $this->save->setLabel('Сгенерировать');
        $this->setAttrib('data-target', 'blank');

        $this->keys_prepend->setValue('купить');
        $this->keys_append->setValue("купить\nукраина\nкиев\nинтернет-магазин\nпродажа\nцена\nстоимость");

    }
}
<?

class Form_Adwords_Adgroup_Ad extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' { width: 670px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $element = new Zend_Form_Element_Select('id_rule');
        $element->setLabel('Создать ключи из:');
        $element->addMultiOption('', '---');
        $element->addMultiOptions( Doctrine_Query::create()->from('AdwordsRuleKeywords')->orderBy('title')->execute()->toKeyValueArray('id', 'title') );
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('remove_exist');
        $element->setLabel('Удалить существующие:');
        $this->addElement($element);


        $this->addSubmitElement();
    }
}
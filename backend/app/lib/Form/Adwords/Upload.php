<?

class Form_Adwords_Upload extends Form_Abstract
{
    public function init()
    {
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        $this->addFileElement();
        $this->addSubmitElement();
        $this->_setDecorsTable();
    }

    public function addFileElement()
    {
        $element = new Zend_Form_Element_File('file');
        $element->setLabel('Из файла:')
                ->setDescription('Допустимы Excel файлы.');

        $sizeValidator = new Zend_Validate_File_Size(array('max'=>(1024*1024*40)));
        $sizeValidator->setMessage('Максимальный размер файла %max%');
        $element->addValidator($sizeValidator);

        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->save->setLabel('Загрузить');
    }

    protected function _setDecorsTable()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'standart-form')),
            array('Description', array('tag' => 'h1', 'placement' => 'prepend', 'escape' => false)),
            'Form',
        ));

        $this->setElementDecorators(array(
            'ViewHelper',
            array('Description', array('class' => 'descr-element', 'escape' => false)),
            array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
            array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
            array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span class="tooltip" title="Обязательно для заполнения">*</span>')),
            array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
        ));

        $this->getElement('file')->setDecorators(
            array(
                'File',
                array('Description', array('class' => 'descr-element', 'escape' => false)),
                array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
            )
        );

        $this->getElement('save')->setDecorators(array(
            array(
                'decorator' => 'ViewHelper'),
            array(
                'decorator' => array('td' => 'HtmlTag'),
                'options' => array('tag' => 'td', 'colspan' => 2)),
            array(
                'decorator' => array('tr' => 'HtmlTag'),
                'options' => array('tag' => 'tr')),
        ));
    }
}
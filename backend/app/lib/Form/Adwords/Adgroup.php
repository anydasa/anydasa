<?

class Form_Adwords_Adgroup extends Form_Abstract
{
    public function render()
    {

        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' { width: 600px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();

        $element = new Zend_Form_Element_Text('id_product');
        $element->setLabel('ID товара:');
//        $element->setAttrib('readonly', 'readonly');
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('id_camp');
        $element->setLabel('Кампания:');
        $element->addMultiOption('', '---');
        $element->addMultiOptions( Doctrine_Query::create()->from('AdwordsCampaign')->orderBy('name')->execute()->toKeyValueArray('id', 'name') );
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('amount');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('status');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOption('', '---');
        $element->addMultiOptions(AdwordsAdgroup::getStatusList());
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);

    }
}
<?

class Form_Adwords_Ad extends Form_Abstract
{
    public function render()
    {

        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' { width: 600px;}
            #'.$this->getId().' #final_urls {height: 100px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();

        $element = new Zend_Form_Element_Text('id_group');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('readonly', 'readonly');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('headline');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('StringLength', false, array(
            10,
            30,
            'UTF-8',
            'messages' => array(
                'stringLengthTooLong' =>  "Max %max% символов",
                'stringLengthTooShort' =>  "Min %min% символов",
            )
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('description1');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('StringLength', false, array(
            10,
            38,
            'UTF-8',
            'messages' => array(
                'stringLengthTooLong' =>  "Max %max% символов",
                'stringLengthTooShort' =>  "Min %min% символов",
            )
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('description2');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addValidator('StringLength', false, array(
            10,
            38,
            'UTF-8',
            'messages' => array(
                'stringLengthTooLong' =>  "Max %max% символов",
                'stringLengthTooShort' =>  "Min %min% символов",
            )
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('display_url');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('final_urls');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('status');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOption('', '---');
        $element->addMultiOptions(AdwordsAd::getStatusList());
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        $values['final_urls'] = implode("\n", (array)$values['final_urls']);

        return parent::populate($values);
    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);

        $values['final_urls'] = array_filter(array_map(function ($item) { return trim($item); }, explode("\n", $values['final_urls'])));

        return $values;
    }
}
<?

class Form_Adwords_FromProduct extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' { width: 670px;}
        ');


        return parent::render();
    }

    public function init()
    {
        $element = new Zend_Form_Element_Select('id_camp');
        $element->setLabel('Кампания:');
        $element->addMultiOption('', '---');
        $element->addMultiOptions( Doctrine_Query::create()->from('AdwordsCampaign')->orderBy('name')->execute()->toKeyValueArray('id', 'name') );
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('amount');
        $element->setLabel(Doctrine::getTable('AdwordsAdgroup')->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $element->setValue(0.1);
        $this->addElement($element);


        $this->addSubmitElement();
    }
}
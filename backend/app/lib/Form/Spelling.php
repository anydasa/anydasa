<?

class Form_Spelling extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find("#toggle").on("change", function () {
                if ($(this).prop("checked")) {
                   CKEDITOR.replace("value", { customConfig: "/js/ckeditor_confings/basic.js"});
                } else {
                   CKEDITOR.instances.value.destroy();
                }
            });
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addSubmitElement();

        $this->addKeyElement();
        $this->addValueElement();
    }

    public function addKeyElement()
    {
        $element = new Zend_Form_Element_Text('key');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addValidator('Regex', false, array(
                    'pattern' => '/[a-z_]+/i',
                    'messages' => '"'.$element->getLabel().'" должно содержать латинские буквы или знак подчеркивания'
                )
        );

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));


        $this->addElement($element);
    }

	public function addValueElement()
    {
        $element = new Zend_Form_Element_Checkbox('toggle');
        $element->setLabel('Использовать редактор:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('value');
        $element->setAttribs(array('style' => "width: 1000px; height: 300px"));
        $this->addElement($element);
    }

}
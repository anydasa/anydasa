<?

class Form_LandingPage extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setCKFinderBasePath('landing-page');

        $this->addJavaScriptText('
            CKEDITOR.replace("body", { customConfig: "/js/ckeditor_confings/LandingPage.js"});
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();

        $this->addDomainElement();
        $this->addHtmlTitleElement();
        $this->addHtmlDescriptionElement();
        $this->addBodyElement();

        $this->addSubmitElement();
    }

    public function addDomainElement()
    {
        $element = new Zend_Form_Element_Text('domain');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $element->addValidator('Callback', true, array(
            'messages' => 'Не похоже на домен',
            'callback' => function ($value) {
                return (
                    preg_match('/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i', $value) //valid chars check
                    && preg_match('/^.{1,253}$/', $value) //overall length check
                    && preg_match('/^[^\.]{1,63}(\.[^\.]{1,63})+$/', $value)
                );
            }
        ));

        $this->addElement($element);
    }

    public function addHtmlTitleElement()
    {
        $element = new Zend_Form_Element_Text('html_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 950px;"));
        $this->addElement($element);
    }

    public function addHtmlDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('html_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 950px; height: 50px"));
        $this->addElement($element);
    }

    public function addBodyElement()
    {
        $element = new Zend_Form_Element_Textarea('body');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }


}
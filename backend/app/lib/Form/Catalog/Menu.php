<?

class Form_Catalog_Menu extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('$(function(){
            var $form = $("form#CatalogMenu");

            $(".image-preview").append("<div id=\"removeImg\" class=\"fa fa-times\" />")
            $("#removeImg").click(function () {
                $("#image_delete").val(1)
                $(".image-preview").remove()
                $("#image").show()
            })

            if ( $(".image-preview").length ) {
                $("#image").hide()
            }

            $("#has_params").change(function () {
                if ( $(this).is(":checked") ) {
                    $("#url").parents("tr").hide();
                    $form.find(".params").parents("tr").show()
                    if ( $(\'select#id_catalog\').val() ) {
                        $("#filters").parents("tr").show();
                    } else {
                        $("#filters").parents("tr").hide();
                    }
                } else {
                    $("#url").parents("tr").show();
                    $form.find(".params").parents("tr").hide()
                }
            }).change()

            $form.find("#stop").val(1)
            $(\'select#id_catalog\').change(function () {
                $form.find("#stop").val("")
                $form.submit();
            })

        })');

        $this->setStyleText('
            #fieldset-price .group-element {float: left; width: 100px; margin: 0 3px;}
            #fieldset-price span {display: none;}
            .MultiCheckbox {max-height: 200px; overflow-y: scroll; font-size: 11px; width: 300px; margin: 10px;}
            .MultiCheckbox label {font-weight: normal; cursor: pointer; line-height: 20px; margin: 0; vertical-align: middle;}
            .MultiCheckbox label input {position: relative; top: 2px;}
            .image-preview {position: relative; display: inline-block;}
            #removeImg {position: absolute; top: 0; right: 0; cursor: pointer;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addStopElement();

        $this->addIdElement();
        $this->addAliasElement();

        $this->addMetaTitleElement();
        $this->addMetaDescriptionElement();

        $this->addImageElement();
        $this->addTitleElement();
        $this->addAgentElement();
        $this->addUrlElement();
        $this->addSubmitElement();

        $this->addDisplayGroup(array('min', 'max', 'id_currency'), 'price', array('legend' => 'Цена грн:', 'escape'=>false));
    }

    public function addMetaTitleElement()
    {
        $element = new Zend_Form_Element_Text('meta_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null('string'));
        $this->addElement($element);
    }

    public function addMetaDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('meta_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px;');
        $element->addFilter(new Zend_Filter_Null('string'));
        $this->addElement($element);
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setValueDisabled(true); // отключение авто receive

        $element->setLabel('Картинка:');

        $Validator = new Zend_Validate_File_MimeType(array('image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Допустим только png формат');
        $element->addValidator($Validator);

        $Validator = new Zend_Validate_File_IsImage(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function addStopElement()
    {
        $element = new Zend_Form_Element_Hidden('stop');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', false);
        $element->setValue(1);
        $this->addElement($element);
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $element->addFilter(new Zend_Filter_Callback('trim'));
        $this->addElement($element);
    }

    public function addAgentElement()
    {
        $element = new Zend_Form_Element_Checkbox('has_params');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('min');
        $element->setAttrib('placeholder', 'от');
        $element->setAttrib('class', 'params');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('max');
        $element->setAttrib('placeholder', 'до');
        $element->setAttrib('class', 'params');
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('id_catalog');
        $element->setLabel('Каталог:');
        $element->setAttrib('class', 'params');
        $element->addMultiOptions([''=>'---'] + Doctrine_Query::create()->from('Catalog')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $this->addElement($element);

        $element = new Zend_Form_Element_Multiselect('filters');
        $element->setLabel('Фильтры:');
        $element->setAttrib('class', 'params');
        $element->setRegisterInArrayValidator(false);
        $element->setAttrib('style', 'height: 300px; width: 300px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('stickers');
        $element->setLabel('Стикеры:');
        $element->setAttrib('class', 'params');
        $element->addMultiOptions(Doctrine::getTable('ProductsStickers')->findAll()->toKeyValueArray('id', 'title'));
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('brands');
        $element->setAttrib('class', 'params');
        $element->setLabel('Бренды:');
        $element->addMultiOptions(Doctrine_Query::create()->from('ProductsBrands')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttrib('style', 'width: 700px;');
        $element->addFilter(new Zend_Filter_Callback('trim'));
        $this->addElement($element);
    }

    public function addUrlElement()
    {
        $element = new Zend_Form_Element_Text('url');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 700px;');

        $this->addElement($element);
    }


    public function populate(array $values)
    {
        parent::populate($values);

        if ( isset($values['id']) ) {
            $Node = Doctrine::getTable($this->modelName)->find($values['id']);

            if ( $url = $Node->getImageUrl() ) {
                $this->getElement('image')->setDescription('<img style="max-height: 200px; max-width: 400px;" src="'.$url.'">');
            }
        }

        $this->_populateFilter($values);
    }

    protected function _setDecorsTable()
    {
        parent::_setDecorsTable();

        $this->getElement('image')->setDecorators(
            array(
                'File',
                array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                array('Description', array('class' => 'image-preview', 'escape' => false)),
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
            )
        );
    }

    public function isValid($data)
    {
        $this->_populateFilter($data);

        $isValid = parent::isValid($data);

        if ( !empty($data['has_params']) ) {
            if ( empty($data['filters']) && empty($data['stickers']) && empty($data['brands']) && empty($data['id_catalog']) && empty($data['min']) && empty($data['max']) ) {
                $this->getElement('has_params')->addError('Нужно указать хоть 1 параметр');
                $isValid = false;
            }
        }

        return $isValid;
    }

    public function getValues()
    {
        $values = parent::getValues();

        foreach ($values as & $val) {
            if ( empty($val) ) {
                $val = NULL;
            }
        }

        if ( empty($values['has_params']) ) {
            $values['filters']      = NULL;
            $values['stickers']     = NULL;
            $values['brands']       = NULL;
            $values['id_catalog']   = NULL;
            $values['min']          = NULL;
            $values['max']          = NULL;
        } else {
            $values['url']          = NULL;
        }

        return $values;
    }

    private function _populateFilter($values)
    {
        if ( !empty($values['id_catalog']) ) {
            $FilterElement  = $this->getElement('filters');
            $Filters        = Doctrine_Query::create()->from('CatalogFilterGroup')->where('id_catalog = ?', $values['id_catalog'])->orderBy('sort')->execute();

            $array = [];
            foreach ($Filters as $Filter) {
                $Values = Doctrine_Query::create()->from('CatalogFilterValue')->where('id_group = ?', $Filter->id)->orderBy('sort')->execute();
                foreach ($Values as $Value) {
                    $array[$Filter->title][$Value->id] = $Value->title;
                }
            }
            $FilterElement->addMultiOptions($array);
        }
    }
}
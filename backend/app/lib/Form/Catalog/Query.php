<?

class Form_Catalog_Query extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( $(this).prop("checked") ) {
                    $(this).closest("label").addClass("selected");
                    $(this).closest("tr").find(">td").prepend(
                        $("<div />")
                            .attr("id", "l"+$(this).attr("id"))
                            .css({fontWeight:"normal", color: "Red", fontSize: "11px", lineHeight: "12px"})
                            .html($(this).parent().text())
                    )
                } else {
                    $("#l"+$(this).attr("id")).remove();
                    $(this).closest("label").removeClass("selected");
                }
            }).change()

            $("form#'.$this->getId().' .MultiCheckbox").filterMultiCheckbox();

            var filters = $.parseJSON( $("form#'.$this->getId().'").attr("data-filters") );

            $("form#'.$this->getId().'").find("select#id_catalog").change(function () {
                $("form#'.$this->getId().' #filters label").hide();

                if ( undefined != filters[$(this).val()] ) {
                    $.each(filters[$(this).val()], function(id_catalog, id_filter) {
                        $("form#'.$this->getId().' #filters-"+id_filter).parent().show()
                    })
                }
            }).change();

        ');



        $this->setStyleText('
            .MultiCheckbox {height: 150px; overflow-y: scroll; font-size: 11px; width: 600px;}
            .MultiCheckbox label {font-weight: normal; cursor: pointer; line-height: 20px; margin: 0; vertical-align: middle; display: block;}
            .MultiCheckbox label.selected,
            .MultiCheckbox label:hover {background: #0055aa; color: White;}
            .MultiCheckbox label input {position: relative; top: 2px; margin: 0; padding: 0;}
            #fieldset-price td .group-element {display: inline-block;}
            #fieldset-price td {vertical-align: top;}
            #fieldset-price td span {display:none;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();

        $this->addTitleElement();
        $this->addDescriptionElement();
        $this->addTypeElement();
        $this->addCatalogElement();
        $this->addBrandsElement();
        $this->addStickersElement();
        $this->addFiltersElement();
        $this->addPriceElement();

        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Select('type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions([
            '' => '----',
            'Каталог-фильтр' => 'Каталог-фильтр'
        ]);
        $this->addElement($element);
    }

    public function addCatalogElement()
    {
        $element = new Zend_Form_Element_Select('id_catalog');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(
            ['' => '----'] +
            Doctrine_Query::create()->from('Catalog')->orderBy('title')->execute()->toKeyValueArray('id', 'title')
        );
        $this->addElement($element);
    }

    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px;');
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addBrandsElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('brands');
        $element->setLabel('Бренды:');
        $element->setSeparator('');
        $element->setAttrib("escape", false);
        $element->addMultiOptions(Doctrine_Query::create()->from('ProductsBrands')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addStickersElement()
    {
        $element = new Zend_Form_Element_MultiCheckbox('stickers');
        $element->setLabel('Стикеры:');
        $element->setSeparator('');
        $element->setAttrib("escape", false);
        $element->addMultiOptions(Doctrine_Query::create()->from('ProductsStickers')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addFiltersElement($id_catalog)
    {
        $element = new Zend_Form_Element_MultiCheckbox('filters');
        $element->setLabel('Фильтры:');
        $element->setSeparator('');
        $element->setAttrib("escape", false);
        $element->addFilter('Null');

        $sql = 'SELECT t1.id id_group, t1.title title_group, t2.id id_value, t2.title title_value
                FROM catalog_filter_group t1, catalog_filter_value t2
                WHERE t1.id = t2.id_group
                  AND t1.id_catalog = :id_catalog
                ORDER BY t1.sort, t2.sort';
        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->bindValue('id_catalog', $id_catalog);
        $stmt->execute();

        foreach ($stmt->fetchAll(Doctrine_Core::FETCH_ASSOC) as $item) {
            $element->addMultiOption($item['id_value'], '<strong>' . $item['title_group'] . '</strong> - ' . $item['title_value']);
        }

        $this->addElement($element);
    }

    public function addPriceElement()
    {
        $element = new Zend_Form_Element_Text('price_min');
        $element->addFilter('Null');
        $element->setAttrib('placeholder', 'от');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('price_max');
        $element->addFilter('Null');
        $element->setAttrib('placeholder', 'до');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('id_currency');
        $element->addFilter('Null');
        $element->addMultiOptions(
            ['' => '----'] +
            Doctrine_Query::create()->from('Currency')->orderBy('sort')->execute()->toKeyValueArray('id', 'title')
        );
        $this->addElement($element);

        $this->addDisplayGroup(array('price_min', 'price_max', 'id_currency'), 'price', array('legend' => 'Цена:', 'escape'=>false));
    }


}
<?

class Form_Catalog_Filter_Index extends Form_Abstract
{
    private Catalog $Catalog;

    public function __construct($formName, Catalog $Catalog, $options = null)
    {
        $this->Catalog = $Catalog;
        parent::__construct($formName, $options);
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("text", { customConfig: "/js/ckeditor_confings/FilterIndex.js"});
        ');

        $this->addJavaScriptText('
                $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( $(this).prop("checked") ) {
                    $(this).closest("label").addClass("selected");
                    $(this).closest("tr").find(">td").prepend(
                        $("<div />")
                            .attr("id", "l"+$(this).attr("id"))
                            .css({fontWeight:"normal", color: "Red", fontSize: "11px", lineHeight: "12px"})
                            .html($(this).parent().text())
                    )
                } else {
                    $("#l"+$(this).attr("id")).remove();
                    $(this).closest("label").removeClass("selected");
                }
            }).change()
            $("form#'.$this->getId().' .MultiCheckbox").filterMultiCheckbox();
        ');

        $this->setStyleText('
            #'.$this->getId().' .group-element {float: left; margin-right: 3px;}

            .MultiCheckbox {height: 150px; overflow-y: scroll; font-size: 11px; width: 600px;}
            .MultiCheckbox label {font-weight: normal; cursor: pointer; line-height: 20px; margin: 0; vertical-align: middle; display: block;}
            .MultiCheckbox label.selected,
            .MultiCheckbox label:hover {background: #0055aa; color: White;}
            .MultiCheckbox label input {position: relative; top: 2px; margin: 0; padding: 0;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addCatalogElement();
        $this->addAliasElement();
        $this->addTagElement();
        $this->addMetaTitleElement();
        $this->addMetaDescriptionElement();
        $this->addMetaKeywordsElement();
        $this->addH1Element();
        $this->addFiltersElement();
        $this->addBrandsElement();
        $this->addStickersElement();
        $this->addTextElement();
        $this->addIsEnabledElement();

        $this->addSubmitElement();
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addCatalogElement()
    {
        $element = new Zend_Form_Element_Hidden('id_catalog');
        $element->setValue($this->Catalog->id);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setAttrib('slug', 'slug');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');

        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z0-9_-]{0,}$/',
                'messages' => 'Только латинские буквы, цифры, "-", "_"'
            )
        );

// TODO bug validate. __construct has been overload

//        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
//            ->addValidator('NoDbRecordExists', true, array(
//                $this->modelName,
//                $element->getName(),
//                'id',
//                'id_catalog',
//                'messages' => array(
//                    'dbRecordExists' => 'Уже есть в базе.'
//                )));

        $this->addElement($element);
    }

    public function addTagElement()
    {
        $element = new Zend_Form_Element_Text('tag_name');
        $element->setLabel('Название:');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('weight');
        $element->setLabel('Вес:');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $this->addDisplayGroup(array('tag_name', 'weight'), 'additional', array('legend'=>'Тег:'));
    }
    public function addMetaTitleElement()
    {
        $element = new Zend_Form_Element_Text('meta_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addMetaKeywordsElement()
    {
        $element = new Zend_Form_Element_Text('meta_keywords');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addH1Element()
    {
        $element = new Zend_Form_Element_Text('h1');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addMetaDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('meta_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px;');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addFiltersElement()
    {
        $sql = 'SELECT t1.id id_group, t1.title title_group, t2.id id_value, t2.title title_value
                FROM catalog_filter_group t1, catalog_filter_value t2
                WHERE t1.id = t2.id_group
                  AND t1.id_catalog = :id_catalog
                ORDER BY t1.sort, t2.sort';
        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->bindValue('id_catalog', $this->Catalog->id);
        $stmt->execute();

        $options = [];
        foreach ($stmt->fetchAll(Doctrine_Core::FETCH_ASSOC) as $item) {
            $options[$item['id_value']] = '<strong>' . $item['title_group'] . '</strong> - ' . $item['title_value'];
        }

        $element = new Zend_Form_Element_MultiCheckbox('filters');
        $element->setLabel('Фильтры:');
        $element->setSeparator('');
        $element->setAttrib("escape", false);
        $element->addFilter('Null');
        $element->setMultiOptions($options);
        $this->addElement($element);
    }

    public function addBrandsElement()
    {
        $brands = Doctrine_Query::create()
            ->from('ProductsBrands b')
            ->innerJoin('b.Products p')
            ->where('? = ANY(p.catalogs)', $this->Catalog->id)
            ->orderBy('title')
            ->execute()
            ->toKeyValueArray('id', 'title');

        $element = new Zend_Form_Element_MultiCheckbox('brands');
        $element->setLabel('Бренды:');
        $element->setSeparator('');
        $element->setAttrib("escape", false);
        $element->addMultiOptions($brands);
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addStickersElement()
    {
        $options = Doctrine_Query::create()
            ->from('ProductsStickers s')
            ->innerJoin('s.ProductsRefStickers prs')
            ->innerJoin('prs.Products p')
            ->where('? = ANY(p.catalogs)', $this->Catalog->id)
            ->orderBy('title')
            ->execute()
            ->toKeyValueArray('id', 'title');

        $element = new Zend_Form_Element_MultiCheckbox('stickers');
        $element->setLabel('Стикеры:');
        $element->setSeparator('');
        $element->setAttrib("escape", false);
        $element->addMultiOptions($options);
        $element->addFilter('Null');
        $this->addElement($element);
    }
}

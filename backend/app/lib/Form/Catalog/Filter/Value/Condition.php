<?

class Form_Catalog_Filter_Value_Condition extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addStyleText('
            .ui-helper-hidden-accessible {display: none;}
            .ui-autocomplete {
              position: absolute;
              top: 0;
              left: 0;
              cursor: default;
              background: white;
              border: 1px solid #EEEEEE;
              border-top: 0;
              box-shadow: 0 2px 5px #000;
            }
            .ui-autocomplete, .ui-autocomplete > li {
              list-style: none;
              margin: 0;
              padding: 0;
            }
            .ui-autocomplete a {
                text-decoration: none;
            }
            .ui-menu-item {
              padding: 2px 5px !important;
              border-bottom: 1px dotted #CCC;
            }
            .ui-menu-item:hover {
              cursor: pointer;
              background: #EEEEEE;
            }
            .ui-menu-item:last-child {
              border: 0;
            }
        ');

        $this->addJavaScriptText('$(function () {
            var presets = $.parseJSON($("form#'.$this->getId().'").attr("data-presets"));

            $("form#'.$this->getId().' select#id_option").on("change", function () {
                var id_option = $(this).val()
                var $element = $("input#param");
                $element.autocomplete({
                    source: presets[id_option],
                    appendTo: $element.parent(),
                    minLength: 0
                }).focusin(function() {
                    $(this).autocomplete("search");
                });
            }).change();

            $("form#'.$this->getId().' select#condition").on("change", function () {
                var $element = $("input#param");

                $element.rules("remove", "number");
                $element.rules("remove", "regex");

                if ( $(this).val() == "numeric_from" || $(this).val() == "numeric_to" ) {
                    $element.rules("add", {
                        number: true,
                        messages: {
                            number: "Должно быть число"
                        }
                    });
                } else if ($(this).val() == "numeric_between") {
                    $element.rules("add", {
                        regex: /^[0-9|\.]+-[0-9|\.]+$/,
                        messages: {
                            regex: "1000-2000"
                        }
                    });
                }

            }).change();

        });');

        $this->setDescription($this->view->ModalLinkHelp(7) . $this->getDescription());

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdValueElement();
        $this->addIdOptionElement();
        $this->addConditionElement();
        $this->addParamElement();
        $this->addSubmitElement();
    }

    public function addIdValueElement()
    {
        $element = new Zend_Form_Element_Hidden('id_value');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addIdOptionElement()
    {
        $element = new Zend_Form_Element_Select('id_option');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addConditionElement()
    {
        $element = new Zend_Form_Element_Select('condition');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions([
            ''                  => '---',
            'numeric_from'      => 'От (число включительно)',
            'numeric_to'        => 'До (число включительно)',
            'numeric_between'   => 'Между (число включительно)',
            'string'            => 'Строка'
        ]);
        $this->addElement($element);
    }
    public function addParamElement()
    {
        $element = new Zend_Form_Element_Text('param');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function populate($values)
    {
        return parent::populate($values);
    }
}
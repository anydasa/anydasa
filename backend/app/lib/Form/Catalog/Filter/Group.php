<?

class Form_Catalog_Filter_Group extends Form_Abstract
{
    public function init()
    {
        $this->addCatalogElement();
        $this->addTitleElement();
//        $this->addPrependToValueElement();
//        $this->addIsCopyElement();
        $this->addIsOpenElement();

        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addCatalogElement()
    {
        $element = new Zend_Form_Element_Hidden('id_catalog');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $this->addElement($element);
    }

    public function addIsCopyElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_copy_option');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addIsOpenElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_open');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttrib('style', 'width: 200px;');
        $this->addElement($element);
    }
    public function addPrependToValueElement()
    {
        $element = new Zend_Form_Element_Text('prepend_to_value');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'width: 50px;');


        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z]{0,5}$/',
                'messages' => 'Не полее пяти латинских букв'
            )
        );
        $element->addFilter(new Zend_Filter_Null);

        $this->addElement($element);
    }
}
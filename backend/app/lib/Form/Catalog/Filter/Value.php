<?

class Form_Catalog_Filter_Value extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {width: 600px;}
            #'.$this->getId().' p.hint {font-size: 11px; color: red; margin:0;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdGroupElement();
        $this->addIdParentElement();
        $this->addAliasElement();
        $this->addTitleElement();
        $this->addSubmitElement();
    }

    public function addIdGroupElement()
    {
        $element = new Zend_Form_Element_Hidden('id_group');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addIdParentElement()
    {
        $element = new Zend_Form_Element_Select('id_parent');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOption('', '---');
        $element->addFilter('Null');
        $this->addElement($element);
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Callback', true, array(
            'messages' => 'Запрещенные символы "'.Products_Filter_Url::SEPARATOR_FILTER_CATEGORY.'" и "'.Products_Filter_Url::SEPARATOR_FILTER_ITEM.'"',
            'callback'   => function ($value) { return !preg_match('/['.Products_Filter_Url::SEPARATOR_FILTER_CATEGORY.Products_Filter_Url::SEPARATOR_FILTER_ITEM.']/', $value); }
        ));
        $element->setDescription('Изменять можно только 1 раз!');
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setDescription('Нельзя изменять по смыслу фильтр! Но если нужно, можно удалить и создать новый.');
        $this->addElement($element);
    }
}
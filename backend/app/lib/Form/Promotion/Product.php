<?

class Form_Promotion_Product extends Zend_Form_SubForm
{
    private $modelName;

    public function __construct($modelName, $options = null)
    {
        $this->modelName = 'PromotionsProducts';
        parent::__construct('PromotionsProducts'.$modelName, $options);
    }

    public function init()
    {
        $this->addElement(new Zend_Form_Element_Hidden('id'));
        $this->addCodeElement();
        $this->addTitleElement();
        $this->addPriceElement();
        $this->addCountElement();
        $this->addRemoveElement();
    }

    public function addCountElement()
    {
        $element = new Zend_Form_Element_Text('count');
        $element->setAttrib('style', 'width: 50px;');
        $element->setAttrib('data-name', $element->getName());
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Поле "'.$element->getLabel().'" обязательное для заполнения')));
        $element->addValidator('Int', true, array('messages' => array('notInt'=>'Поле "'.$element->getLabel().'" может сожержать только числовое значение')));
        $this->addElement($element);
    }
    public function addPriceElement()
    {
        $element = new Zend_Form_Element_Text('price');
        $element->setAttrib('style', 'width: 50px;');
        $element->setAttrib('data-name', $element->getName());
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Поле "'.$element->getLabel().'" обязательное для заполнения')));
        $element->addValidator('Float', true, array('locale' => 'en_US', 'messages' => array('notFloat' => 'Поле "'.$element->getLabel().'" может сожержать только числовое значение')));
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Glitch_Form_Element_Text('title');
        $element->setAttrib('readonly', 'readonly');
        $element->setAttrib('data-name', $element->getName());
        $element->setAttrib('style', 'width: 520px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);
    }

    public function addCodeElement()
    {
        $element = new Glitch_Form_Element_Text('code');
        $element->setAttrib('style', 'width: 50px;');
        $element->setAttrib('readonly', 'readonly');
        $element->setAttrib('data-name', $element->getName());
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);
    }

    public function addRemoveElement()
    {
        $element = new Zend_Form_Element_Button('remove');
        $element->setLabel('Удалить');
        $element->setAttrib('data-name', $element->getName());
        $this->addElement($element);
    }
}
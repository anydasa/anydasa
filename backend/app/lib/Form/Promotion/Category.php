<?

class Form_Promotion_Category extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {width: 900px;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();

        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

}
<?

class Form_Promotion_Type extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {width: 900px;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();

        $this->addSubmitElement();
    }

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Text('id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z0-9]{0,}$/',
                'messages' => 'Только латинские буквы и цифры'
            )
        );
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

}
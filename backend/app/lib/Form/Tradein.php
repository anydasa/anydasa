<?

class Form_Tradein extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addSubmitElement();

        $this->addStatusElement();
        $this->addValueElement();
    }

	public function addValueElement()
    {
        $element = new Zend_Form_Element_Textarea('admin_comment');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 400px; height: 200px"));
        $this->addElement($element);
    }

    public function addStatusElement()
    {
        $element = new Zend_Form_Element_Select('status');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setMultiOptions(['' => '---'] + Tradein::$STATUS_LIST);
        $this->addElement($element);
    }


}
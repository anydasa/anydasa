<?

class Form_Feedback extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addIsLookedElement();
        $this->addNameElement();
        $this->addEmailElement();
        $this->addMessageElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

	public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addMessageElement()
    {
        $element = new Zend_Form_Element_Textarea('message');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setAttribs(array('style' => "width: 950px; height: 300px"));
        $this->addElement($element);
    }

    public function addIsLookedElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_looked');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
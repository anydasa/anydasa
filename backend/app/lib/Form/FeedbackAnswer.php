<?

class Form_FeedbackAnswer extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("message", { customConfig: "/js/ckeditor_confings/MailTemplate.js"});
        ');

        $this->save->setLabel('Отправить');

        return parent::render($view);
    }

    public function init()
    {
        $this->addMessageElement();
        $this->addSubmitElement();
    }


    public function addMessageElement()
    {
        $element = new Zend_Form_Element_Textarea('message');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
}
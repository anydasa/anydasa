<?

class Form_Banner_Group extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {
                width: 400px;
            }
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addPlaceElement();
        $this->addUrlElement();
        $this->addPriorityElement();
        $this->addIsEnabledElement();
        $this->addSubmitElement();
    }

    public function addPlaceElement()
    {
        $element = new Zend_Form_Element_Select('id_place');
        $element->setLabel('Место:');
        $element->addMultiOption('', '----');
        $element->addMultiOptions(Doctrine_Query::create()->from('BannersPlaces')->execute()->toKeyValueArray('id', 'title'));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addUrlElement()
    {
        $element = new Zend_Form_Element_Text('url_destination');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addPriorityElement()
    {
        $element = new Zend_Form_Element_Text('priority');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addValidator('Int', true, array('messages' => array('notInt' => 'Только числовое значение')));
        $element->addFilter('Null');
        $this->addElement($element);
    }
}
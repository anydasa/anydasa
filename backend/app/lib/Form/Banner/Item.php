<?

class Form_Banner_Item extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->setAttrib('style', 'width: 800px;');

        $this->addJavaScriptText('
            CKEDITOR.replace("text", { customConfig: "/js/ckeditor_confings/light.js"});

            $(".image-preview").append("<div id=\"removeImg\" class=\"fa fa-times\" />")
            $("#removeImg").click(function () {
                $("#image_delete").val(1)
                $(".image-preview").remove()
                $("#image").show()
            })

            if ( $(".image-preview").length ) {
                $("#image").hide()
            }
        ');


        $this->setStyleText('
            #'.$this->getId().' .image-preview {position: relative; display: inline-block;}
            #'.$this->getId().' #removeImg {position: absolute; top: 0; right: 0; cursor: pointer; background: white;}
            #'.$this->getId().' .group-element {float: left; margin-right: 3px;}
            #'.$this->getId().' .group-element span {display:none;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addGroupElement();
        $this->addTitleElement();
        $this->addUrlElement();
        $this->addImageElement();
        $this->addImageElement();
        $this->addIsEnabledElement();
        $this->addTextElement();
        $this->addPromotionElement();
        $this->addBannerElement();
        $this->addSubmitElement();

    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addGroupElement()
    {
        $element = new Zend_Form_Element_Hidden('id_group');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addUrlElement()
    {
        $element = new Zend_Form_Element_Text('url');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addPromotionElement()
    {
        $element = new Zend_Form_Element_Multiselect('promotions');
        $element->setLabel('Акции:');
        $element->setAttrib('class', 'params');
        $element->setRegisterInArrayValidator(false);
        $element->addMultiOptions([
            Promotions::getList()->toKeyValueArray('id', 'title' )
        ]);
        $element->setAttrib('style', 'height: 300px; width: 300px;');
        $this->addElement($element);
    }

    public function addBannerElement()
    {
        $element = new Zend_Form_Element_Multiselect('banners');
        $element->setLabel('Баннеры:');
        $element->setAttrib('class', 'params');
        $element->setRegisterInArrayValidator(false);

        $multiOptions = Doctrine_Query::create()->from('Banners');

        if(Zend_Controller_Front::getInstance()->getRequest()->getParam('id')){
            $multiOptions->where('id != ?', Zend_Controller_Front::getInstance()->getRequest()->getParam('id'));
        }

        $multiOptions = $multiOptions->execute()->toKeyValueArray('id', 'title' );

        $element->addMultiOptions([$multiOptions]);
        $element->setAttrib('style', 'height: 300px; width: 300px;');
        $this->addElement($element);
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setValueDisabled(true); // отключение авто receive

        $element->setLabel('Картинка:');

        $Validator = new Zend_Validate_File_MimeType(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Допустим только jpeg,gif,png формат');
        $element->addValidator($Validator);

        $Validator = new Zend_Validate_File_IsImage(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        parent::populate($values);

        if ( isset($values['id']) ) {
            $Node = Doctrine::getTable($this->modelName)->find($values['id']);

            if ( $url = $Node->getImageUrl() ) {
                $this->getElement('image')->setDescription('<img style="max-height: 200px; max-width: 200px;" src="'.$url.'">');
            }
        }
    }

    protected function _setDecorsTable()
    {
        parent::_setDecorsTable();

        $this->getElement('image')->setDecorators(
            array(
                'File',
                array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                array('Description', array('class' => 'image-preview', 'escape' => false)),
                array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                array('Label', array('tag' => 'th')),
                array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
            )
        );
    }

}
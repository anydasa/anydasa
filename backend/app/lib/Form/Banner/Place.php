<?

class Form_Banner_Place extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->setStyleText('
            #'.$this->getId().' {
                width: 400px;
            }
            #'.$this->getId().' .group-element {float: left; margin-right: 3px;}
            #'.$this->getId().' .group-element span {display:none;}
            #'.$this->getId().' #fieldset-dimension input {width: 50px;}
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addCodeElement();
        $this->addTitleElement();
        $this->addImgParamsElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $this->addElement($element);
    }

    public function addCodeElement()
    {
        $element = new Zend_Form_Element_Text('code');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
            ->addValidator('NoDbRecordExists', true, array(
                $this->modelName,
                $element->getName(),
                'id',
                'messages' => array(
                    'dbRecordExists' => 'Уже есть в базе.'
                )));

        $this->addElement($element);
    }

    public function addImgParamsElement()
    {
        $element = new Zend_Form_Element_Text('width');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('placeholder', 'ширина');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Int', true, array('messages' => array('notInt'=>'Не число')));
        $element->addValidator('LessThan', true, array(
            'messages' => ['notLessThan' => "допустимо менее '%max%'"],
            'max' => 1401,
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('height');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('placeholder', 'высота');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addValidator('Int', true, array('messages' => array('notInt'=>'Не число')));
        $element->addValidator('LessThan', true, array(
            'messages' => ['notLessThan' => "допустимо менее '%max%'"],
            'max' => 1001,
        ));
        $this->addElement($element);

        $this->addDisplayGroup(array('width', 'height'), 'dimension', array('legend' => 'Размер', 'escape'=>false));
    }
}
<?

class Form_Vendor_CreateFromSelected extends Form_Abstract
{

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->addSubmitElement();
        $this->_setDecorsTable();

        $this->addJavaScriptText(
            '
                $("#selected-list li").each(function () {
                    var title = $(this).find(\'span\').text();
                    $(this).attr(\'data-title\', title)
                    $(this).find(\'input\').attr(\'value\', title)
                })

                $(\'#replace_from, #replace_to, #append, #prepend\').on(\'keyup\', function () {
                    $("#selected-list li").each(function () {
                        var new_title = $("#prepend").val() +
                            $(this).attr(\'data-title\').replace($("#replace_from").val(), $("#replace_to").val()) +
                            $("#append").val();

                        $(this).find(\'span\').text(new_title)
                        $(this).find(\'input\').attr(\'value\', new_title)
                    })
                })
             ');

        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find(".MultiCheckbox input").on("change", function() {
                if ( this.checked ) {
                    $(this).parents("tr").find(">th").append(
                        $("<div />")
                            .attr("id", "l"+$(this).attr("id"))
                            .css({fontWeight:"normal", color: "Green", fontSize: "11px", lineHeight: "12px"})
                            .html($(this).parent().text())
                    )
                } else {
                    $("#l"+$(this).attr("id")).remove()
                }
            }).change()
        ');

        $this->addJavaScriptText('filterMultiCheckbox(\'form#VendorsProducts #filter_catalogs\', \'form#VendorsProducts #id_catalog\')');

        $this->setStyleText('
            #'.$this->getId().' .group-element {float: left; width: 150px; margin: 0 3px;}
            #'.$this->getId().' .group-element>span {display: none;}
            #'.$this->getId().' #id_brand {float: left; width: 120px;}
            #'.$this->getId().' #id_brand+p {float: left;}
            #'.$this->getId().' #id_brand option:first-letter {font-weight: bold;}
            #'.$this->getId().' .MultiCheckbox {max-height: 300px; overflow-y: scroll; background: white; border: 1px solid Gray; width: 300px;}
            #'.$this->getId().' .MultiCheckbox label {display: block; margin:0; padding: 1px 2px; line-height: 12px; font-weight: normal; font-size: 11px; float: left; width: 100%;}
            #'.$this->getId().' .MultiCheckbox label input {margin: 0; float: left;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $element = new Zend_Form_Element_Text('prepend');
        $element->setAttrib('placeholder', 'в начало');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('append');
        $element->setAttrib('placeholder', 'в конец');
        $this->addElement($element);

        $this->addDisplayGroup(array('prepend', 'append'), 'concat', array('legend' => 'Добавить', 'escape'=>false));

        $element = new Zend_Form_Element_Text('replace_from');
        $element->setAttrib('placeholder', 'с');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('replace_to');
        $element->setAttrib('placeholder', 'на');
        $this->addElement($element);

        $this->addDisplayGroup(array('replace_from', 'replace_to'), 'replace', array('legend' => 'Заменить:', 'escape'=>false));

        $element = new Zend_Form_Element_Select('id_brand');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->setDescription(Zend_Layout::getMvcInstance()->getView()->ModalLinkBotton(array(
            'routeName'   => 'products-brand-add',
            'buttonText'  => '<i class="fa fa-plus"></i>',
            'queryParams' => array('redirect'=>'here')
        )));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addMultiOptions(
            array('' => '---') +
            Doctrine_Query::create()->from('ProductsBrands')->orderBy('title')->execute()->toKeyValueArray('id', 'title')
        );
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('id_state');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(
            Doctrine_Query::create()->from('ProductsState')->orderBy('sort')->execute()->toKeyValueArray('id', 'title')
        );
        $element->addFilter('Null');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('type');
        $element->setLabel(Doctrine::getTable('Products')->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('filter_catalogs');
        $element->setLabel('Фильтр:');
        $this->addElement($element);

        $element = new Zend_Form_Element_MultiCheckbox('id_catalog');
        $element->setLabel('Каталог:');
        $element->setSeparator('');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное для заполнения')));
        $element->addMultiOptions(Doctrine_Query::create()->from('Catalog')->orderBy('title')->execute()->toKeyValueArray('id', 'title'));
        $this->addElement($element);

    }
}
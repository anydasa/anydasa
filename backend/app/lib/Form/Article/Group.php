<?

class Form_Article_Group extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->addSubmitElement();

        $this->_setDecorsTable();

        return parent::render($view);
    }

    public function init()
        {
        $this->addIdElement();

        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }


}
<?

class Form_Article_Item extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {

        $this->addSubmitElement();

        $ckFinderUrl = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Products&responseType=json';

        $this->addJavaScriptText("
            $(function () {
                CKEDITOR.replace( 'text', {
                    language: 'ru',
                    uiColor: '#AADC6E',
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserUploadUrl: '$ckFinderUrl',
                    filebrowserWindowWidth: '1000',
                    filebrowserWindowHeight: '700'
                });
            });
        ");

        $this->addJavaScriptText('
         
            $(function () {
                $(".image-preview").append("<div id=\"removeImg\" class=\"fa fa-times\" />")
                $("#removeImg").click(function () {
                    $("#image_delete").val(1)
                    $(".image-preview").remove()
                    $("#image").show()
                })

                if ( $(".image-preview").length ) {
                    $("#image").hide()
                }
            });
        ');
        $this->addJavaScriptLink('/js/moment-with-locales.js');
        $this->addJavaScriptLink('/js/bootstrap-datetimepicker.min.js');
        $this->addJavaScriptText(
            " $(function () {
                $('#date_publication').datetimepicker({
                    format: 'YYYY-MM-DD H:mm',
                    locale: 'ru'
                });
            });"
        );

        $this->setStyleText('
            .image-preview {position: relative; display: inline-block;}
            .image-preview img {max-width: 200px; max-height: 200px;}
            #removeImg {position: absolute; top: 0; right: 0; cursor: pointer;}
        ');
        $this->addStyleLink('/css/bootstrap/bootstrap-datetimepicker.min.css');

        $this->_setDecorsTable();

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addGroupElement();
        $this->addAliasElement();
        $this->addTitleElement();
        $this->addDatePublicationElement();
        $this->addImageElement();
        $this->addMetaElements();
        $this->addIsEnabledElement();
        $this->addShortTextElement();
        $this->addTextElement();
    }

    public function addAliasElement()
    {
        $element = new Zend_Form_Element_Text('alias');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->setAttrib('slug', 'slug');
        $element->addValidator('Regex', false, array(
                'pattern' => '/^[a-z0-9_-]{0,}$/',
                'messages' => 'Только латинские буквы, цифры, "-", "_"'
            )
        );

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }

    public function addGroupElement()
    {
        $element = new Zend_Form_Element_Select('id_group');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array('' => '---'));
        $element->addMultiOptions(Doctrine::getTable('ArticleGroup')->findAll()->toKeyValueArray('id', 'title'));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

	public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addDatePublicationElement()
    {
        $element = new Zend_Form_Element_Text('date_publication');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }


    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setValueDisabled(true); // отключение авто receive

        $element->setLabel('Картинка:');

        $Validator = new Zend_Validate_File_IsImage(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addShortTextElement()
    {
        $element = new Zend_Form_Element_Textarea('short_text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('rows', '8');
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addMetaElements()
    {
        $element = new Zend_Form_Element_Text('meta_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 950px;"));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('meta_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 950px; height: 50px"));
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        parent::populate($values);

        if ( isset($values['id']) ) {
            if ( $url = Doctrine::getTable($this->modelName)->find($values['id'])->getImageUrl() ) {
                $this->getElement('image')->setDescription('<img style="max-height: 200px; max-width: 200px;" src="'.$url.'">');
            }
        }
    }




}
<?

class Form_Admin_User extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addGroupIdElement();
        $this->addLoginElement();
        $this->addNameElement();
        $this->addEmailElement();

        $this->addFirstNameElement();
        $this->addLastNameElement();
        $this->addMiddleNameElement();
        $this->addBankCardElement();

        $this->addIsEnabledElement();
        $this->addSubmitElement();

        $this->addAttribs(['style'=> 'width: 500px;']);

        $this->_setDecorsTable();
    }

    public function addGroupIdElement()
    {
        $element = new Zend_Form_Element_Select('group_id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addMultiOption('', 'Группы');
        foreach (Doctrine_Query::create()->from('AdminGroups')->execute() as $item) {
            $element->addMultiOption($item->id, $item->name);
        }
        $element->addValidator('InArray', true, array(array_keys($element->getMultiOptions()), 'messages' => array('notInArray' => 'Не допустимое значение')));

        $this->addElement($element);
    }

    public function addLoginElement()
    {
        $element = new Zend_Form_Element_Text('login');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addFirstNameElement()
    {
        $element = new Zend_Form_Element_Text('first_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addLastNameElement()
    {
        $element = new Zend_Form_Element_Text('last_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addMiddleNameElement()
    {
        $element = new Zend_Form_Element_Text('middle_name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
    public function addBankCardElement()
    {
        $element = new Zend_Form_Element_Text('bank_card');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('EmailBool', true, array('messages' => 'Неверный Email'));
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
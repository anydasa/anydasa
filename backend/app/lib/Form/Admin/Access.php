<?

class Form_Admin_Access extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addSubmitElement();
        $this->setAttrib('style', 'width: 1000px;');

        $this->_setDecors();
    }

    public function addIdElement()
    {
        $Routes = Doctrine_Query::create()->from('AdminRoutes')->orderBy('module, controller, action')->execute();
        $array = array();
        foreach ($Routes as $Route) {
            $array[$Route->module.' '.$Route->controller][$Route->id]= $Route->title;
        }
        $element = new Zend_Form_Element_MultiCheckbox('id_route');
        $element->helper = 'groupedMultiCheckbox';
        $element->setMultiOptions($array);
        $this->addElement($element);

    }

    private function _setDecors()
    {
        $this->setDecorators(array(
            'FormElements',
            array('Description', array('tag' => 'h2', 'placement' => 'prepend')),
            'Form',
        ));


    }

}
<?

class Form_Admin_Route extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addNameElement();
        $this->addTypeElement();
        $this->addRouteElement();
        $this->addReverseElement();
        $this->addModuleElement();
        $this->addControllerElement();
        $this->addActionElement();
        $this->addMapElement();
        $this->addIsRegisteredInLogElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();
    }

    public function addRouteElement()
    {
        $element = new Zend_Form_Element_Text('route');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Select('type');

        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addMultiOptions(array(
            '' => '---',
            'Zend_Controller_Router_Route' => 'Zend_Controller_Router_Route',
            'Zend_Controller_Router_Route_Static' => 'Zend_Controller_Router_Route_Static',
            'Zend_Controller_Router_Route_Regex' => 'Zend_Controller_Router_Route_Regex'
        ));
        $this->addElement($element);
    }

    public function addReverseElement()
    {
        $element = new Zend_Form_Element_Text('reverse');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addModuleElement()
    {
        $element = new Zend_Form_Element_Text('module');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addControllerElement()
    {
        $element = new Zend_Form_Element_Text('controller');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addActionElement()
    {
        $element = new Zend_Form_Element_Text('action');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addMapElement()
    {

        $element = new Zend_Form_Element_Text('map');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addIsRegisteredInLogElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_registered_in_log');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
<?

class Form_Admin_Router_Set extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addStyleText('');
        $this->setAttrib('style', 'width: 500px;');

        $this->addJavaScriptText('$(function () {});');

        return parent::render($view);
    }

    public function init()
    {
        $this->addTitleElement();
        $this->addModuleElement();
        $this->addControllerElement();
        $this->addSubmitElement();
    }

    public function addModuleElement()
    {
        $element = new Zend_Form_Element_Text('module');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setLabel('Модуль:');
        $this->addElement($element);
    }
    public function addControllerElement()
    {
        $element = new Zend_Form_Element_Text('controller');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setLabel('Контроллер:');
        $this->addElement($element);
    }
    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setLabel('Название:');
        $this->addElement($element);
    }


}
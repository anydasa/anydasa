<?
class Form_Admin_Page extends Form_Abstract
{
    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addRouteIdElement();
        $this->addParamsElement();
        $this->addAppendUriElement();
        $this->addSubmitElement();

        $this->_setDecorsTable();

        $this->addJavaScriptText('
            filterSelect(\'form#'.$this->getId().' #filter_route\', \'form#'.$this->getId().' #route_id\')
        ');
        $this->setStyleText('
            .group-element span {display: none;}
        ');
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addRouteIdElement()
    {
        $element = new Zend_Form_Element_Select('route_id');
        $element->addMultiOptions([''=>'---'] + Doctrine_Query::create()->from('AdminRoutes')->orderBy('module, controller, action')->execute()->toKeyValueArray('id', 'reverse'));
        $element->addValidator('InArray', true, array(array_keys($element->getMultiOptions()), 'messages' => array('notInArray' => 'Не допустимое значение')));
        $element->setAttribs(array('style' => 'height: 150px;', 'size'=> 4));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('filter_route');
        $element->setAttribs(array('style' => 'width: 400px;'));
        $this->addElement($element);

        $this->addDisplayGroup(array('filter_route', 'route_id'), 'route', array('legend' => 'Роутер', 'escape'=>false));
    }

    public function addParamsElement()
    {
        $element = new Zend_Form_Element_Text('params');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setValue('{}');
        $this->addElement($element);
    }

    public function addAppendUriElement()
    {
        $element = new Zend_Form_Element_Text('append_uri');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
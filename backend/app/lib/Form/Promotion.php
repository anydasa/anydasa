<?

class Form_Promotion extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsTable();

        $this->addJavaScriptLink('/ckeditor/ckeditor.js');
        $this->addJavaScriptLink('/ckfinder/ckfinder.js');
        $this->addJavaScriptLink('/ckeditor/adapters/jquery.js');

        $this->addJavaScriptLink('/js/jquery.ui.datepicker-ru.min.js');
        $this->addJavaScriptText(
            "$(function () {
                $('#time_on').datepicker({
                    userLang : 'ru',
                    dateFormat: 'yy-mm-dd',
                    onSelect: customRange,
                    maxDate: $('#time_off').val()
                })
                $('#time_off').datepicker({
                    userLang : 'ru',
                    dateFormat: 'yy-mm-dd',
                    onSelect: customRange,
                    minDate: $('#time_on').val()
                })
             })

             function customRange(date) {
                if (this.id == 'time_on') {
                    $('#time_off').datepicker('option', 'minDate', date || null);
                    $('#time_on').val(date + ' 00:00:00');
                } else {
                    $('#time_on').datepicker('option', 'maxDate', date || null);
                    $('#time_off').val(date + ' 23:59:59');
                }
            }"
        );
        $this->addJavaScriptText('
            $("form#'.$this->getId().'").find("#description").ckeditor({customConfig: "../js/ckeditor_confings/light.js"})
            CKFinder.setupCKEditor( $("form#'.$this->getId().'").find("#description").ckeditorGet(), "/ckfinder/" );

            /*$("form#'.$this->getId().'").find("#short_description").ckeditor({customConfig: "../js/ckeditor_confings/basic.js"})
            CKFinder.setupCKEditor( $("form#'.$this->getId().'").find("#short_description").ckeditorGet(), "/ckfinder/" );*/

            $(function () {
                $(".image-preview").append("<div id=\"removeImg\" class=\"fa fa-times\" />")
                $("#removeImg").click(function () {
                    $("#image_delete").val(1)
                    $(".image-preview").remove()
                    $("#image").show()
                })

                if ( $(".image-preview").length ) {
                    $("#image").hide()
                }
            });
        ');

        $this->setStyleText('
            .image-preview {position: relative; display: inline-block;}
            .image-preview img {max-width: 200px; max-height: 200px;}
            #removeImg {position: absolute; top: 0; right: 0; cursor: pointer;}
        ');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addSubmitElement();

        $this->addTitleElement();
        $this->addTypeElement();
        $this->addCategoryElement();
        $this->addImageElement();
        $this->addTimeOnElement();
        $this->addTimeOffElement();
        $this->addIsEnabledElement();
//        $this->addShortDescriptionElement();
        $this->addDescriptionElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Select('id_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(
            ['' => '--------']
            +
            Doctrine_Query::create()->from('PromotionsTypes')->orderBy('sort')->execute()->toKeyValueArray('id', 'title')
        );
        $this->addElement($element);
    }

    public function addCategoryElement()
    {
        $element = new Zend_Form_Element_Select('id_category');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(
            ['' => '--------']
            +
            Doctrine_Query::create()->from('PromotionsCategories')->orderBy('sort')->execute()->toKeyValueArray('id', 'title')
        );
        $this->addElement($element);
    }

    public function addImageElement()
    {
        $element = new Zend_Form_Element_File('image');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setValueDisabled(true); // отключение авто receive

        $element->setLabel('Картинка:');

        $Validator = new Zend_Validate_File_IsImage(array('image/jpeg', 'image/gif', 'image/png'));
        $Validator->enableHeaderCheck();
        $Validator->setMessage('Заугружаемая фотография не является изображением');
        $element->addValidator($Validator);

        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('image_delete');
        $element->setLabel('Удалить картинку:');
        $this->addElement($element);
    }

    public function addShortDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('short_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style'=> "height: 50px;"));
        $this->addElement($element);
    }
    public function addDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addTimeOnElement()
    {
        $element = new Zend_Form_Element_Text('time_on');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addFilter('callback', function($v) { return empty($v) ? NULL : date('Y-m-d 00:00:00', strtotime($v)); });
        $this->addElement($element);
    }

    public function addTimeOffElement()
    {
        $element = new Zend_Form_Element_Text('time_off');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addFilter('callback', function($v) { return empty($v) ? NULL : date('Y-m-d 23:59:59', strtotime($v)); });
        $this->addElement($element);
    }

    public function addIsEnabledElement()
    {
        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function populate(array $values)
    {
        parent::populate($values);

        if ( isset($values['id']) ) {
            if ( $url = Doctrine::getTable($this->modelName)->find($values['id'])->getImageUrl() ) {
                $this->getElement('image')->setDescription('<img style="max-height: 200px; max-width: 200px;" src="'.$url.'">');
            }
        }
    }

    public function isValid($data)
    {
        if ( isset($values['id']) ) {
            $url = Doctrine::getTable($this->modelName)->find($values['id'])->getImageUrl();
            if ( Site_Image::isImage($url) ) {
                $this->image->setRequired(false)->removeValidator('NotEmpty');
            }
        }
        return parent::isValid($data);
    }

}
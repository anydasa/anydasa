<?

class Form_Catalog extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $ckFinderUrl = '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Catalog&responseType=json';

        $this->addJavaScriptText("
            $(function () {
                CKEDITOR.replace( 'text', {
                    language: 'ru',
                    uiColor: '#AADC6E',
                    filebrowserBrowseUrl: '/ckfinder/ckfinder.html',
                    filebrowserUploadUrl: '$ckFinderUrl',
                    filebrowserWindowWidth: '1000',
                    filebrowserWindowHeight: '700'
                });
            });
        ");

        $this->addJavaScriptText('
            $("form#'.$this->getId().' #text-label").next().attr("colspan", 2).prev().remove()
        ');


        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addTitleElement();
        $this->addGroupElement();
        $this->addMenuElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }

    public function addMenuElement()
    {
        $element = new Zend_Form_Element_Select('Menu');
        $element->setIsArray(true);

        $options = [''=>'---Меню---'];
        foreach (Doctrine_Query::create()->from('CatalogMenu')->where('level <> 0')->orderBy('lft')->execute() as $item) {
            $options[$item->id] = str_repeat('+---', $item->level-1) . $item->title;
        }
        $element->addMultiOptions($options);

        $element->setLabel('Меню:');
        $this->addElement($element);
    }

    public function addGroupElement()
    {
//        $element = new Zend_Form_Element_Select('id_group_type');
//        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
//
//        $element->addMultiOption('', '----');
//        $element->addMultiOptions(Doctrine_Query::create()->from('ProductsGroupsTypes')->orderBy('sort')->execute()->toKeyValueArray('id', 'title'));
//        $element->addFilter('Null');
//
//        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_group_by_series');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
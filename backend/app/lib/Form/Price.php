<?

class Form_Price extends Zend_Form
{

    public function __construct()
    {
        $this->addFileElement();

        $this->setElementDecorators(array('ViewHelper'));
    }

    public function addFileElement()
    {
        $element = new Zend_Form_Element_File('file');

		$element->setRequired(true);
        $element->setDestination(Zend_Registry::getInstance()->config->path->files);
        $element->addFilter('Rename', array('target' => Zend_Registry::getInstance()->config->path->files.'price.xls', 'overwrite' => true));

        $sizeValidator = new Zend_Validate_File_Size(array('max'=>(1024*1024*10)));
        $sizeValidator->setMessage('Максимальный размер файла %max%');
        $element->addValidator($sizeValidator);

        $uploadValidator = new Zend_Validate_File_Upload;
        $uploadValidator->setMessage('Файл не загружен на сервер. Возможно загружаемый файл слишком большой.');
        $element->addValidator($uploadValidator);

        $this->addElement($element);

    }

    public function isValid($data)
    {
        $valid = parent::isValid($data);

        foreach ($this->getMessages() as $error) {
            $this->addErrorMessages($error);
        }

        return $valid;
    }

}

<?

class Form_OrderAdd extends Form_Abstract
{

    public function init()
    {
        $this->setMethod('post');

        $this->addIdElement();

        $this->addEmailElement();
        $this->addDeliveryElement();
        $this->addDeliveryCostElement();
        $this->addPaymentElement();
        $this->addNameElement();
        $this->addPhoneElement();
        $this->addAddressElement();
        $this->addCommentElement();
        $this->addAdminCommentElement();

        ///
        $this->addCityElement();
        $this->addNpElement();
        $this->addCreditElement();

        $this->addSubmitElement();


    }

    public function render()
    {

        $this->addJavaScriptLink('/js/jquery/maskedinput.js');
        /*$this->addJavaScriptText(
            '$("#'.$this->getId().' #phone").mask("+38 (099) 999-99-99")'
        );*/

        /*$this->addJavaScriptText(
            '$("#'.$this->getId().' #phone").on("change")'
        );*/

        $this->addJavaScriptLink('/js/order/delivery.js');
        $this->addJavaScriptLink('/js/order/payment.js');

        return parent::render();
    }


    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email')->setAttrib('placeholder', "{$element->getLabel()}")
                ->addFilter(new Zend_Filter_StringToLower());

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "{$element->getLabel()} указан не корректно"
        )));

        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addCityElement()
    {
        $element = new Zend_Form_Element_Hidden('city_code');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('city');
        $element->setLabel('Город');
        $element->setDescription('Укажите город');
        $this->addElement($element);
    }

    public function addNpElement()
    {
        $element = new Zend_Form_Element_Select('warehouse');
        $element->addMultiOption('', 'Сначало укажите город');
        $element->setRegisterInArrayValidator(false);
        $element->setLabel('Отделение');
        $element->setDescription('&nbsp;');
        $this->addElement($element);
    }

    public function addPaymentElement()
    {
        $element = new Zend_Form_Element_Select('payment_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOption('', '---');
        $element->addMultiOptions(OrdersPayment::getList()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }

    public function addCreditElement()
    {
        $element = new Zend_Form_Element_Text('offer');
        $element->setDescription('&nbsp;');
        $element->setAttrib('readonly', 'readonly');
        $element->setLabel('Кредитная программа');
        $element->setAttrib('data-modal-link', '/credit/order/');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id_offer');
        $this->addElement($element);
    }

    public function addDeliveryElement()
    {
        $element = new Zend_Form_Element_Select('delivery_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOption('', '---');
        $element->addMultiOptions(OrdersDelivery::getList()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }

    public function addDeliveryCostElement()
    {
        $element = new Zend_Form_Element_Text('delivery_cost');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);
    }

    public function addPhoneElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Введите {$element->getLabel()}")));
        $this->addElement($element);
    }

    public function addAddressElement()
    {
        $element = new Zend_Form_Element_Textarea('address');
        $element->setAttrib('style', 'width: 390px; height: 100px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setAttrib('placeholder', "{$element->getLabel()}");
        $this->addElement($element);
    }

    public function addCommentElement()
    {
        $element = new Zend_Form_Element_Textarea('comment');
        $element->setAttrib('style', 'width: 390px; height: 100px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setAttrib('placeholder', "{$element->getLabel()}");
        $this->addElement($element);
    }

    public function addAdminCommentElement()
    {
        $element = new Zend_Form_Element_Textarea('admin_comment');
        $element->setAttrib('style', 'width: 390px; height: 100px; background: #DACDAc;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setAttrib('placeholder', "{$element->getLabel()}");
        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        $element = new Zend_Form_Element_Button(array(
            'name' => 'save',
            'label' => 'Сохранить',
            'escape' => false,
            'type' => 'submit',
            'class' => 'btn btn-success'
        ));
        $element->setOrder(10000);
        $this->addElement($element);
    }
}
<?

class Form_Seo extends Form_Abstract
{
    public function init()
    {
        $this->addElementsElement();

        $this->_setDecorsTable();

        $this->setStyleText('
            #Seo .group-element {float: left;}
            #Seo .group-element span {display:none;}
        ');
    }

    public function addElementsElement()
    {
        $this->addIdElement();

        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setAttrib('style', 'width: 700px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('mask');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setAttrib('style', 'width: 600px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('mask_type');
        $element->setAttrib('style', ' width: 100px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addMultiOptions(array('static'=>'Статика', 'regex'=>'Регулярка'));
        $this->addElement($element);

        $this->addDisplayGroup(array('mask', 'mask_type'), 'mask1', array('legend'=>'Маска:'));

        $element = new Zend_Form_Element_Textarea('html_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setAttrib('style', 'height: 50px; width: 700px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('html_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px; width: 700px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('html_h1');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px; width: 700px;');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('html_h2');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px; width: 700px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('html_h2_2');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttrib('style', 'height: 50px; width: 700px;');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('html_text');
        $element->setAttribs(array('width' => "700px", 'height' => "200px"));
        $element->helper = 'fckEditor';
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('html_text_2');
        $element->setAttribs(array('width' => "700px", 'height' => "200px"));
        $element->helper = 'fckEditor';
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $element = new Zend_Form_Element_Checkbox('is_enabled');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);

        $this->addSubmitElement();
    }

	public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addHtmlTitleElement()
    {
        $element = new Zend_Form_Element_Text('html_title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 950px;"));
        $this->addElement($element);
    }

    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->helper = 'fckEditor';
        $element->setAttribs(array('width' => "950", 'height' => "500"));
        $this->addElement($element);
    }

    public function addHtmlDescriptionElement()
    {
        $element = new Zend_Form_Element_Textarea('html_description');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setAttribs(array('style' => "width: 950px; height: 50px"));
        $this->addElement($element);
    }
}
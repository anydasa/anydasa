<?

class Form_Auth extends Zend_Form
{

    public function init()
    {
        parent::init();
        $this->setTranslator(Zend_Registry::get('Zend_Translate'));

        $this->addLoginElement();
        $this->addPasswordElement();
        $this->addRememberElement();

        $this->setElementDecorators(array('ViewHelper'));
    }

    public function addLoginElement()
    {
        $element = new Zend_Form_Element_Text('login');
        $element->setRequired(true)->addErrorMessage('Введите логин.');
        $this->addElement($element);
    }

    public function addPasswordElement()
    {
        $element = new Zend_Form_Element_Password('password');
        $element->setRequired(true)->addErrorMessage('Введите пароль.');
        $this->addElement($element);
    }

    public function addRememberElement()
    {
        $element = new Zend_Form_Element_Checkbox('remember');
        $this->addElement($element);
    }

    public function isValid($data)
    {
        $valid = parent::isValid($data);

        foreach ($this->getMessages() as $error) {
            $this->addErrorMessages($error);
        }

        return $valid;
    }

}
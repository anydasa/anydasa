<?

class Form_RedirectRule extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('style', 'min-width: 600px;');

        $this->_setDecorsTable();

        $this->addStyleText('
            .ui-helper-hidden-accessible {display: none;}
            .ui-autocomplete {
              position: absolute;
              top: 0;
              left: 0;
              cursor: default;
              background: white;
              border: 1px solid #EEEEEE;
              border-top: 0;
              box-shadow: 0 2px 5px #000;
            }
            .ui-autocomplete, .ui-autocomplete > li {
              list-style: none;
              margin: 0;
              padding: 0;
            }
            .ui-autocomplete a {
                text-decoration: none;
            }
            .ui-menu-item {
              padding: 2px 5px !important;
              border-bottom: 1px dotted #CCC;
            }
            .ui-menu-item:hover {
              cursor: pointer;
              background: #EEEEEE;
            }
            .ui-menu-item:last-child {
              border: 0;
            }
        ');

        $this->addJavaScriptText('$(function () {
            var $element = $("input#from_url");
            $element.autocomplete({
                source: "/not-found/autocomplete/",
                appendTo: $element.parent(),
                minLength: 0
            }).focusin(function() {
                $(this).autocomplete("search");
            });
        });');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdElement();
        $this->addFromUrlElement();
        $this->addToUrlElement();
        $this->addSubmitElement();
    }

    public function addFromUrlElement()
    {
        $element = new Zend_Form_Element_Text('from_url');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    $this->modelName,
                    $element->getName(),
                    'id',
                    'messages' => array(
                        'dbRecordExists' => 'Уже есть в базе.'
        )));

        $element->setDescription('пример: /category/smartfony/');

        $this->addElement($element);
    }

    public function addToUrlElement()
    {
        $element = new Zend_Form_Element_Text('to_url');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->setDescription('пример: /shop/smart/');
        $this->addElement($element);
    }

}
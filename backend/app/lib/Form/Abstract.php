<?

abstract class Form_Abstract extends Zend_Form
{
    protected $modelName;
    protected $view;


    private $_javaScriptText = '';
    private $_styleText = '';


    function __construct($formName, $options = null)
    {
        $this->addPrefixPath('Site_Form_Decorator', 'Site/Form/Decorator', 'decorator');
        $this->modelName = $formName;
        $this->setName($formName);
        $this->setAttrib('id', $formName);
        $this->view = Zend_Layout::getMvcInstance()->getView();

        $this->setCKFinderBasePath();

        parent::__construct($options);
    }

    protected function setCKFinderBasePath($path = false)
    {
        $SK = new Zend_Session_Namespace('CKFinder');
        $SK->config['basePath'] = $path;
    }

    static function create($formName, $options = null)
    {
        $className = get_called_class();
        return new $className($formName, $options);
    }

    public function addElement($Element)
    {
        if ( $Element instanceof Zend_Form_Element AND $Element->getType() == 'Zend_Form_Element_Text') {
            $Element->addFilter(new Site_Filter_TrimAndDoubleSpace);
            $Element->addFilter(new Site_Filter_Comma2NumericPoint);
            if ($Filter = $Element->getFilter('Null')) {
                $Element->removeFilter('Zend_Filter_Null');
                $Element->addFilter($Filter);
            }
        }

        try {
            if ( $Element->getType() != 'Zend_Form_Element_File' AND '' == $Element->getValue() ) {
                $Element->setValue( Doctrine::getTable($this->modelName)->getDefaultValueOf($Element->getName()) );
            }
        } catch (Exception $e) {}

        parent::addElement($Element);
    }

    public function lockField($field)
    {
        $elem = $this->getElement($field);
        $elem->setAttrib('disabled', true);
        $elem->setAttrib('readonly',true);
    }


    public function addTechFields($params)
    {
        if ( !empty($params['afterSaveCallbackWithJsonResult']) ) {
            $element = new Zend_Form_Element_Hidden('afterSaveCallbackWithJsonResult');
            $element->setValue($params['afterSaveCallbackWithJsonResult']);
            $this->addElement($element);
        }
    }

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Hidden('id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        $element = new Zend_Form_Element_Button(array(
            'name' => 'save',
            'label' => 'Сохранить',
            'escape' => false,
            'type' => 'submit',
            'class' => 'btn btn-primary'
        ));
        $element->setOrder(10000);
        $this->addElement($element);
    }

    public function getOptionsForJqueryPlugin($form = null)
    {
        if ( !$form ) {
            $form = $this;
        }

        foreach ($form->getElements() as $element) {
        	foreach ($element->getValidators() as $validate) {

                $name = $element->getFullyQualifiedName();

        	    $messages = $validate->getMessageTemplates();

        	    if (get_class($validate) == 'Zend_Validate_NotEmpty') {
        	        $array['rules'][$name]['required'] = true;
        	        $array['messages'][$name]['required'] = $messages['isEmpty'];
        	    }
        	    if (get_class($validate) == 'Site_Validate_EmailBool') {
        	        $array['rules'][$name]['email'] = true;
        	        $array['messages'][$name]['email'] = $messages['emailNotValid'];
        	    }
                if (get_class($validate) == 'Zend_Validate_Date') {
        	        $array['rules'][$name]['date'] = true;
        	        $array['messages'][$name]['date'] = $messages['dateInvalid'];
        	    }
                if (get_class($validate) == 'Zend_Validate_Regex') {
                    $array['rules'][$name]['regex'] = new Zend_Json_Expr($validate->getPattern());
                    $array['messages'][$name]['regex'] = $messages['regexInvalid'];
                }
                if (get_class($validate) == 'Zend_Validate_GreaterThan') {
                    $array['rules'][$name]['greaterThan'] = $validate->getMin();
                    $array['messages'][$name]['greaterThan'] = $messages['notGreaterThan'];
                }
                if (get_class($validate) == 'Zend_Validate_LessThan') {
                    $array['rules'][$name]['lessThan'] = $validate->getMax();
                    $array['messages'][$name]['lessThan'] = $messages['notLessThan'];
                }
                if ( get_class($validate) == 'Zend_Validate_Float' ) {
                    $array['rules'][$name]['number']    = true;
                    $array['messages'][$name]['number'] = $messages['notFloat'];
                }
                if ( get_class($validate) == 'Zend_Validate_Int' ) {
                    $array['rules'][$name]['digits']    = true;
                    $array['messages'][$name]['digits'] = $messages['notInt'];
                }
                if ( get_class($validate) == 'Site_Validate_Numeric' ) {
                    $array['rules'][$name]['number']    = true;
                    $array['messages'][$name]['number'] = $messages['notNumeric'];
                }
                if (get_class($validate) == 'Zend_Validate_Identical') {
                    $equalFunction = 'equalTo';
                    /*$filters = $element->getFilters();
                    if (count($filters) && isset($filters['Zend_Filter_Callback']) && 'md5' == $filters['Zend_Filter_Callback']->getCallback()) {
                        $equalFunction = 'equalMd5To';
                    }*/
        	        $array['rules'][$name][$equalFunction] = '#'.$this->getElement($validate->getToken())->getId();
        	        $array['messages'][$name][$equalFunction] = $messages['notSame'];
        	    }

                if ( get_class($validate) == 'Site_Validate_Doctrine_NoDbRecordExists' || get_class($validate) == 'Site_Validate_Doctrine_DbRecordExists' ) {
        	        $array['rules'][$name]['remote']['type'] = 'post';
        	        $array['rules'][$name]['remote']['url'] = '/validate-form-element/?form='.get_class($this) .'&model='.$this->getName() . '&field='.$name;

                    foreach ($form->getElements() as $e) {
                       $array['rules'][$name]['remote']['data'][$e->getName()] = new Zend_Json_Expr('function () {return $("form#'.$this->getId().' #'.$form->getElement($e->getName())->getId().'").val()}');
                    }
        	    }

                if (get_class($validate) == 'Zend_Validate_StringLength') {
                    if ( $validate->getMin() ) {
                        $array['rules'][$name]['minlength'] = $validate->getMin();
                        $array['messages'][$name]['minlength'] = str_replace('%min%', $validate->getMin(), $messages['stringLengthTooShort']);
                    }
                    if ( $validate->getMax() ) {
                        $array['rules'][$name]['maxlength'] = $validate->getMax();
                        $array['messages'][$name]['maxlength'] = str_replace('%max%', $validate->getMax(), $messages['stringLengthTooLong']);
                    }
                }
            }
        }

        return $array;
    }

    public function addStyleText($text)
    {
        $this->_styleText .= '<style>'.$text . '</style>';
    }



    protected function prefixElementNames($form = null)
    {
        if ( !$form ) $form = $this;

        static $belong = array();
        $belong[$form->getName()] = $form->getName();

        foreach ($form->getSubForms() as $subForm) {
            $this->prefixElementNames($subForm);
            unset($belong[$subForm->getName()]);
        }

        $formName = implode('_', $belong);

        foreach ($form->getElements() as $e) {
        	$e->setAttrib('id', $formName . '_' . $e->getName());
            $e->addDecorator('Errors', array(
                                        'escape'           => false,
                                        'elementSeparator' => '<br>',
                                        'elementStart'     => '<label class="error" for="'.$formName . '_' . $e->getName().'">',
                                        'elementEnd'       => '</label>',
            ));
        }
    }

    public function getMessages($name = NULL, $suppressArrayNotation = false)
    {
        $return = array();
        foreach (parent::getMessages($name, $suppressArrayNotation) as $key=>$item) {
            $return[$key] = current($item);
        }
        return $return;
    }

    public function getJavaScriptCode()
    {
        return $this->_javaScriptText;
    }

    public function addStandartJavaScriptValidationForm()
    {
        $this->addJavaScriptText("
            $.validator.addMethod('greaterThan', function (value, el, param) {
                return value > param;
            });
            $.validator.addMethod('lessThan', function (value, el, param) {
                return value < param;
            });
            $('form#".$this->getAttrib('id')."').validate(".Zend_Json::encode($this->getOptionsForJqueryPlugin(), false, array('enableJsonExprFinder' => true)).");

        ");
    }

    public function addJavaScriptText($text)
    {
        $this->_javaScriptText .= '<script type="application/javascript">'.$text . '</script>';
    }

    public function setStyleText($text)
    {
        $this->_styleText = '<style>'.$text . '</style>';
    }

    public function addStyleLink($includeFile)
    {
        $this->_styleText .= '<link href="'.$includeFile.'" media="screen" rel="stylesheet" type="text/css" />';
    }

    public function addJavaScriptLink($includeFile)
    {
        $this->_javaScriptText .= '<script type="application/javascript" src="'.$includeFile . '"></script>';
    }

    public function __toString()
    {
        $this->addStandartJavaScriptValidationForm();
        return parent::__toString();
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $content = parent::render($view);
        return  $this->_styleText . $content . $this->getJavaScriptCode();

    }

    protected function _setDecorsTable($form = null)
    {
        if ( is_null($form) ) {
            $form = $this;
        }

        $form->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'standart-form ', 'id' => 'form_' . $form->getName())),
            array('Description', array('tag' => 'h1', 'placement' => 'prepend', 'escape' => false)),
            'Form',
        ));

        $form->setElementDecorators(array(
            'ViewHelper',
            array('Description', array('escape' => false)),
            array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
            array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
            array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
            array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
        ));

        foreach ( $this->getDisplayGroups() as $group ) {
            $group->setDecorators(array('FormElements', array('Fieldset')));
            foreach ($group->getElements() as $element) {
                $element->setDecorators(array(
                    'ViewHelper',
                    array('Description', array('escape' => false)),
                    array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                    array('Label', array('tag' => 'span', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
                    array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class'=>'group-element')),
                ));

                if ( empty($element->getLabel()) ) {
                    $element->removeDecorator('Label');
                }
            }
        }



        foreach ($form->getElements() as $element) {
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
            if ( 'Zend_Form_Element_MultiCheckbox' == $element->getType() ) {
                $element->setDecorators(array(
                    "ViewHelper",
                    array('Description', array('escape' => false)),
                    array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                    array(array("element" => "HtmlTag"), array("tag"   => "div", "class" =>"MultiCheckbox", 'id' => $element->getId())),
                    array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
                    array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
                    array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),

                ));
            }
            if ( 'Zend_Form_Element_File' == $element->getType() ) {
                $element->setDecorators(array(
                        'File',
                        array('Description', array('escape' => false)),
                        array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                        array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
                        array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
                        array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
                    ));

            }
        }

        $form->getElement('save')->setDecorators(array(
            array('decorator' => 'ViewHelper'),
            array(
                'decorator' => array('td' => 'HtmlTag'),
                'options' => array('tag' => 'td', 'colspan' => 2)),
            array(
                'decorator' => array('tr' => 'HtmlTag'),
                'options' => array('tag' => 'tr')),
        ));

        foreach ($form->getSubForms() as $subForm) {
            $this->_setDecorsTable($subForm);
        }
    }
}
<?

class Form_Help_Note extends Form_Abstract
{
    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptText('
            CKEDITOR.replace("text", { customConfig: "/js/ckeditor_confings/light.js"});
        ');

        return parent::render();
    }

    public function init()
    {
        $this->addIdElement();
        $this->addIdHelpCategoryElement();
        $this->addTitleElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }

    public function addIdHelpCategoryElement()
    {
        $element = new Zend_Form_Element_Select('id_help_category');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOptions(array(''=>'---'));
        $element->addMultiOptions(Doctrine::getTable('HelpCategory')->findAll()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }

    public function addTitleElement()
    {
        $element = new Zend_Form_Element_Text('title');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }
    public function addTextElement()
    {
        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $this->addElement($element);
    }
}
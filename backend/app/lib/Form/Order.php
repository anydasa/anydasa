<?

class Form_Order extends Form_Abstract
{

    public function init()
    {
        $this->addIdElement();

        $this->addEmailElement();
        $this->addDeliveryElement();
        $this->addDeliveryCostElement();
        $this->addDeliveryDesiredDateElement();
        $this->addDeliveryDateElement();
        $this->addPaymentElement();
        $this->addNameElement();
        $this->addPhoneElement();
        $this->addAddressElement();
        $this->addCommentElement();
        $this->addAdminCommentElement();

        $this->addSubmitElement();
    }

    public function render()
    {
        $this->_setDecorsTable();

        $this->addJavaScriptLink('/js/jquery/maskedinput.js');
        $this->addJavaScriptText(
            '$("#'.$this->getId().' #phone").mask("+38 (099) 999-99-99")'
        );
        $this->addJavaScriptLink('/js/moment-with-locales.js');
        $this->addJavaScriptLink('/js/bootstrap-datetimepicker.min.js');

        $this->addStyleLink('/css/bootstrap/bootstrap-datetimepicker.min.css');
        $this->addJavaScriptText(
            " $(function () {
                $('#desired_date_delivery').datetimepicker({
                    format: 'YYYY-MM-DD H:mm',
                    locale: 'ru'
                });
                $('#date_delivery').datetimepicker({
                    format: 'YYYY-MM-DD H:mm',
                    locale: 'ru'
                });
            });"
        );

        return parent::render();
    }


    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email')->setAttrib('placeholder', "{$element->getLabel()}")
                ->addFilter(new Zend_Filter_StringToLower());

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "{$element->getLabel()} указан не корректно"
        )));

        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $this->addElement($element);
    }

    public function addPaymentElement()
    {
        $element = new Zend_Form_Element_Select('payment_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOption('', '---');
        $element->addMultiOptions(OrdersPayment::getList()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }

    public function addDeliveryElement()
    {
        $element = new Zend_Form_Element_Select('delivery_type');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно для заполнения")));
        $element->addMultiOption('', '---');
        $element->addMultiOptions(OrdersDelivery::getList()->toKeyValueArray('id', 'title'));
        $this->addElement($element);
    }

    public function addDeliveryCostElement()
    {
        $element = new Zend_Form_Element_Text('delivery_cost');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);
    }

    public function addDeliveryDesiredDateElement()
    {
        $element = new Zend_Form_Element_Text('desired_date_delivery');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);
    }

    public function addDeliveryDateElement()
    {
        $element = new Zend_Form_Element_Text('date_delivery');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $this->addElement($element);
    }

    public function addPhoneElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Введите {$element->getLabel()}")));
        $this->addElement($element);
    }

    public function addAddressElement()
    {
        $element = new Zend_Form_Element_Textarea('address');
        $element->setAttrib('style', 'width: 390px; height: 100px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setAttrib('placeholder', "{$element->getLabel()}");
        $this->addElement($element);
    }

    public function addCommentElement()
    {
        $element = new Zend_Form_Element_Textarea('comment');
        $element->setAttrib('style', 'width: 390px; height: 100px;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setAttrib('placeholder', "{$element->getLabel()}");
        $this->addElement($element);
    }

    public function addAdminCommentElement()
    {
        $element = new Zend_Form_Element_Textarea('admin_comment');
        $element->setAttrib('style', 'width: 390px; height: 100px; background: #DACDAc;');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment']);
        $element->setAttrib('placeholder', "{$element->getLabel()}");
        $this->addElement($element);
    }
}
<?

$appConfig = array(
    'debug' => array(
        'on' => true,
    ),
    'cache' => array(
        'frontend' => array(
                'caching' => false,
        )
    ),
    'path' => array(
        'bin' => APPLICATION_PATH . 'bin/'
    )
);
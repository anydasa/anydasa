<?
class Series_OptionsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'SeriesOptions';
    const FORM_NAME     = 'Form_Series_Option';
    const ROUTE_PREFIX  = 'series-options-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $this->view->Catalog = $this->findRecordOrException('Catalog', $this->getRequest()->getParam('id_catalog'));

        $params = $this->_request->getQuery();
        $params['id_catalog'] = $this->view->Catalog ->id;

        $Query = $this->getQuery($params);

        $Query->orderBy('sort');


        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

    protected function getForm()
    {
        if ($this->getRequest()->getParam('id_catalog')) {
            /** @var Catalog $catalog */
            $catalog = $this->findRecordOrException('Catalog', $this->getRequest()->getParam('id_catalog'));
        } else {
            /** @var SeriesOptions $SeriesOptions */
            $SeriesOptions = $this->findRecordOrException(self::MODEL_NAME, $this->getRequest()->getParam('id'));
            $catalog = $SeriesOptions->ProductsOption->Catalog;
        }

        $form = new Form_Series_Option('SeriesOptions', $catalog);
        $form->setDescription(sprintf('Параметр серии для категории - %s', $catalog->title));
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->addTechFields( $this->getAllParams() );

        return $form;
    }
}
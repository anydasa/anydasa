<?
class Series_GroupController extends Controller_AbstractList
{
    const MODEL_NAME    = 'SeriesGroups';
    const FORM_NAME     = 'Form_Series_Group';
    const ROUTE_PREFIX  = 'series-group-';
    const COUNT_ON_PAGE = 20;

    public function sortAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->view->Group = $this->findRecordOrException(self::MODEL_NAME, $this->params['id']);

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update('Series')->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }
    }

}
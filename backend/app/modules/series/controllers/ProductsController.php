<?
class Series_ProductsController extends Controller_Account
{
    public function listAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $Series = $this->findRecordOrException('Series', $this->params['id_series']);

        if ($this->_request->isPost()) {

            $Series->SeriesProducts->delete();

            $products = $this->_request->getPost()['product'] ?: [];

            foreach ($products as  $idProduct) {
                $SeriesProducts = new SeriesProducts;
                $SeriesProducts->id_product = $idProduct;
                $SeriesProducts->id_series = $Series->id;
                $SeriesProducts->save();
            }

            $this->view->isSaved = true;

            $Series->refresh(true);
        }

        $this->view->Series = $Series;
    }
}
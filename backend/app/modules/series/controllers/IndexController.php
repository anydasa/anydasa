<?
class Series_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Series';
    const FORM_NAME     = 'Form_Series_Series';
    const ROUTE_PREFIX  = 'series-index-';
    const COUNT_ON_PAGE = 50;

    protected function _postSave($node, $form)
    {
        if ( !empty($form->getValue('image_delete')) ) {
            @unlink($node->getImagePath());
        }

        if ( !empty($form->getValue('image')) ) {
            $target = $node->getImagePath();

            $form->image->addFilter('Rename', array('target' => $target, 'overwrite'  => true));
            $form->image->receive();

            $im = new Imagick($target);
            $im->resizeImage(300, 300, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();

        }
    }

    public function downloadParamsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
        $ExcelReport->setHeadRow(array(
            'id', 'Серия', 'Параметр', 'Значение'
        ));

        $sql = 'SELECT sv.id, s.title AS series_title, sp.title AS param_title, sv.title AS value_title
                FROM series_values sv, series_params sp, series s
                WHERE sv.id_param = sp.id
                  AND s.id = sp.id_series';
        $stmt = Doctrine_Manager::connection()->execute($sql);

        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $item) {
            $ExcelReport->addDataRow(array(
                $item['id'],
                $item['series_title'],
                $item['param_title'],
                $item['value_title'],
            ));
        }

        $ExcelReport->Excel->getActiveSheet()->getRowDimension(1)->setRowHeight(15);

        $lastRow = $ExcelReport->Excel->getActiveSheet()->getHighestDataRow();

        $ExcelReport->Excel->getActiveSheet()
            ->getStyle("A2:C$lastRow")->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'CCCCCC')
                    )
                )
            );

        $ExcelReport->Excel->getActiveSheet()->getColumnDimension('A')->setWidth(1);
        $ExcelReport->Excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
        $ExcelReport->Excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $ExcelReport->Excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);

        echo $ExcelReport->getReport(false);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename="SeriesParams.xlsx"')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }

    public function descriptionAction()
    {
        /** @var Series $node */
        $node = $this->_getNode($this->params['id']);
        $form = new Form_Series_Description('Description', $node);
        $form->setAction($_SERVER['REQUEST_URI']);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $node->save();

                Service_SeriesImage::saveRemoteDescriptionImagesLocally($node);

                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        echo $form;
    }
}

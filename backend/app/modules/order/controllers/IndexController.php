<?

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Order_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Orders';
    const FORM_NAME     = 'Form_Order';
    const ROUTE_PREFIX  = 'order-';
    const COUNT_ON_PAGE = 30;

    public function similarListAction()
    {
        $Order = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id'));

        $this->view->Order = $Order;

        $this->_helper->viewRenderer->setNoRender(false);

    }

    public function listNewAction()
    {
        $this->render('ember');
    }

    protected function getQuery($params)
    {
        $Query = parent::getQuery($params);

        if ( 'overdue' == $params['q'] ) {
            $Query->innerJoin($Query->getRootAlias().'.OrdersStatus osss')
                ->addWhere('COALESCE(updated, created) < NOW() - (INTERVAL \'1 min\' * osss.minutes_limit)');
        }

        if ( !empty($params['id_catalog']) ) {
            $Query->innerJoin($Query->getRootAlias().'.OrdersProducts op')
                ->addWhere('op.code IN (SELECT p.code FROM Products p WHERE ? = ANY(p.catalogs))', $params['id_catalog']);
        }

        if ( $params['who'] != '' ) {
            if($params['who']){
                $Query->addWhere('who_created = ?', $params['who']);
            }else{
                $Query->addWhere('who_created IS NULL');
            }
        }

        if ( isset($params['sent_offer']) ) {
            if($params['sent_offer'] == 'yes')
                $Query->andWhere('sent_offer_html IS NOT NULL');
            elseif($params['sent_offer'] == 'no')
                $Query->andWhere('sent_offer_html IS NULL');

        }

        if ( isset($params['statuses']) && !empty($params['statuses'])) {
            $Query->addWhere('id_status IN (\'' . implode('\',\'',$params['statuses']) . '\')');
        }

        $Query2 = clone $Query;

        $Query2->select('Orders.id');
        $Query2->removeDqlQueryPart('orderby');

        $Query3 = Doctrine_Query::create()
            ->select('SUM(price*count) circulation, SUM(price*count - vendor_price_buy*count) profit')
            ->from('OrdersProducts')
            ->where('id_order IN ('.$Query2->getDql().')')
        ;

        $res = $Query3->execute($Query->getParams()['where']);

        $this->view->sum = $res->toArray()[0];

        $Query2 = clone $Query;
        $Query2->removeDqlQueryPart('orderby');
        $Query2->removeDqlQueryPart('select');

        $Query2->select('count(os.id) as count, os.title, Orders.id_status');
        $Query2->innerJoin($Query2->getRootAlias().'.OrdersStatus os');
        $Query2->groupBy('os.id, Orders.id_status');
        $Query2->setHydrationMode(Doctrine_Core::HYDRATE_SCALAR);

        $Query2->orderBy('os.sort');

        $status_count = array_column($Query2->execute(), 'os_count', 'os_title');

        if ( 'overdue' != $params['q'] ) {
            $Query2->innerJoin($Query->getRootAlias() . '.OrdersStatus osss')
                ->addWhere('COALESCE(updated, created) < NOW() - (INTERVAL \'1 min\' * osss.minutes_limit)');

            $overdue = array();

            foreach ($Query2->execute() as $status) {
                $overdue[$status['os_title']] = array('count' => $status['os_count'],
                                                      'id' => $status['Orders_id_status']);
            }
        }
        /*   get order's owner's   */
        $query = clone $Query;

        $query->innerJoin( $Query->getRootAlias() . '.AdminUsers au');
        $query->select('count(Orders.id) as count_order, au.name as user_name, au.id as user_id');
        $query->groupBy('Orders.who_created, au.name, au.id');
        $query->orderBy('count_order DESC');
        $query->setHydrationMode(Doctrine_Core::HYDRATE_SCALAR);

        $managers_count = [];
        foreach ($query->execute() as $user) {
            $managers_count[] = [
                'id' => $user['au_user_id'],
                'name' => $user['au_user_name'],
                'count_order' => $user['Orders_count_order'],
            ];
        }
        $this->view->managers_count = $managers_count;

        /*      */
        $client_count = [];
        $query = clone $Query;

        $query->select('count(Orders.id) as count');
        $query->addWhere('Orders.who_created IS NULL');
        $query->orderBy('count DESC');

        $client_count = $query->fetchOne();

        $this->view->client_count = $client_count;

        $this->view->status_overdue = $overdue;

        $this->view->status_count = $status_count;

        return $Query;
    }

    public function viewAction()
    {
        $this->view->node = $this->_getNode($this->params['id']);
        $this->render('view');
    }

    public function addAction()
    {
        $modelName = static::MODEL_NAME;
        $form = new Form_OrderAdd($modelName);
        $form->setAction($this->view->url(array(), 'order-add'));
        $form->setDecorators(array(array('ViewScript', array('viewScript' => 'form/order-add.phtml'))));

        $products = Cart::getInstance()->getProducts();
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $node = new $modelName;
                $node->fromArray($form->getValues());
                $node->who_created = $this->user->id;
                if($node->delivery_cost == ''){
                    $node->delivery_cost = NULL;
                }
                
                if(!empty($form->getValue('city')) && $node->delivery_type == 'novaposhta'){
                    $node->address .= 'Город '. $form->getValue('city'). "\n";
                    $node->address .= "Склад: " . Doctrine::getTable('NovaposhtaWarehouses')->findOneByCode( $form->getValue('warehouse') )->description_ru;;
                }

                if($node->payment_type == 'credit'){
                    $node->credit = Credit::getSerializedData();
                }

                $this->_preSave($node, $form, true);
                $node->save();
                $this->_postSave($node, $form, true);

                foreach ($products as $item) {
                    $OrdersProducts = new OrdersProducts;
                    $OrdersProducts->fromArray($item);
                    if ( !empty($OrdersProducts->code) && $Product = Doctrine::getTable('Products')->findOneBy('code', $OrdersProducts->code) ) {
                        $OrdersProducts->vendor_name        = $Product->OwnerProduct->Vendors->name;
                        $OrdersProducts->vendor_price_buy   = $Product->OwnerProduct->getPrice('buy', 'UAH');
                    }
                    $node->Products->add($OrdersProducts);
                }
                $node->Products->save();
                $node->sendMessagesForCurrentStatus();

                Cart::getInstance()->clearProducts();
                $form->reset();
                Credit::clearParams();

                $this->redirect($this->view->url(array(), Zend_Registry::get('myCurrentRoute')->name));

            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate($this->_request->getQuery());
        }
        $this->view->form = $form;

        $this->view->Products = $products;

        $this->render('add');
    }

    public function editAction()
    {
        $node = $this->_getNode($this->params['id']);
        $form = $this->getForm();

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                if($node->delivery_cost == ''){
                    $node->delivery_cost = NULL;
                }

                $node->desired_date_delivery = NULL;
                if(!empty($form->getValue('desired_date_delivery'))){
                    $node->desired_date_delivery = date('Y-m-d H:i:s', strtotime($form->getValue('desired_date_delivery')));
                }

                $node->date_delivery = NULL;
                if(!empty($form->getValue('date_delivery'))){
                    $node->date_delivery = date('Y-m-d H:i:s', strtotime($form->getValue('date_delivery')));
                }


                $this->_preSave($node, $form);
                $node->save();
                $this->_postEdit($node, $form);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        echo $form;
    }

    public function editNewAction()
    {

        $node = $this->_getNode($this->params['id']);
        $modelName = static::MODEL_NAME;
        $form = new Form_OrderAdd($modelName);
        $form->setAction($this->view->url(array('id'=>$node->id), 'order-edit_new'));
        $form->setDecorators(array(array('ViewScript', array('viewScript' => 'form/order-add.phtml'))));

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form);
                $node->save();
                $this->_postEdit($node, $form);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        $this->view->order = $node;
        $this->view->form = $form;

        $this->render('add');
    }

    public function historyAction()
    {
        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id'));
        $this->view->list = $Order->History;
        $this->render('history');
    }

    public function statusChangeAction()
    {
        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id_order'));
        $Status = $this->findRecordOrException('OrdersStatus', $this->_request->getParam('id_status'));

        if ( $this->_request->isPost() ) {

            $Order->admin_msg = NULL;
            $Order->user_msg = NULL;

            $OrderHistory = new OrdersHistory;
            $OrderHistory->id_admin = Zend_Registry::get('CurrentAdmin')->id;
            $OrderHistory->comment = "<div><strong>Смена статуса:</strong> с <i>{$Order->Status->title}</i> на <i>{$Status->title}</i></div>";

            if ( !empty($this->_request->getPost('admin_msg')) ) {
                $Order->admin_msg = $this->_request->getPost('admin_msg');
                $OrderHistory->comment = $OrderHistory->comment . "<div><strong>Сообщение админу:</strong> {$this->_request->getPost('admin_msg')}</div>";
            }
            if ( !empty($this->_request->getPost('user_msg')) ) {
                $Order->user_msg = $this->_request->getPost('user_msg');
                $OrderHistory->comment = $OrderHistory->comment . "<div><strong>Сообщение пользователю:</strong> {$this->_request->getPost('user_msg')}</div>";
            }

            $Order->OrdersHistory->add($OrderHistory);

            $Order->id_status = $Status->id;
            $Order->save();

            $Order->sendMessagesForCurrentStatus();

            $this->goBack();
        }

        $this->view->Order = $Order;
        $this->view->Status = $Status;
        $this->render('status-change');
    }

    public function sentMessageToClientAction(){

        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id_order'));

        if ( $this->_request->isPost() ) {
            $message = $this->_request->getPost('text');
            $email = $this->_request->getPost('email');
            $phone = $this->_request->getPost('phone');

            $sentTo = array();
            if($this->_request->getPost('to-phone') && !empty($phone)){
                Sms::send($phone, $message);
                $sentTo[] = 'телефон';
            }

            if($this->_request->getPost('to-email') && !empty($email)){
                $twig = new Environment(new FilesystemLoader(
                    Zend_Registry::getInstance()->config->path->root . 'templates/twig/'
                ));

                $tpl = "mail/order/user/message.twig";

                $body = $twig->render($tpl, array('message'=>$message));
                $mail = new Site_Mail('UTF-8');
                $mail->setBodyHtml($body);
                $mail->setFrom('info@anydasa.com');
                $mail->setSubject('Сообщение по заказу №'.$Order->id);
                $mail->addTo($email, $Order->name);
                $mail->send();

                $sentTo[] = 'почта';
            }

            $OrderHistory = new OrdersHistory;
            $OrderHistory->id_admin = Zend_Registry::get('CurrentAdmin')->id;
            $comment = "<div><strong>Сообщение пользователю";
            if(!empty($sentTo)){
                $comment .= ' (' . implode('+', $sentTo) . ')';
            }
            $comment .= ":</strong></div><div>{$this->_request->getPost('text')}</div>";
            $OrderHistory->comment = $comment;

            $Order->OrdersHistory->add($OrderHistory);

            $Order->save();

            $this->goBack();
        }

        $this->view->Order = $Order;

        $this->render('message-client');
    }

    public function addCommentAction()
    {
        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id_order'));

        if ( $this->_request->isPost() ) {

            $OrderHistory = new OrdersHistory;
            $OrderHistory->id_admin = Zend_Registry::get('CurrentAdmin')->id;
            $OrderHistory->comment = "<div><strong>Комментрий админа:</strong></div><div>{$this->_request->getPost('comment')}</div>";

            $Order->OrdersHistory->add($OrderHistory);

            $Order->save();

            $this->goBack();
        }

        $this->render('add-comment');
    }

    public function basketEditAction()
    {
        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id_order'));

        $this->view->Order = $Order;



        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();

            $Order->Products->clear();


            foreach ($post['item'] as $item) {
                $OrdersProducts = new OrdersProducts;
                $OrdersProducts->fromArray($item);
                if ( !empty($OrdersProducts->code) && $Product = Doctrine::getTable('Products')->findOneBy('code', $OrdersProducts->code) ) {
                    $OrdersProducts->vendor_name        = $Product->OwnerProduct->Vendors->name;
                    $OrdersProducts->vendor_price_buy   = $Product->OwnerProduct->getPrice('buy', 'UAH');
                }
                $Order->Products->add($OrdersProducts);
            }

            $Order->Products->save();


                echo '<script>
                    $("#basket-'.$Order->id.'")
                        .html("'.SF::escapeJavaScriptText( $this->view->render('index/basket-partial.phtml') ).'")
                        .css({borderColor: "Red"})
                        .animate({borderColor: "transparent"}, 3000);

                        closeModalWindow();
                </script>';
                exit;


        }

        $this->render('basket-edit');
    }


    public function exportEmailsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $data = Doctrine_Query::create()
            ->select('DISTINCT email, name, created')
            ->from('Orders')
            ->where('id_status = ?', 'complete')
            ->addWhere('email <> ?', '')
            ->orderBy('created DESC')
            ->fetchArray()
        ;

        array_walk($data, function (&$v) {unset($v['id']);});

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
        $ExcelReport->setHeadRow(array(
            'Email',
            'Имя',
            'Время создания'
        ));

        $ExcelReport->Excel->getActiveSheet()->fromArray($data, NULL, 'A2');

        echo $ExcelReport->getReport();

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename="EmailList.xlsx"')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }

    public function novapayConfirmAction()
    {
        /** @var Orders $Order */
        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id'));

        $paymentSessionId = $Order->novapay_session_id;

        $novapayService = new Novapay_NovapayService();
        $novapayService->paymentConfirmation($paymentSessionId);
        $status = $novapayService->getStatus($paymentSessionId);
        $Order->novapay_payment_status = $status;
        $Order->save();
        return;
    }

    public function novapayRefreshAction()
    {
        /** @var Orders $Order */
        $Order  = $this->findRecordOrException('Orders', $this->_request->getParam('id'));

        $paymentSessionId = $Order->novapay_session_id;

        $novapayService = new Novapay_NovapayService();
        $status = $novapayService->getStatus($paymentSessionId);
        $Order->novapay_payment_status = $status;
        $Order->save();
        return;
    }

}

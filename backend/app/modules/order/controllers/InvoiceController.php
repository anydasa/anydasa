<?

class Order_InvoiceController extends Controller_Account
{

    public function showSentMessageAction()
    {
        $Order = $this->findRecordOrException('Orders', $this->getParam('id'));

        $this->view->Order = $Order;

        $this->render('sent-message');
    }

    public function choiceProductAction()
    {
        $Order = $this->findRecordOrException('Orders', $this->getParam('id'));

        $Products = $Order->OrdersProducts;

        foreach ($Order->OrdersProducts as $ordersProduct){
            $p = Doctrine::getTable('Products')->findOneByCode($ordersProduct->code);
            foreach ($Products as $key => $product){
                if($product->code == $p->code){
                    $Products[$key]['id'] = $p->id;
                }
            }
        }

        $this->view->Products = $Products;
        $this->view->Order = $Order;

        $this->render('choice-product');
    }


    public function createMessageAction()
    {
        $tpl_name = 'post-order-invoice-products';
        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode($tpl_name) ) {
            throw new Exception("Отсутсвует почтовый шаблон $tpl_name (MailTemplates)");
        }
        /** @var Orders $Order */
        $Order = $this->findRecordOrException('Orders', $this->getParam('id'));

        $form = new Form_Order_InvoiceMail('Orders');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        $data = [];
        $data['order_id']  = $Order->id;

        $Products = $Order->OrdersProducts;

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $form_data = $form->getValues();

                $mail = new Site_Mail('UTF-8');
                $mail->setBodyHtml($form_data['body_html']);
                $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
                $mail->addTo($form_data['email']);

                $emails = explode(';', $form_data['emails']);
                foreach ($emails as $email){

                    if(!empty($email)){
                        $mail->addBcc($email);
                    }
                }

                $mail->setSubject($form_data['subject']);

                $prices = $this->_request->getParam('product_price');
                $names = $this->_request->getParam('product_name');
                $total = 0;

                /** @var  $Product OrdersProducts */
                foreach ($Products as $Product){
                    $summ = 0;
                    $price = Currency::format($Product->price, 'UAH', true );
                    $name = $Product->title;


                    if ( !empty($prices[$Product->id]) ) {
                        $price = Currency::format($prices[$Product->id] , 'UAH', true);
                    }

                    $summ = Currency::format($Product->count * $prices[$Product->id], 'UAH', true);
                    $total += $Product->count * $prices[$Product->id];
                    if ( !empty($names[$Product->id]) ) {
                        $name = $names[$Product->id];
                    }

                    $data['products'][] = [
                        'name'        => $name,
                        'code'        => $Product->code,
                        'price'       => $price,
                        'quantity'    => $Product->count,
                        'summ'        => $summ,
                    ];
                }

                $data['order']['name'] = $form_data['name'];
                $data['order']['created'] = $Order->created;
                $data['order']['phone'] = $Order->phone;
                $data['order']['address'] = $Order->address;
                $data['order']['total'] = Currency::format($total, 'UAH', true);

                $xls = $this->getInvoiceFile($data);
                $objWriter = PHPExcel_IOFactory::createWriter($xls, 'Excel2007');
                $filepath = sys_get_temp_dir().'/invoice.xlsx';
                $objWriter->save($filepath);

                $xlsFile = file_get_contents($filepath);

                $at = new Zend_Mime_Part( $xlsFile );
                $at->type        = mime_content_type($filepath);
                $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
                $at->encoding    = Zend_Mime::ENCODING_BASE64;
                $at->filename    = 'invoiсe_'.date('d-m-Y').'.xlsx';
                $mail->addAttachment($at);

                $mail->send();
                
                $html = '<br>';
                $html .= 'Дата отправки: '.date('Y-m-d H:i:s').'<br>';
                $html .= 'Отправил: '.$this->user->name.'<br>';

                foreach ($data['products'] as $product){
                    $html .= $product['name'] . ' - ' . $product['quantity']  . ' x ' . $product['price'] . ' = ' . $product['summ'] . '<br>';
                }

                $html .= 'Итого: '. $data['order']['total'];

                $Order->sent_invoice_html = $form_data['body_html'];

                /** @var OrdersHistory $OrderHistory */
                $OrderHistory = new OrdersHistory;
                $OrderHistory->id_admin = Zend_Registry::get('CurrentAdmin')->id;
                $OrderHistory->comment = "<div><strong>Отправлен счет фактура:</strong></div>";
                $OrderHistory->comment .= '<div>' . $html . '</div>';
                $Order->OrdersHistory->add($OrderHistory);

                $Order->save();

                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender(TRUE);
                echo 'Отправлено';
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $data['username']  = $Order->name;
            $data['phone'] = $Order->phone;
            $data['anketa'] = unserialize($Order->credit);

            $form->email->setValue($Order->email);
            $form->emails->setValue('Eleonora.naumchuk@ukrsibbank.com;i-shop@otpbank.com.ua;lenda@fsc.ua;Maksim.Denisenko@alfabank.kiev.ua');
            $form->name->setValue($Order->name);
            $form->subject->setValue($MailTemplate->subject);
            $form->body_html->setValue($MailTemplate->renderHTML($data));
        }


        $this->view->Products = $Products;

        $this->view->Order = $Order;
        $this->view->form = $form;

        $this->render('create-message');
    }
    
    private function getInvoiceFile($data){
        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $sheet->setTitle('Cчет фактура');

        $sheet->getColumnDimension('A')->setWidth(8);
        $sheet->getColumnDimension('B')->setWidth(10);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('E')->setWidth(3);

        // массив стилей
        $style_wrap = array(
            // рамки
            'borders'=>array(
                // внешняя рамка
                'outline' => array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                ),
                // внутренняя
                'allborders'=>array(
                    'style'=>PHPExcel_Style_Border::BORDER_THIN,
                )
            )
        );
        $style_bold = array(
            // Шрифт
            'font'=>array(
                'bold' => true,
                'name' => 'Calibri',
                'size' => 11,
            ),
        );

        $style_header = array(
            // Шрифт
            'font'=>array(
                'bold' => true,
                'name' => 'Arial',
                'size' => 10,
            ),
        );

        $style_body = array(
            // Шрифт
            'font'=>array(
                'name' => 'Arial',
                'size' => 10,
            ),
        );


        $sheet->getStyle('A1')->applyFromArray($style_bold);
        $sheet->getStyle('C14')->applyFromArray($style_bold);

        $sheet->getStyle('A17:H17')->applyFromArray($style_wrap);
        $sheet->getStyle('A17:H17')->applyFromArray($style_header);

        $sheet->getStyle('C14')->applyFromArray(['font'=>[
            'bold => true'
        ]]);

        $sheet->setCellValue("A1", 'Постачальник');
        $sheet->setCellValue("A2", 'ФОП Бояров Iгор Олександрович');
        $sheet->setCellValue("A3", 'Україна, 03087, м.Київ, вул. Володимирська, 49а,  (044) 332-19-37');
        $sheet->setCellValue("A4", 'Р/р 26009052710537  в КБ "ПРИВАТБАНК", Розрахунковий Центр,м.Київ МФО 300711');
        $sheet->setCellValue("A5", '№ свідоцтва 068103 ЄДРПОУ 2970115073');
        $sheet->setCellValue("A7", 'Покупець:');
        $sheet->setCellValue("B7", $data['order']['name']);
        $sheet->setCellValue("A8", 'Тел:');
        $sheet->setCellValue("B8", $data['order']['phone']);
        $sheet->setCellValue("A9", 'Адреса:');
        $sheet->setCellValue("B9", $data['order']['address']);

        $sheet->setCellValue("C14", 'Счет Фактура № '.$data['order_id'].' от '.date('d.m.Y', strtotime($data['order']['created'])).' г.');

        $sheet->setCellValue("A17", '№');
        $sheet->setCellValue("B17", 'Артикул');
        $sheet->mergeCells('C17:D17');
        $sheet->setCellValue("C17", 'Найменування');
        $sheet->setCellValue("E17", 'Ед.');
        $sheet->setCellValue("F17", 'Кількість');
        $sheet->setCellValue("G17", 'Ціна');
        $sheet->setCellValue("H17", 'Сума');

        $i = 18;

        foreach ($data['products'] as $key => $product){

            $sheet->setCellValue("A".$i , $key + 1);
            $sheet->setCellValue("B".$i , $product['code']);
            $sheet->setCellValue("C".$i , $product['name']);
            $sheet->setCellValue("E".$i , 'шт.');
            $sheet->setCellValue("F".$i , $product['quantity']);
            $sheet->setCellValue("G".$i , $product['price']);
            $sheet->setCellValue("H".$i , $product['summ']);

            $sheet->getStyle('A'.$i.':H'.$i)->applyFromArray($style_wrap);
            $sheet->getStyle('A'.$i.':H'.$i)->applyFromArray($style_body);

            $i++;
        }

        $sheet->getStyle('H'.$i)->applyFromArray($style_wrap);

        $i++;

        $sheet->getStyle('G'.$i.':H'.$i)->applyFromArray($style_wrap);

        $sheet->getStyle('A'.$i.':H'.$i)->applyFromArray($style_header);
        $sheet->setCellValue("G".$i , 'Разом:');
        $sheet->setCellValue("H".$i , $data['order']['total']);

        $i++;

        $imagePath = Zend_Registry::get('config')->path->pix . 'img/stamp.png';

        if (file_exists($imagePath)) {
            $stamp = new PHPExcel_Worksheet_Drawing();
            $stamp->setPath($imagePath);
            $stamp->setCoordinates("F".$i);
            $stamp->setWorksheet($sheet);
        }

        return $xls;

        /*$this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename="AdwordsProduct.xlsx"')
            ->clearBody();

        $this->getResponse()->sendHeaders();*/
    }


}

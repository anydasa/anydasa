<?
class Order_DeliveryController extends Controller_AbstractList
{
    const MODEL_NAME    = 'OrdersDelivery';
    const FORM_NAME     = 'Form_Order_Delivery';
    const ROUTE_PREFIX  = 'order-delivery-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());

        if ( empty($this->_request->getParam('sort')) ) {
            $Query->orderBy('sort');
        }

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
            $this->redirect($_SERVER['REQUEST_URI']);
        }

        parent::listAction($Query);
    }

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        $node->unlink('Payments');
        $node->link('Payments', $this->_request->getPost('Payments'));
    }

    protected function _getNode($id = null)
    {
        if ($node = Doctrine::getTable(static::MODEL_NAME)->find($id)) {
            $node->Payments;
            return $node;
        }
        $this->_redirectToList();
    }
}
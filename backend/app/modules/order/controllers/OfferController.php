<?

class Order_OfferController extends Controller_Account
{

    public function showSentMessageAction()
    {
        $Order = $this->findRecordOrException('Orders', $this->getParam('id'));

        $this->view->Order = $Order;

        $this->render('sent-message');
    }

    public function choiceProductAction()
    {
        $Order = $this->findRecordOrException('Orders', $this->getParam('id'));

        $Products = $Order->getRecommendedProducts();

        array_walk($Products, function (& $item) {
            $item['fullName'] = $item['name'];
            unset($item['name']);
        });

        $this->view->Products = $Products;
        $this->view->Order = $Order;

        $this->render('choice-product');
    }


    public function createMessageAction()
    {
        $tpl_name = 'post-order-offer-products';
        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode($tpl_name) ) {
            throw new Exception("Отсутсвует почтовый шаблон $tpl_name (MailTemplates)");
        }

        $Order = $this->findRecordOrException('Orders', $this->getParam('id'));

        $form = new Form_Order_Mail('Orders');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $form_data = $form->getValues();

                $mail = new Site_Mail('UTF-8');
                $mail->setBodyHtml($form_data['body_html']);
                $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
                $mail->addTo($form_data['email']);
                $mail->setSubject($form_data['subject']);
                $mail->send();

                $Order->sent_offer_html = $form_data['body_html'];
                $Order->save();

                echo 'Отправлено';
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $discounts = $this->_request->getParam('discount');

            $Products = Doctrine_Query::create()
                ->from('Products')
                ->whereIn('id', $this->_request->getParam('product_id'))
                ->execute()
            ;

            $data = [];
            $data['username']  = $Order->name;
            $data['promocode'] = 'OO-' . $Order->id;

            foreach ($Products as $Product) {

                $price = $Product->getFormatedPrice('retail', 'UAH', true);
                $old_price = '';

                if ( !empty($discounts[$Product->id]) ) {
                    $old_price = $price;
                    $price = Currency::format($Product->getPrice('retail', 'UAH', $discounts[$Product->id]), 'UAH', true);
                }

                $data['products'][] = [
                    'url'         => $Product->getUrl(),
                    'name'        => $Product->getFullName(),
                    'price'       => $price,
                    'old_price'   => $old_price,
                    'img'         => $Product->getImage()->getImagesCount() ? $Product->getImage()->getFirstImage()['small']['url'] : ''
                ];
            }

            $form->email->setValue($Order->email);
            $form->subject->setValue($MailTemplate->subject);
            $form->body_html->setValue($MailTemplate->renderHTML($data));
        }



        $this->view->Order = $Order;
        $this->view->form = $form;

        $this->render('create-message');
    }


}

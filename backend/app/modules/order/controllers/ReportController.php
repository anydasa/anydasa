<?

class Order_ReportController extends Controller_AbstractList
{
    const MODEL_NAME    = '';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'order-report-';
    const COUNT_ON_PAGE = 30;

    public function productsAction()
    {
        $params = $this->_request->getQuery();

        $Query = Doctrine_Query::create();
        $Query->from('Products p');
        $Query->select('p.id,
                        count(*) u_count,
                        sum(op.count) count,
                        sum(op.count) FILTER (WHERE o.id_status = \'complete\') AS __completed,
                        sum(op.count) FILTER (WHERE o.id_status = \'canceled\') AS __canceled,
                        sum(op.count) FILTER (WHERE o.id_status NOT IN (\'canceled\',\'complete\')) AS __another,
                        sum(vendor_price_buy * op.count) FILTER (WHERE o.id_status = \'complete\') sum_buy,
                        sum(price * op.count) FILTER (WHERE o.id_status = \'complete\') sum_sale,
                        sum(price * op.count - vendor_price_buy * op.count) FILTER (WHERE o.id_status = \'complete\') __profit

        ');
        $Query->innerJoin('p.OrdersProducts op');
        $Query->innerJoin('op.Orders o');
        $Query->groupBy('p.id');

        $this->applyParamsToQuery($Query, $params);


        $pager = new Doctrine_Pager($Query, $this->params['page'], static::COUNT_ON_PAGE);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];

        $this->render('products');

    }

    private function applyParamsToQuery(Doctrine_Query $Query, $params)
    {
        if (!empty($params['sort'])) {
            $column = $params['sort'];
            $type = '';
            if ( strstr($column, '-') ) {
                list($column, $type) = explode('-', $column);
            }
            $Query->orderBy($column.' '.$type . ' NULLS LAST');
        } else {
            $Query->orderBy('u_count DESC');
        }

        if ( !empty($params['created']) ) {
            list($from, $to) = explode('~', $params['created']);
            $Query->addWhere('DATE(o.created) >= ?', $from);
            $Query->addWhere('DATE(o.created) <= ?', $to);
        }

        if ( !empty($params['payment_type']) ) {
            $Query->addWhere('o.payment_type= ?', $params['payment_type']);
        }

        if ( !empty($params['id_catalog']) ) {
            $Query->addWhere('p.catalogs @> ARRAY['.$params['id_catalog'].']');
        }
    }

    public function productAction()
    {
        $Product = $this->findRecordOrException('Products', $this->getParam('id'));

        $this->view->Product = $Product;

        $sql = 'SELECT
                    to_char(o.created, \'YYYY-MM\') AS date,
                    SUM(op.count) FILTER (WHERE o.id_status = \'complete\') AS complete,
                    SUM(op.count) FILTER (WHERE o.id_status = \'canceled\') AS canceled
                FROM orders_products op
                INNER JOIN orders o ON (o.id = op.id_order)
                WHERE code = ?
                GROUP BY date
                ORDER BY date';

        $stmt = Doctrine_Manager::connection()->execute($sql, [$Product->code]);

        $this->view->ChartData = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $this->render('product');
    }

    public function productOrdersAction()
    {
        $Product = $this->findRecordOrException('Products', $this->getParam('id'));

        $this->view->Product = $Product;

        $Query = Doctrine_Query::create()
            ->from('Orders o')
            ->innerJoin('o.OrdersProducts op')
            ->where('code = ?', $Product->code)
            ->orderBy('o.created DESC');

        $pager = new Doctrine_Pager($Query, $this->params['page'], 50);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];


        $this->render('product-orders');
    }

    public function reportCountPerDayAction()
    {
        $sql = 'SELECT
                        to_char(o.created, \'YYYY-MM-DD\') AS date,
                    COUNT(*) AS all,
                    COUNT(*) FILTER (WHERE o.id_status = \'complete\') AS complete_all,
                    COUNT(*) FILTER (WHERE o.id_status = \'canceled\') AS canceled_all,
                    COUNT(*) FILTER (WHERE who_created IS NULL) AS user,
                    COUNT(*) FILTER (WHERE who_created IS NOT NULL) AS admin
                FROM orders o
                GROUP BY date
                ORDER BY date';

        $stmt = Doctrine_Manager::connection()->execute($sql);

        $this->view->ChartData = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $this->render('report-count-per-day');
    }

}

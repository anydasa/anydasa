<?
class Order_DepotsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'OrdersDepots';
    const FORM_NAME     = 'Form_Order_Depots';
    const ROUTE_PREFIX  = 'order-depots-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());

        if ( empty($this->_request->getParam('sort')) ) {
            $Query->orderBy('sort');
        }

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
            $this->redirect($_SERVER['REQUEST_URI']);
        }

        parent::listAction($Query);
    }
}
<?

class Order_ExcelController extends Controller_Account
{
    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $Order = Doctrine::getTable('Orders')->find($this->params['id']);

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
        $ExcelReport->setHeadRow(array('ID', 'Код', 'Название', 'Цена', 'Кол-во', 'Поставщик', 'Закупка'));

        foreach ($Order->OrdersProducts as $item) {
            $ExcelReport->addDataRow(array(
                $item->id,
                $item->code,
                $item->title,
                Currency::format($item->price, 'USD'),
                $item->count,
                $item->VendorsProducts->exists() ? $item->VendorsProducts->Vendors->name : NULL,
                $item->VendorsProducts->exists() ? Currency::format($item->VendorsProducts->price_buy, Currency::getById($item->VendorsProducts->id_currency)->iso) : NULL,
            ));
        }

        echo $ExcelReport->getReport();

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename=Арморс.Заказ.'.$Order->email.' - '.date('H,i Y.m.d').'.xlsx')
            ->clearBody();

        $this->getResponse()->sendHeaders();

    }
}

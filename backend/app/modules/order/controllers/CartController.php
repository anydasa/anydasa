<?

class Order_CartController extends Controller_Account
{
    const FORM_NAME     = 'Form_Order_Cart';
    const ROUTE_PREFIX  = 'order-cart-';
    const COUNT_ON_PAGE = 50;

    public function editAction()
    {

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();
            Cart::getInstance()->clearProducts();
            foreach ($post['item'] as $item) {

                if ( !empty($item['code']) && $Product = Doctrine::getTable('Products')->findOneBy('code', $item['code']) ) {
                    Cart::getInstance()->setProduct($Product->id, $item['count'], $item['price']);
                }

            }

            $products = Cart::getInstance()->getProducts();

            foreach ($products as $key=>$product){

                $owner = Products_Query::create()
                    ->setOnlyPublic()
                    ->where('p.id = ?', $key)
                    ->fetchOne();

                if($owner->id_set){

                    $ProductsSetsProduct = Doctrine_Query::create()
                        ->from('ProductsSetsProduct')
                        ->where('id_set = ?', $owner->id_set)
                        ->execute();

                    $used_groups = array();
                    $discountProducts = [];

                    foreach ($ProductsSetsProduct as $setsProduct){
                        if(isset($products[$setsProduct->id_product]) && !isset($used_groups[$setsProduct->group_num])){
                            $discountProducts[$setsProduct->group_num] = $setsProduct->id_product;
                        }
                    }

                    if(!empty($discountProducts)){
                        Cart::getInstance()->setProductsSet($owner, Products_Query::create()
                            ->setOnlyPublic()
                            ->whereIn('p.id', $discountProducts)
                            ->execute());
                    }


                }
            }
            $this->view->Products = Cart::getInstance()->getProducts();

            echo '<script>
                    $("#cart")
                        .html("'.SF::escapeJavaScriptText( $this->view->render('index/cart-partial.phtml') ).'")
                        .css({borderColor: "Red"})
                        .animate({borderColor: "transparent"}, 3000);

                        closeModalWindow();
                        addDeliveryToCart();
                        addPaymentToCart();
                </script>';



        }

        $this->view->Products = Cart::getInstance()->getProducts();



    }

}

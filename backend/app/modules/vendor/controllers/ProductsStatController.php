<?

class Vendor_ProductsStatController extends Controller_AbstractList
{
    const MODEL_NAME    = 'VendorsProductsStat';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'vendor-products-stat-';
    const COUNT_ON_PAGE = 20;

    public function listAction()
    {
        $this->view->VendorProduct = Doctrine::getTable('VendorsProducts')->find($this->_request->getQuery()['id_product']);
        parent::listAction();
    }
}

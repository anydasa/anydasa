<?
class Vendor_PriceController extends Controller_Account
{

    public function uploadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $Vendor = Doctrine::getTable('Vendors')->find($this->params['id']);

        $form = new Form_Vendor_Price('Vendor');
        $form->setAction($_SERVER['REQUEST_URI']);

        $form_description = Zend_Registry::get('myCurrentRoute')->title . '<br><span style="color: Red;">' . $Vendor->name .'</span>';

        if ( $Vendor->getFileUrl() ) {
            $form_description .=
                '<div style="font-size: 12px; font-weight: normal; margin: 10px;">
                    <p>Есть шаблон-прайс. <a href="'.$this->view->url(array('id' => $Vendor->id), 'vendor-price-config').'">использовать</a></p>
                    <p>При загрузке шаблон-прайс будет заменен на загружаемый</p>
                </div>';
        }

        $form->setDescription($form_description);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $Vendor->saveFile($form->file->getFileInfo()['file']['tmp_name']);
                $this->redirect($this->view->url(array('id'=>$this->params['id']), 'vendor-price-config'));
            }
        }

        echo $form;
    }

    public function processAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $Vendor = Doctrine::getTable('Vendors')->find($this->params['id']);

        $form = new Form_Vendor_Price('Vendor');
        $form->setAttrib('data-target', 'self');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title. '<br><span style="color: Red;">' . $Vendor->name .'</span>');

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {

                $Stat = $Vendor->processUploaded($form->file->getFileInfo()['file']['tmp_name']);

                echo 'Распознано: '.$Stat->count_valid_rows.' Пропущено: '.$Stat->count_invalid_rows.'<br>';
                echo 'Сохранено: ' .$Stat->count_saved_rows.' (обновленно: '.$Stat->count_updated_rows.'; добавленно: '.$Stat->count_inserted_rows.')<br>';
                echo 'Не сохранено: '.$Stat->count_notsaved_rows;
                return;
            }
        }

        echo $form;
    }

    public function processByUrlAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $Vendor = Doctrine::getTable('Vendors')->find($this->params['id']);

        if ( !empty($Vendor->url_file) OR !empty($Vendor->google_drive_key) ) {

            try {
                $Stat = $Vendor->process();
                echo 'Распознано: '.$Stat->count_valid_rows.' Пропущено: '.$Stat->count_invalid_rows.'<br>';
                echo 'Сохранено: ' .$Stat->count_saved_rows.' (обновленно: '.$Stat->count_updated_rows.'; добавленно: '.$Stat->count_inserted_rows.')<br>';
                echo 'Не сохранено: '.$Stat->count_notsaved_rows;
                return;
            } catch (Exception $e) {
                Zend_Debug::dump($e);
            }
        }

    }

    public function configAction()
    {
        $Vendor  = Doctrine::getTable('Vendors')->find($this->params['id']);

        try {
            $Excel = PHPExcel_IOFactory::load($Vendor->getFilePath());
        } catch (Exception $e) {
            $this->redirect($_SERVER['HTTP_REFERER']);
        }


        $PriceImport = new Site_PriceImport($Excel, $Vendor);
        $PriceImport->Excel->setActiveSheetIndex((integer)$this->params['sheet']);

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();

            $Config = $PriceImport->getConfig();
            $Config->setColumns($post['columns']);
            $Config->setMacros($post['macros']);
            $Config->setHeaderCountRows($post['header_count_rows']);

            if ( isset($post['saveDefaultMacrosList']) ) {
                $Config->setDefaultMacros($post['macros']);
            }

            $Config->save();

            $this->redirect($_SERVER['REQUEST_URI']);
        }

        $Separator = $PriceImport->getSeparator();
        $Separator->separate();

        $this->view->Vendor             = $Vendor;
        $this->view->PriceImport        = $PriceImport;
        $this->view->CollectionValues   = $Separator->getValues();
        $this->view->CollectionGarbage  = $Separator->getGarbage();
    }

}
<?
class Vendor_StatController extends Controller_AbstractList
{
    const MODEL_NAME    = 'VendorsStat';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'vendor-stat-';
    const COUNT_ON_PAGE = 20;

    public function listAction()
    {
        $this->view->Vendor = Doctrine::getTable('Vendors')->find($this->params['id_vendor']);
        parent::listAction();
    }
}
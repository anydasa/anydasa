<?
class Vendor_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Vendors';
    const FORM_NAME     = 'Form_Vendor';
    const ROUTE_PREFIX  = 'vendor-';
    const COUNT_ON_PAGE = 100;

    public function listAction()
    {
        parent::listAction();
        $sql = 'SELECT
                    id_vendor,
                    COUNT(*) total_count,
                    COUNT(*) FILTER (WHERE is_actual = 1) actual_count,
                    COUNT(*) FILTER (WHERE is_actual = 1 AND id_product IS NOT NULL) actual_linked_count,
                    COUNT(*) FILTER (WHERE id_product IS NOT NULL) linked_count
                FROM vendors_products
                GROUP BY id_vendor';
        $stmt = Doctrine_Manager::connection()->execute($sql);

        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $this->view->agg_count = array_combine(
            array_column($res, 'id_vendor'),
            $res
        );
    }

    protected function getQuery($params)
    {
        $Query = parent::getQuery($params);

        if ( empty($params['sort']) ) {
            $Query->orderBy('last_time_processed IS NULL DESC');
            $Query->addOrderBy('NOW() < last_time_processed + (INTERVAL \'1 sec\' * interval_actual)');
            $Query->addOrderBy('NOW() < last_time_processed + (INTERVAL \'1 sec\' * interval_process)');
        }

        if ( 'overdue' == $params['q'] ) {
            $Query->addWhere('NOW() > last_time_processed + (INTERVAL \'1 sec\' * interval_actual)');
        }
        if ( 'need_process' == $params['q'] ) {
            $Query->addWhere('NOW() > last_time_processed + (INTERVAL \'1 sec\' * interval_process)');
        }

        return $Query;
    }

    public function viewAction()
    {
        $this->view->Vendor = $this->findRecordOrException(static::MODEL_NAME, $this->params['id']);
        $this->render('view');
    }

    public function toogleProductsAction()
    {
        $node = $this->_getNode($this->params['id']);
        Doctrine_Query::create()
            ->update('VendorsProducts')
            ->set('is_enabled', $this->params['is_enabled'])
            ->where('id_vendor = ?', $node->id)
            ->execute();

        $this->_redirectToList();
    }
}
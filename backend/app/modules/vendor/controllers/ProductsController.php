<?

class Vendor_ProductsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'VendorsProducts';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'vendor-products-';
    const COUNT_ON_PAGE = 50;

    public function viewAction()
    {
        $Product = $this->findRecordOrException(static::MODEL_NAME, $this->params['id']);
        $this->view->Product = $Product;

        $this->render('view');
    }

    public function deleteSelectedAction()
    {
        $Records = Doctrine_Query::create()->from(static::MODEL_NAME)->whereIn('id', explode(',', $_COOKIE['CollectionRowToggle_'.static::MODEL_NAME]))->execute();
        $Records->delete();
        setcookie('CollectionRowToggle_'.static::MODEL_NAME, NULL, 0, '/');
        $this->_redirectToList();
    }

    public function createFromSelectedAction()
    {
        $form = new Form_Vendor_CreateFromSelected(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);

        $Records = Doctrine_Query::create()->from(static::MODEL_NAME)->whereIn('id', explode(',', $_COOKIE['CollectionRowToggle_'.static::MODEL_NAME]))->execute();

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $post = $this->_request->getPost();
                foreach ($Records as $Record) {
                    $Product = new Products;
                    $Product->fromArray($Record->toArray());
                    $Product->title         = $post['product_title'][$Record->id];
                    $Product->alias         = SF::slugify($post['product_title'][$Record->id]);
                    $Product->status        = Products::STATUS_ORDER;
                    $Product->is_visible    = 1;
                    $Product->type          = $post['type'];
                    $Product->id_state      = $post['id_state'];
                    $Product->balance       = $Record->balance ? $Record->balance : 0;
                    $Product->id_brand      = $form->getValue('id_brand');
                    $Product->link('Catalogs', $form->getValue('id_catalog'));

                    if ( !empty($Record->url) ) {
                        $Product->ProductsLinks->add( (new ProductsLinks)->set('url', $Record->url) );
                    }

                    try {
                        $Product->save();
                        $Product->refresh();

                        $Record->id_product = $Product->id;
                        $Record->save();

                        echo '<div style="color: Green;">'.$Product->title.'</div>';
                    } catch (Exception $ignored) {
                        echo '<div style="color: Red;">'.$Product->title.'</div>';
                    }
                }
                setcookie('CollectionRowToggle_'.static::MODEL_NAME, NULL, 0, '/');
                return;
            }
        } else {
            foreach ($Records as $Record) {
                if ( !empty($Record->brand) && $id_brand = Doctrine_Query::create()->select('id')->from('ProductsBrands')->where('title ILIKE ?', '%'.$Record->brand.'%')->fetchOne()->id ) {
                    $form->getElement('id_brand')->setValue($id_brand);
                }
            }
        }

        $descr = '<ul id="selected-list" style="margin:20px 0; font-weight: normal; font-size: 12px;">';
        foreach ($Records as $Record) {
            $descr .= '<li><span>'.$Record->title.'</span>';
            $descr .= '<input type="hidden" name="product_title['.$Record->id.']" />';
        }
        $descr .= '</ul>';
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title.$descr);

        echo $form;
    }

    public function linkAction()
    {
        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();
            $node = $this->findRecordOrException(static::MODEL_NAME, $post['id_vendor_product']);

            $node->id_product = $this->findRecordOrException('Products', $post['id_product'])->id;
            $node->save();
        }
        $this->goBack();
    }

    public function unlinkAction()
    {
        if ( $node = $this->_getNode($this->params['id']) ) {
            $node->id_product = NULL;
            $node->save();
        }

        $this->goBack();
    }
    public function toggleActualAction()
    {
        Doctrine_Manager::connection()->prepare('SELECT vendors_products_toggle_actual()')->execute();
        $this->goBack();
    }
}
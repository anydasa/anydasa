<?
class Vendor_AutofitProductsController extends Controller_Account
{
    public function equalTitleAction()
    {
        $Query = Doctrine_Query::create()
            ->from('Products p')
            ->innerJoin('p.VendorsProducts vp ON p.title = vp.title')
            ->where('vp.id_product IS NULL');

        $pager = new Doctrine_Pager($Query, $this->params['page'], 100);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
    }

    public function searchCodeAction()
    {
        $sql = "SELECT vp.title vp_title,
                      p.title p_title,
                      p.id p_id,
                      vp.id vp_id
                FROM products p, vendors_products vp
                WHERE p.manufacturer_code_auto = substring(vp.title from '\(([A-Z][A-Z0-9]+?)\)')
                  AND vp.id_product IS NULL
                  AND p.manufacturer_code_auto IS NOT NULL
                  AND vp.title <> p.title";

        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->execute();
        $this->view->list = $stmt->fetchAll(Doctrine_Core::FETCH_ASSOC);
    }
}
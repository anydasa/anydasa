<?

class Users_EmailController extends Controller_AbstractList
{
    const MODEL_NAME    = 'UsersEmails';
    const FORM_NAME     = 'Form_User_Email';
    const ROUTE_PREFIX  = 'users-email-';
    const COUNT_ON_PAGE = 20;

    protected function getQuery($params)
    {
        $Query = parent::getQuery($params);
        $Query->addWhere('id_user = ?', $this->params['id_user']);
        $Query->orderBy('email');

        return $Query;
    }

    protected function _preSave(& $node, $form)
    {
        $node->id_user = $this->params['id_user'];
    }
}

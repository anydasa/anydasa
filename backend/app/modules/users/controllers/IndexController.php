<?

class Users_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Users';
    const FORM_NAME     = 'Form_User';
    const ROUTE_PREFIX  = 'users-';
    const COUNT_ON_PAGE = 20;


    public function generateTokenAction()
    {
        $node = $this->_getNode($this->params['id']);
        $node->generateToken();
        $node->save();
        $this->_redirectToList();
    }

    public function diffAction()
    {
        $form = new Form_User_Diff('Diff');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setAttrib('data-return-to', 'window');

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {

                $filename = $form->file->getFileInfo()['file']['tmp_name'];

                $Excel = PHPExcel_IOFactory::load($filename);
                $t = array();
                $users = Doctrine::getTable('Users')->findAll()->toKeyValueArray('email', 'company_name');

                foreach($Excel->getActiveSheet()->getRowIterator() as $row) {
                    $it = $row->getCellIterator();
                    $it->setIterateOnlyExistingCells(false);
                    foreach ($it as $cell) {
                        $value = $cell->getCalculatedValue();

                        if ( preg_match_all('/[a-z\d._%+-]+@[a-z\d.-]+\.[a-z]{2,4}\b/i', $value, $res) ) {
                            $C = $cell->getColumn();
                            $R = $cell->getRow();
                            $objRichText = new PHPExcel_RichText();
                            foreach ($res[0] as $email) {

                                if ( isset($users[$email]) ) {
                                    $objBold = $objRichText->createText($users[$email] . ': '. $email);
                                } else {
                                    $objBold = $objRichText->createTextRun($email);
                                    $objBold->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                }
                                $objRichText->createText('; ');
                            }

                            $t[++$C.$R] = $objRichText;

                        }
                    }
                }

                foreach ($t as $key=>$val) {
                    $Excel->getActiveSheet()->getCell($key)->setValue($val);
                }


                $ExcelWriter = new PHPExcel_Writer_Excel2007($Excel);
                $ExcelWriter->save('php://output');

                $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->setHeader('Pragma', 'public', true)
                    ->setHeader('Set-Cookie:', 'fileDownload=true; path=/', true)
                    ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                    ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
                    ->setHeader('Content-Transfer-Encoding', 'binary')
                    ->setHeader('Content-Disposition', 'attachment; filename=ProductsList.xlsx')
                    ->clearBody();

                $this->getResponse()->sendHeaders();
                exit;

            }

        }

        echo $form;
    }

    public function setPassAction()
    {
        $node = $this->_getNode($this->params['id']);
        $form = new Form_Pass(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title . ' для пользователя №'. $node->id);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {

                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList();
            }
        } else {
            $form->populate($node->toArray());
        }

        echo $form;
    }
}

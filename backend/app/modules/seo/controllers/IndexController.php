<?
class Seo_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Seo';
    const FORM_NAME     = 'Form_Seo';
    const ROUTE_PREFIX  = 'seo-';
    const COUNT_ON_PAGE = 50;
}
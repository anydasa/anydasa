<?
class Article_GroupController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ArticleGroup';
    const FORM_NAME     = 'Form_Article_Group';
    const ROUTE_PREFIX  = 'article-group-';
    const COUNT_ON_PAGE = 50;

}
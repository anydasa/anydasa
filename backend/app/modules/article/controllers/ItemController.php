<?
class Article_ItemController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ArticleItem';
    const FORM_NAME     = 'Form_Article_Item';
    const ROUTE_PREFIX  = 'article-item-';
    const COUNT_ON_PAGE = 50;

    public function listAction($Query = null)
    {
        if ( is_null($Query) ) {
            $Query = $this->getQuery($this->_request->getQuery());
        }

        $countSymbols = 0;

        if ( $this->_request->isXmlHttpRequest() && strstr($this->_request->getHeader('Accept'), 'application/json')) {
            $__field = $this->_request->getQuery()['__field'];
            $__field_escape = str_replace('|', '.', $__field);
            if ( !empty($__field) ) {
                $Query->select("DISTINCT ($__field_escape) as $__field_escape");
                $Query->removeDqlQueryPart('orderby');
            }

            $result = $Query->limit(20)->execute()->toArray();

            foreach ($result as & $item) {
                $item[$__field] = $item[$__field_escape];
                unset($item[$__field_escape]);
            }

            return $this->_helper->json($result);
        }

        $this->_helper->viewRenderer->setNoRender(false);

        $pager = new Doctrine_Pager($Query, $this->params['page'], static::COUNT_ON_PAGE);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $this->view->count = count($Query->execute());

        foreach ($Query->execute() as $item){
            $text = strip_tags($item->text);
            $text = str_replace("&nbsp;", '', $text);
            $text = preg_replace('/(\s+)/i', " ", $text);
            $text = str_replace(" ", '', $text);

            $countSymbols += mb_strlen($text);
        }

        $this->view->countSymbols = $countSymbols;

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function editAction()
    {

        $node = $this->_getNode($this->params['id']);
        $form = $this->getForm();

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                if(!$node->is_enabled && $form->getValue('is_enabled')){
                    $node->date_publication = date('Y-m-d H:i:s');
                }
                $node->fromArray($form->getValues());

                $this->_preSave($node, $form);
                $node->save();
                $this->_postEdit($node, $form);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        echo $form;
    }

    protected function _postSave($node, $form)
    {
        if ( !empty($form->getValue('image_delete')) ) {
            @unlink($node->getImagePath());
        }

        if ( !empty($form->getValue('image')) ) {
            $target = $node->getImagePath();

            $form->image->addFilter('Rename', array('target' => $target, 'overwrite'  => true));
            $form->image->receive();

            $im = new Imagick($target);
            $im->resizeImage(500, 375, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();

        }

    }

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        if(!$node->id){
            $node->user_id = $this->user->id;
        }
    }
}
<?
class Spelling_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Spelling';
    const FORM_NAME     = 'Form_Spelling';
    const ROUTE_PREFIX  = 'spelling-';
    const COUNT_ON_PAGE = 50;
}
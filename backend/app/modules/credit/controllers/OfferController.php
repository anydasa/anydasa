<?
class Credit_OfferController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CreditCompanyOffers';
    const FORM_NAME     = 'Form_Credit_Offer';
    const ROUTE_PREFIX  = 'credit-offer-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $this->view->Company = $this->findRecordOrException('CreditCompany', $this->params['id_company']);

        $Query = $this->getQuery($this->_request->getQuery());
        $Query->addWhere('id_company = ?', $this->params['id_company']);
        $Query->orderBy('sort');

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

    protected function getForm()
    {
        $form = parent::getForm();

        $Company = !empty($this->params['id_company']) ?
            $this->findRecordOrException('CreditCompany', $this->params['id_company']) :
            $this->findRecordOrException(static::MODEL_NAME, $this->params['id'])->CreditCompany;

        $form->setDescription( $form->getDescription() . ' для ' . $Company->name);

        $form->getElement('id_company')->setValue($Company->id);

        return $form;
    }

}
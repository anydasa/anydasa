<?
class Credit_IndexController extends Controller_Main
{
    public function productAction()
    {

        if ( $Product = Doctrine::getTable('Products')->find(intval($this->_request->getParam('id_product'))) AND
            $Product->isAvailableForOrder() AND
            !Cart::getInstance()->issetProduct($Product->id)
        ) {
            Cart::getInstance()->setProduct($Product->id);
            $this->view->Product = $Product;
        }

        Credit::setParam('redirect', $this->view->url([], 'order-add'));
        Credit::setParam('btn_name', 'Перейти к оформлению заказа');

        $this->forward('list');
    }

    public function orderAction()
    {
        Credit::setParam('redirect', $this->view->url([], 'order-add'));
        Credit::setParam('btn_name', 'Продолжить оформление заказа');

        $this->forward('list');
    }

    public function listAction()
    {
        if ( empty(Credit::getParam('redirect')) ) {
            Credit::setParam('redirect', $this->view->url([], 'order-add'));
        }
        if ( empty(Credit::getParam('btn_name')) ) {
            Credit::setParam('btn_name', 'Перейти к оформлению заказа');
        }

        $this->view->Credit     = Credit::getInstance();
        $this->view->Cart       = Cart::getInstance();
        $this->view->commission = Doctrine::getTable('OrdersPayment')->find('credit')->commission;
        $this->view->sum        = Cart::getInstance()->getSum($this->view->commission);
    }

    public function offerAction()
    {
        $Offer = Credit::getOfferById( $this->_request->getParam('id_offer') );

        if ( !$Offer ) {
            $this->forward('index');
        }

        Cart::setPayment('credit');

        if ( $this->_request->isPost() ) {

            $OrderSess = new Zend_Session_Namespace('Order');
            $OrderSess->steps['payment']['data']['payment'] = 'credit';

            Credit::setParam('Offer', $Offer);
            Credit::setParam('params', $this->_request->getPost('credit'));
            echo '<script>
                    $("#Orders input[name=\"offer\"]").val("'.Credit::getParam('Offer')->CreditCompany->name . ' - ' . Credit::getParam('Offer')->title.'");
                    closeModalWindow();
                </script>';
            exit();
        } else {
            Credit::setParam('params',  [
                'sum' => Currency::round(Cart::getInstance()->getSum(Doctrine::getTable('OrdersPayment')->find('credit')->commission), 'UAH'),
                'first_payment_percent' =>  $Offer->min_first_payment_percent,
                'month' =>  $Offer->max_month
            ]);
        }

        $this->view->calc_params = Credit::getParam('params');
        $this->view->Offer = $Offer;
    }

    public function profileAction()
    {
        $Offer = Credit::getOfferById( $this->_request->getParam('id_offer') );
        $form = new Form_CreditProfile($Offer);
        $form->setAction($_SERVER['REQUEST_URI']);

        if ( !$Offer->isRequiredProfile() ) {
            $this->redirect($this->view->url([], 'order'));
        }

        if ( $this->_request->isPost() ) {
            if ( $form->isValid($this->_request->getPost()) ) {
                Credit::setParam('profile', $form->getAssociatedValues() );
                echo '<script>
                    closeModalWindow();
                </script>';
                exit();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate(Credit::hasParam('profile') ? Credit::getParam('profile') : []);
        }

        $this->view->Offer = $Offer;
        $this->view->form = $form;
    }
}
<?
class Credit_CompanyController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CreditCompany';
    const FORM_NAME     = 'Form_Credit_Company';
    const ROUTE_PREFIX  = 'credit-company-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        $Query->orderBy('sort');

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

}
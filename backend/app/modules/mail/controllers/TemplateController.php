<?

class Mail_TemplateController extends Controller_AbstractList
{
    const MODEL_NAME    = 'MailTemplates';
    const FORM_NAME     = 'Form_Mail_Template';
    const ROUTE_PREFIX  = 'mail-template-';
    const COUNT_ON_PAGE = 20;
}
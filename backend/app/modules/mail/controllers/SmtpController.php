<?
class Mail_SmtpController extends Controller_AbstractList
{
    const MODEL_NAME    = 'MailSmtp';
    const FORM_NAME     = 'Form_Mail_Smtp';
    const ROUTE_PREFIX  = 'mail-smtp-';
    const COUNT_ON_PAGE = 20;
}
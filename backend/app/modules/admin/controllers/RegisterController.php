<?
class Admin_RegisterController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Register';
    const FORM_NAME     = 'Form_Admin_Register';
    const ROUTE_PREFIX  = 'admin-register-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->_helper->layout->enableLayout();

        $Query = Doctrine_Query::create()->from(static::MODEL_NAME);

        if ( !empty($this->params['sort']) ) {
            $Query->orderBy(str_replace('-', ' ', $this->params['sort']));
        }

        $pager = new Doctrine_Pager($Query, $this->params['page'], static::COUNT_ON_PAGE);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }
}
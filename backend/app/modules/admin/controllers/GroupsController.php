<?

class Admin_GroupsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdminGroups';
    const FORM_NAME     = 'Form_Admin_Group';
    const ROUTE_PREFIX  = 'admin-groups-';
    const COUNT_ON_PAGE = 50;

    public function accessAction()
    {
        $node = $this->findRecordOrException(static::MODEL_NAME, $this->params['id']);


        if ($this->_request->isPost()) {
            $node->unlink('Access');
            $node->link('Access', $this->_request->getPost()['id_route']);
            $node->save();
            $this->_redirectToList();
        }

        $Query = parent::getQuery($this->_request->getQuery(), 'AdminRoutes');

        $Routes = $Query->orderBy('module, controller, action')->execute();

        $this->view->list = $Routes;
        $this->view->selected = $node->AdminGroupsRefRoutes->toKeyValueArray('route_id', 'route_id');

        echo $this->view->render('access.phtml');
    }

}

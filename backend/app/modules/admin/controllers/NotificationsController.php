<?
class Admin_NotificationsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdminNotifications';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'admin-notifications-';
    const COUNT_ON_PAGE = 50;

    public function viewAction()
    {
        $this->view->node = $this->_getNode($this->params['id']);

        $this->view->node->is_readed = 1;
        $this->view->node->save();

        $this->render('view');
    }

    public function setAllReadedAction()
    {
        Doctrine_Query::create()->update(self::MODEL_NAME)->set('is_readed', 1)->execute();
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}
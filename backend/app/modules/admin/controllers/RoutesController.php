<?

class Admin_RoutesController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdminRoutes';
    const FORM_NAME     = 'Form_Admin_Route';
    const ROUTE_PREFIX  = 'admin-routes-';
    const COUNT_ON_PAGE = 50;

    public function setAction()
    {
        $form = new Form_Admin_Router_Set('AdminRoutes');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);


        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues();

                $Collection = new Doctrine_Collection('AdminRoutes');

                $Router = new AdminRoutes;
                $Router->fromArray([
                    'title'     => 'Список '.$values['title'],
                    'type'      => 'Zend_Controller_Router_Route_Static',
                    'name'      => $values['module'].'-'.$values['controller'].'-list',
                    'route'     => $values['module'].'/'.$values['controller'].'/list',
                    'reverse'   => $values['module'].'/'.$values['controller'].'/list',
                    'module'    => $values['module'],
                    'controller'=> $values['controller'],
                    'action'    => 'list',
                    'map'       => '{}',
                ]);
                $Collection->add($Router);

                $Router = new AdminRoutes;
                $Router->fromArray([
                    'title'     => 'Добавить '.$values['title'],
                    'type'      => 'Zend_Controller_Router_Route_Static',
                    'name'      => $values['module'].'-'.$values['controller'].'-add',
                    'route'     => $values['module'].'/'.$values['controller'].'/add',
                    'reverse'   => $values['module'].'/'.$values['controller'].'/add',
                    'module'    => $values['module'],
                    'controller'=> $values['controller'],
                    'action'    => 'add',
                    'map'       => '{}',
                ]);
                $Collection->add($Router);

                $Router = new AdminRoutes;
                $Router->fromArray([
                    'title'     => 'Редактировать '.$values['title'],
                    'type'      => 'Zend_Controller_Router_Route_Regex',
                    'name'      => $values['module'].'-'.$values['controller'].'-edit',
                    'route'     => $values['module'].'/'.$values['controller'].'/edit/(\d+)',
                    'reverse'   => $values['module'].'/'.$values['controller'].'/edit/%s',
                    'module'    => $values['module'],
                    'controller'=> $values['controller'],
                    'action'    => 'edit',
                    'map'       => '{id}',
                ]);
                $Collection->add($Router);

                $Router = new AdminRoutes;
                $Router->fromArray([
                    'title'     => 'Удалить '.$values['title'],
                    'type'      => 'Zend_Controller_Router_Route_Regex',
                    'name'      => $values['module'].'-'.$values['controller'].'-delete',
                    'route'     => $values['module'].'/'.$values['controller'].'/delete/(\d+)',
                    'reverse'   => $values['module'].'/'.$values['controller'].'/delete/%s',
                    'module'    => $values['module'],
                    'controller'=> $values['controller'],
                    'action'    => 'delete',
                    'map'       => '{id}',
                ]);
                $Collection->add($Router);


                $Collection->save();

                $this->redirect('/admin/routes/list?module='.$values['module'].'&controller='.$values['controller']);
            }

            $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
        }

        echo $form;
    }

}

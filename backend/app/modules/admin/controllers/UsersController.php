<?

class Admin_UsersController extends Controller_Account
{
    private $modelName = 'AdminUsers';
    private $formName  = 'Form_Admin_User';

    public function preDispatch()
    {
        parent::preDispatch();
        $this->view->routePrefix    = 'admin-users-';
        $this->view->modelName = $this->modelName;

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function listAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->_helper->layout->enableLayout();

        $Query = Doctrine_Query::create()->from($this->modelName);

        if ( !empty($this->params['sort']) ) {
            $Query->orderBy(str_replace('-', ' ', $this->params['sort']));
        }

        $pager = new Doctrine_Pager($Query, $this->params['page'], 50);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function setPassAction()
    {
        $node = $this->_getNode($this->params['id']);
        $form = new Form_Pass($this->modelName);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title . ' для пользователя №'. $node->id);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {

                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList();
            }
        } else {
            $form->populate($node->toArray());
        }

        echo $form;
    }

    public function accessAction()
    {
        $node = $this->_getNode($this->params['id']);

        if ($this->_request->isPost()) {
            $node->unlink('Access');
            $node->link('Access', $this->_request->getPost()['id_route']);
            $node->save();
            $this->_redirectToList();
        }

        $this->view->list = Doctrine_Query::create()
            ->from('AdminRoutes')
            ->orderBy('module, controller, action')
            ->execute();

        $this->view->disabled = $node->AdminGroups->AdminGroupsRefRoutes->toKeyValueArray('route_id', 'route_id');
        $this->view->selected = $node->AdminUsersRefRoutes->toKeyValueArray('route_id', 'route_id') + $this->view->disabled;

        echo $this->view->render('access.phtml');

    }

    public function editAction()
    {
        $node = $this->_getNode($this->params['id']);
        $form = new $this->formName($this->modelName);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList();
            }
        } else {
            $form->populate($node->toArray());
        }

        echo $form;
    }

    public function addAction()
    {
        $form = new $this->formName($this->modelName);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node = new $this->modelName;
                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList();
            }
        }

        echo $form;
    }

    public function deleteAction()
    {
        if ($Node = $this->_getNode($this->params['id'])) {
            $Node->delete();
            $this->_redirectToList();
        }
    }

    private function _getNode($id = null)
    {
        if ($node = Doctrine::getTable($this->modelName)->find($id)) {
            return $node;
        }
        $this->_redirectToList();
    }

    private function _redirectToList()
    {
        $redirectUri = empty($_SESSION['REDIRECT_URI']) ? $this->view->url(array(), Zend_Registry::get('myCurrentRoute')->name) : $_SESSION['REDIRECT_URI'];

        if ( $this->_request->isXmlHttpRequest() ) {
            echo '<script type="text/javascript">window.location = "'.$redirectUri.'";</script>';
            exit;
        }

        $this->_redirect($redirectUri);
    }

}

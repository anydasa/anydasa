<?

class Admin_PagesController extends Controller_Account
{
    private $modelName = 'AdminPages';
    private $formName  = 'Form_Admin_Page';

    public function preDispatch()
    {
        parent::preDispatch();
        $this->view->routePrefix    = 'admin-pages-';
        $this->view->modelName = $this->modelName;

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function treeAction()
    {
        $this->view->tree = Doctrine_Query::create()
            ->from($this->modelName)
            ->orderBy('lft')
            ->execute();
    }

    public function listAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->_helper->layout->enableLayout();

        $node = $this->_getNode($this->params['id']);
        $this->view->node = $node;

        $this->view->childrens = $node->getNode()->getChildren();
        $this->view->parents = $this->_getParents($node);

        $Sess = new Zend_Session_Namespace('cut');
        $this->view->cut = $Sess->cut;

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];

    }

    public function sortAction()
    {
        $node = $this->_getNode($this->params['id']);

        switch ($this->params['method']) {
            case 'all':
                if ($node->getNode()->hasChildren()) {
                    $childrens = Doctrine_Query::create()
                        ->from($this->modelName)
                        ->where('lft > ? AND rgt < ?', array($node->lft, $node->rgt))
                        ->addWhere('level = ?', $node->level + 1)
                        ->orderBy('title')
                        ->execute();

                    foreach ($childrens as $child) {
                        Doctrine::getTable($this->modelName)->find($child->id)->getNode()->moveAsLastChildOf($node);
                    }
                }
                break;

            case 'up':
                if ($prevNode = $node->getNode()->getPrevSibling()) {
                    $node->getNode()->moveAsPrevSiblingOf($prevNode);
                }
                $this->_redirectToList($node->getNode()->getParent()->id);
                break;

            case 'down':
                if ($prevNode = $node->getNode()->getNextSibling()) {
                    $node->getNode()->moveAsNextSiblingOf($prevNode);
                }
                $this->_redirectToList($node->getNode()->getParent()->id);
                break;
        }

        $this->_redirectToList($node->id);
    }

    public function cutAction()
    {
        $node = $this->_getNode($this->params['id']);
        $Sess = new Zend_Session_Namespace('cut');

        switch ($this->params['method']) {
            case 'cut':
                $Sess->cut = $node->id;
                $this->_redirectToList($node->getNode()->getParent()->getLast()->id);
                break;
            case 'paste':
                if ($node_cuted = Doctrine::getTable($this->modelName)->find(@$Sess->cut)) {
                    $node_cuted->getNode()->moveAsLastChildOf($node);
                    $Sess->__unset();
                }
                $this->_redirectToList($node->id);
                break;
        }
    }

    public function editAction()
    {
        $node = $this->_getNode($this->params['id']);

        $form = new $this->formName($this->modelName);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList($node->getNode()->getParent()->id);
            }
        } else {
            $form->populate($node->toArray());
        }

        echo $form;
    }

    public function addAction()
    {
        $parentNode = $this->_getNode($this->params['id']);

        $form = new $this->formName($this->modelName);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node = new $this->modelName;
                $node->fromArray($form->getValues());
                $node->getNode()->insertAsLastChildOf($parentNode);
                $this->_redirectToList($parentNode->id);
            }
        }

        echo $form;
    }

    public function deleteAction()
    {
        if ($node = $this->_getNode($this->params['id'])) {
            $parentId = $node->getNode()->hasParent() ? $node->getNode()->getParent()->id : 1;
            $node->getNode()->delete();
            $this->_redirectToList($parentId);
        }
    }

    private function _getParents(Doctrine_Record $Node)
    {
        $parents = array();
        if ($Node->getNode()->hasParent()) {
            $parents = $Node->getNode()->getAncestors()->toArray();
        }
        $parents[] = $Node->toArray();
        return $parents;
    }

    private function _getNode($id = null)
    {
        if ($node = Doctrine::getTable($this->modelName)->find($id)) {
            return $node;
        }
        $this->_redirectToList();
    }

    private function _redirectToList($id = 1)
    {
        $redirectUri = empty($_SESSION['REDIRECT_URI']) ? $this->view->url(array('id' => $id), $this->view->routePrefix.'list') : $_SESSION['REDIRECT_URI'];

        if ( $this->_request->isXmlHttpRequest() ) {
            echo '<script type="text/javascript">window.location = "'.$redirectUri.'";</script>';
            exit;
        }

        $this->_redirect($redirectUri);
    }

}

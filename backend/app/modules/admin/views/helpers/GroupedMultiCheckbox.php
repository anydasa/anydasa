<?
class Site_View_Helper_GroupedMultiCheckbox extends Zend_View_Helper_FormMultiCheckbox
{
    public function groupedMultiCheckbox($name, $value = null, $attribs = null, $options = null, $listsep = "\n")
    {

        if (is_array($options))
        {
            $html = array();
            $html [] =
            '<style>
                fieldset {float: left; border: dotted 1px #808080; margin: 5px; padding: 5px 0 5px 5px;}
                fieldset>legend {font-weight: bold; font-size: 12px; margin: 0; border: 0}
                fieldset>div {width: 170px; height: 80px; overflow-y: auto; overflow-x: hidden;}
                fieldset label {white-space: nowrap; font-size: 11px; margin: 0;}
            </style>';

            $translator = $this->getTranslator();
            foreach ($options as $group => $values)
            {
                $group_html = array();
                $group_html[] = '<fieldset>';
                $text_group = null !== $translator ? $translator->translate($group) : $group;
                $group_html[] = '<legend>' . $this->view->escape($text_group) . '</legend>';

                $group_html[] = '<div>';
                $group_html[] = $this->formMultiCheckbox($name, $value, $attribs, $values, $listsep);
                $group_html[] = '</div>';

                $group_html[] = '</fieldset>';

                $html[] = implode("\n", $group_html);
            }
            $html []= '<div style="clear: both;"></div>';
            return implode("\n", $html);
        }
        else
        {
            return $this->formMultiCheckbox($name, $value, $attribs, $options, $listsep);
        }
    }
}
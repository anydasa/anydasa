<?

class LandingPage_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'LandingPage';
    const FORM_NAME     = 'Form_LandingPage';
    const ROUTE_PREFIX  = 'landing-page-index-';
    const COUNT_ON_PAGE = 20;
}
<?

class Ajax_NpController extends Controller_Main
{
    public function preDispatch()
    {
//        $this->_helper->layout->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);

    }

    public function getCitiesAction()
    {
        $result = [];

        if ( !empty($this->_request->getParam('q')) ) {
            $Query = Doctrine_Query::create()
                        ->from('NovaposhtaCities')
                        ->where('description ilike ? ', $this->_request->getParam('q') . '%')
                        ->orWhere('description_ru ilike ?', $this->_request->getParam('q') . '%')
                        ->orWhere('description ilike ?', SF::switcher($this->_request->getParam('q'), 1) . '%')
                        ->orWhere('description_ru ilike ?', SF::switcher($this->_request->getParam('q'), 1) . '%')
                        ->limit(5)
                        ->execute();

            $Query->loadRelated('NovaposhtaAreas');
            $result = $Query->toArray();
        }

        return $this->_helper->json->sendJson($result);
    }

    public function getWarehousesAction()
    {
        $result = [];

        if ( !empty($this->_request->getParam('city_code')) ) {
            $Query = Doctrine_Query::create()
                ->from('NovaposhtaWarehouses')
                ->where('city_code = ?', $this->_request->getParam('city_code'))
                ->execute();

            $result = $Query->toArray();
        }

        return $this->_helper->json->sendJson($result);
    }


}
<?

class Ajax_OrderController extends Controller_Main
{

    public function autocompleteAction()
    {
        $result = [];
        $findBy = $this->_request->getParam('_field');
        $value = $this->_request->getParam($findBy);

        if ( !empty($findBy) && !empty($value)) {
            $Query = Doctrine_Query::create()
                ->from('Orders')
                ->where($findBy.' LIKE ?', '%'.$value.'%')
                ->limit(10)
                ->execute();

            $result = $Query->toArray();
        }

        return $this->_helper->json->sendJson($result);
    }


}
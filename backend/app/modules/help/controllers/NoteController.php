<?
class Help_NoteController extends Controller_AbstractList
{
    const MODEL_NAME    = 'HelpNote';
    const FORM_NAME     = 'Form_Help_Note';
    const ROUTE_PREFIX  = 'help-note-';
    const COUNT_ON_PAGE = 50;

    public function viewAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->view->Record = $this->_getNode($this->params['id']);
    }
}
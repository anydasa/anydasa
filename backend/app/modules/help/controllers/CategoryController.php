<?
class Help_CategoryController extends Controller_AbstractList
{
    const MODEL_NAME    = 'HelpCategory';
    const FORM_NAME     = 'Form_Help_Category';
    const ROUTE_PREFIX  = 'help-category-';
    const COUNT_ON_PAGE = 50;
}
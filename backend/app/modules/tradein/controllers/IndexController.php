<?

class Tradein_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Tradein';
    const FORM_NAME     = 'Form_Tradein';
    const ROUTE_PREFIX  = 'tradein-index-';
    const COUNT_ON_PAGE = 20;
}
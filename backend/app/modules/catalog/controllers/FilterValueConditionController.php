<?
class Catalog_FilterValueConditionController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CatalogFilterValueCondition';
    const FORM_NAME     = 'Form_Catalog_Filter_Value_Condition';
    const ROUTE_PREFIX  = 'catalog-filter-value-condition-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $this->view->Value = $this->findRecordOrException('CatalogFilterValue', $this->params['id_value']);

        $Query = Doctrine_Query::create()
            ->from('CatalogFilterValueCondition')
            ->where('id_value = ?', $this->params['id_value']);

        parent::listAction($Query);
    }

    protected function getForm()
    {
        $form = parent::getForm();

        $Group = empty($this->params['id']) ?
            $this->findRecordOrException('CatalogFilterValue', $this->params['id_value'])->CatalogFilterGroup :
            $this->findRecordOrException('CatalogFilterValueCondition', $this->params['id'])->CatalogFilterValue->CatalogFilterGroup;


        $Query = Doctrine_Query::create()->from('ProductsOption')->orderBy('sort');

        $Query->addWhere('id_catalog = ?', $Group->id_catalog);
        $form->id_option->addMultiOption('', '---');

        $presets = [];
        foreach ($Query->execute() as $Option) {
            $form->id_option->addMultiOption($Option->id, $Option->group_name. ' --- ' . $Option->title . ', '. $Option->unit);
            $presets[$Option->id] = $Option->getPresets();
        }

        $form->setAttrib('data-presets', Zend_Json::encode($presets));
        $form->id_value->setValue( $this->params['id_value'] );

        return $form;
    }



}
<?
class Catalog_FilterIndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CatalogFilterIndex';
    const FORM_NAME     = 'Form_Catalog_Filter_Index';
    const ROUTE_PREFIX  = 'catalog-filter-index-';
    const COUNT_ON_PAGE = 20;

    public function fullListAction()
    {
        $Query = parent::getQuery($this->_request->getQuery());

        $Query->addOrderBy('id_catalog');

        if ( $this->_request->isXmlHttpRequest() && strstr($this->_request->getHeader('Accept'), 'application/json')) {
            $Query->select($this->getParam('__field'));
            $Result = $Query->limit(10)->execute();

            return $this->_helper->json($Result->toArray());
        }

        $pager = new Doctrine_Pager($Query, $this->params['page'], static::COUNT_ON_PAGE);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];

        $this->render('full-list');
    }

    public function getQuery($params)
    {
        $Query = parent::getQuery($params);
        $Query->addWhere('id_catalog = ?', $this->_request->getParam('id_catalog'));
        return $Query;
    }

    public function getForm()
    {
        /** @var Catalog $Catalog */
        $Catalog = $this->findRecordOrException('Catalog', $this->_request->getParam('id_catalog'));

        $form = new Form_Catalog_Filter_Index(static::MODEL_NAME, $Catalog);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);
        $form->addTechFields($this->getAllParams());

        return $form;
    }

    public function copyAction()
    {
        $copy = $this->_getNode($this->params['id'])->toArray();

        $form = $this->getForm();
        $form->setAction($this->view->url(array('id_catalog' => $this->_request->getParam('id_catalog')), static::ROUTE_PREFIX.'add'));

        unset($copy['id']);
        $form->populate($copy);

        echo $form;
    }
}
<?

class Catalog_MenuController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CatalogMenu';
    const FORM_NAME     = 'Form_Catalog_Menu';
    const ROUTE_PREFIX  = 'catalog-menu-';
    const COUNT_ON_PAGE = 20;

    public function recountAction()
    {
        foreach (Doctrine::getTable('CatalogMenu')->findAll() as $Menu) {
            try {
                $Menu->count_products = Products_Query::create()
                    ->setParamsQuery(['menu_alias' => $Menu->alias])
                    ->count();
            } catch (Exception $e) {
                Zend_Debug::dump('Чертовы ошибки'); exit;
            }

            $Menu->save();
        }
        $this->goBack();
    }

    public function addListAction()
    {
        $parentNode = $this->_getNode($this->params['id']);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            $list = explode("\n", str_replace(array("\r\n", "\n\r"), "\n", trim($post['text'])));
            array_walk($list, function (&$value) {$value = trim($value);});

            $list = array_reverse($list);

            foreach ($list as $item) {
                $node = new $this->modelName;
                $node->alias = $item;
                $node->title = $item;
                $node->getNode()->insertAsLastChildOf($parentNode);
            }

            $this->_redirectToList($parentNode->id);
        }

        $this->view->parents = $this->_getParents($parentNode);


        echo $this->view->render('catalogs/form-list.phtml');
    }

    public function listAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->_helper->layout->enableLayout();

        $node = $this->_getNode($this->params['id']);
        $this->view->node = $node;

        $this->view->childrens = $node->getNode()->getChildren();
        $this->view->parents = $this->_getParents($node);

        $Sess = new Zend_Session_Namespace('cut');
        $this->view->cut = $Sess->cut;

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];

    }

    public function sortAction()
    {
        $node = $this->_getNode($this->params['id']);

        switch ($this->params['method']) {
            case 'all':
                if ($node->getNode()->hasChildren()) {
                    $childrens = Doctrine_Query::create()
                        ->from($this->modelName)
                        ->where('lft > ? AND rgt < ?', array($node->lft, $node->rgt))
                        ->addWhere('level = ?', $node->level + 1)
                        ->orderBy('title')
                        ->execute();

                    foreach ($childrens as $child) {
                        Doctrine::getTable($this->modelName)->find($child->id)->getNode()->moveAsLastChildOf($node);
                    }
                }
                break;

            case 'up':
                if ($prevNode = $node->getNode()->getPrevSibling()) {
                    $node->getNode()->moveAsPrevSiblingOf($prevNode);
                }
                $this->_redirectToList($node->getNode()->getParent()->id);
                break;

            case 'down':
                if ($prevNode = $node->getNode()->getNextSibling()) {
                    $node->getNode()->moveAsNextSiblingOf($prevNode);
                }
                $this->_redirectToList($node->getNode()->getParent()->id);
                break;
        }

        $this->_redirectToList($node->id);
    }

    public function cutAction()
    {
        $node = $this->_getNode($this->params['id']);
        $Sess = new Zend_Session_Namespace('cut');

        switch ($this->params['method']) {
            case 'cut':
                $Sess->cut = $node->id;
                $this->_redirectToList($node->getNode()->getParent()->getLast()->id);
                break;
            case 'paste':
                if ($node_cuted = Doctrine::getTable(static::MODEL_NAME)->find(@$Sess->cut)) {
                    $node_cuted->getNode()->moveAsLastChildOf($node);
                    $Sess->__unset('cut');
                }
                $this->_redirectToList($node->id);
                break;
        }
    }

    public function addAction()
    {
        $parentNode = $this->_getNode($this->params['id']);

        $modelName = static::MODEL_NAME;
        $form = $this->getForm();

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $node = new $modelName;
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form);
                $node->getNode()->insertAsLastChildOf($parentNode);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate($this->_request->getQuery());
        }

        echo $form;
    }

    protected function _postSave($node, $form)
    {
        if ( !empty($form->getValue('image_delete')) ) {
            $node->img = null;
        }

        if ( !empty($form->getValue('image')) ) {
            $target = Zend_Registry::get('config')->path->pix.'img/menu/'.$node->id;

            $form->image->addFilter('Rename', array('target' => $target, 'overwrite'  => true));
            $form->image->receive();

            $im = new Imagick($target);
            $im->trimImage(0);
            $im->resizeImage(80, 80, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();
        }

        $node->save();

        $this->recountAction();
    }

    public function deleteAction()
    {
        if ($node = $this->_getNode($this->params['id'])) {
            $parentId = $node->getNode()->hasParent() ? $node->getNode()->getParent()->id : 1;
            $node->getNode()->delete();
            $this->_redirectToList($parentId);
        }
    }

    private function _getParents(Doctrine_Record $Node)
    {
        $parents = array();
        if ($Node->getNode()->hasParent()) {
            $parents = $Node->getNode()->getAncestors()->toArray();
        }
        $parents[] = $Node->toArray();
        return $parents;
    }

    protected function _redirectToList($id = 1)
    {
        $redirectUri = empty($_SESSION['REDIRECT_URI']) ? $this->view->url(array('id' => $id), $this->view->routePrefix.'list') : $_SESSION['REDIRECT_URI'];
        $this->redirect($redirectUri);
    }

}
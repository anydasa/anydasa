<?
class Catalog_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Catalog';
    const FORM_NAME     = 'Form_Catalog';
    const ROUTE_PREFIX  = 'catalog-';
    const COUNT_ON_PAGE = 20;

    public function exportFiltersAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $data = Doctrine_Query::create()
            ->select('v.id, v.alias, c.title, g.title, v.title')
            ->from('CatalogFilterValue v')
            ->innerJoin('v.CatalogFilterGroup g')
            ->innerJoin('g.Catalog c')
            ->orderBy('c.title, g.sort, v.sort')
            ->setHydrationMode(Doctrine::HYDRATE_SCALAR)
            ->execute()
        ;

        $data = array_map(function ($item) {
            return [
                $item['v_id'],
                $item['c_title'],
                $item['g_title'],
                $item['v_title'],
                $item['v_alias'],
            ];
        }, $data);

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
        $ExcelReport->setHeadRow(array(
            'Номер',
            'Категория',
            'Группа',
            'Фильтр',
            'Алиас'
        ));

        $ExcelReport->Excel->getActiveSheet()->fromArray($data, NULL, 'A2');

        echo $ExcelReport->getReport(false);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename="FiltersList.xlsx"')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }
    //---------------------------------------------------------------------------------

    public function getQuery($params)
    {
        $Query = parent::getQuery($params);
        if ( !empty($params['menu']) ) {
            $Menu = Doctrine::getTable('CatalogMenu')->find($params['menu']);
            $Query->innerJoin($Query->getTableAliasMap()['c'].'.CatalogMenu cm');
            $Query->addWhere('cm.lft >= ? AND cm.rgt <= ?', [$Menu->lft, $Menu->rgt]);
        }
        return $Query;
    }

    public function recountFiltersAction()
    {
        $stmt = Doctrine_Manager::connection()->prepare('SELECT products_update_filters()');
        $stmt->execute();
        $this->redirect('/');
    }

    public function recountFiltersOneCatalogAction()
    {
        $stmt = Doctrine_Manager::connection()->prepare('SELECT products_update_filters_catalog(?)');
        $stmt->bindValue(1, $this->getParam('id_catalog'));
        $stmt->execute();

        $this->view->errors = $stmt->fetchAll(PDO::FETCH_COLUMN);


        $this->render('update-filters-errors');
    }

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        $post = $this->_request->getPost();

        $node->unlink('CatalogMenu');
        $node->link('CatalogMenu', array_filter($post['Menu']));
    }
}
<?
class Catalog_FilterGroupController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CatalogFilterGroup';
    const FORM_NAME     = 'Form_Catalog_Filter_Group';
    const ROUTE_PREFIX  = 'catalog-filter-group-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        $Query->addWhere('id_catalog = ?', $this->params['id_catalog']);
        $Query->orderBy('sort');
        $this->view->Catalog = $this->findRecordOrException('Catalog', $this->params['id_catalog']);

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

    protected function getForm()
    {
        $form = parent::getForm();
        $form->populate(['id_catalog'=>$this->params['id_catalog']]);
        return $form;
    }
}
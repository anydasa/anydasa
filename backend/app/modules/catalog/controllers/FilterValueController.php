<?
class Catalog_FilterValueController extends Controller_AbstractList
{
    const MODEL_NAME    = 'CatalogFilterValue';
    const FORM_NAME     = 'Form_Catalog_Filter_Value';
    const ROUTE_PREFIX  = 'catalog-filter-value-';
    const COUNT_ON_PAGE = 1000;

    public function listAction()
    {
        $this->view->Group = $this->findRecordOrException('CatalogFilterGroup', $this->params['id_group']);

        $Query = $this->getQuery($this->_request->getQuery());
        $Query->addWhere('id_group = ?', $this->params['id_group']);
        $Query->orderBy('sort');

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

    protected function getForm()
    {
        $form = parent::getForm();

        $Group = !empty($this->params['id_group']) ?
            $this->findRecordOrException('CatalogFilterGroup', $this->params['id_group']) :
            $this->findRecordOrException(static::MODEL_NAME, $this->params['id'])->CatalogFilterGroup;

        $p = Doctrine_Query::create()->from('CatalogFilterValue')->where('id_group = ?', $Group->id)->orderBy('sort')->execute()->toKeyValueArray('id', 'title');

        $form->getElement('id_parent')->addMultiOptions($p);


        $form->getElement('id_group')->setValue($Group->id);

        return $form;
    }

}
<?
class Banners_GroupController extends Controller_AbstractList
{
    const MODEL_NAME    = 'BannersGroups';
    const FORM_NAME     = 'Form_Banner_Group';
    const ROUTE_PREFIX  = 'banners-group-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        if (empty($this->_request->getParam('sort'))) {
            $Query->orderBy('priority DESC NULLS LAST');
        }

        parent::listAction($Query);
    }

    protected function getForm()
    {
        $form = parent::getForm();

        $formAction = $form->getAction();
        $formAction .= '&redirect='.urlencode($_SERVER['HTTP_REFERER']);
        $form->setAction($formAction);

        return $form;
    }

}
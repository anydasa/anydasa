<?
class Banners_PlaceController extends Controller_AbstractList
{
    const MODEL_NAME    = 'BannersPlaces';
    const FORM_NAME     = 'Form_Banner_Place';
    const ROUTE_PREFIX  = 'banners-place-';
    const COUNT_ON_PAGE = 50;

}
<?
class Banners_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Banners';
    const FORM_NAME     = 'Form_Banner_Item';
    const ROUTE_PREFIX  = 'banners-index-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        $Query->addWhere('id_group = ?', $this->params['id_group']);
        $Query->orderBy('sort');
        $this->view->Group = $this->findRecordOrException('BannersGroups', $this->params['id_group']);

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

    public function editAction()
    {

        $node = $this->_getNode($this->params['id']);
        $form = $this->getForm(['banner_id' => $this->params['id']]);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form);
                $node->save();
                $this->_postEdit($node, $form);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );

            $values = [];
            foreach ($node->BannersRefPromotion as $bannersPromotion){
                $values[] = $bannersPromotion->promotion_id;
            }
            if(!empty($values)){
                $form->promotions->setValue($values);
            }

            $values = [];
            foreach ($node->BannersRefBanner_2 as $bannersToBanner){
                $values[] = $bannersToBanner->banner_id2;
            }
            if(!empty($values)){
                $form->banners->setValue($values);
            }

            $values = [];
            foreach ($node->BannersRefBanner as $bannersToBanner){
                $values[] = $bannersToBanner->banner_id;
            }
            if(!empty($values)){
                $form->banners->setValue($values);
            }

        }

        echo $form;
    }

    protected function getForm()
    {
        $Group = !empty($this->params['id_group']) ?
            $this->findRecordOrException('BannersGroups', $this->params['id_group']) :
            $this->findRecordOrException('Banners', $this->params['id'])->Group;

        $form = parent::getForm();
        $form->populate(['id_group' => $Group->id]);
        $descriptionForm = $form->getDescription();
        $descriptionForm .= ' ('.$Group->Place->width.'x'.$Group->Place->height.')';
        $form->setDescription($descriptionForm);

        return $form;
    }


    protected function _postSave($node, $form)
    {
        if ( !empty($form->getValue('image_delete')) ) {
            @unlink($node->getImagePath());
        }

        if ( !empty($form->getValue('image')) ) {
            $target = $node->getImagePath();

            $form->image->addFilter('Rename', array('target' => $target, 'overwrite'  => true));
            $form->image->receive();

            $im = new Imagick($target);
            $im->resizeImage($node->Group->Place->width, $node->Group->Place->height, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();

        }

        if ( !empty($form->getValue('promotions')) ) {

            $query = Doctrine_Query::create()->from('BannersRefPromotion')->where('banner_id = ?', $node->id);

            foreach ($query->execute() as $result){
                $result->delete();
            }

            foreach ($form->getValue('promotions') as $promotion_id){
                $bannersPromotion = new BannersRefPromotion();
                $bannersPromotion->promotion_id = $promotion_id;
                $bannersPromotion->banner_id = $node->id;
                $bannersPromotion->save();
            }
        }

        if ( !empty($form->getValue('banners')) ) {

            $query = Doctrine_Query::create()->from('BannersRefBanner')->where('banner_id = ?', $node->id);

            foreach ($query->execute() as $result){
                $result->delete();
            }

            foreach ($form->getValue('banners') as $banner_id){
                $bannersBanner = new BannersRefBanner();
                $bannersBanner->banner_id2 = $banner_id;
                $bannersBanner->banner_id = $node->id;
                $bannersBanner->save();
            }

            foreach ($node->RelatedBanners as $banner){
                $banner->is_enabled = $form->getValue('is_enabled');
                $banner->save();
            }

        }
    }
}
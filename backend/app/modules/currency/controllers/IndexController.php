<?
class Currency_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Currency';
    const FORM_NAME     = 'Form_Currency';
    const ROUTE_PREFIX  = 'currency-';
    const COUNT_ON_PAGE = 50;
}
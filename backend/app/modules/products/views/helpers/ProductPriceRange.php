<?

class Site_View_Helper_ProductPriceRange
{

    public function ProductPriceRange(Products $Product)
    {

        $array = [];

        foreach ($Product->VendorsProducts as $P) {
            $array []= [
                'id_vendor'   => $P->id_vendor,
                'vendor_name' => $P->Vendors->name,
                'price'       => $P->getPrice('buy', 'USD'),
                'is_actual'   => $P->isActual()
            ];
        }

        $array = array_filter($array, function ($item) {
            return $item['is_actual'];
        });

        uasort($array, function ($a, $b) {
            return ($a['price'] - $b['price']) * 1000;
        });

        $str = '';

        foreach ($array as $item) {
            $str .= '<div data-id_vendor="'.$item['id_vendor'].'">';
            $str .=     '<span>' . $item['vendor_name'] . ':</span> ';
            $str .=     '<span>' . Currency::format($item['price'], 'USD') . '</span>';
            $str .= '</div>';
        }

        return $str;
    }

}
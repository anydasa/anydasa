<?
class Products_CustomListController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsLists';
    const FORM_NAME     = 'Form_Products_Lists';
    const ROUTE_PREFIX  = 'products-custom-list-';
    const COUNT_ON_PAGE = 100;

    public function productsAction()
    {
        $Record = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id'));

        if ($this->_request->isPost()) {

            $Record->ProductsListsRef->clear();
            $data = [];
            foreach ($this->_request->getPost('text') as $id => $text) {
                $data[] = [
                    'id_list'    => $Record->id,
                    'id_product' => $id,
                    'text'       => $text,
                    'sort'       => count($data),
                ];
            }

            $Record->ProductsListsRef->fromArray($data);
            $Record->save();
        }

        $this->view->Record = $Record;

        $this->render('products');
    }
}
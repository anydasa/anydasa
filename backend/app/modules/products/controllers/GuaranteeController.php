<?
class Products_GuaranteeController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsGuarantee';
    const FORM_NAME     = 'Form_Products_Guarantee';
    const ROUTE_PREFIX  = 'products-guarantee-';
    const COUNT_ON_PAGE = 20;

    public function editRowSetAction()
    {
        $Products = Doctrine_Query::create()->from('Products')->whereIn('id', explode(',', $this->params['id_products']))->execute();

        $form = new Form_Products_GuaranteeRowSet(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title . ':<br><br><p style="font-weight: normal; font-size: 11px;">'.implode('<br>', $Products->toKeyValueArray('id','title')).'</p>');

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                foreach ($Products as $Product) {
                    $Product->id_guarantee = $form->getValues()['id_guarantee'];
                }
                $Products->save();
                $this->goBack();
            }
        }

        echo $form;
    }
}
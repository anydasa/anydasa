<?

class Products_PriceController extends Controller_Account
{

    public function indexAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $Query = Products_Query::create();
        $Query->setOnlyPublic()
              ->setParamsQuery(array('sort' => 'brand'))
              ->setParamsQuery(array('sort' => 'type'))
              ;

        echo (new Products_Price($Query->execute(), $this->getRequest()->getQuery()))->getPrice('trade');

        $this->_sendFile();
    }

    private function _sendFile()
    {

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename=ProductsList.xlsx')
            ->clearBody();

        $this->getResponse()->sendHeaders();

    }
}

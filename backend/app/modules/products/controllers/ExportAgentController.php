<?
class Products_ExportAgentController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsExportAgents';
    const FORM_NAME     = 'Form_Products_ExportAgent';
    const ROUTE_PREFIX  = 'products-export-agent-';
    const COUNT_ON_PAGE = 50;

    protected function _postSave($node, $form)
    {
        $Products_Export = new Products_Export(Zend_Registry::getInstance()->config->toArray());
        $Products_Export->generate($node->id);
    }

    public function generateAction()
    {
        $node = $this->findRecordOrException(self::MODEL_NAME, $this->_request->getParam('id'));

        $Products_Export = new Products_Export(Zend_Registry::getInstance()->config->toArray());
        $Products_Export->generate($node->id);

        $this->goBack();
    }

}
<?

class Products_QuickOptionsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Products';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'products-quick-options-';
    const COUNT_ON_PAGE = 50;

    public function listAction($Query = null)
    {
        $params = $this->getRequest()->getParams();
        $params = array_diff_key($params, array_flip(['action', 'module', 'controller']));
        $filtered_params = array_filter($params, function ($var) {return ($var !== '');});
        if ( $filtered_params != $params) {
            $this->redirect(strtok($_SERVER["REQUEST_URI"],'?').'?'.http_build_query($filtered_params));
        }

        $Catalog = $this->findRecordOrException('Catalog', $params['id_catalog']);

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();
            foreach ($post['product'] as $productId => $productData) {
                /** @var Products $Product */
                $Product = $this->findRecordOrException('Products', $productId);
                $Product->ProductsRefOption->clear();

                foreach ($productData['option'] as $idOption => $value) {
                    $Option = new ProductsRefOption;
                    $Option->fromArray([
                        'id_product' => $Product->id,
                        'id_option' => $idOption,
                        'values'    => $value,
                    ]);

                    $Product->ProductsRefOption->add($Option);
                }
                $Product->save();
            }
        }

        $Query = Products_Query::create();
        $Query->setParamsQuery($params);

        $countOnPage = !empty($params['on_page']) ? $params['on_page'] : self::COUNT_ON_PAGE;
        $pager       = new Doctrine_Pager($Query, $params['page'], $countOnPage);

        $this->view->list       = $pager->execute();
        $this->view->list->loadRelated('ProductsRefOption');

        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $form = new Form_Products_Search('filter');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->populate($this->_request->getQuery());
        $form->removeElement('id_catalog');
        $form->removeElement('notid_catalog');

        $this->view->form = $form;
        $this->view->catalog = $Catalog;

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];

        $this->render('list');
    }

    public function saveAction()
    {
        if ( $this->_request->isPost() ) {
            $Record = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id_product'));
            $Record->fromArray($this->_request->getPost());
            $Record->save();
        }
    }

    public function uploadAction()
    {
        $form = new Form_ExcelUpload(self::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {

                $Excel = PHPExcel_IOFactory::load($form->file->getFileInfo()['file']['tmp_name']);

                $array = $Excel->getActiveSheet()->toArray(null, true, true, true);

                $headerRow = array_shift($array);
                unset($headerRow['A'], $headerRow['B']);
                array_walk($headerRow, function (& $item) {
                    preg_match('/\((\d+?)\)$/', trim($item), $res);
                    $item = (int) $res[1];
                });

                foreach ($array as $item) {
                    /** @var Products $Product */
                    $Product = $this->findRecordOrException('Products', (int) $item['A']);
                    $Product->title = $item['B'];

                    $Product->ProductsRefOption->clear();

                    foreach ($headerRow as $col => $idOption) {
                        $value = trim($item[$col]);
                        if ('' == $value) {
                            continue;
                        }

                        $Option = new ProductsRefOption;
                        $Option->fromArray([
                            'id_product' => $Product->id,
                            'id_option' => $idOption,
                            'values'    => $value,
                        ]);

                        $Product->ProductsRefOption->add($Option);
                    }
                    $Product->save();
                }

                $this->render('upload-result');
                return;
            }
        }
        echo $form;
    }

    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $params = $this->getRequest()->getParams();
        $params = array_diff_key($params, array_flip(['action', 'module', 'controller']));
        /** @var Catalog $Catalog */
        $Catalog = $this->findRecordOrException('Catalog', $params['id_catalog']);

        $result = Products_Query::create()->setParamsQuery($params)->execute();

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));

        $headers = [
            'id',
            'Название товара',
        ];

        foreach ($Catalog->ProductsOption as $ProductsOption) {
            $headers[]= $ProductsOption->title . '('.$ProductsOption->id.')';
        }

        $ExcelReport->setHeadRow($headers);

        /** @var Products $Product */
        foreach ($result as $Product) {
            $row = [
                $Product->id,
                $Product->title,
            ];
            foreach ($Catalog->ProductsOption as $ProductsOption) {
                if ($Product->getOptionValueByOptionId($ProductsOption->id)) {
                    $row []= $Product->getOptionValueByOptionId($ProductsOption->id)->getValues()[0];
                } else {
                    $row []= '';
                }
            }
            $ExcelReport->addDataRow($row);
        }

        $ExcelReport->Excel->getActiveSheet()->getRowDimension(1)->setRowHeight(15);

        $lastColumn = $ExcelReport->Excel->getActiveSheet()->getHighestDataColumn();
        foreach(range('A', $lastColumn) as $columnID) {
            $ExcelReport->Excel->getActiveSheet()->getColumnDimension($columnID)->setWidth(10);
        }

        $ExcelReport->Excel->getActiveSheet()->getStyle("A1:{$lastColumn}1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        echo $ExcelReport->getReport(false);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename="AdwordsProduct.xlsx"')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }
}

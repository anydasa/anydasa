<?
class Products_OptionController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsOption';
    const FORM_NAME     = 'Form_Products_Option';
    const ROUTE_PREFIX  = 'products-option-';
    const COUNT_ON_PAGE = 1000;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        $Query->addWhere('id_catalog = ?', $this->params['id_catalog']);
        $Query->orderBy('sort');

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
        }

        parent::listAction($Query);
    }

    protected function getForm()
    {
        $form = parent::getForm();
        $form->getElement('id_catalog')->setValue($this->params['id_catalog']);
        return $form;
    }

    public function linkAction()
    {
        $node = $this->_getNode($this->params['id_option']);
        if ( $this->_request->isPost() ) {
            if ( in_array($this->params['link'], $node->linked) ) {
                $node->linked = array_diff($node->linked, [$this->params['link']]);
            } else {
                $node->linked = array_merge($node->linked, [$this->params['link']]);
            }
            $node->save();
        }
        return $this->_helper->json($node->linked);
    }
}
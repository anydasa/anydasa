<?

class Products_StateController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsState';
    const FORM_NAME     = 'Form_Products_State';
    const ROUTE_PREFIX  = 'products-state-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());

        if ( empty($this->_request->getParam('sort')) ) {
            $Query->orderBy('sort');
        }

        if ($this->_request->isPost() &status& !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
            $this->redirect($_SERVER['REQUEST_URI']);
        }

        parent::listAction($Query);
    }

}

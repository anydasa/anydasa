<?
class Products_ListController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Products';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'products-list-';
    const COUNT_ON_PAGE = 100;

    public function preDispatch()
    {
        parent::preDispatch();

        $this->_helper->viewRenderer->setNoRender(false);
    }

    private function _filterQuery()
    {
        $params = $this->getRequest()->getQuery();

        $filtered_params = array_filter($params, function ($var) {return ($var !== '');});
        if ( $filtered_params != $params) {
            $this->redirect(strtok($_SERVER["REQUEST_URI"],'?').'?'.http_build_query($filtered_params));
        }
    }

    private function _initSearchForm()
    {
        $form = @new Form_Products_Search('filter');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->populate($this->_request->getQuery());

        $this->view->form = $form;
    }

    public function priceCompareAction()
    {
        $this->_filterQuery();
        $this->_initSearchForm();

        $Query = Products_Query::create();
        $Query->setParamsQuery($this->_request->getQuery());

        $pager = new Doctrine_Pager($Query, $this->getParam('page'), $this->getParam('on_page') ?: 20);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function profitAction()
    {
        $this->_filterQuery();
        $this->_initSearchForm();

        $Query = Products_Query::create();

        $Query->setParamsQuery($this->_request->getQuery());

        $Query->leftJoin("vendors_products vp ON (p.id_vendor_product = vp.id)");
        $Query->addSelect('vp.price_buy / (SELECT rate FROM currency WHERE id = vp.id_currency) * (SELECT rate FROM currency WHERE iso = \'UAH\') as buy');

        $Query->addSelect('p.price_retail / (SELECT rate FROM currency WHERE id = p.id_currency) * (SELECT rate FROM currency WHERE iso = \'UAH\') as retail');

        $Query->addSelect('(p.price_retail / (SELECT rate FROM currency WHERE id = p.id_currency) * (SELECT rate FROM currency WHERE iso = \'UAH\'))
                           - vp.price_buy / (SELECT rate FROM currency WHERE id = vp.id_currency) * (SELECT rate FROM currency WHERE iso = \'UAH\') as profit');

        //--------------
        $Query2 = clone $Query;
        $Query2->removeDqlQueryPart('orderby');

        $sql = "SELECT count(*) AS count, sum(res.profit) AS sum_profit, sum(res.buy) AS sum_buy, sum(res.retail) AS sum_retail
                FROM (".$Query2->getSqlQuery().") as res";
        $stmt = Doctrine_Manager::connection()->prepare($sql);
        foreach ($Query2->getParams()['where'] as $id=>$item) {
            $stmt->bindValue($id+1, $item);
        }
        $stmt->execute();

        $this->view->agg = $stmt->fetch(PDO::FETCH_ASSOC);
        //--------------

        if (! empty($this->getParam('sort'))) {
            list($f,$t) = explode('-', $this->getParam('sort'));
            $pass = ['profit', 'buy', 'retail'];
            if (in_array($f, $pass)) {
                $Query->orderBy($f.' '.$t.' NULLS LAST');
            }
        }

        $pager = new Doctrine_Pager($Query, $this->getParam('page'), $this->getParam('on_page') ?: 20);

        $this->view->list       = $pager->execute([], Doctrine::HYDRATE_ARRAY);
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function pricingAction()
    {
        $this->_filterQuery();
        $this->_initSearchForm();

        $Query = Products_Query::create();
        $Query->setParamsQuery($this->_request->getQuery());

        if ( 'exists' == $this->getParam('hotline') ) {
            $Query->addWhere("EXISTS(SELECT 1 FROM products_pricing_hotline WHERE id_product = p.id)");
        } elseif ( 'enabled' == $this->getParam('hotline') ) {
            $Query->addWhere("EXISTS(SELECT 1 FROM products_pricing_hotline WHERE id_product = p.id AND is_enabled = true)");
        } elseif ( 'disabled' == $this->getParam('hotline') ) {
            $Query->addWhere("EXISTS(SELECT 1 FROM products_pricing_hotline WHERE id_product = p.id AND is_enabled = false)");
        }

        $pager = new Doctrine_Pager($Query, $this->getParam('page'), $this->getParam('on_page') ?: 20);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function adwordsAction()
    {
        $this->_filterQuery();
        $this->_initSearchForm();

        $Query = Products_Query::create();
        $Query->setParamsQuery($this->_request->getQuery());

        $pager = new Doctrine_Pager($Query, $this->getParam('page'), $this->getParam('on_page') ?: 20);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

}
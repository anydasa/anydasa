<?

class Products_BrandController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsBrands';
    const FORM_NAME     = 'Form_Products_Brand';
    const ROUTE_PREFIX  = 'products-brand-';
    const COUNT_ON_PAGE = 100;

    protected function _postSave($node, $form)
    {
        $this->_processImage($node, $form->getValues());

        parent::_postSave($node, $form);
    }

    private function _processImage($Obj, $values)
    {

        if (!empty($values['image_delete']) && !empty($Obj->id) && 1 == $values['image_delete']) {
            unlink(Zend_Registry::get('config')->path->pix.'img/brands/'.$Obj->id.'.jpg');
        }

        if (!empty($values['image'])) {

            $uploaded_file = Zend_Registry::get('config')->path->pix.'img/brands/'.$values['image'];

            $Image = new Site_Image;
            $saved_file_name = $Image->saveAs($uploaded_file, 'image/jpeg', true);

            $Image->resizeImageMaxSide($saved_file_name, 200, 200);

            $file_pathinfo = pathinfo($saved_file_name);
            $new_file_name = $file_pathinfo['dirname'].'/'. $Obj->id . '.' . $file_pathinfo['extension'];
            rename($saved_file_name, $new_file_name);
        }
    }
}

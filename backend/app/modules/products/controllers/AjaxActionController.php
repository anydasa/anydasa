<?

class Products_AjaxActionController extends Controller_Account
{
    public function preDispatch()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        if ( !$this->_request->isXmlHttpRequest() ) {
            ob_start();
        }
    }
    public function postDispatch()
    {
        if ( !$this->_request->isXmlHttpRequest() ) {
            ob_end_clean();
            $this->redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function getFormattedTitleAction()
    {
        return $this->_helper->json->sendJson(SF::formatString($this->params['title']));
    }

    public function isActionAction()
    {
        $node = $this->_getNode($this->params['id']);
        if ($node->is_action) {
            $node->is_action = 0;
            $node->save();
            echo 0;
        } else {
            $node->is_action = 1;
            $node->save();
            echo 1;
        }
    }

    public function isNewAction()
    {
        $node = $this->_getNode($this->params['id']);
        if ($node->is_new) {
            $node->is_new = 0;
            $node->save();
            echo 0;
        } else {
            $node->is_new = 1;
            $node->save();
            echo 1;
        }
    }
    public function isVisibleAction()
    {
        $node = $this->_getNode($this->params['id']);
        if ($node->is_visible) {
            $node->is_visible = 0;
            $node->save();
            echo 0;
        } else {
            $node->is_visible = 1;
            $node->save();
            echo 1;
        }
    }
    public function isHitAction()
    {
        $node = $this->_getNode($this->params['id']);
        if ($node->is_hit) {
            $node->is_hit = 0;
            $node->save();
            echo 0;
        } else {
            $node->is_hit = 1;
            $node->save();
            echo 1;
        }
    }

    public function deleteAction()
    {
        $node = $this->_getNode($this->params['id']);
        $node->delete();
        echo '0';
    }

    public function statusAction()
    {
        $node = $this->_getNode($this->params['id']);
        $node->status = $this->params['status'];
        $node->save();
        echo $this->params['status'];
    }

    public function importUploadImagesExcelAction()
    {
        if (!isset($_FILES['xls'])) {
            return $this->_helper->json->sendJson(['message' => 'error']);
        }

        $tmpfname = $_FILES['xls']['tmp_name'];
        $filename = basename($_FILES['xls']['name']);
        move_uploaded_file($tmpfname, ROOT_PATH . 'pix/files/temp/' . $filename);
        $tmpfname = ROOT_PATH . 'pix/files/temp/' . $filename;
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastColumn = $worksheet->getHighestColumn();
        $lastColumn++;
        $cellsToReturn = [];
        for ($column = 'A'; $column != $lastColumn; $column++) {
            $cellToReturn = [];
            $cellValue = $worksheet->getCell($column.'1')->getValue();
            if (!empty($cellValue)) {
                $cellToReturn['value'] = $cellValue;
                $cellToReturn['code'] = $column;
                $cellsToReturn[] = $cellToReturn;
            }
        }
        return $this->_helper->json->sendJson(['message' => 'success', 'columns' => $cellsToReturn, 'filename' => $tmpfname]);
    }

    public function importUploadSubmitImagesExcelAction()
    {
        set_time_limit(600);
        $post = $this->_request->getPost();
        $cellsMap = $this->getCellsMapArray($post);

        $tmpfname = $post['filename'];
        $extractedExcelData = $this->parseExcelFile($tmpfname, $cellsMap);
        $codesArr = $extractedExcelData[0];
        $excelExtracted = $extractedExcelData[1];

        $extractedChangesByProductsCodes = [];
        foreach ($excelExtracted as $item) {
            $extractedChangesByProductsCodes[$item[$cellsMap['code']]] = $item;
        }

        $codesString = implode(' ', $codesArr);

        if (empty($codesString)) {
            $dataToReturn = [];
            $dataToReturn['countFoundCodes'] = 0;
            $dataToReturn['notFoundCodes'] = 0;
            $dataToReturn['countNotFoundCodes'] = 0;
            $dataToReturn['errors'] = 0;
            $dataToReturn['imagesUploaded'] = 0;

            return $this->_helper->json->sendJson(['message' => 'success', 'data' => $dataToReturn]);
        }

        $Query = Products_Query::create();
        $Query->setParamsQuery(array('code' => $codesString));
        $productsToUpdate = $Query->execute();

        $dataToReturn = $this->massProcessProductsUpdate($productsToUpdate, $extractedChangesByProductsCodes, $codesArr, $cellsMap);
        unlink($tmpfname);

        return $this->_helper->json->sendJson(['message' => 'success', 'data' => $dataToReturn]);
    }

    protected function getCellsMapArray($post)
    {
        $cellsMap = [];
        if (!empty($post['code'])) {
            $cellsMap['code'] = $post['code'];
        }
        if (!empty($post['image_1'])) {
            $cellsMap['image_1'] = $post['image_1'];
        }
        if (!empty($post['image_2'])) {
            $cellsMap['image_2'] = $post['image_2'];
        }
        if (!empty($post['image_3'])) {
            $cellsMap['image_3'] = $post['image_3'];
        }
        if (!empty($post['image_4'])) {
            $cellsMap['image_4'] = $post['image_4'];
        }
        if (!empty($post['image_5'])) {
            $cellsMap['image_5'] = $post['image_5'];
        }
        if (!empty($post['image_6'])) {
            $cellsMap['image_6'] = $post['image_6'];
        }
        if (!empty($post['image_7'])) {
            $cellsMap['image_7'] = $post['image_7'];
        }
        if (!empty($post['image_8'])) {
            $cellsMap['image_8'] = $post['image_8'];
        }

        return $cellsMap;
    }

    protected function parseExcelFile($tmpfname, $cellsMap)
    {
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfname);
        $excelObj = $excelReader->load($tmpfname);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $lastColumn = $worksheet->getHighestColumn();
        $lastColumn++;
        $columnNames = [];
        for ($column = 'A'; $column != $lastColumn; $column++) {
            $columnNames[$column] = $worksheet->getCell($column.'1')->getValue();
        }
        $codesArr = [];
        $excelExtracted = [];
        for($row = 2; $row <= $lastRow; $row++) {
            $excelExtracted[$row - 2] = [];
            for ($column = 'A'; $column != $lastColumn; $column++) {
                if (in_array($column, $cellsMap)) {
                    $extractedValue = $worksheet->getCell($column.$row)->getValue();
                    if (isset($extractedValue)) {
                        $excelExtracted[$row - 2][$column] = $extractedValue;
                        if ($column == $cellsMap['code']) {
                            $codesArr[] = $extractedValue;
                        }
                    }
                }
            }
        }

        return array($codesArr, $excelExtracted);
    }

    protected function massProcessProductsUpdate($productsToUpdate, $extractedChangesByProductsCodes, $codesArr, $cellsMap)
    {
        $foundCodes = [];
        $errors = [];
        $imagesUploaded = 0;

        foreach ($productsToUpdate as $product) {
            $isError = false;
            $foundCodes[] = $product->code;
            $updateValues = $extractedChangesByProductsCodes[$product->code];

            $Image = $product->getImage();
            $destination = $Image->getTempDirForUpload();
            foreach ($updateValues as $key => $link) {
                if ($key != $cellsMap['code'] && !empty($link)) {
                    $Image->addNewImage(trim($link));
                    $imagesUploaded++;
                }
            }
        }
        $dataToReturn = [];
        $dataToReturn['countFoundCodes'] = count($foundCodes);
        $dataToReturn['notFoundCodes'] = array_diff($codesArr, $foundCodes);
        $dataToReturn['countNotFoundCodes'] = count($dataToReturn['notFoundCodes']);
        $dataToReturn['errors'] = $errors;
        $dataToReturn['imagesUploaded'] = $imagesUploaded;

        return $dataToReturn;
    }

    private function _getNode($id = null)
    {
        if ($node = Doctrine::getTable('Products')->find($id)) {
            return $node;
        }
    }
}

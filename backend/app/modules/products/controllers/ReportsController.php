<?

class Products_ReportsController extends Controller_Account
{

    public function marginsAction()
    {

        $Query = Products_Query::create()
            ->setOnlyPublic()
            ->setOnlyAvailableForOrder()
            ->where('p.price_retail * (SELECT rate FROM currency WHERE id = p.id_currency) < (SELECT vp.price_retail * (SELECT rate FROM currency WHERE id = vp.id_currency) FROM vendors_products vp WHERE vp.id = p.id_vendor_product)')
            ;


        $pager = new Doctrine_Pager($Query, $this->params['page'], 100);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
    }


}

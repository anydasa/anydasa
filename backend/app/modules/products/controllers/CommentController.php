<?
class Products_CommentController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsComments';
    const FORM_NAME     = 'Form_Products_Comment';
    const ROUTE_PREFIX  = 'products-comment-';
    const COUNT_ON_PAGE = 20;

    public function answerAction()
    {
        $parent = $this->findRecordOrException(static::MODEL_NAME, $this->getParam('id'));
        $parent->is_checked = true;
        $parent->save();

        $modelName = static::MODEL_NAME;

        $form = new Form_Products_CommentAnswer('ProductsComments');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);
        $form->id_parent->setValue($parent->id);
        $form->id_product->setValue($parent->id_product);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $node = new $modelName;
                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate($this->_request->getQuery());
        }

        echo $form;
    }
}
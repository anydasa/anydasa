<?
class Products_LinksController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsLinks';
    const FORM_NAME     = 'Form_Products_Links';
    const ROUTE_PREFIX  = 'products-links-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        $Query->addWhere('id_product = ?', $this->params['id_product']);
        $Query->orderBy('url');
        $this->view->Product = $this->findRecordOrException('Products', $this->params['id_product']);
        parent::listAction($Query);
    }

    public function imagesAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $Product = $this->findRecordOrException('Products', $this->params['id_product']);

        if ($this->_request->isPost()) {
            $ProductImage = $Product->getImage();

            $delete = $this->_request->getPost()['delete'];

            if ( is_array($delete) ) {
                krsort($delete);
                $self = $this->_request->getPost()['self'];
                foreach ($delete as $key=>$temp) {
                    if ( !isset($self[$key]) ) {
                        $ProductImage->removeImage($key);
                    }
                }
            }

            if ( !empty($this->_request->getPost()['images']) ) {
                foreach ($this->_request->getPost()['images'] as $link=>$temp) {
                    $ProductImage->addNewImage($link);
                }
            }

            $this->redirect($this->view->url(array('id'=> $this->params['id_product'], 'action' => 'list'), 'products-product-image'));
        }

        $this->view->Product = $Product;
        $this->view->list    = $this->view->Product->getLink()->parseImages();
    }

    public function pricesOnlineAction()
    {
        $Product = $this->findRecordOrException('Products', $this->params['id_product']);
        foreach ($Product->Links as $Link) {
            $Link->updateRemotePriceList();
        }
        $this->view->Product = $Product;
        $this->render('prices');
    }

    public function updatePricesRowSetAction()
    {

        if ( empty($this->params['id_products']) ) {
            echo '<p style="font-size: 2em;">Не выбраны товары</p>';
            return;
        }

        $Products = Doctrine_Query::create()->from('Products')->whereIn('id', explode(',', $this->params['id_products']))->execute();

        $form = new Form_Empty(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(
            Zend_Registry::get('myCurrentRoute')->title . ': '
            . '<ul style="font-weight: normal; font-size: 11px;"><li>'.implode('<li>', $Products->toKeyValueArray('id','title')).'</ul>'
        );

        $form->addSubmitElement();
        $form->save->setLabel('Обновить');

        if ($this->_request->isPost()) {
            foreach ($Products as $Product) {
                foreach ($Product->Links as $Link) {
                    $Link->updateRemotePriceList();
                }
            }
            $this->goBack();
        }

        echo $form;
    }

    public function pricesAction()
    {
        $Product = $this->findRecordOrException('Products', $this->params['id_product']);
        $this->view->Product = $Product;
        $this->render('prices');
    }

    protected function getForm()
    {
        $form = parent::getForm();
        $form->getElement('id_product')->setValue($this->params['id_product']);
        return $form;
    }

    protected function _redirectToList()
    {
        $this->redirect($this->view->url(array('id_product'=>$this->params['id_product']), 'products-links-list'));
    }
}
<?

class Products_ChoiceController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Products';
    const FORM_NAME     = 'Form_Products_Product';
    const ROUTE_PREFIX  = 'products-choice-';
    const COUNT_ON_PAGE = 20;

    public function listAction()
    {
        $params = $this->getRequest()->getQuery();

        $filtered_params = array_filter($params, function ($var) {return ($var !== '');});
        if ( $filtered_params != $params) {
            $this->redirect(strtok($_SERVER["REQUEST_URI"],'?').'?'.http_build_query($filtered_params));
        }

        $Query = Products_Query::create();
        $Query->setParamsQuery($params);

        $countOnPage = !empty($params['on_page']) ? $params['on_page'] : 10;
        $pager       = new Doctrine_Pager($Query, $params['page'], $countOnPage);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $this->render('list');
    }

    public function setAction()
    {
        $params = $this->getRequest()->getQuery();

        $filtered_params = array_filter($params, function ($var) {return ($var !== '');});
        if ( $filtered_params != $params) {
            $this->redirect(strtok($_SERVER["REQUEST_URI"],'?').'?'.http_build_query($filtered_params));
        }
        
        $product_id = $this->params['id'];

        $query = Doctrine_Query::create();
        $query->from('Products')
            ->where('id = ?', $product_id);

        $product = $query->fetchOne();

        $query = Doctrine_Query::create();
        $query->from('Products p')
            ->leftJoin( $query->getRootAlias().'.ProductsSetsProduct psp')
            ->select('p.*, psp.group_num as set_group, psp.discount as discount')
            ->andWhere('psp.id_set = ?', $product->id_set)
            ->orderBy('psp.group_num');
        

        $countOnPage = !empty($params['on_page']) ? $params['on_page'] : 10;
        $pager       = new Doctrine_Pager($query, $params['page'], $countOnPage);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $this->render('set_list');
    }

    public function advancedSearchAction()
    {
        $form = new Form_Products_Search('filter');
        $form->setAction($this->view->url([], 'products-choice-list'));
        $form->setMethod('GET');
        $form->populate($this->_request->getQuery());
        echo $form;
    }

}

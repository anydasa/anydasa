<?

class Products_ProductController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Products';
    const FORM_NAME     = 'Form_Products_Product';
    const ROUTE_PREFIX  = 'products-product-';
    const COUNT_ON_PAGE = 20;

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        $node->unlink('Catalogs');
        $node->link('Catalogs', $form->getValues()['Catalogs']);
    }

    protected function _postEdit(Doctrine_Record $node, Zend_Form $form)
    {
        $this->view->Product = $node;

        echo '<script>
                $(".products-list tr[data-id='.$node->id.']")
                    .replaceWith("'.SF::escapeJavaScriptText($this->view->render('product/list-item-partial.phtml')).'");

                    $(".products-list tr[data-id='.$node->id.'] td")
                        .css({opacity: 0.5})
                        .animate({opacity: 1}, 1000);
                    closeModalWindow();
            </script>';
        exit;
    }

    public function listAction()
    {
        $params = $this->getRequest()->getQuery();
        $filtered_params = array_filter($params, function ($var) {return ($var !== '');});
        if ( $filtered_params != $params) {
            $this->redirect(strtok($_SERVER["REQUEST_URI"],'?').'?'.http_build_query($filtered_params));
        }

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();

            if ( !empty($post['id']) ) {
                $Query = Products_Query::create();
                $Query->setParamsQuery(array('id'=>$post['id']));

                foreach ($Query->execute() as  $node) {
                    switch ($post['submit-action']) {
                        case 'prices':
                            $node->getLink()->updatePrices();
                            break;
                        case 'is_link_checked':
                            $node->is_link_checked = $post['submit-value'];
                            $node->save();
                            break;
                        case 'is_visible':
                            $node->is_visible = $post['submit-value'];
                            $node->save();
                            break;
                        case 'is_spy_prices':
                            $node->is_spy_prices = $post['submit-value'];
                            $node->save();
                            break;
                        case 'is_new':
                            $node->is_new = $post['submit-value'];
                            $node->save();
                            break;
                        case 'delete':
                            $node->delete();
                            break;
                    }
                }
            }
        }

        $Query = Products_Query::create();
        $Query->setParamsQuery($params);

        if ( !empty($_SESSION['sort']) ) {
            $Query->setParamsQuery(['sort' => $_SESSION['sort']]);
        } else {
            $Query->setParamsQuery(['sort' => 'sort-desc']);
            $Query->setParamsQuery(['first_by_status' => 1]);
            $Query->setParamsQuery(['first_by_status' => 2]);
            $Query->setParamsQuery(['first_by_status' => 3]);
            $Query->setParamsQuery(['first_by_status' => 0]);
        }

        $countOnPage = !empty($params['on_page']) ? $params['on_page'] : 20;
        $pager       = new Doctrine_Pager($Query, $params['page'], $countOnPage);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $this->view->list->loadRelated('Promotions');
        $this->view->list->loadRelated('ProductsLinks');
        $this->view->list->loadRelated('LinksPrices');
        $this->view->list->loadRelated('ProductsBrands');

        $form = new Form_Products_Search('filter');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->populate($this->_request->getQuery());

        $this->view->form = $form;

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];

        $this->render('list');
    }

    public function createFromVendorProductAction()
    {
        /** @var VendorsProducts $VendorProduct */
        $VendorProduct = $this->findRecordOrException('VendorsProducts', $this->params['id_vendor_product']);

        $modelName = static::MODEL_NAME;
        /** @var Form_Products_Product $form */
        $form = $this->getForm();

        if ( !empty($VendorProduct->url) ) {
            $form->addUrlElement();
        }

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $node = new $modelName;
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form);
                $node->save();
                $this->_postSave($node, $form);
                $node->refresh();
                $VendorProduct->id_product = $node->id;
                $VendorProduct->save();

                setcookie('id_brand',    $form->getValue('id_brand'), time() + 3600, '/');
                //setcookie('id_catalogs', serialize($form->getValue('id_catalogs')), time() + 3600, '/');

                $this->_redirectToList();
            }
        } else {

            if ( !empty($VendorProduct->brand) ) {
                $id_brand = Doctrine_Query::create()->select('id')->from('ProductsBrands')->where('title ILIKE ?', '%'.$VendorProduct->brand.'%')->fetchOne()->id;
            }
            if (empty($id_brand)) {
                $id_brand = $_COOKIE['id_brand'];
            }

            $form->populate(
                array_merge(
                    $VendorProduct->set('id', NULL)->toArray(),
                    [
                        'alias'       => $VendorProduct->title,
                        'status'      => Products::STATUS_ORDER,
                        'is_visible'  => 1,
                        'balance'     => $VendorProduct->balance ? $VendorProduct->balance : 0,
                        'id_brand'    => $id_brand,
                        //'id_catalogs' => unserialize($_COOKIE['id_catalogs'])
                    ]
                )
            );
        }

        echo $form;
    }

    public function specialOfferAction()
    {
        $node = $this->_getNode($this->params['id']);

        $form = new Form_Products_ProductSpecialOffer(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $node->fromArray($form->getValues());
                $this->_preSave($node, $form);
                $node->save();
                $this->_postEdit($node, $form);
                $this->_postSave($node, $form);
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        echo $form;
    }

    public function groupsAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $this->view->Product = $this->findRecordOrException(self::MODEL_NAME, $this->params['id_product']);

        $Query = $this->getQuery($this->params, 'ProductsGroups');

        if ( 0 == count(array_filter($Query->getDqlPart('orderby'), function($var) { return preg_match("/ts_rank.*/i", $var); })) ) {
            //Если нет сортировки по релевантности
            $Query->orderBy('(SELECT t1.title FROM ProductsBrands t1 WHERE t1.id IN (SELECT t2.id_brand FROM Products t2 WHERE t2.id IN (SELECT t3.id_product FROM ProductsRefGroups t3 WHERE t3.id_group = '.$Query->getRootAlias().'.id)) LIMIT 1), title');
        }

        $pager = new Doctrine_Pager($Query, $this->params['page'], static::COUNT_ON_PAGE);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function editRowSetCatalogAction()
    {
        if ( empty($this->params['id_products']) ) {
            echo '<p style="font-size: 2em;">Не выбраны товары</p>';
            return;
        }

        $Products = Doctrine_Query::create()->from('Products')->whereIn('id', explode(',', $this->params['id_products']))->execute();

        $form = new Form_Products_ProductCatalog(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);

        $form->setDescription(
            Zend_Registry::get('myCurrentRoute')->title . ': '
            . '<ul style="font-weight: normal; font-size: 11px;"><li>'.implode('<li>', $Products->toKeyValueArray('id','title')).'</ul>'
        );


        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            if ($form->isValid($post)) {

                foreach ($Products as $Product) {
                    $Product->unlink('Catalogs');
                    $Product->link('Catalogs', $form->getValues()['id_catalogs']);
                    $Product->save();
                }
                $this->goBack();
            }
        }

        echo $form;
    }

    public function descriptionAction()
    {
        /** @var Products $product */
        $product = $this->_getNode($this->params['id']);
        $form = new Form_Products_Description('Description', $product);
        $form->setAction($_SERVER['REQUEST_URI']);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $product->fromArray($form->getValues());
                $product->save();

                Service_ProductImage::saveRemoteDescriptionImagesLocally($product);

                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $data = $product->toArray();

            if ('vendor' === $this->_request->getParam('from') && $product->VendorsProducts->count()) {
                $data['description'] = $product->VendorsProducts->getFirst()->description;
            }

            $form->populate($data);
        }

        echo $form;
    }

    public function vendorProductsListAction()
    {
        $this->view->Product = $this->_getNode($this->params['id']);

        $this->_helper->viewRenderer->setNoRender(false);

        $Query = $this->getQuery($this->_request->getQuery(), 'VendorsProducts');
        $Query->addWhere('id_product IS NULL');

        $pager = new Doctrine_Pager($Query, $this->params['page'], 10);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
    }

    public function viewAction()
    {
        $this->view->Product = $this->findRecordOrException(static::MODEL_NAME, $this->getParam('id'));
        $this->render('view');
    }

    public function optionsAction()
    {
        $Product = $this->findRecordOrException(static::MODEL_NAME, $this->getParam('id_product'));
        $Catalog = $this->findRecordOrException('Catalog', $this->getParam('id_catalog'));

        $this->view->Product = $Product;
        $this->view->Links = $Catalog->ProductsOption->toKeyValueArray('id', 'linked');
        $form = new Form_Products_ProductsOption('ProductsOption');
        $form->setAction($_SERVER['REQUEST_URI']);

        $form->setDescription(
            Zend_Registry::get('myCurrentRoute')->title
            . ': <h4>'.$Product->title.'</h4>'
            . $this->view->render('product/option-description.phtml')
        );

        $form->addOptionsElements($Product, $Catalog->id);

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();
            if ( $form->isValid($post) ) {
                $Product->ProductsRefOption->clear();
                $formValues = $form->getValues();

                foreach ($formValues['Options']['values'] as $id_option => $value) {
                    $Option = new ProductsRefOption;
                    $Option->fromArray([
                        'id_product' => $Product->id,
                        'id_option' => $id_option,
                        'values'    => $value,
                        'addition'  => $formValues['addition'][$id_option]
                    ]);

                    $Product->ProductsRefOption->add($Option);
                }

                $Product->save();
                $this->_postEdit($Product, $form);
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        echo $form;
    }


    public function deleteAction()
    {
        $this->findRecordOrException(static::MODEL_NAME, $this->params['id'])->delete();
        $this->goBack();
    }

}

<?

class Products_ExcelController extends Controller_Account
{

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $params = $this->params;

        if ( empty($params['sort']) ) {
            $params['sort'] = 'time_desc';
        }

        $Query = Products_Query::create();
        $Query->setParamsQuery($params);
        $Products = $Query->execute();

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
        $ExcelReport->setHeadRow(array('Код', 'Название', 'Остаток', 'Категория', 'Категория', 'Категория', 'Мин.Цена', 'Партн.Цена', 'Опт.Цена', 'Розн.Цена'));

        foreach ($Products as $item) {
            $catalogs = $item->Catalogs->getFirst()->getNode()->getAncestors()->toKeyValueArray('id', 'title');
            array_shift($catalogs);
            $catalogs[] = $item->Catalogs->getFirst()->title;

            $ExcelReport->addDataRow(array(
                $item->code,
                $item->title,
                (int)$item->balance,
                $catalogs[0],
                $catalogs[1],
                $catalogs[2],
                $item->getFormatedPrice('min', 'USD'),
                $item->getFormatedPrice('partner', 'USD'),
                $item->getFormatedPrice('trade', 'USD'),
                $item->getFormatedPrice('retail', 'USD')
            ));
        }
        echo $ExcelReport->getReport();

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename=ProductsList.xlsx')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }

}

<?
class Products_AccessoriesController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsAccessories';
    const FORM_NAME     = 'Form_Products_Accessory';
    const ROUTE_PREFIX  = 'products-accessories-';
    const COUNT_ON_PAGE = 20;

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        $post = $this->_request->getPost();

        $node->unlink('Products');
        $node->link('Products', $post['ownerProducts']);

        $node->unlink('Accessories');
        $node->link('Accessories', $post['relatedProducts']);
    }
}
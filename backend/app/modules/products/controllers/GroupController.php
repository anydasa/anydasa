<?
class Products_GroupController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsGroups';
    const FORM_NAME     = 'Form_Products_Group_Group';
    const ROUTE_PREFIX  = 'products-group-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());

        if ( empty($this->params['sort']) ) {
            //$Query->orderBy('(SELECT t1.title FROM ProductsBrands t1 WHERE t1.id IN (SELECT t2.id_brand FROM Products t2 WHERE t2.id IN (SELECT t3.id_product FROM ProductsRefGroups t3 WHERE t3.id_group = '.$Query->getRootAlias().'.id)) LIMIT 1), title');
        }

        parent::listAction($Query);
    }

    public function productsAdvancedSearchAction()
    {
        $form = new Form_Products_Search('filter');
        $form->setAction($this->view->url(array('id_group' => $this->getParam('id_group')), 'products-group-products-search'));
        $form->setMethod('GET');
        $form->populate($this->_request->getQuery());
        echo $form;
    }

    public function productsSearchAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $this->view->Group = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id_group'));

        $params = $this->getRequest()->getQuery();

        $Query = Products_Query::create();
        $Query->setParamsQuery($params);

        $pager       = new Doctrine_Pager($Query, $params['page'], 20);

        $this->view->list       = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function iconsAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $this->view->Group = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id_group'));

        $Query = Doctrine_Query::create()
            ->from('ProductsRefGroups t1')
            ->where('id_type = ?', $this->view->Group->Type->id)
            ->orderBy('t1.title');

        $pager = new Doctrine_Pager($Query, $this->params['page'], 200);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 5));

    }

    public function iconsFromProductAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);
        $this->view->Product = $this->findRecordOrException('Products', $this->getParam('id_product'));
    }

    public function productsAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        $this->view->Group = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id_group'));

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()
                    ->update('ProductsRefGroups')
                    ->set('sort', $position)
                    ->where('id_group = ?', $this->view->Group->id)
                    ->addWhere('id_product = ?', $id)
                    ->execute();
            }
        }

        $Query = Doctrine_Query::create()
            ->from('ProductsRefGroups')
            ->where('id_group = ?', $this->view->Group->id)
            ->orderBy('sort');



        $pager = new Doctrine_Pager($Query, $this->params['page'], 50);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 5));

        $_SESSION['REDIRECT_URI'] = $_SERVER['REQUEST_URI'];
    }

    public function productDeleteAction()
    {
        Doctrine_Query::create()
            ->from('ProductsRefGroups')
            ->where('id_product = ?',  $this->getParam('id_product'))
            ->addWhere('id_group = ?', $this->getParam('id_group'))
            ->fetchOne()
            ->delete();

        $this->_redirectToList();
    }

    public function productSaveAction()
    {
        $Group   = $this->findRecordOrException('ProductsGroups', $this->getParam('id_group'));
        $Product = $this->findRecordOrException('Products', $this->getParam('id_product'));

        $node = Doctrine_Query::create()
            ->from('ProductsRefGroups')
            ->where('id_product = ?', $Product->id)
            ->addWhere('id_group = ?', $Group->id)
            ->fetchOne();

        $form = new Form_Products_Group_Product('ProductsRefGroups');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription('Группа: <strong style="color: red;">'.$Group->title.'</strong><br>Товар: <span style="color: green;">'.$Product->title.'</span>');

        if ( !$node ) {
            $node = new ProductsRefGroups;
            $node->id_product = $Product->id;
            $node->id_group = $Group->id;
            $node->id_type = $Group->Type->id;
        }


        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $form->receive($node);
                $node->fromArray($form->getValues());
                $node->save();
                $this->_redirectToList();
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate( $node->toArray() );
        }

        echo $form;
    }
}
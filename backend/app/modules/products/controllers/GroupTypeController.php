<?
class Products_GroupTypeController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsGroupsTypes';
    const FORM_NAME     = 'Form_Products_Group_Type';
    const ROUTE_PREFIX  = 'products-group-type-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $Query = $this->getQuery($this->_request->getQuery());
        $Query->orderBy('sort');

        if ($this->_request->isPost() && !empty($this->_request->getPost()['sort']) ) {
            foreach ($this->_request->getPost()['sort'] as $position=>$id) {
                Doctrine_Query::create()->update(static::MODEL_NAME)->set('sort', $position)->where('id = ?', $id)->execute();
            }
            $this->redirect($_SERVER['REQUEST_URI']);
        }

        parent::listAction($Query);
    }

    public function getForm()
    {
        $form = parent::getForm();
        if ( 'edit' == $this->getRequest()->getActionName() ) {
            $form->removeElement('code');
        }
        return $form;
    }
}
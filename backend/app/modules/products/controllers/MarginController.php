<?

class Products_MarginController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsMargins';
    const FORM_NAME     = 'Form_Products_Margin';
    const ROUTE_PREFIX  = 'products-margin-';
    const COUNT_ON_PAGE = 20;

    public function editAction()
    {
        $Product = Doctrine::getTable('Products')->find($this->params['id_product']);

        /** @var Form_Products_Margin $form */
        $form = $this->getForm();
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title.'<br><br>' .htmlspecialchars($Product->title));

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $Product->ProductsMargins->fromArray($form->getValues());
                $this->_preSave($Product->ProductsMargins, $form);

                $Product->ProductsMargins->save();
                $this->goBack();
            }
        } else {
            $form->populate($Product->ProductsMargins->toArray());
        }

        echo $form;
    }

    public function editRowSetAction()
    {
        if ( empty($this->params['id_products']) ) {
            echo '<p style="font-size: 2em;">Не выбраны товары</p>';
            return;
        }

        $Products = Doctrine_Query::create()->from('Products')->whereIn('id', explode(',', $this->params['id_products']))->execute();

        $form = $this->getForm();
        $form->removeElement('id_product');

        $form->setDescription(
            Zend_Registry::get('myCurrentRoute')->title . ': '
            . '<ul style="font-weight: normal; font-size: 11px;"><li>'.implode('<li>', $Products->toKeyValueArray('id','title')).'</ul>'
        );

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                if (empty($post['price_pm']) || $post['price_pm'] === "") {
                    unset($post['price_pm']);
                }
                foreach ($Products as $Product) {
                    $Product->ProductsMargins->fromArray($post);
                    $Product->ProductsMargins->save();
                }
                $this->goBack();
            }
        } else {
            $ProductsMargins = new ProductsMargins;
            $ProductsMargins->assignDefaultValues();
            $form->populate($ProductsMargins->toArray());
        }

        echo $form;
    }

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        if (empty($node->price_pm)) {
            $node->price_pm = null;
        }
        if (empty($node->id_partial_payments)) {
            $node->id_partial_payments = null;
        }
    }


}

<?

class Products_ImageController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Products';
    const FORM_NAME     = 'Form_Products_Image';
    const ROUTE_PREFIX  = 'products-product-';
    const COUNT_ON_PAGE = 20;

    public function listAction()
    {
        $this->_helper->viewRenderer->setNoRender(false);

        /** @var Products $product */
        $product = $this->_getNode($this->params['id']);
        $Images = $product->getImage();

        if ( $this->_request->isPost() ) {
            $images = empty($this->_request->getPost()['images']) ? array() : $this->_request->getPost()['images'];
            $Images->selfIntersect($images);
        }

        $this->view->Images = $Images;
        $this->view->Product = $product;
    }

    /**
     * @throws Zend_Exception
     * @throws Zend_Form_Exception
     */
    public function addAction()
    {
        $form = $this->getForm();
        /** @var Products $product */
        $product = $this->_getNode($this->params['id']);
        $Image = $product->getImage();

        $form->getElement('image')->setDestination( $Image->getTempDirForUpload() );

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                if ( !empty($form->getValue('image')) ) {
                    foreach ((array)$form->getValue('image') as $img) {
                        $Image->addNewUploadImage($img);
                    }
                }
                if ( !empty($form->getValue('byLink')) ) {
                    foreach (explode("\n", $form->getValue('byLink')) as $link) {
                        $Image->addNewImage(trim($link));
                    }
                }
                $this->_redirectToList();
            }
        } else {
            $formValues = $this->_request->getQuery();

            if ('vendor' === $this->_request->getParam('from') && $product->VendorsProducts->count()) {
                $images = [];
                /** @var VendorsProducts $vendorsProduct */
                foreach ($product->VendorsProducts as $vendorsProduct) {
                    $images = array_merge($images, $vendorsProduct->getImages());
                }
                $formValues['byLink'] = implode("\n", $images);
            }
            $form->populate($formValues);
        }

        echo $form;
    }

    protected function _redirectToList()
    {
        $this->redirect($this->view->url(array('id'=>$this->params['id'], 'action'=>'list'), 'products-product-image'));
    }

}

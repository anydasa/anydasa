<?
class Products_SetController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsSets';
    const FORM_NAME     = 'Form_Products_Set';
    const ROUTE_PREFIX  = 'products-set-';
    const COUNT_ON_PAGE = 20;

    protected function _preSave(Doctrine_Record $node, Zend_Form $form)
    {
        $post = $this->_request->getPost();

        $node->unlink('Products');
        $node->link('Products', $post['ownerProducts']);

        $node->ProductsSetsProduct->clear();

        foreach ($post['products'] as $product) {
            $ProductsSetsProduct = new ProductsSetsProduct;
            $ProductsSetsProduct->fromArray($product);
            $node->ProductsSetsProduct->add($ProductsSetsProduct);
        }
    }
}
<?

class Products_ImportImagesExcelController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Products';
    const FORM_NAME     = 'Form_Products_Product';
    const ROUTE_PREFIX  = 'products-import-images-excel-product-';
    const COUNT_ON_PAGE = 20;


    public function importAction()
    {
        $this->render('import');
    }
}

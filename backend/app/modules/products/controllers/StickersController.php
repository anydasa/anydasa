<?
class Products_StickersController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsStickers';
    const FORM_NAME     = 'Form_Products_Stickers';
    const ROUTE_PREFIX  = 'products-stickers-';
    const COUNT_ON_PAGE = 20;


    public function editOneAction()
    {
        $Product = $this->findRecordOrException('Products', $this->getParam('id_product'));

        $form = new Form_Products_ProductStickers(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title . ': <h4 style="width: 300px">'.$Product->title.'</h4>');

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();



            if ($form->isValid($post)) {

                $old = $Product->Stickers->toKeyValueArray('id', 'id');
                $unlink = array_diff($old, $form->getValues()['Stickers']);
                $link = array_diff($form->getValues()['Stickers'], $old);

                $productMargins = $Product->ProductsMargins;

                /**
                 * Если стикер Оплата частями убрать с товара, то все заполненные  поля Кол-во Месяцев ОП
                 * и Цена ПМ в блоке маржа становятся незаполненными.
                 */
                if (!empty($unlink) && isset($unlink[25])) {
                    if(!empty($productMargins)) {
                        $productMargins['id_partial_payments'] = null;
                        $productMargins['price_pm'] = null;
                        $productMargins->save();
                    }
                }

                if ( !empty($unlink) ) {
                    $Product->unlink('Stickers', $unlink);
                }
                if ( !empty($link) ) {
                    $Product->link('Stickers', $link);
                }

                $Product->save();
                $this->goBack();
            }
        } else {
            $form->populate(['Stickers'=>$Product->Stickers->toKeyValueArray('id', 'id')]);
        }

        echo $form;
    }

    public function editRowSetAction()
    {
        if ( empty($this->params['id_products']) ) {
            echo '<p style="font-size: 2em;">Не выбраны товары</p>';
            return;
        }

        $Products = Doctrine_Query::create()->from('Products')->whereIn('id', explode(',', $this->params['id_products']))->execute();

        $form = new Form_Products_ProductStickers(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(
            Zend_Registry::get('myCurrentRoute')->title . ': '
            . '<span style="color: Red;">Не сбрасывает установленные</span>'
            . '<ul style="font-weight: normal; font-size: 11px;"><li>'.implode('<li>', $Products->toKeyValueArray('id','title')).'</ul>'
        );

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                foreach ($Products as $Product) {
                    $Product->link('Stickers', $form->getValues()['Stickers']);
                }
                $Products->save();
                $this->goBack();
            }
        }

        echo $form;
    }
}
<?

class Products_PricingHotlineController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsPricingHotline';
    const FORM_NAME     = 'Form_Products_PricingHotline';
    const ROUTE_PREFIX  = 'products-pricing_hotline-';
    const COUNT_ON_PAGE = 20;


    public function editAction()
    {
        $Product = Doctrine::getTable('Products')->find($this->params['id_product']);

        $form = $this->getForm();
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title.'<br><br>' .htmlspecialchars($Product->title));

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $Product->PricingHotline->fromArray($form->getValues());
                $Product->PricingHotline->save();
                $this->goBack();
            }
        } else {
            $form->populate($Product->PricingHotline->toArray());
        }

        echo $form;
    }


    public function editRowsetAction()
    {
        if ( empty($this->params['id_products']) ) {
            echo '<p style="font-size: 2em;">Не выбраны товары</p>';
            return;
        }

        $Products = Doctrine_Query::create()->from('Products')->whereIn('id', explode(',', $this->params['id_products']))->execute();

        $form = $this->getForm();

        $form->setDescription(
            Zend_Registry::get('myCurrentRoute')->title . ': '
            . '<ul style="font-weight: normal; font-size: 11px;"><li>'.implode('<li>', $Products->toKeyValueArray('id','title')).'</ul>'
        );

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                foreach ($Products as $Product) {
                    $Product->PricingHotline->fromArray($form->getValues());
                    $Product->PricingHotline->save();
                }
                $this->goBack();
            }
        }

        echo $form;
    }
}

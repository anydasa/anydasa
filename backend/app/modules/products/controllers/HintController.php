<?

class Products_HintController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsHints';
    const FORM_NAME     = 'Form_Products_Hint';
    const ROUTE_PREFIX  = 'products-hint-';
    const COUNT_ON_PAGE = 20;

}

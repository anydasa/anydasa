<?

class Products_LetMeKnowController extends Controller_AbstractList
{
    const MODEL_NAME    = 'ProductsLetMeKnow';
    const FORM_NAME     = 'Form_Products_LetMeKnow';
    const ROUTE_PREFIX  = 'products-let-me-know-';
    const COUNT_ON_PAGE = 20;
}

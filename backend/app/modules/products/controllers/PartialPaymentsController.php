<?
class Products_PartialPaymentsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'PartialPayments';
    const FORM_NAME     = 'Form_Products_PartialPayments';
    const ROUTE_PREFIX  = 'partial-payments-';
    const COUNT_ON_PAGE = 20;
}
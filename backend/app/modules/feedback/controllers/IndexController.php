<?
class Feedback_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Feedback';
    const FORM_NAME     = 'Form_Feedback';
    const ROUTE_PREFIX  = 'feedback-';
    const COUNT_ON_PAGE = 50;

    public function sendAnswerAction(){
        $tpl_name = 'feedback-answer';
        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode($tpl_name) ) {
            throw new Exception("Отсутсвует почтовый шаблон $tpl_name (MailTemplates)");
        }

        /** @var Feedback $feedback */
        $feedback = $this->findRecordOrException( static::MODEL_NAME, $this->getParam('id'));

        $form = new Form_FeedbackAnswer('FeedbackAnswer');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                
                $form_data = $form->getValues();

                $mail = new Site_Mail('UTF-8');
                $mail->setBodyHtml($form_data['message']);
                $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
                $mail->addTo($feedback->email);
                $mail->addBcc($MailTemplate->MailSmtp->username);

                $mail->setSubject($MailTemplate->subject);

                $mail->send();

                $feedbackAnswer = new FeedbackAnswer();
                $feedbackAnswer->user_id = Zend_Registry::get('CurrentAdmin')->id;
                $feedbackAnswer->message = $form_data['message'];

                $feedback->FeedbackAnswer->add($feedbackAnswer);
                $feedback->save();

                echo 'Отправлено';
                return;
            }
        }

        $data['question'] = $feedback->message;
        $form->message->setValue($MailTemplate->renderHTML($data));

        $this->view->form = $form;

        $this->render('send-answer');
    }

    public function viewAnswersAction(){
        $feedback = $this->findRecordOrException( static::MODEL_NAME, $this->getParam('id'));

        $this->view->list = $feedback->FeedbackAnswer;

        $this->render('list-answer');
    }

    /*
     * return Doctrine_Query
     */
    protected function getQuery($params)
    {
        $Query = parent::getQuery($params);

        if ( !empty($params['sent_answer']) ) {
            if($params['sent_answer'] == 'yes'){
                $Query->andWhere('(SELECT COUNT(*) FROM FeedbackAnswer fa WHERE fa.feedback_id = Feedback.id) > 0');
            }elseif($params['sent_answer'] == 'no'){
                $Query->andWhere('(SELECT COUNT(*) FROM FeedbackAnswer fa WHERE fa.feedback_id = Feedback.id) = 0');
            }
        }

        if ( !empty($params['status'])) {
            if($params['status'] == 3){
                if($params['sent_answer'] != 'yes')
                    $Query->andWhere('posted < NOW() - (INTERVAL \'30 min\')');
                else
                    $Query->andWhere('posted > NOW()');
            }elseif($params['status'] == 1){
                $Query->andWhere('is_looked = 1');
            }elseif($params['status'] == 2){
                $Query->andWhere('is_looked = 0');
            }
        }
        //--------

        return $Query;
    }
}
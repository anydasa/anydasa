<?

class Sms_TemplateController extends Controller_AbstractList
{
    const MODEL_NAME    = 'SmsTemplates';
    const FORM_NAME     = 'Form_Sms_Template';
    const ROUTE_PREFIX  = 'sms-template-';
    const COUNT_ON_PAGE = 20;
}
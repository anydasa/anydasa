<?

class StaticPages_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'StaticPages';
    const FORM_NAME     = 'Form_StaticPage';
    const ROUTE_PREFIX  = 'static-pages-';
    const COUNT_ON_PAGE = 20;
}
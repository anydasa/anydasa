<?

class IndexController extends Controller_Account
{

    public function indexAction()
    {
        $method = "user" . $this->user->id . "Action";

        if ( method_exists($this, $method) ) {
            $this->forward("user" . $this->user->id);
        }
    }

    public function user3Action() {}
    public function user37Action() {}

    public function user44Action()
    {
        $this->forward('user3');
    }

    public function user42Action()
    {
        $this->forward('user3');
    }

    public function user34Action()
    {
        $this->forward('user3');
    }

    public function user16Action()
    {
        $this->forward('user3');
    }


    public function removeLogsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function databaseSizeAction()
    {
        $sql =   'SELECT relname AS "relation", pg_size_pretty(pg_relation_size(C.oid)) AS "size"
                  FROM pg_class C LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
                  WHERE nspname NOT IN (\'pg_catalog\', \'information_schema\')
                    AND pg_relation_size(C.oid) > 1000000
                  ORDER BY pg_relation_size(C.oid) DESC';

        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->execute();

        $this->view->result = $stmt->fetchAll();
    }

    public function recordExistAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        try {
            $query = Doctrine_Query::create()
                ->from($this->params['table'])
                ->where($this->params['field'] . ' = ?', trim($this->params[$this->params['field']]));

                if ( !empty($this->params['exclude']) && !empty($this->params[$this->params['exclude']])) {
                    $query->addWhere($this->params['exclude'] . ' <> ?', trim($this->params[$this->params['exclude']]));
                }

                $isset = (bool)$query->fetchOne();
        } catch (Exception $e) {
            $isset = false;
        }

        if (isset($this->params['not'])) {
            $isset = !$isset;
        }

        echo Zend_Json::encode($isset);
    }

    public function clearCacheAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        Site_Cache::clear();

        $this->redirect('/');
    }

    public function updateIndexSearchAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $sql = 'SELECT set_fts_goods()';

        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->execute();

        $this->view->notices = array('Поиск обновленн');

        echo $this->view->render('process.phtml');
    }

    public function phpinfoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
        phpinfo();
    }

    public function generateModelAction()
    {
        @SF::rmDirR(Zend_Registry::getInstance()->config->path->root . 'models_temp');

        Doctrine::generateModelsFromDb(
            Zend_Registry::getInstance()->config->path->root . 'models_temp',
            array(),
            array(
                'packagesPrefix' => 'Package',
                'packagesPath' => '',
                'packagesFolderName' => 'packages',
                'suffix' => '.php',
                'generateBaseClasses' => true,
                'baseClassesPrefix' => 'generated',
                'baseClassesDirectory' => '',
                'baseClassName' => 'Doctrine_Record'
            )
        );

        echo '<p>Модели сгенерированы.</p>';
    }

    public function pregMatchAction()
    {
        preg_match_all('/'.$this->params['re'].'/i', $this->params['input'], $rr);
        foreach($rr[1] as $item) {
            $this->view->result .= $item . "\n";
        }
    }

    public function showTimeAction()
    {
        $uri = Zend_Uri::factory($_SERVER['HTTP_REFERER']);
    	$uri->addReplaceQueryParameters(array('showtime'=>1));

        $this->redirect($uri->getUri());
    }

    public function backupAction()
    {
        $dbusername = Zend_Registry::getInstance()->config->db->params->username;
        $dbname     = Zend_Registry::getInstance()->config->db->params->dbname;
        $dbpass     = Zend_Registry::getInstance()->config->db->params->password;
        $rootpath   = Zend_Registry::getInstance()->config->path->root;

        $distPath   = dirname(Zend_Registry::getInstance()->config->path->root) .
                        '/backups/'.
                        basename(Zend_Registry::getInstance()->config->path->root).'/';


        $tarCommand  = 'tar -pczf ' . $distPath . date("Y.m.d-H:i").'.tar.bz2' . ' ' . $rootpath;
        $dumpCommand = 'export PGPASSWORD='.$dbpass . '; pg_dump -h 127.0.0.1 --exclude-table-data \'*_stat\' --exclude-table-data \'*_logs\' -U ' . $dbusername . ' ' . $dbname .
                       ' > ' .
                       $rootpath . 'database.sql';

        @unlink($rootpath . 'database.sql');

        if ( !$this->params['full'] ) {
             $tarCommand .= ' --exclude "'.Zend_Registry::getInstance()->config->path->root .'pix"';
        }
        exec($dumpCommand);
        exec($tarCommand);
        @unlink($rootpath . 'database.sql');

        $this->_redirect('/');
    }

    public function dumpDatabaseAction()
    {
        $dbusername = Zend_Registry::getInstance()->config->db->params->username;
        $dbname     = Zend_Registry::getInstance()->config->db->params->dbname;
        $dbpass     = Zend_Registry::getInstance()->config->db->params->password;
        $rootpath   = Zend_Registry::getInstance()->config->path->root;

        if ( strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ) {
            exec(Zend_Registry::getInstance()->config->path->bin. "dumpDB.bat {$dbpass} -U $dbusername $dbname > {$rootpath}database.sql");
        } else {
            unlink($rootpath . 'database.tar.bz2');
            exec('export PGPASSWORD='.$dbpass . '; pg_dump -O -x -h 127.0.0.1 --exclude-table-data \'*_stat\' --exclude-table-data \'*_logs\' -U' . $dbusername . ' ' . $dbname . ' > ' . $rootpath . 'database.sql');
            exec('tar czf ' . $rootpath . 'database.tar.bz2' . ' -C '. $rootpath . ' database.sql');
            unlink($rootpath . 'database.sql');
        }

        $this->redirect('/');
    }

    public function dumpDatabaseAllAction()
    {
        $dbusername = Zend_Registry::getInstance()->config->db->params->username;
        $dbname     = Zend_Registry::getInstance()->config->db->params->dbname;
        $dbpass     = Zend_Registry::getInstance()->config->db->params->password;
        $rootpath   = Zend_Registry::getInstance()->config->path->root;

        if ( strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ) {
            exec(Zend_Registry::getInstance()->config->path->bin. "dumpDB.bat {$dbpass} -U $dbusername $dbname > {$rootpath}database.sql");
        } else {
            unlink($rootpath . 'database.tar.bz2');
            exec('export PGPASSWORD='.$dbpass . '; pg_dump -O -x -h 127.0.0.1 -U' . $dbusername . ' ' . $dbname . ' > ' . $rootpath . 'database.sql');
            exec('tar czf ' . $rootpath . 'database.tar.bz2' . ' -C '. $rootpath . ' database.sql');
            unlink($rootpath . 'database.sql');
        }

        $this->redirect('/');
    }
}

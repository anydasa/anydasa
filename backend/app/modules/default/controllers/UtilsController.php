<?

class UtilsController extends Controller_Account
{

    public function slugAction()
    {
        $return = '';

        if ( $this->_request->isPost() && !empty($this->_request->getPost('string')) ) {
            $return = SF::slugify($this->_request->getPost('string'));
        }

        return $this->_helper->json->sendJson($return);
    }


}

<?

class ErrorController extends Controller_Main
{

    public function errorAction()
    {
        $this->_helper->layout()->disableLayout();

        if (null !== ($errors = $this->_getParam('error_handler'))) {

            switch ($errors->type) {

                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
                case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                    $this->pageNotFoundAction();
                    break;
                default:
                    if ($errors->exception instanceof Site_Exception_AccessDenied) {
                        $this->accessDediedAction();
                    }
                    break;
            }
        } else {
            Site_Log::add('Call Error Controller', Zend_Log::ERR);
        }
    }

    public function pageNotFoundAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');
        $this->render('page-not-found');
    }

    public function accessDediedAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setRawHeader('HTTP/1.1 401 Access denied');
        $this->render('access-denied');
    }

    public function unauthorizedAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setRawHeader('HTTP/1.1 401 Unauthorized');
    }

}


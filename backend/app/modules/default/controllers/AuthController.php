<?

class AuthController extends Controller_Main
{

    function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->redirect($this->view->url(array(), 'login'));
    }

    public function loginAction()
    {
        if ( Zend_Auth::getInstance()->getIdentity() ) {
            $this->redirect($this->view->url(array(), 'main'));
        }

        $this->_helper->layout->disableLayout();

        $form = new Form_Auth;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {

                $authAdapter = new Site_Auth_Adapter_Doctrine_Table(Doctrine_Manager::connection());
                $authAdapter->setTableName('AdminUsers')
                    ->setIdentityColumn('login')
                    ->setCredentialColumn('password')
                    ->setIdentity($form->login->getValue())
                    ->setCredential(md5($form->password->getValue()));

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);

                if ($result->isValid()) {
                    $data = $authAdapter->getResultRowObject(null, 'password');
                    if (0 == $data->is_enabled) {
                        $form->addErrorMessage($this->view->translate('Данный аккаунт отключен'))->markAsError();
                        $auth->clearIdentity();
                    } else {

                        $Sess = new stdClass;
                        $Sess->user = $data;

                        //------------------ //для фронтенда
                        $Sess->allowEditFile =
                            Doctrine::getTable('AdminUsers')
                                ->find($data->id)
                                ->isAllowedRoute('file-edit');
                        //------------------ \\для фронтенда

                        Zend_Auth::getInstance()->getStorage()->write($Sess);

                        if (1 == $form->getElement('remember')->getValue()) {
                            Zend_Session::rememberMe(1209600);
                            $authNamespace = new Zend_Session_Namespace('Zend_Auth');
                            $authNamespace->setExpirationSeconds(1209600);
                        }

                        $this->redirect( $_SERVER['REQUEST_URI'] );
                    }
                } else {
                    $form->addErrorMessage($this->view->translate('Неверный логин или пароль'))->markAsError();
                }
            }
        }
        $this->view->form = $form;
    }

}
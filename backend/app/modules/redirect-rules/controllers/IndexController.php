<?
class RedirectRules_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'RedirectRules';
    const FORM_NAME     = 'Form_RedirectRule';
    const ROUTE_PREFIX  = 'redirect-rules-';
    const COUNT_ON_PAGE = 50;

    protected function getQuery($params, $modelName = null)
    {
        $Query = parent::getQuery($params, $modelName);

        if ( empty($this->params['sort']) ) {
            $Query->orderBy('redirect_count desc');
        }

        return $Query;
    }
}
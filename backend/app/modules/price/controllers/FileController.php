<?

class Price_FileController extends Controller_Account
{

	public function addAction()
	{
		$form = new Form_Price;

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            if ($form->isValid($post)) {
                $form->getValues();
				$this->_redirectToList();
            }
        }
		$this->view->form = $form;
	}

	private function _redirectToList()
    {
        $this->_redirect($this->view->url(array('action'=>'list'), 'price-parser'));
    }
}

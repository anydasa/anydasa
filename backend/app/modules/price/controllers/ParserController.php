<?

class Price_ParserController extends Controller_Account
{
    private $_price_keys = array(
                                    'B' => 'code',
                                    'C' => 'is_new',
                                    'D' => 'title',
                                    'E' => 'brand',
                                    'F' => 'price_min',
                                    'G' => 'price_partner',
                                    'H' => 'price_trade',
                                    'I' => 'price_retail',
                                    'J' => 'balance',
    );

    public function indexAction()
    {
        if ( !is_file(Zend_Registry::getInstance()->config->path->files .'price.xls') ) {
            $this->_redirect($this->view->url(array('action'=>'list'), 'price-add'));
        }

	    $rows = array();
        $Excel = PHPExcel_IOFactory::load(Zend_Registry::getInstance()->config->path->files .'price.xls');

        foreach($Excel->getActiveSheet()->getRowIterator() as $row) {
            $isGarbage = true;
            $temp_row = array();

            foreach($row->getCellIterator() as $key=>$cell) {

                $columnKey = $cell->getColumn();
                $value = trim($cell->getFormattedValue());

                if ( !isset($this->_price_keys[$columnKey]) )   continue; //Если колонка не описана, пропускаем
                if ( 'K' == $columnKey )                        break;    //Если номер колонки за пределами описаных, останавлиаем обработку ряда и приступаем к следующему


                if ('code' == $this->_price_keys[$columnKey] && is_numeric($value) ) {
                // Если артикул не число, значит это мусор, не останавливаем обработку, чтоб потом отобразить мусорные значения
                    $isGarbage = false;
                }

                if ( 'is_new' == $this->_price_keys[$columnKey] ) {
                    $value = ('' == trim($value)) ? 0 : 1;
                }

//---------------- КОСТЫЛЬ Для кодировки --- ну нельзя узнать какая кодировка :((((((
                $temp_value= mb_convert_encoding($value, 'Windows-1252', 'UTF8');
                $temp_value= mb_convert_encoding($temp_value, 'UTF8', 'Windows-1251');
                if ( !strstr($temp_value, '???') ) $value = $temp_value;
//-----------------------------------------------------------------------------------

                $temp_row[$this->_price_keys[$columnKey]] = $value;

            }

            if ($isGarbage) {
                $rows['garbage'][] = $temp_row;
            } else {
                $rows['separated'][$temp_row['code']] = $temp_row;
            }

        }

        if ($this->_request->isPost()) {

            $currentTempProductCodes = Doctrine_Query::create()->select('code,code')->from('ProductsTemp')->execute()->toKeyValueArray('code','code');
            $separatedRows = array_diff_key($rows['separated'], $currentTempProductCodes);

            $Collection = new Doctrine_Collection('ProductsTemp');
            $Collection->fromArray($separatedRows);
            $Collection->save();

            unlink(Zend_Registry::getInstance()->config->path->files .'price.xls');

            $this->redirect($this->view->url(array(), 'products-temp-sync'));

        }

        $this->view->rows = $rows;

        $this->render('list');
	}

	private function _redirectToList()
    {
        $this->_redirect($this->view->url(array('action'=>'list'), 'main'));
    }
}

<?
class NotFound_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'NotFoundPages';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'not-found-';
    const COUNT_ON_PAGE = 50;

    public function listAction($Query = null)
    {
        $Query = $this->getQuery($this->_request->getQuery());

        if ( $this->_request->isPost() && $this->_request->getPost('delete') ) {
            $Query->removeDqlQueryPart('orderby');
            $Query->delete('NotFoundPages '. $Query->getRootAlias());
            $Query->execute();

            $this->goBack();
        }



        parent::listAction($Query);
    }

    protected function getQuery($params, $modelName = null)
    {
        $Query = parent::getQuery($params, $modelName);

        /*$Query->addWhere('uri NOT IN (SELECT t1.uri
                                        FROM NotFoundPages t1, RedirectRules t2
                                        WHERE t1.uri ~ t2.from_url
                                        GROUP BY t1.uri
        )');*/

        if ( empty($this->param['sort']) ) {
            $Query->orderBy('time desc');
        }

        return $Query;
    }

    public function autocompleteAction()
    {
        return $this->_helper->json(NotFoundPages::findByMask($this->params['term']));
    }

    public function deleteGarbageAction()
    {
        $sql = "DELETE FROM not_found_pages WHERE uri ~* '.[(png)|(jpg)|(jpeg)|(gif)|(ico)]$'";

        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->execute();

        $this->goBack();
    }

}
<?
class Promotions_IndexController extends Controller_AbstractList
{
    const MODEL_NAME    = 'Promotions';
    const FORM_NAME     = 'Form_Promotion';
    const ROUTE_PREFIX  = 'promotions-';
    const COUNT_ON_PAGE = 50;

    protected function _postSave($node, $form)
    {
        if ( !empty($form->getValue('image_delete')) ) {
            @unlink($node->getImagePath());
        }

        if ( !empty($form->getValue('image')) ) {
            $target = $node->getImagePath();

            $form->image->addFilter('Rename', array('target' => $target, 'overwrite'  => true));
            $form->image->receive();

            $im = new Imagick($target);
            $im->resizeImage(500, 375, imagick::FILTER_LANCZOS, 0.9, true);
            $im->writeImage();

        }
        
        foreach ($node->Banners as $banner){
            $banner->is_enabled = $form->getValue('is_enabled');
            $banner->save();
        }

    }

}
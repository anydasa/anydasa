<?
class Promotions_ProductsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'PromotionsProducts';
    const FORM_NAME     = 'Form_Promotion';
    const ROUTE_PREFIX  = 'promotions-products-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        $this->view->Promotion = $this->findRecordOrException('Promotions', $this->params['id_promotion']);
        $this->render('list');
    }

    public function linkAction()
    {
        $Promotion = $this->findRecordOrException('Promotions', $this->params['id_promotion']);

        if ( $this->_request->isPost() ) {
            $Promotion->link('Products', $this->_request->getPost('id_product'));
            $Promotion->save();
        }
    }

    public function unlinkAction()
    {
        $Promotion = Doctrine::getTable('Promotions')->find($this->params['id_promotion']);

        if ( $this->_request->isPost() ) {
            $Promotion->unlink('Products', [$this->_request->getPost('id_product')]);
            $Promotion->save();
        }

        $this->redirect(
            $this->view->url(
                array('id_promotion'=>$Promotion->id),
                self::ROUTE_PREFIX.'list'
            ) . '?' . $_SERVER['QUERY_STRING']
        );
    }
}
<?

class SpellingError_IndexController extends Controller_Account
{

    public function listAction()
    {
        $Query = Doctrine_Query::create()->select('*')->from('SpellingErrors')->addWhere('checked = 0')->orderBy('time DESC');
        $pager = new Doctrine_Pager($Query, $this->params['page'], 40);
        $this->view->list = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
    }
    public function checkAction()
    {
        if ($Node = $this->_getNode($this->params['id'])) {
            $Node->checked = 1;
            $Node->save();
            $this->_redirectToList();
        }
    }
    private function _getNode($id = null)
    {
        if ($node = Doctrine::getTable('SpellingErrors')->find($id)) {
            return $node;
        }
        $this->_redirectToList();
    }
    private function _redirectToList()
    {
        $this->_redirect($this->view->url(array(), 'spelling-error-list'));
    }
}

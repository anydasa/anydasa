<?

class Adwords_AdController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdwordsAd';
    const FORM_NAME     = 'Form_Adwords_Ad';
    const ROUTE_PREFIX  = 'adwords-ad-';
    const COUNT_ON_PAGE = 50;

    public function listAction()
    {
        parent::listAction();

        if ( $this->_request->isXmlHttpRequest() ) {
            $this->render('simple-list');
        }
    }

    protected function getForm()
    {
        $form = parent::getForm();

        if ( !empty($this->getParam('id_group')) ) {
            $form->getElement('id_group')->setValue($this->getParam('id_group'));
        }

        return $form;
    }


    public function downloadAction()
    {
        $AdGroupIds = array_values(Doctrine::getTable('AdwordsAdgroup')->findAll()->toKeyValueArray('id', 'id'));

        foreach (AdwordsAd::getRemoteAds($AdGroupIds) as $ad) {
            if ( !($Record = Doctrine::getTable('AdwordsAd')->find($ad['id'])) ) {
                $Record = new AdwordsAd;
            }
            $Record->isDisabledTrigger = true;
            $Record->fromArray($ad);
            $Record->save();
        }

        $this->_redirectToList();
    }
}
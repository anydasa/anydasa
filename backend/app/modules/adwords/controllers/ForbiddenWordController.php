<?

class Adwords_ForbiddenWordController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdwordsForbiddenWord';
    const FORM_NAME     = 'Form_Adwords_ForbiddenWord';
    const ROUTE_PREFIX  = 'adwords-forbidden-word-';
    const COUNT_ON_PAGE = 50;

}
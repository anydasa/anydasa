<?

class Adwords_RuleKeywordsController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdwordsRuleKeywords';
    const FORM_NAME     = 'Form_Adwords_Rule_Keywords';
    const ROUTE_PREFIX  = 'adwords-rule-keywords-';
    const COUNT_ON_PAGE = 50;
}
<?

class Adwords_ProductKeyController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdwordsProductKey';
    const FORM_NAME     = '';
    const ROUTE_PREFIX  = 'adwords-product-key-';
    const COUNT_ON_PAGE = 50;

    protected function getQuery($params, $modelName = null)
    {
        $p_params = $params['Products'];
        unset($params['Products']);

        $Query = parent::getQuery($params, $modelName);
        $Query->innerJoin('AdwordsProductKey.Products p');

        if ( is_numeric($p_params['status']) ) {
            $Query->addWhere('p.status = ?', $p_params['status']);
        }
        if ( is_numeric($p_params['id_catalog']) ) {
            $Query->addWhere(sprintf('p.catalogs @> ARRAY[%s]', $p_params['id_catalog']));
        }


        return $Query;
    }

    public function fillAction()
    {
        Doctrine_Manager::connection()
            ->prepare('SELECT adwords_product_key_fill()')
            ->execute();

        $this->_redirectToList();
    }

    public function saveAction()
    {
        if ( $this->_request->isPost() ) {
            $Record = $this->findRecordOrException(self::MODEL_NAME, $this->getParam('id_product'));
            $Record->fromArray($this->_request->getPost());
            $Record->save();
        }
    }

    public function uploadAction()
    {
        $form = new Form_Adwords_Upload(self::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {

                $Excel = PHPExcel_IOFactory::load($form->file->getFileInfo()['file']['tmp_name']);

                $array = $Excel->getActiveSheet()->toArray(null, true, true, true);

                $first_coll = array_shift($array);

                array_walk($first_coll, function (& $item) {
                    preg_match('/\((.+?)\)/', $item, $res);
                    $item = $res[1];
                });

                $result = [];
                foreach ($array as $item) {
                    $item = array_combine($first_coll, $item);
                    if ( !empty($item['id_product']) ) {
                        $Record = new AdwordsProductKey;
                        $Record->fromArray($item);
                        $Record->replace();
                        $result []= $Record->toArray();
                    }
                }
                $this->view->result = $result;
                $this->render('upload-result');
                return;
            }
        }


        echo $form;
    }
    public function downloadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        $array = [];
        $offset = 0;
        $limit = 300;
        while(true) {
            $Query = $this->getQuery($this->params);
            $Query->limit($limit);
            $Query->offset($offset*$limit);

            $result = $Query->fetchArray();
            if ( empty($result) ) {
                break;
            }
            $array = array_merge($array, $result);
            $offset++;
        }

        $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
        $ExcelReport->setHeadRow(array(
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('id_product')['comment'] .        ' (id_product)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('product_type')['comment'] .      ' (product_type)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('brand_name')['comment'] .        ' (brand_name)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('manufacturer_code')['comment'] . ' (manufacturer_code)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('product_model')['comment'] .     ' (product_model)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('param1')['comment'] .            ' (param1)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('param2')['comment'] .            ' (param2)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('param3')['comment'] .            ' (param3)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('param4')['comment'] .            ' (param4)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('param5')['comment'] .            ' (param5)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('headline1')['comment'] .         ' (headline1)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('headline2')['comment'] .         ' (headline2)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('description1')['comment'] .      ' (description1)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('description2')['comment'] .      ' (description2)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('description3')['comment'] .      ' (description3)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('description4')['comment'] .      ' (description4)',
            Doctrine::getTable(self::MODEL_NAME)->getDefinitionOf('is_ready')['comment'] .          ' (is_ready)',
        ));

        foreach ($array as $item) {
            $ExcelReport->addDataRow(array(
                $item['id_product'],
                $item['product_type'],
                $item['brand_name'],
                $item['manufacturer_code'],
                $item['product_model'],
                $item['param1'],
                $item['param2'],
                $item['param3'],
                $item['param4'],
                $item['param5'],
                $item['headline1'],
                $item['headline2'],
                $item['description1'],
                $item['description2'],
                $item['description3'],
                $item['description4'],
                ($item['is_ready'] ? '1' : '0'),
            ));
        }

        $ExcelReport->Excel->getActiveSheet()->getRowDimension(1)->setRowHeight(15);

        $lastRow = $ExcelReport->Excel->getActiveSheet()->getHighestDataRow();

        foreach(['K', 'L'] as $columnID) {
            $objConditional1 = new PHPExcel_Style_Conditional();
            $objConditional1->setConditionType(PHPExcel_Style_Conditional::CONDITION_EXPRESSION)->setConditions("LEN({$columnID}2) > 30");
            $objConditional1->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
            $conditionalStyles = $ExcelReport->Excel->getActiveSheet()->getStyle("{$columnID}2")->getConditionalStyles();
            array_push($conditionalStyles, $objConditional1);
            $ExcelReport->Excel->getActiveSheet()->getStyle("{$columnID}2:{$columnID}{$lastRow}")->setConditionalStyles($conditionalStyles);
        }

        foreach(['M', 'N', 'O', 'P'] as $columnID) {
            $objConditional1 = new PHPExcel_Style_Conditional();
            $objConditional1->setConditionType(PHPExcel_Style_Conditional::CONDITION_EXPRESSION)->setConditions("LEN({$columnID}2) > 38");
            $objConditional1->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getEndColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
            $conditionalStyles = $ExcelReport->Excel->getActiveSheet()->getStyle("{$columnID}2")->getConditionalStyles();
            array_push($conditionalStyles, $objConditional1);
            $ExcelReport->Excel->getActiveSheet()->getStyle("{$columnID}2:{$columnID}{$lastRow}")->setConditionalStyles($conditionalStyles);
        }

        $lastColumn = $ExcelReport->Excel->getActiveSheet()->getHighestDataColumn();
        foreach(range('A', $lastColumn) as $columnID) {
            $ExcelReport->Excel->getActiveSheet()->getColumnDimension($columnID)->setWidth(10);
        }
        $ExcelReport->Excel->getActiveSheet()->getStyle("A1:{$lastColumn}1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

        echo $ExcelReport->getReport(false);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename="AdwordsProduct.xlsx"')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }
}
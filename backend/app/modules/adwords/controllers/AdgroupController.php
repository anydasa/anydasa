<?

class Adwords_AdgroupController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdwordsAdgroup';
    const FORM_NAME     = 'Form_Adwords_Adgroup';
    const ROUTE_PREFIX  = 'adwords-adgroup-';
    const COUNT_ON_PAGE = 100;

    public function listAction()
    {
        parent::listAction();

        if ( $this->_request->isXmlHttpRequest() ) {
            $this->render('simple-list');
        }
    }

    protected function getForm()
    {
        $form = parent::getForm();

        if ( 'edit' == $this->getRequest()->getActionName() ) {
            $form->removeElement('id_camp');
        } elseif ( !empty($this->getParam('id_camp')) ) {
            $form->getElement('id_camp')->setValue($this->getParam('id_camp'));
        }

        return $form;
    }

    public function downloadAction()
    {
        $campaignIds = array_values(Doctrine::getTable('AdwordsCampaign')->findAll()->toKeyValueArray('id', 'id'));

        $adGroups = AdwordsAdgroup::getRemoteGroups($campaignIds);

        if ( !empty($adGroups) ) {
            foreach ($adGroups as $item) {
                if ( !($Record = Doctrine::getTable('AdwordsAdgroup')->find($item['id'])) ) {
                    $Record = new AdwordsAdgroup;
                }
                $Record->isDisabledTrigger = true;
                $Record->fromArray($item);
                $Record->download_at = 'NOW()';
                $Record->save();
            }

            /*$keywords = AdwordsAdgroupKeywords::getRemoteKeywords(array_column($adGroups, 'id'));
            foreach ($keywords as $item) {
                $Record = new AdwordsAdgroupKeywords;
                $Record->fromArray($item);
                $Record->replace();
            }*/
        }

        $this->_redirectToList();
    }



    public function keysAction()
    {
        $id_group = $this->getParam('id');

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();

            AdwordsAdgroupKeywords::removeKeywords($id_group);
            AdwordsAdgroupKeywords::addKeywords($post['keys'], $id_group);
        }

        $this->view->negative_list =
            Doctrine_Query::create()
                ->from('AdwordsAdgroupKeywords')
                ->where('id_group = ?', $id_group)
                ->addWhere('criterion_use = ?', 'NEGATIVE')
                ->orderBy('match_type, text')
                ->execute();

        $this->view->biddable_list =
            Doctrine_Query::create()
                ->from('AdwordsAdgroupKeywords')
                ->where('id_group = ?', $id_group)
                ->addWhere('criterion_use = ?', 'BIDDABLE')
                ->orderBy('criterion_use, match_type, text')
                ->execute();

        $this->render('keys');
    }

    public function postKeywordsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->_request->isPost() ) {
            $keys = array_values($this->_request->getPost()['keys']);
            $this->view->result = AdwordsAdgroupKeywords::addKeywords($keys);
            $this->render('keys2success');
        }

        $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
    }

    public function genKeywordsAction()
    {
        $ids = (array) explode(',', $this->getParam('id'));
        $ids[] = 0;

        $Collection = Doctrine_Query::create()
            ->from('AdwordsAdgroup')
            ->whereIn('id', $ids)
            ->addWhere('id_product IS NOT NULL')
            ->execute();

        if ( $Collection->count() == 0 ) {
            $this->getResponse()->setRawHeader('HTTP/1.1 500 Bad Request');
            echo 'Не выбраны записи';
            return ;
        }

        $form = new Form_Adwords_Adgroup_Keywords;
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);
        $form->setProducts($Collection);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $params = $form->getValues();
                $keys = [];
                foreach ($Collection as $Record) {
                    foreach (AdwordsRuleKeywords::generateFromText($Record->Products->title, $params) as $key) {
                        $key['id_group'] = $Record->id;
                        $keys[] = $key;
                    }
                }
                $this->view->keys = $keys;
                $this->render('keys2');
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        print $form;
    }

    public function removeKeywordsAction()
    {
        $ids = (array) explode(',', $this->getParam('id'));
        $ids[] = 0;

        $Collection = Doctrine_Query::create()
            ->from('AdwordsAdgroup')
            ->whereIn('id', $ids)
            ->execute();

        if ( $Collection->count() == 0 ) {
            $this->getResponse()->setRawHeader('HTTP/1.1 500 Bad Request');
            echo 'Не выбраны записи';
            return ;
        }

        $form = new Form_Empty(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription(Zend_Registry::get('myCurrentRoute')->title);
        $form->addSubmitElement();
        $form->save->setLabel('Удалить');

        $formDescription = $form->getDescription();
        $formDescription .= '<ul style="font-size: 11px; color: gray; margin-top: 5px; max-height: 400px; overflow-y: scroll">';
        foreach ($Collection as $Record) {
            $formDescription .= "<li>{$Record->Products->title}</li>";
        }
        $formDescription .= '</ul>';
        $form->setDescription($formDescription);

        if ( $this->_request->isPost() ) {
            AdwordsAdgroupKeywords::removeKeywords( array_values($Collection->toKeyValueArray('id', 'id')) );
            $this->_redirectToList();
        }

        print $form;
    }

    public function multiAdAction()
    {
        $ids = (array) explode(',', $this->getParam('id'));
        $ids[] = 0;

        $Collection = Doctrine_Query::create()
            ->from('AdwordsAdgroup')
            ->whereIn('id', $ids)
            ->addWhere('id_product IS NOT NULL')
            ->execute();

        if ( $Collection->count() == 0 ) {
            $this->getResponse()->setRawHeader('HTTP/1.1 500 Bad Request');
            echo 'Не выбраны записи';
            return ;
        }

        $form = new Form_Adwords_Adgroup_Ad(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $formDescription = Zend_Registry::get('myCurrentRoute')->title;
        $formDescription .= '<ul style="font-size: 11px; color: gray; margin-top: 5px; max-height: 400px; overflow-y: scroll">';
        foreach ($Collection as $Record) {
            $formDescription .= "<li>{$Record->name}</li>";
        }
        $formDescription .= '</ul>';

        $form->setDescription($formDescription);


        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {

                if ( !empty($form->getValue('remove_exist')) ) {
                    AdwordsAd::removeAds(array_values($Collection->toKeyValueArray('id','id')));
                }

                if ( !empty($form->getValue('id_rule')) ) {
                    $ads = [];


                    foreach ($Collection as $Record) {
                        $display_url = AdwordsAd::makeDisplayUrl('anydasa.com', $Record->Products->title);
                        $description1 = 'Отличная цена.';

                        if ( $Record->Products->getPrice('retail', 'UAH') > 0 ) {
                            $description1 = 'Всего за ' . $Record->Products->getFormatedPrice('retail', 'UAH', true);
                        }

                        $ads []= [
                            'id_group'       => $Record->id,
                            'headline'       => trunc_words($Record->Products->getFullName(), 30),
                            'description1'   => $description1,
                            'description2'   => 'Действуют акции и скидки.',
                            'display_url'    => $display_url,
                            'final_urls'     => array($Record->Products->getUrl()),
                            'status'         => 'ENABLED'
                        ];

                        $ads []= [
                            'id_group'       => $Record->id,
                            'headline'       => trunc_words($Record->Products->getFullName(), 30),
                            'description1'   => 'Доставка 1-2 дня. Лояльные цены.',
                            'description2'   => 'Действуют акции и скидки.',
                            'display_url'    => $display_url,
                            'final_urls'     => array($Record->Products->getUrl()),
                            'status'         => 'ENABLED'
                        ];

                        $ads []= [
                            'id_group'       => $Record->id,
                            'headline'       => trunc_words($Record->Products->getFullName(), 30),
                            'description1'   => 'Отличное качество сервиса. Гарантия.',
                            'description2'   => 'Конкурентные цены. 7 лет на рынке.',
                            'display_url'    => $display_url,
                            'final_urls'     => array($Record->Products->getUrl()),
                            'status'         => 'ENABLED'
                        ];
                    }
                    AdwordsAd::addAds($ads);
                }

                $this->_redirectToList();
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        print $form;
    }

    public function fromProductAction()
    {
        $id_products = (array) explode(',', $this->getParam('id_products'));
        $id_products[] = 0;

        $Products = Products_Query::create()->setParamsQuery(['id' => $id_products])->execute();
        if ( $Products->count() == 0 ) {
            $this->getResponse()->setRawHeader('HTTP/1.1 500 Bad Request');
            echo 'Не выбраны товары';
            return ;
        }

        $form = new Form_Adwords_FromProduct(static::MODEL_NAME);
        $form->setAction($_SERVER['REQUEST_URI']);
        $formDescription = Zend_Registry::get('myCurrentRoute')->title;
        $formDescription .= '<ul style="font-size: 11px; margin-top: 5px;">';
        foreach ($Products as $Product) {
            $formDescription .= "<li><span style='color: #808080;'>{$Product->type}</span> {$Product->title}</li>";
        }
        $formDescription .= '</ul>';

        $form->setDescription($formDescription);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                AdwordsAdgroup::createFromProduct($Products, $form->getValue('id_camp'), $form->getValue('amount'));
                print 'Запрос сделан';
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        print $form;
    }



}
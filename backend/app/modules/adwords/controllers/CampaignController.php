<?

class Adwords_CampaignController extends Controller_AbstractList
{
    const MODEL_NAME    = 'AdwordsCampaign';
    const FORM_NAME     = 'Form_Adwords_Campaign';
    const ROUTE_PREFIX  = 'adwords-campaign-';
    const COUNT_ON_PAGE = 50;

    public function downloadAction()
    {
        foreach (AdwordsCampaign::getRemoteCampaigns() as $campaign) {
            if ( !($Record = Doctrine::getTable('AdwordsCampaign')->find($campaign['id'])) ) {
                $Record = new AdwordsCampaign;
            }
            $Record->isDisabledTrigger = true;
            $Record->fromArray($campaign);
            $Record->save();
        }

        $this->_redirectToList();
    }

}
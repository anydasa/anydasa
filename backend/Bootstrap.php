<?
class Bootstrap
{
    public static $registry;
    public static $frontController;

    public function __construct()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);
    }

    public static function run($config)
    {

        try {
            self::setupRegistry();
            self::setupLogger();
            self::setupConfiguration($config);

            self::setupLog();
            self::setupView();
            self::setupDbAdapter();

            self::setupSession();
            self::setupFrontController();
            self::setupRouter();
            self::setupCache();
            //self::registerAdminLogPlugin();
            self::registerMultiLangRouter();
            self::$frontController->dispatch();

        } catch (Doctrine_Connection_Pgsql_Exception $e) {
            header('HTTP/1.1 500 Bad Request');
            echo $e->getMessage();
        } catch (Exception $e) {
            throw $e;
            
            header('HTTP/1.1 500 Bad Request');
            exit;
        }
    }
    public static function setupCache()
    {
        Site_Cache::init();
    }

    public static function setupFrontController()
    {
        self::$frontController = Zend_Controller_Front::getInstance();
        self::$frontController->addModuleDirectory(self::$registry->config->path->app . 'modules');
        self::$frontController->throwExceptions(self::$registry->config->debug->on);
    }

    public static function registerAdminLogPlugin()
    {
        self::$frontController->registerPlugin(new Site_Controller_Plugin_AdminLog());
    }

    public static function registerMultiLangRouter()
    {
        self::$frontController->registerPlugin(new Site_Controller_Plugin_RouteLang(
                self::$registry->config->default_locale,
                self::$registry->config->locales->toArray(),
                self::$registry->config->path->langs
        ));
    }

    public static function setupRegistry()
    {
        self::$registry = new Zend_Registry();
        Zend_Registry::setInstance(self::$registry);
    }

    public static function setupSession()
    {
        Zend_Session::setOptions(self::$registry->config->session->toArray());
    }

    public static function setupLog()
    {
        Site_Log::init(self::$registry->config->path->logs, self::$registry->config->debug->on);
    }

    public static function setupConfiguration($config)
    {
        self::$registry->config = new Zend_Config($config);
    }

    public static function setupView()
    {
        Zend_Layout::startMvc(array(
            'layoutPath' => self::$registry->config->path->app . 'views/layouts',
            'layout' => 'main'
        ));

        $layout = Zend_Layout::getMvcInstance();

        $view = $layout->getView();

        $view->setBasePath(self::$registry->config->path->app . 'views/');

        $layout->setView($view);

        require_once 'Zend/Controller/Action/Helper/ViewRenderer.php';
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewDoctype = new Zend_View_Helper_Doctype();
        $viewDoctype->setDoctype('XHTML1_TRANSITIONAL');

        $viewDoctype->setView($view);
        $viewRenderer->setView($view);
        //Zend_Registry::get('Zend_Translate')->setDisableTranslator(true);

        require_once 'Zend/Controller/Action/HelperBroker.php';
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        $view->addHelperPath(self::$registry->config->path->app . 'views/helpers', 'Site_View_Helper');
        $view->addHelperPath(self::$registry->config->path->lib . 'Site/View/Helper', 'Site_View_Helper');

    }

    public static function setupDbAdapter()
    {
        require_once 'Site/Db/Connection.php';
        require_once 'Site/Db/Doctrine.php';

        $db = self::$registry->config->db;

        $connection = new Site_Db_Connection($db->dsn, $db->username, $db->password);
        Site_Db_Doctrine::init(
            $connection, self::$registry->config->path->doctrineModels
        );
    }

    public static function setupRouter()
    {
        require_once 'Zend/Controller/Router/Rewrite.php';
        require_once 'Zend/Config/Ini.php';

        $router = self::$frontController->getRouter();
        $router->removeDefaultRoutes();

        $router->addConfig(new Zend_Config_Ini(self::$registry->config->path->app . 'etc/core-router.ini', 'core-route'), 'routes');
        $router->addConfig(new Zend_Config(AdminRoutes::getAsArray()));


        self::$frontController->setRouter($router);
    }

    public static function setupLogger()
    {
        Sentry\init(['dsn' => getenv('SENTRY_DSN') ]);

        $sentryClient = \Sentry\ClientBuilder::create(['dsn' => getenv('SENTRY_DSN')])->getClient();

        $logger = new \Monolog\Logger('app');
        $sentryHandler = new \Sentry\Monolog\Handler(new \Sentry\State\Hub($sentryClient));
        $logger->pushHandler($sentryHandler);

        Zend_Registry::set('logger', $logger);
    }
}

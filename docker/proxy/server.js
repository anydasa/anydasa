const express = require('express');
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
var url = require('url');


const app = express();

const waitSelectorList = {
    "www.citrus.ua": "#__layout"
}

app.get('/url/', async (req, res) => {
    const requestedUrl = req.query.url
    const requestedDomain = url.parse(requestedUrl, true).host
    const waitSelector = waitSelectorList[requestedDomain]

    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-dev-shm-usage',
            '--disable-accelerated-2d-canvas',
            '--disable-gpu',
            '--window-size=1920x1080',
            '--user-data-dir=/var/puppeter/'
        ]
    });

    const page = await browser.newPage();

    if (requestedDomain === 'hotline.ua') {
        await page.goto('https://hotline.ua/');
        const cookiesSet = await page.cookies('https://hotline.ua/');
        cookiesSet.push({
            name: 'language',
            value: 'ru',
        })

        await page.setCookie(...cookiesSet)
    }

    // await page.emulate(devices['iPhone 6']);
    // await page.emulate(devices['LG Optimus L70']);
    await page.emulate(devices['Microsoft Lumia 550']);

    await page.setRequestInterception(true);
    page.on('request', request => {
        if (!request.isNavigationRequest()) {
            request.continue();
            return;
        }
        const headers = request.headers();
        headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9';
        headers['Accept-Language'] = 'en-US,en;q=0.9,ru;q=0.8,uk;q=0.7';
        headers['Connection'] = 'keep-alive';
        request.continue({ headers });
    });

    await page.goto(requestedUrl);
    if (waitSelector !== undefined) {
        await page.waitForSelector(waitSelector);
    }

    // console.log(await page.cookies())

    const html = await page.content();
    browser.close();

    return res.end(html);
});

app.listen(8080);
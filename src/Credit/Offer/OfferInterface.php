<?php

namespace Credit\Offer;


interface OfferInterface
{
    public function __construct(array $params = array());

    public function getMinFirstPayment($sum, $params = array());

    public function getFirstPayment($sum, $monthCount, array $params = array());

    public function getAvailableTerms();
}
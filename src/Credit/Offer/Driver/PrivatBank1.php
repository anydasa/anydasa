<?php

namespace Credit\Offer\Driver;

use \Credit\Offer\OfferAbstract;

class PrivatBank1 extends OfferAbstract
{
    protected $minSum     = 300;
    protected $maxSum     = 300000;
    protected $minMonth   = 2;
    protected $maxMonth   = 24;
    protected $commission = 2.9;


    public function __construct(array $params = array())
    {
        $this->terms = array_combine(
            range($this->minMonth, $this->maxMonth),
            range($this->minMonth, $this->maxMonth)
        );
    }

    public function getFirstPayment($sum, $monthCount, array $params = array())
    {
        if ($sum > $this->maxSum) {
            $this->addError('sumIsGreaterThan');
        }

        if ($sum < $this->minSum) {
            $this->addError('sumIsLessThan');
        }

        if (!array_key_exists($monthCount, $this->terms)) {
            $this->addError('termNotAllow');
        }

        if ($this->hasErrors()) {
            return false;
        }

        return ($sum * $this->commission / 100) + ($sum / $monthCount);
    }

}

<?php

namespace Credit\Offer;


abstract class OfferAbstract implements OfferInterface
{
    protected $errors = array();
    protected $terms = array();

    protected $errorsTemplate = array(
        'sumIsGreaterThan' => '',
        'sumIsLessThan'    => '',
        'termNotAllow'     => '',
    );

    public function __construct(array $params = array())
    {
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function hasErrors()
    {
        return !empty($this->errors);
    }

    protected function clearErrors()
    {
        $this->errors = array();
    }

    protected function addError($code, array $params = array())
    {
        $this->errors[$code] = vsprintf(
            $this->errorsTemplate[$code],
            $params
        );
    }

    public function getMinFirstPayment($sum, $params = array())
    {
        $min = false;

        foreach ($this->terms as $month => $term) {
            if ($firstPayment = $this->getFirstPayment($sum, $month, $params)) {
                if (!$min || $min > $firstPayment) {
                    $min = $firstPayment;
                }
            }
        }

        return $min;
    }

    public function getAvailableTerms()
    {
        return $this->terms;
    }
}

<?php

namespace Credit\Offer;


class OfferFactory
{
    /**
     * @param string $name
     * @param array $params
     * @return OfferInterface
     * @throws \Exception
     */
    public static function getDriver($name, array $params = array())
    {
        $driversNamespace = '\Credit\Offer\Driver';
        $driverClass = $driversNamespace.'\\'.$name;

        if(!class_exists($driverClass)) {
            throw new \Exception(sprintf('Driver class %s not found.', $driverClass));
        }

        return new $driverClass($params);
    }
}
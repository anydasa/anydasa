<?php

namespace Order\Delivery;


class DeliveryCourier extends DeliveryHandler {


    public function getCostForProduct(\Products $Product)
    {
        $product_price = $Product->getPrice('retail', 'UAH');
        $sum = 0;

        if ( !is_null($this->Cart) ) {
            $sum += $this->Cart->getSum();

            if ( $this->Cart->issetProduct($Product->id) ) {
                $product_price = 0;
            }
        }

        $sum += $product_price;

        return $sum > $this->DeliveryRecord->free_from_price ? 0 : $this->DeliveryRecord->cost;
    }

    public function getCost()
    {
        $sum = 0;

        if ( !is_null($this->Cart) ) {
            $sum += $this->Cart->getSum();
        }

        return $sum > $this->DeliveryRecord->free_from_price ? 0 : $this->DeliveryRecord->cost;
    }
}
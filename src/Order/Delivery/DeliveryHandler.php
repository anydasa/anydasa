<?php

namespace Order\Delivery;


abstract class DeliveryHandler implements IDelivery {

    /**
     * @var \OrdersDelivery
     */
    protected $DeliveryRecord;

    /**
     * @var \Cart
     */
    protected $Cart = null;

    /**
     * @var \Users
     */
    protected $Client = null;

    public function __construct(\OrdersDelivery $DeliveryRecord)
    {
        $this->DeliveryRecord = $DeliveryRecord;
    }

    /**
     * @param \Cart $Cart
     * @return $this
     */
    public function setCart(\Cart $Cart)
    {
        $this->Cart = $Cart;
        return $this;
    }

    /**
     * @param \Users $Client
     * @return $this
     */
    public function setClient(\Users $Client)
    {
        $this->Client = $Client;
        return $this;
    }

    /**
     * @param \OrdersDelivery $DeliveryRecord
     * @return IDelivery
     * @throws \Exception
     */
    public static function factory(\OrdersDelivery $DeliveryRecord)
    {
        $class = 'Order\Delivery\Delivery' . \SF::mb_ucfirst($DeliveryRecord->id);

        if ( @class_exists($class) ) {
            return new $class($DeliveryRecord);
        }

        return new DeliveryDefault($DeliveryRecord);
    }

    /**
     * @param \Products $Product
     * @return bool
     */
    public function getCostForProduct(\Products $Product)
    {
        return false;
    }

    /**
     * @return bool
     */
    public function getCost()
    {
        return false;
    }

}
<?php

namespace Order\Delivery;


interface IDelivery {

    public function __construct(\OrdersDelivery $DeliveryRecord);

    public function getCostForProduct(\Products $Product);

}
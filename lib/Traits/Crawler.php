<?


trait Crawler {

    protected static function getClassByUrl($url)
    {
        $class = 'Products_Links_Domain_'.
            implode('',
                array_map(
                    function($word) { return ucfirst($word); },
                    explode('.', preg_replace('/^www\./', '', parse_url($url)['host']))
                )
            );
        return @class_exists($class) ? $class : false;
    }

    public function getHtml($url, $cache_lifetime = 0, $use_proxy = false, $options = [])
    {
        $cacheOptions = Zend_Registry::getInstance()->config->cache->toArray();
        $cacheOptions['frontend']['caching'] = $cache_lifetime > 0;
        $cacheOptions['frontend']['lifetime'] = $cache_lifetime;

        $cache = Zend_Cache::factory('Core', $cacheOptions['core'], $cacheOptions['frontend'], $cacheOptions['backend']);

        $key_cache = md5($url);

        if ( !$return = $cache->load($key_cache) ) {
            $return = $this->getByGuzzle($url);
//            $return = $this->getByProxy($url);
            $cache->save($return, $key_cache);
        }

        return $return;
    }

    private function getByProxy($url)
    {
        return file_get_contents('http://proxy:8080/url/?url='.$url);
    }

    private function getByGuzzle($url)
    {
        $client = new \GuzzleHttp\Client();

        $response = $client->get($url);

        return $response->getBody()->getContents();
    }

}
<?php

/*
 *    author:		Kyle Gadd
 *    documentation:	http://www.php-ease.com/classes/sitemap.html
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class Sitemap_Index
{

    private $filename   = 'index';

    public function __construct ($base_url, $base_path, $compress = true)
    {
        $this->compress  = ($compress) ? '.gz' : '';
        $this->base_url  = $base_url;
        $this->base_path = $base_path;
    }

    private function index ($file)
    {
        $sitemaps = array();
        $index = "sitemap.xml{$this->compress}";

        if (file_exists($this->base_path . $index)) {

            $xml  = (!empty($this->compress)) ? gzfile($this->base_path . $index) : file($this->base_path . $index);
            $tags = $this->xml_tag(implode('', $xml), array('sitemap'));

            foreach ($tags as $xml) {
                $loc = str_replace($this->base_url, '', $this->xml_tag($xml, 'loc'));
                $lastmod = $this->xml_tag($xml, 'lastmod');
                $lastmod = ($lastmod) ? date('c', strtotime($lastmod)) : date('c');
                if (file_exists($this->base_path . $loc)) $sitemaps[$loc] = $lastmod;
            }
        }

        $sitemaps[$file] = date('c');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
        $xml .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

        foreach ($sitemaps as $loc => $lastmod) {
            $xml .= '  <sitemap>' . "\n";
            $xml .= '    <loc>' . $this->base_url . $loc . '</loc>' . "\n";
            $xml .= '    <lastmod>' . $lastmod . '</lastmod>' . "\n";
            $xml .= '  </sitemap>' . "\n";
        }

        $xml .= '</sitemapindex>' . "\n";
        if (!empty($this->compress)) $xml = gzencode($xml, 9);

        $fp = fopen($this->base_path . $index, 'wb');
        fwrite($fp, $xml);
        fclose($fp);
    }

    private function xml_tag ($xml, $tag, &$end='')
    {
        if (is_array($tag)) {
            $tags = array();
            while ( $value = $this->xml_tag($xml, $tag[0], $end) ) {
                $tags[] = $value;
                $xml    = substr($xml, $end);
            }
            return $tags;
        }
        $pos = strpos($xml, "<{$tag}>");

        if ($pos === false) return false;

        $start  = strpos($xml, '>', $pos) + 1;
        $length = strpos($xml, "</{$tag}>", $start) - $start;
        $end    = strpos($xml, '>', $start + $length) + 1;

        return ($end !== false) ? substr($xml, $start, $length) : false;
    }



}

?>
<?


class Turbosms
{
    private $Client;

    public function __construct($login, $password, $url = 'http://turbosms.in.ua/api/wsdl.html')
    {
        $this->Client = new SoapClient($url);

        $this->Client->Auth([
            'login'    => $login,
            'password' => $password
        ]);
    }

    public function send($sender, $destination, $message)
    {
        $result = $this->Client->SendSMS([
            'sender'        => $sender,
            'destination'   => $destination,
            'text'          => $message
        ]);

        return $result->SendSMSResult->ResultArray;
    }


    public function status($id)
    {
        return $this->Client->GetMessageStatus(['MessageId' => $id])->GetMessageStatusResult;
    }


    public function balance()
    {
        return $this->Client->GetCreditBalance()->GetCreditBalanceResult;
    }
}

<?
class Site_PriceImport
{
    private static $columns = array(
        'code'              => 'Код',
        'manufacturer_code' => 'Код производителя',
        'title'             => 'Название',
        'comment'           => 'Комментарий',
        'brand'             => 'Бренд',
        'balance'           => 'Баланс',

        'currency'          => 'Валюта',
        'currency_buy'      => 'Валюта закупка',
        'currency_retail'   => 'Валюта РРЦ',

        'price_buy'         => 'Цена закупка',
        'price_retail'      => 'Цена РРЦ',

        'description'       => 'Описание',
        'warranty'          => 'Гарантия',
        'status'            => 'Статус',
        'group'             => 'Группа',
        'type'              => 'Тип',
        'url'               => 'www адрес',
        'state'             => 'Состояние',
        'temp'              => 'Служебное',
    );

    private static $conditions = array(
        'empty'             => 'пустое',
        'numeric'           => 'число',
        'not-numeric'       => 'не число',
        'contain'           => 'содержит',
        'not-contain'       => 'не содержит',
        'equal'             => 'равно',
        'not-equal'         => 'не равно',
        'more'              => 'больше',
        'more-or-equal'     => 'больше или равно',
        'less'              => 'меньше',
        'less-or-equal'     => 'меньше или равно',
        'background-color'  => 'цвет заливки',
        'color'             => 'цвет теста'
    );

    public $Excel;
    public $Vendor;

    public function __construct(PHPExcel $Excel, Vendors $Vendor)
    {
        $this->Excel  = $Excel;
        $this->Vendor = $Vendor;
    }

    public static function getColumns()
    {
        return self::$columns;
    }
    public static function getColumn($name)
    {
        if ( !isset(self::$columns[$name]) ) {
            throw new Exception("Нет колонки $name");
        }
        return self::$columns[$name];
    }
    public static function getCondtiotions()
    {
        return self::$conditions;
    }

    public function getConfig()
    {
        return new Site_PriceImport_Config($this);
    }

    public function getSeparator()
    {
        return new Site_PriceImport_Separator($this);
    }

    public static function getCellValue($cell, $style)
    {
        $returnValue = null;

        if ($cell->getValue() !== null) {
            if ($cell->getValue() instanceof PHPExcel_RichText) {
                $returnValue = $cell->getValue()->getPlainText();
            } else {
                $returnValue = $cell->getCalculatedValue();
            }

            $returnValue = PHPExcel_Style_NumberFormat::toFormattedString(
                $returnValue,
                ($style && $style->getNumberFormat()) ?
                    $style->getNumberFormat()->getFormatCode() :
                    PHPExcel_Style_NumberFormat::FORMAT_GENERAL
            );
        }

        return $returnValue;
    }

    public static function getCellBackgroundColor($style)
    {
        $color = 'FFFFFF';

        if ( $style->getFill()->getFillType() != PHPExcel_Style_Fill::FILL_NONE ) {
            $color = $style->getFill()->getStartColor()->getRGB();
        }

        return $color;
    }

}
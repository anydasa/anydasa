<?
class Site_Mail extends Zend_Mail
{
    public function send($transport = null)
    {
        if ( IS_DEV ) {
            file_put_contents(ROOT_PATH . 'mail-'.implode(',', $this->getRecipients()).'.html', '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >'.$this->getBodyHtml()->getRawContent());
        } else {
            $this->addBcc('all-tech@anydasa.com');
            return $this;
            parent::send($transport);
        }
        return $this;
    }

	/*public function setBodyHtml($html, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
	{
        if ( preg_match_all('~["src|background"]="(.*?)"~', $html, $t_images) ) {
            foreach ($t_images[1] as $item) {
                if ( $imgsize = @getimagesize($item) ) {

                    $it['src'] = $item;
                    $it['binary'] = file_get_contents($it['src']);
                    $it['name'] = @basename($item);

                    $html = str_replace($it['src'], 'cid:'.md5($item), $html);

                    $mp = new Zend_Mime_Part($it['binary']);
                    $mp->encoding = Zend_Mime::ENCODING_BASE64;
                    $mp->type = $imgsize['mime'];
                    $mp->disposition = Zend_Mime::DISPOSITION_INLINE;
                    $mp->filename = $it['name'];
                    $mp->id = md5($item);

                    $this->addAttachment($mp);
                }
            }
        }
		parent::setBodyHtml($html, $charset, $encoding);
	}*/

	protected function _encodeHeader($value)
    {
		if (Zend_Mime::isPrintable($value)) {
			return $value;
		} else {
			return '=?'.$this->_charset.'?B?'.base64_encode($value).'?=';
		}
    }
}
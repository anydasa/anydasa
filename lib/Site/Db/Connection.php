<?

require_once 'Site/Log.php';
require_once 'Zend/Log.php';

class Site_Db_Connection extends PDO
{

    /**
     * Строка параметров для подключения.
     * Params string.
     *
     * @var string
     */
    private $dsn = null;
    private $username = null;
    private $password = null;
    /**
     * Параметры соединения.
     * DB options.
     *
     * @var array
     */
    private $options = null;

    /**
     * Конструктор класса, получает параметры соединения.
     * Class constructur, get prepared config.
     *
     * @param array $config
     */
    function __construct($dsn, $username=null, $password = null, $options = null)
    {
        $start_time = microtime(true);
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
        $this->options = $options;
        try {
            parent::__construct($this->dsn, $this->username, $this->password, $this->options);
        } catch (PDOException $e) {
            Site_Log::add($e->getMessage(), Zend_Log::ERR);
            require_once 'Site/Db/Exception.php';
            throw new Site_Db_Exception($e->getMessage());
        }
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $connection_time = microtime(true) - $start_time;
        Site_Log::add('Connection created, connection time - ' . $connection_time, Zend_Log::DEBUG);
    }

}
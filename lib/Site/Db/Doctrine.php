<?

class Site_Db_Doctrine
{

    static public function init(Site_Db_Connection $connection, $models_path = null)
    {
        Doctrine_Manager::getInstance()->setAttribute('model_loading', 'conservative');
        Doctrine_Manager::getInstance()->setAttribute('autoload_table_classes', true);
        Doctrine_Manager::getInstance()->setAttribute(Doctrine_Core::ATTR_AUTO_ACCESSOR_OVERRIDE, true);

        Doctrine_Manager::connection($connection);

        if (!is_null($models_path)) {
            Doctrine::loadModels($models_path);
        }
    }
}

<?

define ('UTF32_BIG_ENDIAN_BOM'   , chr(0x00) . chr(0x00) . chr(0xFE) . chr(0xFF));
define ('UTF32_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE) . chr(0x00) . chr(0x00));
define ('UTF16_BIG_ENDIAN_BOM'   , chr(0xFE) . chr(0xFF));
define ('UTF16_LITTLE_ENDIAN_BOM', chr(0xFF) . chr(0xFE));
define ('UTF8_BOM'               , chr(0xEF) . chr(0xBB) . chr(0xBF));

class Site_Encoding_Master
{
    public function detectFile($filename)
    {
        $text = file_get_contents($filename);
        return $this->detectStr($text);
    }

    public function detectStr($text)
    {
        if ( $encode = $this->detectUTF($text) ) {
            return $encode;
        }

        $weights = array();
        $specters = array();

        foreach (array('cp1251', 'iso8859-5', 'koi8r') as $encoding) {
            $specters[$encoding] = require dirname(__FILE__). '/specters/'.$encoding.'.php';
        }

        for ($i = 0; $i < strlen($text); ++$i) {
            foreach ($specters as $encoding => $char_specter) {
                $weights[$encoding] += $char_specter[ord($text[$i])];
            }
        }
        arsort($weights);
        return array_keys($weights)[0];
    }


    public function detectUTF($text)
    {
        $first2 = substr($text, 0, 2);
        $first3 = substr($text, 0, 3);
        $first4 = substr($text, 0, 3);

        if ($first3 == UTF8_BOM || preg_match('#.#u', $text) ) return 'UTF-8';
        elseif ($first4 == UTF32_BIG_ENDIAN_BOM) return 'UTF-32BE';
        elseif ($first4 == UTF32_LITTLE_ENDIAN_BOM) return 'UTF-32LE';
        elseif ($first2 == UTF16_BIG_ENDIAN_BOM) return 'UTF-16BE';
        elseif ($first2 == UTF16_LITTLE_ENDIAN_BOM) return 'UTF-16LE';

        return false;
    }


}
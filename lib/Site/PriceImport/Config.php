<?

class Site_PriceImport_Config
{
    private $Vendor;
    private $activeSheet;

    private $config;

    public function __construct(Site_PriceImport $PriceImport)
    {
        $this->Vendor = $PriceImport->Vendor;
        $this->activeSheet = $PriceImport->Excel->getActiveSheetIndex();
        $this->unserializeConfig();

    }

    public function setActiveSheet($sheet)
    {
        $this->activeSheet = $sheet;
        return $this;
    }

    public function issetConfigSheet($index)
    {
        return !empty($this->config['sheets'][$index]);
    }

    public function getHeaderCountRows()
    {
        $count = $this->config['sheets'][$this->activeSheet]['header_count_rows'];
        return $count ? $count : 0;
    }

    public function setHeaderCountRows($count = 0)
    {
        if ( empty($count) ) {
            unset($this->config['sheets'][$this->activeSheet]['header_count_rows']);
        } else {
            $this->config['sheets'][$this->activeSheet]['header_count_rows'] = $count;
        }
        return $this;
    }

    public function getColumns()
    {
        $columns = $this->config['sheets'][$this->activeSheet]['columns'];
        return empty($columns) ? array() : $columns;
    }

    public function setColumns($config)
    {
        $this->config['sheets'][$this->activeSheet]['columns'] = array_filter($config);
        return $this;
    }

    public function setDefaultMacros($config)
    {
        $this->config['defaultMacrosList'] = array_filter($config);
        return $this;
    }

    public function getMacros()
    {
        $macros = $this->config['sheets'][$this->activeSheet]['macros'];
        return empty($macros) ? array() : $macros;
    }

    public function getPresetsMacros()
    {
        return json_decode('{"sheets":[{"columns":{"B":"title","C":"manufacturer_code","D":"price_buy","F":"price_retail","G":"temp"},"macros":[{"action":"skip","field":"temp","condition":"not-contain","value":"\u0448\u0442"},{"action":"default","field":"currency","value":"UAH"},{"action":"default","field":"currency_buy","value":"USD"},{"action":"default","field":"price_retail","value":"USD"}]}]}', true);
    }

    public function getDefaultMacros()
    {
        $macros = $this->config['defaultMacrosList'];
        return empty($macros) ? array() : $macros;
    }

    public function setMacros($config)
    {
        $this->config['sheets'][$this->activeSheet]['macros'] = $config;
        return $this;
    }

    public function save()
    {
        $this->serializeConfig();
        $this->Vendor->save();
        $this->Vendor->refresh();
        $this->unserializeConfig();
    }

    private function unserializeConfig()
    {
        $this->config = json_decode($this->Vendor->price_config, true);
    }

    private function serializeConfig()
    {
        foreach($this->config['sheets'] as $key=>$sheet) {
            if ( !array_filter($sheet) ) {
                unset($this->config['sheets'][$key]);
            }
        }
        $this->Vendor->price_config = $this->config ? json_encode($this->config) : null;
    }

    public function getVendor()
    {
        return $this->Vendor;
    }

}
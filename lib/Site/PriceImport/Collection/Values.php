<?

class Site_PriceImport_Collection_Values
{
    private $_data = array();
    private $Vendor;

    private $_inserted = array();
    private $_updated  = array();
    private $_notsaved = array();

    function __construct(Vendors $Vendor)
    {
        $this->Vendor = $Vendor;
    }

    public function add($data)
    {
        $this->_data []= $data;
    }

    public function getCount()
    {
        return count($this->_data);
    }

    public function getColumns()
    {
        $returnData = array();
        foreach($this->_data as $row) {
            foreach ($row as $key=>$cell) {
                $returnData[$key] = null;
            }
        }

        return array_intersect_key(Site_PriceImport::getColumns(), $returnData);
    }

    public function getDataMatrix()
    {
        $columns = array_fill_keys(array_keys($this->getColumns()), null);
        $returnData = array();

        foreach($this->_data as $key=>$row) {
            $returnData[$key]= array_merge($columns, array_intersect_key($row, $columns));
        }

        return $returnData;
    }

    public function save()
    {

        foreach ($this->_data as $key=>$val) {
            $val['id_vendor']       = $this->Vendor->id;
            $val['time_processed']  = new Doctrine_Expression('NOW()');
            $val['is_actual']       = 1;

            $Product = Doctrine_Query::create()->from('VendorsProducts')->where('code = ? AND id_vendor = ?', array($val['code'], $this->Vendor->id))->fetchOne();
            $isUpdate = true;

            if ( !$Product ) {
                $Product = new VendorsProducts;
                $isUpdate = false;
            }

            $Product->fromArray($val);

            if ( $Product->trySave() ) {
                if ( $isUpdate ) {
                    $this->_updated[$key]  = $key;
                } else {
                    $this->_inserted[$key] = $key;
                }
            } else {
                $this->_notsaved[$key]     = $key;
            }
        }

//        Doctrine_Manager::connection()->prepare('SELECT vendors_products_toggle_actual()')->execute();
    }

    public function getNotSaved()
    {
        return array_intersect_key($this->_data, $this->_notsaved);
    }
    public function getInserted()
    {
        return array_intersect_key($this->_data, $this->_inserted);
    }
    public function getUpdated()
    {
        return array_intersect_key($this->_data, $this->_updated);
    }
    public function getSaved()
    {
        return array_diff_key($this->_data, $this->_notsaved);
    }

}
<?

class Site_PriceImport_Collection_Garbage
{
    private $_data = array();
    private $_causeSkipping = array();

    public function add($data, $causeSkipping)
    {
        $newkey = count($this->_data);

        $this->_data[$newkey]= $data;
        $this->_causeSkipping[$newkey]= $causeSkipping;
    }
    public function getCount()
    {
        return count($this->_data);
    }
    public function getColumns()
    {
        $returnData = array();
        foreach($this->_data as $row) {
            foreach ($row as $key=>$cell) {
                $returnData[$key] = null;
            }
        }

        return array_intersect_key(Site_PriceImport::getColumns(), $returnData);
    }
    public function getData()
    {
        $columns = array_fill_keys(array_keys($this->getColumns()), null);
        $returnData = array();

        foreach($this->_data as $key=>$row) {
            $returnData[$key]= array_merge($columns, array_intersect_key($row, $columns));
        }

        return $returnData;
    }
    public function getCauseSkippingByKey($key)
    {
        return $this->_causeSkipping[$key];
    }
}
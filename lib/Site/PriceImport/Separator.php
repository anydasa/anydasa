<?

class Site_PriceImport_Separator
{
    private $PriceImport;

    private $CollectionValues;
    private $CollectionGarbage;

    function __construct(Site_PriceImport $PriceImport)
    {
        $this->PriceImport = $PriceImport;

        $this->CollectionValues  = new Site_PriceImport_Collection_Values($PriceImport->Vendor);
        $this->CollectionGarbage = new Site_PriceImport_Collection_Garbage;
    }

    public function separateAllSheets()
    {
        foreach($this->PriceImport->Excel->getSheetNames() as $idx => $sheetName) {
            $this->PriceImport->Excel->setActiveSheetIndex($idx);
            $this->separate();
        }
    }

    public function separate()
    {
        $Sheet  = $this->PriceImport->Excel->getActiveSheet();
        $Config = $this->PriceImport->getConfig();

        $list   = $Sheet->toArray(null, true, true, true);

        foreach ($list as $row_key=>$row) {
            $temp = $this->normaliseRow($row);

            $rowIsValid = !empty($Config->getColumns());
            $causeSkipping = 'Не указаны колонки';

            if ( $Config->getHeaderCountRows() >= $row_key ) {
                $rowIsValid = false;
                $causeSkipping = 'Шапка';
            }

            if ( $rowIsValid && !empty($Config->getMacros()) ) {
                foreach ($Config->getMacros() as $macro_key=>$macro) {
                    if ( 'trim' == $macro['action'] ) {
                        $temp[$macro['field']] = str_replace($macro['value'], '', $temp[$macro['field']]);
                    }
                    if ( 'replace' == $macro['action'] ) {
                        $temp[$macro['field']] = str_replace($macro['value'], $macro['valueto'], $temp[$macro['field']]);
                    }
                    if ( 'skip' == $macro['action'] && $this->checkMacroCondition($macro, $temp, $row_key) ) {
                        $rowIsValid = false;
                        $causeSkipping = 'Сработал макрос №'.$macro_key;
                        break;
                    }
                    if ( 'condition' == $macro['action'] && $this->checkMacroCondition($macro, $temp, $row_key) ) {
                        $temp[$macro['fieldto']] = $macro['valueto'];
                    }
                    if ( 'default' == $macro['action'] && empty($temp[$macro['field']]) ) {
                        $temp[$macro['field']] = $macro['value'];
                    }
                    if ( 'concat' == $macro['action'] && !empty($temp[$macro['field2']]) ) {
                        if ( 'begin' == $macro['position'] ) {
                            $temp[$macro['field']] = $macro['value'] . $temp[$macro['field2']] . $macro['value2'] . $temp[$macro['field']];
                        } elseif ( 'end' == $macro['position'] ) {
                            $temp[$macro['field']] = $temp[$macro['field']] . $macro['value'] . $temp[$macro['field2']] . $macro['value2'];
                        }
                    }
                }
            }

            $temp['price_buy']    = round(SF::tofloat($temp['price_buy']), 2);
            $temp['price_retail'] = round(SF::tofloat($temp['price_retail']), 2);

            if ( $rowIsValid && empty($temp['title']) ) {
                $causeSkipping = 'Пустое ' . $this->PriceImport->getColumn('title');
                $rowIsValid = false;
            }
            if ( $rowIsValid && empty($temp['price_buy']) ) {
                $causeSkipping = $this->PriceImport->getColumn('price_buy') . ' не число или 0';
                $rowIsValid = false;
            }
            if ( $rowIsValid && isset($temp['balance']) && !is_numeric($temp['balance']) ) {
                $causeSkipping = $this->PriceImport->getColumn('balance') . ' не число ';
                $rowIsValid = false;
            }
            if ( $rowIsValid && empty($temp['currency']) ) {
                $causeSkipping = 'Не указана '.$this->PriceImport->getColumn('currency');
                $rowIsValid = false;
            }
            if ( $rowIsValid && !Currency::issetByISO($temp['currency']) ) {
                $causeSkipping = 'Неверное значение для поля '.$this->PriceImport->getColumn('currency');
                $rowIsValid = false;
            } elseif ($rowIsValid) {
                $temp['id_currency'] = Currency::getByISO($temp['currency'])->id;
            }
            if ( $rowIsValid && isset($temp['currency_buy']) && !Currency::issetByISO($temp['currency_buy']) ) {
                $causeSkipping = 'Неверное значение для поля '.$this->PriceImport->getColumn('currency_buy');
                $rowIsValid = false;
            }
            if ( $rowIsValid && isset($temp['currency_retail']) && !Currency::issetByISO($temp['currency_retail']) ) {
                $causeSkipping = 'Неверное значение для поля '.$this->PriceImport->getColumn('currency_retail');
                $rowIsValid = false;
            }

            if ( $rowIsValid && isset($temp['currency_buy']) && $temp['currency_buy'] != $temp['currency'] ) {
                $temp['price_buy'] = round($temp['price_buy'] / Currency::getByISO($temp['currency_buy'])->rate * Currency::getByISO($temp['currency'])->rate, 2);
            }
            if ( $rowIsValid && isset($temp['currency_retail']) && $temp['currency_retail'] != $temp['currency'] ) {
                $temp['price_retail'] = round($temp['price_retail'] / Currency::getByISO($temp['currency_retail'])->rate * Currency::getByISO($temp['currency'])->rate, 2);
            }

            if ( $rowIsValid ) {
                $this->CollectionValues->add($temp);
            } else {
                $this->CollectionGarbage->add($temp, $causeSkipping);
            }
        }
    }

    private function checkMacroCondition($macro, $row, $row_key)
    {
        if ( 'empty' == $macro['condition'] && empty($row[$macro['field']]) ) {
            return true;
        }
        if ( 'numeric' == $macro['condition'] && is_numeric($row[$macro['field']]) ) {
            return true;
        }
        if ( 'not-numeric' == $macro['condition'] && !is_numeric($row[$macro['field']]) ) {
            return true;
        }
        if ( 'contain' == $macro['condition'] && false !== mb_strripos($row[$macro['field']], $macro['value']) ) {
            return true;
        }
        if ( 'not-contain' == $macro['condition'] && false === mb_strripos($row[$macro['field']], $macro['value']) ) {
            return true;
        }
        if ( 'equal' == $macro['condition'] && $macro['value'] == $row[$macro['field']] ) {
            return true;
        }
        if ( 'not-equal' == $macro['condition'] && $macro['value'] != $row[$macro['field']] ) {
            return true;
        }
        if ( 'more' == $macro['condition'] && $macro['value'] > $row[$macro['field']] ) {
            return true;
        }
        if ( 'more-or-equal' == $macro['condition'] && $macro['value'] >= $row[$macro['field']] ) {
            return true;
        }
        if ( 'less' == $macro['condition'] && $macro['value'] < $row[$macro['field']] ) {
            return true;
        }
        if ( 'less-or-equal' == $macro['condition'] && $macro['value'] <= $row[$macro['field']] ) {
            return true;
        }
        if ( 'background-color' == $macro['condition'] ) {
            $coordinate = array_search($macro['field'], $this->PriceImport->getConfig()->getColumns()).$row_key;
            $backgroundColor = $this->PriceImport->Excel->getActiveSheet()->getStyle($coordinate)->getFill()->getStartColor()->getRGB();
            if ( $backgroundColor == mb_convert_case(trim($macro['value'], '#'), MB_CASE_UPPER) ) {
                return true;
            }

        }
        if ( 'color' == $macro['condition'] ) {
            $coordinate = array_search($macro['field'], $this->PriceImport->getConfig()->getColumns()).$row_key;
            $fontColor = $this->PriceImport->Excel->getActiveSheet()->getStyle($coordinate)->getFont()->getColor()->getRGB();
            if ( $fontColor == mb_convert_case(trim($macro['value'], '#'), MB_CASE_UPPER) ) {
                return true;
            }
        }

        return false;
    }

    private function normaliseRow($row)
    {
        $configColumns = $this->PriceImport->getConfig()->getColumns();

        $return = array();

        foreach ($row as $key=>$val) {
            if ( isset($configColumns[$key]) ) {
                $val = trim($val);
                $return [$configColumns[$key]]= $val;
            }
        }

        if ( empty($return['code']) ) {
            $return['code'] = hash('crc32', $return['title']);
        }

        return $return;
    }

    public function getValues()
    {
        return $this->CollectionValues;
    }

    public function getGarbage()
    {
        return $this->CollectionGarbage;
    }

}
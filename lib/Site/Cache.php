<?

class Site_Cache
{

    public static $cache = null;


    public static function init()
    {
        $frontendOptions = Zend_Registry::getInstance()->config->cache->frontend->toArray();
        $backendOptions  = Zend_Registry::getInstance()->config->cache->backend->toArray();

        self::$cache = Zend_Cache::factory('Core', Zend_Registry::getInstance()->config->cache->core, $frontendOptions, $backendOptions);
    }
    public static function save($data, $name, $params = array())
    {
        if ( null == self::$cache ) {
            self::init();
        }

        return self::$cache->save(
                        $data,
                        self::makeId($name, $params),
                        self::makeTagsFromParams($params)
        );
    }
    public static function clear()
    {
        if ( null == self::$cache ) {
            self::init();
        }

        return self::$cache->clean();
    }
    public static function load($name, $params = array())
    {
        if ( null == self::$cache ) {
            self::init();
        }

        return self::$cache->load(self::makeId($name, $params));
    }
    public static function makeTagsFromParams($params)
    {
        $result = array();
        foreach ($params as $key=>$value) {
            if (in_array((string)$key, array('controller', 'action', 'module'))) continue;
            $result[] = preg_replace('/[^a-zA-Z0-9_]/', '', $key.'_'.$value);
        }
        return $result;
    }
    public static function makeId($name, $params)
    {
        ksort($params);
        $str = $name.'_'.implode("_", self::makeTagsFromParams($params));
        return str_replace('-', '_', $str);
    }

}
<?

require_once 'Zend/Controller/Plugin/Abstract.php';
require_once 'Zend/Registry.php';

class Site_Controller_Plugin_RouteLocation extends Zend_Controller_Plugin_Abstract
{

    protected $_siteHost = null;
    protected $_defaultLocation = null;

    public function __construct($defaultLocation, $siteHost)
    {
        $this->_defaultLocation = $defaultLocation;
        $tHost = parse_url($siteHost);
        $this->_siteHost = $tHost['host'];
    }

    public function preDispatch($Request)
    {
        $currentLocation = preg_replace('/[\.]*' . $this->_siteHost . '/', '', $Request->getHttpHost());

        if (empty($currentLocation) || false === ($Location = Location::getBySubdomain($currentLocation))) {
            Zend_Controller_Front::getInstance()
                ->getResponse()
                ->setRedirect(Zend_Registry::get('config')->url->main, 302)
                ->sendHeaders();
        }

        Zend_Registry::set('Location', $Location);
    }

}
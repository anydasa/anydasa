<?

require_once 'Zend/Controller/Plugin/Abstract.php';
require_once 'Zend/Translate.php';
require_once 'Zend/Registry.php';
require_once 'Zend/Locale.php';

class Site_Controller_Plugin_RouteLang extends Zend_Controller_Plugin_Abstract
{

    protected $_defaultLanguage = null;
    protected $_allowedLanguages = null;
    protected $_currentLang = null;
    protected $_locales = null;
    protected $_langsPath = null;

    public function __construct($defaultLang, array $locales, $langsPath)
    {
        $allowedLanguages = array_keys($locales);

        if (!in_array($defaultLang, $allowedLanguages)) {
            require_once 'Zend/Config/Exception.php';
            throw new Zend_Config_Exception('Default language not exist in allowed langs.');
        }

        $this->_defaultLanguage = $defaultLang;
        $this->_allowedLanguages = $allowedLanguages;
        $this->_locales = $locales;
        $this->_langsPath = $langsPath;
    }

    public function routeStartup(Zend_Controller_Request_Abstract $Request)
    {
        $requestUri = explode('/', ltrim($Request->getRequestUri(), '/'));

        if (count($requestUri) &&
            in_array($requestUri[0], $this->_allowedLanguages) &&
            $requestUri[0] != $this->_defaultLanguage
        ) {
            $this->_currentLang = array_shift($requestUri);
            $requestUri = '/' . implode('/', $requestUri);
            $Request->setRequestUri($requestUri);
        } else {
            $this->_currentLang = $this->_defaultLanguage;
        }

        $this->_setLocale();
        $this->_setTranslate();

        require_once 'Zend/Controller/Front.php';
    }

    public function preDispatch(Zend_Controller_Request_Abstract $Request)
    {
        if ($this->_currentLang != $this->_defaultLanguage) {
            require_once 'Zend/Layout.php';
            Zend_Layout::getMvcInstance()->getView()->prefixLangUrl = '/' . $this->_currentLang;
        }
        Zend_Registry::set('lang', $this->_currentLang);
    }

    private function _setLocale()
    {
        if (!empty($this->_locales[$this->_currentLang])) {
            $locale = new Zend_Locale($this->_locales[$this->_currentLang]);

            Zend_Locale::$compatibilityMode = false;

            if (!Zend_Locale::isLocale($this->_locales[$this->_currentLang])) {
                require_once 'Zend/Locale/Exception.php';
                throw new Zend_Locale_Exception('Locale "' . $this->_locales[$this->_currentLang] . '" is unavailable.');
            }

            Zend_Registry::set('Zend_Locale', $locale);
        }
    }

    private function _setTranslate()
    {
        require_once 'Site/Log.php';

        $translate = new Zend_Translate(
                'array',
                $this->_langsPath,
                Zend_Registry::get('Zend_Locale'),
                array(
                    'scan' => Zend_Translate::LOCALE_DIRECTORY,
                    'log' => Site_Log::getInstance(),
                    'logUntranslated' => false
                )
        );
        Zend_Registry::set('Zend_Translate', $translate);
    }

}
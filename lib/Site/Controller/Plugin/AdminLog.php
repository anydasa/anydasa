<?

require_once 'Zend/Controller/Plugin/Abstract.php';

class Site_Controller_Plugin_AdminLog extends Zend_Controller_Plugin_Abstract
{

    public function dispatchLoopShutdown()
    {
        $user_name = Zend_Registry::isRegistered('User') ? Zend_Registry::get('User')->name : 'Anonymous';
        $title = Zend_Layout::getMvcInstance()->getView()->headTitle()->getValue();

        AdminLogs::log($user_name, $_SERVER['REQUEST_URI'], is_array($title) ? implode('', $title) : $title);
    }

}
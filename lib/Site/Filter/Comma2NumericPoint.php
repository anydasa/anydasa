<?php

class Site_Filter_Comma2NumericPoint implements Zend_Filter_Interface
{

    public function filter($value)
    {
        return preg_replace('/(\d),(\d)/', '$1.$2', $value);
    }
}

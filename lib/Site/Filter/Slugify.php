<?php

class Site_Filter_Slugify implements Zend_Filter_Interface
{

    public function filter($value)
    {
        return Site_Function::slugify($value);
    }
}

<?php

class Site_Filter_Md5 implements Zend_Filter_Interface
{

    public function filter($value)
    {
        if ( !empty($value) ) {
            return md5($value);
        }
        return $value;
    }
}

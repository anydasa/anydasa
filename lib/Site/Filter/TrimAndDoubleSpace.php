<?php

class Site_Filter_TrimAndDoubleSpace implements Zend_Filter_Interface
{

    public function filter($value)
    {
        return trim(
            preg_replace("/[\x20\x20]+/", "\x20", $value)
        );
    }
}

<?
class Site_Image
{
    protected $img_dir;
    protected $jpegqual = 100;

    public function setDir($img_dir)
    {
        if (! is_dir($img_dir) ) {
            throw new Site_Exception('Directory for image not exists.');
        }
        if (! is_readable($img_dir) ) {
            throw new Site_Exception('Directory for image not readable.');
        }
        if (! is_writable($img_dir) ) {
            throw new Site_Exception('Directory for image not writable.');
        }
        return $this->img_dir = rtrim($img_dir, '/') .'/';
    }

    public function resizeImageMaxSide($file_name, $max_width, $max_height)
    {
        if (! self::isImage($file_name)) {
            throw new Site_Exception('Can\'t resize because file is not image or file not readable.');
        }

        if (! is_writable($file_name) ) {
            throw new Site_Exception('Can\'t resize because image not writable.');
        }

        list($old_width, $old_height)   = getimagesize($file_name);

        $divisor_width  = $max_width / $old_width;
        $divisor_height = $max_height / $old_height;
        $divisor        = ($divisor_width > $divisor_height) ? $divisor_height : $divisor_width;

        $width  = round($old_width * $divisor);
        $height = round($old_height * $divisor);

        return $this->_resizeImage($file_name, $width, $height);
    }

    public function saveAs($file_name, $toMimeType, $delete_source = false)
    {
        if (! self::isImage($file_name)) {
            throw new Site_Exception('Can\'t saveAs "'.$file_name.'" because file is not image or file not readable.');
        }

        if (! is_writable($file_name) ) {
            throw new Site_Exception('Can\'t saveAs "'.$file_name.'" because image not writable.');
        }

        switch ( self::getImageMimeType($file_name) )
        {
            case 'image/jpeg':
                $im = imageCreateFromJpeg($file_name);
                break;
            case 'image/gif':
                $input = imageCreateFromGif($file_name);
                list($width, $height) = getimagesize($file_name);
                $output = imagecreatetruecolor($width, $height);
                $white = imagecolorallocate($output,  255, 255, 255);
                imagefilledrectangle($output, 0, 0, $width, $height, $white);
                imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
                imagejpeg($output, $file_name);
                $im = $output;
                break;
            case 'image/png':
                $input = imageCreateFromPng($file_name);
                list($width, $height) = getimagesize($file_name);
                $output = imagecreatetruecolor($width, $height);
                $white = imagecolorallocate($output,  255, 255, 255);
                imageColorTransparent($output, $white);
                imagefilledrectangle($output, 0, 0, $width, $height, $white);
                imagecopy($output, $input, 0, 0, 0, 0, $width, $height);
                imagejpeg($output, $file_name);
                $im = $output;
                break;
            default:
                return false;
                break;
        }

        $file_pathinfo = pathinfo($file_name);
        $file_name_without_ext = $file_pathinfo['dirname'].'/'.basename($file_pathinfo['basename'], '.'.$file_pathinfo['extension']) . '.';

        switch ($toMimeType)
        {
            case 'image/jpeg':
                $result = imageJpeg($im, $file_name_without_ext.'jpg', $this->jpegqual) ? $file_name_without_ext.'jpg' : false;
                break;
            case 'image/gif':
                $result = imageGif($im, $file_name_without_ext.'gif') ? $file_name_without_ext.'gif' : false;
                break;
            case 'image/png':
                $result = imagePng($im, $file_name_without_ext.'png') ? $file_name_without_ext.'png' : false;
                break;
            default:
                return false;
                break;
        }

        if (false !== $result && $delete_source && $result != $file_name) {
            unlink($file_name);
        }

        return $result;
    }


    private function _resizeImage($file_name, $newwidth, $newheight)
    {
        list($width, $height)   = getimagesize($file_name);

        switch ( self::getImageMimeType($file_name) )
        {
            case 'image/jpeg':
                $im = imageCreateTrueColor($newwidth, $newheight);
                $source = imageCreateFromJpeg($file_name);
                imageCopyResampled($im, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                $result = imageJpeg($im, $file_name, $this->jpegqual) ? true : false;
                break;
            case 'image/gif':
                $im = imageCreate($newwidth, $newheight);
                $source = imageCreateFromGif($file_name);
                $color = imageColorallocate($im, 0,0,0);
                imageColorTransparent($im, $color);
                imageCopyResampled($im, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
                $result = imageGif($im, $file_name) ? true : false;
                break;
            case 'image/png':
                $im = imageCreate($newwidth, $newheight);
                imagesavealpha($im, true);
                imagealphablending($im, true);
                $source = imageCreateFromPng($file_name);
                $color = imageColorallocate($im, 0,0,0);
                imageColorTransparent($im, $color);
                imageCopyResampled($im, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                $result = imagePng($im, $file_name, 9, PNG_ALL_FILTERS) ? true : false;
                break;
            default:
                $result = false;
                break;
        }
        imageDestroy($source);

        return $result;
    }

    private static function getNewSizesByMaxSide($imgpath, $max_width, $max_height)
    {
        list($old_width, $old_height)   = getimagesize($imgpath);

        $divisor_width  = $max_width / $old_width;
        $divisor_height = $max_height / $old_height;
        $divisor        = ($divisor_width > $divisor_height) ? $divisor_height : $divisor_width;

        return [
            round($old_width * $divisor),
            round($old_height * $divisor)
        ];
    }

    public function intoRectangle($imgpath, $b_width, $b_height, $b_color, $padding = 10)
    {
        if (! self::isImage($imgpath)) {
            throw new Site_Exception('Can\'t marge because File 1 is not image.');
        }

        $rgb = self::HexToRGB($b_color);

        list($src_width, $src_height) = self::getNewSizesByMaxSide($imgpath, $b_width - $padding, $b_height - $padding);

        switch ( $this->getImageMimeType($imgpath) )
        {
            case 'image/jpeg':
                $im = imageCreateFromJpeg($imgpath);
                $bg = imageCreateTrueColor($b_width, $b_height);
                imagefill($bg, 0, 0, imagecolorallocate($bg, $rgb['r'], $rgb['g'], $rgb['b']));

                imageCopyResampled(
                    $bg,
                    $im,
                    ($b_width  - $src_width) / 2,
                    ($b_height - $src_height) / 2,
                    0,
                    0,
                    ($src_width),
                    ($src_height),
                    imagesx($im),
                    imagesy($im)
                );

                $result = imageJpeg($bg, $imgpath, $this->jpegqual);
                break;
            case 'image/gif':
                throw new Site_Exception('Not implemented.');
                break;
            case 'image/png':
                throw new Site_Exception('Not implemented.');
                break;
        }

        return (bool) $result;
    }

    public function margeImages($img1, $img2, $img2position, $amount = 100)
    {
        if (! self::isImage($img1)) {
            throw new Site_Exception('Can\'t marge because File 1 is not image.');
        }
        if (! self::isImage($img2)) {
            throw new Site_Exception('Can\'t marge because File 2 is not image.');
        }

        try {
            switch ( $this->getImageMimeType($img1) )
            {
                case 'image/jpeg':
                    $dst_im = imageCreateFromJpeg($img1);
                    break;
                case 'image/gif':
                    $dst_im = imageCreateFromGif($img1);
                    break;
                case 'image/png':
                    $dst_im = imageCreateFromPng($img1);
                    break;
            }

            $img2_size = getimagesize($img2);

            switch ( $this->getImageMimeType($img2) )
            {
                case 'image/jpeg':
                    $src_im = imageCreateFromJpeg($img2);
                    imageCopyMerge($dst_im, $src_im, $img2position['x'], $img2position['y'], 0, 0, $img2_size[0], $img2_size[1], $amount);
                    break;
                case 'image/gif':
                    $src_im = imageCreateFromGif($img2);
                    imageCopyMerge($dst_im, $src_im, $img2position['x'], $img2position['y'], 0, 0, $img2_size[0], $img2_size[1], $amount);
                    break;
                case 'image/png':
                    $src_im = imageCreateFromPng($img2);
                    ImageAlphaBlending($dst_im, true);
                    imageCopy($dst_im, $src_im, $img2position['x'], $img2position['y'], 0, 0, $img2_size[0], $img2_size[1]);
                    break;
            }

            $file_pathinfo = pathinfo($img1);
            $new_file_name = $file_pathinfo['dirname'].'/'.basename($file_pathinfo['basename'], '.'.$file_pathinfo['extension']) . '.'.$file_pathinfo['extension'];

            switch ( $this->getImageMimeType($img1) )
            {
                case 'image/jpeg':
                    $result = imageJpeg($dst_im, $new_file_name, $this->jpegqual) ? $new_file_name : false;
                    break;
                case 'image/gif':
                    $result = imageGif($dst_im, $new_file_name) ? $new_file_name : false;
                    break;
                case 'image/png':
                    $result = imageGif($dst_im, $new_file_name) ? $new_file_name : false;
                    break;
            }

            imageDestroy($dst_im);
            imageDestroy($src_im);

        } catch (exception $e) {
            echo 'as';
        }

        return $result ? $new_file_name = end(explode('/', $new_file_name)) : false;
    }

    static public function getImageMimeType($img_path)
    {
        if (! self::isImage($img_path)) {
            return false;
        }
        $size = getimagesize($img_path);
        return $size['mime'];
    }

    static public function getWH($img_path) {
        if (self::isImage($img_path)) {
            $img = getimagesize($img_path);
            return $img[3];
        }
        return '';
    }
    static public function isImage($img_path)
    {
        return is_file($img_path) && is_readable($img_path) && getimagesize($img_path) ? true : false;
    }

    private function _imagettftextblur(&$image,$size,$angle,$x,$y,$color,$fontfile,$text,$blur_intensity = null)
    {
        $blur_intensity = !is_null($blur_intensity) && is_numeric($blur_intensity) ? (int)$blur_intensity : 0;
        if ($blur_intensity > 0)
        {
            $text_shadow_image = imagecreatetruecolor(imagesx($image),imagesy($image));
            imagefill($text_shadow_image,0,0,imagecolorallocate($text_shadow_image,0x00,0x00,0x00));
            imagettftext($text_shadow_image,$size,$angle,$x,$y,imagecolorallocate($text_shadow_image,0xFF,0xFF,0xFF),$fontfile,$text);
            for ($blur = 1;$blur <= $blur_intensity;$blur++)
                imagefilter($text_shadow_image,IMG_FILTER_GAUSSIAN_BLUR);
            for ($x_offset = 0;$x_offset < imagesx($text_shadow_image);$x_offset++)
            {
                for ($y_offset = 0;$y_offset < imagesy($text_shadow_image);$y_offset++)
                {
                    $visibility = (imagecolorat($text_shadow_image,$x_offset,$y_offset) & 0xFF) / 255;
                    if ($visibility > 0)
                        imagesetpixel($image,$x_offset,$y_offset,imagecolorallocatealpha($image,($color >> 16) & 0xFF,($color >> 8) & 0xFF,$color & 0xFF,(1 - $visibility) * 127));
                }
            }
            imagedestroy($text_shadow_image);
        }
        else {
            return imagettftext($image,$size,$angle,$x,$y,$color,$fontfile,$text);
        }
    }

    public function autocropJpeg($filename)
    {
        if ('image/jpeg' != getimagesize($filename)['mime']) {
            return false;
        }

        $image = imagecreatefromjpeg($filename);

        $width = imagesx($image);
        $heigth = imagesy($image);

        $img = imagecreatetruecolor($width, $heigth);
        imagecopy($img, $image, 0, 0, 0, 0, $width, $heigth);

        imagefilter($img, IMG_FILTER_GRAYSCALE);
        imagefilter($img, IMG_FILTER_SMOOTH, 10);
        imagetruecolortopalette($img, false, 16);

        $b_top = $b_btm = $b_lft = $b_rgt = 0;
        $crop_color = imagecolorat($img, 1, 1);

        for (; $b_top < $heigth; ++$b_top)
            for ($x = 0; $x < $width; ++$x)
                if (imagecolorat($img, $x, $b_top) != $crop_color)
                    break 2;

        for (; $b_btm < $heigth; ++$b_btm)
            for ($x = 0; $x < $width; ++$x)
                if (imagecolorat($img, $x, $heigth - $b_btm - 1) != $crop_color)
                    break 2;

        for (; $b_lft < $width; ++$b_lft)
            for ($y = 0; $y < $heigth; ++$y)
                if (imagecolorat($img, $b_lft, $y) != $crop_color)
                    break 2;

        for (; $b_rgt < $width; ++$b_rgt)
            for ($y = 0; $y < $heigth; ++$y)
                if (imagecolorat($img, $width - $b_rgt - 1, $y) != $crop_color)
                    break 2;

        $b_lft = max($b_lft - 5, 0);
        $b_rgt = max($b_rgt - 5, 0);
        $b_top = max($b_top - 5, 0);
        $b_btm = max($b_btm - 5, 0);

        if (max($b_lft, $b_rgt, $b_top, $b_btm) > 0) {
            $croped = imagecreatetruecolor($width - ($b_lft + $b_rgt), $heigth - ($b_top + $b_btm));
            imagecopy($croped, $image, 0, 0, $b_lft, $b_top, $width, $heigth);
            imagejpeg($croped, $filename);

            return true;
        }

        return false;
    }

	public static function HexToRGB($hex)
    {
        $hex = ltrim($hex, '#');
        $color = array();

        if(strlen($hex) == 3) {
            $color['r'] = hexdec(str_repeat(substr($hex, 0, 1), 2));
            $color['g'] = hexdec(str_repeat(substr($hex, 1, 1), 2));
            $color['b'] = hexdec(str_repeat(substr($hex, 2, 1), 2));
        } else if(strlen($hex) == 6) {
            $color['r'] = hexdec(substr($hex, 0, 2));
            $color['g'] = hexdec(substr($hex, 2, 2));
            $color['b'] = hexdec(substr($hex, 4, 2));
        }

        return $color;
    }

    public static function RGBToHex($r, $g, $b)
    {
        $hex = "#";
        $hex.= str_pad(dechex($r), 2, "0", STR_PAD_LEFT);
        $hex.= str_pad(dechex($g), 2, "0", STR_PAD_LEFT);
        $hex.= str_pad(dechex($b), 2, "0", STR_PAD_LEFT);

        return $hex;
    }

}
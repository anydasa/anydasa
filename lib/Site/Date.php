<?

class Site_Date
{

    private $_date = null;

    function __construct($date)
    {
        $this->_date = getdate($date);
        if ($this->_date['seconds'] < 10)
            $this->_date['seconds'] = '0' . $this->_date['seconds'];
        if ($this->_date['minutes'] < 10)
            $this->_date['minutes'] = '0' . $this->_date['minutes'];
        if ($this->_date['hours'] < 10)
            $this->_date['hours'] = '0' . $this->_date['hours'];
    }

    public function isToday()
    {
        return (date('j') == $this->_date['mday']) ? true : false;
    }

    public function isYesterday()
    {
        return (date('j') == ($this->_date['mday'] + 1)) ? true : false;
    }

    public function get($part)
    {
        if (!empty($this->_date[$part])) {
            return $this->_date[$part];
        }
        return false;
    }

}
<?

class Site_CSV
{
    private $array = array();

    static public function strToArray($str)
    {
        return self::parse_csv($str);
    }

    static public function arrayToCsv(array $array, $isUTF8 = false)
    {
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        $df = fopen("php://output", 'w');

        if ( $isUTF8 ) {
            fprintf($df, chr(0xEF).chr(0xBB).chr(0xBF));
        }

        foreach ($array as $row) {
            fputcsv($df, $row, ';');
        }

        fclose($df);

        return ob_get_clean();
    }


    static private function parse_csv($str)
    {
        $str = preg_replace_callback('/([^"]*)("((""|[^"])*)"|$)/s', array(self, 'parse_csv_quotes'), $str);
        $str = preg_replace('/\n$/', '', $str);

        return array_map(array(self, 'parse_csv_line'), explode("\n", $str));
    }

    static private function parse_csv_quotes($matches)
    {
        $str = str_replace("\r", "\rR", $matches[3]);
        $str = str_replace("\n", "\rN", $str);
        $str = str_replace('""', "\rQ", $str);
        $str = str_replace(',', "\rC", $str);

        return preg_replace('/\r\n?/', "\n", $matches[1]) . $str;
    }

    static private function parse_csv_line($line)
    {
        return array_map(array(self, 'parse_csv_field'), explode(';', $line));
    }

    static private function parse_csv_field($field)
    {
        $field = str_replace("\rC", ',', $field);
        $field = str_replace("\rQ", '"', $field);
        $field = str_replace("\rN", "\n", $field);
        $field = str_replace("\rR", "\r", $field);

        return $field;
    }
}
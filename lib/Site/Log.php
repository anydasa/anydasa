<?

class Site_Log
{

    private static $_logger = null;

    public static function getInstance()
    {
        return self::$_logger;
    }

    public static function add($msg, $code = Zend_Log::INFO)
    {
        if (!is_null(self::$_logger)) {
            try {
                self::$_logger->log($msg, $code);
                return true;
            } catch (Zend_Log_Exception $e) {
                // do nothing
            }
        }

        return false;
    }

    public static function init($logdir, $debug = false)
    {
        if (is_dir($logdir) && is_writable($logdir)) {

            $logger = new Zend_Log();

            $w_err = new Zend_Log_Writer_Stream($logdir . 'error.log');
            $f_err = new Zend_Log_Filter_Priority(Zend_Log::ERR);
            $w_err->addFilter($f_err);
            $logger->addWriter($w_err);

            $w_notice = new Zend_Log_Writer_Stream($logdir . 'notice.log');
            $f_notice = new Zend_Log_Filter_Priority(Zend_Log::NOTICE);
            $w_notice->addFilter($f_notice);
            $f_notice = new Zend_Log_Filter_Priority(Zend_Log::ERR, '>');
            $w_notice->addFilter($f_notice);
            $logger->addWriter($w_notice);

            $w_info = new Zend_Log_Writer_Stream($logdir . 'info.log');
            $f_info = new Zend_Log_Filter_Priority(Zend_Log::INFO, '=');
            $w_info->addFilter($f_info);
            $logger->addWriter($w_info);

            if ($debug) {
                $w_debug = new Zend_Log_Writer_Stream($logdir . 'debug.log');
                $f_debug = new Zend_Log_Filter_Priority(Zend_Log::DEBUG, '=');
                $w_debug->addFilter($f_debug);
                $logger->addWriter($w_debug);
            }

            self::$_logger = $logger;
        } else {
            require_once 'Zend/Log/Exception.php';
            throw new Zend_Log_Exception("Отсутcтвует каталог \"$logdir\" или нет прав на запись.");
        }
    }

}
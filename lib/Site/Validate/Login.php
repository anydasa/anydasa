<?
class Site_Validate_Login extends Zend_Validate_Abstract
{
    private $_token;
    private $_table;
    private $_identityColumn;
    private $_credentialColumn;

    const MISMATCH = 'mismatch';

    protected $_messageTemplates = array(
        self::MISMATCH => 'Mismatch password'
    );

    public function __construct($token, $table, $identityColumn, $credentialColumn)
    {
        $this->_token            = $token;
        $this->_table            = $table;
        $this->_identityColumn   = $identityColumn;
        $this->_credentialColumn = $credentialColumn;

        if ( !class_exists('Doctrine_Query') ) {
            require_once 'Site/Db/Exception.php';
            throw new Site_Db_Exception('Class Doctrine_Query not exist');
        }
    }

     public function isValid($value, $context = null)
     {

         $authAdapter = new Site_Auth_Adapter_Doctrine_Table(Doctrine_Manager::connection());

         $authAdapter
             ->setTableName($this->_table)
             ->setIdentityColumn($this->_identityColumn)
             ->setCredentialColumn($this->_credentialColumn)
             ->setIdentity($context[$this->_token])
             ->setCredential($value);

         $result = $authAdapter->authenticate();

        if ( Zend_Auth_Result::SUCCESS == $result->getCode() ) {
            return true;
        }

        $this->_error(self::MISMATCH);
        return false;
     }
}
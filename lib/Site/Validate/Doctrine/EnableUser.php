<?

class Site_Validate_Doctrine_EnableUser extends Zend_Validate_Abstract
{
    const DISABLED_REASON = 'disabledReason';

    protected $_table = null;

    protected $_messageVariables = array(
        'disabled_reason' => '_disabled_reason'
    );
    protected $_disabled_reason;

    protected $_messageTemplates = array(
        self::DISABLED_REASON => 'Ваш аккаунт заблокирован причина "%disabled_reason%"'
    );

    public function __construct($table)
    {
        $this->_table = $table;

        if (!class_exists('Doctrine_Query')) {
            require_once 'Site/Db/Exception.php';
            throw new Site_Db_Exception('Class Doctrine_Query not exist');
        }
    }

    public function isValid($value)
    {
        $this->_setValue($value);

        $Query = Doctrine_Query::create()
            ->select('disabled_reason')
            ->from($this->_table)
            ->where('email ilike ?', $value)
            ->addWhere('is_enabled = 0');

        if ( ($error_message = $Query->fetchOne()) !== false) {
            $this->_disabled_reason = $error_message->disabled_reason;
            $this->_error(self::DISABLED_REASON);
            return false;
        }

        return true;
    }

}
<?

class Site_Validate_Doctrine_InTable extends Zend_Validate_Abstract
{
    const RECORD_NOT_EXISTS = 'notInTable';

    protected $_table = null;
    protected $_field = null;
    protected $_query = null;
    protected $_messageTemplates = array(
        self::RECORD_NOT_EXISTS => 'Record with value %value% not exists in table'
    );

    public function __construct($table, $field)
    {
        $this->_table = $table;
        $this->_field = $field;
        if (!class_exists('Doctrine_Query')) {
            require_once 'Site/Db/Exception.php';
            throw new Site_Db_Exception('Class Doctrine_Query not exist');
        }
        $this->_query = Doctrine_Query::create();
    }

    public function isValid($value)
    {
        $this->_setValue($value);

        $this->_query
            ->select($this->_field)
            ->from($this->_table)
            ->whereNotIn($this->_field, $value);

        if ($this->_query->fetchOne() === false) {
            $this->_error(self::RECORD_NOT_EXISTS);
            return false;
        }

        return true;
    }

}
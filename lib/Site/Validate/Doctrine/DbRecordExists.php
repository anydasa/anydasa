<?

class Site_Validate_Doctrine_DbRecordExists extends Zend_Validate_Abstract
{
    const RECORD_NOT_EXISTS = 'dbRecordNotExists';

    protected $_query   = null;
    protected $_table   = null;
    protected $_exclude = null;

    protected $_messageTemplates = array(
        self::RECORD_NOT_EXISTS => 'Record with value %value% not exists in table'
    );

    public function __construct($table, $field, $exclude = null)
    {
        $this->_table   = $table;
        $this->_field   = $field;
        $this->_exclude = $exclude;

        $this->_query = Doctrine_Query::create()->select($field)->from($table);
    }

    public function getParams()
    {
        return array(
            'table' => $this->_table,
            'field' => $this->_field,
            'exclude' => $this->_exclude,
        );
    }

    public function isValid($value)
    {
        $this->_setValue($value);

        $this->_query->addWhere($this->_field . '::text ilike ?', $value);

        if ( !empty($this->_exclude) && !empty($context[$this->_exclude])) {
            $this->_query->addWhere($this->_exclude . ' <> ?', $context[$this->_exclude]);
        }

        if ($this->_query->fetchOne() === false) {
            $this->_error(self::RECORD_NOT_EXISTS);
            return false;
        }


        return true;
    }

}
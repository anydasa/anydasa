<?

class Site_Validate_Doctrine_NoDbRecordExists extends Zend_Validate_Abstract
{
    const RECORD_EXISTS = 'dbRecordExists';

    protected $_id;
    protected $_table   = null;
    protected $_exclude = null;
    protected $_unic_field = null;

    protected $_messageTemplates = array(
        self::RECORD_EXISTS => 'Record with value %value% already exists in table'
    );

    protected $_messageVariables = array(
        'id' => '_id',
    );

    public function __construct($table, $field, $exclude = null, $unic_field = null)
    {
        $this->_table       = $table;
        $this->_field       = $field;
        $this->_exclude     = $exclude;
        $this->_unic_field  = $unic_field;
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue($value);

        $query = Doctrine_Query::create()
                    ->from($this->_table)
                    ->addWhere($this->_field . '::text ilike ?', $value);

        if ( !empty($this->_exclude) && !empty($context[$this->_exclude])) {
            $query->addWhere($this->_exclude . ' <> ?', $context[$this->_exclude]);
        }

        if ( !empty($this->_unic_field) && !empty($context[$this->_unic_field])) {
            $query->addWhere($this->_unic_field . ' = ?', $context[$this->_unic_field]);
        }

        if (($result = $query->fetchOne()) !== false) {
            $this->_id = $result[$this->_field];
            $this->_error(self::RECORD_EXISTS);
            return false;
        }

        return true;
    }
}
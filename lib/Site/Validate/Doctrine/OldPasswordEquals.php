<?

class Site_Validate_Doctrine_OldPasswordEquals extends Zend_Validate_Abstract
{
    const RECORD_NOT_EQUAL = 'dbRecordNotEquals';

    protected $_table = null;
    protected $_query = null;
    protected $_fieldSearch = null;
    protected $_searchValue = null;
    protected $_fieldCompare = null;
    protected $_messageTemplates = array(
        self::RECORD_NOT_EQUAL => 'Record with value %value% not equals'
    );

    public function __construct($table, $fieldSearch, $fieldCompare, $searchValue)
    {
        $this->_table = $table;
        $this->_fieldSearch = $fieldSearch;
        $this->_fieldCompare = $fieldCompare;
        $this->_searchValue = $searchValue;

        if (!class_exists('Doctrine_Query')) {
            require_once 'Site/Db/Exception.php';
            throw new Site_Db_Exception('Class Doctrine_Query not exist');
        }
        $this->_query = Doctrine_Query::create();
    }

    public function isValid($value)
    {
        $this->_setValue($value);

        $this->_query
            ->select($this->_fieldSearch)
            ->from($this->_table)
            ->where($this->_fieldSearch . ' = ?', $this->_searchValue);

        $node = $this->_query->fetchOne();

        if ($node === false || $node->{$this->_fieldCompare} != md5($value)) {
            $this->_error(self::RECORD_NOT_EQUAL);
            return false;
        }

        return true;
    }

}
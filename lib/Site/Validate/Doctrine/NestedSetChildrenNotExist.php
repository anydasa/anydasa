<?

class Site_Validate_Doctrine_NestedSetChildrenNotExist extends Zend_Validate_Abstract
{
    const RECORD_HAS_CHILDRENS = 'recordHasChildrens';
    const RECORD_NOT_FOUND = 'recordNotFound';

    protected $_table = null;
    protected $_messageTemplates = array(
        self::RECORD_HAS_CHILDRENS => 'Record %value% has childrens',
        self::RECORD_NOT_FOUND => 'Record %value% not found'
    );

    public function __construct($table)
    {
        $this->_table = $table;
        if (!class_exists('Doctrine_Table')) {
            require_once 'Site/Db/Exception.php';
            throw new Site_Db_Exception('Class Doctrine_Table not exist');
        }
        $this->_table = Doctrine::getTable($this->_table);
    }

    public function isValid($value)
    {
        $this->_setValue($value);

        $node = $this->_table->find($value);

        if (!$node) {
            $this->_error(self::RECORD_NOT_FOUND);
            return false;
        }

        if ($node->getNode()->hasChildren()) {
            $this->_error(self::RECORD_HAS_CHILDRENS);
            return false;
        }

        return true;
    }

}
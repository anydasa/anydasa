<?

require_once 'Zend/Validate/Abstract.php';
require_once 'Zend/Validate.php';

class Site_Validate_EmailBool extends Zend_Validate_Abstract
{
    const NOT_VALID = 'emailNotValid';

    protected $_messageTemplates = array(
        self::NOT_VALID => 'This email address is not valid'
    );

    public function isValid($value)
    {
        if (Zend_Validate::is($value, 'EmailAddress')) {
            return true;
        }

        $this->_error(self::NOT_VALID);
        return false;
    }

}
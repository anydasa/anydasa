<?

class Site_Validate_File_ImageSize extends Zend_Validate_File_ImageSize
{
    const SIDE_TOO_BIG    = 'fileImageSideTooBig';
    const SIDE_TOO_SMALL  = 'fileImageSideTooSmall';

    protected $_my_messageTemplates = array(
        self::SIDE_TOO_BIG    => "Maximum allowed side for image '%value%' should be '%maxside%px' but 'width = %width%px' and 'height = %height%px'",
        self::SIDE_TOO_SMALL  => "Minimum allowed side for image '%value%' should be '%minside%px' but 'width = %width%px' and 'height = %height%px'",
    );

    protected $_my_messageVariables = array(
        'minside'  => '_minside',
        'maxside'  => '_maxside',
    );

    protected $_maxside;
    protected $_minside;

    function __construct($options)
    {
        parent::__construct($options);

        $this->_messageTemplates += $this->_my_messageTemplates;
        $this->_messageVariables += $this->_my_messageVariables;

        if ( isset($options['minside']) ) {
            $this->setMinSide($options['minside']);
        }
        if ( isset($options['maxside']) ) {
            $this->setMaxSide($options['maxside']);
        }
    }

    protected function setMaxSide($maxside)
    {
        $this->_maxside = (int) $maxside;
        return $this;
    }

    protected function setMinSide($minside)
    {
        $this->_minside = (int) $minside;
        return $this;
    }

    public function isValid($value, $file = null)
    {
        $isValid = parent::isValid($value, $file);

        if ( ($this->_minside !== null) and ($this->_width < $this->_minside and $this->_height < $this->_minside) ) {
            $this->_throw($file, self::SIDE_TOO_SMALL);
            return false;
        }

        if ( ($this->_maxside !== null) AND ($this->_width > $this->_maxside OR $this->_height > $this->_maxside) ) {
            $this->_throw($file, self::SIDE_TOO_BIG);
            return false;
        }

        return $isValid;
    }
}
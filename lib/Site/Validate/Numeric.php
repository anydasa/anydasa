<?

require_once 'Zend/Validate/Abstract.php';
require_once 'Zend/Validate.php';

class Site_Validate_Numeric extends Zend_Validate_Abstract
{
    const NOT_VALID = 'notNumeric';

    protected $_messageTemplates = array(
        self::NOT_VALID => 'This numeric is not valid'
    );

    public function isValid($value)
    {
        if ( is_numeric($value) OR empty($value) ) {
            return true;
        }

        $this->_error(self::NOT_VALID);
        return false;
    }

}
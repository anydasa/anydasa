<?

require_once 'Zend/Validate/Abstract.php';

class Site_Validate_PasswordConfirmation extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';
    const EMPTY_CONFIRM_FIELD = 'emptyConfirmField';

    protected $_contextKey;
    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Password confirmation does not match',
        self::EMPTY_CONFIRM_FIELD => 'Password confirmation is empty'
    );

    public function __construct($key = 'password_confirm')
    {
        $this->_contextKey = $key;
    }

    public function isValid($value, $context = null)
    {
        $value = (string) $value;

        if (is_array($context)) {
            if (isset($context[$this->_contextKey]) && empty($context[$this->_contextKey])) {
                $this->_error(self::EMPTY_CONFIRM_FIELD);
                return false;
            } elseif (isset($context[$this->_contextKey]) && ($value === $context[$this->_contextKey])) {
                return true;
            }
        } elseif (is_string($context) && empty($context)) {
            $this->_error(self::EMPTY_CONFIRM_FIELD);
            return false;
        } elseif (is_string($context) && ($value === $context)) {
            return true;
        }

        $this->_error(self::NOT_MATCH);
        return false;
    }

}
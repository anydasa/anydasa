<?

class Site_Validate_ArrayLength extends Zend_Validate_Abstract
{
    const TOO_SHORT = 'arrayLengthTooShort';
    const TOO_LONG = 'arrayLengthTooLong';

    protected $_min;
    protected $_max;
    protected $_field;
    protected $_messageTemplates = array(
        self::TOO_SHORT => "'%value%' is less than %min% items long",
        self::TOO_LONG => "'%value%' is greater than %max% items long",
    );
    protected $_messageVariables = array(
        'min' => '_min',
        'max' => '_max'
    );

    public function __construct($min = 0, $max = null, $field)
    {
        $this->setMin($min);
        $this->setMax($max);
        $this->_field = $field;
    }

    public function setMin($min)
    {
        if (null !== $this->_max && $min > $this->_max) {
            /**
             * @see Zend_Validate_Exception
             */
            require_once 'Zend/Validate/Exception.php';
            throw new Zend_Validate_Exception("The minimum must be less than or equal to the maximum length, but $min >"
                . " $this->_max");
        }
        $this->_min = max(0, (integer) $min);
        return $this;
    }

    public function setMax($max)
    {
        if (null === $max) {
            $this->_max = null;
        } else if ($max < $this->_min) {
            require_once 'Zend/Validate/Exception.php';
            throw new Zend_Validate_Exception("The maximum must be greater than or equal to the minimum length, but "
                . "$max < $this->_min");
        } else {
            $this->_max = (integer) $max;
        }

        return $this;
    }

    public function isValid($value, $context = null)
    {
        $this->_setValue($value);

        $length = count($context[$this->_field]);


        if ($length < $this->_min) {
            $this->_error(self::TOO_SHORT);
        }
        if ($this->_max < $length) {
            $this->_error(self::TOO_LONG);
        }

        if (count($this->_messages)) {
            return false;
        } else {
            return true;
        }
    }

}


<?php

class Site_View_Helper_Time
{
    public function time($time, $show_minutes = true)
    {
        if ( is_null($time) ) {
            return '';
        }

        $time = new DateTime($time);

        if ( $time->format('Y-m-d') == (new DateTime('today'))->format('Y-m-d') ) {
            $timestr = 'сегодня';
        } elseif ( $time->format('Y-m-d') == (new DateTime('yesterday'))->format('Y-m-d') ) {
            $timestr = 'вчера';
        } else {
            $timestr = $time->format('d.m.Y');
        }

        if ( $show_minutes ) {
            $timestr .= $time->format(', H:i');
        }

         return $timestr;
    }
}
<?php

class Site_View_Helper_Fieldset extends Zend_View_Helper_FormElement
{
   
    public function fieldset($name, $content, $attribs = null)
    {
        $info = $this->_getInfo($name, $content, $attribs);
        extract($info);

        // get legend
        $legend = '';
        if (isset($attribs['legend'])) {
            $legendString = trim($attribs['legend']);
            if (!empty($legendString)) {
                $legend = '<th>'
                        . (($escape) ? $this->view->escape($legendString) : $legendString)
                        . '</th>' . PHP_EOL;
            }
            unset($attribs['legend']);
        }

        // get id
        if (!empty($id)) {
            $id = ' id="' . $this->view->escape($id) . '"';
        } else {
            $id = '';
        }

        // render fieldset
        $xhtml = '<tr'
               . $id
               . $this->_htmlAttribs($attribs)
               . '>'
               . $legend
               . '<td>' . $content . '</td>'
               . '</tr>';

        return $xhtml;
    }
}

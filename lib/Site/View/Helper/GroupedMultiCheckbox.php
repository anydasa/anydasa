<?
class Site_View_Helper_GroupedMultiCheckbox extends Zend_View_Helper_FormMultiCheckbox
{
    public function groupedMultiCheckbox($name, $value = null, $attribs = null, $options = null, $listsep = "\n")
    {
        if (is_array($options))
        {
            $html = array();
            $translator = $this->getTranslator();
            foreach ($options as $group => $values)
            {
                $group_html = array();
                $group_html[] = '';
                $text_group = null !== $translator ? $translator->translate($group) : $group;
                $group_html[] = '' . $this->view->escape($text_group) . '';
                $group_html[] = $this->formMultiCheckbox($name, $value, $attribs, $values, $listsep);
                $group_html[] = '';

                $html[] = implode("\n", $group_html);
            }
            return implode("\n", $html);
        }
        else
        {
            return $this->formMultiCheckbox($name, $value, $attribs, $options, $listsep);
        }
    }
}
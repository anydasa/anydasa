<?

class Site_Months
{

    static function getList()
    {
        return array(
            1 => 'январь',
            2 => 'февраль',
            3 => 'март',
            4 => 'апрель',
            5 => 'май',
            6 => 'июнь',
            7 => 'июль',
            8 => 'август',
            9 => 'сентябрь',
            10 => 'октябрь',
            11 => 'ноябрь',
            12 => 'декабрь',
        );
    }

    static function getByNumber($number)
    {
        $list = self::getList();
        if (in_array($number, array_keys($list))) {
            return $list[$number];
        }
        return '';
    }

    static function getGenitiveList()
    {
        return array(
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        );
    }

    static function getGenitiveByNumber($number)
    {
        $list = self::getGenitiveList();
        if (in_array($number, array_keys($list))) {
            return $list[$number];
        }
        return '';
    }

}
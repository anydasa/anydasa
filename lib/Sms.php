<?


class Sms
{
    public static function send($phone, $message)
    {
        if ( IS_DEV ) {
            file_put_contents(ROOT_PATH . 'sms.txt', $message);
        } else {
            return;

            $config = Zend_Registry::getInstance()->config->sms->toArray();
            $Turbosms = new Turbosms($config['login'], $config['password']);
            return $Turbosms->send($config['sender'], $phone, $message);
        }
        
    }
}

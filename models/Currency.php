<?

class Currency extends BaseCurrency
{

    static protected $list = array();

    public function preInsert($event)
    {
        $this->sort = Doctrine_Query::create()->select('max(sort) max')->from(__CLASS__)->fetchOne()->max + 1;
    }

    static public function getById($id)
    {
        foreach (self::getList() as $Currency) {
            if ($Currency->id == $id) {
                return $Currency;
            }
        }
        throw new Exception('Данной валюты с ID ("'.$id.'")  нет.');
    }

    public static function getBase()
    {
        foreach (self::getList() as $Currency) {
            if (1 == $Currency->rate) {
                return $Currency;
            }
        }
        throw new Exception('Не найдена базовая валюта.');
    }

    /*public static function getListWithBase($iso = 'UAH')
    {
        $return = Doctrine_Query::create()->from('Currency')->orderBy('sort')->execute();
        $CurrentBase = self::getBase();

        foreach ($return as $Currency) {
            if ( $Currency->iso == $iso ) {
                $Currency->rate = 1;
            } else {
                $Currency->rate = round(($Currency->rate / $CurrentBase->rate) * static::getByISO($iso)->rate, $Currency->precision);
            }

        }
        return $return;
    }*/

    static public function getByISO($iso): self
    {
        foreach (self::getList() as $Currency) {
            if ($Currency->iso == $iso) {
                return $Currency;
            }
        }
        throw new Exception('Данной валюты ("'.$iso.'")  нет.');
    }

    public static function issetByISO($iso)
    {
        foreach (self::getList() as $Currency) {
            if ($Currency->iso == $iso) {
                return true;
            }
        }
        return false;
    }

    static public function getVisibleList()
    {
        $list = self::getList();

        foreach ($list as $key=>$Currency) {
            if ( 0 == $Currency->is_visible ) {
                $list->remove($key);
            }
        }

        return $list;
    }

    static public function getList()
    {
        if ( empty(self::$list) ) {
            self::$list =
                Doctrine_Query::create()
                    ->from('Currency')
                    ->orderBy('sort')
                    ->execute();
        }
        return self::$list;
    }

    static public function format($number, $iso, $strip_html = false)
    {
        if ( 0 == $number ) {
            return 0;
        }

        $Currency = self::getByISO($iso);
        $formated_value = number_format($number, $Currency->precision, $Currency->decimal, $Currency->thousand);

        $return = str_replace('%v', $formated_value, $Currency->format);
        $return = str_replace('%s', $Currency->symbol, $return);

        if ( $strip_html ) {
            $return = strip_tags(html_entity_decode($return));
        }

        return $return;
    }
    
    static public function round($number, $iso)
    {
        return round($number, Currency::getByISO($iso)->precision);
    }

    static public function conv($number, $fromISO, $toISO)
    {
        $return = ($number / Currency::getByISO($fromISO)->rate) * Currency::getByISO($toISO)->rate;

        return round($return, Currency::getByISO($toISO)->precision);
    }

}
<?php

/**
 * @property Doctrine_Collection|ProductsOption[] $ProductsOption
 * @property Doctrine_Collection|SeriesOptions[] $SeriesOptions
 */
class Catalog extends BaseCatalog
{
    public function setUp()
    {
        parent::setUp();

        $this->hasMany('ProductsOption as ProductsOption', array(
                'local'    => 'id',
                'foreign'  => 'id_catalog',
                'orderBy'  => 'sort ASC'
            )
        );

        $this->hasMany('SeriesOptions', array(
                'local'    => 'id',
                'foreign'  => 'id_catalog',
                'orderBy'  => 'sort ASC'
            )
        );
    }

    public function toArray($deep = true, $prefixKey = false)
    {
        $array = parent::toArray($deep, $prefixKey);
        $array['Menu'] = $this->CatalogMenu->toKeyValueArray('id', 'id');
        return $array;
    }

}
<?php


class Products_Price
{
    protected $Excel;
    protected $showImages  = false;
    protected $showGroup   = false;

    protected $levelColors = array (
        1 => '7f7f7f',
        2 => 'a5a5a5',
        3 => 'd8d8d8',
        4 => 'd8d8d8'
    );

    function __construct(Doctrine_Collection $List, $options = array())
    {
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $this->Excel = $objReader->load( $this->getTplPath() );

        $this->Excel->setActiveSheetIndex(0);

        if (isset($options['showImages'])) {
            $this->showImages = (bool)$options['showImages'];
        }

        if (isset($options['showGroup'])) {
            $this->showGroup = (bool)$options['showGroup'];
        }

        $this->makePrice($List);
    }

    protected  function getTplPath()
    {
        $tpl = dirname(dirname($_SERVER['DOCUMENT_ROOT'])).'/templates/TemplatePrice.xlsx';
        if ( !is_file($tpl) ) {
            throw new Site_Exception;
        }
        return $tpl;
    }

    protected function insertImage($row, $img)
    {
        if ( $this->showImages && @getimagesize($img) )
        {
            $this->Excel->getActiveSheet()->getRowDimension($row)->setRowHeight(80);
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath( $img );
            $objDrawing->setCoordinates('L'.$row);
            $objDrawing->setHeight(80);
            $objDrawing->setOffsetX(10);
            $objDrawing->setOffsetY(10);
            $objDrawing->getShadow()->setVisible(true);
            $objDrawing->getShadow()->setDirection(45);
            $objDrawing->setWorksheet($this->Excel->getActiveSheet());
        }
    }

    protected function insertProduct($row, $Product, $Catalog)
    {
        if ( $this->showGroup ) {
            $this->Excel->getActiveSheet()->getRowDimension($row)->setOutlineLevel(1);
        }

        $this->Excel->getActiveSheet()->getCell('A'.$row)->setValue($Product['code']);
        $this->Excel->getActiveSheet()->getCell('B'.$row)->setValue($Product['brand']);
        $this->Excel->getActiveSheet()->getCell('C'.$row)->setValue($Product['type']);

        $this->Excel->getActiveSheet()->getCell('D'.$row)->setValue($Product['title']);
        $this->Excel->getActiveSheet()->getCell('E'.$row)->setValue($Product['price_min']);
        $this->Excel->getActiveSheet()->getCell('F'.$row)->setValue($Product['price_partner']);
        $this->Excel->getActiveSheet()->getCell('G'.$row)->setValue($Product['price_trade']);
        $this->Excel->getActiveSheet()->getCell('H'.$row)->setValue($Product['price_retail']);

        if ( $Product['promotion'] ) {
            $this->Excel->getActiveSheet()->getCell('J'.$row)->setValue('акция')->getHyperlink()->setUrl('http://kissandmakeup.com.ua/promotions/'.$Product['promotion']);
            $this->Excel->getActiveSheet()->getStyle('J'.$row)->getFont()->applyFromArray(array('color' => array('rgb' => 'FF0000'), 'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE));
        }

        $this->Excel->getActiveSheet()->getCell('K'.$row)->setValue('перейти')->getHyperlink()->setUrl('http://kissandmakeup.com.ua/product/'.$Product['code']);;
        $this->Excel->getActiveSheet()->getStyle('K'.$row)->getFont()->applyFromArray(array('color' => array('rgb' => '0000FF'), 'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE));

        $this->Excel->getActiveSheet()->getStyle('E'.$row.':K'.$row)
            ->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $this->Excel->getActiveSheet()->getStyle('I'.$row)->getFill()->applyFromArray(array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array('rgb' => 'DBB04A'),
        ));

        $this->insertImage($row, $Product['img']);
    }

    protected function insertCatalog($row, $Catalog)
    {
        if ( $this->showGroup ) {
            $this->Excel->getActiveSheet()->getRowDimension($row)->setOutlineLevel(0);
        }

        $this->Excel->getActiveSheet()->mergeCells('A'.$row.':K'.$row);
        $this->Excel->getActiveSheet()->getCell('A'.$row)->setValue($Catalog['title']);
        $this->Excel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
        $this->Excel->getActiveSheet()->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $this->Excel->getActiveSheet()->getStyle('A'.$row)->getFont()->setBold(true)->setSize(10);
        $this->Excel->getActiveSheet()->getStyle('A'.$row)->getFill()->applyFromArray(array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'startcolor' => array('rgb' => $this->levelColors[1])));
    }

    protected function makePrice($List)
    {

        $Products = $this->preparProducts($List);
        $Catalogs = $this->preparCatalog();
        $Catalogs = $this->dropEmptyCatalog($Catalogs, $Products);

        $row = 10;
        foreach ($Catalogs as $catalog) {
            $row++;
            $this->insertCatalog($row, $catalog);
            if ( !empty($Products[$catalog['id']]) ) {
                foreach($Products[$catalog['id']] as $Product) {
                    $row++;
                    $this->insertProduct($row, $Product, $catalog);
                }
            }
        }

        $this->Excel->getActiveSheet()->getStyle('A10:K' . $this->Excel->getActiveSheet()->getHighestRow())->applyFromArray(
            array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
            )))
        );
        $this->Excel->getActiveSheet()->getCell('D8')->setValue(date('d.m.Y'));

    }
    public function getPrice($prices = null)
    {
        if ( !is_array($prices) ) {
            $prices = array($prices);
        }

        in_array('trade', $prices)   ? null : $this->Excel->getActiveSheet()->removeColumn('G');
        in_array('partner', $prices) ? null : $this->Excel->getActiveSheet()->removeColumn('F');
        in_array('min', $prices)     ? null : $this->Excel->getActiveSheet()->removeColumn('E');


        $this->Excel->getActiveSheet()->getCell('D8')->setValue(date('d.m.Y'));

        $objWriter = new PHPExcel_Writer_Excel2007($this->Excel);

        ob_start();
        $objWriter->save('php://output');
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }

    protected function dropEmptyCatalog($catalogs, $products)
    {
        return array_intersect_key($catalogs, $products);
    }



    protected function preparCatalog()
    {
        $catalogs = array();

        foreach ( Doctrine_Query::create()->from('Catalog')->orderBy('title')->execute() as $catalog) {
            $catalogs[$catalog->id] = $catalog->toArray();
        }

        return $catalogs;
    }

    protected function preparProducts(Doctrine_Collection $List)
    {
        $List->loadRelated('ProductsBrands');

        $cats = array();
        foreach (Doctrine::getTable('ProductsRefCatalog')->findAll()->toArray() as $cat) {
            $cats[$cat['id_product']] = $cat['id_catalog'];
        }

        $return = array();
        foreach ($List as $item) {
            $return[$cats[$item->id]][] = array(
                'code'          => $item->code,
                'brand'         => $item->ProductsBrands->title,
                'type'          => $item->type,
                'title'         => $item->title,
                'price_min'     => $item->getFormatedPrice('min', 'USD'),
                'price_partner' => $item->getFormatedPrice('partner', 'USD'),
                'price_trade'   => $item->getFormatedPrice('trade', 'USD'),
                'price_retail'  => $item->getFormatedPrice('retail', 'USD'),
                'img'           => $item->getImage()->getFirstImage()['thumb']['path'],
                'promotion'     => $item->getActualPromotion()->id,
            );
        }

        return $return;
    }
}
<?php


class Products_Privat
{
    protected $Excel;
    protected $showImages  = false;
    protected $showGroup   = false;
    protected $catalogs;
    protected $agent;


    protected $head = [
        'Код(склад)',
        'Каталог',
        'Тип товара',
        'Название товара',
        'Бренд',
        'Наличие статус',
        'Розничная цена',
        'Количество платежей в ОЧ',
        'Ссылка на Фото1',
        'Ссылка на Фото2',
        'Описание товара',
    ];

    protected $columns = [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
    ];

    function __construct($List, $Agent, $options = array())
    {
        $this->agent = $Agent;
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $this->Excel = $objReader->load( $this->getTplPath() );

        $this->Excel->setActiveSheetIndex(0);

        $catalogs = Doctrine::getTable('Catalog')->findAll(Doctrine_Core::HYDRATE_ARRAY);

        $this->catalogs = array_combine(
            array_column($catalogs, 'id'),
            array_column($catalogs, 'title')
        );

        $row = 1;
        $this->insertHead($row);

        foreach ($List as $Product) {
            $row++;
            $this->insertProduct($row, $Product);
        }
    }

    protected function insertHead($row = 1)
    {
        $i = 0;
        foreach ($this->head as $title) {
            $column = $this->columns[$i];
            $this->Excel->getActiveSheet()->getCell($column . $row)->setValue($title);
            $i++;
        }
    }

    protected  function getTplPath()
    {
        $tpl = dirname(dirname($_SERVER['DOCUMENT_ROOT'])).'/templates/Privat.xlsx';
        if ( !is_file($tpl) ) {
            throw new Site_Exception;
        }
        return $tpl;
    }


    protected function insertProduct($row, $Product)
    {
        $this->getSheet()->getCell('A' . $row)->setValueExplict($Product['code'], PHPExcel_Cell_DataType::TYPE_STRING);
        $this->getSheet()->getCell('B' . $row)->setValue($this->catalogs[reset($Product['catalogs'])]);
        $this->getSheet()->getCell('C' . $row)->setValue($Product['type']);
        $this->getSheet()->getCell('D' . $row)->setValue($Product['title']);
        $this->getSheet()->getCell('E' . $row)->setValue($Product['brand']);
        $this->getSheet()->getCell('F' . $row)->setValue(($Product['status'] == Products::STATUS_AVAILABLE || $Product['status'] == Products::STATUS_ORDER) ? 1 : 0);
        $this->getSheet()->getCell('G' . $row)->setValue($Product['price_retail']);
        $this->getSheet()->getCell('H' . $row)->setValue(((!empty($this->agent->params['parts_payment']) && !empty($Product['parts_payment'])) ? 3: 0));
        $this->getSheet()->getCell('I' . $row)->setValue(current($Product['images']));
        $this->getSheet()->getCell('J' . $row)->setValue(next($Product['images']));
        $this->getSheet()->getCell('K' . $row)->setValue($Product['description']);
    }

    protected function getSheet()
    {
        return $this->Excel->getActiveSheet();
    }

    public function getPrice()
    {
        $objWriter = new PHPExcel_Writer_Excel2007($this->Excel);

        ob_start();
        $objWriter->save('php://output');
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }
}
<?php


class Products_Image
{
    private $_isUseWotermark    = false;
    private $_productId         = null;
    private $_imgDir            = array();
    private $_wotermark         = array();

    private $_sizes = array(//width, height, padding
        'thumb'  => array(50, 50, 3),
        'small'  => array(100, 100, 5),
        'middle' => array(300, 300, 15),
        'big'    => array(1000, 1000, 50)
    );

    function __construct($product_id)
    {
        $this->_productId       =  $product_id;
        $this->_imgDir['url']   = Zend_Registry::get('config')->url->pix  . 'img/products/' . $product_id . '/';
        $this->_imgDir['path']  = Zend_Registry::get('config')->path->pix . 'img/products/' . $product_id . '/';

        $this->_wotermark['url']   = Zend_Registry::get('config')->url->pix  . 'img/watermark.png';
        $this->_wotermark['path']  = Zend_Registry::get('config')->path->pix . 'img/watermark.png';
    }

    static function getAllProducts($filter = null)
    {
        $dir = Zend_Registry::get('config')->path->pix  . 'img/products/';
        $return = array();

        foreach (glob($dir.'*', GLOB_ONLYDIR) as $imgDir) {

            if ( $filter && 'has_duplicate' == $filter ) {
                $Ob = new Products_Image(basename($imgDir));
                if ( !$Ob->hasDuplicates() ) {
                    continue;
                }
            }

            if ( $filter && 'has_dead' == $filter ) {
                $Ob = new Products_Image(basename($imgDir));
                if ( $Ob->hasDeadImages() ) {
                    continue;
                }
            }

            if ( $filter && 'very_small' == $filter ) {
                $Ob = new Products_Image(basename($imgDir));
                if ( $Ob->hasVerySmallOriginal() ) {
                    continue;
                }
            }

            $return []= basename($imgDir);
        }

        return $return;
    }

    public function hasVerySmallOriginal()
    {
        foreach ($this->getImagesList(false) as $item) {
            $imgsize = getimagesize($item['original']['path']);

            if ( $imgsize[0] < 500 && $imgsize[1] < 500 ) {
                return false;
            }

            return true;
        }
    }

    public function hasDeadImages()
    {
        foreach ($this->getImagesList(false) as $img) {
            foreach ($this->_sizes as $size_name=>$size) {
                if ( !is_file($img[$size_name]['path']) || !is_readable($img[$size_name]['path']) || !getimagesize($img[$size_name]['path']) ) {
                    return false;
                }
            }
        }
        return true;
    }

    public function getImgDir()
    {
        return $this->_imgDir;
    }

    public function getTempDirForUpload()
    {
        return dirname($this->_imgDir['path']) .'/';
    }

    public function addNewUploadImage($file_name)
    {
        $temp_file_path = $this->getTempDirForUpload() . $file_name;

        if ( is_file($temp_file_path) ) {
            $this->addNewImage($temp_file_path);
            unlink($temp_file_path);
            return true;
        }

        return false;
    }

    public function hasDuplicates()
    {

        $array = array();

        if ( $this->getImagesCount() > 0 ) {
            foreach ($this->getImagesList() as $img) {
                $array[] = md5_file($img['original']['path']);
            }
        }

        return count($array) !== count(array_unique($array));
    }

    public function isFirst($imgId)
    {
        return 1 == $imgId ? true : false;
    }

    public function isLast($imgId)
    {
        $list = $this->getImagesList();
        return $imgId == max(array_keys($list)) ? true : false;
    }

    public function getImagesCount()
    {
        $count = 0;

        foreach (glob($this->_imgDir['path'].'*', GLOB_ONLYDIR) as $imgDir) {
            $count ++;
        }

        return $count;
    }

    public function selfIntersect(array $images)
    {
        $list = array_keys($this->getImagesList());
        $self_list = array_flip(array_intersect($images, $list));

        rsort($list);

        foreach ($list as $key) {
            if ( !isset($self_list[$key]) ) {
                Site_Function::rmDirR($this->_imgDir['path'].$key);
            } else {
                rename($this->_imgDir['path'].$key, $this->_imgDir['path'].((int)$self_list[$key]+10000) );
            }
        }

        $this->_adjustImages();
        $this->_removeIsEmptyRootDir();
    }

    public function moveDown($imgId)
    {
        $this->_adjustImages();

        if ( $this->getImagesCount() > 1 && !$this->isLast($imgId) ) {
            rename($this->_imgDir['path'].$imgId,       $this->_imgDir['path'].'temp');
            rename($this->_imgDir['path'].($imgId+1),   $this->_imgDir['path'].$imgId);
            rename($this->_imgDir['path'].'temp',       $this->_imgDir['path'].($imgId+1));
        }
    }

    public function moveUp($imgId)
    {
        $this->_adjustImages();

        if ( $this->getImagesCount() > 1 && !$this->isFirst($imgId) ) {
            rename($this->_imgDir['path'].$imgId,       $this->_imgDir['path'].'temp');
            rename($this->_imgDir['path'].($imgId-1),   $this->_imgDir['path'].$imgId);
            rename($this->_imgDir['path'].'temp',       $this->_imgDir['path'].($imgId-1));
        }
    }

    public function getFirstImage()
    {
        return current($this->getImagesList());
    }

    public function removeAllImages()
    {
        @Site_Function::rmDirR($this->_imgDir['path']);
    }

    public function removeImage($imgId)
    {
        Site_Function::rmDirR($this->_imgDir['path'].$imgId);
        $this->_adjustImages();
        $this->_removeIsEmptyRootDir();
    }

    private function _adjustImages()
    {
        if ( $this->getImagesCount() > 0 ) {
            $i = 1;
            foreach ($this->getImagesList() as $key=>$item) {
                rename($this->_imgDir['path'].$key, $this->_imgDir['path'].$i);
                $i++;
            }
        }
    }

    public function createRootImageDir()
    {
        $dir = $this->_imgDir['path'];

        if ( !is_dir($dir) ) {
            if ( !mkdir($dir, 0777) ) {
                throw new Exception('Дириктория '.$dir.' не была создана');
            }
        }
    }

    private function _createItemImageDir($imgId)
    {
        $this->createRootImageDir();

        $dir = $this->_imgDir['path'].$imgId.'/';

        if ( !is_dir($dir) ) {
            if ( !mkdir($dir, 0777) ) {
                throw new Exception('Дириктория '.$dir.' не была создана');
            }
        }
    }

    private function _removeIsEmptyRootDir()
    {
        if ( 0 === (count(glob($this->_imgDir['path']."*"))) ) {
            rmdir($this->_imgDir['path']);
        }
    }

    private function _removeIsEmptyItemDir($imgId)
    {
        if ( 0 === (count(glob($this->_imgDir['path'].$imgId."/*"))) ) {
            rmdir($this->_imgDir['path'].$imgId);
            $this->_removeIsEmptyRootDir();
        }
    }

    public function addNewImage($sourceImgPath)
    {
        $newImageId = $this->_getNextImageId();
        $this->_createItemImageDir($newImageId);

        $newImagePathname = $this->_imgDir['path'] . $newImageId . '/original.jpg';

        $options  = array(
            'http' => array(
                'user_agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
                'header'     => 'Accept-Encoding: gzip\r\n'
            )
        );
        $context = stream_context_create($options);

        if ( true === @copy($sourceImgPath, $newImagePathname, $context) ) {
            $this->_createSizes($newImagePathname);
            return $newImagePathname;
        }

        $this->_removeIsEmptyItemDir($newImageId);
        return false;
    }

    private function _getNextImageId()
    {
        $newId = max(array_merge(array(0), array_keys($this->getImagesList()))) + 1;
        return $newId;
    }

    public function regenerate()
    {
        foreach ($this->getImagesList(false) as $imgId=>$img) {

            array_map('unlink', preg_grep( '/^((?!original.jpg).)*$/', glob($this->_imgDir['path'].$imgId.'/*')) );

            if ( Site_Image::isImage($this->_imgDir['path'].$imgId.'/original.jpg') ) {
                $this->_createSizes($this->_imgDir['path'].$imgId.'/original.jpg');
            }
        }
    }

    public function getImagesList($fillExistingSizes = true)
    {
        $array = array();

        foreach (glob($this->_imgDir['path'].'*', GLOB_ONLYDIR) as $imgDir) {
            $imgId = basename($imgDir);
            $array[$imgId] = array();



            foreach ( glob($imgDir.'/*') as $imgSize ) {
                $array[$imgId][basename($imgSize, '.jpg')] = array(
                                                        'path'=> $imgSize,
                                                        'url' => $this->_imgDir['url'] . $imgId . '/'. basename($imgSize)
                );
            }
            if ( $fillExistingSizes ) {
                foreach ($this->_sizes as $size_key => $size) {
                    if ( !isset($array[$imgId][$size_key]) ) {
                        $max = array_intersect_key($this->_sizes, $array[$imgId]);
                        end($max);
                        $array[$imgId][$size_key] = $array[$imgId][key($max)];
                    }
                }
            }
        }
        ksort($array);
        return count($array) ? $array : array($this->_getNotAvailable());
    }

    private function _getNotAvailable()
    {
        $array = array();

        foreach ($this->_sizes as $size_key => $size) {
            $array[$size_key] = array(
                'path' => Zend_Registry::get('config')->path->public . 'img/nophoto/'.$size_key.'.png',
                'url' => '/img/nophoto/'.$size_key.'.png'
            );
        }

        return $array;
    }

    public function _createSizes($originalImagePath)
    {
        ini_set('memory_limit', '300M');

        $Image = new Site_Image;

        if ('image/jpeg' != Site_Image::getImageMimeType($originalImagePath)) {
            $originalImagePath = $Image->saveAs($originalImagePath, 'image/jpeg', true);
        }

//        $Image->autocropJpeg($originalImagePath);

        foreach ($this->_sizes as $basename=>$size) {
            $itemImageName = dirname($originalImagePath).'/'. $basename .'.jpg';
            copy($originalImagePath, $itemImageName);
            (new Site_Image)->intoRectangle($itemImageName, $size[0], $size[1], '#FFFFFF', $size[2]);

            if ( $this->_isUseWotermark ) {
                $this->setWotermark($itemImageName);
            }
        }
    }

    public function setWotermark ($filepath_original)
    {
        $imgs = getimagesize($filepath_original);
        $wotermarks = getimagesize($this->_wotermark['path']);

        if ( $wotermarks[0] > $imgs[0] || $wotermarks[1] > $imgs[1] ) {
            return;
        }

        $Image = new Site_Image;

        $Image->margeImages($filepath_original, $this->_wotermark['path'], array('x' => ($imgs[0] - $wotermarks[0]) / 2, 'y' => ($imgs[1] - $wotermarks[1]) / 2));
    }
}
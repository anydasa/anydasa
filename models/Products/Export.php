<?


class Products_Export
{
    private $spelling;
    private $tpl_dir;
    private $ProductRoute;
    private $config;

    function __construct()
    {
        $this->config = Zend_Registry::getInstance()->config->toArray();

        $routes = new Zend_Controller_Router_Rewrite;
        $routes->addConfig(new Zend_Config_Ini($this->config['path']['root'].'frontend/app/etc/routers/modules/product.ini', 'product'), 'routes');
        $this->ProductRoute = $routes->getRoute('product');


        $this->tpl_dir = dirname(__FILE__). '/Export/Templates/';

        $this->spelling = Doctrine::getTable('Spelling')
            ->findAll()
            ->toKeyValueArray('key', 'value');
    }

    public function generateOld()
    {
        $generated = [];
        $Query = Doctrine_Query::create()
            ->select('*, (time_generate + INTERVAL \'1 SEC\' * interval_generate < NOW() OR time_generate IS NULL) as is_old ')
            ->from('ProductsExportAgents');

        foreach ($Query->execute() as $Agent) {
            if ( $Agent->is_old OR !is_file($Agent->getFilePath()) ) {

                $this->generate($Agent->id);
                $generated []= $Agent->title;
            }
        }

        return $generated;
    }

    public function generate($id_agent)
    {
        ini_set('memory_limit', '1000M');

        $Agent = Doctrine::getTable('ProductsExportAgents')->find($id_agent);

        if ( $Agent ) {

            $Res = Products_Query::create()
                ->select('p.*, b.title as brand, prom.title as promotion_title, 
                prod_marg.price_retail as prod_marg_price_retail, prod_marg.id_partial_payments, prod_marg.price_pm')
                ->innerJoin('products_brands b ON (p.id_brand = b.id)')
                ->leftJoin('promotions prom ON (p.id_promotion = prom.id AND prom.is_enabled = 1)')
                ->leftJoin('products_margins prod_marg ON p.id = prod_marg.id_product')
                ->setParamsQuery($Agent->params)
                ->execute([], Doctrine_Core::HYDRATE_ARRAY);

            $Guarantee = Doctrine_Query::create()->from('ProductsGuarantee INDEXBY id')->fetchArray();
            $State = Doctrine_Query::create()->from('ProductsState INDEXBY id')->fetchArray();

            $partialPayments = Doctrine_Query::create()->from('PartialPayments')->execute(array(), Doctrine_Core::HYDRATE_ARRAY);

            $partsPaymentSticker = ProductsStickersHelper::getInstance()->findOneBy('alias', 'oplata-chastyami');
            $partsPaymentId = $partsPaymentSticker['id'];

            $Products = [];

            foreach ($Res as $Item) {
                $Products[$Item['id']]                  = $Item;
                $Products[$Item['id']]['part_count']    = 3;
                $Products[$Item['id']]['url']           = $this->config['url']['domain'] . $this->ProductRoute->assemble(['alias' => $Item['alias'], 'id' => $Item['id']], 'product');
                $Products[$Item['id']]['price']         = $this->getPrice($Item, $Agent);
                $Products[$Item['id']]['old_price']     = $this->getOldPrice($Item, $Agent);
                $Products[$Item['id']]['price_retail']  = $this->convertPriceRetail($Item, $Agent);
                if(!empty($Agent->params['parts_payment']) && 13 !== $id_agent) {
                    if (in_array($partsPaymentId, Site_Db_Function::pg_array_parse($Item['stickers']))) {
                        $Products[$Item['id']]['parts_payment'] = 1;
                        $Products[$Item['id']]['price']     = round(1.045 * $Products[$Item['id']]['price']);
                        $Products[$Item['id']]['old_price'] = round(1.045 * $Products[$Item['id']]['old_price']);
                    }
                }

                $currentPartialMonthesCount = 0;
                $currentPartialMonthesPercent = 0;
                //Оплата частями для приватмаркета
                /**
                 * Для товаров, на которых не стоит стикер оплата частями
                ЦенаПМ - не заполнена
                В XML Приватмаркет цена товара рассчитывается по формуле:
                цена розничная + (2,75%).

                Для товаров, на которых не стоит стикер оплата частями
                ЦенаПМ - заполнена
                В XML Приватмаркет цена товара берется из заполненного поля

                Для товаров, со стикером ОплатаЧастями
                Цена ПМ - не заполнена
                В XML Приватмаркет цена товара рассчитывается по формуле: цена розничная + (2,75% + %ОтВыбранногоЗначенияК-воМесяцев)

                Для товаров, со стикером ОплачаЧастями
                Цена ПМ - заполнена
                В XML Приватмаркет цена товара берется из заполненного поля
                 */
                if(13 === $id_agent && !empty($Agent->params['parts_payment'])) {
                    if (in_array($partsPaymentId, Site_Db_Function::pg_array_parse($Item['stickers']))) {
                        $Products[$Item['id']]['parts_payment'] = 1;
                    }
                    if (!in_array($partsPaymentId, Site_Db_Function::pg_array_parse($Item['stickers']))) {
                        if (empty($Products[$Item['id']]['price_pm'])) {
                            $Products[$Item['id']]['price']     = round($Products[$Item['id']]['price_retail'] +
                                $Products[$Item['id']]['price_retail'] * 0.0275);
                        } else  {
                            $Products[$Item['id']]['price'] = round($Products[$Item['id']]['price_pm']);
                        }
                        $Products[$Item['id']]['old_price'] = round($Products[$Item['id']]['old_price'] +
                            $Products[$Item['id']]['old_price'] * 0.0275);
                    } else {
                        foreach ($partialPayments as $partialPayment) {
                            if ($partialPayment['id'] === $Products[$Item['id']]['id_partial_payments']) {
                                $currentPartialMonthesPercent = $partialPayment['percent_value'];
                                $currentPartialMonthesCount   = $partialPayment['month_count'];
                            }
                        }
                        $Products[$Item['id']]['parts_payment'] = 1;
                        $Products[$Item['id']]['part_count'] = !empty($currentPartialMonthesCount) ? $currentPartialMonthesCount : 3;
                        if (empty($Products[$Item['id']]['price_pm'])) {
                            $Products[$Item['id']]['price']     = round($Products[$Item['id']]['price_retail'] +
                                $Products[$Item['id']]['price_retail'] * 0.0275 +
                                ($Products[$Item['id']]['price_retail'] * $currentPartialMonthesPercent / 100));
                        } else  {
                            $Products[$Item['id']]['price'] = round($Products[$Item['id']]['price_pm']);
                        }
                        $Products[$Item['id']]['old_price'] = round($Products[$Item['id']]['old_price'] +
                            $Products[$Item['id']]['old_price'] * 0.0275 +
                            ($Products[$Item['id']]['old_price'] * $currentPartialMonthesPercent / 100));
                    }
                }

                $Products[$Item['id']]['currency']  = $Agent->params['currency'];
                $Products[$Item['id']]['catalogs']  = Site_Db_Function::pg_array_parse($Item['catalogs']);
                $Products[$Item['id']]['images']    = [];
                if(isset($Agent->params['sold_phrases']) && $Agent->params['sold_phrases']){
                    $Products[$Item['id']]['sold_phrases'] = $Item['sold_phrases_hotline']?$Item['sold_phrases_hotline']:Zend_Registry::getInstance()->spelling['sold_phrases_hotline'];
                }

                if ( !empty($Item['id_guarantee']) ) {
                    $Products[$Item['id']]['Guarantee'] = $Guarantee[$Item['id_guarantee']];
                }

                if ( !empty($Item['id_state']) ) {
                    $Products[$Item['id']]['State'] = $State[$Item['id_state']];
                }

                $Img = new Products_Image($Item['id']);
                if ( $Img->getImagesCount() > 0 ) {
                    foreach ($Img->getImagesList() as $Imgitem) {
                        $Products[$Item['id']]['images'][] = $Imgitem['original']['url'];
                    }
                }
            }


            if ( 'price.xlsx' == $Agent->params['template'] ) {
                $content = (new Products_Price($Products, array( 'showGroup'=>1 )))->getPrice($Agent->params['price_type']);
            } elseif ('privat.csv' == $Agent->params['template']) {
                $Agent->params['template'] = 'privat.xlsx';
                $content = (new Products_Privat($Products, $Agent, array( 'showGroup'=>1 )))->getPrice();
            } else {
                $tpl = $this->tpl_dir . $Agent->params['template'];
                $config = $this->config;
                $content = call_user_func(function() use($tpl, $Products, $Agent, $config){
                    ob_start();
                    require($tpl);
                    return ob_get_clean();
                });
            }

            $this->saveToFile($Agent->getFilePath(), $content);

            $Agent->result_count  = count($Products);
            $Agent->time_generate = 'NOW()';

            $Agent->save();
        }
    }

    private function saveToFile($filename, $content)
    {
        if ( 'application/xml' == (new finfo(FILEINFO_MIME_TYPE))->buffer($content) ) {
            $content = preg_replace('~\s*(<([^>]*)>[^<]*</\2>|<[^>]*>)\s*~','$1',$content);
            $XML = simplexml_load_string($content);
            $XML->asXML($filename);
        } else {
            file_put_contents($filename, $content);
        }
    }

    private function getOldPrice($Item, $Agent)
    {
        $stickers = Site_Db_Function::pg_array_parse($Item['stickers']);

        $issetMarkdownSticker = ProductsStickersHelper::getInstance()->existsInStack('alias', 'markdown', $stickers);
        $issetSaleSticker = ProductsStickersHelper::getInstance()->existsInStack('alias', 'sale', $stickers);

        if ($issetMarkdownSticker || $issetSaleSticker) {
            if (empty($Item['markdown_percent'])) {
                $Item['markdown_percent'] = 25;
            }
        } else {
            $Item['markdown_percent'] = 0;
        }

        if (!empty($Item['markdown_percent'])) {
            $price = $this->getPrice($Item, $Agent);
            $price = $price / (1 - $Item['markdown_percent'] / 100);
            $price = Currency::round($price, $Agent->params['currency']);

            return $price;
        }

        return null;
    }

    private function getPrice($Item, $Agent)
    {
        $price = $Item['price_'.$Agent->params['price_type']];
        if (!empty($Agent->params['increase_percent'])) {
            $increase = 1 + $Agent->params['increase_percent'] / 100;
            $price *= $increase;
        }

        $price = Currency::conv(
            $price,
            Currency::getById($Item['id_currency'])->iso,
            $Agent->params['currency']
        );

        return $price;
    }

    private function convertPriceRetail($Item, $Agent)
    {
        $price = $Item['price_retail'];

        $price = Currency::conv(
            $price,
            Currency::getById($Item['id_currency'])->iso,
            $Agent->params['currency']
        );

        return $price;
    }

}
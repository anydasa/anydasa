<?php

require_once 'Traits/Crawler.php';

class Products_Links
{
    use Crawler;

    protected $Product;

    function __construct(Products $Product)
    {
        $this->Product = $Product;
    }

    public function updatePrices()
    {
        Doctrine_Query::create()->delete('ProductsLinksPrices')->where('id_product = ?', $this->Product->id)->execute();

        foreach ($this->parsePrices() as $price) {
            $Price = new ProductsLinksPrices;
            $Price->fromArray($price);
            $Price->save();
        }

        $this->Product->last_update_link_prices = 'NOW()';
        $this->Product->save();
    }

    public function getPrices()
    {
        return $this->Product->LinksPrices->count() ?
            SF::multisort($this->Product->LinksPrices->toArray(), 'price_uah') :
            array();
    }

    public function getCompanyPosition($company_name)
    {
        $i = 0;

        foreach ($this->getPrices() as $price) {
            $i++;
            if ( false !== strstr($price['company_name'], $company_name) ) {
                return $i;
            }
        }

        return false;
    }

    public function getParams()
    {
        $return_array = array();

        foreach ($this->Product->Links as $Link) {
            if ( $class = self::getClassByUrl($Link->url) ) {
                /** @var Products_Links_Interface $instance */
                $instance = new $class($Link->url);

                if (empty($Link->html)) {
                    $html = $this->getHTML($Link->url);
                } else {
                    $html = $Link->html;
                }

                $return_array [$Link->url]= $instance->getParams($html);
            }
        }

        return $return_array;
    }

    public function parsePrices()
    {
        $return_array = array();

        foreach ($this->Product->Links as $Link) {
            if ( $class = self::getClassByUrl($Link->url) ) {
                $html = $this->getHTML($Link->url, false);
                $Link->last_time_check = new Doctrine_Expression('NOW()');
                $Link->save();

                foreach ((new $class($Link->url))->getPrices($html) as $price) {
                    $item = [
                        'id_link'=>$Link->id,
                        'id_product'=>$this->Product->id
                    ];
                    $return_array []= array_merge($price, $item);
                }
            }
        }

        return SF::multisort($return_array, 'price_uah');
    }

    protected static function getClassByUrl($url)
    {
        $class = 'Products_Links_Domain_'.
            implode('',
                array_map(
                    function($word) { return ucfirst($word); },
                    explode('.', preg_replace('/^www\./', '', parse_url($url)['host']))
                )
            );
        return @class_exists($class) ? $class : false;
    }

    public function parseImages()
    {
        $return_array = array();

        foreach ($this->Product->ProductsLinks as $Link) {
            if ( $class = self::getClassByUrl($Link->url) ) {
                /** @var Products_Links_Interface $instance */
                $instance = new $class($Link->url);

                if (empty($Link->html)) {
                    $html = $this->getHTML($Link->url);
                } else {
                    $html = $Link->html;
                }

                foreach ( $instance->getImages($html) as $img_url) {
                    $img_url = parse_url($img_url);
                    if ( empty($img_url['host']) ) {
                        $img_url['host']   = parse_url($Link->url)['host'];
                        $img_url['scheme'] = parse_url($Link->url)['scheme'];
                    }
                    $return_array []= SF::build_url($img_url);
                }
            }
        }

        return $return_array;
    }


}
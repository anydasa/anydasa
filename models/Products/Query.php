<?

class Products_Query extends NoteQueryAbstract
{
    private $_isOnlyPublic = false;

    public function groupBySeries()
    {
        $this->addSelect('first_value(prg.id_product) OVER (
                PARTITION BY prg.id_series
                ORDER BY
                    CASE WHEN status IN(1,2,3) THEN 1+p.sort ELSE 0 END DESC,
                    CASE p.status WHEN 1 THEN 1 END,
                    CASE p.status WHEN 2 THEN 3 END,
                    CASE p.status WHEN 3 THEN 2 END,
                    prg.sort
            ) first_in_group');

        $this->addSelect('count(prg.id_product) OVER (PARTITION BY prg.id_series) count_in_series');

        $this->leftJoin("series_products prg ON p.id = prg.id_product");

        return $this;
    }

    public function setParamsQuery(array $params = array())
    {
        if (! empty($params['period'])) {
            $this->addWhere("p.created > NOW() - INTERVAL '".(int)$params['period']." day'");
        }
        if (! empty($params['links_prices_period'])) {
            $this->andWhere("p.last_update_link_prices < NOW() - INTERVAL '".(int)$params['links_prices_period']." day' OR p.last_update_link_prices IS NULL");
        }
        if (! empty($params['id'])) {
            if ('array' == gettype($params['id']))
                $this->whereIn('p.id', $params['id']);
            else
                $this->andWhereIn('p.id', array_map('trim', preg_split('/\n/',$params['id'])));
        }

        if (! empty($params['exclude_id_group'])) {
            $this->andWhere('p.id NOT IN (SELECT id_product FROM products_ref_groups WHERE id_group = ?)', $params['exclude_id_group']);
        }

        //-----------------------------------------------
        if (! empty($params['id_group_type'])) {
            $id_group_type = is_array($params['id_group_type']) ? $params['id_group_type'] : [$params['id_group_type']];
            $this->andWhere('p.id IN (SELECT id_product FROM products_ref_groups WHERE id_type IN ('.implode(',', $id_group_type).'))');
        }
        if (! empty($params['notid_group_type'])) {
            $id_group_type = is_array($params['notid_group_type']) ? $params['notid_group_type'] : [$params['notid_group_type']];
            $this->andWhere('p.id NOT IN (SELECT id_product FROM products_ref_groups WHERE id_type IN ('.implode(',', $id_group_type).'))');
        }
        //-----------------------------------------------

        if ( !empty($params['excludeid']) ) {
            $excludeid = is_array($params['excludeid']) ? $params['excludeid'] : array_map('trim', preg_split('/\n/',$params['excludeid']));
            $this->andWhereNotIn('p.id', $excludeid);
        }

        if ( array_key_exists('id_accessory', $params) ) {
            $this->andWhere('p.id_accessory = ?', $params['id_accessory']);
        }

        if ( array_key_exists('id_accessory_ref', $params) ) {
            $this->andWhere('p.id IN (SELECT id_product FROM products_ref_accessories WHERE id_accessory = ?)', $params['id_accessory_ref']);
        }

        if ( array_key_exists('is_pricing_hotline', $params) ) {
            if ( $params['is_pricing_hotline'] ) {
                $this->addWhere("EXISTS(SELECT 1 FROM products_pricing_hotline WHERE id_product = p.id AND is_enabled = true)");
            } else {
                $this->addWhere("NOT EXISTS(SELECT 1 FROM products_pricing_hotline WHERE id_product = p.id AND is_enabled = false)");
            }
        }

        if ( array_key_exists('vendors_product_more_than', $params) ) {
            $this->addWhere("? < (SELECT COUNT(*) FROM vendors_products WHERE id_product = p.id)", $params['vendors_product_more_than']);
        }

        if ( array_key_exists('in_adwords', $params) ) {
            if ( $params['in_adwords'] ) {
                $this->addWhere("EXISTS(SELECT 1 FROM adwords_adgroup WHERE id_product = p.id)");
            } else {
                $this->addWhere("NOT EXISTS(SELECT 1 FROM adwords_adgroup WHERE id_product = p.id)");
            }
        }

        if ( array_key_exists('id_set', $params) ) {
            if ( is_array($params['id_set']) ) {
                $this->andWhereIn('p.id_set', $params['id_set']);
            } elseif ('null' == $params['id_set']) {
                $this->andWhere('p.id_set IS NULL');
            } else {
                $this->andWhere('p.id_set = ?', $params['id_set']);
            }
        }

        if ( !empty($params['has_others_links']) ) {
            $this->addWhere("description ~ 'http[s]{0,1}://(?!pix.anydasa.com|anydasa.com|youtube.com|www.youtube.com)'");
        }

        if ( array_key_exists('has_promotion', $params) ) {
            if ( $params['has_promotion'] ) {
                $this->addWhere('p.id_promotion IS NOT NULL');
            } else {
                $this->addWhere('p.id_promotion IS NULL');
            }
        }

        if ( array_key_exists('is_empty_price', $params) ) {
            if ( $params['is_empty_price'] ) {
                $this->addWhere('p.price_retail = 0');
            } else {
                $this->addWhere('p.price_retail > 0');
            }
        }

        if (! empty($params['id_promotion'])) {
            $this->andWhere('p.id_promotion = ?', $params['id_promotion']);
        }

        if (! empty($params['id_guarantee'])) {
            if ('null' == $params['id_guarantee']) {
                $this->andWhere('p.id_guarantee IS NULL');
            } else {
                $this->andWhere('p.id_guarantee = ?', $params['id_guarantee']);
            }
        }

        if (@is_numeric($params['discount'])) {
            $this->andWhere('p.discount = ?', $params['discount']);
        }
        if (@is_numeric($params['is_link_checked'])) {
            $this->andWhere('p.is_link_checked = ?', $params['is_link_checked']);
        }
        if (! empty($params['k'])) {
            $this->setKey($params['k']);
            if ( empty($params['sort']) ) {
                $this->order('rank', 'DESC');
            }
        }
        if (! empty($params['orkey'])) {
            $this->setKey($params['orkey'], 'or');
            if ( empty($params['sort']) ) {
                $this->order('rank', 'DESC');
            }
        }
        if (! empty($params['title'])) {
            $this->addWhere("p.title ilike ? ", '%'.$params['title'].'%');
        }
        if (! empty($params['alias'])) {
            $this->addWhere("p.alias ilike ? ", '%'.$params['alias'].'%');
        }

        if ( @is_numeric($params['is_img']) ) {
            if ( 1 == $params['is_img'] ) {
                $this->andWhereIn('p.id', Products_Image::getAllProducts());
            } else {
                $this->andWhereNotIn('p.id', Products_Image::getAllProducts());
            }
        }

        if ( @is_numeric($params['has_very_small_orig_img']) ) {
            if ( 1 == $params['has_very_small_orig_img'] ) {
                $this->andWhereIn('p.id', Products_Image::getAllProducts('very_small'));
            } else {
                $this->andWhereNotIn('p.id', Products_Image::getAllProducts('very_small'));
            }
        }

        if (! empty($params['first_by_action']) ) {
            $this->addOrderBy('id_promotion NULLS LAST');
        }

        if (! empty($params['first_by_status']) ) {
            $this->addOrderBy('CASE status WHEN '.$params['first_by_status'].' THEN 1 END');
        }

        if ( ! empty($params['img_has_dead']) ) {
            $dead = Products_Image::getAllProducts('has_dead');
            if ( empty($dead) ) {
                $dead = array(-1);
            }
            $this->andWhereIn('p.id', $dead );
        }

        if ( ! empty($params['has_bad_price']) ) {
            $this->andWhere('
                p.price_trade   > p.price_retail   OR
                p.price_partner > p.price_trade    OR
                p.price_min     > p.price_partner  OR
                p.price_min     = 0 OR
                p.price_partner = 0 OR
                p.price_trade   = 0 OR
                p.price_retail  = 0
            ');
        }

        if ( @is_numeric($params['has_series']) ) {
            if ( $params['has_series'] ) {
                $this->addWhere('EXISTS (SELECT id FROM series_products WHERE id_product = p.id)');
            } else {
                $this->addWhere('NOT EXISTS (SELECT id FROM series_products WHERE id_product = p.id)');
            }
        }

        if ( @is_numeric($params['has_groups']) ) {
            if ( $params['has_groups'] ) {
                $this->addWhere('EXISTS (SELECT id FROM products_ref_groups WHERE id_product = p.id)');
            } else {
                $this->addWhere('NOT EXISTS (SELECT id FROM products_ref_groups WHERE id_product = p.id)');
            }
        }

        if ( @is_numeric($params['has_set']) ) {
            if ( $params['has_set'] ) {
                $this->addWhere('p.id_set IS NOT NULL');
            } else {
                $this->addWhere('p.id_set IS NULL');
            }
        }

        if ( @is_numeric($params['has_accessories']) ) {
            if ( $params['has_accessories'] ) {
                $this->addWhere('EXISTS (SELECT id FROM products_ref_accessories WHERE id_product = p.id)');
            } else {
                $this->addWhere('NOT EXISTS (SELECT id FROM products_ref_accessories WHERE id_product = p.id)');
            }
        }

        if ( !empty($params['id_state']) ) {
            if ( 'null' == $params['id_state'] ) {
                $this->addWhere('p.id_state IS NULL');
            } else {
                $this->addWhere('p.id_state = ?', $params['id_state']);
            }
        }

        if ( !empty($params['states']) ) {
            $states = is_array($params['states']) ? $params['states'] : [(int)$params['states']];
            $this->andWhereIn('p.id_state', $states);
        }

        if ( @is_numeric($params['has_vendor_products']) ) {
            if ( $params['has_vendor_products'] ) {
                $this->addWhere('EXISTS (SELECT id FROM vendors_products WHERE id_product = p.id)');
            } else {
                $this->addWhere('NOT EXISTS (SELECT id FROM vendors_products WHERE id_product = p.id)');
            }
        }

        if ( @is_numeric($params['has_catalog']) ) {
            if ( $params['has_catalog'] ) {
                $this->addWhere('EXISTS (SELECT id FROM products_ref_catalog WHERE id_product = p.id)');
            } else {
                $this->addWhere('NOT EXISTS (SELECT id FROM products_ref_catalog WHERE id_product = p.id)');
            }
        }


        if ( @is_numeric($params['filled_require_option']) ) {
            $s = 'SELECT pop.* FROM products_option pop
                  WHERE pop.id_catalog = ANY(p.catalogs)
                    AND pop.is_require = true
                    AND pop.id NOT IN (
                        SELECT pro.id_option FROM products_ref_option pro
                        WHERE pro.id_product = p.id AND pro.values IS NOT NULL
                    )';
            if ( $params['filled_require_option'] ) {
                $this->addWhere('NOT EXISTS ('.$s.')');
            } else {
                $this->addWhere('EXISTS ('.$s.')');
            }
        }

        if ( !empty($params['option_param']) && !empty($params['option_value']) ) {
            $this->addWhere('p.id IN (
                SELECT id_product
                FROM products_option, products_ref_option
                WHERE id = id_option
                  AND title ILIKE ?
                  AND array_to_string(values, \', \') ~ ?
            )', ['%'.$params['option_param'].'%', $params['option_value']]);
        } elseif ( !empty($params['option_param']) ) {
            $this->addWhere('p.id IN (
                SELECT id_product
                FROM products_option, products_ref_option
                WHERE id = id_option
                  AND title ILIKE ?
            )', '%'.$params['option_param'].'%');
        } elseif ( !empty($params['option_value']) ) {
            $this->addWhere('p.id IN (
                SELECT id_product
                FROM products_ref_option
                WHERE array_to_string(values, \', \') ~ ?
            )', $params['option_value']);
        }

        if ( ! empty($params['id_option']) ) {
            $id_option = is_array($params['id_option']) ? $params['id_option'] : [(int)$params['id_option']];
            $this->addWhere('p.id IN (SELECT id_product FROM products_ref_option WHERE id_option IN ('.implode(',', $id_option).'))');
        }

        if ( ! empty($params['img_has_dublicate']) ) {
            $duplicate = Products_Image::getAllProducts('has_duplicate');
            if ( empty($duplicate) ) {
                $duplicate = array(-1);
            }
            $this->andWhereIn('p.id', $duplicate );
        }

        if (! empty($params['code'])) {
            $this->andWhereIn('p.code', array_map('trim', explode(' ',$params['code'])));
        }
        if (! empty($params['sort'])) {
            $sort = explode('-', $params['sort']);
            $this->order($sort[0], @$sort[1]);
        }

        if (@is_numeric($params['balance_from'])) {
            $this->andWhere('p.balance >= ?', $params['balance_from']);
        }
        if (@is_numeric($params['balance_to'])) {
            $this->andWhere('p.balance <= ?', $params['balance_to']);
        }

        if (@is_numeric($params['expected_balance_from'])) {
            $this->andWhere('p.expected_balance >= ?', $params['expected_balance_from']);
        }
        if (@is_numeric($params['expected_balance_to'])) {
            $this->andWhere('p.expected_balance <= ?', $params['expected_balance_to']);
        }


        if (!empty($params['is_sale'])) {
            $this->andWhere('p.discount > 0');
        }
        if (@is_numeric($params['is_visible'])) {
            $this->andWhere('p.is_visible = ?', $params['is_visible']);
        }
        if (@is_numeric($params['is_spy_prices'])) {
            if ( $params['is_spy_prices'] ) {
                $this->andWhere('p.id IN (SELECT id_product FROM products_links WHERE interval_update_price IS NOT NULL)');
            } else {
                $this->andWhere('p.id IN (SELECT id_product FROM products_links WHERE interval_update_price IS NULL)');
            }
        }


        if ( !empty($params['brand-alias']) ) {
            $this->andWhere('p.id_brand = (SELECT id FROM products_brands WHERE alias = ?)', $params['brand-alias']);
        }

        if ( !empty($params['id_vendor']) ) {
            $this->andWhere('p.id IN (SELECT id_product FROM vendors_products WHERE id_vendor IN ('.implode(',', $params['id_vendor']).'))');
        }

        if ( !empty($params['notid_vendor']) ) {
            $this->andWhere('p.id NOT IN (SELECT id_product FROM vendors_products WHERE id_vendor IN ('.implode(',', $params['notid_vendor']).') AND id_product IS NOT NULL)');
        }

        if ( @is_numeric($params['has_margin']) ) {
            if ( $params['has_margin'] ) {
                $this->andWhere('p.id IN (SELECT id_product FROM products_margins)');
            } else {
                $this->andWhere('p.id NOT IN (SELECT id_product FROM products_margins)');
            }
        }

        if ( @is_numeric($params['has_vendor_owner']) ) {
            if ( $params['has_vendor_owner'] ) {
                $this->andWhere('p.id_vendor_product IS NOT NULL');
            } else {
                $this->andWhere('p.id_vendor_product IS NULL');
            }
        }

        if ( @is_numeric($params['has_links']) ) {
            if ( $params['has_links'] ) {
                $this->andWhere('p.id IN (SELECT id_product FROM products_links)');
            } else {
                $this->andWhere('p.id NOT IN (SELECT id_product FROM products_links)');
            }
        }

        if ( !empty($params['id_vendor_owner']) ) {
            $owner = is_array($params['id_vendor_owner']) ? $params['id_vendor_owner'] : [(int)$params['id_vendor_owner']];
            $this->andWhere('p.id_vendor_product IN (SELECT id FROM vendors_products WHERE id_vendor = ANY(?))', '{'.implode(',', $owner).'}');
        }

        if ( !empty($params['notid_vendor_owner']) ) {
            $owner = is_array($params['notid_vendor_owner']) ? $params['notid_vendor_owner'] : [(int)$params['notid_vendor_owner']];
            $this->andWhere('p.id_vendor_product NOT IN (SELECT id FROM vendors_products WHERE id_vendor = ANY(?))', '{'.implode(',', $owner).'}');
        }

        if ( !empty($params['id_brand']) ) {
            $brand = is_array($params['id_brand']) ? $params['id_brand'] : [(int)$params['id_brand']];
            $this->whereIn('p.id_brand', $brand);
        }

        if ( array_key_exists('min', $params) ) {
            $this->addWhere("ROUND(p.price_retail / (SELECT rate FROM currency WHERE id = p.id_currency) * (SELECT rate FROM currency WHERE iso = 'UAH')) >= ?", intval($params['min']));
        }
        if ( array_key_exists('max', $params) ) {
            $this->addWhere("ROUND(p.price_retail / (SELECT rate FROM currency WHERE id = p.id_currency)* (SELECT rate FROM currency WHERE iso = 'UAH')) <= ?", intval($params['max']));
        }


        if ( !empty($params['menu_alias']) ) {
            $this->setMenu($params['menu_alias'], $params);
        }


        /* ----------------------------------------------------------------------------- */
        if ( !empty($params['filter_params']) ) {
            $params['filter'] = Products_Filter_Url::parsePath($params['filter_params']);
        }

        if ( !empty($params['filter']['stickers']) ) {
            if ( is_string($params['filter']['stickers']) ) {
                $params['filter']['stickers'] = [$params['filter']['stickers']];
            }
            $this->andWhere('p.stickers @> (SELECT array_agg(id)::INTEGER[] FROM products_stickers WHERE alias = ANY(\'{' . implode(',', $params['filter']['stickers']) . '}\'::TEXT[]))');
        }
        if ( !empty($params['filter']['brands']) ) {

            $f = $params['filter']['brands'];
            array_walk($f, function (& $item) {
                $item = Doctrine_Manager::connection()->quote($item);
            });

            $this->andWhere('p.id_brand = ANY (SELECT id FROM products_brands WHERE alias = ANY(ARRAY[' . implode(',', $f) . ']::TEXT[]))');
        }

        if ( !empty($params['filter']['filters']) ) {

            $sql =   'SELECT t1.id_group, \'p.filters && ARRAY[\' || array_to_string(array_agg(t1.id), \',\') || \']\'
                      FROM catalog_filter_value t1
                      WHERE t1.alias = ANY(:filters)
                        AND t1.id_group IN (SELECT id FROM catalog_filter_group WHERE id_catalog = :id_catalog)
                      GROUP BY t1.id_group';

            $stmt = Doctrine_Manager::connection()->prepare($sql);
            $stmt->bindValue(':filters', '{' . implode(',', $params['filter']['filters']) . '}');
            $stmt->bindValue(':id_catalog', $params['id_catalog']);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
            if ( !empty($res) ) {
                $this->andWhere(implode(' AND ', $res));
            }
        }
        /* ----------------------------------------------------------------------------- */

        if ( isset($params['status']) and ('' !== $params['status']) ) {
            $status = is_array($params['status']) ? $params['status'] : [(int)$params['status']];
            $this->andWhereIn('p.status', $status);
        }

        if ( !empty($params['id_catalog']) ) {
            $catalogs = is_array($params['id_catalog']) ? $params['id_catalog'] : [(int)$params['id_catalog']];
            $this->andWhere('p.catalogs && ARRAY['.implode(',', $catalogs).']');
        }

        if ( !empty($params['notid_sticker']) ) {
            $stickers = is_array($params['notid_sticker']) ? $params['notid_sticker'] : [(int)$params['notid_sticker']];
            $this->andWhere("p.stickers @@ '!".implode('&!', $stickers). "'");
        }

        if ( !empty($params['notid_catalog']) ) {
            $catalogs = is_array($params['notid_catalog']) ? $params['notid_catalog'] : [(int)$params['notid_catalog']];
            $this->andWhere('NOT(p.catalogs && ARRAY['.implode(',', $catalogs).'])');
        }

        return $this;
    }

    public function count($params = array())
    {

        $key_cache = 'productsResultCount_' . md5($this->getCountSqlQuery() . http_build_query($this->getCountQueryParams($params)));

        if ( !$return = Site_Cache::load($key_cache) ) {

            $return = parent::count($params);

            Site_Cache::save($return, $key_cache);
        }
        return $return;
    }

    public function getCountSqlQuery($params = array())
    {
        $q = $this->getSqlQueryWithoutLimit($params);
        return "SELECT COUNT( * ) as num_results FROM ($q) num_results";
    }

    /**
     * @param array $params
     * @param null $hydrationMode
     * @return array|Doctrine_Collection
     * @throws Doctrine_Collection_Exception
     * @throws Doctrine_Exception
     */
    public function execute($params = array(), $hydrationMode = null)
    {
        if ( empty($this->_dqlParts['orderby']) ) {
            $this->setOrderDefault();
        }
        $this->addOrderBy('p.id DESC'); //добавляем чтоб небыло беспорядочной сортировки (особенность postgres)

        $params = array_merge($this->getFlattenedParams(), $params);


        $key_cache = 'productsResult_' . md5($this->_prepareSql(). http_build_query($params));

        if ( !$return = Site_Cache::load($key_cache) ) {
            if (null === $hydrationMode) {
                $hydrationMode = Doctrine_Core::HYDRATE_RECORD;
            }

            $result = $this->_conn->execute($this->_prepareSql(), $params);

            if (Doctrine_Core::HYDRATE_RECORD == $hydrationMode) {
                $return = new Doctrine_Collection('Products');
                foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $row) {
                    $v = new Products;
                    $v->hydrate($row);
                    $v->assignIdentifier(true);
                    $return->add($v);
                }
            } elseif(Doctrine_Core::HYDRATE_ARRAY == $hydrationMode) {
                $return = $result->fetchAll();
            } else {
                throw new Doctrine_Exception("Unknown hydrate mode.");
            }

            Site_Cache::save($return, $key_cache);
        }

        return $return;
    }

    static function create()
    {
        $Obj = new self();
        $Obj->select('p.*');
        $Obj->from('products p');
        return $Obj;
    }

    public function setOrderDefault()
    {
        $params = array();
        $params['first_by_status'] = Products::STATUS_AVAILABLE;
        $params['sort'] = 'title';
        $this->setParamsQuery($params);
    }

    public function setKey($k, $type = '')
    {
        //$this->_isHeadline = true;
        if (mb_check_encoding($k, 'Windows-1251') && !mb_check_encoding($k, 'UTF-8')) {
            $k = mb_convert_encoding($k, 'UTF-8', 'Windows-1251');
        }

        $this->addSelect('k')->addWhere('p.fts @@@ k');
        $this->addSelect("ts_headline(title, k) as title");

        if ( 'or' == $type ) {
            $this->addFrom("plainto_or_tsquery('russian', translate(strip_spec_chars(" . $this->_conn->quote($k) . "),'ё','е')) k");
        } else {
            $this->addFrom("plainto_tsquery('russian', translate(strip_spec_chars(" . $this->_conn->quote($k) . "),'ё','е')) k");
        }

        return $this;
    }

    public function setOnlyPublic()
    {
        $this->_isOnlyPublic = true;
        $this->andWhere('p.is_visible = ?', 1);
        $this->andWhere("p.stickers @@ (SELECT ('!' || id)::query_int FROM COALESCE((SELECT id fROM products_stickers WHERE alias = 'hidden'), 0) as id)");


        return $this;
    }

    public function setOnlyAvailableForOrder()
    {
        $this->andWhere('p.status <> ?', 0);

        return $this;
    }

    public function setMenu($alias, & $params)
    {
        if ( $Menu = Doctrine::getTable('CatalogMenu')->findOneByAlias($alias) ) {
            if ( $Menu->has_params ) {
                $params['id_catalog'] = $Menu->id_catalog;
                $this->addWhere($this->getSqlPartMenuAgent($Menu));
            } else {
                $Query = Doctrine_Query::create()
                    ->from('CatalogMenu')
                    ->where('lft > ? AND rgt < ? AND has_params = 1', [$Menu->lft, $Menu->rgt]);
                $filters_conditions = [];

                foreach ($Query->execute() as $Menu) {
                    $filters_conditions []= $this->getSqlPartMenuAgent($Menu);
                }
                $filters_conditions = array_filter($filters_conditions);

                if ( empty($filters_conditions) ) {
                    $this->addWhere('p.id = 0');
                } else {
                    $this->addWhere('('.implode(') OR (', $filters_conditions).')');
                }
            }
        } else {
            $this->addWhere('p.id = 0');
        }

        return $this;
    }

    private function getSqlPartMenuAgent($Menu)
    {
        $params_conditions = [];

        if ( !empty($Menu->filters) ) {
            $sql =   'SELECT id_group, \'p.filters && ARRAY[\' || array_to_string(array_agg(id), \',\') || \']\'
                                      FROM catalog_filter_value
                                      WHERE id = ANY(:filters)
                                      GROUP BY id_group';

            $stmt = Doctrine_Manager::connection()->prepare($sql);
            $stmt->bindValue(':filters', '{' . implode(',', $Menu->filters) . '}');
            $stmt->execute();
            $params_conditions []= implode(' AND ', $stmt->fetchAll(PDO::FETCH_KEY_PAIR));
        }

        if ( !empty($Menu->stickers) ) {
            $params_conditions []= 'p.stickers @> \'{' . implode(',', $Menu->stickers) . '}\'::INTEGER[]';
        }

        if ( !empty($Menu->brands) ) {
            $params_conditions []= 'p.id_brand = ANY(\'{' . implode(',', $Menu->brands) . '}\'::INTEGER[])';
        }

        if ( !empty($Menu->id_catalog) ) {
            $params_conditions []= $Menu->id_catalog . ' = ANY (p.catalogs)';
        }

        return implode(' AND ', $params_conditions);
    }

    public function first($param)
    {
        switch ($param) {
            case 'comments':
                $this->addOrderBy('CASE WHEN (p.comments_count > 0) THEN 1 ELSE 2 END');
                break;
            case 'promotions':
                $this->leftJoin(" promotions prom ON (p.id_promotion = prom.id)");
                $this->addOrderBy('CASE WHEN (prom.is_enabled = 1) THEN 1 ELSE 2 END');
                break;
            default:
        }
    }

    public function order($field, $type = 'DESC')
    {
        $type = (strtoupper($type) == 'DESC') ? 'DESC' : 'ASC';

        switch ($field) {
            case 'rank':
                if ( preg_grep('/^\S+tsquery/i', $this->_dqlParts['from']) ) {
                    $this->addSelect("ts_rank(p.fts, k, 32) as rank");
                    $this->addOrderBy('rank '.$type);
                }
                break;
            case 'id':
                $this->addOrderBy('p.id '.$type);
                break;
            case 'created':
                $this->addOrderBy('p.created '.$type.' NULLS LAST');
                break;
            case 'updated':
                $this->addOrderBy('p.updated '.$type.' NULLS LAST');
                break;
            case 'time':
                $this->addOrderBy('GREATEST(p.created, p.updated) '.$type);
                break;
            case 'sort':
                $this->addOrderBy('p.sort '.$type);
                break;
            case 'brand':
                $this->addOrderBy('p.id_brand '.$type);
                break;
            case 'title':
                $this->addOrderBy('p.title '.$type);
                break;
            case 'articul':
                $this->addOrderBy('p.articul '.$type);
                break;
            case 'rating':
                $this->addOrderBy('p.rating '.$type);
                break;
            case 'balance':
                $this->addOrderBy('p.balance '.$type);
                break;
            case 'expected_balance':
                $this->addOrderBy('p.expected_balance '.$type);
                break;
            case 'price':
                $this->addOrderBy('price_retail / (SELECT rate FROM currency WHERE id = id_currency) '.$type);
                break;
            case 'code':
                $this->addOrderBy('p.code '.$type);
                break;
            case 'is_new':
                $this->addOrderBy('p.is_new '.$type);
                break;
            case 'type':
                $this->addOrderBy('p.type '.$type);
                break;
            case 'comments':
                $this->addOrderBy('p.comments_count '.$type);
                break;

            default:
                break;
        }

        return $this;
    }



    public function getDqlPart($queryPart)
    {
        if ( ! isset($this->_dqlParts[$queryPart])) {
            throw new Doctrine_Query_Exception('Unknown query part ' . $queryPart);
        }

        return $this->_dqlParts[$queryPart];
    }

    public function getSqlQuery($params = array())
    {
        $q = $this->getSqlQueryWithoutLimit($params);
        $q .= ( ! empty($this->_dqlParts['limit']))?   ' LIMIT ' . implode(' ', $this->_dqlParts['limit']) : '';
        $q .= ( ! empty($this->_dqlParts['offset']))?  ' OFFSET ' . implode(' ', $this->_dqlParts['offset']) : '';

        return $q;
    }

    protected function getSqlQueryWithoutLimit($params = array())
    {
        $this->_params['exec'] = $params;
        $this->_execParams = $this->getFlattenedParams();
        $this->fixArrayParameterValues($this->_execParams);

        $q = 'SELECT ' . $this->_buildSqlSelectPart();
        $q .= ' FROM ' . $this->_buildSqlFromPart();

        $this->_processWherePart();

        $q .= ( ! empty($this->_dqlParts['where']))?   ' WHERE '    . implode(' ', $this->_dqlParts['where']) : '';
        $q .= ( ! empty($this->_dqlParts['having']))?  ' HAVING '   . implode(' AND ', $this->_dqlParts['having']) : '';
        $q .= ( ! empty($this->_dqlParts['groupby']))? ' GROUP BY ' . implode(', ', $this->_dqlParts['groupby']) : '';
        $q .= ( ! empty($this->_dqlParts['orderby']))? ' ORDER BY ' . implode(', ', $this->_dqlParts['orderby']) : '';

        if ( preg_match('/first_in_group/is', $q) ) {
            $q = "SELECT * FROM ($q) sub  WHERE first_in_group IS NULL OR id = first_in_group";
        }

        return $q;
    }
}

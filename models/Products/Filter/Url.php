<?

class Products_Filter_Url
{
    const SEPARATOR_FILTER_CATEGORY = '~';
    const SEPARATOR_FILTER_ITEM = '_';

    private $params = [
        'brands'    => [],
        'filters'   => [],
        'stickers'  => [],
        'price'     => []
    ];

    public static function parsePath($path)
    {
        return (new Products_Filter_Url)
            ->setPath($path)
            ->getParams();
    }

    public static function buildPath($params)
    {
        return (new Products_Filter_Url)
            ->setParams($params)
            ->getPath();
    }

    private function _sort()
    {
        $this->params = array_map(function ($v) { sort($v); return $v; }, $this->params);
        return $this;
    }



    public function getParams()
    {
        return $this->params;
    }

    public function setParams($params)
    {
        $this->params = $params;

        $this->_sort();

        return $this;
    }

    public function setPath($path)
    {
        list (
            $this->params['brands'],
            $this->params['filters'],
            $this->params['stickers'],
            $this->params['price']
        ) = explode(self::SEPARATOR_FILTER_CATEGORY, $path);

        foreach ($this->params as $key => $param) {
            $this->params[$key] = array_filter(explode(self::SEPARATOR_FILTER_ITEM, $param));
        }

        $this->_sort();

        return $this;
    }

    public function getPath()
    {
        $href = '';

        if ( !empty($this->params['brands']) ) {
            $href .= implode(self::SEPARATOR_FILTER_ITEM, $this->params['brands']);
        }
        $href .= self::SEPARATOR_FILTER_CATEGORY;

        if ( !empty($this->params['filters']) ) {
            $href .= implode(self::SEPARATOR_FILTER_ITEM, $this->params['filters']);
        }
        $href .= self::SEPARATOR_FILTER_CATEGORY;

        if ( !empty($this->params['stickers']) ) {
            $href .= implode(self::SEPARATOR_FILTER_ITEM, $this->params['stickers']);
        }
        $href .= self::SEPARATOR_FILTER_CATEGORY;

        if ( !empty($this->params['prices']) ) {
            $href .= implode(self::SEPARATOR_FILTER_ITEM, $this->params['prices']);
        }
        $href .= self::SEPARATOR_FILTER_CATEGORY;

        return rtrim($href, self::SEPARATOR_FILTER_CATEGORY);
    }

    public function clearPart($part)
    {
        $this->params[$part] = [];

        return $this;
    }

    public function togglePartParam($part, $value, $context = null)
    {

        if ('brands' == $part) {
            if ( false !== ($key = array_search($value, $this->params['brands'])) ) {
                unset($this->params['brands'][$key]);
            } else {
                $this->params['brands'][] = $value;
            }
        }

        if ('stickers' == $part) {
            if ( false !== ($key = array_search($value,  $this->params['stickers'])) ) {
                unset( $this->params['stickers'][$key]);
            } else {
                $this->params['stickers'][] = $value;
            }
        }

        if ('filters' == $part) {
            if ( false !== ($key = array_search($value,  $this->params['filters'])) ) {
                unset( $this->params['filters'][$key]);
            } else {
                $this->params['filters'][] = $value;

                $ids = array_column($context['filters']['Values'], 'id', 'alias');
                $filter_id = $ids[$value];
                $current_filter_value = $context['filters']['Values'][$filter_id];

                if ( !empty($current_filter_value['children']) ) {
                    $children = $current_filter_value['children'];

                    $aliases = array_column($context['filters']['Values'], 'id', 'alias');

                    $removable = array_flip(array_filter($aliases, function($item) use ($children) {
                        return in_array($item, $children);
                    }));

                    $this->params['filters'] = array_diff($this->params['filters'], $removable);
                }

                if ( !empty( $current_filter_value['id_parent'] ) ) {
                    $this->params['filters'] = array_diff($this->params['filters'], array($context['filters']['Values'][$current_filter_value['id_parent']]['alias']));
                }
            }



        }

        $this->_sort();

        return $this;
    }
}
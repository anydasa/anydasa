<?

class Products_Links_Domain_FUa implements Products_Links_Interface
{
    public function getImages($html)
    {
        $return = [];

        preg_match_all('/"(https:\/\/f.ua\/statik\/images\/products\/400.*?)"/is', $html, $res);

        foreach ($res[1] as $img) {
            $return [] = str_replace('/400/', '/original/', $img);
        }

        return $return;
    }

    public function getPrices($html)
    {
        $return_array = array();

        return SF::iconvArray('windows-1251', 'UTF-8', $return_array);
    }

    public function getParams($html)
    {
        $return_array = array();

        if ( preg_match('/<div class="clear properties tab_div">(.*?)<ul class="property_show_all"/is', $html, $res) ) {

            if ( preg_match_all('/(.*?<\/table>)/is', $res[1], $res2) ) {
                foreach ($res2[1] as $group_item) {
                    $group = '';

                    if ( preg_match('/<span>(.*?)<\/span><i><\/i>/', $group_item, $group_res) ) {
                        $group = trim($group_res[1]);
                    }

                    preg_match_all('/(<tr.*?tr>)/is', $group_item, $param_res);
                    foreach ($param_res[1] as $item) {
                        preg_match('/(<td class="name".*?<\/td>)/is', $item, $title);
                        preg_match('/(<td class="value".*?<\/td>)/is', $item, $value);

                        $return_array[] = array(
                            'group' => $group,
                            'param' => trim(strip_tags($title[1])),
                            'value' => trim(strip_tags($value[1])),
                        );
                    }
                }
            }
        }




        return SF::iconvArray('windows-1251', 'UTF-8', $return_array);
    }
}
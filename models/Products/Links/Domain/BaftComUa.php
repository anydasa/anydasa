<?

class Products_Links_Domain_BaftComUa implements Products_Links_Interface
{
    public function getImages($html)
    {
        if (preg_match_all('/data-fancybox="gallery" href="(.*?)"/is', $html, $res2)) {
            return $res2[1];
        }

        return [];
    }

    public function getPrices($html)
    {
        // TODO: Implement getPrices() method.
    }

    public function getParams($html)
    {
        $return = [];

        if ( preg_match('/<tbody class="characteristics__body">(.*?)<\/tbody>/is', $html, $res) ) {
            if (preg_match_all('/<tr class="characteristics__row">(.*?)<\/tr>/is', $res[1], $res2)) {
                foreach ($res2[1] as $group_item) {
                    preg_match_all('/(<td class="characteristics__column".*?<\/td>)/is', $group_item, $column);

                    $return[] = array(
                        'group' => '',
                        'param' => trim(strip_tags($column[1][0])),
                        'value' => trim(strip_tags($column[1][1])),
                    );
                }
            }
        }

        return $return;
    }
}

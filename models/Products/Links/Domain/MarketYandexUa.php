<?

class Products_Links_Domain_MarketYandexUa implements Products_Links_Interface
{
    public function getImages($html)
    {
        $return = [];
        if ( preg_match_all('/product-card-gallery__thumbs-item.*?(mdata.yandex.net\/i\?path=.*?\.jpeg)/is', $html, $res) ) {
            foreach ($res[1] as $img) {
                $return []= 'http://'.$img;
            }

        }
        return $return;
    }

    public function getPrices($html)
    {
        $return_array = array();

        if ( preg_match('/<div.*?product-card__spec.*?href="(.*?)".*?<\/div>/is', $html, $res) ) {
            $html = file_get_contents($res[1]);

            if ( preg_match_all('/tx-price[\s|b]+tx-price.*?box(.*?)<\/div>\s*<\/div>\s*<\/div>/is', $html, $res) ) {
                foreach ($res[1] as $id=>$item) {
                    preg_match('/price fr.*?orng.*?>(.*?)<\/a>/is', $item, $price_uah);
                    preg_match('/price fr.*?usd.*?>(.*?)<\/a>/is', $item, $price_usd);
                    preg_match('/eventStoreName\': \'(.*?)\'/is', $item, $CName);
                    preg_match('/<p class="descr">\s*<a.*?>(.*?)<\/a>/is', $item, $PName);

                    $return_array []= array(
                        'company'      => $CName[1],
                        'product_name' => trim($PName[1]),
                        'price_uah'    => round(SF::tofloat($price_uah[1]), 2),
                        'price_usd'    => round(SF::tofloat($price_usd[1]), 2),
                    );
                }
            }
        }
        return SF::iconvArray('windows-1251', 'UTF-8', $return_array);
    }

    public function getParams($html)
    {
        $return_array = array();

        if ( preg_match('/<div.*?product-card__spec.*?href="(.*?)".*?<\/div>/is', $html, $res) ) {
            $html = file_get_contents( 'http://market.yandex.ua'.$res[1] );

            if (preg_match_all('/(<div.*?product-spec__body.*?<\/div>)/is', $html, $res2)) {
                foreach ($res2[1] as $group_item) {
                    $group = '';

                    if (preg_match('/(<h2(.*?)h2>)/', $group_item, $group_res)) {
                        $group = trim(strip_tags($group_res[1]));
                    }

                    preg_match_all('/(<dl.*?dl>)/is', $group_item, $param_res);
                    foreach ($param_res[1] as $item) {
                        preg_match('/(<dt.*?dt>)/is', $item, $title);
                        preg_match('/(<dd.*?dd>)/is', $item, $value);

                        $return_array[] = array(
                            'group' => $group,
                            'param' => trim(strip_tags(preg_replace('<div.*div>', '', $title[1]))),
                            'value' => trim(strip_tags($value[1])),
                        );
                    }
                }
            }
        }

        return $return_array;
    }
}
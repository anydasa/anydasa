<?

class Products_Links_Domain_AmazonCom implements Products_Links_Interface
{
    public function getImages($html)
    {
        $return = [];
            if ( preg_match_all('/hiRes":"(.*?)"/is', $html, $res) ) {
            $return = array_unique($res[1]);
        }

        return $return;
    }

    public function getPrices($html)
    {
        return [];
    }

    public function getParams($html)
    {
        return [];
    }
}
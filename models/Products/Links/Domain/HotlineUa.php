<?

class Products_Links_Domain_HotlineUa implements Products_Links_Interface
{
    public function getImages($html)
    {
        preg_match_all('/data-src-canvas="(https.+?\.jpg)"/is', $html, $res);
        return $res[1];
    }

    public function getPrices($html)
    {
        $return_array = array();

        if ( preg_match('/<link rel="canonical" href="(.*?)" \/>/is', $html, $res) ) {

            preg_match('/<meta.*?name="csrf-token".*?content="(.*?)"/is', $html, $token);
            $csrf_token = $token[1];


            $options = array(
                'http' => array(
                    'header' => "X-Requested-With: XMLHttpRequest\r\nReferer: " . $res[1] . "\r\n" .
                                "X-CSRF-Token: $csrf_token"
                )
            );

            $html = $this->getHtml($res[1].'?tab=2', 0, false, $options);


            $html = iconv('windows-1251', 'UTF-8', $html);
            $html = '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'.$html;

            $dom = new Zend_Dom_Query($html);

            foreach ($dom->query('.tx-price-line') as $rowNode) {

                $html = $rowNode->ownerDocument->saveHTML($rowNode);
                $html = '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'.$html;
                $rowDom = new Zend_Dom_Query($html);

                $return_array []= array(
                    'src'          => 'hotilne',
                    'company_name' => $rowDom->query('h3')->current()->textContent,
                    'product_name' => $rowDom->query('.descr i')->current()->textContent,
                    'price_uah'    => round(SF::tofloat($rowDom->query('.price .orng')->current()->textContent), 2),
                    'price_usd'    => round(SF::tofloat($rowDom->query('.price .usd')->current()->textContent), 2),
                );
            }
        }

        return $return_array;
    }

    public function getParams($html)
    {
        $return_array = array();

        if ( preg_match_all('/<table.*?seo-table*.?>(.*?)<\/table/is', $html, $res) ) {
            $dom = new PHPHtmlParser\Dom();
            $dom->loadStr($res[1][1]);

            $group = '';
            foreach ($dom->find('tr') as $item) {

                if ($item->find('.title-section')->count()) {
                    $group = trim($item->text(true));
                    continue;
                }

                $cells = $item->find('td');

                $return_array[] = array(
                    'group' => $group,
                    'param' => trim($cells[0]->text()),
                    'value' => trim(strip_tags($cells[1])),
                );
            }
        }

        return $return_array;
    }
}

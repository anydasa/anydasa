<?

require_once 'Traits/Crawler.php';

class Products_Links_Domain_EkUa implements Products_Links_Interface
{
    use Crawler;

    public function getImages($html)
    {
        $return = [];

        if ( preg_match_all('/\$\.prettyPhoto\.open_pg\(\'(http:\/\/mzimg\.com\/big.*?)\'/is', $html, $res) ) {
            $return = $res[1];
        }
        if ( preg_match('/\$\.prettyPhoto\.open_pg\(\'(\/jpg_zoom1.*?)\'/is', $html, $res2) ) {
            array_push($return, 'http://ek.ua'. $res2[1]);
        }

        return $return;
    }

    public function getPrices($html)
    {
        $return_array = array();

        return SF::iconvArray('windows-1251', 'UTF-8', $return_array);
    }

    public function getParams($html)
    {
        $html = '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'.$html;
        $dom = new Zend_Dom_Query($html);
        $res = $dom->query('#item-bookmarks a');
        $res->next();

        if ( preg_match('/"(\/ek-item\.php\?resolved_name_=.*?&view_=tbl)"/is', $html, $res)) {
            $html = $this->getHtml('http://ek.ua' . $res[1]);
        }

        $html = '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'.$html;

        $return_array = array();

        $dom = new Zend_Dom_Query($html);

        $group = '';
        foreach ($dom->query('table#help_table table tr') as $rowNode) {

            $html = $rowNode->ownerDocument->saveHTML($rowNode);



            $html = '<meta http-equiv="content-type" content="text/html; charset=utf-8" />'.$html;
            $rowDom = new Zend_Dom_Query($html);

            if ( $gr = $rowDom->query('.op1-title span')->current()->textContent ) {
                $group = trim($gr);
                continue;
            }

            $param = $rowDom->query('td.op1 span')->current()->textContent;
            $value = $rowDom->query('td.op3')->current()->textContent;

            if ( $rowDom->query('.descr-color .ib')->current() ) {
                $color = $rowDom->query('.descr-color .ib')->current()->getAttribute('title');
                $param = 'Цвет';
                $value = $color;
            }

            if ( !empty($param) && !empty($value) ) {
                $return_array[] = array(
                    'group' => $group,
                    'param' => $param,
                    'value' => $value,
                );
            }

        }


        return $return_array;
    }
}
<?

interface Products_Links_Interface
{
    public function getImages($html);
    public function getPrices($html);
    public function getParams($html);
}
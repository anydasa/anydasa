<?

class Products_Filter
{
    private $_Catalog;

    private $_data = NULL;

    function __construct(Catalog $Catalog)
    {
        $this->_Catalog = $Catalog;
        $this->loadData();
    }

    protected function loadData()
    {
        $this->_data['brands']   = $this->getBrands();
        $this->_data['stickers'] = $this->getStickers();
        $this->_data['filters']  = $this->getFilters();
        $this->_data['indexes']  = $this->getIndexes();

        return $this;
    }

    public function getAllData()
    {
        return $this->_data;
    }

    public function getData($part)
    {
        return $this->_data[$part];
    }

    public function getIndexPathByAlias($alias)
    {
        $Array = array_column($this->_data['indexes']['Values'], 'alias', 'path');
        return array_search($alias, $Array);
    }

    public function getIndexAliasByPath($path)
    {
        $Array = array_column($this->_data['indexes']['Values'], 'path', 'alias');
        return array_search($path, $Array);
    }

    public function markSelected($path)
    {

        $params = Products_Filter_Url::parsePath($path);

        foreach ($this->_data['brands']['Values'] as $key => $item) {
            $item['is_selected'] = in_array($item['alias'], $params['brands']);
            if ($item['is_selected']) {
                $this->_data['brands']['selected'][$item['id']] = $item['alias'];
            }
            $this->_data['brands']['Values'][$key] = $item;
        }

        foreach ($this->_data['stickers']['Values'] as $key => $item) {
            $item['is_selected'] = in_array($item['alias'], $params['stickers']);
            if ($item['is_selected']) {
                $this->_data['stickers']['selected'][$item['id']] = $item['alias'];
            }
            $this->_data['stickers']['Values'][$key] = $item;
        }



        foreach ($this->_data['filters']['Groups'] as $key => $item) {
            $item['selected'] = [];
            foreach($item['CatalogFilterValue'] as $key2 => $item2) {
                $item2['is_selected'] = in_array($item2['alias'], $params['filters']);
                if ($item2['is_selected']) {
                    $item['selected'][$item2['id']] = $item2['alias'];
                    $this->_data['filters']['selected'][$item2['id']] = $item2['alias'];
                }
                $item['CatalogFilterValue'][$key2] = $item2;
            }
            $this->_data['filters']['Groups'][$key] = $item;
        }

        foreach ($this->_data['indexes']['Values'] as $item) {
            if ( $item['path'] == $path ) {
                $this->_data['indexes']['selected'] = $item;
            }
        }

    }

    public function getIndexes()
    {
        $return = [
            'count_products' => 0,
            'selected' => []
        ];

        $return['Values'] = Doctrine_Query::create()
            ->from('CatalogFilterIndex t1 INDEXBY id')
            ->where('id_catalog = ?', $this->_Catalog->id)
            ->execute()
            ->toArray()
        ;

        foreach ($return['Values'] as $id => & $item) {
            $params = array_intersect_key($item, array_flip(['brands', 'filters', 'stickers']));
            if ( false === ($path = $this->getPathParamsByIds($params)) ) {
                unset($return['Values'][$id]);
            } else {
                $item['path'] = $path;
            }
        }
        return $return;
    }

    protected function getPathParamsByIds($ids)
    {
        $return = [];

        $stickers_values = array_column($this->_data['stickers']['Values'], 'alias', 'id');
        $filters_values  = array_column($this->_data['filters']['Values'], 'alias', 'id');
        $brands_values   = array_column($this->_data['brands']['Values'], 'alias', 'id');

        $return['brands']   = array_intersect_key($brands_values, array_flip($ids['brands']));
        $return['filters']  = array_intersect_key($filters_values, array_flip($ids['filters']));
        $return['stickers'] = array_intersect_key($stickers_values, array_flip($ids['stickers']));

        if ( count($ids, COUNT_RECURSIVE) !== count($return, COUNT_RECURSIVE) ) {
            return false;
        }

        return Products_Filter_Url::buildPath($return);
    }

    public function getBrands()
    {
        $return = [
            'count_products' => 0,
            'selected' => []
        ];

        $return['Values'] = Doctrine_Query::create()
            ->select('t1.id, t1.alias, t1.title')
            ->from('ProductsBrands t1 INDEXBY id')
            ->where('t1.id IN (SELECT t2.id_brand FROM Products t2 WHERE ? = ANY(catalogs) AND is_visible = 1)', $this->_Catalog->id)
            ->orderBy('title')
            ->execute()
            ->toArray();

        return $return;
    }

    protected function getStickers()
    {
        $return = [
            'count_products' => 0,
            'selected' => []
        ];

        $return['Values'] = Doctrine_Query::create()
            ->select('t1.id, t1.alias, t1.title')
            ->from('ProductsStickers t1 INDEXBY id')
            ->where('t1.id IN (SELECT unnest(stickers) FROM Products t2 WHERE ? = ANY(catalogs) AND is_visible = 1)', $this->_Catalog->id)
            ->addWhere('t1.is_show_in_filter = true')
            ->orderBy('title')
            ->execute()
            ->toArray();

        return $return;
    }

    protected function getFilters()
    {
        $return = [
            'count_products' => 0,
            'selected' => []
        ];

        $return['Groups'] = Doctrine_Query::create()
            ->from('CatalogFilterGroup t1 INDEXBY id')
            ->innerJoin('t1.CatalogFilterValue t2 INDEXBY id')
            ->where('t1.id_catalog = ?', $this->_Catalog->id)
            ->orderBy('t1.sort, t2.sort')
            ->execute()
            ->toArray();

        foreach ($return['Groups'] as $gkey=>$Group) {
            foreach ($Group['CatalogFilterValue'] as $id=>$item) { // Устанавливаем детей для значений
                if ( empty($Group['CatalogFilterValue'][$id]['children']) ) {
                    $Group['CatalogFilterValue'][$id]['children'] = array();
                }
                if ( !empty($item['id_parent']) ) {
                    $Group['CatalogFilterValue'][$item['id_parent']]['children'] []= $id;
                }
            }
            $return['Groups'][$gkey] = $Group;
        }

        $Values = [];
        foreach ($return['Groups'] as $Group) {
            $Values += $Group['CatalogFilterValue'];
        }

        $return['Values'] = $Values;

        return $return;
    }

    public function populateCounters($requestParams)
    {
        $this->populateFilterCounters($requestParams);
        $this->populateBrandCounters($requestParams);
        $this->populateStickersCounters($requestParams);
        $this->populatePrices($requestParams);
    }

    protected function populateFilterCounters($requestParams)
    {
        $count = $this->getFilterCounters($requestParams);

        foreach($this->_data['filters']['Groups'] as & $Group) {
            $Group['count_products'] = 0;

            if ( count($Group['selected']) ) {

                $item_filter = Products_Filter_Url::parsePath($requestParams['filter_params']);

                foreach ($Group['CatalogFilterValue'] as $itemValue) {
                    $item_filter['filters'] = array_diff($item_filter['filters'], [$itemValue['alias']]);
                }
                $item_requestParams = $requestParams;
                $item_requestParams['filter_params'] = Products_Filter_Url::buildPath($item_filter);

                //Вычисляем и оставляем кол-во данной группы
                $item_count = array_intersect_key($this->getFilterCounters($item_requestParams), $Group['CatalogFilterValue']);
                $count = $count + $item_count;
            }



            foreach($Group['CatalogFilterValue'] as & $Value) {
                if ( isset($count[$Value['id']]) ) {
                    $Value['count_products']         = $count[$Value['id']];
                    $Group['count_products']        += $Value['count_products'];
                } else {
                    $Value['count_products'] = 0;
                }
            }
        }
    }

    private function getFilterCounters($requestParams)
    {
        $Query = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery($requestParams)
            ->select('unnest(p.filters), count(*)')
            ->addGroupBy('unnest(p.filters)')
            ->removeDqlQueryPart('orderby');

        return $this->fetchKeyPair($Query);;
    }

    protected function populateBrandCounters($requestParams)
    {
        $requestParams['filter_params'] = (new Products_Filter_Url)->setPath($requestParams['filter_params'])->clearPart('brands')->getPath();

        $Query = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery($requestParams)
            ->select('p.id_brand, count(*)')
            ->addGroupBy('id_brand')
            ->removeDqlQueryPart('orderby');

        $count = $this->fetchKeyPair($Query);

        $this->_data['brands']['count_products'] = 0;
        foreach ($this->_data['brands']['Values'] as & $item) {
            $item['count_products'] = @(integer) $count[$item['id']];
            $this->_data['brands']['count_products'] += $item['count_products'];
        }

        return $this;
    }

    protected function populateStickersCounters($requestParams)
    {
        $requestParams['filter_params'] = (new Products_Filter_Url)->setPath($requestParams['filter_params'])->clearPart('stickers')->getPath();

        $Query = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery($requestParams)
            ->select('unnest(p.stickers), count(*)')
            ->addGroupBy('unnest(p.stickers)')
            ->removeDqlQueryPart('orderby');

        $count = $this->fetchKeyPair($Query);

        $this->_data['stickers']['count_products'] = 0;
        foreach ($this->_data['stickers']['Values'] as & $item) {
            $item['count_products'] = @(integer) $count[$item['id']];
            $this->_data['stickers']['count_products'] += $item['count_products'];
        }

        return $this;
    }

    protected function populatePrices($requestParams)
    {
        $this->_data['prices']['selected']['min'] = $requestParams['min'];
        $this->_data['prices']['selected']['max'] = $requestParams['max'];

        unset($requestParams['min']);
        unset($requestParams['max']);

        $Query = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery($requestParams)
            ->select('MIN(price_retail / (SELECT rate FROM currency WHERE id = p.id_currency) * (SELECT rate FROM currency WHERE iso = \'UAH\'))')
            ->addSelect('MAX(price_retail / (SELECT rate FROM currency WHERE id = p.id_currency) * (SELECT rate FROM currency WHERE iso = \'UAH\'))')
            ->addWhere('price_retail > 0')
            ->removeDqlQueryPart('orderby');

        $count = $this->fetchKeyPair($Query);

        $this->_data['prices']['Values']['min'] = Currency::round(@reset(array_keys($count)), 'UAH') ;
        $this->_data['prices']['Values']['max'] = Currency::round(@reset($count), 'UAH');

        if ( NULL === $this->_data['prices']['selected']['min'] ) {
            $this->_data['prices']['selected']['min'] = $this->_data['prices']['Values']['min'];
        }
        if ( NULL === $this->_data['prices']['selected']['max'] ) {
            $this->_data['prices']['selected']['max'] = $this->_data['prices']['Values']['max'];
        }

        return $this;
    }

    private function fetchKeyPair($Query)
    {
        $stmt = Doctrine_Manager::connection()->prepare($Query->getSqlQuery());
        $mainQueryParams = $Query->getParams();

        foreach ($mainQueryParams['where'] as $id=>$item) {
            $stmt->bindValue($id+1, $item);
        }
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
    }
}

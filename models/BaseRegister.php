<?php

/**
 * BaseRegister
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property string $key
 * @property string $value
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseRegister extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('register');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'register_id',
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Название',
             'primary' => false,
             ));
        $this->hasColumn('key', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Ключ',
             'primary' => false,
             ));
        $this->hasColumn('value', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Значение',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}
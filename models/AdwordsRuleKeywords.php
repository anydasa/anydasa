<?php


class AdwordsRuleKeywords extends BaseAdwordsRuleKeywords
{

    public static function generateFromText($text, $params)
    {
        if ( false !== array_search('remove_cyrillic', (array)$params['options']) ) {
            $text = preg_replace('/[а-я]/ui', '', $text);
        }
        if ( false !== array_search('remove_brackets', (array)$params['options']) ) {
            $text = preg_replace('/\(.+?\)/ui', '', $text);
        }


        $remove = explode("\n", $params['keys_remove']);
        $remove = (array) array_filter(array_map('trim', $remove));

        $remove += ['!', '@', '%', ',', '*', '(', ')', '-', '"', '+'];

        $matches = array_map(function ($item) {
            return '/' . addcslashes(preg_quote($item), '/') . '/uis';
        }, $remove);

        $text = preg_replace($matches, ' ', $text);

        $text = preg_replace('/\s+/', ' ', $text);
        $text = trim($text);

        $base_words = parse_words($text);

        $prepend = explode("\n", $params['keys_prepend']);
        $prepend = (array) array_filter(array_map('trim', $prepend));
        $append = explode("\n", $params['keys_append']);
        $append = (array) array_filter(array_map('trim', $append));
        $negative = explode("\n", $params['keys_negative']);
        $negative = (array) array_filter(array_map('trim', $negative));

        $biddable = [];
        $i = 0;

        do {

            if ( false !== array_search('include_without_additives', (array)$params['options']) ) {
                $item_key_array  = $base_words;
                $item_key_string = implode(' ', $base_words);
                if ( count($item_key_array) <= 10 && strlen($item_key_string) <= 80 ) {
                    $biddable[] = $item_key_string;
                }
            }


            foreach ($prepend as $prepend_item) {
                $item_key_array  = array_merge(parse_words($prepend_item), $base_words);
                $item_key_string = implode(' ', $item_key_array);
                if ( count($item_key_array) <= 10 && strlen($item_key_string) <= 80 ) {
                    $biddable[] = $item_key_string;
                }
            }

            foreach ($append as $append_item) {
                $item_key_array  = array_merge($base_words, parse_words($append_item));
                $item_key_string = implode(' ', $item_key_array);
                if ( count($item_key_array) <= 10 && strlen($item_key_string) <= 80 ) {
                    $biddable[] = $item_key_string;
                }
            }

            if ( !boolval($params['is_iterative_decrease']) ) {
                break;
            }
            if ( count($base_words) <= intval($params['iterative_min_word']) ) {
                break;
            }
            if ( '' !== strval($params['iterative_count']) ) {
                if ( $i++ >= $params['iterative_count']) {
                    break;
                }
            }
            array_pop($base_words);

        } while (count($base_words));


        $biddable = array_map(function ($item) {
            return [
                'text'          => $item,
                'criterion_use' => 'BIDDABLE',
                'match_type'    => 'PHRASE',
            ];
        }, $biddable);

        $negative = array_map(function ($item) {
            return [
                'text'          => $item,
                'criterion_use' => 'NEGATIVE',
                'match_type'    => 'EXACT',
            ];
        }, $negative);

        return $biddable + $negative;
    }

}
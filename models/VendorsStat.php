<?php

/**
 * VendorsStat
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class VendorsStat extends BaseVendorsStat
{

    public function preDelete($event)
    {
        @unlink($this->getFilePath());
    }

    public static function getPathDir()
    {
        return Zend_Registry::get('config')->path->pix  . 'files/vendors/price_stat/';
    }

    public static function getUrlDir()
    {
        return Zend_Registry::get('config')->url->pix  . 'files/vendors/price_stat/';
    }

    public function getLastTime()
    {
        return Doctrine_Query::create()->from('VendorsStat')->where('id_vendor = ?', $this->id)->orderBy('time DESC')->fetchOne()->time;
    }

    public static function addStat($file, Vendors $Vendor, Site_PriceImport_Separator $Separator)
    {
        $Stat = new VendorsStat;

        $Stat->id_vendor        = $Vendor->id;
        $Stat->price_config     = $Vendor->price_config;
        $Stat->time_processed   = $Vendor->last_time_processed;

        $Stat->count_invalid_rows  = $Separator->getGarbage()->getCount();
        $Stat->count_valid_rows    = $Separator->getValues()->getCount();
        $Stat->count_inserted_rows = count($Separator->getValues()->getInserted());
        $Stat->count_updated_rows  = count($Separator->getValues()->getUpdated());
        $Stat->count_saved_rows    = count($Separator->getValues()->getSaved());
        $Stat->count_notsaved_rows = count($Separator->getValues()->getNotSaved());
        $Stat->fileext = SF::FileExt($file);

        $Stat->save();
        $Stat->refresh();

        copy($file, $Stat::getPathDir() . $Stat->id . $Stat->fileext);

        return $Stat;
    }

    public function getFilePath()
    {
        if ( is_file(self::getPathDir() . $this->id . $this->fileext) ) {
            return self::getPathDir() . $this->id . $this->fileext;
        }
        return false;
    }

    public function getFileUrl()
    {
        if ( is_file(self::getPathDir() . $this->id . $this->fileext) ) {
            return self::getUrlDir() . $this->id . $this->fileext;
        }
        return false;
    }
}
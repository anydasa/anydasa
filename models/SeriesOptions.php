<?php

/**
 * @property ProductsOption $ProductsOption
 */
class SeriesOptions extends BaseSeriesOptions
{
    public const DISPLAY_TYPES = [
        self::DISPLAY_TYPE_PRODUCT_THUMP => 'Миниатюра товара',
        self::DISPLAY_TYPE_PRODUCT_OPTION => 'Текст параметра',
    ];

    const DISPLAY_TYPE_PRODUCT_THUMP = 1;
    const DISPLAY_TYPE_PRODUCT_OPTION = 2;

    public function setUp()
    {
        parent::setUp();

        $this->hasOne('ProductsOption', array(
                'local'    => 'id_product_option',
                'foreign'  => 'id',
            )
        );
    }

    public function getDisplayTypeString()
    {
        return self::DISPLAY_TYPES[$this->display_type];
    }
}

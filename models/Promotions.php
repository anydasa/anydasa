<?php

/**
 * Promotions
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Promotions extends BasePromotions
{
    public function setUp()
    {
        parent::setUp();

        $this->hasOne('PromotionsTypes as Type', array(
            'local' => 'id_type',
            'foreign' => 'id'));

        $this->hasOne('PromotionsCategories as Category', array(
            'local' => 'id_category',
            'foreign' => 'id'));

        $this->hasMany('Banners as Banners', array(
                'refClass' => 'BannersRefPromotion',
                'local'    => 'promotion_id',
                'foreign'  => 'banner_id',
            )
        );
    }

    public function getImageUrl()
    {
        return Zend_Registry::get('config')->url->pix  . 'img/promotions/' . $this->id . '?'.time();
    }
    public function getImagePath()
    {
        return Zend_Registry::get('config')->path->pix  . 'img/promotions/' . $this->id;
    }

    public function delete(Doctrine_Connection $conn = NULL)
    {
        parent::delete($conn);
        unlink($this->getImagePath());
    }

    public function getRemainingDays()
    {
        $time_off = new DateTime($this->time_off);
        $today    = new DateTime();

        $days = (int)$today->diff($time_off, false)->format('%r%a') + 1;

        return $days;
    }


    public function getPeriodStr()
    {
        $time_on = new DateTime($this->time_on);
        $time_off = new DateTime($this->time_off);

        $str = $time_on->format('j') . ' ' . Site_Months::getGenitiveByNumber($time_on->format('n'));
        $str .= ' - ' .$time_off->format('j') . ' ' . Site_Months::getGenitiveByNumber($time_off->format('n'));

        return $str;
    }

    static public function getList()
    {

        return Doctrine_Query::create()
            ->from('Promotions')
            ->execute();
    }
}
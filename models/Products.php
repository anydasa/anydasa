<?php

/**
 * @property Doctrine_Collection|ProductsOption[] $Options
 * @property Doctrine_Collection|Series[] $Series
 * @property Doctrine_Collection|Catalog[] $Catalogs
 */
class Products extends BaseProducts
{
    const STATUS_NOT_AVAILABLE   = 0;
    const STATUS_AVAILABLE       = 1;
    const STATUS_WAIT            = 2;
    const STATUS_ORDER           = 3;
    const STATUS_VENDOR          = 3;

    const PRICE_TYPE_RETAIL  = 'retail';
    const PRICE_TYPE_TRADE   = 'trade';
    const PRICE_TYPE_MIN     = 'min';
    const PRICE_TYPE_PARTNER = 'partner';

    public static $PRICE_TYPES = array(
        self::PRICE_TYPE_RETAIL  => 'РРЦ',
        self::PRICE_TYPE_TRADE   => 'Оптавая',
        self::PRICE_TYPE_PARTNER => 'Партнерская',
        self::PRICE_TYPE_MIN     => 'Минимальная',
    );

    private static $_status = array(
        self::STATUS_NOT_AVAILABLE  => 'Нет в наличии',
        self::STATUS_AVAILABLE      => 'В наличии',
        self::STATUS_WAIT           => 'Ожидается',
        self::STATUS_ORDER          => 'На складе',
    );


    private $_Image = null;

    public function setUp()
    {
        parent::setUp();

        $this->hasOne('Series', array(
                'refClass' => 'SeriesProducts',
                'local'    => 'id_product',
                'foreign'  => 'id_series',
            )
        );

        $this->hasMany('Catalog as Catalogs', array(
                'refClass' => 'ProductsRefCatalog',
                'local'    => 'id_product',
                'foreign'  => 'id_catalog',
            )
        );

        $this->hasMany('ProductsLinks as Links', array(
                'local'   => 'id',
                'foreign' => 'id_product',
                'orderBy' => 'url DESC'
            )
        );

        $this->hasMany('ProductsLinksPrices as LinksPrices', array(
                'refClass' => 'ProductsLinks',
                'local'   => 'id_product',
                'foreign' => 'id'
            )
        );

        $this->hasMany('ProductsComments as Comments', array(
                'local'    => 'id',
                'foreign'  => 'id_product',
                'orderBy'  => 'created ASC'
            )
        );

        $this->hasMany('OrdersProducts', array(
                'local'    => 'code',
                'foreign'  => 'code',
            )
        );

        $this->hasMany('ProductsOption as Options', array(
                'refClass' => 'ProductsRefOption',
                'local'    => 'id_product',
                'foreign'  => 'id_option',
                'orderBy'  => 'sort ASC',
            )
        );

        $this->hasMany('ProductsGroups as Groups', array(
                'refClass' => 'ProductsRefGroups',
                'local'    => 'id_product',
                'foreign'  => 'id_group',
            )
        );

        $this->hasMany('ProductsLists as List', array(
                'refClass' => 'ProductsListsRef',
                'local'    => 'id_product',
                'foreign'  => 'id_list',
            )
        );

        $this->hasMany('ProductsStickers as Stickers', array(
                'refClass' => 'ProductsRefStickers',
                'local'    => 'id_product',
                'foreign'  => 'id_sticker',
                'orderBy'  => 'title'
            )
        );

        $this->hasOne('ProductsBrands as Brand', array(
                'local'    => 'id_brand',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('VendorsProducts as OwnerProduct', array(
                'local'    => 'id_vendor_product',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('ProductsSets as Set', array(
                'local'    => 'id_set',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('ProductsAccessories as Accessory', array(
                'local'    => 'id_accessory',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('ProductsGuarantee as Guarantee', array(
                'local'    => 'id_guarantee',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('Promotions as Promotion', array(
            'local' => 'id_promotion',
            'foreign' => 'id'));

        $this->hasOne('ProductsMargins', array(
             'local' => 'id',
             'foreign' => 'id_product'));

        $this->hasOne('ProductsPricingHotline as PricingHotline', array(
            'local'   => 'id',
            'foreign' => 'id_product')
        );
    }

    public function getActualSpecialOffer()
    {
        if (empty($this->special_offer_expiry)) {
            return null;
        }

        $expiry = new \DateTime($this->special_offer_expiry);
        $now = new \DateTime();

        $isActual = $now->getTimestamp() <= $expiry->getTimestamp();

        if ($isActual && !empty($this->special_offer_text)) {
            return $this->special_offer_text;
        }

        return null;
    }

    public function getCatalogsId()
    {
        if ( preg_match('/^{(.*)}$/', $this->_data['catalogs'], $matches) && '' != $matches[1] ) {
            return str_getcsv($matches[1]);
        }
        return [];
    }

    public function getUrl()
    {
        $routes = new Zend_Controller_Router_Rewrite;
        $routes->addConfig( new Zend_Config_Ini(dirname(dirname(__FILE__)) . '/frontend/app/etc/routers/modules/product.ini', 'product'), 'routes');

        return REQUEST_SCHEME . '://anydasa.com.ua/' . ltrim($routes->getRoute('product')->assemble([
            'alias' => rawurlencode($this->alias),
            'id' => $this->id
        ], 'product'), '/');
    }

    public function getPreMarkdownPrice($currencyISO)
    {
        if ( empty($this->markdown_percent) ) {
            return false;
        }
        $price = $this->getPrice('retail', $currencyISO);

        $price = $price / (1 - $this->markdown_percent / 100);

        return new ProductPriceFormatter($price, $currencyISO);
    }

    public function getRatingValue()
    {
        $rating_votes = $this->getRatingVotes();

        if ( empty($rating_votes) ) {
            return 0;
        }

        return array_sum($rating_votes) / count($rating_votes);
    }

    public function getRatingCount()
    {
        return count($this->getRatingVotes());
    }

    public function getRatingVotes()
    {
        if ( preg_match('/^{(.*)}$/', $this->_data['rating_votes'], $matches) && '' != $matches[1] ) {
            return str_getcsv($matches[1]);
        }
        return [];
    }

    public function getStickerIds()
    {
        if ( preg_match('/^{(.*)}$/', $this->_data['stickers'], $matches) && '' != $matches[1] ) {
            return str_getcsv($matches[1]);
        }
        return [];
    }

    public static function getStatusList()
    {
        return self::$_status;
    }

    public function getStatusTitle()
    {
        return self::$_status[$this->status];
    }

    public function getProfit($sCurrencyIso)
    {
        if ( !$this->OwnerProduct->exists() ) {
            return false;
        }

        if ( 0 == $this->OwnerProduct->getPrice('buy', $sCurrencyIso) ) {
            return false;
        }

        return $this->getPrice('retail', $sCurrencyIso) - $this->OwnerProduct->getPrice('buy', $sCurrencyIso);

    }

    public function getGroupInfo($groupType = null)
    {
        $return = [];

        $sql = "SELECT
                      p.id as product_id,
                      pg.id as group_id,
                      pgt.title as group_title,
                      pgt.code as group_type_code,
                      pgt.is_show_image as group_type_is_show_image,
                      p.title as product_title,
                      p.alias as product_alias,
                      p.status as product_status,
                      prg.title as title
                FROM products p
                        JOIN products_ref_groups prg ON (p.id = prg.id_product)
                JOIN products_groups pg ON (pg.id = prg.id_group)
                JOIN products_groups_types pgt ON (pg.id_type = pgt.id)
                WHERE p.is_visible = 1 AND pgt.code != '" . ProductsGroupsTypes::PRODUCTS_GROUP_LINE . "'
                    AND pg.id IN (SELECT t.id_group FROM products_ref_groups t WHERE t.id_product = :id_product)
                ORDER BY pgt.sort, (substring(prg.title, '^[0-9]+'))::int, substring(prg.title, '[^0-9_].*$')";

        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->bindValue('id_product', $this->id);
        $stmt->execute();

        foreach ($stmt->fetchAll(Doctrine_Core::FETCH_ASSOC) as $item) {
            $Ref = (new ProductsRefGroups)
                ->set('id_group', $item['group_id'])
                ->set('id_product', $item['product_id']);

            $item['img_src'] = $Ref->issetImg() ? $Ref->getImgUrl() : (new Products_Image($item['product_id']))->getFirstImage()['small']['url'];
            $return[$item['group_title']] []= $item;
        }

        if ($groupType) {
            $return2 = [];
            foreach ($return as $item) {
                foreach ($item as $item2) {
                    if ( $item2['group_type_code'] == $groupType ) {
                        $return2 []= $item2;
                    }
                }
            }
            return $return2;
        }


        return $return;
    }

    public function getFullName()
    {
        $str = '';
        if ( @!stristr($this->title, $this->type) ) {
            $str = $this->type . ' ';
        }
        return trim($str . $this->title);
    }

    public function getTypedName()
    {
        return $this->getFullName();
    }

    public function getProps($separator = ', ', $is_prime = true)
    {
        $return = [];

        $Query = Doctrine_Query::create()
            ->select('t1.*, t2.*')
            ->from('ProductsOption t1')
            ->leftJoin('t1.ProductsRefOption t2')
            ->where('t2.id_product = ?', $this->id)
            ->orderBy('t1.sort');

        if ( $is_prime ) {
            $Query->addWhere('t1.is_prime = true');
        }


        foreach ($Query->execute() as $Param) {
            $temp = [
                'is_prime'      => $Param['is_prime'],
                'param'         => $Param['title'],
                'param_brief'   => $Param['title_brief'],
                'group'         => $Param['group_name'],
                'group_brief'   => $Param['group_name_brief'],
                'unit'          => $Param['unit'],
                'value'         => array()
            ];

            foreach ($Param['ProductsRefOption'] as $Value) {
                $temp['value'] = implode($separator, $Value['values']);
            }

            $return []= $temp;
        }

        return $return;
    }

    public function getUserPrice($user, $currencyISO)
    {
        $userPrice = !empty($user) ? str_replace('price_', '', $user->price) : 'trade';
        return $this->getPrice($userPrice, $currencyISO);
    }

    public function getFormatedUserPrice($user, $currencyISO)
    {
        $price = $this->getUserPrice($user, $currencyISO);
        if ( empty($price) ) {
            return 0;
        }
        return Currency::format($price, $currencyISO);
    }

    public function getPrice($priceType, $inCurrencyISO, $discount = 0)
    {
        if ( !array_key_exists('price_'.$priceType, $this->_data) ) {
            throw new Exception('price type is wrong');
        }

        $price = ($this->_data['price_'.$priceType] / Currency::getById($this->id_currency)->rate) * Currency::getByISO($inCurrencyISO)->rate;

        if ( $discount > 0 ) {
            $price -= $price * $discount / 100;
        }

        return round($price, Currency::getByISO($inCurrencyISO)->precision);
    }

    public function getFormatedPrice($priceType, $currencyISO = null, $strip_html = false)
    {
        if ( null === $currencyISO ) {
            $currencyISO = $this->Currency->iso;
        }
        $price = $this->getPrice($priceType, $currencyISO);
        if ( empty($price) ) {
            return 'Цену уточняйте';
        }
        return Currency::format($price, $currencyISO, $strip_html);
    }

    public function getFormatedDicsountPrice($priceType, $currencyISO = null, $discount, $strip_html = false)
    {
        if ( null === $currencyISO ) {
            $currencyISO = $this->Currency->iso;
        }
        $price = $this->getPrice($priceType, $currencyISO, $discount);
        if ( empty($price) ) {
            return 'Цену уточняйте';
        }
        return Currency::format($price, $currencyISO, $strip_html);
    }

    /*public function getSiblingsInGroup($group_type, bool $include_record = false)
    {
        $Query = Doctrine_Query::create()
            ->from('Products p')
            ->innerJoin('p.ProductsRefGroups prg')
            ->innerJoin('prg.ProductsGroups pg')
            ->where('pg.type = ?', $group_type);

        if ( !$include_record ) {
            $Query->addWhere('prg.id_product <> ?', $this->id);
        }

        return $Query->execute();
    }*/

    public function hasNotFilledRequireOption($id_catalog = NULL)
    {
        $sql = 'SELECT pop.* FROM products_option pop
                WHERE pop.id_catalog = ANY (:catalogs)
                  AND pop.is_require = true
                  AND pop.id NOT IN ( SELECT pro.id_option FROM products_ref_option pro
                                      WHERE pro.id_product = :id_product AND values IS NOT NULL
                  )';

        $stmt = Doctrine_Manager::connection()->prepare($sql);

        if ( NULL == $id_catalog ) {
            $stmt->bindValue('catalogs', $this->_data['catalogs']);
        } else {
            $stmt->bindValue('catalogs', '{'. ((int) $id_catalog) .'}');
        }

        $stmt->bindValue('id_product', $this->id);
        $stmt->execute();

        return (bool) $stmt->rowCount();
    }

    public function isAvailableForOrder()
    {
        return (
            $this->status == self::STATUS_AVAILABLE
            ||
            $this->status == self::STATUS_VENDOR
        );
    }

    public function isAvailableForCredit()
    {
        return $this->isAvailableForOrder() && $this->getPrice('retail', 'UAH');
    }

    public function getLink()
    {
        return new Products_Links($this);
    }

    public function isRetailFromVendor()
    {
        if ( !$this->id_vendor_product ) {
            return false;
        }

        return round($this->price_retail * Currency::getById($this->id_currency)->rate)
                ==
               round($this->OwnerProduct->price_retail * Currency::getById($this->OwnerProduct->id_currency)->rate);
    }

    /**
     * @return Products_Image
     *
     * @throws Exception
     */
    public function getImage(): Products_Image
    {
        if ( !$this->id ) {
            throw new Exception('Для работы с изображениями необходимо сохранить товар');
        }

        if ( is_null($this->_Image) ) {
            $this->_Image = new Products_Image($this->id);
        }

        return $this->_Image;
    }

    public function delete(Doctrine_Connection $conn = NULL)
    {
        parent::delete($conn);
        $this->getImage()->removeAllImages();
    }

    public function toArray($deep = true, $prefixKey = false)
    {
        $array = parent::toArray($deep, $prefixKey);
        //$array['option_values'] = $this->ProductsRefOption->toKeyValueArray('id_option', 'id_option');
        $array['Catalogs'] = $this->Catalogs->toKeyValueArray('id', 'id');
        $array['status_title'] = self::$_status[$this->status];
        $array['fullName'] = $this->getFullName();
        return $array;
    }

    public function hasSticker($alias)
    {
        return ProductsStickersHelper::getInstance()
            ->existsInStack('alias', $alias, $this->getStickerIds());
    }

    public function getOptionValueByOptionId($id): ?ProductsRefOption
    {
        /** @var ProductsRefOption  $ProductsRefOption */
        foreach ($this->ProductsRefOption as $ProductsRefOption) {
            if ($ProductsRefOption->id_option == $id) {
                return $ProductsRefOption;
            }
        }

        return null;
    }

    public function removeOptionValueByOptionId($id): bool
    {
        /** @var ProductsRefOption  $ProductsRefOption */
        foreach ($this->ProductsRefOption as $ProductsRefOption) {
            if ($ProductsRefOption->id_option == $id) {
                $ProductsRefOption->delete();
                return true;
            }
        }

        return false;
    }

    public function getSeriesMatrix(): array
    {
        if (!$this->Series->count()) {
            return [];
        }

        /** @var Catalog $Catalog */
        $Catalog = $this->Catalogs->get(0);

        /** @var Series $Series */
        $Series = $this->Series->get(0);
        $Series->refresh(true);

        $matrix = [];

        foreach($Series->SeriesProducts as $SeriesProducts) {
            $SeriesProducts->loadReference('Products');
            foreach ($Catalog->SeriesOptions as $SeriesOption) {
                $Value = $SeriesProducts->Products->getOptionValueByOptionId($SeriesOption->ProductsOption->id);
                if ($Value) {
                    $matrix[$SeriesProducts->Products->id][$SeriesOption->id] = $Value->getValues()[0];
                }
            }
        }

        $curr = [];

        foreach ($Catalog->SeriesOptions as $SeriesOption) {
            $Value = $this->getOptionValueByOptionId($SeriesOption->ProductsOption->id);
            if ($Value) {
                $curr[$SeriesOption->id] = $Value->getValues()[0];
            }
        }

        $return = [];

        foreach ($Catalog->SeriesOptions as $SeriesOption) {
            $i = $curr;
            unset($i[$SeriesOption->id]);
            foreach($matrix as $idProduct => $productOptions) {
                $return[$SeriesOption->id]['SeriesOption'] = $SeriesOption;
                unset($productOptions[$SeriesOption->id]);
                $intersect = array_intersect_assoc($i, $productOptions);
                if (count($intersect) == count($i)) {
                    $return[$SeriesOption->id]['Products'][$idProduct]= $Series->getProductById($idProduct);
                }
            }
        }

        return $return;
    }
}

class ProductPriceFormatter
{
    private $price;
    private $currencyISO;

    function __construct($price, $currencyISO)
    {
        $this->price = $price;
        $this->currencyISO = $currencyISO;
    }

    public function formatString()
    {
        return Currency::format($this->price, $this->currencyISO, true);
    }

    public function formatHtml()
    {
        return Currency::format($this->price, $this->currencyISO);
    }

    public function __toString()
    {
        return (string) Currency::round($this->price, $this->currencyISO);
    }
}
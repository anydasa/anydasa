<?
class Vendors_Image
{
    public static function getRemoteImagesByProductCode($code)
    {
        $sql = 'SELECT id_vendor, code, url
                FROM vendors_products
                WHERE id_product IN (SELECT id FROM products WHERE code = ?)
                AND (url IS NOT NULL OR id_vendor = 1)
                ORDER BY id_vendor';

        $stmt = Doctrine_Manager::connection()->prepare($sql);
        $stmt->bindValue(1, $code);
        $stmt->execute();

        if ( $result = $stmt->fetchAll(Doctrine_Core::FETCH_ASSOC) ) {
            foreach($result as $item) {
                $images = self::getRemoteImagesByUrl($item['url']);
                if ( !empty($images) ) {
                    return $images;
                }
            }
        }

        return array();
    }

    public static function getRemoteImagesByUrl($url)
    {
        $class = 'Vendors_Image_Domain_'.
            implode('',
                array_map(
                    function($word) { return ucfirst($word); },
                    explode('.', ltrim(parse_url($url)['host'], 'www.'))
                )
            );

        if ( @class_exists($class) ) {
            return (new $class)->parsePage($url);
        }

        return [];
    }

    public static function parseImagesFromPage($url)
    {

    }

}
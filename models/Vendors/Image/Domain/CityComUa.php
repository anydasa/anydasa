<?

class Vendors_Image_Domain_CityComUa implements Vendors_Image_Domain_Interface
{
    public function parsePage($url)
    {
        $html = file_get_contents($url);

        if ( preg_match('/ul.*?thumbnails(.*)?ul/isU', $html, $res) ) {
            if ( preg_match_all('/src=".*(\d+_\d+\.jpg)?"/isU', $res[1], $res2) ) {
                return array_map(
                    function($word) { return 'http://city.com.ua/images/products/original/'.$word; },
                    $res2[1]
                );
            }
        }

        return array();
    }
}
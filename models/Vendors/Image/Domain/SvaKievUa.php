<?

class Vendors_Image_Domain_SvaKievUa implements Vendors_Image_Domain_Interface
{
    public function parsePage($url)
    {
        $html = file_get_contents($url);

        if ( preg_match('/<ul class="fotos">(.*?)<\/ul>/is', $html, $res) ) {
            if ( preg_match_all('/href="(\/images.+?)"/is', $res[1], $res2) ) {
                return array_map(
                    function($word) { return 'http://sva.kiev.ua'.$word; },
                    $res2[1]
                );
            }
        }

        return array();
    }
}
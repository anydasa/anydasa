<?php


class Vendors_PriceHandler_Kin
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        $json = json_decode(json_encode($xml), true);

        foreach ($json['nomenklatura'] as $item) {

            $data = [
                'type'        => $item['category'],
                'code'        => '1002-'.$item['code'],
                'brand'       => $item['brand'],
                'balance'     => $item['quantity'],
                'title'       => $item['name'],
                'price_buy'   => Currency::round($item['price'], 'UAH'),
                'id_currency' => Currency::getByISO('UAH')->id,
            ];

            if (!empty($item['RIP'])) {
                $data['price_retail'] = Currency::round($item['RIP'], 'UAH');
            } else {
                $data['price_retail'] = null;
            }

            $return[] = $data;
        }

        return $return;
    }
}
<?php


class Vendors_PriceHandler_ArmorsFormatRozetka
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        $catalogs = [];

        foreach ($xml->shop->categories->category as $cat) {
            $catalogs[(string)$cat->attributes()['id']] = (string)$cat;
        }

        foreach ($xml->shop->offers->offer as $item) {
            $params = [
                'vendorCode' => trim($item->art),
                'categoryName' => $catalogs[(int)$item->categoryId],
            ];

            foreach ($item->param as $param) {
                $params[(string)$param->attributes()['name']] = (string)$param;
            }

            $type = trim($params['Тип']);

            $return[] = [
                'type' => $type,
                'code' => trim($item->code1c),
                'balance' => $item->stock_quantity,
                'title' => trim(str_replace($type, '', $item->name)),
                'price_buy' => round(SF::tofloat($item->priceopt), 2),
                'price_retail' => round(SF::tofloat($item->price), 2),
                'brand' => trim($item->vendor),
                'id_currency' => Currency::getByISO($item->currencyId)->id,
                'url' => trim($item->url),
                'img' => implode("\n", (array)$item->picture),
                'comment' => json_encode($params),
            ];
        }

        return $return;
    }
}

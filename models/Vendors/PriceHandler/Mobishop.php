<?php


class Vendors_PriceHandler_Mobishop
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        $json = json_decode(json_encode($xml), true);


        $catalogs = [];

        foreach ($xml->shop->categories->category as $category) {
            $id = (string)$category['id'];
            $catalogs[$id] = (string)$category;
        }

        foreach ($json['shop']['offers']['offer'] as $item) {

            if ('true' != $item['@attributes']['available']) {
                continue;
            }

            $return[] = [
                'type'        => $catalogs[(string)$item['categoryId']],
                'code'        => '1000'.trim((string)$item['@attributes']['id']),
                'title'       => trim((string)$item['name']),
                'price_buy'   => round(SF::tofloat((string)$item['price']), 2),
                'price_retail'=> round(SF::tofloat((string)$item['price']), 2),
//                'description' => trim((string)$item['description']),
                'img'         => trim((string)$item['picture']),
                'url'         => trim((string)$item['url']),
                'id_currency' => 2,
            ];
        }

        return $return;
    }
}
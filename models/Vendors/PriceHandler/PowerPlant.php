<?php


class Vendors_PriceHandler_PowerPlant
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);
        foreach ($xml->preke as $item) {
            if ( intval($item->likutis) > 0 ) {
                $return[] = [
                    'code'        => trim($item->kodas),
                    'title'       => trim($item->pav_ru),
                    'balance'     => intval($item->likutis),
                    'price_buy'   => round(SF::tofloat($item->kaina_su), 2),
                    'price_retail'=> round(SF::tofloat($item->kaina_su), 2),
                    'description' => trim($item->past_lt),
                    'url'         => trim($item->image),
                    'id_currency' => 2,
                ];
            }
        }
        return $return;
    }
}
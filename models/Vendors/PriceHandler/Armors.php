<?php


class Vendors_PriceHandler_Armors
{

    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        foreach ($xml->shop->offers->offer as $item) {
            /*$params = [];
            foreach($item->param as $param) {
                $key = (string)$param->attributes()->name;
                $params[$key] = trim((string)$param);
            }*/

            $type = trim($item->type);

            $return[] = [
                'type'          => trim($item->type),
                'code'          => trim($item->id),
                'description'          => trim($item->description),
                'title'         => trim(str_replace($type, '', $item->name)),
                'price_buy'     => round(SF::tofloat($item->priceOpt), 2),
                'price_retail'  => round(SF::tofloat($item->price), 2),
                'brand'         => trim($item->brand),
                'id_currency'   => Currency::getByISO('UAH')->id,
                'url' => trim($item->url),
                'img'         => '',
            ];
        }

        return $return;
    }
}
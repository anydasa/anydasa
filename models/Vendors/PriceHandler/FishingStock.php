<?php


class Vendors_PriceHandler_FishingStock
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        $catalogs = [];

        foreach ($xml->catalog->category as $cat) {
            $catalogs[(string) $cat->attributes()['id']] = (string) $cat;
        }

        foreach ($xml->items->item as $item) {
            $itemArray = json_decode(json_encode($item), true);

            $params = [
                'vendorCode' => trim($itemArray['vendorCode']),
                'article' => trim($itemArray['article']),
                'country' => trim($itemArray['country']),
                'categoryName' => $catalogs[$itemArray['categoryId']],
            ];

            foreach ($item->param as $param) {
                $params[(string) $param->attributes()['name']] = (string) $param;
            }

            $return[] = [
                'code'        => trim((string) $item->attributes()['id']),
                'title'       => trim($item->name),
                'description' => trim($item->description),
                'brand'       => trim($item->vendor),
                'type'        => $params['Тип'],
                'comment'     => json_encode($params),
                'price_buy'   => round(SF::tofloat($item->priceuah), 2),
                'price_retail'=> round(SF::tofloat($item->priceuah), 2),
                'id_currency' => 2,
                'balance'     => 'true' === ((string) $item->available) ? 1 : 0,
                'img'         => implode("\n", (array) $item->image)
            ];
        }

        return $return;
    }
}

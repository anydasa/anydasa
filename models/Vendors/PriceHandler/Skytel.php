<?php


class Vendors_PriceHandler_Skytel
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        foreach ($xml->product as $item) {
            $return[] = [
                'code'        => trim($item->kod),
                'title'       => trim($item->name),
                'balance'     => intval($item->stock),
                'price_buy'   => round(SF::tofloat($item->price), 2),
                'price_retail'=> 0,
                'description' => trim($item->description),
                'id_currency' => 1,
            ];
        }

        return $return;
    }
}
<?php


class Vendors_PriceHandler_AllBaby
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        foreach ($xml->shop->offers->offer as $item) {
            $return[] = [
                'code'        => trim($item->barcode),
                'title'       => trim($item->name),
                'url'         => trim($item->url),
                'price_buy'   => round(SF::tofloat($item->price), 2),
                'price_retail'=> round(SF::tofloat($item->price), 2),
                'description' => trim($item->description),
                'id_currency' => 2,
                'balance'     => 'true' === ((string) $item->attributes()['available']) ? 1 : 0,
                'img'         => implode("\n", (array) $item->picture)
            ];
        }

        return $return;
    }
}
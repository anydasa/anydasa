<?php


class Vendors_PriceHandler_Dako
{
    public function getValues($file)
    {
        $return = [];
        $xml = simplexml_load_file($file);

        $json = json_decode(json_encode($xml), true);

        foreach ($json['items']['item'] as $item) {

            if ('Есть в наличии' != $item['StockName']) {
                continue;
            }

            $data = [
                'type'        => $item['Category'].' / '.$item['Subcategory'],
                'url'         => 'http://b2b.dako.ua/products/pr-'.$item['Id'],
                'code'        => '1001-'.$item['Id'],
                'brand'       => trim($item['Brand']),
                'warranty'    => trim((string)$item['Warranty']),
                'title'       => trim($item['Name'].' '.$item['Code']),
                'price_buy'   => Currency::conv((string)$item['Price'], 'USD', 'UAH'),
                'img'         => trim(@current($item['Images']['Image'])),
                'id_currency' => Currency::getByISO('UAH')->id,
            ];

            if (!empty($item['RRP'])) {
                $data['price_retail'] = Currency::round((string)$item['RRP'], 'UAH');
            } else {
                $data['price_retail'] = null;
            }

            $return[] = $data;

        }

        return $return;
    }
}
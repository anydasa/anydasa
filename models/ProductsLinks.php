<?php

require_once 'Traits/Crawler.php';

class ProductsLinks extends BaseProductsLinks
{
//    use Crawler;

    public static $INTERVAL_UPDATE_PRICE_LIST = array(
        3600   => 'Ежечасно',
        10800  => 'Каждые 3 часа',
        21600  => 'Каждые 6 часа',
        43200  => 'Каждые 12 часов',
        86400  => 'Ежедневно',
        259200 => 'Каждые 3 дня',
        604800 => 'Еженедельно'
    );

    public function setUp()
    {
        parent::setUp();

        $this->hasMany('ProductsLinksPrices as Prices', array(
                'local'   => 'id',
                'foreign' => 'id_link',
                'orderBy' => 'price_uah ASC'
            )
        );
    }

    public function getHost()
    {
        return parse_url($this->url, PHP_URL_HOST);
    }

    public function getRemotePriceList()
    {
        $return_array = array();

        if ( $class = self::getClassByUrl($this->url) ) {
            $html = $this->getHTML($this->url, false);
            $prices = (new $class($this->url))->getPrices($html);

            foreach ($prices as $price) {
                $return_array []= $price;
            }

        }

        return SF::multisort($return_array, 'price_uah');
    }

    public function updateRemotePriceList()
    {
        if ( true == $this->is_inexact ) {
            return;
        }

        $prices = $this->getRemotePriceList();

        $this->Prices->clear();

        foreach ($prices as $price) {
            $Price = new ProductsLinksPrices;
            $Price->fromArray($price);
            $this->Prices->add($Price);
        }
        $this->save();

        $this->prices_updated_at = new Doctrine_Expression('NOW()');
        $this->save();
    }



}
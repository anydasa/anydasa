<?

class AdwordsAdgroupKeywords extends BaseAdwordsAdgroupKeywords
{
    public static $SPEC_SYMBOLS_KEY = ['!', '@', '%', ',', '*', '(', ')', '+'];

    public static function getCriterionUseList()
    {
        return array(
            'NEGATIVE' => 'Негативное',
            'BIDDABLE' => 'Позитивное',
        );
    }

    public static function getRemoteKeywords($adGroupIds)
    {
        $return = array();

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupCriterionService = $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        $selector               = new Selector();
        $selector->fields       = array('Id', 'CriteriaType', 'KeywordMatchType', 'KeywordText');
        $selector->predicates[] = new Predicate('AdGroupId', 'IN', $adGroupIds);
        $selector->predicates[] = new Predicate('CriteriaType', 'IN', array('KEYWORD'));
        $selector->predicates[] = new Predicate('Status', 'NOT_IN', array('REMOVED'));
        $selector->paging       = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

        do {
            $page = $adGroupCriterionService->get($selector);
            if (isset($page->entries)) {
                foreach ($page->entries as $adGroupCriterion) {

                    $return[] = [
                        'criterion_use' => $adGroupCriterion->criterionUse,
                        'text'          => $adGroupCriterion->criterion->text,
                        'match_type'    => $adGroupCriterion->criterion->matchType,
                        'id_criterion'  => $adGroupCriterion->criterion->id,
                        'id_group'      => $adGroupCriterion->adGroupId
                    ];
                }
            }
            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return $return;
    }

    public static function removeKeywords($adGroupIds)
    {
        $adGroupIds = (array) $adGroupIds;

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupCriterionService = $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        $operations = array();

        foreach (self::getRemoteKeywords($adGroupIds) as $key) {
            $adGroupCriterion = new AdGroupCriterion();
            $adGroupCriterion->adGroupId = $key['id_group'];
            $adGroupCriterion->criterion = new Criterion($key['id_criterion']);

            $operation = new AdGroupCriterionOperation();
            $operation->operand = $adGroupCriterion;
            $operation->operator = 'REMOVE';

            $operations[] = $operation;
        }

        if ( !empty($operations) ) {
            $adGroupCriterionService->mutate($operations);
        }

        Doctrine_Query::create()
            ->delete(__CLASS__)
            ->whereIn('id_group', $adGroupIds)
            ->execute()
        ;
    }
    public static function addKeywords($keys)
    {
        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupCriterionService = $user->GetService('AdGroupCriterionService', ADWORDS_VERSION);

        $operations = array();

        foreach ($keys as $key) {
            $keyword = new Keyword();
            $keyword->text      = $key['text'];
            $keyword->matchType = $key['match_type'];

            $adGroupCriterion = ('BIDDABLE' == $key['criterion_use']) ? new BiddableAdGroupCriterion : new NegativeAdGroupCriterion;
            $adGroupCriterion->adGroupId = $key['id_group'];
            $adGroupCriterion->criterion = $keyword;

            $adGroupCriteria[] = $adGroupCriterion;

            $operation = new AdGroupCriterionOperation();
            $operation->operand = $adGroupCriterion;
            $operation->operator = 'ADD';
            $operations[] = $operation;
        }

        $result = $adGroupCriterionService->mutate($operations);

        $Collection = new Doctrine_Collection(__CLASS__);

        foreach ($result->value as $adGroupCriterion) {

            $Record = new self();
            $Record->id_group       = $adGroupCriterion->adGroupId;
            $Record->id_criterion   = $adGroupCriterion->criterion->id;

            $Record->text           = $adGroupCriterion->criterion->text;
            $Record->match_type     = $adGroupCriterion->criterion->matchType;
            $Record->criterion_use  = mb_strtoupper(str_replace('AdGroupCriterion', '', get_class($adGroupCriterion)));

            $Collection->add($Record);
        }

        $Collection->replace();

        return $result;
    }
}
<?php

/**
 * BaseMailSmtp
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $host
 * @property string $auth
 * @property string $username
 * @property string $password
 * @property Doctrine_Collection $MailTemplates
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseMailSmtp extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('mail_smtp');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'mail_smtp_id',
             ));
        $this->hasColumn('host', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'SMTP host',
             'primary' => false,
             ));
        $this->hasColumn('auth', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Тип авторизации',
             'primary' => false,
             ));
        $this->hasColumn('username', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Имя пользователя',
             'primary' => false,
             ));
        $this->hasColumn('password', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Пароль',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('MailTemplates', array(
             'local' => 'id',
             'foreign' => 'id_smtp'));
    }
}
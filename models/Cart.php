<?

class Cart
{
    protected static $_instance = null;

    protected $_Products  = [];
    protected $_currency;

    function __construct()
    {
        $Session = self::getSession();

        if ( !empty($Session->Products) ) {
            $this->_Products = $Session->Products;
        }

        $this->_currency = Currency::getByISO('UAH');
    }

    public static function destroy()
    {
        self::getSession()->unsetAll();
        self::$_instance = null;
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public static function setPayment($payment)
    {
        self::getSession()->payment = $payment;
    }

    public static function getPayment()
    {
        return self::getSession()->payment;
    }

    public function unsetAll()
    {
        self::getSession()->unsetAll();
    }

    protected function saveToSession()
    {
        self::getSession()->Products = $this->_Products;
    }

    protected function _calcDiscount(Doctrine_Collection $Products)
    {
        $sets = [];

        foreach ($Products as $Product) {
            $this->_Products[$Product->id]['discount'] = 0;
            if ( !empty( $Product->id_set ) ) {
                $sets [$Product->id_set] = [];
            }
        }

        foreach ($Products as $Product) {
            $p = & $this->_Products[$Product->id];
            if ( !empty($p['id_set']) AND array_key_exists($p['id_set'], $sets) ) {
                $sets[$p['id_set']][] = $Product->id;
            }
        }

        foreach ($sets as $id_set => $id) {
            if ( count($id) ) {
                $Set = Doctrine_Query::create()
                    ->from('ProductsSetsProduct')
                    ->where('id_set = ?', $id_set)
                    ->andWhereIn('id_product', $id)
                    ->execute();

                foreach ($Set as $SetProduct) {
                    $discounts = $SetProduct->discount;
                    $this->_Products[$SetProduct->id_product]['discount'] =
                        count($discounts) < count($id) ? end($discounts) : $discounts[count($id)-1];
                }
            }
        }

    }

    public static function issetProduct($idProduct)
    {
        $Session = self::getSession();

        return isset($Session->Products[$idProduct]);
    }

    protected static function getSession()
    {
        return new Zend_Session_Namespace('Cart');
    }

    public function getCurrency()
    {
        return $this->_currency;
    }

    public function isEmpty()
    {
        return 0 == $this->getProductsCount();
    }

    public function isFull()
    {
        return ! $this->isEmpty();
    }

    public function setCurrency($iso)
    {
        $this->_currency = Currency::getByISO($iso);
        return $this;
    }

    public function getDelivery()
    {
        Zend_Debug::dump(self::getSession()->steps); exit;
    }

//    -----------------------------
    public function setProduct($id, $count = 1, $price = null)
    {
        $this->_Products[$id]['id']     = $id;
        $this->_Products[$id]['count']  = $count;
        if ($price != null) {
            $this->_Products[$id]['price'] = $price;
        }

        $this->saveToSession();
        return $this;
    }

    public function setProductsSet(Products $Owner, Doctrine_Collection $Products, $count = 1)
    {
        $this->_Products[$Owner->id]['id']     = $Owner->id;
        $this->_Products[$Owner->id]['count']  = $count;

        foreach ($Products as $Product) {
            $this->_Products[$Product->id]['id']           = $Product->id;
            $this->_Products[$Product->id]['count']        = $count;
            $this->_Products[$Product->id]['id_set']       = $Owner->id_set;
        }

        $this->saveToSession();
        return $this;
    }

    public function removeProduct($id)
    {
        unset($this->_Products[$id]);
        $this->saveToSession();
        return $this;
    }

    public function clearProducts()
    {
        $this->_Products = [];
        $this->saveToSession();
        return $this;
    }
//    -----------------------------
    public function getProductCount($id)
    {
        return (int) $this->_Products[$id]['count'];
    }

    public function getProducts()
    {
        if ( !empty($this->_Products) ) {
            $Products = Products_Query::create()
                ->setOnlyPublic()
                ->whereIn('p.id', array_keys($this->_Products))
                ->execute();
        }

        if ( !empty($Products) ) {

            $this->_calcDiscount($Products);

            foreach ($Products as $Product) {
                /** @var Products $Product */
                $this->_Products[$Product->id]['id']              = $Product->id;
                $this->_Products[$Product->id]['code']            = $Product->code;
                $this->_Products[$Product->id]['title']           = $Product->title;
                $this->_Products[$Product->id]['currency']        = $this->_currency->iso;
                $this->_Products[$Product->id]['promotion']       = $Product->Promotion->is_enabled ? $Product->Promotion->title : false;
                $this->_Products[$Product->id]['is_set']          = $Product->id_set;

                if (!isset($this->_Products[$Product->id]['price'])) {
                    $this->_Products[$Product->id]['price']       = $Product->getPrice('retail', $this->_currency->iso, $this->_Products[$Product->id]['discount'] );
                }

                $this->_Products[$Product->id]['old_price'] = $Product->getPrice('retail', $this->_currency->iso);

                $this->_Products[$Product->id]['total']     = $this->_Products[$Product->id]['price'] * $this->_Products[$Product->id]['count'];
                $this->_Products[$Product->id]['old_total'] = $this->_Products[$Product->id]['old_price'] * $this->_Products[$Product->id]['count'];

                $this->_Products[$Product->id]['vendor_name']      = $Product->OwnerProduct->exists() ? $Product->OwnerProduct->Vendors->name : NULL;
                $this->_Products[$Product->id]['vendor_price_buy'] = $Product->OwnerProduct->exists() ? $Product->OwnerProduct->getPrice('buy', 'UAH') : NULL;
            }
        }

        return $this->_Products;
    }

    public function getProductsDoctrine()
    {
        if ( !empty($this->_Products) ) {
            $Products = Products_Query::create()
                ->setOnlyPublic()
                ->whereIn('p.id', array_keys($this->_Products))
                ->execute();

            return $Products;
        }

        return false;
    }

    public function getSum($margin_percent = 0)
    {
        $sum = 0;
        foreach ($this->getProducts() as $Product) {
            $sum += $Product['total'];
        }
        if ( $margin_percent > 0 ) {
            $sum += $sum * $margin_percent / 100;
        }
        return $sum;
    }
    public function getOldSum()
    {
        $sum = 0;
        foreach ($this->getProducts() as $Product) {
            $sum += $Product['old_total'];
        }
        return $sum;
    }


    public function getProductsCount()
    {
        $count = 0;

        foreach ($this->_Products as $Product) {
            $count += $Product['count'];
        }

        return $count;
    }
}
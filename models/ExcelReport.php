<?

class ExcelReport
{

    public $Excel;

    function __construct($title)
    {
        PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);

        $this->Excel = new PHPExcel;

        $this->Excel->getActiveSheet()->setTitle($title);
        $this->Excel->getActiveSheet()->setShowGridlines(false);
    }
    public function setHeadRow($data)
    {
        $data = array_map(function ($val) {return $val .'   ';}, $data);

        $this->Excel->getActiveSheet()->fromArray($data, NULL, 'A1');

        $dimension = $this->Excel->getActiveSheet()->calculateWorksheetDimension();

        $this->Excel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
        $this->Excel->getActiveSheet()->getStyle($dimension)->getFont()->setBold(true);

        $this->Excel->getActiveSheet()->getStyle($dimension)->getAlignment()
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $this->Excel->getActiveSheet()->getStyle($dimension)->getFill()->applyFromArray(array(
            'type'       => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => array('rgb' => 'DBB04A'),
        ));

        $this->Excel->getActiveSheet()->setAutoFilter($dimension);
    }

    public function addDataRow($data)
    {
        $row = $this->Excel->getActiveSheet()->getHighestDataRow() + 1;
        $this->Excel->getActiveSheet()->fromArray($data, NULL, 'A'.$row);
    }

    public function getReport($isAutoSize = true)
    {
        foreach(range('A', $this->Excel->getActiveSheet()->getHighestDataColumn()) as $columnID) {
            $this->Excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize($isAutoSize);
        }

        $this->Excel->getActiveSheet()->getStyle($this->Excel->getActiveSheet()->calculateWorksheetDimension())->applyFromArray(
            array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)))
        );

        $ExcelWriter = new PHPExcel_Writer_Excel2007($this->Excel);

        ob_start();
        $ExcelWriter->save('php://output');
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }
}
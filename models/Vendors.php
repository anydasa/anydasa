<?php

use function \GuzzleHttp\json_decode;

class Vendors extends BaseVendors
{
    public static $ProcessIntervals = array(
        3600   => 'Ежечасно',
        10800  => 'Каждые 3 часа',
        43200  => 'Каждые 12 часов',
        86400  => 'Ежедневно',
        259200 => 'Каждые 3 дня',
        604800 => 'Еженедельно',
    );
    public static $ActualIntervals = array(
        3600   => 'Час',
        10800  => '3 часа',
        43200  => '12 часов',
        86400  => 'День',
        259200 => '3 дня',
        604800 => 'Неделя',
    );

    public function preDelete($event)
    {
        $this->VendorsStat->delete();
        @unlink($this->getFilePath());
    }

    public function isActual()
    {
        return time() < $this->interval_actual + strtotime($this->last_time_processed);
    }

    public function isSkippedLoading()
    {
        return time() > $this->interval_process + strtotime($this->last_time_processed);
    }

    public function getLinkedProductsCount()
    {
        return (int) Doctrine_Query::create()
            ->select('COUNT(*) as count')
            ->from('VendorsProducts')
            ->addWhere('id_vendor = ?', $this->id)
            ->addWhere('id_product IS NOT NULL')
            ->execute()
            ->getFirst()
            ->count;
    }

    public function getEnabledProductsCount()
    {
        return (int) Doctrine_Query::create()
            ->select('COUNT(*) as count')
            ->from('VendorsProducts')
            ->addWhere('id_vendor = ?', $this->id)
            ->addWhere('is_enabled = 1')
            ->execute()
            ->getFirst()
            ->count;
    }

    public function getProductsCount()
    {
        return (int) Doctrine_Query::create()
            ->select('COUNT(*) as count')
            ->from('VendorsProducts')
            ->addWhere('id_vendor = ?', $this->id)
            ->execute()
            ->getFirst()
            ->count;
    }

    public function getDisabledProductsCount()
    {
        if ( $all_count = $this->getProductsCount() ) {
            return $all_count - $this->getEnabledProductsCount();
        }
        return 0;
    }

    public static function getPathDir()
    {
        return Zend_Registry::get('config')->path->pix  . 'files/vendors/price_config/';
    }

    public static function getUrlDir()
    {
        return Zend_Registry::get('config')->url->pix  . 'files/vendors/price_config/';
    }

    public function saveFile($filename)
    {
        $this->fileext = SF::FileExt($filename);
        $this->save();

        copy($filename, self::getPathDir() . $this->id . $this->fileext);
    }

    public function process()
    {
        if ( !empty($this->google_drive_key) ) {
            return $this->processFromGoogle($this->google_drive_key);
        }

        if ( !empty($this->url_file) ) {
            return $this->processFromWWW($this->url_file);
        }
    }

    public function processFromGoogle($fileId)
    {
        $client = new Google_Client();
//        $client->setAuthConfig(ROOT_PATH.'/etc/client_secret_751826720078-i9ombu972matfkvb69a7uumefi6jn7h1.apps.googleusercontent.com.json');
        $client->setAuthConfig(ROOT_PATH.'/etc/megabite-f2c4e6e4b8c2.json');
        $client->setSubject('anydasa.com@gmail.com');
//        $client->fetchAccessTokenWithAuthCode('4/8wdWtG1OMsOVWcmQwuqjQTDAkX3VMovLROk1TgXcobU');

        $client->setScopes([
            Google_Service_Drive::DRIVE_FILE,
            Google_Service_Drive::DRIVE_METADATA,
            Google_Service_Drive::DRIVE_METADATA_READONLY,
            Google_Service_Drive::DRIVE_PHOTOS_READONLY,
            Google_Service_Drive::DRIVE_SCRIPTS,
            Google_Service_Drive::DRIVE_APPDATA,
            Google_Service_Drive::DRIVE_READONLY,
        ]);

        $service = new Google_Service_Drive($client);

        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = $service->files->export($fileId, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

        $tmpfname = tempnam(sys_get_temp_dir(), '');
        register_shutdown_function('unlink', $tmpfname);
        file_put_contents($tmpfname, $response->getBody());

        return $this->_process($tmpfname);
    }

    public function processFromWWW($filename)
    {
        $tmpfname = tempnam(sys_get_temp_dir(), '');
        register_shutdown_function('unlink', $tmpfname);

        $client = new \GuzzleHttp\Client();
        $client->get($filename, ['sink' => $tmpfname]);

        return $this->_process($tmpfname);
    }

    public function processUploaded($filename)
    {
        return $this->_process($filename);
    }

    private function _process($localFileName)
    {
        if ( !empty($this->handler_class) ) {
            $className = $this->handler_class;

            if ( class_exists($className) ) {

                $Values = new Site_PriceImport_Collection_Values($this);

                foreach ( (new $className)->getValues($localFileName)  as $item) {
                    $Values->add($item);
                }

                $Values->save();

                $this->last_time_processed = new Doctrine_Expression('NOW()');
                $this->save();
                $this->refresh();

                VendorsProducts::recheckActual();
            }

        } else {
            $PriceImport = new Site_PriceImport(PHPExcel_IOFactory::load($localFileName), $this);
            $Separator = $PriceImport->getSeparator();
            $Separator->separateAllSheets();
            $Separator->getValues()->save();

            $this->last_time_processed = new Doctrine_Expression('NOW()');
            $this->save();
            $this->refresh();

            VendorsProducts::recheckActual();

            return VendorsStat::addStat($localFileName, $this, $Separator);
        }
    }

    public function getFilePath()
    {
        if ( is_file(self::getPathDir() . $this->id . $this->fileext) ) {
            return self::getPathDir() . $this->id . $this->fileext;
        }
        return false;
    }

    public function getFileUrl()
    {
        if ( is_file(self::getPathDir() . $this->id . $this->fileext) ) {
            return self::getUrlDir() . $this->id . $this->fileext;
        }
        return false;
    }

}
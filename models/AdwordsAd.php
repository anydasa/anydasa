<?

require_once 'Google/Api/Ads/AdWords/Lib/AdWordsUser.php';
require_once 'Google/Api/Ads/Common/Util/ErrorUtils.php';


class AdwordsAd extends BaseAdwordsAd
{
    public $isDisabledTrigger = false;

    public static function getStatusList()
    {
        return array(
            'ENABLED'  => 'Включено',
            'PAUSED'   => 'Приостановлено',
        );
    }

    public function setFinalUrls($data)
    {
        parent::_set('final_urls', '{' . SF::arrayToCsv((array)$data) . '}');
        return $this;
    }

    public function getFinalUrls()
    {
        if ( preg_match('/^{(.*)}$/', $this->_data['final_urls'], $matches) && '' != $matches[1] ) {
            return str_getcsv($matches[1]);
        }
        return [];
    }

    public static function makeDisplayUrl($domain, $text)
    {
        $allow_len = 35 - strlen($domain . '/');
        $text = SF::slugify($text, false);
        $text = str_replace('-', ' ', $text);
        $text = trunc_words($text, $allow_len);
        $text = str_replace(' ', '_', $text);

        return $domain. '/' . $text;
    }

    public function __preInsert(Doctrine_Event $event)
    {
        if ($this->isDisabledTrigger) {
            return;
        }

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupAdService = $user->GetService('AdGroupAdService', ADWORDS_VERSION);

        $textAd = new TextAd();
        $textAd->headline       = $this->headline;
        $textAd->description1   = $this->description1;
        $textAd->description2   = $this->description2;
        $textAd->displayUrl     = $this->display_url;
        $textAd->finalUrls      = $this->final_urls;

        $adGroupAd = new AdGroupAd();
        $adGroupAd->adGroupId   = $this->id_group;
        $adGroupAd->ad          = $textAd;
        $adGroupAd->status      = $this->status;

        $operations = $this->_CheckExemptionRequest($user, [new AdGroupAdOperation($adGroupAd, null, 'ADD')]);


        if ( sizeof($operations) == 0 ) {
            $event->skipOperation();
            return;
        }


        $result = $adGroupAdService->mutate($operations);

        $this->id            = $result->value[0]->ad->id;
        $this->headline      = $result->value[0]->ad->headline;
        $this->display_url   = $result->value[0]->ad->displayUrl;
        $this->final_urls    = $result->value[0]->ad->finalUrls;
        $this->description1  = $result->value[0]->ad->description1;
        $this->description2  = $result->value[0]->ad->description2;
    }

    private static function _CheckExemptionRequest(AdWordsUser $user, $operations)
    {
        $adGroupAdValidationService = $user->GetService('AdGroupAdService', ADWORDS_VERSION, null, null, true);

        try {
            $adGroupAdValidationService->mutate($operations);
        } catch (SoapFault $fault) {
            $errors = ErrorUtils::GetApiErrors($fault);
            if (sizeof($errors) == 0) {
                throw $fault;
            }
            foreach ($errors as $error) {
                if ($error->ApiErrorType == 'PolicyViolationError') {
                    $operationIndex = ErrorUtils::GetSourceOperationIndex($error);
                    if ($error->isExemptable) {
                        if ( !$AdwordsForbiddenWord = Doctrine::getTable('AdwordsForbiddenWord')->findOneByKey($error->key->violatingText) ) {
                            $AdwordsForbiddenWord = new AdwordsForbiddenWord;
                            $AdwordsForbiddenWord->key = $error->key->violatingText;
                            $AdwordsForbiddenWord->save();
                        }
                        $headline = $operations[$operationIndex]->operand->ad->headline;
                        $headline = str_replace($AdwordsForbiddenWord->key, $AdwordsForbiddenWord->replacement, $headline);
                        $headline = preg_replace('/\s+/', ' ', $headline);
                        $operations[$operationIndex]->operand->ad->headline = $headline;
                    }
                } else {
                    throw $fault;
                }
            }
        }

        return $operations;
    }

    public function __preUpdate()
    {
        if ($this->isDisabledTrigger) {
            return;
        }
        $user = new AdWordsUser(ADWORDS_INI_PATH);

        $adGroupAdService = $user->GetService('AdGroupAdService', ADWORDS_VERSION);

        $textAd = new TextAd();
        $textAd->id             = $this->id;
        $textAd->headline       = $this->headline;
        $textAd->description1   = $this->description1;
        $textAd->description2   = $this->description2;
        $textAd->displayUrl     = $this->display_url;
        $textAd->finalUrls      = $this->final_urls;

        $adGroupAd = new AdGroupAd();
        $adGroupAd->adGroupId   = $this->id_group;
        $adGroupAd->ad          = $textAd;
        $adGroupAd->status      = $this->status;

        $result = $adGroupAdService->mutate([new AdGroupAdOperation($adGroupAd, null, 'SET')]);

        $this->id            = $result->value[0]->ad->id;
        $this->headline      = $result->value[0]->ad->headline;
        $this->display_url   = $result->value[0]->ad->displayUrl;
        $this->final_urls    = $result->value[0]->ad->finalUrls;
        $this->description1  = $result->value[0]->ad->description1;
        $this->description2  = $result->value[0]->ad->description2;
    }

    public function __preDelete()
    {
        if ( $this->isDisabledTrigger ) {
            return;
        }

        try {
            $user = new AdWordsUser(ADWORDS_INI_PATH);
            $adGroupAdService = $user->GetService('AdGroupAdService', ADWORDS_VERSION);

            $adGroupAd = new AdGroupAd();
            $adGroupAd->adGroupId = $this->id_group;
            $adGroupAd->ad = new Ad($this->id);

            $adGroupAdService->mutate([new AdGroupAdOperation($adGroupAd, null, 'REMOVE')]);
        } catch (Exception $e) {}

    }

//--------------------------------------------------------------------------------------------
    public static function getRemoteAds($adGroupIds)
    {
        $return = array();

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupAdService = $user->GetService('AdGroupAdService', ADWORDS_VERSION);

        $selector = new Selector();
        $selector->fields       = array('Url', 'CreativeFinalUrls', 'Id');
        $selector->predicates[] = new Predicate('AdGroupId', 'IN', $adGroupIds);
        $selector->predicates[] = new Predicate('AdType', 'IN', array('TEXT_AD'));
        $selector->predicates[] = new Predicate('Status', 'IN', array('ENABLED', 'PAUSED'));
        $selector->paging       = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

        do {
            $page = $adGroupAdService->get($selector);
            if (isset($page->entries)) {
                foreach ($page->entries as $adGroupAd) {
                    $return[] = [
                        'id_group'      => $adGroupAd->adGroupId,
                        'status'        => $adGroupAd->status,
                        'id'            => $adGroupAd->ad->id,
                        'headline'      => $adGroupAd->ad->headline,
                        'description1'  => $adGroupAd->ad->description1,
                        'description2'  => $adGroupAd->ad->description2,
                        'display_url'   => $adGroupAd->ad->displayUrl,
                        'final_urls'    => $adGroupAd->ad->finalUrls,
                    ];
                }
            }
            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return $return;
    }

    public static function removeAds($adGroupIds)
    {
        Doctrine_Query::create()
            ->delete(__CLASS__)
            ->whereIn('id_group', $adGroupIds)
            ->execute()
        ;

        $operations = [];

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupAdService = $user->GetService('AdGroupAdService', ADWORDS_VERSION);

        $Ads = self::getRemoteAds($adGroupIds);

        foreach ($Ads as $Ad) {
            $adGroupAd = new AdGroupAd();
            $adGroupAd->adGroupId = $Ad['id_group'];
            $adGroupAd->ad = new Ad($Ad['id']);

            $operations[] = new AdGroupAdOperation($adGroupAd, null, 'REMOVE');
        }
        if ( sizeof($operations) > 0 ) {
            $adGroupAdService->mutate($operations);
        }

    }

    public static function addAds($ads)
    {
        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupAdService = $user->GetService('AdGroupAdService', ADWORDS_VERSION);

        $operations = [];

        foreach ($ads as $item) {
            $textAd = new TextAd();
            $textAd->headline       = $item['headline'];
            $textAd->description1   = $item['description1'];
            $textAd->description2   = $item['description2'];
            $textAd->displayUrl     = $item['display_url'];
            $textAd->finalUrls      = $item['final_urls'];

            $adGroupAd = new AdGroupAd();
            $adGroupAd->adGroupId   = $item['id_group'];
            $adGroupAd->ad          = $textAd;
            $adGroupAd->status      = $item['status'];

            $operations[]= new AdGroupAdOperation($adGroupAd, null, 'ADD');
        }

        $operations = self::_CheckExemptionRequest($user, $operations);

        if ( sizeof($operations) == 0 ) {
            return;
        }

        $result = $adGroupAdService->mutate($operations);
        $Collection = new Doctrine_Collection(__CLASS__);

        foreach ($result->value as $result_item) {

            $Record = new self();

            $Record->id            = $result_item->ad->id;
            $Record->id_group      = $result_item->adGroupId;
            $Record->headline      = $result_item->ad->headline;
            $Record->display_url   = $result_item->ad->displayUrl;
            $Record->final_urls    = $result_item->ad->finalUrls;
            $Record->description1  = $result_item->ad->description1;
            $Record->description2  = $result_item->ad->description2;
            $Record->status        = $result_item->status;

            $Collection->add($Record);
        }

        $Collection->replace();
    }
}
<?php

/**
 * Pages
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class AdminPages extends BaseAdminPages
{
    public function setUp()
    {
        parent::setUp();
        $this->actAs('NestedSet');
    }
    public function preSave($event)
    {
        if ( empty($this->route_id) ) $this->route_id = null;
    }
}
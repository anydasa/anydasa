<?

require_once LIB_PATH . '/AdWordsUser.php';


class AdwordsCampaign extends BaseAdwordsCampaign
{
    public $isDisabledTrigger = false;

    public function setUpdatedFields($data)
    {
        parent::_set('updated_fields', '{' . SF::arrayToCsv((array)$data) . '}');
        return $this;
    }

    public function getUpdatedFields()
    {
        if ( preg_match('/^{(.*)}$/', $this->_data['updated_fields'], $matches) && '' != $matches[1] ) {
            return str_getcsv($matches[1]);
        }
        return [];
    }

    public static function getStatusList()
    {
        return array(
            'ENABLED' => 'Включено',
            'PAUSED'  => 'Приостановлено',
        );
    }

    public static function getOptimizationStatusList()
    {
        return array(
            'OPTIMIZE'              => 'Оптимизация (клики)',
            'CONVERSION_OPTIMIZE'   => 'Оптимизация (конверсии)',
            'ROTATE'                => 'Равномерное чередование',
            'ROTATE_INDEFINITELY'   => 'Чередование без ограничения времени',
        );
    }

    public function preInsert($event)
    {
        if ( $this->isDisabledTrigger ) {
            return;
        }

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $campaignService = $user->GetService('CampaignService', ADWORDS_VERSION);

        $budgetService = $user->GetService('BudgetService', ADWORDS_VERSION);
        $budget = new Budget();
        $budget->name = $this->name;
        $budget->period = 'DAILY';
        $budget->amount = new Money($this->budget_amount * 1000000);
        $budget->deliveryMethod = 'STANDARD';
        $budget->isExplicitlyShared = false; // только для 1 кампании
        $result = $budgetService->mutate([new BudgetOperation($budget, 'ADD')]);
        $budget = $result->value[0];

        $campaign = new Campaign();
        $campaign->name = $this->name;
        $campaign->advertisingChannelType = 'SEARCH';
        $campaign->budget = new Budget();
        $campaign->budget->budgetId = $budget->budgetId;

        // Set bidding strategy (required).
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->biddingStrategyType = 'MANUAL_CPC';

        // You can optionally provide a bidding scheme in place of the type.
        $biddingScheme = new ManualCpcBiddingScheme();
        $biddingScheme->enhancedCpcEnabled = false;
        $biddingStrategyConfiguration->biddingScheme = $biddingScheme;

        $campaign->biddingStrategyConfiguration = $biddingStrategyConfiguration;

        // Set network targeting (optional).
        $networkSetting = new NetworkSetting();
        $networkSetting->targetGoogleSearch = true;
        $networkSetting->targetSearchNetwork = false;
        $networkSetting->targetContentNetwork = false;
        $campaign->networkSetting = $networkSetting;

        // Set additional settings (optional).
        $campaign->status                       = $this->status;
        $campaign->startDate                    = date('Ymd', strtotime('now'));
        $campaign->endDate                      = date('Ymd', strtotime('+20 year'));
        $campaign->adServingOptimizationStatus  = $this->ad_serving_optimization_status;

        // Set frequency cap (optional).
        $frequencyCap = new FrequencyCap();
        $frequencyCap->impressions = 5;
        $frequencyCap->timeUnit = 'DAY';
        $frequencyCap->level = 'ADGROUP';
        $campaign->frequencyCap = $frequencyCap;

        // Set advanced location targeting settings (optional).
        $geoTargetTypeSetting = new GeoTargetTypeSetting();
        $geoTargetTypeSetting->positiveGeoTargetType = 'DONT_CARE';
        $geoTargetTypeSetting->negativeGeoTargetType = 'DONT_CARE';
        $campaign->settings[] = $geoTargetTypeSetting;

        $result = $campaignService->mutate([new CampaignOperation($campaign, 'ADD')]);

        $this->id = $result->value[0]->id;

        $campaignCriterionService = $user->GetService('CampaignCriterionService', ADWORDS_VERSION);
        $campaignCriteria = array();
        $campaignCriteria[] = new CampaignCriterion($this->id, null, new Location(null,null,null,null,2804)); //Украина
        $campaignCriteria[] = new NegativeCampaignCriterion($this->id, null, new Location(null,null,null,null,1012838)); //Луганск
        $campaignCriteria[] = new NegativeCampaignCriterion($this->id, null, new Location(null,null,null,null,1012842)); //Донецк
        $campaignCriteria[] = new NegativeCampaignCriterion($this->id, null, new Location(null,null,null,null,1012843)); //Горловка
        $campaignCriteria[] = new NegativeCampaignCriterion($this->id, null, new Location(null,null,null,null,1012844)); //Макеевка

        $operations = array();
        foreach ($campaignCriteria as $campaignCriterion) {
            $operations[] = new CampaignCriterionOperation($campaignCriterion, 'ADD');
        }
        $campaignCriterionService->mutate($operations);
    }

    public function preUpdate($event)
    {
        if ( $this->isDisabledTrigger ) {
            return;
        }

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $campaignService = $user->GetService('CampaignService', ADWORDS_VERSION);

        $campaign = new Campaign();
        $campaign->id                           = $this->id;
        $campaign->name                         = $this->name;
        $campaign->status                       = $this->status;
        $campaign->adServingOptimizationStatus  = $this->ad_serving_optimization_status;

        if ( $this->getModified()['budget_amount'] ) {
            $selector = new Selector();
            $selector->fields = array('BudgetId', 'Amount', 'Period');
            $selector->predicates[] = new Predicate('Id', 'IN', array($this->id));
            $budget = $campaignService->get($selector)->entries[0]->budget;
            $budget->amount = new Money($this->budget_amount * 1000000);

            $budgetService = $user->GetService('BudgetService', ADWORDS_VERSION);
            $budgetService->mutate(array(new BudgetOperation($budget, 'SET')));
        }

        $campaignService->mutate([new CampaignOperation($campaign, 'SET')]);

    }

    public function preDelete($event)
    {
        if ( $this->isDisabledTrigger ) {
            return;
        }

        try {
            $user = new AdWordsUser(ADWORDS_INI_PATH);
            $campaignService = $user->GetService('CampaignService', ADWORDS_VERSION);

            $selector = new Selector();
            $selector->fields = array('Id');
            $selector->predicates[] = new Predicate('Id', 'IN', array($this->id));
            $campaign = $campaignService->get($selector)->entries[0];
            $campaign->status = 'REMOVED';
            $campaignService->mutate([new CampaignOperation($campaign, 'SET')]);
        } catch (Exception $e) {}

    }

    public static function getRemoteCampaigns()
    {
        $return = array();

        $user = new AdWordsUser(ADWORDS_INI_PATH);

        $campaignService = $user->GetService('CampaignService', ADWORDS_VERSION);

        $selector = new Selector();
        $selector->fields = array('Id', 'Name', 'Status', 'AdServingOptimizationStatus', 'Amount', 'BudgetId');
        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

        do {
            $page = $campaignService->get($selector);
            if (isset($page->entries)) {
                foreach ($page->entries as $campaign) {
                    $return[] = [
                        'id' => $campaign->id,
                        'name' => $campaign->name,
                        'status' => $campaign->status,
                        'ad_serving_optimization_status' => $campaign->adServingOptimizationStatus,
                        'budget_amount' => $campaign->budget->amount->microAmount / 1000000,

                    ];
                }
            }
            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return $return;
    }
}
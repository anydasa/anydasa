<?php

/**
 * @property Doctrine_Collection|SeriesProducts[] $SeriesProducts
 */
class Series extends BaseSeries
{
    public function setUp()
    {
        parent::setUp();

        $this->hasMany('Products as Products', array(
                'refClass' => 'SeriesProducts',
                'local'    => 'id_series',
                'foreign'  => 'id_product',
                'orderBy'  => 'sort'
            )
        );

        $this->hasMany('SeriesProducts', array(
                'local'    => 'id',
                'foreign'  => 'id_series',
                'orderBy'  => 'sort'
            )
        );

        $this->hasMany('SeriesParams as Params', array(
            'local'   => 'id',
            'foreign' => 'id_series',
            'orderBy'  => 'sort',
        ));

        $this->hasOne('SeriesGroups as Group', array(
            'local'   => 'id_group',
            'foreign' => 'id'
        ));
    }

    public function getImageUrl()
    {
        if ( is_file($this->getImagePath()) ) {
            return Zend_Registry::get('config')->url->pix  . 'img/series/' . $this->id;
        }
        return false;
    }

    public function getImagePath()
    {
        return Zend_Registry::get('config')->path->pix  . 'img/series/' . $this->id;
    }

    public function postDelete($event)
    {
        @unlink($this->getImagePath());
    }

    public function getHtmlImgCode()
    {
        $str = '<img src="%s" alt="" width="100%%">';
        $str = sprintf($str, $this->getImageUrl());

        return $str;
    }

    public function getMinCostProduct()
    {
        $min = null;
        $Product = null;

        /** @var Products $Product */
        foreach ($this->Products as $ProductItem) {
            $price = $ProductItem->getPrice('retail', 'UAH');
            if ($price > 0 && (is_null($min) || $price < $min)) {
                $min = $price;
                $Product = $ProductItem;
            }
        }

        return $Product;
    }

    public function getProductById($id): ?Products
    {
        foreach($this->SeriesProducts as $SeriesProducts) {
            if ($id == $SeriesProducts->id_product) {
                return $SeriesProducts->Products;
            }
        }

        return null;
    }

    public function getProducts()
    {
        $return = [];

        foreach($this->SeriesProducts as $SeriesProducts) {
            $return[]= $SeriesProducts->Products;
        }

        return $return;
    }
}

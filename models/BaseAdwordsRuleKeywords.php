<?php

/**
 * BaseAdwordsRuleKeywords
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property string $keys_prepend
 * @property string $keys_append
 * @property string $keys_negative
 * @property string $keys_remove
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseAdwordsRuleKeywords extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('adwords_rule_keywords');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'primary' => true,
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Название',
             'primary' => false,
             ));
        $this->hasColumn('keys_prepend', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Ключи в начало',
             'primary' => false,
             ));
        $this->hasColumn('keys_append', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Ключи в конец',
             'primary' => false,
             ));
        $this->hasColumn('keys_negative', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Минус слова',
             'primary' => false,
             ));
        $this->hasColumn('keys_remove', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Удаляемые подстроки',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}
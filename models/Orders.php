<?

class Orders extends BaseOrders
{
    public function setUp()
    {
        parent::setUp();

        $this->hasMany('OrdersProducts as Products', array(
            'local' => 'id',
            'foreign' => 'id_order'
        ));

        $this->hasMany('OrdersHistory as History', array(
            'local' => 'id',
            'foreign' => 'id_order',
            'orderBy' => 'created'
        ));

        $this->hasOne('OrdersStatus as Status', array(
                'local'    => 'id_status',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('OrdersPayment as Payment', array(
                'local'    => 'payment_type',
                'foreign'  => 'id'
            )
        );

        $this->hasOne('OrdersDelivery as Delivery', array(
                'local'    => 'delivery_type',
                'foreign'  => 'id'
            )
        );
    }

    public function getPhone(){
        $phone = parent::rawGet('phone');

        if($phone != '-' && !empty($phone)){
            $str = '';
            $str .= substr($phone, 0, 2) . ' (';
            $str .= substr($phone, 2, 3) . ') ';
            $str .= substr($phone, 5, 3) . '-';
            $str .= substr($phone, 8, 2) . '-';
            $str .= substr($phone, 10);
            $str = str_replace('+', '', $str);

            $str = '+' . $str;

            $phone = $str;
        }

        return $phone;

    }

    public function postInsert($event)
    {
        /** @var self $Order */
        $Order = $event->getInvoker()->refresh();

        AdminNotifications::addRecord('Новый заказ',$Order);

        Telegram::send(sprintf(
            "Новый заказ № %s на сумму %s \nИмя: %s \nтел: %s \nemail: %s",
            $this->id,
            $this->getSum(),
            $this->name,
            $this->phone,
            $this->email
        ));
    }

    public function preSave($event)
    {
        $modified = $this->getModified();

        if ( !empty($modified['id_status']) ) {
            $this->token_chast = ('waitingCreditChast' == $modified['id_status']) ? md5($this->id . rand()) : null;
        }

        if(!empty($this->phone)){
            $phone = $this->phone;

            $phone = str_replace(' ', '', $phone);
            $phone = str_replace('-', '', $phone);
            $phone = str_replace('(', '', $phone);
            $phone = str_replace(')', '', $phone);
            $phone = str_replace('+', '', $phone);

            $this->phone = $phone;
        }

    }

    public function isOverdue()
    {
        $res = Doctrine_Query::create()
            ->from('Orders o')
            ->innerJoin('o.OrdersStatus os')
            ->addWhere('o.id = ?', $this->id)
            ->addWhere('COALESCE(updated, created) < NOW() - (INTERVAL \'1 min\' * os.minutes_limit)')
            ->count();

        return !empty($res);
    }
    public function sendMessagesForCurrentStatus()
    {
        return $this->sendMessagesStatus($this->OrdersStatus->id);
    }

    public function sendMessagesStatus($id_status)
    {
        return; //TODO hot hack
        $Status = OrdersStatus::getById($id_status);

        $data = $this->_getDataForSender($id_status);

        $tpl_dir = Zend_Registry::getInstance()->config->path->root . '/templates/twig/';
        //TODO ERROR Shuld be update
        $loader = new Twig_Loader_Filesystem($tpl_dir);
        $twig = new Twig_Environment($loader);


        if ( $Status->is_send_user_sms && !empty($this->phone) ) {
            $tpl = "sms/order/user/{$Status->id}.twig";
            if ( !is_file($tpl_dir.$tpl) ) {
                $tpl = pathinfo($tpl, PATHINFO_DIRNAME) . "/_.twig";
            }

            $message = $twig->render($tpl, $data);
//            Sms::send($this->phone, $message);
        }


        if ( $Status->is_send_admin_sms ) {
            $tpl = "sms/order/admin/{$Status->id}.twig";
            if ( !is_file($tpl_dir.$tpl) ) {
                $tpl = pathinfo($tpl, PATHINFO_DIRNAME) . "/_.twig";
            }

            $message = $twig->render($tpl, $data);
//            Sms::send('+38(067) 644-61-71', $message);
        }


        if ( $Status->is_send_user_mail && !empty($this->email) ) {
            $tpl = "mail/order/user/{$Status->id}.twig";
            if ( !is_file($tpl_dir.$tpl) ) {
                $tpl = pathinfo($tpl, PATHINFO_DIRNAME) . "/_.twig";
            }

            $body = $twig->render($tpl, $data);
            $mail = new Site_Mail('UTF-8');
            $mail->setBodyHtml($body);
            $mail->setFrom('info@anydasa.com');
            $mail->setSubject($Status->mail_subject);
            $mail->addTo($this->email, $this->name);
            $mail->send();
        }


        if ( $Status->is_send_admin_mail ) {
            $tpl = "mail/order/admin/{$Status->id}.twig";
            if ( !is_file($tpl_dir.$tpl) ) {
                $tpl = pathinfo($tpl, PATHINFO_DIRNAME) . "/_.twig";
            }

            $body = $twig->render($tpl, $data);

            $mail = new Site_Mail('UTF-8');
            $mail->setBodyHtml($body);
            $mail->setFrom('info@anydasa.com');
            $mail->setSubject($Status->mail_subject);
            $mail->addTo('info@anydasa.com', '');
            $mail->send();
        }

    }


    protected function _getDataForSender($id_status)
    {
        $this->loadReference('Status');
        $this->loadReference('Products');

        $return = [];
        $return['config'] = Zend_Registry::getInstance()->config->toArray();
        $return['order']  = $this->toArray();
        $return['order']['who_created']  = $this->AdminUsers->name;
        $return['order']['total'] = Currency::format($this->getSum(), 'UAH', true);

        if ( $cost = $this->getCostDelivery() ) {
            $return['order']['delivery_cost'] = Currency::format($cost, 'UAH', true);
        }

        foreach ($this->Products->toArray() as $product) {
            $P = Doctrine::getTable('Products')->findOneByCode($product['code']);
            if ( $P && $P->getImage()->getImagesCount() ) {
                $product['img'] = $P->getImage()->getFirstImage()['small']['url'];
            }
            $product['url'] = $P->getUrl();
            $product['total'] = Currency::format($product['price'] * $product['count'], 'UAH', true);
            $product['price'] = Currency::format($product['price'], 'UAH', true);
            $return['order']['products'][] = $product;
        }

        if ( 'new' == $id_status ) {
            $return['recommended_products'] = $this->getRecommendedProducts();
        }

        return $return;
    }

    public function getRecommendedProducts()
    {
        $ordered_products_codes = $this->Products->toKeyValueArray('code', 'code');


        $Products = Doctrine_Query::create()
            ->from('Products')
            ->whereIn('code', array_values($ordered_products_codes))
            ->addWhere('id_set IS NOT NULL')
            ->execute()
        ;


        $return = [];
        $exclude = [];

        foreach ($Products as $Product) {
            $ProductsSetsProduct = $Product->Set->getProductsInComplect();
            $ProductsSetsProduct->loadRelated('Products');

            foreach ($ProductsSetsProduct as $ProductSet) {

                if ( in_array($ProductSet->Products->code, $ordered_products_codes) ) {
                    $exclude []= [
                        'parent_code' => $Product->code,
                        'group_num'   => $ProductSet->group_num,
                    ];
                }

                $product_temp = [
                    'id'          => $ProductSet->Products->id,
                    'parent_code' => $Product->code,
                    'code'        => $ProductSet->Products->code,
                    'group_num'   => $ProductSet->group_num,
                    'url'         => $ProductSet->Products->getUrl(),
                    'name'        => $ProductSet->Products->getFullName(),
                    'price'       => $ProductSet->Products->getFormatedPrice('retail', 'UAH', true)
                ];
                if ( $ProductSet->Products->getImage()->getImagesCount() ) {
                    $product_temp['img'] = $ProductSet->Products->getImage()->getFirstImage()['small']['url'];
                }

                $return[$ProductSet->Products->code] = $product_temp;
            }
        }

        $return = array_filter($return, function ($item) use (& $exclude) {
            foreach ($exclude as $exclude_item) {
                if ( $exclude_item['parent_code'] == $item['parent_code'] && $exclude_item['group_num'] == $item['group_num'] ) {
                    return false;
                }
            }
            $exclude [] = [
                'parent_code' => $item['parent_code'],
                'group_num'   => $item['group_num'],
            ];
            return true;
        });

        $return = array_slice($return, 0, 10);

        return array_values($return);
    }



    public function getCostDelivery()
    {
        if($this->delivery_cost){
            return $this->delivery_cost;
        }else{
            return $this->OrdersDelivery->free_from_price < $this->getSum() ? 0 : $this->OrdersDelivery->cost;
        }
    }

    public function findSimilar($count = false)
    {
        $Query = Doctrine_Query::create()
            ->from('Orders')
            ->where('id = 0')

        ;

        if ( !empty($this->email) ) {
            $Query->orWhere('email like ? AND id <> ?', [$this->email, $this->id]);
        }

        if ( !empty($this->phone) ) {
            $Query->orWhere('phone like ? AND id <> ?', [$this->phone, $this->id]);
        }

        if ( $count ) {
            return $Query->count();
        }

        return $Query->limit(20)->execute();
    }

    public function getExcelContent()
    {
        $tpl = dirname(dirname(__FILE__)).'/templates/Bill.xlsx';
        if ( !is_file($tpl) ) {
            throw new Site_Exception;
        }


        $objReader = PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load( $tpl );

        $str = $objPHPExcel->getActiveSheet()->getCell('B1')->getValue();
        $str = str_replace('{id}', $this->id, $str);
        $str = str_replace('{created}', date('m.d.Y', strtotime($this->created)), $str);
        $objPHPExcel->getActiveSheet()->getCell('B1')->setValue($str);

        $str = $objPHPExcel->getActiveSheet()->getCell('B3')->getValue();
        $str = str_replace('{name}', $this->name, $str);
        $objPHPExcel->getActiveSheet()->getCell('B3')->setValue($str);

        $str = $objPHPExcel->getActiveSheet()->getCell('B4')->getValue();
        $str = str_replace('{email}', $this->email, $str);
        $objPHPExcel->getActiveSheet()->getCell('B4')->setValue($str);

        $str = $objPHPExcel->getActiveSheet()->getCell('B5')->getValue();
        $str = str_replace('{phone}', $this->phone, $str);
        $objPHPExcel->getActiveSheet()->getCell('B5')->setValue($str);

        $rowStart = 41;

        foreach ($this->OrdersProducts as $product) {
            $objPHPExcel->getActiveSheet()->insertNewRowBefore($rowStart+1, 1);
            $temp[] = array(
                $product->code,
                $product->title,
                $product->price,
                "=C$rowStart*F$rowStart",
                "=C$rowStart*INDEX(I:I,M39)",
                $product->count,
                "=C$rowStart*F$rowStart*INDEX(I:I,M39)"
            );
            $rowStart++;
        }

        $objPHPExcel->getActiveSheet()->removeRow($rowStart,1);

        $objPHPExcel->getActiveSheet()->getCell("G$rowStart")->setValue("=SUM(G41:G".($rowStart-1).")");


        $objPHPExcel->getActiveSheet()->fromArray($temp, NULL, 'A41');

        $Currency = array();
        foreach (Doctrine_Query::create()->from('Currency')->where('is_visible = 1')->orderBy('sort')->execute() as $item) {
            $Currency[$item->id] = array(
                $item->title,
                $item->rate,
            );
        }

        $objPHPExcel->getActiveSheet()->fromArray($Currency, NULL, 'H40');

        $objPHPExcel->addNamedRange(
            new PHPExcel_NamedRange('Валюты', $objPHPExcel->getActiveSheet(), 'H40:H'.(39+count($Currency)))
        );

        $objPHPExcel->getActiveSheet()->getCell('G39')->setValue(current($Currency)[0]);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

        ob_start();
        $objWriter->save('php://output');
        $result = ob_get_contents();
        ob_end_clean();

        return $result;
    }

    public function getSum()
    {
        $sum = 0;
        foreach ($this->OrdersProducts as $item) {
           $sum +=  $item->price * $item->count;
        }
        return $sum;
    }

    public function addProducts(Cart $Cart)
    {
        foreach ($Cart->getProducts() as $product) {
            $OrdersProducts = new OrdersProducts;

            $OrdersProducts->code               = $product['code'];
            $OrdersProducts->count              = $product['count'];
            $OrdersProducts->title              = $product['title'];
            $OrdersProducts->price              = $product['price'];
            $OrdersProducts->promotion          = $product['promotion'];
            $OrdersProducts->vendor_name        = $product['vendor_name'];
            $OrdersProducts->vendor_price_buy   = $product['vendor_price_buy'];

            $this->OrdersProducts->add($OrdersProducts);
        }

    }

    public function getOrderProcessingTime(){
        $final = false;
        $date_created = strtotime($this->created);

        $intervals = array();
        $date_updated = time();

        if($this->History->count()){
            foreach ($this->History as $key => $history){
                if(strpos($history->comment, 'Смена статуса')){
                    if(isset($this->History[$key-1]) && (strpos($this->History[$key-1]->comment, 'на <i>Выполнен') || strpos($this->History[$key-1]->comment, 'на <i>Отклоненный'))){
                        $intervals[] = strtotime($history->created) - strtotime($this->History[$key-1]->created);
                    }elseif(!isset($this->History[$key+1]) && (strpos($history->comment, 'на <i>Выполнен') || strpos($history->comment, 'на <i>Отклоненный'))){
                        $date_updated = strtotime($history->created);
                        $final = true;
                    }
                }
            }
        }

        $total_interval = $date_updated - $date_created;

        foreach ($intervals as $interval){
            $total_interval -= $interval;
        }


        $processing_time = array(
            'days' => (int)($total_interval / 3600 / 24 % 60),
            'h' => (int)($total_interval / 3600 % 24),
            'i' => $total_interval / 60 % 60,
            'final' => $final
        );

        return $processing_time;
    }

}

<?php

/**
 * BaseOrdersProducts
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property decimal $price
 * @property integer $count
 * @property integer $id_order
 * @property integer $id_vendor_product
 * @property string $vendor_name
 * @property decimal $vendor_price_buy
 * @property string $promotion
 * @property Orders $Orders
 * @property VendorsProducts $VendorsProducts
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseOrdersProducts extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('orders_products');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'orders_products_id',
             ));
        $this->hasColumn('code', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Артикул',
             'primary' => false,
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Название товара',
             'primary' => false,
             ));
        $this->hasColumn('price', 'decimal', null, array(
             'type' => 'decimal',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Цена',
             'primary' => false,
             ));
        $this->hasColumn('count', 'integer', 2, array(
             'type' => 'integer',
             'length' => 2,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество',
             'primary' => false,
             ));
        $this->hasColumn('id_order', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Номер заказа',
             'primary' => false,
             ));
        $this->hasColumn('id_vendor_product', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Товар поставщика',
             'primary' => false,
             ));
        $this->hasColumn('vendor_name', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Поставщик',
             'primary' => false,
             ));
        $this->hasColumn('vendor_price_buy', 'decimal', null, array(
             'type' => 'decimal',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Закупка',
             'primary' => false,
             ));
        $this->hasColumn('promotion', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Акция',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Orders', array(
             'local' => 'id_order',
             'foreign' => 'id'));

        $this->hasOne('VendorsProducts', array(
             'local' => 'id_vendor_product',
             'foreign' => 'id'));
    }
}
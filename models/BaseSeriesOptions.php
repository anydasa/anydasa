<?php

/**
 * BaseSeriesOptions
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $id_catalog
 * @property integer $id_product_option
 * @property integer $display_type
 * @property integer $sort
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSeriesOptions extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('series_options');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'series_options_id',
             ));
        $this->hasColumn('id_catalog', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'primary' => false,
             ));
        $this->hasColumn('id_product_option', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Параметр продукта',
             'primary' => false,
             ));
        $this->hasColumn('display_type', 'integer', 2, array(
             'type' => 'integer',
             'length' => 2,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Тип отображения',
             'primary' => false,
             ));
        $this->hasColumn('sort', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Сортировка',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}
<?


require_once LIB_PATH . '/AdWordsUser.php';

class AdwordsAdgroup extends BaseAdwordsAdgroup
{
    public $isDisabledTrigger = false;

    public static function getStatusList()
    {
        return array(
            'ENABLED' => 'Включено',
            'PAUSED'  => 'Приостановлено',
        );
    }

    public static function getMatchTypeList()
    {
        return array(
            'BROAD'  => 'Широкое соответствие',
            'EXACT'  => 'Точное соответствие',
            'PHRASE' => 'Фразовое соответствие',
        );
    }

    public function preInsert($event)
    {
        if ($this->isDisabledTrigger) {
            return;
        }


    }

    public function preUpdate($event)
    {
        if ($this->isDisabledTrigger) {
            return;
        }

        /*$user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupService = $user->GetService('AdGroupService', ADWORDS_VERSION);

        $adGroup = new AdGroup();
        $adGroup->id = $this->id;
        $adGroup->campaignId = $this->id_camp;
        $adGroup->name = $this->name;

        $bid = new CpcBid();
        $bid->bid =  new Money($this->amount * 1000000);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->bids[] = $bid;
        $adGroup->biddingStrategyConfiguration = $biddingStrategyConfiguration;

        $adGroup->status = $this->status;

        $adGroupService->mutate([new AdGroupOperation($adGroup, 'SET')]);*/
    }

    public function preDelete($event)
    {
        if ( $this->isDisabledTrigger ) {
            return;
        }

        try {
            $user = new AdWordsUser(ADWORDS_INI_PATH);
            $adGroupService = $user->GetService('AdGroupService', ADWORDS_VERSION);
            $adGroup = new AdGroup();
            $adGroup->id = $this->id;
            $adGroup->status = 'REMOVED';
            $adGroupService->mutate([new AdGroupOperation($adGroup, 'SET')]);
        } catch (Exception $e) {}

    }

    public static function getRemoteGroups($campaignIds)
    {
        $return = array();

        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupService = $user->GetService('AdGroupService', ADWORDS_VERSION);

        $selector = new Selector();
        $selector->fields = array('Id', 'Name', 'CampaignId', 'Status', 'CpcBid');
        $selector->ordering[] = new OrderBy('Name', 'ASCENDING');
        $selector->predicates[] = new Predicate('CampaignId', 'IN', $campaignIds);

        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

        do {
            $page = $adGroupService->get($selector);
            if (isset($page->entries)) {
                foreach ($page->entries as $adGroup) {
                    $return[] = [
                        'id' => $adGroup->id,
                        'id_camp' => $adGroup->campaignId,
                        'name' => $adGroup->name,
                        'status' => $adGroup->status,
                        'amount' => $adGroup->biddingStrategyConfiguration->bids[0]->bid->microAmount / 1000000,
                    ];
                }
            }
            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($page->totalNumEntries > $selector->paging->startIndex);

        return $return;
    }

    public static function createFromProduct(Doctrine_Collection $Products, $CampId, $amount)
    {
        $user = new AdWordsUser(ADWORDS_INI_PATH);
        $adGroupService = $user->GetService('AdGroupService', ADWORDS_VERSION);

        $operations = [];
        foreach ($Products as $Product) {
            $adGroup = new AdGroup();
            $adGroup->campaignId = $CampId;
            $adGroup->name       = $Product->getFullName() . ' ' . uniqid(). ' #' . $Product->id;

            $bid = new CpcBid();
            $bid->bid =  new Money($amount * 1000000);
            $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
            $biddingStrategyConfiguration->bids[] = $bid;
            $adGroup->biddingStrategyConfiguration = $biddingStrategyConfiguration;

            $adGroup->status = 'ENABLED';

            $operations[] = new AdGroupOperation($adGroup, 'ADD');
        }

        if ( !empty($operations) ) {
            $result = $adGroupService->mutate($operations);

            foreach ($result->value as $item) {

                $Record = new self;
                $Record->id         = $item->id;
                $Record->status     = $item->status;
                $Record->id_camp    = $item->campaignId;
                $Record->name       = $item->name;
                $Record->amount     = $item->biddingStrategyConfiguration->bids[0]->bid->microAmount / 1000000;

                if ( preg_match('/^.*#(\d+)$/', $item->name, $id_product) ) {
                    $Record->id_product = $id_product[1];
                }

                $Record->replace();
            }
        }

    }

}
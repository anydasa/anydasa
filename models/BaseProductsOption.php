<?php

/**
 * BaseProductsOption
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property boolean $is_prime
 * @property integer $sort
 * @property boolean $is_require
 * @property integer $id_catalog
 * @property string $group_name
 * @property string $group_name_brief
 * @property string $title_brief
 * @property string $unit
 * @property string $linked
 * @property string $type
 * @property Catalog $Catalog
 * @property Doctrine_Collection $ProductsRefOption
 * @property Doctrine_Collection $CatalogFilterValueCondition
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseProductsOption extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('products_option');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'products_option_param_id',
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Название',
             'primary' => false,
             ));
        $this->hasColumn('is_prime', 'boolean', 1, array(
             'type' => 'boolean',
             'length' => 1,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => false,
             'comment' => 'Основной',
             'primary' => false,
             ));
        $this->hasColumn('sort', 'integer', 2, array(
             'type' => 'integer',
             'length' => 2,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Сортировка',
             'primary' => false,
             ));
        $this->hasColumn('is_require', 'boolean', 1, array(
             'type' => 'boolean',
             'length' => 1,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => false,
             'comment' => 'Обязательный',
             'primary' => false,
             ));
        $this->hasColumn('id_catalog', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Каталог',
             'primary' => false,
             ));
        $this->hasColumn('group_name', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Группа',
             'primary' => false,
             ));
        $this->hasColumn('group_name_brief', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Группа (кратко)',
             'primary' => false,
             ));
        $this->hasColumn('title_brief', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Название (кратко)',
             'primary' => false,
             ));
        $this->hasColumn('unit', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Еденица измирения',
             'primary' => false,
             ));
        $this->hasColumn('linked', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'primary' => false,
             ));
        $this->hasColumn('type', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Тип',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Catalog', array(
             'local' => 'id_catalog',
             'foreign' => 'id'));

        $this->hasMany('ProductsRefOption', array(
             'local' => 'id',
             'foreign' => 'id_option'));

        $this->hasMany('CatalogFilterValueCondition', array(
             'local' => 'id',
             'foreign' => 'id_option'));
    }
}
<?php

/**
 * BaseNovaposhtaCities
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $code
 * @property string $area_code
 * @property string $description
 * @property string $description_ru
 * @property NovaposhtaAreas $NovaposhtaAreas
 * @property Doctrine_Collection $NovaposhtaWarehouses
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseNovaposhtaCities extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('novaposhta_cities');
        $this->hasColumn('code', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'Код',
             'primary' => true,
             ));
        $this->hasColumn('area_code', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Код региона',
             'primary' => false,
             ));
        $this->hasColumn('description', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Описание',
             'primary' => false,
             ));
        $this->hasColumn('description_ru', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Описание рус',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('NovaposhtaAreas', array(
             'local' => 'area_code',
             'foreign' => 'code'));

        $this->hasMany('NovaposhtaWarehouses', array(
             'local' => 'code',
             'foreign' => 'city_code'));
    }
}
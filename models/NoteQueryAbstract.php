<?

class NoteQueryAbstract extends Doctrine_Query_Abstract
{
    protected  $_isHeadline = false;

    protected function _buildSqlFromPart()
    {
        $q = '';

        foreach ($this->_dqlParts['from'] as $k => $part) {
            if ($k === 0) {
                $q .= $part;
                continue;
            }
            if ( preg_match('/\bJOIN\b/i', $part) ) {
                $q .= ' ' . $part;
            } else {
                $q .= ', ' . $part;
            }
        }

        return $q;
    }
    public function getCountQueryParams($params = array())
    {
        if ( ! is_array($params)) {
            $params = array($params);
        }

        $this->_params['exec'] = $params;

        $params = array_merge($this->_params['join'], $this->_params['where'], $this->_params['having'], $this->_params['exec']);

        $this->fixArrayParameterValues($params);

        return $this->_execParams;
    }



    public function count($params = array())
    {
        $sql = $this->getCountSqlQuery();
        $params = $this->getCountQueryParams($params);
        $results = $this->getConnection()->fetchAll($sql, $params);



        if (count($results) > 1) {
            $count = count($results);
        } else {
            if (isset($results[0])) {
                $results[0] = array_change_key_case($results[0], CASE_LOWER);
                $count = $results[0]['num_results'];
            } else {
                $count = 0;
            }
        }
        return (int) $count;
    }
    protected function _buildSqlSelectPart()
    {
        $q = '';

        $distinct = !empty($this->_dqlParts['distinct']) ? 'DISTINCT ' : '';
        $q = $distinct . implode(', ', $this->_dqlParts['select']);

        return $q;
    }
    protected function _prepareSql()
    {
        if ($this->_isHeadline) {
            $sql = 'SELECT *' .
                        ', ts_headline(description, k, $$StartSel=\'<span class="headline">\', StopSel=\'</span>\'$$) as description' .
                        ', ts_headline(jobtitle, k, $$StartSel=\'<span class="headline">\', StopSel=\'</span>\'$$) as jobtitle FROM ('. $this->getSqlQuery() .') AS foo';
        } else {
            $sql = $this->getSqlQuery();
        }
        return $sql;
    }

    public function fetchOne($params = array(), $hydrationMode = null)
    {
        $collection = $this->execute($params, $hydrationMode);

        if (count($collection) === 0) {
            return false;
        }

        if ($collection instanceof Doctrine_Collection) {
            return $collection->getFirst();
        } else if (is_array($collection)) {
            return array_shift($collection);
        }

        return false;
    }
    protected function _processWherePart()
    {
        foreach ($this->_dqlParts['where'] as $k => $part) {
            if ( preg_match('/\s(AND)|(OR)\s/i', $part) ) {
                $this->_dqlParts['where'][$k] = '('.$part.')';
            }
        }
    }
    public function getSqlQuery($params = array())
    {
        $this->_params['exec'] = $params;
        $this->_execParams = $this->getFlattenedParams();
        $this->fixArrayParameterValues($this->_execParams);

        $q = 'SELECT ' . $this->_buildSqlSelectPart();
        $q .= ' FROM ' . $this->_buildSqlFromPart();

        $this->_processWherePart();

        $q .= ( ! empty($this->_dqlParts['where']))?   ' WHERE '    . implode(' ', $this->_dqlParts['where']) : '';
        $q .= ( ! empty($this->_dqlParts['having']))?  ' HAVING '   . implode(' AND ', $this->_dqlParts['having']) : '';
        $q .= ( ! empty($this->_dqlParts['groupby']))? ' GROUP BY ' . implode(', ', $this->_dqlParts['groupby']) : '';
        $q .= ( ! empty($this->_dqlParts['orderby']))? ' ORDER BY ' . implode(', ', $this->_dqlParts['orderby']) : '';
        $q .= ( ! empty($this->_dqlParts['limit']))?   ' LIMIT ' . implode(' ', $this->_dqlParts['limit']) : '';
        $q .= ( ! empty($this->_dqlParts['offset']))?  ' OFFSET ' . implode(' ', $this->_dqlParts['offset']) : '';

        return $q;
    }
    public function parseDqlQuery($query) {}

}

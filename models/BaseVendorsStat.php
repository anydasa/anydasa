<?php

/**
 * BaseVendorsStat
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $id_vendor
 * @property string $price_config
 * @property integer $count_valid_rows
 * @property integer $count_invalid_rows
 * @property integer $count_updated_rows
 * @property integer $count_inserted_rows
 * @property integer $count_notsaved_rows
 * @property integer $count_saved_rows
 * @property string $fileext
 * @property timestamp $time_processed
 * @property Vendors $Vendors
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseVendorsStat extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('vendors_stat');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'vendors_stat_id',
             ));
        $this->hasColumn('id_vendor', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Поставщик',
             'primary' => false,
             ));
        $this->hasColumn('price_config', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Текущий конфиг',
             'primary' => false,
             ));
        $this->hasColumn('count_valid_rows', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество распознанных строк',
             'primary' => false,
             ));
        $this->hasColumn('count_invalid_rows', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество пропущенных строк',
             'primary' => false,
             ));
        $this->hasColumn('count_updated_rows', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество обновленных строк',
             'primary' => false,
             ));
        $this->hasColumn('count_inserted_rows', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество новых строк',
             'primary' => false,
             ));
        $this->hasColumn('count_notsaved_rows', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество не сохранненых строк !!!',
             'primary' => false,
             ));
        $this->hasColumn('count_saved_rows', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Количество сохранненых строк',
             'primary' => false,
             ));
        $this->hasColumn('fileext', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Расширение файла',
             'primary' => false,
             ));
        $this->hasColumn('time_processed', 'timestamp', null, array(
             'type' => 'timestamp',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Время обновления',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Vendors', array(
             'local' => 'id_vendor',
             'foreign' => 'id'));
    }
}
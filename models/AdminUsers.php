<?php


class AdminUsers extends BaseAdminUsers
{
    static protected $allowedRotes = array();

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('AdminRoutes as Access', array(
                'refClass' => 'AdminUsersRefRoutes',
                'local'    => 'user_id',
                'foreign'  => 'route_id'
            )
        );
    }

    public function getAllowedRoutes()
    {
        if ( empty(self::$allowedRotes[$this->id]) ) {
            if ( 1 == $this->group_id ) { //Группа Администраторы/Супер пользователи
                $sql = 'SELECT r.id, r.name FROM admin_routes r';
                $routes = Doctrine_Manager::connection()->fetchAssoc($sql);
            } else {
                $sql = 'SELECT r.id, r.name FROM admin_routes r
                    RIGHT OUTER JOIN admin_users_ref_routes ua ON r.id = ua.route_id
                    WHERE ua.user_id = ?

                    UNION

                    SELECT r.id, r.name FROM admin_routes r
                    RIGHT OUTER JOIN admin_groups_ref_routes ga ON r.id = ga.route_id
                    WHERE ga.group_id = ?';
                $routes = Doctrine_Manager::connection()->fetchAssoc($sql, array($this->id, $this->group_id));
            }

            foreach ($routes as $route) {
                self::$allowedRotes[$this->id][$route['id']] = $route['name'];
            }
        }
        return self::$allowedRotes[$this->id];
    }

    public function assignAllowedRoutes()
    {
        return $this->getAllowedRoutes();
    }

    public function isAllowedRoute($route_name)
    {
        return (bool) in_array($route_name, $this->getAllowedRoutes());
    }

    static public function getList()
    {
        return Doctrine_Query::create()
            ->from( __CLASS__ )
            ->addWhere('is_enabled = 1')
            ->execute();
    }
}
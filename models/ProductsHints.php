<?php

/**
 * ProductsHints
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class ProductsHints extends BaseProductsHints
{

    public static function getHintByProduct(Products $Product)
    {
        $Query = Doctrine_Query::create()->from('ProductsHints');

        $Query->addWhere("is_enabled = true");

        $Query->addWhere("? ~*  json_extract_path_text(params::json, 'product_title_contains')", $Product->title);
        $Query->addWhere("json_extract_path_text(params::json, 'product_title_notcontains') IS NULL OR ? !~* json_extract_path_text(params::json, 'product_title_notcontains')", $Product->title);
        $Query->addWhere("json_extract_path_text(params::json, 'product_id_brand') IS NULL OR ? = json_extract_path_text(params::json, 'product_id_brand')", $Product->id_brand);

        $Query->orderBy('sort DESC');

        return $Query->fetchOne();
    }
}
<?


class Credit
{
    public static $profile_fields = [
        'fio'       => 'Имя',
        'birthdate' => 'Дата рождения',
        'inn'       => 'ИНН и паспорт',
        'contacts'  => 'Контакты',
        'address'   => 'Адрес проживания',
    ];

    protected static $_instance = null;

    protected $_Companies;



    public static function issetReadyOffer()
    {
        $isReady = false;

        $Offer = self::getParam('Offer');

        if ( !empty($Offer) ) {
            $isReady = true;
        }

        if ( $isReady&& $Offer->isRequiredProfile() && empty(self::getParam('profile')) ) {
            $isReady = false;
        }


        return $isReady;
    }

    public static function setParam($name, $value)
    {
        if ( empty(self::getSession()->params) ) {
            self::getSession()->params = new stdclass;
        }
        self::getSession()->params->{$name} = $value;
    }

    public static function clearParams()
    {
        unset(self::getSession()->params);
    }

    public static function hasParam($name)
    {
        return isset(self::getSession()->params->{$name});
    }

    public static function getParam($name)
    {
        return self::getSession()->params->{$name};
    }

    public static function getAllParams()
    {
        return self::getSession()->params;
    }

    public static function getSerializedData()
    {
        $params = self::getSession()->params;
        $Offer = $params->Offer;

        $return = [
            'Программа' => $Offer->CreditCompany->name  . ' - ' . $Offer->title,
            'Период'    => $params->params['month'] . "мес",
        ];

        if ( !empty($params->params['first_payment_percent']) ) {
            $return['Первоначальный взнос'] = $params->params['first_payment_percent'] . "%";
        }

        if ( !empty($params->profile) ) {
            $return += $params->profile;
        }

        return serialize($return);
    }

    function __construct()
    {
        $this->_Companies = Doctrine_Query::create()
            ->from('CreditCompany c INDEXBY c.id')
            ->leftJoin('c.CreditCompanyOffers o INDEXBY o.id')
            ->addWhere('o.is_enabled = true')
            ->orderBy('c.sort, o.sort')
            ->execute();
    }

    public static function getMinMonthlyPayment($sum)
    {
        if ( $Offer = self::getOfferWithMinMonthlyPayment($sum) ) {
            return $Offer->getMinMonthlyPayment($sum);
        }

        return false;
    }

    public static function getOfferWithMinMonthlyPayment($sum)
    {
        $min   = null;
        $Offer = null;

        foreach (self::getInstance()->getCompaniesWithOffers() as $Company) {
            foreach ($Company->CreditCompanyOffers as $OfferItem) {
                if ( $OfferItem->isAvailable($sum) ) {
                    $monthly_payment = $OfferItem->getMinMonthlyPayment($sum);
                    if ( is_null($min) || $min > $monthly_payment ) {
                        $min = $monthly_payment;
                        $Offer = $OfferItem;
                    }
                }
            }
        }

        return $Offer;
    }

    public static function getOfferById($id)
    {

        foreach (self::getInstance()->getCompaniesWithOffers() as $Company) {
            foreach ($Company->CreditCompanyOffers as $OfferItem) {
                if ( $OfferItem->id == $id ) {
                    return $OfferItem;
                }
            }
        }

        return false;
    }

    public static function destroy()
    {
        self::$_instance = null;
    }

    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function unsetAll()
    {
        self::getSession()->unsetAll();
    }

    protected function saveToSession()
    {
    }

    protected static function getSession()
    {
        return new Zend_Session_Namespace('Credit');
    }


    public function getCompaniesWithOffers($sum = 0)
    {
        return $this->_Companies;
    }
}
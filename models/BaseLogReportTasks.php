<?php

/**
 * BaseLogReportTasks
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property string $emails
 * @property string $phones
 * @property timestamp $executed_at
 * @property integer $exec_interval
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseLogReportTasks extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('log_report_tasks');
        $this->hasColumn('id', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             'sequence' => 'log_report_tasks_id',
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Название',
             'primary' => false,
             ));
        $this->hasColumn('code', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Код',
             'primary' => false,
             ));
        $this->hasColumn('emails', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '{}',
             'comment' => 'Emails',
             'primary' => false,
             ));
        $this->hasColumn('phones', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '{}',
             'comment' => 'Телефоны',
             'primary' => false,
             ));
        $this->hasColumn('executed_at', 'timestamp', null, array(
             'type' => 'timestamp',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Последнее исполнение',
             'primary' => false,
             ));
        $this->hasColumn('exec_interval', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => false,
             'comment' => 'Интервал',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}
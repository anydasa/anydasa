<?php

/**
 * ProductsSetsProduct
 *
 * This class has been auto-generated by the Doctrine ORM Framework
 *
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class ProductsSetsProduct extends BaseProductsSetsProduct
{
    public function setDiscount($data)
    {
        if ( !empty($data) && is_array($data) ) {
            parent::_set('discount', '{' . implode(',', $data) . '}');
        } else {
            parent::_set('discount', NULL);
        }
        return $this;
    }

    public function getDiscount()
    {
        return empty($this->_data['discount']) ? [] : array_map('intval', Site_Db_Function::pg_array_parse($this->_data['discount']));
    }
}
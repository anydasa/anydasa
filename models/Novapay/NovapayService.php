<?

class Novapay_NovapayService
{
    private $private_key = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEA7YQwYDqNbDIbUXZL2HYvZX6wi59DgsMlYjwpoSgHvn0RTnWl
/uR2sIJ9tquYh5Ya+TtUhzrc0N6mZPTLUuyK4qKq2NJaG2Xdr2M5LhMjF46llWkD
qMW/CcMdVTTE4b1FxOOXURNmZ7nWCchuiVU9nm0K5qYAQpPJFzwg9uDljYgcMp3N
9L1WaiyzIqJqL4R9L39j2t4yOUb4m44xyjujGViHm5lQBNklkOcsxMlb0T3AVNcA
+KckSl32TdvvmSx9BwzK4SDqQAR8MtKA4hbeA3KGCflRlJKv5KcCpPOOzaoUlaTN
OLmJIhl0/VjpsKqLNMQOyRSkpRVCFhgp6sO/ewIDAQABAoIBAEBaqbTZCIqBRQ+c
as56rzrjybf67hLXByEHxgvJSdfeETtd+x0GD/ahVKiS8+AA1swivDNrynq5aQI/
pXuRZcwkYQAgdpOn1Rn5W3vVaZOvbcP+0SQAeFOPzznP82xqmSXQuKYaCIwgORMr
gG+rbeeoCeUWo0lmu3yVKSVbKDdhXQqQB0EiC1rZoEKr9iKOAS5Hvi7pU/bxpH3a
xEGh8ov8E89UtubiIL+LhqsN5dVgowWiaPbA945z497VgjAu+/I2jQ6HhuyPNhK1
SMa0LExXN5xmzF3WJ8ofVMS/hJbLYWgidfUi+MYCgm3W1YtpnNFBKDiDa83cEWqo
r5/GJYECgYEA9w9yXkAsUe3c3Qw1lGSZJNzSlbwCKA4wMaiLhsZWfyLrkzzQnY+t
tu+5Tfa3EU3bLof3juwSBSVwYcb3ZJWGeVURYQwO3ZirZQ3xxEQTI5FTCuJRPMsf
8oiq6NxgZrK3NdtmtecH6+IvrO/J3UPKc8a+iop3zR2tTU4rs2jAbMECgYEA9hxV
yAGXZavNHfrqIn9NCZLySaYrMKSZj36xCcksHK7do77OmsqAp8wQuxYIPoABNO/8
bd2ry33b8Zd3dv5iRI3GMmmrxQ7yDviKeUptKKBZsm9CWEcGnmNOia+ZyJbRl9Fp
jnrtUNOCQxwz144eTAKLV2JUD6Kgfr1ee1EDbzsCgYEA7Q0zLV/hppLWUno+hq2n
i4kdvXHxl8FVWLBhf+WaZM56voGhoSyU/2wwnq/Uo5PSdGkdjVLRT4LGu+qOwUH/
DzgiPr21HcY43fNtQGYY/w2XYmAYln5HnwynAFtDXAaqZ9CmUm7kWN5j5EkHpXhA
LqpJdOC7ZmHNQNl6cOBXkYECgYA7Qi9VbSyrCmblJRljHQvLllpIaX5UxA1Fg9fU
5197uI8dckAE/WVlAbm1kmSByAiCWpaJTaqj4LYowbO+LxoyL4DdepwlYqfd+vI8
qjMGaTWvxSJQZyms0XSDqoh4x/fHemDUMb0ajRL8XboN2OZqnuI2NDLRYPMMEUTC
pIsTKQKBgGqbsr+cn+Ny2cPO8KCx1y6WWrT2X5k286rjAMGxNNG/aNqejpTixAaJ
dA8rov0FcJ03MOhw69XYxkVGpLqWhtMjiWSYuHJBSspvp0QcD9nQykXDLfO7FeeA
p8WexL0FZSkNlkMbcpMI6U0g51cwacZeGA3qXoKWzWfz2Brmom90
-----END RSA PRIVATE KEY-----
EOD;

    private $public_key = <<<EOD
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw1FeLVQlCYMnxVMPwhHA
AYik6KGfYz0GJW0SP4dBs6XQ2Ap2kP0X3K5WtJNnPehiWf7jJz9XH2Xh/17t37kZ
KXGEdWYtPUAWQItLGSIwmPMau+YBFFvLD8OReFhFXc6sjReSPJSFV8KDtOP7By9u
+KxYqZTVqPxCeYXHOzT7vtDJBJDLbe0pJ3B3wRihMEuHP54X4zqEAi/vbqArhHDD
O07FZpQ3PA/Fkgj8jMTUxU3LxmIIkNIuLz+Ze/PxL88qvRkRoHd73agYSs5bVdCg
urGUs2hGFQap4KiyR0TRtaJujM715y1gjVFN7Khkkol/dJaHRqxUaZv3dlL+RMXG
/wIDAQAB
-----END PUBLIC KEY-----
EOD;

    private $prod_private_key = <<<EOD
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAzKm5ddNdsx3ZF5t3oN8xu6Dvclvlbz3AEOM1P5zk2sejUOE0
DV+jphAqJlg7DrALZnCL+yAYIqZhwExQTgCZudFjwC7zp8uukic4gm6CRn0M1xQY
uellKnexn64QtXwe3e+X45AjVjjhwLXo2UnkeZ5DBeHCKcX5jamE1Lx0virBqTiT
hV5/OMVzpLNPh+vg9plkQ8wv5GZlVw0Idl6bxiq+wzNxac7nslZq7wue+3A6/8us
MtLDZlf7UGqhgMYXcEdD8148NvPdkNsW/3Yfjd5WjG2Zc8H/xbpXbSNU2XboWKfp
ZL4AzuFmoOLJzQS67f2QLDKsQBP1BGFGPzxGSQIDAQABAoIBAQCIda4LyseR8xfZ
rrY/1DGDyhV9aZp75m9Wkzfg10qcOTpCZjXfqOIjimmgMXoHHAgt4GLlKQwOpmNM
5VUx/SKCjAN3k5awTZVzK6V5ChfgyeYyzdqCswTaIP6U2mmXqE+oaOUcPPHivoB/
jorXvnYRRR2pIVLlqXPwKZRAh/tLA2OyPNCjkaYBrVsldE4T0SpQlVD5q5HgLLf6
VlK5GDqGu2trODbGamOCnr6Of/ZABC1rulTCGL8ou2gcwjrEbiW15bU+O2PC1rwt
MN9ZwKyhRQjg5ApymeGFupC646wmFb1KGjHcMYArtXViWujOEl0BceBVoCywLjK5
vVOa1CIBAoGBAO2qcebo9fTJtoS4SV8195ugY4psubfGlK5ncVGJW5cLyfb/Pr87
X/+7cUa1fIos+dsz88TBFpXRLkXKXLDg56giqvN29Q+PwFh7G8ZfTy6aFOJodLew
Z6KHS3PRvZvD4YUHxhSbW72PdMpn8AhBGORP+PH/ZxT0Ozt1T6Sj5hhBAoGBANxz
hZM2RmZWaM3abO3pXEnHbDP8P0Wik2WFC7et229D+8FJvMhDq19U88TqurqP2A6E
AHE1+yNc0vN6Ad/IgQ4OpwXgxQBBgYyqk7fuYYA0jg2fY2xNP8EL0KLKnt3oFL6H
PBZ+XQFljcBaYxMja/a/HREZRw0+b+SXEMGfamwJAoGAZt0nkUZw9bK8QXM1pnTI
J15i1s26D/Vt8pIo9J5buuNKVZIhyUjcguXQ4Ea6kV9oANzae+UKbmtr+J7eQVwG
R7pBfUCWTlbsbB9UZSZpMgVUVq3ZHovGPw0JtiYoR5ufJyG3NMINEd1eMiCMPjZW
nLKUT8E4D1SpslFS2DJr60ECgYBczLR6F+riwZU0srIQJKAY3r4J1DG+WoW5wpwA
IkuxNukPoL67fuDxLI3BYx5f0pWc5qxY++RQKo8St1V6oqjRBhIBQ3EqfmjOki/E
ybB5i6YQzfvIZ2xjE1osbScbOssguUTuIxt7rwhQ5Lu4Hr0gDatquz1RsergS0de
eoEOIQKBgQCwuaImgNMyHbWiWj0tB9iVvknZVPaEGc549Rs0FJbXHaUdeQXG4NOj
Xvcob2xF/N8aasiHw1BsdY2swXvJqFJVBJh+2EtAPf6FCGrKsfw2ykaZkTzRxylt
vBf8AB+CA2f+xh1Nk3m1HRZLENlHeBwMQvKsu0MjQUEfruksxNtDpQ==
-----END RSA PRIVATE KEY-----
EOD;


    private $prod_public_key = <<<EOD
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzKm5ddNdsx3ZF5t3oN8x
u6Dvclvlbz3AEOM1P5zk2sejUOE0DV+jphAqJlg7DrALZnCL+yAYIqZhwExQTgCZ
udFjwC7zp8uukic4gm6CRn0M1xQYuellKnexn64QtXwe3e+X45AjVjjhwLXo2Unk
eZ5DBeHCKcX5jamE1Lx0virBqTiThV5/OMVzpLNPh+vg9plkQ8wv5GZlVw0Idl6b
xiq+wzNxac7nslZq7wue+3A6/8usMtLDZlf7UGqhgMYXcEdD8148NvPdkNsW/3Yf
jd5WjG2Zc8H/xbpXbSNU2XboWKfpZL4AzuFmoOLJzQS67f2QLDKsQBP1BGFGPzxG
SQIDAQAB
-----END PUBLIC KEY-----
EOD;


    private $merchantId = '51043870-9125-11ea-b07a-615c418e924f';

    const SESSION_CREATE_ENDPOINT = 'https://api-ecom.novapay.ua/v1/session'; //https://api-qecom.novapay.ua/v1/session

    const PAYMENT_ENDPOINT = 'https://api-ecom.novapay.ua/v1/payment'; //'https://api-qecom.novapay.ua/v1/payment'

    const COMPLETE_HOLD_ENDPOINT = 'https://api-ecom.novapay.ua/v1/complete-hold'; //'https://api-qecom.novapay.ua/v1/complete-hold'

    const PAYMENT_STATUS_ENDPOINT = 'https://api-ecom.novapay.ua/v1/get-status'; //'https://api-ecom.novapay.ua/v1/get-status'

    public function beginSession(
        $clientPhone = '',
        $callbackUrl = '',
        $successUrl = ''
    ) {
        $client = null;

        $clientPhone = str_replace(' ', '', $clientPhone);
        $clientPhone = str_replace('-', '', $clientPhone);
        $clientPhone = str_replace('(', '', $clientPhone);
        $clientPhone = str_replace(')', '', $clientPhone);

        $postRequest = array(
            'merchant_id' => $this->merchantId,
            'client_phone' => $clientPhone,
            'callback_url' => $callbackUrl,
            'success_url' => $successUrl
        );

        $jsonRequest = json_encode($postRequest, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

        $apiResponseDecoded = $this->makeRequest(self::SESSION_CREATE_ENDPOINT, $jsonRequest);

        $paymentSessionId = $apiResponseDecoded['id'];

        return $paymentSessionId;
    }

    public function payment($paymentSessionId, $amount) {
        $postRequest = array(
            'merchant_id' => $this->merchantId,
            'session_id' => $paymentSessionId,
            'use_hold' => true,
            'amount' => $amount,
        );

        $jsonRequest = json_encode($postRequest, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

        $apiResponseDecoded = $this->makeRequest(self::PAYMENT_ENDPOINT, $jsonRequest);

        $url = $apiResponseDecoded['url'];
        
        return $url;
    }
    
    public function paymentConfirmation($paymentSessionId) {
        $postRequest = array(
            'merchant_id' => $this->merchantId,
            'session_id' => $paymentSessionId,
        );

        $jsonRequest = json_encode($postRequest, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

        $apiResponseDecoded = $this->makeRequest(self::COMPLETE_HOLD_ENDPOINT, $jsonRequest);

        $status = $apiResponseDecoded['status'];
        
        return $status;
    }
    
    public function getStatus($paymentSessionId) {
        $postRequest = array(
            'merchant_id' => $this->merchantId,
            'session_id' => $paymentSessionId,
        );

        $jsonRequest = json_encode($postRequest, JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);

        $apiResponseDecoded = $this->makeRequest(self::PAYMENT_STATUS_ENDPOINT, $jsonRequest);

        $status = $apiResponseDecoded['status'];
        
        return $status;
    }
    
    private function makeRequest($url, $jsonRequest) {
        $binary_signature = '';

        openssl_sign($jsonRequest, $binary_signature, $this->prod_private_key, OPENSSL_ALGO_SHA1);
        $xSign = base64_encode($binary_signature);

        $cURLConnection = curl_init($url);
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $jsonRequest);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, array(
            'Content-type:application/json',
            'x-sign: ' . $xSign
        ));

        $apiResponse = curl_exec($cURLConnection);

        $apiResponseDecoded = json_decode($apiResponse, true);

        curl_close($cURLConnection);

        return $apiResponseDecoded;
    }
}

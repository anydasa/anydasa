<?php

/**
 * BasePromotionsTypes
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $id
 * @property string $title
 * @property integer $sort
 * @property Doctrine_Collection $Promotions
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasePromotionsTypes extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('promotions_types');
        $this->hasColumn('id', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'comment' => 'ID',
             'primary' => true,
             ));
        $this->hasColumn('title', 'string', null, array(
             'type' => 'string',
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'comment' => 'Название',
             'primary' => false,
             ));
        $this->hasColumn('sort', 'integer', 4, array(
             'type' => 'integer',
             'length' => 4,
             'fixed' => false,
             'unsigned' => false,
             'notnull' => true,
             'default' => '0',
             'comment' => 'Сортировка',
             'primary' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Promotions', array(
             'local' => 'id',
             'foreign' => 'id_type'));
    }
}
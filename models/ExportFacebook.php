<?php

/**
 * Created by PhpStorm.
 * Date: 02.01.2017
 */
class Products_Export_Facebook
{
    protected static $availabilityLabels = [
        0 => 'out of stock', //Нет в наличии
        1 => 'in stock', //В наличии
        2 => 'preorder', //Ожидается
        3 => 'in stock', // На складе
    ];

    protected static $stateLabels = [
        'new' => 'new',
        'certrefurbished' => 'refurbished',
        'used' => 'used',
        'refurbished' => 'refurbished',
        'userrefurbished' => 'refurbished',
    ];

    // Max size: 100
    public static function getTitle($Product, $params)
    {
        if (!empty($Product['type'])) {
            $title = $Product['type'] . ' ' . $Product['title'];
        } else {
            $title = $Product['title'];
        }
        return mb_substr($title, 0, 100);
    }

    public static function getAvailability($Product, $params)
    {
        return self::$availabilityLabels[$Product['status']];
    }

    public static function getCondition($Product, $params)
    {
        return self::$stateLabels[$Product['id_state']];
    }

    // Если есть описание, то используем его какое есть.
    // Если описания нет, то дублируем в описание название товара.
    // в FB есть ограничение на длину описания 5000 символов
    public static function getDescription($Product, $params)
    {
        $description = trim(html_entity_decode(strip_tags($Product['description'])));
        if (empty($description) || !preg_match('#\w#', $description)) {
            if (!empty($Product['type'])) {
                $title = $Product['type'] . ' ' . $Product['title'];
            } else {
                $title = $Product['title'];
            }
            $description = trim(htmlspecialchars($title));
        }

        if (!empty($params['append_text'])) {
            $description .= ' ' . $params['append_text'];
        }

        return mb_substr($description, 0, 5000);
    }

    public static function getImageLink($Product, $params, $config)
    {
        $imageLink = current($Product['images']);
        if(empty($imageLink)) {
            $imageLink = $config['url']['domain'] . 'img/nophoto/big.png';
        }

        return $imageLink;
    }

    public static function getLink($Product, $params)
    {
        return $Product['url']
            . '?utm_source=fb&utm_campaign=fb-remarketing&utm_medium=cpc&utm_term='
            . Site_Function_Slug::slugify(self::getTitle($Product, $params));
    }
}
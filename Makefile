-include .env
-include .env.local
export

ARGS = $(filter-out $@,$(MAKECMDGOALS))

.PHONY: fix-permission
fix-permission: ## fix permission for docker env
	sudo chown -R $(shell whoami):$(shell whoami) ./vendor
	sudo chmod 0777 -R ./frontend/logs/
	sudo chmod 0777 -R ./frontend/sessions/
	sudo chmod 0777 -R ./backend/logs/
	sudo chmod 0777 -R ./backend/logs/app/
	sudo chmod 0777 -R ./backend/sessions/
	sudo chmod 0777 -R ./pix/img/products/
	sudo chmod 0777 -R ./cache/
	sudo chmod 0777 -R ./pix/img/menu
	sudo chmod 0777 -R ./frontend/public/


once:
	docker-compose exec php php /var/www/cron/once/${ARGS}

.PHONY: composer
composer: ## Update project dependencies
		docker-compose exec php composer ${ARGS}

.PHONY: dump-db
dump-db: ## Dump DB
		docker-compose exec db pg_dump -h 127.0.0.1 anydasa > sql.sql

.PHONY: compass-compile
compass-compile: ## Compile css
		docker-compose exec compass compass compile /src/sass/

.PHONY: help
help: ## Display this help message
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*## *" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

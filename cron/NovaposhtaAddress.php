<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$apiKey = '48e370da15e2ce8b59353133494e2ac5';

function getNP($jsonParams)
{
    return
        json_decode(
            file_get_contents('https://api.novaposhta.ua/v2.0/json/',null, stream_context_create(array(
                'http' => array(
                    'method'           => 'POST',
                    'header'           => "Content-type: application/x-www-form-urlencoded;\r\n",
                    'content'          => json_encode($jsonParams),
                ),
            )))
            , true);
}

$data = getNP(["apiKey" => $apiKey, "modelName" => "Address", "calledMethod" => "getAreas"])['data'];
if (!empty($data)) {
    Doctrine::getTable('NovaposhtaAreas')->findAll()->delete();
    foreach ($data as $item) {
        if (!($Area = Doctrine::getTable('NovaposhtaAreas')->findOneByCode($item['Ref']))) {
            $Area = new NovaposhtaAreas;
            $Area->code = $item['Ref'];
            $Area->description = $item['Description'];
            $Area->description_ru = empty($item['DescriptionRu']) ? $item['Description'] : $item['DescriptionRu'];
            $Area->save();
        }
    }
}

foreach (getNP(["apiKey" => $apiKey, "modelName" => "Address", "calledMethod" => "getCities"])['data'] as $item) {
    if ( ! ($City = Doctrine::getTable('NovaposhtaCities')->findOneByCode($item['Ref'])) ) {
        $City = new NovaposhtaCities;
        $City->code             = $item['Ref'];
        $City->area_code        = $item['Area'];
        $City->description      = $item['Description'];
        $City->description_ru   = empty($item['DescriptionRu']) ? $item['Description'] : $item['DescriptionRu'];
        $City->save();
    }
}

foreach (getNP(["apiKey" => $apiKey, "modelName" => "Address", "calledMethod" => "getWarehouses"])['data'] as $item) {
    if ( ! ($Warehouse = Doctrine::getTable('NovaposhtaWarehouses')->findOneByCode($item['Ref'])) ) {
        $Warehouse = new NovaposhtaWarehouses;
        $Warehouse->code             = $item['Ref'];
        $Warehouse->city_code        = $item['CityRef'];
        $Warehouse->description      = $item['Description'];
        $Warehouse->description_ru   = empty($item['DescriptionRu']) ? $item['Description'] : $item['DescriptionRu'];
        try {
            $Warehouse->save();
        } catch (Exception $e) {}
    }
}

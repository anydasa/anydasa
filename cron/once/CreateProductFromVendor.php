<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once __DIR__ . '/../setupConfig.php';

$Res = Doctrine_Query::create()
    ->from('VendorsProducts t1')
    ->addWhere('t1.id_vendor = 181')
    ->addWhere('t1.id_product IS NULL')
    ->addWhere('t1.is_enabled = 1')
    ->limit(1000)
    ->execute();

/** @var VendorsProducts $VendorProduct */
foreach ($Res as $VendorProduct) {
    $params = json_decode($VendorProduct->comment, true);

    $productCode = $VendorProduct->id_vendor . '-' . $VendorProduct->code;

    /** @var Products $Product */
    $Product = Doctrine::getTable('Products')->findOneByCode($productCode);

    if (!$Product) {
        $Product = new Products();
        $Product->code = $productCode;
        $Product->alias = $VendorProduct->title;
    }

    $Brand = getBrand($VendorProduct);
    $Catalog = getCatalog($VendorProduct);

    $Product->title = $VendorProduct->title;
    $Product->description = $VendorProduct->description;
    $Product->id_vendor_product = $VendorProduct->id;
    $Product->id_currency = $VendorProduct->id_currency;
    $Product->price_retail = $VendorProduct->price_retail;
    $Product->ProductsBrands = $Brand;
    $Product->link('Catalogs', [$Catalog->id]);

    try {
        $Product->save();
    } catch (Exception $e) {
        echo "\n {$e->getMessage()}";
        echo "\n {$VendorProduct->id} / {$VendorProduct->title}";
        continue;
    }

    $VendorProduct->id_product = $Product->id;
    $VendorProduct->save();

//    setOption($Product, $VendorProduct, $Catalog);
    setExternalLink($Product, $VendorProduct);
    setMargin($VendorProduct);
    setImages($Product, $VendorProduct);
}

function setImages(Products $Product, VendorsProducts $VendorProduct)
{
    if ($Product->getImage()->getImagesCount() > 0) {
        return;
    }

    foreach ($VendorProduct->getImages() as $image) {
        $Product->getImage()->addNewImage($image);
    }
}

function setMargin(VendorsProducts $VendorProduct)
{
    $VendorProduct->Products->ProductsMargins->fromArray([
        'id_currency' =>  Currency::getByISO('UAH')->id,
        'price_min' =>  0.00,
        'price_partner' =>  0.00,
        'price_trade' =>  0.00,
        'price_retail' =>  0.00,
        'type_min' => 'present',
        'type_partner' => 'present',
        'type_trade' => 'present',
        'type_retail' => 'present',
        'round_min' => 2,
        'round_partner' => 2,
        'round_trade' => 2,
        'round_retail' => 2,
        'use_retail' => true
    ]);

    $VendorProduct->Products->ProductsMargins->save();
}

function setExternalLink(Products $Product, VendorsProducts $VendorProduct)
{
    $productsLinks = new ProductsLinks();

    $productsLinks->fromArray([
        'id_product' => $Product->id,
        'url' => $VendorProduct->url,
        'is_inexact' => false,
    ]);

    try {
        $productsLinks->save();
        return true;
    } catch (Exception $e) {
    }

    return false;
}

function setOption(Products $Product, VendorsProducts $VendorProduct, Catalog $Catalog)
{
    $params = json_decode($VendorProduct->comment, true);

    foreach ($params as $name => $param) {
        $Option = getOption($name, $Catalog);
        addOptionValue($Product, $Option, $param);
    }
}

function addOptionValue(Products $Product, ProductsOption $Option, $value): bool
{
    $ProductsRefOption = new ProductsRefOption;
    $ProductsRefOption->fromArray([
        'id_product' => $Product->id,
        'id_option' => $Option->id,
        'values' => $value,
    ]);

    try {
        $ProductsRefOption->save();
        return true;
    } catch (Exception $e) {
    }

    return false;
}

function getOption(string $name, Catalog $Catalog): ProductsOption
{
    $Option = Doctrine::getTable('ProductsOption')->findOneByTitleAndIdCatalog($name, $Catalog->id);
    if (!$Option) {
        $Option = new ProductsOption();
        $Option->title = $name;
        $Option->id_catalog = $Catalog->id;
        $Option->type = ProductsOption::TYPE_TEXT;
        $Option->save();
    }

    return $Option;
}

function getCatalog(VendorsProducts $VendorProduct): Catalog
{
    $params = json_decode($VendorProduct->comment, true);
    $catalogName = ' ---- TRANSIT / ' . $params['categoryName'];

    $Catalog = Doctrine::getTable('Catalog')->findOneByTitle($catalogName);
    if (!$Catalog) {
        $Catalog = new Catalog();
        $Catalog->title = $catalogName;
        $Catalog->save();
    }

    return $Catalog;
}

function getBrand(VendorsProducts $VendorProduct): ProductsBrands
{
    $Brand = Doctrine_Query::create()
        ->from('ProductsBrands b')
        ->where('LOWER(b.title) LIKE LOWER(?)', $VendorProduct->brand)
        ->fetchOne();

    if (!$Brand) {
        $Brand = new ProductsBrands();
        $Brand->alias = $VendorProduct->brand;
        $Brand->title = $VendorProduct->brand;
        $Brand->save();
    }

    return $Brand;
}

<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once __DIR__.'/../setupConfig.php';

$Res = Doctrine_Query::create()
        ->from('Series t1')
        ->execute();

/** @var Series $Item */
foreach ($Res as $Item) {
    if ($Item->SeriesProducts->count() < 2) {
        $Item->delete();
    }
}

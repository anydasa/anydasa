<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once __DIR__.'/../setupConfig.php';

$Res = Doctrine_Query::create()
        ->from('VendorsProducts t1')
        ->addWhere('t1.id_vendor = 181')
        ->addWhere('t1.id_product IS NOT NULL')
        ->execute();


foreach ($Res as $Item)
{
    $Item->Products->ProductsMargins->fromArray([
        'id_currency' =>  Currency::getByISO('UAH')->id,
        'price_min' =>  0.00,
        'price_partner' =>  0.00,
        'price_trade' =>  0.00,
        'price_retail' =>  0.00,
        'type_min' => 'present',
        'type_partner' => 'present',
        'type_trade' => 'present',
        'type_retail' => 'present',
        'round_min' => 2,
        'round_partner' => 2,
        'round_trade' => 2,
        'round_retail' => 2,
        'use_retail' => true
    ]);

    $Item->Products->ProductsMargins->save();
}

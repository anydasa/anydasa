<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once __DIR__.'/../setupConfig.php';

$idCatalog = 243;

$Res = Products_Query::create()
        ->setParamsQuery(['id_catalog' => $idCatalog])
        ->execute();

$Catalog =  Doctrine::getTable('Catalog')->find($idCatalog);
if (!$Catalog) {
    throw new \Exception('Catalog not found');
}

/** @var Products $Product */
foreach ($Res as $Product) {
    $title = trim($Product->title);

    if (preg_match('/^.*?(\d+(:?\.\d+)?)g.*$/', $title, $pregRes)) {
        $Option = getOption('Вес', $Catalog);
        addOptionValue($Product, $Option, $pregRes[1]);
    }

    if (preg_match('/^.*?(\d+(:?\.\d+)?)mm.*$/', $title, $pregRes)) {
        $Option = getOption('Размер', $Catalog);
        addOptionValue($Product, $Option, $pregRes[1]);
    }

    if (preg_match('/^.*\((.*)\)$/', $title, $pregRes)) {
        $Option = getOption('Цвет', $Catalog);
        addOptionValue($Product, $Option, trim($pregRes[1]));
    }
}

function getOption(string $name, Catalog $Catalog): ProductsOption
{
    $Option =  Doctrine::getTable('ProductsOption')->findOneByTitleAndIdCatalog($name, $Catalog->id);
    if (!$Option) {
        $Option = new ProductsOption();
        $Option->title = $name;
        $Option->id_catalog = $Catalog->id;
        $Option->type = ProductsOption::TYPE_TEXT;
        $Option->save();
    }

    return $Option;
}

function addOptionValue(Products $Product, ProductsOption $Option, $value, $overwrite = false): bool
{
    $ProductsRefOption = null;

    if ($overwrite) {
        $ProductsRefOption = Doctrine::getTable('ProductsRefOption')->findOneByIdProductAndIdOption($Product->id, $Option->id);
    }

    if (!$ProductsRefOption) {
        $ProductsRefOption = new ProductsRefOption;
    }

    $ProductsRefOption->fromArray([
        'id_product' => $Product->id,
        'id_option' => $Option->id,
        'values'    => $value,
    ]);

    try {
        $ProductsRefOption->save();
        return true;
    } catch (Exception $e) {
    }

    return false;
}

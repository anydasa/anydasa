<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$Query = Doctrine_Query::create()
    ->from('ProductsLinks')
    ->where("COALESCE(prices_updated_at, DATE '0001-01-01') < NOW() - INTERVAL '3 hour'")
    ->addWhere("url ILIKE '%hotline.ua%'")
    ->addWhere('id_product IN (SELECT t1.id_product FROM ProductsPricingHotline t1 WHERE t1.is_enabled = true)');

foreach ($Query->limit(20)->execute() as $Link) {
    $Link->updateRemotePriceList();
    sleep(1);
}

if ( $Query->count() < 10 ) {

    $Query = Doctrine_Query::create()
        ->from('ProductsLinks')
        ->where("COALESCE(prices_updated_at, DATE '0001-01-01') < NOW() - (INTERVAL '1 second' * interval_update_price)");

    foreach ($Query->limit(20)->execute() as $Link) {
        $Link->updateRemotePriceList();
        sleep(1);
    }

}


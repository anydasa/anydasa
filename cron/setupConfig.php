<?
$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__)) .'/frontend/public/';

error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../etc/functions.php';
require_once __DIR__.'/../etc/config.php';

date_default_timezone_set($config['timezone']);

ini_set('memory_limit', '300M');

$paths = implode(PATH_SEPARATOR, array(
        $config['path']['lib'],
        $config['path']['appLib'],
        $config['path']['doctrineModels'],
        $config['path']['src'],
        SRC_PATH,
    )
);

$paths = get_include_path() . ':' . $paths;
set_include_path($paths);

Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);
$connection = new Site_Db_Connection($config['db']['dsn'], $config['db']['username'], $config['db']['password']);
Site_Db_Doctrine::init($connection, $config['path']['doctrineModels']);

$registry = new Zend_Registry();
Zend_Registry::setInstance($registry);


$registry->config = new Zend_Config($config);


$routes = new Zend_Controller_Router_Rewrite;
$routes->addConfig(new Zend_Config_Ini($config['path']['app'] . 'etc/routers/core.ini', 'core'), 'routes');
foreach (glob($config['path']['app'] . 'etc/routers/modules/*.ini') as $filename) {
    $routes->addConfig(new Zend_Config_Ini($filename, basename($filename, '.ini')), 'routes');
}

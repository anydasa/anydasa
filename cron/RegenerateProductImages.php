<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';


$Query = Doctrine_Query::create()
    ->from('Products')
    ->orderBy('id')
//    ->limit(50)
    ->offset(0);

foreach ($Query->execute() as $Product) {
    $Product->getImage()->regenerate();
}
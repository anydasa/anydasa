<?php

use Twig\Environment;
use Twig\Loader\ArrayLoader;

error_reporting(E_ERROR | E_WARNING | E_PARSE);



include_once 'setupConfig.php';

if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('post-order-accessories') ) {
    throw new Exception('Отсутсвует почтовый шаблон post-order-accessories (MailTemplates)');
}

$sender_type = 'accessories';

$Res = Doctrine_Query::create()
        ->from('Orders o')
        /*->leftJoin('o.OrdersProducts op')
        ->addWhere('o.id_status = ?', 'complete')
        ->addWhere("o.updated < NOW() - INTERVAL '5 days'")
        ->addWhere("o.email ~ '@'")
        ->addWhere('op.code IN (SELECT p.code FROM Products p WHERE id_set IS NOT NULL)')
        ->addWhere("NOT EXISTS(SELECT * FROM orders_sender os WHERE os.id_order = o.id AND os.type = ?)", $sender_type)*/
        ->where('id = 31223')
        ->limit(1)
        ->execute();

foreach ($Res as $Item)
{

    $ordered_products_codes = $Item->Products->toKeyValueArray('code', 'code');

    $Products = Doctrine_Query::create()
        ->from('Products')
        ->whereIn('code', $ordered_products_codes)
        ->addWhere('id_set IS NOT NULL')
        ->execute()
    ;


    $return = [];
    $exclude = [];

    foreach ($Products as $Product) {
        $ProductsSetsProduct = $Product->Set->getProductsInComplect();
        $ProductsSetsProduct->loadRelated('Products');

        foreach ($ProductsSetsProduct as $ProductSet) {

            if ( in_array($ProductSet->Products->code, $ordered_products_codes) ) {
                $exclude []= [
                    'parent_code' => $Product->code,
                    'group_num'   => $ProductSet->group_num,
                ];
            }

            $product_temp = [
                'parent_code' => $Product->code,
                'code'        => $ProductSet->Products->code,
                'group_num'   => $ProductSet->group_num,
                'url'         => $ProductSet->Products->getUrl(),
                'name'        => $ProductSet->Products->getFullName(),
                'price'       => $ProductSet->Products->getFormatedPrice('retail', 'UAH', true)
            ];

            if ( $ProductSet->Products->getImage()->getImagesCount() ) {
                $product_temp['img'] = $ProductSet->Products->getImage()->getFirstImage()['small']['url'];
            }

            $return[$ProductSet->Products->code] = $product_temp;
        }
    }

    $return = array_filter($return, function ($item) use (& $exclude) {
        foreach ($exclude as $exclude_item) {
            if ( $exclude_item['parent_code'] == $item['parent_code'] && $exclude_item['group_num'] == $item['group_num'] ) {
                return false;
            }
        }
        $exclude [] = [
            'parent_code' => $item['parent_code'],
            'group_num'   => $item['group_num'],
        ];
        return true;
    });





    Zend_Debug::dump($return); exit;

    $data = [];

    $Products = Doctrine_Query::create()
        ->from('Products')
        ->whereIn('code', $Item->Products->toKeyValueArray('code', 'code'))
        ->addWhere('id_set IS NOT NULL')
        ->execute()
    ;
    $prod = [];

    foreach ($Products as $Product) {
        $ProductsSetsProduct = $Product->Set->getProductsInComplect();
        $ProductsSetsProduct->loadRelated('Products');

        foreach ($ProductsSetsProduct as $ProductSet) {
            $discount =  max(
                array_filter($ProductSet->discount, function ($item) {
                    return $item < 40;
                })
            );
            $old_price = $ProductSet->Products->getPrice('retail', 'UAH');
            $price = $old_price - ($old_price * $discount / 100);

            $prod[] = [
                'cat'       => current($ProductSet->Products->getCatalogsId()),
                'id'        => $ProductSet->Products->id,
                'name'      => $ProductSet->Products->getFullName(),
                'discount'  => $discount,
                'price'     => Currency::format($price, 'UAH', true),
                'old_price' => Currency::format($old_price, 'UAH', true),
            ];
        }
    }

    Zend_Debug::dump($prod); exit;


    Zend_Debug::dump($Products->toKeyValueArray('code', 'title')); exit;

    if ( $Item->Products->count() && $Product = Doctrine::getTable('Products')->findOneByCode($Item->Products->get(0)->code) ) {
        $data['product_url'] = $Product->getUrl();
    }

    if ( !empty($Item->email) ) {
        $Twig = new Environment(new ArrayLoader([
            'subject' => $MailTemplate->subject
        ]));
        $subject = $Twig->render('subject', $data);

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($MailTemplate->renderHTML($data));
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($Item->email, $Item->name);
        $mail->setSubject($subject);
        $mail->send();

    }

    $Log = new OrdersSender;
    $Log->id_order = $Item->id;
    $Log->type = $sender_type;
    $Log->save();

}
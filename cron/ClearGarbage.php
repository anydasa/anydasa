<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

exec('find '.$config['session']['save_path'].' -size  -100b -name "sess*" -mmin +60 -delete');

foreach (glob($config['session']['save_path'].'*') as $filename) {
    if ( (time()-filectime($filename)) > $config['session']['gc_maxlifetime'] ) {
        unlink($filename);
    }
}
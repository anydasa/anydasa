<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$tpl = Doctrine::getTable('SubscribeTemplates')->findOneByCode('daily_price');

foreach (Doctrine_Query::create()->from('Users')->where('is_recive_price = 1')->execute() as $User)
{
    $price = str_replace('price_', '', $User->price);

    $body_text = $tpl->body_text;
    $body_html = $tpl->body_html;

    $unsubsribe_href = $config['url']['domain'].'account/unsubscribe/price/?token='.$User->token;
    $body_html = str_replace('{{UNSUBSCRIBE_HREF}}', $unsubsribe_href, $body_html);
    $body_text = str_replace('{{UNSUBSCRIBE_HREF}}', $unsubsribe_href, $body_text);

    $body_html = str_replace('{{URL_PRICE_WITH_IMAGES}}', $config['url']['pix'].'files/prices/'.$price.'-with-images.xlsx', $body_html);
    $body_text = str_replace('{{URL_PRICE_WITH_IMAGES}}', $config['url']['pix'].'files/prices/'.$price.'-with-images.xlsx', $body_text);

    $mail = new Site_Mail('UTF-8');
    $mail->setBodyText($body_text);
    $mail->setBodyHtml($body_html);
    $mail->setFrom($tpl->from_email, $tpl->from_name);
    $mail->addTo($User->email, $User->name);

    foreach ($User->UsersEmails as $Email) {
        $mail->addTo($Email->email, $Email->name);
    }

    $mail->setSubject($tpl->subject);

    $path = $config['path']['pix'].'files/prices/'.$price.'-with-groups.xlsx';
    $at = new Zend_Mime_Part( file_get_contents($path) );
    $at->type        = mime_content_type($path);
    $at->disposition = Zend_Mime::DISPOSITION_INLINE;
    $at->encoding    = Zend_Mime::ENCODING_BASE64;
    $at->filename    = 'Арморс. '.date('d.m.Y').'.xlsx';
    $mail->addAttachment($at);

    $mail->send();
}
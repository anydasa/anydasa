<?php

use Twig\Environment;
use Twig\Loader\ArrayLoader;

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('hotline-comment') ) {
    throw new Exception('Отсутсвует почтовый шаблон hotline-comment (MailTemplates)');
}


$Res = Doctrine_Query::create()
        ->from('Orders')
        ->addWhere('id_status = ?', 'complete')
        ->addWhere("updated < NOW() - INTERVAL '5 days'")
        ->addWhere("email ~ '@'")
        ->addWhere('NOT EXISTS(SELECT * FROM orders_sender WHERE id_order = id)')
        ->limit(10)
        ->execute();

foreach ($Res as $Item)
{
    $data = [
        'user_name'    => $Item->name,
        'product_name' => $Item->Products->count() ? $Item->Products->get(0)->title : 'товар',
    ];

    if ( $Item->Products->count() && $Product = Doctrine::getTable('Products')->findOneByCode($Item->Products->get(0)->code) ) {
        $data['product_url'] = $Product->getUrl();
    }

    if ( !empty($Item->email) ) {
        $Twig = new Environment(new ArrayLoader([
            'subject' => $MailTemplate->subject
        ]));
        $subject = $Twig->render('subject', $data);

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($MailTemplate->renderHTML($data));
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($Item->email, $Item->name);
        $mail->setSubject($subject);
        $mail->send();
        
    }

    $Log = new OrdersSender;
    $Log->id_order = $Item->id;
    $Log->type = 'hotline-comment';
    $Log->save();

}
<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$Query = Doctrine_Query::create()
    ->from('LogReportTasks')
    ->where("COALESCE(executed_at, DATE '0001-01-01') < NOW() - (INTERVAL '1 sec' * exec_interval)");

foreach ($Query->execute() as $Task) {

    $Logs = Doctrine_Query::create()->from('Log');

    if ( $Task->executed_at ) {
        $Logs->addWhere("created_at > ?::TIMESTAMP", $Task->executed_at);
    }

    $Logs->addWhere('code = ?', $Task->code);
    $Logs->limit(1000);

    $data = $Logs->execute()->toArray();

    if ( !empty($data) ) {
        $keys = array_keys(call_user_func_array('array_merge', array_column($data, 'jdata'))); // получаем все ключи
        sort($keys);

        $body = '<table style="border: 1px solid GrayText; width: 1%; font-size: 11px;">';
        $body .= '<tr><th style="background-color: #b0b0b0;"></th>';
        foreach ($keys as $key) {
            $body .= '<th style="background-color: #b0b0b0;">' . $key . '</th>';
        }
        $body .= '</tr>';

        foreach ($data as $i => $log) {
            $body .= '<tr>';
            $body .= '<td style="white-space: nowrap; '. ($i%2?'background-color: #f9f9f9;':'') .'"">'. date("m/d H:i:s", strtotime($log['created_at'])) . '</td>';
            foreach ($keys as $key) {
                $body .= '<td style="white-space: nowrap; '. ($i%2?'background-color: #f9f9f9;':'') .'"">'. $log['jdata'][$key] . '</td>';
            }
            $body .= '</tr>';
        }
        $body .= '</table>';

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($body);
        $mail->setFrom('info@anydasa.com');
        $mail->addTo($Task->emails);
        $mail->setSubject($Task->title);
        $mail->send();
    }

    $Task->executed_at = 'NOW()';
    $Task->save();
}


<?

$Res = Doctrine_Query::create()
    ->select('id, alias, title, updated')
    ->from('Products')
    ->where('is_visible = ?', 1)
    ->orderBy('id')
    ->fetchArray();

$sitemap->page('products');


foreach ($Res as $item) {
    $loc = $routes->assemble([
        'alias' => $item['alias'],
        'id' => $item['id']
    ], 'product');

    $image = array();
    $Img = new Products_Image($item['id']);

    if ($Img->getImagesCount()) {
        foreach ($Img->getImagesList() as $key => $item_image) {
            $image [] = array(
                'loc' => $item_image['big']['url'],
                'title' => htmlspecialchars($item['title'], ENT_XML1, 'UTF-8', false) . " рис.$key"
            );
        }
    }

    $sitemap->url($loc, $item['updated'], 'weekly', 0.5, $image);
}

$sitemap->close();

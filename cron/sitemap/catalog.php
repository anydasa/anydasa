<?

$Catalogs = Doctrine_Query::create()->from('Catalog')->execute();


$sitemap->page('catalog');

foreach ($Catalogs as $item) {
    foreach ($item->CatalogMenu as $Menu) {
        $loc = $routes->assemble([
            'menu_alias' => $Menu->alias,
            'menu_id' => $Menu->id,
        ], 'shop', true, false);

        $sitemap->url($loc, '', 'always', 0.7);
    }

    foreach ($item->CatalogFilterIndex as $Index) {
        $loc = $routes->assemble(array(
            'menu_alias' => $item->CatalogMenu->getFirst()->alias,
            'menu_id' => $item->CatalogMenu->getFirst()->id,
            'filter_params' => $Index->alias
        ), 'shop-filter-params', true, false);

        $sitemap->url($loc, '', 'always', 0.7);
    }
}

$sitemap->close();

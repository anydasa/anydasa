<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$Products = Products_Query::create()
              ->setOnlyPublic()
              ->setParamsQuery(array('balance_from' => 1))
              ->setParamsQuery(array('sort' => 'brand'))
              ->setParamsQuery(array('sort' => 'type'))
              ->execute();

$dir = $config['path']['pix'].'files/prices/';

@mkdir($dir);


file_put_contents($dir.'min-with-images.xlsx',      (new Products_Price($Products, array( 'showImages'=>1 )))->getPrice('min'));
file_put_contents($dir.'partner-with-images.xlsx',  (new Products_Price($Products, array( 'showImages'=>1 )))->getPrice('partner'));
file_put_contents($dir.'trade-with-images.xlsx',    (new Products_Price($Products, array( 'showImages'=>1 )))->getPrice('trade'));



file_put_contents($dir.'min-with-groups.xlsx',      (new Products_Price($Products, array( 'showGroup'=>1 )))->getPrice('min'));
file_put_contents($dir.'partner-with-groups.xlsx',  (new Products_Price($Products, array( 'showGroup'=>1 )))->getPrice('partner'));
file_put_contents($dir.'trade-with-groups.xlsx',    (new Products_Price($Products, array( 'showGroup'=>1 )))->getPrice('trade'));
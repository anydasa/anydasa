<?php
error_reporting(E_ERROR | E_PARSE);

include_once 'setupConfig.php';

$VendorsList = Doctrine_Query::create()->from('Vendors')
                    ->addWhere('is_auto_upload = 1')
                    ->addWhere('last_time_processed + INTERVAL \'1 SEC\' * interval_process < NOW() OR last_time_processed IS NULL')
                    ->execute();

foreach ($VendorsList as $Vendor) {

    try {
        $Vendor->process();
    } catch (Exception $e) {
        echo "Exception {$Vendor->name}\n";
        var_dump($e);
    }

}
<?

$maxMin = 50;
$time = Doctrine::getTable('Vendors')->find(1)->last_time_processed;

if ( (strtotime($time) + $maxMin*60) < time() ) {
// Если время синхронизации более $maxMin минут
    $mail = new Site_Mail('UTF-8');
    $mail->setFrom('noreply@anydasa.com', 'anydasa.com');
    $mail->addTo('info@anydasa.com');
    $mail->setSubject('anydasa.com. Синхронизация товаров была более '.$maxMin.' минут');
    $mail->setBodyHtml('anydasa.com. Синхронизация товаров была более '.$maxMin.' минут');
    $mail->setBodyText('anydasa.com. Синхронизация товаров была более '.$maxMin.' минут');
    $mail->send();
}
<?php

$Query = Doctrine_Query::create()->from('Feedback f');

$Query->select('f.id');

$Query->leftJoin($Query->getRootAlias().'.FeedbackAnswer fa')
    ->where('fa.id IS NULL');

$Query->andWhere('f.posted < NOW() - (INTERVAL \'30 min\')');

$Query->buildSqlQuery(false);


$mail = new Site_Mail('UTF-8');
$mail->setFrom('noreply@anydasa.com', 'anydasa.com');
$mail->addTo('info@anydasa.com');
$mail->setSubject('Просроченные сообщения "Обратная связь"');

$feedbacks = $Query->execute();

$html = '<h2>У вас есть новые <a href="http://admin.'.APPLICATION_DOMAIN.'/feedback/list?status=3">просроченные сообщения ('.$feedbacks->count(). ')</a></h2>';


if($feedbacks && $feedbacks->count()){
    foreach ($feedbacks as $feedback) {
        $html .= '<a href="http://admin.'.APPLICATION_DOMAIN.'/feedback/list?id='.$feedback->id.'">Сообщение №'.$feedback->id ."</a><br>";
    }

    $mail->setBodyHtml($html);
    $mail->send();
}



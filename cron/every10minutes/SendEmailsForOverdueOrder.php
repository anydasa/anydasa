<?php
$orderSenderType = 'order-overdue';

// Delete old records
$Query1 = Doctrine_Query::create()->from('OrdersSender os');
$Query1->innerJoin($Query1->getRootAlias().'.Orders o')->where('os.created_at < o.updated AND os.type=\''.$orderSenderType.'\'');
foreach($Query1->execute() as $orderSender){
    $orderSender->delete();
}
////

$Query = Doctrine_Query::create()->from('Orders');

$Query->select('Orders.id');

$Query->innerJoin($Query->getRootAlias().'.OrdersStatus os')
    ->where('COALESCE(updated, created) < NOW() - (INTERVAL \'1 min\' * os.minutes_limit)');

$Query->leftJoin($Query->getRootAlias().'.OrdersSender osr')
    ->andWhere('osr.id_order IS NULL OR osr.type != \''.$orderSenderType.'\'');

$Query->buildSqlQuery(false);


$mail = new Site_Mail('UTF-8');
$mail->setFrom('noreply@anydasa.com', 'anydasa.com');
$mail->addTo('info@anydasa.com');
$mail->setSubject('Просроченные заказы');

$orders = $Query->execute();

$html = '<h2>У вас есть новые <a href="http://admin.'.APPLICATION_DOMAIN.'/order/list?q=overdue">просроченные заказы ('.$orders->count(). ')</a></h2>';


if($orders && $orders->count()){
    foreach ($orders as $order) {
        $html .= '<a href="http://admin.'.APPLICATION_DOMAIN.'/order/list?id-array='.$order->id.'">Заказ №'.$order->id ."</a><br>";

        $Log = new OrdersSender;
        $Log->id_order = $order->id;
        $Log->type = $orderSenderType;
        $Log->save();
    }

    $mail->setBodyHtml($html);
    $mail->send();
}



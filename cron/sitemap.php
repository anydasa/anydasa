<?php

include_once 'setupConfig.php';

$sitemap = new Sitemap(Zend_Registry::getInstance()->config->url->domain, Zend_Registry::getInstance()->config->path->root.'frontend/public/', false);

foreach (glob(dirname(__FILE__).'/sitemap/*.php') as $path) {
    include_once  $path;
}
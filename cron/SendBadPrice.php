<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$Products = Products_Query::create()
              ->setParamsQuery(['has_bad_price' => 1, 'status' => 1])
              ->andWhere('p.balance > 0 OR p.expected_balance > 0')
              ->execute();

$file = sys_get_temp_dir().'/price.xlsx';

$Price = new Products_Price($Products, array( 'showGroup'=>1 ));
file_put_contents($file, $Price->getPrice(array('min', 'trade', 'partner')));

$mail = new Site_Mail('UTF-8');
$mail->setFrom('noreply@anydasa.com', 'anydasa.com');
$mail->addTo('info@anydasa.com');
$mail->setSubject('Нарушенные Цены');

if ( $Products->count() ) {
    $mail->setBodyHtml('Поправьте цены по своим категориям!');
    $at              = new Zend_Mime_Part(file_get_contents($file));
    $at->type        = mime_content_type($file);
    $at->disposition = Zend_Mime::DISPOSITION_INLINE;
    $at->encoding    = Zend_Mime::ENCODING_BASE64;
    $at->filename    = 'Нарушенные цены.xlsx';
    $mail->addAttachment($at);
} else {
    $mail->setBodyHtml('Цены в порядке!');
}

$mail->send();
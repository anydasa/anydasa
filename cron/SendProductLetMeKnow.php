<?php

use Twig\Environment;
use Twig\Loader\ArrayLoader;

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('product_let-me-know') ) {
    throw new Exception('Отсутсвует почтовый шаблон (MailTemplates)');
}

if ( !$SmsTemplate = Doctrine::getTable('SmsTemplates')->findOneByCode('product_let-me-know') ) {
    throw new Exception('Отсутсвует Sms шаблон (SmsTemplates)');
}

$Res = Doctrine_Query::create()
        ->select('t1.*')
        ->from('ProductsLetMeKnow t1')
        ->innerJoin('t1.Products t2')
        ->whereIn('t2.status', [Products::STATUS_VENDOR, Products::STATUS_AVAILABLE])
        ->addWhere('t1.processed = FALSE')
        ->execute();


foreach ($Res as $Item)
{
    $data = [
        'name'                  => $Item->name,
        'created'               => $Item->created,
        'product_title'         => $Item->Products->getTypedName(),
        'product_status_title'  => $Item->Products->getStatusTitle(),
        'product_url'           => $Item->Products->getUrl(),
        'product_id'            => $Item->Products->id,
    ];

    if ( !empty($Item->email) ) {
        $Twig = new Environment(new ArrayLoader([
            'subject' => $MailTemplate->subject
        ]));
        $subject = $Twig->render('subject', $data);

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($MailTemplate->renderHTML($data));
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($Item->email, $Item->name);
        $mail->setSubject($subject);
        $mail->send();
    }

    if ( !empty($Item->phone) ) {
        $message = $SmsTemplate->render($data);
        Sms::send($Item->phone, $message);
    }

    $Item->processed = true;
    $Item->save();
}
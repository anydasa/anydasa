<?php

error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once 'setupConfig.php';

$mail = new Site_Mail('UTF-8');
$mail_body = array();

//------------------------------------------------------------------------------------------------------------------------------------------

$ProductsSpy = Products_Query::create()->where('is_spy_prices = 1')->execute();

if ( $ProductsSpy->count() ) {
    $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
    $ExcelReport->setHeadRow(array('Код', 'Название', 'Остаток', 'Позиция', 'Цена Мегабайта', 'Отклонение', 'Минимальная', 'Максимальныя', 'Статус', 'Внешние связи', 'Время'));
    foreach ($ProductsSpy as $Item) {

        $Link   = $Item->getLink();
        $prices = $Link->getPrices();
        $links  = $Item->ProductsLinks->toKeyValueArray('url', 'last_time_check');

        $position  = $Link->getCompanyPosition('Megabite.com.ua');
        $priceMB   = $position ? $prices[$position-1]['price_uah'] : '';
        $price_min = reset($prices)['price_uah'];
        $price_max = end($prices)['price_uah'];
        $deviation = $position ? round(($prices[$position-1]['price_uah'] - $price_min) / $price_min * 100, 2).'%' : '';

        if ( 1 == $position ) {
            $deviation = !empty($prices[1]) ?
                            'у конкурента +' . round(($prices[1]['price_uah'] - $priceMB) / $priceMB * 100, 2).'%' :
                            'нет у конкурентов';
        }

        $ExcelReport->addDataRow(array(
            $Item->code,
            $Item->title,
            $Item->balance,
            $position,
            $priceMB,
            $deviation,
            $price_min,
            $price_max,
            Products::getStatusList()[$Item->status],
            $Item->ProductsLinks->count() ? 'есть' : 'нет',
            reset($prices)['time']
        ));
    }

    $at = new Zend_Mime_Part( $ExcelReport->getReport() );
    $at->type        = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    $at->disposition = Zend_Mime::DISPOSITION_INLINE;
    $at->encoding    = Zend_Mime::ENCODING_BASE64;
    $at->filename    = 'Отслеживаемые цены. '.date('H:i Y.m.d').'.xlsx';
    $mail->addAttachment($at);
}



//------------------------------------------------------------------------------------------------------------------------------------------

$WithoutMargin = Products_Query::create()
    ->setParamsQuery(array('notid_vendor_owner' => 1, 'has_margin' => 0))
    ->addWhere('p.id_vendor_product IS NOT NULL')
    ->execute();
if ( $WithoutMargin->count() ) {
    $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
    $ExcelReport->setHeadRow(array('Код', 'Название', 'Остаток', 'Ожидается'));
    foreach ($WithoutMargin as $Item) {
        $ExcelReport->addDataRow(array(
            $Item->code,
            $Item->title,
            $Item->balance,
            $Item->expected_balance
        ));
    }
    $at = new Zend_Mime_Part( $ExcelReport->getReport() );
    $at->type        = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    $at->disposition = Zend_Mime::DISPOSITION_INLINE;
    $at->encoding    = Zend_Mime::ENCODING_BASE64;
    $at->filename    = 'Товары не от 1С с неустановленной маржой. '.date('H:i Y.m.d').'.xlsx';
    $mail->addAttachment($at);
}

$Temp = Doctrine_Query::create()
    ->from('VendorsProducts')
    ->where('id_vendor = 1 AND id_product IS NULL AND is_actual = 1')
    ->addWhere('balance > 0 OR expected_balance > 0')->execute();

if ( $Temp->count() ) {
    $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
    $ExcelReport->setHeadRow(array('Код', 'Название', 'Остаток', 'Ожидается'));
    foreach ($Temp as $Item) {
        $ExcelReport->addDataRow(array(
            $Item->code,
            $Item->title,
            $Item->balance,
            $Item->expected_balance
        ));
    }
    $at = new Zend_Mime_Part( $ExcelReport->getReport() );
    $at->type        = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    $at->disposition = Zend_Mime::DISPOSITION_INLINE;
    $at->encoding    = Zend_Mime::ENCODING_BASE64;
    $at->filename    = 'С положительным остатком актуальных товаров не привязанные 1C поставщика. '.date('H:i Y.m.d').'.xlsx';
    $mail->addAttachment($at);
}



$Products = Products_Query::create()->setParamsQuery(array('balance_from' => 1, 'is_img'=>0))->execute();
if ( $Products->count() ) {
    $ExcelReport = new ExcelReport(date('H,i Y.m.d'));
    $ExcelReport->setHeadRow(array('Код', 'Название', 'Остаток', 'Ожидается'));
    foreach ($Products as $Item) {
        $ExcelReport->addDataRow(array(
            $Item->code,
            $Item->title,
            $Item->balance,
            $Item->expected_balance
        ));
    }
    $at = new Zend_Mime_Part( $ExcelReport->getReport() );
    $at->type        = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    $at->disposition = Zend_Mime::DISPOSITION_INLINE;
    $at->encoding    = Zend_Mime::ENCODING_BASE64;
    $at->filename    = 'C положительным остатком но без фотографий в рабочем каталоге. '.date('H:i Y.m.d').'.xlsx';
    $mail->addAttachment($at);
}


$mail_body []= 'С положительным остатком актуальных товаров не привязанные 1C поставщика: '. $Temp->count();
$mail_body []= 'C положительным остатком но без фотографий в рабочем каталоге: '. $Products->count();
$mail_body []= 'Товары не от 1С с неустановленной маржой: '. $WithoutMargin->count();
$mail_body = '<p>' . implode('</p><p>', $mail_body) . '</p>';


$mail->setFrom('noreply@anydasa.com', 'Мегабайт');
$mail->addTo('info@anydasa.com');
$mail->setSubject('Мегабайт. Информация о сайте');
$mail->setBodyHtml($mail_body);

$mail->send();
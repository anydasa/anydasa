<?

include_once __DIR__.'/../setupConfig.php';


DropFeed('Product');
$adCustomizerFeed = CreateCustomizerFeed('Product');

$user = new AdWordsUser(ADWORDS_INI_PATH);

$offset = 0;
$limit = 200;
while(true) {
    $Products = Doctrine_Query::create()
        ->from('Products p')
        ->innerJoin('p.AdwordsAdgroup g')
        ->offset($offset*$limit)
        ->limit($limit)
        ->orderBy('p.id')
        ->execute()
    ;

    if ( $Products->count() == 0 ) {
        break;
    }

    $feedItemService = $user->GetService('FeedItemService', ADWORDS_VERSION);
    $operations = [];
    foreach ($Products as $Product) {
        $data = [
            'Price' => $Product->getFormatedPrice('retail', 'UAH', true),
        ];
        foreach ($Product->AdwordsAdgroup as $Group) {
            $operations []= CreateFeedItemAddOperation($adCustomizerFeed, $Group->id, $data);
        }
    }
    $result =   $feedItemService->mutate($operations);
    $offset++;
}

function CreateFeedItemAddOperation($adCustomizerFeed, $adGroupId, $data)
{
    $FeedItem = new FeedItem();
    $FeedItem->feedId = $adCustomizerFeed->feedId;
    $FeedItem->adGroupTargeting = new FeedItemAdGroupTargeting($adGroupId);
    $FeedItem->attributeValues = array();

    foreach ($adCustomizerFeed->feedAttributes as $Attr) {
        $typeValue = strtolower($Attr->type) . "Value";
        $AttrValue = new FeedItemAttributeValue();
        $AttrValue->feedAttributeId = $Attr->id;
        $AttrValue->$typeValue      = $data[$Attr->name];

        $FeedItem->attributeValues []= $AttrValue;
    }

    $operation = new FeedItemOperation();
    $operation->operand = $FeedItem;
    $operation->operator = 'ADD';
    return $operation;
}



function GetCustomizerFeedByName($FeedName)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);

    $adCustomizerFeedService = $user->GetService('AdCustomizerFeedService', ADWORDS_VERSION);

    $selector = new Selector();
    $selector->fields = array('FeedId', 'FeedName', 'FeedStatus', 'FeedAttributes');
    $selector->predicates[] = new Predicate('FeedName', 'EQUALS', $FeedName);
    $selector->predicates[] = new Predicate('FeedStatus', 'EQUALS', 'ENABLED');

    $result = $adCustomizerFeedService->get($selector);

    return $result->entries[0];
}

function DropFeed($feedName)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $adCustomizerFeedService = $user->GetService('AdCustomizerFeedService', ADWORDS_VERSION);

    if ( $F = GetCustomizerFeedByName($feedName) ) {
        $adCustomizerFeedService->mutate(array(new AdCustomizerFeedOperation($F, 'REMOVE')));
    }
}

function CreateCustomizerFeed($feedName)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $adCustomizerFeedService = $user->GetService('AdCustomizerFeedService', ADWORDS_VERSION);

    $customizerFeed = new AdCustomizerFeed();
    $customizerFeed->feedName = $feedName;
    $customizerFeed->feedAttributes = array(
        new AdCustomizerFeedAttribute(null, 'Price', 'STRING'),
    );

    $result = $adCustomizerFeedService->mutate(array(new AdCustomizerFeedOperation($customizerFeed, 'ADD')));
    return $result->value[0];
}
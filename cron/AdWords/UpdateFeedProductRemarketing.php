<?

include_once __DIR__.'/../setupConfig.php';

$Feed = GetFeedByName('ProductRemarketing');



if ( empty($Feed) ) {
    $Feed = CreateFeed('ProductRemarketing');
    FeedMappingDriver::assocAttrAndMap($Feed);
    CreateFeedMap($Feed);
} else {
    FeedMappingDriver::assocAttrAndMap($Feed);
}


downloadFromServer($Feed);
prepareForUploadOnServer();
uploadOnServer($Feed);

function prepareForUploadOnServer()
{
    $Products = Doctrine_Query::create()
        ->from('Products p')
        ->whereIn('status', [Products::STATUS_AVAILABLE, Products::STATUS_VENDOR])
        ->execute()
    ;

    Doctrine_Query::create()
        ->update('AdwordsFeedProductRemarketing')
        ->set('operation', '?', 'REMOVE')
        ->whereNotIn('id', array_values($Products->toKeyValueArray('id', 'id')))
        ->execute();

    $FeedItems = Doctrine_Query::create()
        ->from('AdwordsFeedProductRemarketing INDEXBY id')
        ->execute()
    ;

    foreach ($Products as $Product) {

        $data = getProductData($Product);

        if ( $FeedItems->contains($Product->id) ) {
            $FeedItems->get($Product->id)->fromArray($data);
            if ( $FeedItems->get($Product->id)->isModified() ) {
                $FeedItems->get($Product->id)->operation = 'SET';
            }
        } else {
            $feedProduct = new AdwordsFeedProductRemarketing;
            $feedProduct->fromArray($data);
            $feedProduct->operation = 'ADD';
            $FeedItems->add($feedProduct);
        }
    }

    $FeedItems->save();

}

function getProductData($Product)
{
    $productName = trim($Product->getFullName());

    return [
        'id'         => $Product->id,
        'title'      => mb_strlen($productName) > 25 ? trunc_words($Product->title, 25) : $productName,
        'final_urls' => array($Product->getUrl()),
        'image_url'  => $Product->getImage()->getImagesCount() ? $Product->getImage()->getFirstImage()['big']['url'] : NULL,
        'price'      => $Product->getPrice('retail', 'UAH') > 0 ? $Product->getPrice('retail', 'UAH') . ' UAH' : NULL,
    ];
}

function uploadOnServer($Feed)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);

    $page = 0;
    $limit = 200;
    while(true) {
        $FeedItems = Doctrine_Query::create()
            ->from('AdwordsFeedProductRemarketing')
            ->where('operation IS NOT NULL')
            ->offset($page*$limit)
            ->limit($limit)
            ->orderBy('id')
            ->execute()
        ;

        if ( $FeedItems->count() == 0 ) {
            break;
        }

        $feedItemService = $user->GetService('FeedItemService', ADWORDS_VERSION);
        $operations = [];
        foreach ($FeedItems as $Item) {
            $FeedItem  = new FeedItem();
            $FeedItem->feedId           = $Feed->id;
            $FeedItem->feedItemId       = $Item->feed_item_id;
            $FeedItem->attributeValues  = FeedMappingDriver::arrayToAttrs($Item->toArray());
            $operations []= new FeedItemOperation($FeedItem, $Item->operation);
        }
        $feedItemService->mutate($operations);
        sleep(3);
        $page++;
    }
}

function GetFeedByName($FeedName)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);

    $FeedService = $user->GetService('FeedService', ADWORDS_VERSION);

    $selector = new Selector();
    $selector->fields = array('Id', 'Name', 'FeedStatus', 'Attributes', 'Origin', 'SystemFeedGenerationData');
    $selector->predicates[] = new Predicate('Name', 'EQUALS', $FeedName);
    $selector->predicates[] = new Predicate('FeedStatus', 'EQUALS', 'ENABLED');

    $result = $FeedService->get($selector);

    return $result->entries[0];
}

function DropFeed($feedName)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $FeedService = $user->GetService('FeedService', ADWORDS_VERSION);

    if ( $F = GetFeedByName($feedName) ) {
        $FeedService->mutate(array(new FeedOperation($F, 'REMOVE')));
    }
}

function GetFeedMap($FeedId)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $FeedMappingService = $user->GetService('FeedMappingService', ADWORDS_VERSION);

    $selector = new Selector();
    $selector->fields = array('AttributeFieldMappings');
    $selector->predicates[] = new Predicate('FeedId', 'EQUALS', $FeedId);
    $selector->predicates[] = new Predicate('Status', 'EQUALS', 'ENABLED');
    $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

    $result = $FeedMappingService->get($selector);

    return $result->entries[0];
}

function CreateFeedMap($savedFeed)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $FeedMappingService = $user->GetService('FeedMappingService', ADWORDS_VERSION);

    $FeedMapping = new FeedMapping();

    $FeedMapping->feedId = $savedFeed->id;
    $FeedMapping->placeholderType = 14;
    $FeedMapping->status = 'ENABLED';
    $FeedMapping->attributeFieldMappings = [];

    foreach (FeedMappingDriver::getFields() as $field) {
        $FeedMapping->attributeFieldMappings []=
            new AttributeFieldMapping($field['feedAttributeId'], $field['mappingPlaceholder']);
    }

    $result = $FeedMappingService->mutate(array(new FeedMappingOperation($FeedMapping, 'ADD')));
    return $result->value[0];
}

function CreateFeed($feedName)
{
    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $FeedService = $user->GetService('FeedService', ADWORDS_VERSION);

    $Feed = new Feed();

    $Feed->name       = $feedName;
    $Feed->origin     = 'USER';
    $Feed->attributes = [];

    foreach (FeedMappingDriver::getFields() as $key => $value) {
        $FeedAttribute = new FeedAttribute;

        $FeedAttribute->name        = $value['feedAttributeName'];
        $FeedAttribute->type        = $value['feedAttributeType'];
        $FeedAttribute->isPartOfKey = $value['feedAttributeIsKey'];

        $Feed->attributes []= $FeedAttribute;
    }

    $result = $FeedService->mutate(array(new FeedOperation($Feed, 'ADD')));
    return $result->value[0];
}

function downloadFromServer($Feed)
{
    Doctrine_Query::create()->delete('AdwordsFeedProductRemarketing')->execute();

    $user = new AdWordsUser(ADWORDS_INI_PATH);
    $feedItemService = $user->GetService('FeedItemService', ADWORDS_VERSION);

    $selector = new Selector();
    $selector->fields = array('AttributeValues');
    $selector->predicates[] = new Predicate('FeedId', 'EQUALS', $Feed->id);
    $selector->predicates[] = new Predicate('Status', 'EQUALS', 'ENABLED');
    $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE);

    do {
        $Collection = new Doctrine_Collection('AdwordsFeedProductRemarketing');
        $page = $feedItemService->get($selector);
        if (isset($page->entries)) {
            foreach ($page->entries as $item) {
                $Record = new AdwordsFeedProductRemarketing;
                $Record->feed_item_id = $item->feedItemId;
                $Record->fromArray(FeedMappingDriver::attrsToArray($item->attributeValues));
                $Collection->add($Record);
            }
        }
        $Collection->save();
        $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
    } while ($page->totalNumEntries > $selector->paging->startIndex);
}



abstract class FeedMappingDriverAbstract
{
    public static function getFields()
    {
        return static::$fields;
    }
    public static function assocAttrAndMap($Feed)
    {
        $nameAttrArray = array_combine(
            array_column(static::$fields, 'feedAttributeName'),
            array_keys(static::$fields)
        );

        foreach ($Feed->attributes as $item) {
            $key = $nameAttrArray[$item->name];
            static::$fields[$key]['feedAttributeId'] = $item->id;
        }
    }

    public static function attrsToArray($feedAttrs)
    {
        $attrFieldArray = array_combine(
            array_column(static::$fields, 'feedAttributeId'),
            array_keys(static::$fields)
        );

        $return = [];
        foreach ($feedAttrs as $item) {
            $key = $attrFieldArray[$item->feedAttributeId];
            $return[$key] = $item->{static::$fields[$key]['feedAttributeValueType']};
        }

        return $return;
    }

    public static function arrayToAttrs($array)
    {
        $array = array_intersect_key($array, static::$fields);

        $return = [];

        foreach ($array as $key => $val) {
            $Value = new FeedItemAttributeValue();
            $Value->feedAttributeId = static::$fields[$key]['feedAttributeId'];
            $Value->{static::$fields[$key]['feedAttributeValueType']} = $val;
            $return[] = $Value;
        }

        return $return;
    }
}

class FeedMappingDriver extends FeedMappingDriverAbstract
{
    protected static $fields = [
        'id' => [
            'mappingPlaceholder'     => 2,
            'feedAttributeId'        => NULL,
            'feedAttributeIsKey'     => true,
            'feedAttributeType'      => 'STRING',
            'feedAttributeName'      => 'ID',
            'feedAttributeValueType' => 'stringValue',
        ],
        'title' => [
            'mappingPlaceholder'     => 5,
            'feedAttributeId'        => NULL,
            'feedAttributeIsKey'     => false,
            'feedAttributeType'      => 'STRING',
            'feedAttributeName'      => 'Item title',
            'feedAttributeValueType' => 'stringValue',
        ],
        'price' => [
            'mappingPlaceholder'     => 9,
            'feedAttributeId'        => NULL,
            'feedAttributeIsKey'     => false,
            'feedAttributeType'      => 'STRING',
            'feedAttributeName'      => 'Price',
            'feedAttributeValueType' => 'stringValue',
        ],
        'image_url' => [
            'mappingPlaceholder'     => 10,
            'feedAttributeId'        => NULL,
            'feedAttributeIsKey'     => false,
            'feedAttributeType'      => 'URL',
            'feedAttributeName'      => 'Image URL',
            'feedAttributeValueType' => 'stringValue',
        ],
        'final_urls' => [
            'mappingPlaceholder'     => 13,
            'feedAttributeId'        => NULL,
            'feedAttributeIsKey'     => false,
            'feedAttributeType'      => 'URL_LIST',
            'feedAttributeName'      => 'Final URL',
            'feedAttributeValueType' => 'stringValues',
        ],
    ];
}
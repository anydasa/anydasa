<?

include_once __DIR__.'/../setupConfig.php';

$user = new AdWordsUser(ADWORDS_INI_PATH);
$adGroupService = $user->GetService('AdGroupService', ADWORDS_VERSION);
$offset = 0;
$limit = 500;

while(true) {

    $QueryResult = Doctrine_Query::create()
        ->from('AdwordsAdgroup')
        ->where('id_product IS NOT NULL')
        ->offset($offset*$limit)
        ->limit($limit)
        ->orderBy('id')
        ->execute()
    ;

    if ( $QueryResult->count() == 0 ) {
        break;
    }

    $operations = [];
    foreach ($QueryResult as & $ResultRecord) {

        $ResultRecord->status =
            $ResultRecord->Products->isAvailableForOrder() ? 'ENABLED' : 'PAUSED';

        if ( $ResultRecord->isModified() ) {
            $adGroup = new AdGroup();
            $adGroup->id     = $ResultRecord->id;
            $adGroup->status = $ResultRecord->status;
            $operations []= new AdGroupOperation($adGroup, 'SET');
        }
    }

    $QueryResult->save();

    if ( !empty($operations) ) {
        $adGroupService->mutate($operations);
    }

    $offset++;
}

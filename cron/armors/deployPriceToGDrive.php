<?

include_once __DIR__.'/../setupConfig.php';


$client = new Google_Client();
//        $client->setAuthConfig(ROOT_PATH.'/etc/client_secret_751826720078-i9ombu972matfkvb69a7uumefi6jn7h1.apps.googleusercontent.com.json');
$client->setAuthConfig(ROOT_PATH.'/etc/megabite-f2c4e6e4b8c2.json');
$client->setSubject('anydasa.com@gmail.com');
//        $client->fetchAccessTokenWithAuthCode('4/8wdWtG1OMsOVWcmQwuqjQTDAkX3VMovLROk1TgXcobU');

$client->setScopes([
    Google_Service_Drive::DRIVE_FILE,
    Google_Service_Drive::DRIVE_METADATA,
    Google_Service_Drive::DRIVE_METADATA_READONLY,
    Google_Service_Drive::DRIVE_PHOTOS_READONLY,
    Google_Service_Drive::DRIVE_SCRIPTS,
    Google_Service_Drive::DRIVE_APPDATA,
]);

$service = new Google_Service_Drive($client);

$files = array(
    array(
        'path' => '/www/armors.com.ua/pix/files/prices/min-with-groups.xlsx',
        'name' => 'Armors/min',
        'fileId' => '10v9Jie7P45D1LBSM1dM8ifyRGu2WemQl7yA6COhwUgo'
    ),
    array(
        'path' => '/www/armors.com.ua/pix/files/prices/trade-with-groups.xlsx',
        'name' => 'Armors/trade',
        'fileId' => '1qggoTkKuXoJwAx-4toEKFOiZuR_ZUFk62u96P6lwHZ8'
    ),
    array(
        'path' => '/www/armors.com.ua/pix/files/prices/partner-with-groups.xlsx',
        'name' => 'Armors/partner',
        'fileId' => '182KRYWFJgs80ju2X1p6lk7zW_SO8VjUrRAuuEl6pP1E'
    )
);

try {

    foreach ($files as $file) {
        $fileMetadata = new Google_Service_Drive_DriveFile(array(
            'name' => $file['name'],
            'mimeType' => 'application/vnd.google-apps.spreadsheet'
        ));

        $service->files->update($file['fileId'], $fileMetadata, array(
            'data' => file_get_contents($file['path']),
            'mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'uploadType' => 'multipart',
            'fields' => 'id'));
    }

} catch (Exception $e) {
    print $e->getMessage();
}


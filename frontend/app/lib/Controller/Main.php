<?

abstract class Controller_Main extends Zend_Controller_Action
{
    public function init()
    {
        if ( $RedirectUrl = RedirectRules::matchRedirectUrl($_SERVER['REQUEST_URI']) ) {
            $this->redirect($RedirectUrl, ['code' => 301]);
        }

        if (isset($_GET['cache']))
            Site_Cache::$cache->clean();

        $user = Zend_Auth::getInstance()->getIdentity();
        $this->user = $this->view->user = (! empty($user->id)) ? Doctrine::getTable('Users')->find($user->id) : null;


        if ( !empty($_SERVER['HTTP_REFERER']) && parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != Zend_Registry::getInstance()->config->url->host ) {
            $_SESSION['referer'] = $_SERVER['HTTP_REFERER'];
        }

        $this->view->ga = [];

        $this->setUpCurrency();
        $this->setDisplayTypeList();

        $this->view->params = $this->params = $this->getAllParams();
        $this->view->pix = Zend_Registry::getInstance()->config->url->pix;
        $this->view->SiteConfig = Zend_Registry::getInstance()->config;

        Zend_Registry::getInstance()->spelling = $this->_getSpelling();

        $this->view->staticPages        = $this->_getStaticPagesList();

        if ( $this->_request->isXmlHttpRequest() ) {
            $this->_helper->layout->disableLayout();
        } else {
            $this->view->headTitle('Каталог товаров anydasa.com — портативная электроника, гаджеты и аксессуары в Украине.');
            $this->view->headTitle()->setDefaultAttachOrder('PREPEND');
            $this->view->headTitle()->setSeparator(' ');

            $this->view->headDescription('anydasa.com каталог товаров. Портативная электроника, аксессуары и комплектующие. ✓ Полное описание.');
            $this->view->headDescription()->setDefaultAttachOrder('PREPEND');
            $this->view->headDescription()->setSeparator(' ');

            $this->view->layout()->header = $this->view->render('header.phtml');
            $this->view->layout()->footer = $this->view->render('footer.phtml');
            $this->view->layout()->nav    = $this->view->render('nav.phtml');
        }

    }

    private function setUpCurrency()
    {
        $Currency = Doctrine_Query::create()->from('Currency')->where('is_visible = 1')->orderBy('sort')->execute();
        if ( empty($_SESSION['currency']) ) {
            $_SESSION['currency'] = Currency::getByISO('UAH')->iso;
        }
        foreach ($Currency as $Curr) {
            if ( $Curr->iso == $_SESSION['currency'] ) {
                Zend_Registry::getInstance()->currency_current = $Curr;
            }
        }
        Zend_Registry::getInstance()->currency_list    = $Currency;
    }

    private function setDisplayTypeList()
    {
        if ( empty($_SESSION['display_type_list']) ) {
            $_SESSION['display_type_list'] = 'list';
        }
    }

    private function _getSpelling()
    {
        $key_cache = 'spelling_list_key_value';
        if ( !$return = Site_Cache::load($key_cache) ) {
            $return = Doctrine::getTable('Spelling')->findAll()->toKeyValueArray('key', 'value');
            Site_Cache::save($return, $key_cache);
        }

        return $return;
    }
    private function _getStaticPagesList()
    {
        $key_cache = 'static_pages_list_alias_title';

        if ( !$return = Site_Cache::load($key_cache) ) {
            $return = Doctrine_Query::create()->select('alias, title')->from('StaticPages')->execute()->toKeyValueArray('alias', 'title');
            Site_Cache::save($return, $key_cache);
        }

        return $return;
    }

    public function redirect($url, array $options = array())
    {
        if ( $this->_request->isXmlHttpRequest() ) {
            echo 'Location:'.$url;
            exit;
        }
        parent::redirect($url, $options);
        exit;
    }
}
<?

abstract class Controller_Account extends Controller_Main
{
    public function preDispatch()
    {
        if ( !$this->_request->isXmlHttpRequest() ) {
            Zend_Layout::getMvcInstance()->setLayout('account');
        }

        if (! $this->user) {
            $this->_redirect($this->view->url(array(), 'main', true));
        }
    }

    public function init()
    {
        parent::init();

        $leftBar = [
            ['controller' => 'info',        'title' => 'Личные данные'],
            ['controller' => 'discount',    'title' => 'Дисконт'],
            ['controller' => 'orders',      'title' => 'Заказы'],
            ['controller' => 'notice',      'title' => 'Уведомления'],
        ];

        foreach ($leftBar as & $item)
        {
            $item['selected'] = false;
            if ( $item['controller'] == $this->getRequest()->getControllerName() ) {
                $item['selected'] = true;
            }
        }
        $this->view->leftBar = $leftBar;
    }
}


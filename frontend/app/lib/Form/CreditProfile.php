<?


class Form_CreditProfile extends Form_Abstract
{
    private $Offer;

    function __construct(CreditCompanyOffers $Offer)
    {
        $this->Offer = $Offer;
        parent::__construct('CreditProfile');
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');
        $this->addJavaScriptLink('/js/vendor/maskedinput.js');

        $this->addJavaScriptText("
            $('#phone').mask('+38 (999) 999-99-99');
            $('#inn').mask('9999999999');
            $('#birthdate').mask('99.99.9999');
         ");

        $this->addStyleText("
            #element-submit {
                margin: 15px 0 0 0;
                text-align: right;
            }
            .element-description {
                float: left;
                line-height: 3.5em;
            }
            #submit {
                float: right;
            }
        ");

        $this->setAttrib('data-target', 'self');
        $this->submit->setLabel('Сохранить анкету');
        $this->submit->setAttrib('class', 'btn btn-large btn-important');

        $this->submit->setDescription(
            '<span class="link link-black" data-target="self" data-modal-link="'. Zend_Layout::getMvcInstance()->getView()->url(['id_offer' => $this->Offer->id], 'credit-offer') .'">
                К программе
            </span>'
        );

        return parent::render($view);
    }

    public function init()
    {

        foreach ($this->Offer->profile_fields as $field) {
            $field = SF::mb_ucfirst(strtolower($field));
            $method = "add{$field}Element";

            if ( method_exists($this, $method) ) {
                $this->{$method}();
            }

        }

        $this->addSubmitElement();
    }

    public function addFioElement()
    {
        $element = new Zend_Form_Element_Text('first_name');
        $element->setLabel('Имя');
        $element->setAttrib('title', 'Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('last_name');
        $element->setLabel('Фамилия');
        $element->setAttrib('title', 'Фамилия');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('middle_name');
        $element->setLabel('Отчество');
        $element->setAttrib('title', 'Отчество');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);

        $this->addDisplayGroup(['first_name', 'last_name', 'middle_name'], 'name', array('legend' => '&nbsp;', 'escape'=>false));
    }

    public function addInnElement()
    {
        $element = new Zend_Form_Element_Text('inn');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setLabel('ИНН');
        $element->setAttrib('title', 'ИНН');
        $element->setAttrib('placeholder', 'xxxxxxxxxx');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('passport_series');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setLabel('Серия паспорта');
        $element->setAttrib('title', 'Серия паспорта');
        $this->addElement($element);

        $this->addDisplayGroup(['inn', 'passport_series'], 'pass', array('legend' => '&nbsp;', 'escape'=>false));
    }



    public function addBirthdateElement()
    {
        $element = new Zend_Form_Element_Text('birthdate');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setLabel('Дата рождения');
        $element->setAttrib('title', 'Дата рождения');
        $element->setAttrib('placeholder', 'дд.мм.гггг');
        $this->addElement($element);

    }

    public function addAddressElement()
    {
        $element = new Zend_Form_Element_Text('obl');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setLabel('Область');
        $element->setAttrib('title', 'Область проживания');
        $element->setAttrib('placeholder', 'Область проживания');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('city');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setLabel('Город');
        $element->setAttrib('title', 'Город проживания');
        $element->setAttrib('placeholder', 'Город проживания');
        $this->addElement($element);

        $this->addDisplayGroup(['obl', 'city'], 'address', array('legend' => '&nbsp;', 'escape'=>false));
    }

    public function addContactsElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->setLabel('Телефон');
        $element->setAttrib('title', 'Телефон');
        $element->setAttrib('placeholder', '+38 (xxx) xxx-xx-xx');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email');
        $element->setAttrib('title', 'Email');
        $element->setAttrib('placeholder', 'xxx@xxxx.xx');
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('EmailBool', true, array('messages' => array('emailNotValid'=> "Не корректно")));
        $this->addElement($element);

        $this->addDisplayGroup(['phone', 'email'], 'phone_email', array('legend' => '&nbsp;', 'escape'=>false));
    }

    public function getAssociatedValues()
    {
        $return = [];

        foreach ($this->getValues() as $key => $val) {
            $title = $this->{$key}->getAttrib('title');

            if ( !empty($title) ) {
                $return[$title] = $val;
            }

        }

        return $return;
    }
}
<?

abstract class Form_Abstract extends Zend_Form
{
    protected $modelName;

    private $_javaScriptText = '';
    private $_styleText = '';

    function __construct($formName, $options = null)
    {
        $this->addPrefixPath('Site_Form_Decorator', 'Site/Form/Decorator', 'decorator');
        $this->modelName = $formName;
        $this->setName($formName);
        $this->setAttrib('id', $formName);
        parent::__construct($options);
    }

    static function create($formName, $options = null)
    {
        $className = get_called_class();
        return new $className($formName, $options);
    }

    public function isValid($data)
    {
        $isValid = parent::isValid($data);

        foreach($this->getElements() as $element) {
            if($element->hasErrors()) {
                $element->setAttrib('class', 'error');
            }
        }

        foreach ($this->getSubForms() as $subForm) {
            foreach($subForm->getElements() as $element) {
                if($element->hasErrors()) {
                    $element->setAttrib('class', 'error');
                }
            }
        }

        return $isValid;
    }

    public function lockField($field)
    {
        $elem = $this->getElement($field);
        $elem->setAttrib('disabled', true);
        $elem->setAttrib('readonly',true);
    }


    public function addTechFields($params)
    {
        if ( !empty($params['afterSaveCallbackWithJsonResult']) ) {
            $element = new Zend_Form_Element_Hidden('afterSaveCallbackWithJsonResult');
            $element->setValue($params['afterSaveCallbackWithJsonResult']);
            $this->addElement($element);
        }
    }

    public function addIdElement()
    {
        $element = new Zend_Form_Element_Hidden('id');
        $element->setLabel(Doctrine::getTable($this->modelName)->getDefinitionOf($element->getName())['comment'].':');
        $element->addFilter(new Zend_Filter_Null);
        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        $element = new Zend_Form_Element_Button(array(
            'name' => 'submit',
            'label' => 'Сохранить',
            'escape' => false,
            'type' => 'submit',
            'class' => 'btn btn-important'
        ));
        $element->setOrder(10000);
        $this->addElement($element);


    }

    public function getOptionsForJqueryPlugin($form = null)
    {
        $array = [];

        if ( !$form ) {
            $form = $this;
        }

        foreach ($form->getElements() as $element) {
        	foreach ($element->getValidators() as $validate) {
                $name = $element->getFullyQualifiedName();

                $messages = $validate->getMessageTemplates();
        	    if (get_class($validate) == 'Zend_Validate_NotEmpty') {
        	        $array['rules'][$name]['required'] = true;
        	        $array['messages'][$name]['required'] = $messages['isEmpty'];
        	    }
        	    if (get_class($validate) == 'Site_Validate_EmailBool') {
        	        $array['rules'][$name]['email'] = true;
        	        $array['messages'][$name]['email'] = $messages['emailNotValid'];
        	    }
                if (get_class($validate) == 'Zend_Validate_Date') {
        	        $array['rules'][$name]['date'] = true;
        	        $array['messages'][$name]['date'] = $messages['dateInvalid'];
        	    }
                if (get_class($validate) == 'Zend_Validate_Regex') {
                    $array['rules'][$name]['regex'] = new Zend_Json_Expr($validate->getPattern());
                    $array['messages'][$name]['regex'] = $messages['regexInvalid'];
                }
                if ( get_class($validate) == 'Zend_Validate_Float' ) {
                    $array['rules'][$name]['number']    = true;
                    $array['messages'][$name]['number'] = $messages['notFloat'];
                }
                if ( get_class($validate) == 'Zend_Validate_Int' ) {
                    $array['rules'][$name]['number']    = true;
                    $array['messages'][$name]['number'] = $messages['notInt'];
                }
                if ( get_class($validate) == 'Site_Validate_Numeric' ) {
                    $array['rules'][$name]['number']    = true;
                    $array['messages'][$name]['number'] = $messages['notNumeric'];
                }
                if (get_class($validate) == 'Zend_Validate_Identical') {
                    $equalFunction = 'equalTo';
        	        $array['rules'][$name][$equalFunction] = '#' .  $this->getId(). ' #'.$this->getElement($validate->getToken())->getId();
        	        $array['messages'][$name][$equalFunction] = $messages['notSame'];
        	    }

                if (get_class($validate) == 'Zend_Validate_StringLength') {
                    if ( $validate->getMin() ) {
                        $array['rules'][$name]['minlength'] = $validate->getMin();
                        $array['messages'][$name]['minlength'] = str_replace('%min%', $validate->getMin(), $messages['stringLengthTooShort']);
                    }
                    if ( $validate->getMax() ) {
                        $array['rules'][$name]['maxlength'] = $validate->getMax();
                        $array['messages'][$name]['maxlength'] = str_replace('%max%', $validate->getMax(), $messages['stringLengthTooLong']);
                    }
                }
            }
        }

        foreach ($form->getSubForms() as $subForm) {
            $array = array_merge_recursive($array, $this->getOptionsForJqueryPlugin($subForm));
        }

        return $array;
    }

    protected function prefixElementNames($form = null)
    {
        if ( !$form ) $form = $this;

        static $belong = array();
        $belong[$form->getName()] = $form->getName();

        foreach ($form->getSubForms() as $subForm) {
            $this->prefixElementNames($subForm);
            unset($belong[$subForm->getName()]);
        }

        $formName = implode('_', $belong);

        foreach ($form->getElements() as $e) {
        	$e->setAttrib('id', $formName . '_' . $e->getName());
            $e->addDecorator('Errors', array(
                                        'escape'           => false,
                                        'elementSeparator' => '<br>',
                                        'elementStart'     => '<label class="error" for="'.$formName . '_' . $e->getName().'">',
                                        'elementEnd'       => '</label>',
            ));
        }
    }

    public function getMessages($name = null, $suppressArrayNotation = false)
    {
        $return = array();

        foreach (parent::getMessages() as $key=>$item) {
            $return[$key] = current($item);
        }

        return $return;
    }

    public function getJavaScriptCode()
    {
        return $this->_javaScriptText;
    }

    public function addStandartJavaScriptValidationForm($options = array())
    {
        $this->addJavaScriptLink('/js/vendor/validate.js');

        $this->addJavaScriptText("
            jQuery.validator.prototype.hideThese = function (errors) { this.addWrapper( errors ).remove(); };
            $.validator.addMethod('email', function (value, el, param) {
                var regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                return regex.test(value);
            });
            $('form#".$this->getAttrib('id')."').validate(".Zend_Json::encode($this->getOptionsForJqueryPlugin() + $options, false, array('enableJsonExprFinder' => true)).")
        ");
    }

    public function addJavaScriptText($text)
    {
        $this->_javaScriptText .= '<script type="application/javascript">'. JSMin::minify($text) . '</script>';
    }

    public function setStyleText($text)
    {
        $this->_styleText = '';
        return $this->addStyleText($text);
    }
    public function addStyleText($text)
    {
        $this->_styleText .= '<style>'. (new CSSmin)->run($text) . '</style>';
        return $this;
    }

    public function addJavaScriptLink($includeFile)
    {
        if ( ($f = Zend_Registry::getInstance()->config->path->public . ltrim($includeFile, '/')) AND is_file($f) ) {
            $this->addJavaScriptText(file_get_contents($f));
        } elseif ( ($f = Zend_Registry::getInstance()->config->path->pix . ltrim($includeFile, '/')) AND is_file($f) ) {
            $this->addJavaScriptText(file_get_contents($f));
        } elseif ( ($f = $includeFile) AND is_file($f) ) {
            $this->addJavaScriptText(file_get_contents($f));
        } else {
            $this->_javaScriptText .= '<script type="application/javascript" src="'.$includeFile . '"></script>';
        }
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $content = parent::render($view);
        return  $this->_styleText . $content . $this->getJavaScriptCode();

    }

    protected function _setDecorsDiv($form = null)
    {
        if ( is_null($form) ) {
            $form = $this;
        }

        foreach ($form->getSubForms() as $subForm) {
            $subForm->setElementsBelongTo($subForm->getId());
            $this->_setDecorsDiv($subForm);
            $subForm->removeDecorator('Form');
        }

        $form->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'div', 'class' => 'div form', 'id' => 'form_' . $form->getId())),
            array('Description', array('tag' => 'h1', 'placement' => 'prepend', 'escape' => false)),
            'Form',
        ));

        foreach ($form->getElements() as $element) {
            $element->setDecorators(array(

                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
        }

        foreach ( $form->getDisplayGroups() as $group ) {
            $group->setDecorators(array(
                    'FormElements',

                    array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
                    array('HtmlTag',array('tag' => 'table',  'class' => 'group')),
                )
            );

            foreach ($group->getElements() as $element) {
                $element->addDecorators(array(
                    array(array('data' => 'HtmlTag'), array('tag' => 'td')),
                ));
            }
        }


    }

    protected function _setDecorsTable($form = null)
    {
        if ( is_null($form) ) {
            $form = $this;
        }

        $form->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'standart-form ', 'id' => 'form_' . $form->getName())),
            array('Description', array('tag' => 'h1', 'placement' => 'prepend', 'escape' => false)),
            'Form',
        ));

        $form->setElementDecorators(array(
            'ViewHelper',
            array('Description', array('escape' => false)),
            array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
            array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
            array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
            array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
        ));

        foreach ( $form->getDisplayGroups() as $group ) {
            $group->setDecorators(array('FormElements', array('Fieldset')));
            foreach ($group->getElements() as $element) {
                $element->setDecorators(array(
                    'ViewHelper',
                    array('Description', array('escape' => false)),
                    array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                    array('Label', array('tag' => 'span', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
                    array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class'=>'group-element')),
                ));
            }
        }

        foreach ($form->getElements() as $element) {
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
            if ( 'Zend_Form_Element_MultiCheckbox' == $element->getType() ) {
                $element->setDecorators(array(
                    "ViewHelper",
                    array('Description', array('escape' => false)),
                    array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                    array(array("element" => "HtmlTag"), array("tag"   => "div", "class" =>"MultiCheckbox")),
                    array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
                    array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
                    array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),

                ));
            }
            if ( 'Zend_Form_Element_File' == $element->getType() ) {
                $element->setDecorators(array(
                        'File',
                        array('Description', array('escape' => false)),
                        array('Errors', array('escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<label class="error">', 'elementEnd' => '</label>')),
                        array('decorator' => array('td' => 'HtmlTag'), 'options' => array('tag' => 'td')),
                        array('Label', array('tag' => 'th', 'class'=>'title', 'escape' => false, 'requiredSuffix' => '<span data-toggle="tooltip" title="Обязательно для заполнения">*</span>')),
                        array('decorator' => array('tr' => 'HtmlTag'), 'options' => array('tag' => 'tr')),
                    ));

            }
        }

        $form->getElement('submit')->setDecorators(array(
            array('decorator' => 'ViewHelper'),
            array(
                'decorator' => array('td' => 'HtmlTag'),
                'options' => array('tag' => 'td', 'colspan' => 2)),
            array(
                'decorator' => array('tr' => 'HtmlTag'),
                'options' => array('tag' => 'tr')),
        ));

        foreach ($form->getSubForms() as $subForm) {
            $this->_setDecorsTable($subForm);
        }
    }
}
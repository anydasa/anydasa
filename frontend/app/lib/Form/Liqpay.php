<?

class Form_Liqpay extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("form#'.$this->getId().'").find("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);


        $this->addStyleText("
            #form_{$this->getId()} {
                width: 400px;
                margin: auto;
            }
            form#{$this->getId()} .element-title {
                display: block;
            }
            form#{$this->getId()} #description {
                height: 100px;
                width: 100%;
            }

            form#{$this->getId()} #submit {
                border: 0;
                background: none repeat scroll 0 0 #6CA91C;
                box-shadow: 0 3px 0 #528015;
                color: #FFFFFF;
                cursor: pointer;
                display: inline-block;
                min-width: 128px;
                padding: 9px 10px;
                text-align: center;
                text-decoration: none;
                text-transform: uppercase;
                font-weight: bold;
                float: right;
            }
            form#{$this->getId()} #submit:hover {
                background: none repeat scroll 0 0 #548316;
                box-shadow: 0 3px 0 #426811;
            }
        ");

        return parent::render($view);
    }

    public function init()
    {
        $this->setAction('https://www.liqpay.com/api/pay');
        $this->setMethod('POST');
        $this->setAttrib('accept-charset', 'utf-8');
        $this->setDescription('Оплата банковской картой (Visa, MasterCard)');

        $element = new Zend_Form_Element_Text('amount');
        $element->setLabel('Сумма оплаты');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')->addValidator('Numeric', true, array('messages' => array('notNumeric' => "Не число")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('description');
        $element->setLabel('Комментарий');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('public_key');
        $element->setValue('i40398218791');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('currency');
        $element->setValue('UAH');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('result_url');
        $element->setValue(REQUEST_SCHEME.'://anydasa.com/');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('type');
        $element->setValue('buy');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('language');
        $element->setValue('ru');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('sandbox');
        $element->setValue(0);
        $this->addElement($element);

        $element = new Zend_Form_Element_Button(array(
            'name' => 'submit',
            'label' => 'Перейти к оплате',
            'escape' => false,
            'type' => 'submit'
        ));
        $this->addElement($element);
    }

}
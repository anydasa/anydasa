<?


class Form_RecoveryPassword extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');
        $this->setAttrib('data-target', 'self');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("form#'.$this->getId().'").find("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');

        $this->addJavaScriptText("
            $(function () {
                $('form#{$this->getId()}').find('#email').val($.cookie('email')).blur(function () {
                    $.cookie('email', this.value)
                })
             })
         ");


        $this->addStyleText("
            form#{$this->getId()} {
                width: 400px;
            }
        ");

        foreach ($this->getElements() as $element) {
            $element->setDecorators(array(
                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
        }

        return parent::render($view);
    }

    public function init()
    {
        $this->addEmailElement();
        $this->addSubmitElement();
    }

    public function addEmailElement()
    {
        $element = new Glitch_Form_Element_Text('email');
        $element->setLabel('Email')
                ->setAttrib('placeholder', "example@gmail.com")
                ->addFilter(new Zend_Filter_StringToLower());

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array('messages' => array('emailNotValid'=> "Указан не корректно")));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
                ->addValidator('DbRecordExists', true, array('Users','email','messages' => array('dbRecordNotExists' => 'Вы не зарегистрированы.')));

        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->submit->setLabel('Выслать');
    }
}
<?

class Form_Order_Delivery extends Form_Abstract
{
    function __construct()
    {
        parent::__construct('Form_Order_Delivery');
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('class', 'show_required');
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addJavaScriptLink('/js/common/order/delivery.js');

        $this->addStandartJavaScriptValidationForm([
            'errorElement'   => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {
                $(element).closest(".element-row").find(".element-description").before(error);
            }'),
        ]);

        return parent::render($view);
    }

    public function init()
    {
        $this->addCityElement();
        $this->addTypeElement();

        $this->addSubForm(new Form_Order_Delivery_NovaPoshta, 'novaposhta');
        $this->addSubForm(new Form_Order_Delivery_Courier, 'courier');
        $this->addSubForm(new Form_Order_Delivery_Courier, 'npcourier');
        $this->addSubForm(new Form_Order_Delivery_Depot, 'pickup');

    }

    public function addCityElement()
    {
        $element = new Zend_Form_Element_Hidden('city_code');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('city');
        $element->setLabel('Город');
        $element->setDescription('Укажите город');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Куда доставляем?")));
        $this->addElement($element);
    }

    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Radio('delivery');

        foreach (OrdersDelivery::getList() as $Delivery) {
            $element->addMultiOption($Delivery->id, $Delivery->title);
        }

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Как доставить Вам покупку?")));
        $element->setDescription('&nbsp;');
        $this->addElement($element);
    }

    public function setDefaults(array $values)
    {
        if ( !empty($values['city_code']) ) {
            $this->getSubForm('novaposhta')->getElement('warehouse')->setAttrib('data-city_code', $values['city_code'])->addMultiOptions(
                Doctrine_Query::create()
                    ->from('NovaposhtaWarehouses')
                    ->where('city_code = ?', $values['city_code'])
                    ->execute()
                    ->toKeyValueArray('code', 'description_ru')
            );
        }

        parent::setDefaults($values);
    }

    public function isValid($data)
    {
        $subforms = $this->getSubForms();
        unset($subforms[$data['delivery']]);

        foreach ($subforms as $key => $subform) {
            $this->removeSubForm($key);
        }

        $isValid = parent::isValid($data);;

        foreach ($subforms as $key => $subform) {
            $subform->setAttrib('style', 'display: none;');
            $this->addSubForm($subform, $key);
        }

        return $isValid;
    }


}
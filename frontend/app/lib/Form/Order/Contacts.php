<?

class Form_Order_Contacts extends Form_Abstract
{
    function __construct()
    {
        parent::__construct('Form_Order_Contacts');
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('class', 'show_required');
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-description").append(error);}'),
//            'invalidHandler' => ''
        ]);

        $this->addJavaScriptLink('/js/vendor/maskedinput.js');

        $this->addJavaScriptText("
            $('form#{$this->getId()}').find('#phone').mask('+38 (999) 999-99-99')
         ");

        $this->addStyleText("
            form#{$this->getId()} {
                width: 800px;
            }

            form#{$this->getId()} input {
                background-color: White;
                width: 250px;
                display: inline-block;
            }
            form#{$this->getId()} .element-title {
                width: 100px;
                display: inline-block;
                white-space: nowrap;
                text-align: right;
            }
            form#{$this->getId()} .element-title label {
                float: none;
            }

            form#{$this->getId()} .element-description {
                display: inline-block;
                white-space: nowrap;
                font-size: 12px;
                color: gray;
                position: relative;
            }
            form#{$this->getId()} .element-description .error {
                width: 100%;
                display: inline-block;
                position: absolute;
                top:0;
                left:0;
                color: red;
                background: #EEE;
            }
        ");


        return parent::render($view);
    }

    public function init()
    {
        $this->addNameElement();
        $this->addPhoneElement();
        $this->addEmailElement();
    }

    public function addEmailElement()
    {
        $element = new Glitch_Form_Element_Text('email');
        $element->setAttrib('placeholder', '@');
        $element->setDescription('На этот почтовый ящик пришлем подробности заказа');
        $element->setLabel('Email');
        $element->addFilter(new Zend_Filter_StringToLower());

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array('messages' => array('emailNotValid'=> "{$element->getLabel()} указан не корректно")));

        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Glitch_Form_Element_Text('name');
        $element->setAttrib('placeholder', 'Ваше имя');
        $element->setDescription('На это имя будут оформлены все документы');
        $element->setLabel('ФИО');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Введите {$element->getLabel()}")));
        $element->addValidator('StringLength', false, array(2, 32,
                'messages' => array(
                    'stringLengthTooShort' => "Коротковато для имени",
                    'stringLengthTooLong' =>  "{$element->getLabel()} должно содержать менее %max% символов",
                )
            )
        );
        $this->addElement($element);
    }
    public function addPhoneElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setAttrib('placeholder', '+38 (xxx) xxx-xx-xx');
        $element->setDescription('Контактный телефон только на случай вопросов по вашему заказу');
        $element->setLabel('Телефон');

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Введите {$element->getLabel()}")));
        $this->addElement($element);
    }
    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->submit->setLabel('Продолжить оформление');
    }
}
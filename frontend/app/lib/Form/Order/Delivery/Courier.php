<?

class Form_Order_Delivery_Courier extends Zend_Form_SubForm
{
    public function init()
    {
        $element = new Zend_Form_Element_Textarea('address');
        $element->setLabel('Адрес');
        $element->setDescription('Укажите адрес доставки');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Необходимо указать адрес доставки")));
        $this->addElement($element);

        $this->addBeginTimeElement();
        $this->addEndTimeElement();
    }

    public function addBeginTimeElement()
    {
        $element = new Zend_Form_Element_Text('begin_time');

        $element->setLabel('Желаемое время доставки &nbsp;&nbsp;c');
        $element->setAttrib('type', 'number');
        $element->addValidator('Between', true, array('min'=>0,'max'=>23, 'messages'=>array('notBetween'=>'')));
        $element->setValue('10');
        $element->setDescription('&nbsp;');
        $this->addElement($element);
    }

    public function addEndTimeElement()
    {
        $element = new Zend_Form_Element_Text('end_time');

        $element->setLabel('до');
        $element->setAttrib('type', 'number');
        $element->addValidator('Between', true, array('min'=>0,'max'=>23, 'messages'=>array('notBetween'=>'')));
        $element->setValue('20');
        $element->setDescription('Дату доставки менеджер сообщит вам после оформления заказа по телефону');
        $this->addElement($element);
    }
}
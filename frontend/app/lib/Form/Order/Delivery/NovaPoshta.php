<?

class Form_Order_Delivery_NovaPoshta extends Zend_Form_SubForm
{
    public function init()
    {
        $element = new Zend_Form_Element_Select('warehouse');
        $element->addMultiOption('', '------------------');
        $element->setRegisterInArrayValidator(false);
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Необходимо указать отделение")));
        $element->setLabel('Отделение');
        $element->setDescription('&nbsp;');
        $this->addElement($element);
    }
}
<?

class Form_Order_Comment extends Form_Abstract
{
    function __construct()
    {
        parent::__construct('Form_Order_Comment');
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('class', 'show_required');
        $this->setAttrib('novalidate', 'novalidate');
        $this->_setDecorsDiv();
        return parent::render($view);
    }

    public function init()
    {
        $element = new Zend_Form_Element_Textarea('comment');
        $element->setLabel('Комментарий');
        $this->addElement($element);
    }
}
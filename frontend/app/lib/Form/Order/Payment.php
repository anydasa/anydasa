<?

class Form_Order_Payment extends Form_Abstract
{
    function __construct()
    {
        parent::__construct('Form_Order_Payment');
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsDiv();

        $this->setAttrib('class', 'show_required');
        $this->setAttrib('novalidate', 'novalidate');

        $this->addJavaScriptLink('/js/common/order/payment.js');

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) { $(element).closest(".element-row").find(".element-description").before(error); }')
        ]);

        return parent::render($view);
    }

    public function init()
    {
        $this->addTypeElement();

        $this->addSubForm(new Form_Order_Payment_Credit, 'credit');
    }



    public function addTypeElement()
    {
        $element = new Zend_Form_Element_Radio('payment', array('escape'=>false));

        foreach (OrdersPayment::getList() as $Payment) {
            $title = $Payment->title;
            if ( $Payment->commission > 0 ) {
                $title .= " <span>(+{$Payment->commission}%)</span>";
            }
            $element->addMultiOption( $Payment->id, $title);
        }

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Как Вам удобнее оплатить заказ?")));
        $element->setDescription('&nbsp;');
        $this->addElement($element);
    }

    public function isValid($data)
    {
        $subforms = $this->getSubForms();
        unset($subforms[$data['payment']]);

        foreach ($subforms as $key => $subform) {
            $this->removeSubForm($key);
        }

        $isValid = parent::isValid($data);;

        foreach ($subforms as $key => $subform) {
            $subform->setAttrib('style', 'display: none;');
            $this->addSubForm($subform, $key);
        }

        return $isValid;
    }
}
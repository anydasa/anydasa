<?

class Form_Order_Payment_Email extends Form_Abstract
{
    function __construct()
    {
        parent::__construct('Form_Order_Payment_Email');
    }

    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('class', 'show_required');
      //  $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();
        $this->setAttrib('data-target', 'self');
        $this->setAttrib('action', '/order/set-email/');
        $this->setMethod('post');

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-description").append(error);}'),
//            'invalidHandler' => ''
        ]);


        return parent::render($view);
    }

    public function init()
    {

        $this->addEmailElement();
        $this->addSubmitElement();
    }

    public function addEmailElement()
    {
        $element = new Glitch_Form_Element_Text('email');
        $element->setAttrib('placeholder', '@');
        $element->setLabel('Email');
        $element->addFilter(new Zend_Filter_StringToLower());
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Введите {$element->getLabel()}")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array('messages' => array('emailNotValid'=> "{$element->getLabel()} указан не корректно")));

        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();

        $this->submit->setLabel('Сохранить');
    }
}
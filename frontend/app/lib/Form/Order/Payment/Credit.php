<?

class Form_Order_Payment_Credit extends Zend_Form_SubForm
{

    public function init()
    {
        $element = new Zend_Form_Element_Text('offer');
        $element->setDescription('&nbsp;');
        $element->setAttrib('readonly', 'readonly');
        $element->setAttrib('placeholder', 'Выберите кредитную программу');
        $element->setAttrib('data-modal-link', '/credit/order/');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Необходимо выбрать кредитную программу')));
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id_offer');
        $this->addElement($element);

    }

}
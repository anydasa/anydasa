<?

class Form_Registration extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("form#'.$this->getId().'").find("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');
        $this->addJavaScriptLink('/js/vendor/maskedinput.js');

        $this->addJavaScriptText("
            $(function () {
                $('form#{$this->getId()}').find('#email').val($.cookie('email')).blur(function () {
                    $.cookie('email', this.value)
                })
                $('form#{$this->getId()}').find('#name').val($.cookie('name')).blur(function () {
                    $.cookie('name', this.value)
                })
                $('form#{$this->getId()}').find('#phone').val($.cookie('phone')).blur(function () {
                    $.cookie('phone', this.value)
                })
             })
             $('#phone').mask('+38 (099) 999-99-99')
         ");


        $this->addStyleText("
            form#{$this->getId()} {
                width: 350px;
            }
            form#{$this->getId()} div.group #element-password_confirm {
                float: right;
            }
        ");

        foreach ($this->getElements() as $element) {
            $element->setDecorators(array(
                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
        }

        return parent::render($view);
    }

    public function init()
    {
        $this->addNameElement();
        $this->addEmailElement();
        $this->addPasswordElement();
        $this->addPhoneElement();

        $this->addSubmitElement();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Glitch_Form_Element_Text('email');
        $element->setLabel('Email')->addFilter(new Zend_Filter_StringToLower());
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->addValidator('StringLength', true, array(
                2,
                32,
                'messages' => array(
                    'stringLengthTooShort' => "Что то какой-то короткий {$element->getLabel()} ну хотя бы %min% символа",
                    'stringLengthTooLong' =>  "Ооо..., извините, но Ваш {$element->getLabel()} уж слишком большой (миксимум мы верим в %max% символов)",
        )));
        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array('messages' => array('emailNotValid'=> "{$element->getLabel()} указан не корректно")));
        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
              ->addValidator('NoDbRecordExists', true, array(
                    'Users',
                    'email',
                    'messages' => array(
                        'dbRecordExists' => 'Вы уже зарегестрированы ранее.'
        )));
        $this->addElement($element);
    }

    public function addPasswordElement()
    {
        $element = new Zend_Form_Element_Password('password');
        $element->setLabel('Пароль')->addFilter(new Zend_Filter_Callback('md5'));
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $this->addElement($element);

        /* -------------------------------------------------   */

        $element = new Zend_Form_Element_Password('password_confirm');
        $element->setLabel('Еще раз');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $element->addValidator('Identical', true, array('password', 'messages' => array('notSame' => 'Не совпадает')));
        $this->addElement($element);

        $this->addDisplayGroup(['password', 'password_confirm'], 'pass', array('legend' => '&nbsp;', 'escape'=>false));
    }

    public function addPhoneElement()
    {
        $element = new Glitch_Form_Element_Text('phone');
        $element->setLabel('Телефон');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->submit->setLabel('Зарегистрироваться');
    }
}
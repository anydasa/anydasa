<?

class Form_User_Info extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->_setDecorsDiv();
        $this->addJavaScriptLink('/js/vendor/maskedinput.js');
        $this->addJavaScriptText("
            $(function () {
                $('.user_info').on('submit', 'form#{$this->getId()}', function (e) {

                     $(e.target).find('[type=submit]')
                        .css({height: $(this).find('[type=submit]').outerHeight(true), width: $(this).find('[type=submit]').outerWidth(true)})
                        .html('<i class=\"bar\"></i>')


                    var type     =  'POST' == $(e.target).attr('method').toUpperCase() ? 'POST' : 'GET';
                    var formData = ('POST' == type) ? new FormData(e.target) : $(e.target).serialize() ;
                    $.ajax({
                        url:         $(this).attr('action'),
                        type:        type,
                        data:        formData,
                        contentType: false,
                        cache:       false,
                        processData: false
                    }).error(function(data) {
                        $('.user_info').replaceWith( $(data.responseText) );
                        $(e.target).find('[type=submit]').html(submitLabel);
                    }).success(function(data) {
                        $('.user_info').replaceWith( $(data) );
                        $(e.target).find('[type=submit]').html(submitLabel);
                    });
                    return false;
                });
            });
            $('form#{$this->getId()}').find('#phone').mask('+38 (099) 999-99-99');
         ");

        $this->addStyleText("
            form#{$this->getId()} {
                width: 400px;
            }
            form#{$this->getId()} .element-title {
                display: inline-block;
            }
        ");

        $this->setAttrib('autocomplete ', 'off');

        return parent::render($view);
    }

    public function init()
    {
        $this->addNameElement();
        $this->addPasswordElement();
        $this->addPhoneElement();

        $this->addSubmitElement();
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Hidden('email');
        $this->addElement($element);

        $element = new Zend_Form_Element_Text('name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);
    }

    public function addPasswordElement()
    {
        $element = new Zend_Form_Element_Password('old_password');
        $element->setLabel('Старый пароль')->addFilter(new Site_Filter_Md5);
        $element->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $Validate = new Site_Validate_Login('email', 'users', 'email', 'password');
        $Validate->setMessage('Не верный пароль');
        $element->addValidator($Validate);
        $this->addElement($element);

        $element = new Zend_Form_Element_Password('password');
        $element->setLabel('Пароль')->addFilter(new Site_Filter_Md5);
        $element->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $this->addElement($element);

        $element = new Zend_Form_Element_Password('password_confirm');
        $element->setLabel('Еще раз');
        $element->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $element->addValidator('Identical', true, array('password', 'messages' => array('notSame' => 'Не совпадает')));
        $this->addElement($element);
    }

    public function addPhoneElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setLabel('Телефон');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Обязательное')));
        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();
        $this->submit->setLabel('Сохранить изминения');
    }

    public function isValid($data)
    {
        if ( !empty($data['old_password']) || !empty($data['password']) ) {
            $this->old_password->setRequired(true);
            $this->password->setRequired(true);
            $this->password_confirm->setRequired(true);
        }
        return parent::isValid($data);
    }

    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);
        if ( empty($values['password']) ) {
            unset($values['password']);
        }
        return $values;
    }
}
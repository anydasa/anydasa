<?


class Form_Feedback extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {

        $this->_setDecorsDiv();

        $this->setAttrib('novalidate', 'novalidate');

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("form#'.$this->getId().'").find("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');

        $this->addJavaScriptText("
            $(function () {
                $('form#{$this->getId()}').find('#email').val($.cookie('email')).blur(function () {
                    $.cookie('email', this.value)
                })
                $('form#{$this->getId()}').find('#name').val($.cookie('name')).blur(function () {
                    $.cookie('name', this.value)
                })
             })
         ");


        $this->addStyleText("
            form#{$this->getId()} {
                width: 400px;
            }
            form#{$this->getId()} #message {
                width: 100%;
                height: 100px;
                resize: vertical;
            }
        ");

        $this->submit->setLabel('Отправить');

        $this->setAttrib('data-target', 'self');

        return parent::render($view);
    }

    public function init()
    {
        $this->addNameElement();
        $this->addEmailElement();
        $this->addMessageElement();
        $this->addSubmitElement();
    }

    public function addNameElement()
    {
        $element = new Glitch_Form_Element_Text('name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Glitch_Form_Element_Text('email');
        $element->setLabel('Email');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "указан не корректно"
        )));

        $this->addElement($element);
    }


    public function addMessageElement()
    {
        $element = new Zend_Form_Element_Textarea('message');
        $element->setLabel('Сообщение');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));
        $element->addFilter(new Zend_Filter_Callback('strip_tags'));

        $this->addElement($element);
    }


}
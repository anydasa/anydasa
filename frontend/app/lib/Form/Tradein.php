<?


class Form_Tradein extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {

        $this->_setDecorsDiv();

        $this->setAttrib('novalidate', 'novalidate');

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("form#'.$this->getId().'").find("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');
        $this->addJavaScriptLink('/js/vendor/maskedinput.js');

        $this->addJavaScriptText("
            $(function () {
                $('form#{$this->getId()}').find('#user_email').val($.cookie('email')).blur(function () {
                    $.cookie('email', this.value)
                });
                $('form#{$this->getId()}').find('#user_name').val($.cookie('name')).blur(function () {
                    $.cookie('name', this.value)
                });
                $('form#{$this->getId()}').find('#user_phone').mask('+38 (099) 999-99-99');
             })
         ");


        $this->addStyleText("

            form#{$this->getId()} .element-description {
                font-size: 11px;
                color: Gray;
            }

            form#{$this->getId()} #element-product_can_enable,
            form#{$this->getId()} #element-product_working_state,
            form#{$this->getId()} #element-product_buttons_available,
            form#{$this->getId()} #element-product_has_body_damage,
            form#{$this->getId()} #element-product_has_screen_damage
            {
                width: 730px;
                height: 30px;
            }

            form#{$this->getId()} #element-product_can_enable .element-title,
            form#{$this->getId()} #element-product_working_state .element-title,
            form#{$this->getId()} #element-product_buttons_available .element-title,
            form#{$this->getId()} #element-product_has_body_damage .element-title,
            form#{$this->getId()} #element-product_has_screen_damage .element-title
            {
                float: left;
                line-height: 25px;
                font-size: 15px;
            }

            form#{$this->getId()} #element-product_can_enable select,
            form#{$this->getId()} #element-product_working_state select,
            form#{$this->getId()} #element-product_buttons_available select,
            form#{$this->getId()} #element-product_has_body_damage select,
            form#{$this->getId()} #element-product_has_screen_damage select
            {
                width: 100px;
                float: right;
            }
            form#{$this->getId()} #element-product_working_state select {
                width: 300px;
            }
            form#{$this->getId()} #element-submit {
                text-align: right;
                padding-right: 25px;
                margin-top: 20px;
            }

        ");


        $this->submit->setLabel('Отправить');
        $this->submit->setAttrib('class', 'btn btn-large btn-green');

        $this->setAttrib('data-target', 'self');

        return parent::render($view);
    }

    public function init()
    {
        $this->addNameElement();
        $this->addEmailElement();
        $this->addPhoneElement();

        $this->addDisplayGroup(['user_name', 'user_email', 'user_phone'], 'user', array('legend' => '&nbsp;', 'escape'=>false));

        $this->addProductTypeElement();
        $this->addProductBrandElement();
        $this->addProductModelElement();

        $this->addDisplayGroup(['product_type', 'product_brand', 'product_model'], 'product', array('legend' => '&nbsp;', 'escape'=>false));

        $this->addProductSelectElement();

        $this->addWantBuyElement();

        $this->addSubmitElement();
    }

    public function addNameElement()
    {
        $element = new Glitch_Form_Element_Text('user_name');
        $element->setLabel('Ваше имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Glitch_Form_Element_Text('user_email');
        $element->setLabel('Ваш email');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "указан не корректно"
        )));

        $this->addElement($element);
    }

    public function addPhoneElement()
    {
        $element = new Glitch_Form_Element_Text('user_phone');
        $element->setLabel('Ваш телефон');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }

    public function addProductTypeElement()
    {
        $element = new Glitch_Form_Element_Text('product_type');
        $element->setLabel('Тип товара');
        $element->setDescription('Например: Смартфон, Планшет, Ноутбук ...');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }

    public function addProductBrandElement()
    {
        $element = new Glitch_Form_Element_Text('product_brand');
        $element->setLabel('Производитель товара');
        $element->setDescription('Например: Apple, Samsung, Lenovo ...');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }

    public function addProductModelElement()
    {
        $element = new Glitch_Form_Element_Text('product_model');
        $element->setLabel('Модель товара');
        $element->setDescription('Например: LG H818P G4 DS Leather Black');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }

    public function addWantBuyElement()
    {
        $element = new Glitch_Form_Element_Text('want_buy');
        $element->setLabel('Хочу приобрести товар');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $this->addElement($element);
    }

    public function addProductSelectElement()
    {
        $element = new Zend_Form_Element_Select('product_can_enable');
        $element->setLabel('Может ли устройство быть включено?');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setMultiOptions([
            ''    => '',
            'Да'  => 'Да',
            'Нет' => 'Нет'
        ]);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('product_working_state');
        $element->setLabel('Устройство в полностью рабочем состоянии?');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setMultiOptions([
            ''    => '',
            'Да, все функции работают как и должны'  => 'Да, все функции работают как и должны',
            'Нет, некоторые функции не работают из-за физической или программной неисправности' => 'Нет, некоторые функции не работают из-за физической или программной неисправности'
        ]);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('product_buttons_available');
        $element->setLabel('Все ли кнопки в наличии и функциональны?');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setMultiOptions([
            ''    => '',
            'Да'  => 'Да',
            'Нет' => 'Нет'
        ]);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('product_has_body_damage');
        $element->setLabel('Есть ли явные повреждения корпуса (за исключением обыкновенного износа)?');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setMultiOptions([
            ''    => '',
            'Нет' => 'Нет',
            'Да'  => 'Да',
        ]);
        $this->addElement($element);

        $element = new Zend_Form_Element_Select('product_has_screen_damage');
        $element->setLabel('Есть ли повреждения экрана (за исключением обыкновенного износа)?');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательно")));
        $element->setMultiOptions([
            ''    => '',
            'Нет' => 'Нет',
            'Да'  => 'Да',
        ]);
        $this->addElement($element);

    }

}
<?


class Form_Auth extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');
        $this->setAttrib('data-target', 'prev');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');
        $this->addJavaScriptText("
            $(function () {
                $('#email').val($.cookie('email')).blur(function () {
                    $.cookie('email', this.value)
                })
             })
         ");

        $this->addStyleText("
            #login {
                width: 300px;
            }
            #element-submit {
                margin: 15px 0 0 0;
                float: left;
                width: 100%;
            }
            #element-submit button {
                float: left;
            }
            #element-submit .element-description {
                float: right;
                font-size: 0.85em;
                line-height: 1.2em;
                margin: 9px;
            }
            #element-submit .element-description>* {
                margin: 2px 0;
            }
        ");


        foreach ($this->getElements() as $element) {
            $element->setDecorators(array(
                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
        }


        return parent::render($view);
    }

    public function init()
    {
        $this->addEmailElement();
        $this->addPasswordElement();
        $this->addSubmitElement();
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email')->addFilter(new Zend_Filter_StringToLower());

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "{$element->getLabel()} указан не корректно"
        )));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
                ->addValidator('DbRecordExists', true, array('Users','email','messages' => array(
                        'dbRecordNotExists' => 'Вы не зарегистрированы'
        )));

        $element->addPrefixPath('Site_Validate_Doctrine', 'Site/Validate/Doctrine/', 'validate')
                ->addValidator('EnableUser', true, array('Users',));

        $this->addElement($element);
    }

    public function addPasswordElement()
    {
        $element = new Zend_Form_Element_Password('password');
        $element->setLabel('Пароль');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => 'Введите пароль')));
        $element->addFilter(new Zend_Filter_Callback('md5'));

        $Validate = new Site_Validate_Login('email', 'users', 'email', 'password');
        $Validate->setMessage('Неверный пароль');
        $element->addValidator($Validate);

        $this->addElement($element);
    }

    public function addSubmitElement()
    {
        parent::addSubmitElement();

        $this->submit->setLabel('ВОЙТИ');
        $this->submit->setDescription(
            '<span class="link link-black" data-modal-name="recovery-password" data-target="self" data-modal-link="/recovery-password/">Восстановить пароль</span>'
        );
    }
}
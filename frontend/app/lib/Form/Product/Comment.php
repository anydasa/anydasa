<?


class Form_Product_Comment extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptText('
            $rating = $("<div style=\"text-align: center\" />")
                        .append("<div>Оцените этот товар</div>")
                        .append(
                            $("<div class=\"rating rating_vote rating2x\" />")
                                .append("<label><input type=radio name=rating value=1></label>")
                                .append("<label><input type=radio name=rating value=2></label>")
                                .append("<label><input type=radio name=rating value=3></label>")
                                .append("<label><input type=radio name=rating value=4></label>")
                                .append("<label><input type=radio name=rating value=5></label>")
                        );
            $("#form_ProductAddComment").prepend($rating);
        ');

        $this->addJavaScriptLink('/js/vendor/cookie.js');

        $this->setAttrib('style', 'width: 600px;');

        $this->addStyleText("
            #element-submit {
                margin: 15px 0 0 0;
                float: left;
                width: 100%;
            }
            #element-submit button {
                float: left;
            }
            #text, #dignity, #shortcoming {
                height: 100px;
                font-size: 14px;
                resize: none;
            }

            #dignity, #shortcoming {
                height: 50px;
            }
            
            .element-description {
                display: inline-block;
                color: gray;
                font-size: 12px;
            }
        ");

        $this->setAttrib('data-target', 'self');

        foreach ($this->getElements() as $element) {
            $element->setDecorators(array(
                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
            if ( 'rating' == $element->getName() ) {
                $element->setDecorators(array());
            }

        }

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdProductElement();
        $this->addRatingElement();
        $this->addNameElement();
//        $this->addEmailElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }


    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $this->addElement($element);
    }

    public function addRatingElement()
    {
        $element = new Zend_Form_Element_Hidden('rating');
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('author_name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->addValidator('StringLength', false, array(3, 50, 'messages' => "От 3 до 50 символов"));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email')->addFilter(new Zend_Filter_StringToLower());

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "указан не корректно"
        )));

        $this->addElement($element);
    }

    public function addTextElement()
    {

        $element = new Zend_Form_Element_Textarea('dignity');
        $element->setLabel('Достоинства');
//        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('shortcoming');
        $element->setLabel('Недостатки');
//        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));
        $this->addElement($element);

        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel('Комментарий');
        $element->setDescription('Будет выводиться на сайте после проверки модератором');
//        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));
        $this->addElement($element);
    }


    public function addSubmitElement()
    {
        parent::addSubmitElement();

        $this->submit->setLabel('Добавить');
    }
}
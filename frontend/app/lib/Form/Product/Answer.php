<?


class Form_Product_Answer extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-title label").after(error);}')
        ]);


        $this->addJavaScriptLink('/js/vendor/cookie.js');

        $this->setAttrib('style', 'width: 600px;');

        $this->addStyleText("
            #element-submit {
                margin: 15px 0 0 0;
                float: left;
                width: 100%;
            }
            #element-submit button {
                float: left;
            }
            #text {
                height: 100px;
                font-size: 13px;
                resize: none;
            }
            .element-description {
                display: inline-block;
                color: gray;
                font-size: 12px;
            }
            span.error + .element-description {
                display: none;
            }
        ");

        $this->setAttrib('data-target', 'self');

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdProductElement();
        $this->addNameElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }


    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('id_parent');
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('author_name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Представьтесь пожалуйста")));
        $element->addValidator('StringLength', false, array(3, 50, 'messages' => "От 3 до 50 символов"));
        $this->addElement($element);
    }

    public function addTextElement()
    {

        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel('Ответ');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Напишите ответ")));
        $this->addElement($element);
    }


    public function addSubmitElement()
    {
        parent::addSubmitElement();

        $this->submit->setLabel('Отправить');
    }
}
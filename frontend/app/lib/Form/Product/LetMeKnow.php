<?


class Form_Product_LetMeKnow extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-title").append(error);}')
        ]);

        $this->addJavaScriptLink('/js/vendor/cookie.js');
        $this->addJavaScriptLink('/js/vendor/maskedinput.js');

        $this->setAttrib('style', 'width: 400px;');

        $this->addJavaScriptText("
            $('#phone').mask('+38 (099) 999-99-99');
         ");

        $this->addStyleText("
            #element-submit {
                margin: 15px 0 0 0;
                float: left;
                width: 100%;
            }
            #element-submit button {
                float: left;
            }
            #element-submit .element-description {
                float: left;
                font-size: 0.8em;
                line-height: 1.2em;
                margin: -3px auto auto 20px;
            }
            #element-submit .element-description>* {
                margin: 2px 0;
            }
        ");

        $this->setAttrib('data-target', 'self');

        foreach ($this->getElements() as $element) {
            $element->setDecorators(array(
                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }
        }

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdProductElement();
        $this->addNameElement();
        $this->addEmailElement();
        $this->addPhoneElement();
        $this->addSubmitElement();
    }


    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Обязательное")));
        $element->addValidator('StringLength', false, array(3, 50, 'messages' => "От 3 до 50 символов"));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('email');
        $element->setLabel('Email')->addFilter(new Zend_Filter_StringToLower());

        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Укажите {$element->getLabel()}")));

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "указан не корректно"
        )));

        $this->addElement($element);
    }

    public function addPhoneElement()
    {
        $element = new Zend_Form_Element_Text('phone');
        $element->setLabel('Телефон');
        $this->addElement($element);
    }



    public function addSubmitElement()
    {
        parent::addSubmitElement();

        $this->submit->setLabel('ОТПРАВИТЬ');
    }
}
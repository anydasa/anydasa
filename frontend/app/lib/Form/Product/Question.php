<?


class Form_Product_Question extends Form_Abstract
{
    public function render(Zend_View_Interface $view = NULL)
    {
        $this->setAttrib('novalidate', 'novalidate');

        $this->_setDecorsDiv();

        $this->addStandartJavaScriptValidationForm([
            'errorElement' => 'span',
            'errorPlacement' => new Zend_Json_Expr('function(error, element) {$("#element-" + element.attr("id") + " .element-title label").after(error);}')
        ]);


        $this->addJavaScriptLink('/js/vendor/cookie.js');

        $this->setAttrib('style', 'width: 600px;');

        $this->addStyleText("
            #element-submit {
                margin: 15px 0 0 0;
                float: left;
                width: 100%;
            }
            #element-submit button {
                float: left;
            }
            #text {
                height: 100px;
                font-size: 13px;
                resize: none;
            }
            .element-description {
                display: inline-block;
                color: gray;
                font-size: 12px;
            }
            span.error + .element-description {
                display: none;
            }
        ");

        $this->setAttrib('data-target', 'self');

        foreach ($this->getElements() as $element) {
            $element->setDecorators(array(
                array('Label', array('placement' => 'prepend', 'class'=>'title', 'escape' => false)),
                array('Errors', array('placement' => 'append', 'escape' => false, 'elementSeparator' => '<br>', 'elementStart' => '<span id="'.$element->getId().'-error" class="error">', 'elementEnd' => '</span>')),
                array('Description', array('escape' => false, 'class' => 'element-description', 'tag' => 'div')),
                array('decorator' => array('div' => 'HtmlTag'), 'options' => array('tag' => 'div', 'class' => 'element-title')),
                'ViewHelper',
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'element-row', 'id' => 'element-'.$element->getId()))
            ));
            if ( 'Zend_Form_Element_Button' == $element->getType() ) {
                $element->removeDecorator('Label');
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Checkbox' == $element->getType() ) {
                $element->removeDecorator('div');
            }
            if ( 'Zend_Form_Element_Hidden' == $element->getType() ) {
                $element->setDecorators(array('ViewHelper'));
            }

        }

        return parent::render($view);
    }

    public function init()
    {
        $this->addIdProductElement();
        $this->addNameElement();
        $this->addEmailElement();
        $this->addTextElement();
        $this->addSubmitElement();
    }


    public function addIdProductElement()
    {
        $element = new Zend_Form_Element_Hidden('id_product');
        $this->addElement($element);

        $element = new Zend_Form_Element_Hidden('is_question');
        $element->setValue(1);
        $this->addElement($element);
    }

    public function addNameElement()
    {
        $element = new Zend_Form_Element_Text('author_name');
        $element->setLabel('Имя');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Представьтесь пожалуйста")));
        $element->addValidator('StringLength', false, array(3, 50, 'messages' => "От 3 до 50 символов"));
        $this->addElement($element);
    }

    public function addEmailElement()
    {
        $element = new Zend_Form_Element_Text('author_email');
        $element->setLabel('Email')->addFilter(new Zend_Filter_StringToLower());
        $element->setDescription('Сюда вышлем уведомление об ответе');

        $element->addPrefixPath('Site_Validate', 'Site/Validate/', 'validate')
                ->addValidator('EmailBool', true, array(
                    'messages' => array(
                        'emailNotValid'=> "не корректно"
        )));

        $this->addElement($element);
    }

    public function addTextElement()
    {

        $element = new Zend_Form_Element_Textarea('text');
        $element->setLabel('Вопрос');
        $element->setDescription('Будет выводиться на сайте после проверки модератором');
        $element->setRequired(true)->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => "Напишите свой вопрос")));
        $this->addElement($element);
    }


    public function addSubmitElement()
    {
        parent::addSubmitElement();

        $this->submit->setLabel('Отправить');
    }
}
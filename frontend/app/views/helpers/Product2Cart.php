<?

class Site_View_Helper_Product2Cart extends Zend_View_Helper_Abstract
{

    public function Product2Cart($Product)
    {
        if ( !($Product instanceof Products ) ) {
            $Product = Doctrine::getTable('Products')->find($Product);
        }

        if ( Cart::issetProduct($Product->id) ) {
            return '<span class="btn btn-green btn2cart btn2cart_'.$Product->id.'" data-id="'.$Product->id.'" data-count="'.Cart::getInstance()->getProductCount($Product->id).'">В корзине</span>';
        }

        $ga = "ga('send', 'event', 'Нажатие на кнопку', 'Купить', '".htmlentities(strip_tags($Product->getFullName()), ENT_COMPAT)."');";
        $data = 'data-id="'.$Product->id.'" data-count="1" data-price="' . $Product->getPrice('retail', 'UAH'). '"';
        return '<span onclick="'.$ga.'" class="btn btn-important btn2cart btn2cart_'.$Product->id.'" '.$data.'>Купить</span>';
    }
}
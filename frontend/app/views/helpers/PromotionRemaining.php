<?

class Site_View_Helper_PromotionRemaining extends Zend_View_Helper_Abstract
{

    public function PromotionRemaining(Promotions $Promotion)
    {
        $remainingDays = $Promotion->getRemainingDays();

        if ( 1 == $remainingDays ) {
            $str = '<div class="promotion_remaining_today">Последний день акции!</div>';
        } elseif( $remainingDays > 1 ) {
            $str  = '<div class="promotion_remaining_title">'.SF::numberof($remainingDays, 'Остал', ['ся', 'ось', 'ось']).'</div>';
            $str .= '<div class="promotion_remaining_number">'.$remainingDays.'</div>';
            $str .= '<div class="promotion_remaining_day">'.SF::numberof($remainingDays, '', array('день', 'дня', 'дней')).'</div>';
        } else {
            $str = 'Акция завершена';
        }

        return "<div class=\"promotion_remaining\">{$str}</div>";
    }


}
<?

class Site_View_Helper_tableComment
{

    public function tableComment($modelName, $fieldName)
    {
    	return Doctrine::getTable($modelName)->getDefinitionOf($fieldName)['comment'];
    }

}
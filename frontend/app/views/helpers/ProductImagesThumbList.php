<?

class Site_View_Helper_ProductImagesThumbList extends Zend_View_Helper_Abstract
{

    public function productImagesThumbList(Products $Product)
    {
        $str = '';
        $view = Zend_Layout::getMvcInstance()->getView();

        $images = $Product->getImage()->getImagesList();

        if ( !empty($images) ) {
            $str .= '<div class="thumbs">';
            foreach ($images as $id=>$img) {
                $str .= '<a href="' . $img['big']['url']   .'" data-lightbox="example-set-'. $Product->id .'" title="">';
                $str .= '<img src="'. $img['thumb']['url'] .'" alt="'.$view->escape($Product->title).' рис.'.$id.'" >';
                $str .= '</a>';
            }
            $str .= '</div>';
        }
        return $str;
    }
}
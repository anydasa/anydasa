<?

class Site_View_Helper_MenuLink extends Zend_View_Helper_Abstract
{
    public function MenuLink($Item2)
    {
        if (!empty($Item2->url)) {
            return $Item2->url;
        }

        return Zend_Layout::getMvcInstance()->getView()->url([
            'menu_alias' => $Item2->alias,
            'menu_id' => $Item2->id,
        ], 'shop', true);
    }
}

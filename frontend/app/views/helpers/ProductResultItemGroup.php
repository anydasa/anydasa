<?

class Site_View_Helper_ProductResultItemGroup extends Zend_View_Helper_Abstract
{
    public function ProductResultItemGroup(Products $Product, $groupType)
    {
        $return = '';

        $items = $Product->getGroupInfo($groupType);

        if ( count($items) > 1 ) {
            $typeName = current($items)['group_title'];
            $return = '<div class="count_in_group">'.$typeName.': ' . count($items) . ' '. SF::numberof(count($items), 'вариант', ['', 'а', 'ов']) .'</div>';
        }

        return $return;
    }
}
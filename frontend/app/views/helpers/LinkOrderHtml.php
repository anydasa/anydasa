<?

class Site_View_Helper_linkOrderHtml
{

    public function linkOrderHtml($name, $label)
    {

    	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());

        list($field, $type) = explode('-', $uri->getQueryAsArray()['sort']);

        if ( empty($type) || $field != $name) {
            $sort_str = $name.'-asc';
        } elseif('asc' == $type) {
            $sort_str = $name.'-desc';
        } elseif('desc' == $type) {
            $sort_str = null;
        }

        $uri->addReplaceQueryParameters( array('sort'=>$sort_str) );

    	return '<a href="'.$uri->getUri().'" class="orderlink '. (($field == $name) ? $type : '').'">'.$label.'</a>';
    }

}
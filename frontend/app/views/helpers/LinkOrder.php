<?

class Site_View_Helper_linkOrder
{

    public function linkOrder($name)
    {
	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());

    list($field, $type) = explode('_', $uri->getQueryAsArray()['sort']);

	$sort_str = ( !empty($type) && 'desc' == $type) ? $name : $name.'_desc';

    $uri->addReplaceQueryParameters( array('sort'=>$sort_str) );

	return $uri->getUri();
    }

}
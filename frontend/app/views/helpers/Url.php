<?

require_once 'Zend/View/Helper/Abstract.php';

class Site_View_Helper_Url extends Zend_View_Helper_Abstract
{

    public function url(array $urlOptions = array(), $name = null, $reset = false, $encode = true)
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $uri = $router->assemble($urlOptions, $name, $reset, $encode);

        return REQUEST_SCHEME . '://' . APPLICATION_DOMAIN . $uri;
    }
}
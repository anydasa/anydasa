<?

class Site_View_Helper_HeadScript extends Zend_View_Helper_HeadScript
{
    public function toString($indent = null)
    {
        $indent = (null !== $indent)
            ? $this->getWhitespace($indent)
            : $this->getIndent();

        if ($this->view) {
            $useCdata = $this->view->doctype()->isXhtml() ? true : false;
        } else {
            $useCdata = $this->useCdata ? true : false;
        }
        $escapeStart = ($useCdata) ? '//<![CDATA[' : '//<!--';
        $escapeEnd   = ($useCdata) ? '//]]>'       : '//-->';

        $items = array();
        $files = array();
        $this->getContainer()->ksort();
        foreach ($this as $item) {
            if (!$this->_isValid($item)) {
                continue;
            }

            $items[] = $this->itemToString($item, $indent, $escapeStart, $escapeEnd);
            $files[] = $item->attributes['src'];
        }

        $res_file = '';
        foreach ($files as $f) {
            $res_file .= file_get_contents($_SERVER['DOCUMENT_ROOT'].$f);
        }

        $compressed_fname = md5($res_file).'.js';
        $compressed =  $_SERVER['DOCUMENT_ROOT'].'/js/compress/'.$compressed_fname;

        if ( !is_file($compressed) ) {
            file_put_contents($compressed, $res_file);
        }

        return '<script type="text/javascript" src="/js/compress/'.$compressed_fname.'"></script>';
    }
}

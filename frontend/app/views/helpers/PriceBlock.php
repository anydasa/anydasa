<?

class Site_View_Helper_PriceBlock extends Zend_View_Helper_Abstract
{
    public function PriceBlock($price, $old, $currencyISO)
    {
        $str = '<span class="price_block">';

        if ( !empty($old) AND $price != $old ) {
            $str .= '<span class="old">'.Currency::format($old, $currencyISO).'</span>';
        }

        $str .=     '<span class="price">'.Currency::format($price, $currencyISO).'</span>';

        if ( !empty($old) AND $price != $old ) {
            $str .= '<span class="diff">Экономия&nbsp;<span>'.Currency::format($old - $price, $currencyISO).'</span></span>';
        }

        $str .= '</span>';

        return $str;
    }
}
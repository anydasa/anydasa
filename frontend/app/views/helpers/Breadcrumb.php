<?

require_once 'Zend/View/Helper/Abstract.php';

class Site_View_Helper_Breadcrumb extends Zend_View_Helper_Abstract
{

    public function breadcrumb($current, array $urlOptions = array())
    {
        $view   = Zend_Layout::getMvcInstance()->getView();

        $str = '<div class="breadcrumb">';
        $str .= '<div><a href="/">Главная</a></div>';

        foreach ($urlOptions as $item) {
            $str .= '<div>';
            $str .= '<a href="'.$item['url'].'">'.$view->escape($item['title']).'</a>';
            $str .= '</div>';
        }

        if ( !empty($current) ) {
            $str .= '<div>'.$current.'</div>';
        }

        $str .= '</div>';

        return $str . $this->schema($urlOptions);
    }

    private function schema($urlOptions)
    {
        $itemListElement = [];
        foreach ($urlOptions as $k => $item) {
            $itemListElement []= [
                "@type" => "ListItem",
                "position" => $k+1,
                "item" => [
                    "@id" => $item['url'],
                    "name" => $item['title']
                ]
            ];
        }

        $BreadcrumbList = [
            "@context" => "http://schema.org",
            "@type" => "BreadcrumbList",
            "itemListElement" => $itemListElement
        ];

        return '<script type="application/ld+json">'.json_encode($BreadcrumbList).'</script>';
    }
}

<?

class Site_View_Helper_PromotionPeriodLabel extends Zend_View_Helper_Abstract
{

    public function promotionPeriodLabel(Promotions $Promotion)
    {

        $str = '<div class="promotion-period">';

        if ( $Promotion->time_on == $Promotion->time_off ) {
            $str .= '<div class="time_on">Только</div>';
        } else {
            $str .= '<div class="time_on">';
            $str .= date('d', strtotime($Promotion->time_on))  . ' ' . Site_Months::getGenitiveByNumber(date('n', strtotime($Promotion->time_on)));
            $str .= ' - </div>';
        }

        $str .= '<div class="time_off">';
        $str .= date('d',strtotime($Promotion->time_off)) . ' ' . Site_Months::getGenitiveByNumber(date('n', strtotime($Promotion->time_off)));
        $str .= '</div>';
        $str .= '</div>';

        return $str;
    }


}
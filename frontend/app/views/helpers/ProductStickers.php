<?

class Site_View_Helper_ProductStickers extends Zend_View_Helper_Abstract
{
    public function ProductStickers(Products $Product)
    {
        $return = '';
        foreach ($Product->Stickers as $Sticker) {
            if ( $Sticker->is_show_in_product ) {
                $return .= "<div class='sticker sticker-{$Sticker->alias}' style='background: {$Sticker->background}; color: {$Sticker->color}; '>{$Sticker->title}</div>";
            }
        }
        return $return;
    }
}
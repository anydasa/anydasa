<?

class Site_View_Helper_linkReplaceGetParam
{

    public function linkReplaceGetParam($array)
    {

    	$uri = Zend_Uri::factory('http://' . Zend_Controller_Front::getInstance()->getRequest()->getHttpHost()  . Zend_Controller_Front::getInstance()->getRequest()->getRequestUri());
    	$uri->addReplaceQueryParameters($array);

    	return $uri->getUri();
    }

}
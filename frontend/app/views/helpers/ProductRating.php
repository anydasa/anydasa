<?

class Site_View_Helper_ProductRating extends Zend_View_Helper_Abstract
{
    public function ProductRating($Product)
    {
        return $str = '';
        if ( $Product->rating > 0 ) {
            $rating = round($Product->rating);

            $active  = @array_fill(0, $rating, '<span class="active"></span>');
            $passive = @array_fill(0, 5-$rating, '<span class="passive"></span>');

            if ( $Product->rating ) {
                $str .= '<span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                            <meta itemprop="ratingValue" content="'.$Product->rating.'" />
                            <meta itemprop="bestRating" content="5" />
                            <meta itemprop="ratingCount" content="20" />
                        </span>';
            }

            $str .= '<span class="rating rating2x">' . @implode('', $active) . @implode('', $passive). '</span>';
        }
        return $str;
    }
}
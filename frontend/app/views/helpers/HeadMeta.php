<?

class Site_View_Helper_HeadMeta extends Zend_View_Helper_HeadMeta
{
    public function getContentByTypeName($type, $name)
    {
        foreach ($this->getContainer()->getArrayCopy() as $item) {
            if ($item->type == $type && $item->name == $name) {
                return $item->content;
            }
        }
        return '';
    }

    protected function _isValid($item)
    {
        if ((!$item instanceof stdClass)
            || !isset($item->type)
            || !isset($item->modifiers))
        {
            return false;
        }

        $isHtml5 = is_null($this->view) ? false : $this->view->doctype()->isHtml5();

        if (!isset($item->content)
            && (! $isHtml5 || (! $isHtml5 && $item->type !== 'charset'))) {
            return false;
        }

        return true;
    }

}

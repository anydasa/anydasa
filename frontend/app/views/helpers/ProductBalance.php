<?

class Site_View_Helper_ProductBalance extends Zend_View_Helper_Abstract
{

    public function productBalance(Products $Product)
    {
        return $Product->balance > 0 ? 'В наличии' : ($Product->expected_balance > 0 ? 'Ожидается' : 'Нет');
    }
}
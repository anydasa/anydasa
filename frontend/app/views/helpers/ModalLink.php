<?

class Site_View_Helper_modalLink
{

    public function modalLink($label, $route_name, $route_params = array())
    {
        $view = Zend_Layout::getMvcInstance()->getView();
    	return '<span data-modal-link="'.$view->url($route_params, $route_name).'" data-modal-name="'.$route_name.'">'.$label.'</span>';
    }

}
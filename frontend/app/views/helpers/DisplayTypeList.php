<?

require_once 'Zend/View/Helper/Abstract.php';

class Site_View_Helper_DisplayTypeList extends Zend_View_Helper_Abstract
{

    public function displayTypeList()
    {
        $view = Zend_Layout::getMvcInstance()->getView();

        $str = '<div'. ('list' == $_SESSION['display_type_list'] ? ' class="active" ' : '') .'>
            <a href="'. $view->url(array('type'=>'list'), 'set-display-type-list') .'" data-title="Показывать кратко" class="tooltip">
                <img src="/img/application-list.png" width="16" height="16" alt="" /></a></div> ';

        $str .= '<div'. ('tile' == $_SESSION['display_type_list'] ? ' class="active" ' : '') .'>
            <a href="'. $view->url(array('type'=>'tile'), 'set-display-type-list') .'" data-title="Показывать подробно" class="tooltip">
                <img src="/img/application-split-tile.png" width="16" height="16" alt="" /></a></div>';
        return $str;

    }
}
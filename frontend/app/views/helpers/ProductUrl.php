<?

class Site_View_Helper_ProductUrl extends Zend_View_Helper_Abstract
{

    public function productUrl(Products $Product)
    {
        return Zend_Layout::getMvcInstance()->getView()->url([
            'alias'=>$Product->alias,
            'id'=>$Product->id,
        ], 'product');
    }
}
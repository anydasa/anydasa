<?

class Site_View_Helper_showPager
{


    public function showPager(Doctrine_Pager_Range $pagerRange)
    {
        Zend_Uri::setConfig(['allow_unwise' => true]);

        $Url = Zend_Uri::factory(REQUEST_SCHEME.'://' .
            Zend_Controller_Front::getInstance()->getRequest()->getHttpHost() .
            Zend_Controller_Front::getInstance()->getRequest()->getRequestUri()
        );

        $pager = $pagerRange->getPager();

        $this->_404Exception($pager->getLastPage(), $Url);

        if ($pager->getLastPage() > 1) {
            $str = '<div class="pagination-container">';
            $view = Zend_Layout::getMvcInstance()->getView();

            if (1 != $pager->getPage()) {
                $Url->addReplaceQueryParameters(['page'=>$pager->getPreviousPage() == 1 ? null : $pager->getPreviousPage()]);
                $str .= '<a title="' . $view->translate('На страницу назад') . ' (Ctrl + ←)" id="prev" class="prev" href="' . $Url->getUri() . '"><span>' . $view->translate('<') . '</span></a>';
                $view->headLink(array('rel'=>'prev', 'href'=> $Url->getUri()));
            }
//            $str .= '<div class="pagination_content">';
            foreach ($pagerRange->rangeAroundPage() as $key => $page) {
                $Url->addReplaceQueryParameters(['page'=>$page == 1 ? null : $page]);
                if ($page == $pager->getPage()) {
                    $str .= '<strong>' . $page . '</strong>';
                } else {
                    $str .= '<a href="' . $Url->getUri() . '">' . $page . '</a>';
                }
            }
//            $str .= '</div>';
            if ($pager->getPage() != $pager->getLastPage()) {
                $Url->addReplaceQueryParameters(['page'=>$pager->getNextPage()]);
                $str .= '<a title="' . $view->translate('На страницу вперед') . ' (Ctrl + →)" rel="next" id="next" class="next" href="' . $Url->getUri() . '">' . $view->translate('>') . '</a>';
                $view->headLink(array('rel'=>'next', 'href'=> $Url->getUri()));
            }
            $str .= '</div>';
        }

        return $str;
    }

    private function _404Exception($lastPage, Zend_Uri_Http $Url)
    {
        if (isset($Url->getQueryAsArray()['page'])
            AND ($lastPage < $Url->getQueryAsArray()['page'] || 0 >= $Url->getQueryAsArray()['page'])
        ) {
            $Url->addReplaceQueryParameters(['page' => null]);

            $_redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
            $_redirector->gotoUrl($Url->getUri());


//            throw new Site_Exception_NotFound;
        }
    }
}
<?php

class Site_View_Helper_HandleLogin extends Zend_Controller_Action_Helper_Abstract
{

    public function handleLogin()
    {
        $view   = Zend_Layout::getMvcInstance()->getView();

        if ( empty($view->user) ) :
            $str = "
                <ul>
                    <li><a class=\"registration\"> {$view->modalLink('<i></i> Регистрация', 'registration')}</a></li>
                    <li><a class=\"login\"> {$view->modalLink('<i></i> Вход', 'login')}</a></li>
                </ul>";
        else :
            $str = "
                <ul>
                    <li id='auth-user-link'>
                        <a href='{$view->url(array('controller' => 'info'), 'user')}'>{$view->user->email}</a>
                    </li>
                    <li><a href=\"{$view->url(array(), 'logout')}\">выход</a></li>
                </ul>";
        endif;


        return '<div>'. $str .'</div>';
    }


}

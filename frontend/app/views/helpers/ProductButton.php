<?

class Site_View_Helper_ProductButton extends Zend_View_Helper_Abstract
{
    public function ProductButton(Products $Product)
    {
        if (  Products::STATUS_NOT_AVAILABLE == $Product->status || Products::STATUS_WAIT == $Product->status ) {
            $ga = "ga('send', 'event', 'Нажатие на кнопку', 'Сообщить когда появится', '". addcslashes($Product->getFullName(), '"\'') ."');";
            $return = '<div onclick="'.$ga.'" class="btn btn-normal let-me-know" data-modal-link="/product-let-me-know/'.$Product->id.'/">Сообщить, когда появится</div>';
        } else {
            $return = Zend_Layout::getMvcInstance()->getView()->Product2Cart($Product);
        }
        return '<div class="product_button_wrap">'. $return .'</div>';
    }
}
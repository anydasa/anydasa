<?

class Site_View_Helper_ProductYourPrice extends Zend_View_Helper_Abstract
{
    public function ProductYourPrice(Products $Product, $user = null)
    {
        $view = Zend_Layout::getMvcInstance()->getView();

        if ( empty($user) ) {
            return $view->modalLink('Вход', 'login');
        }

        return ($p = $Product->getFormatedUserPrice($user, $_SESSION['currency'])) ? $p : 'Звоните';
    }
}
<?

class Site_View_Helper_ProductName extends Zend_View_Helper_Abstract
{
    public function ProductName($Product)
    {
        return trim($Product->type . ' ' . $Product->title);
    }
}
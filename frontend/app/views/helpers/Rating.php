<?

class Site_View_Helper_Rating extends Zend_View_Helper_Abstract
{
    public function Rating($rating)
    {
        $rating = round($rating);
        $active  = @array_fill(0, $rating, '<span class="active"></span>');
        $passive = @array_fill(0, 5-$rating, '<span class="passive"></span>');

        return '<span class="rating">' . @implode('', $active) . @implode('', $passive). '</span>';
    }
}
<?

use Twig\Environment;
use Twig\Loader\ArrayLoader;

class Site_View_Helper_BannerShopFourthProduct extends Zend_View_Helper_Abstract
{
    public function BannerShopFourthProduct($uri)
    {
        $BannersList = Banners::getBanners('shop_fourth_product', $uri);

        if (0 == $BannersList->count()) {
            return '';
        }

        /** @var Banners $Banner */
        $Banner = $BannersList->getFirst();

        $Twig = new Environment(new ArrayLoader([
            'tpl' => '<div class="product_item product_table" style="text-align: center;">
                        {% if url is not empty %}<a href="{{ url }}">{% endif %}
                            <img src="{{ src }}" style="max-width: 100%;">
                        {% if url is not empty %}</a>{% endif %}
                    </div>'
        ]));

        return $Twig->render('tpl', $data = [
            'src' => $Banner->getImageUrl(),
            'url' => $Banner->url,
        ]);
    }
}

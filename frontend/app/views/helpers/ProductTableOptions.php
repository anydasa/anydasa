<?

class Site_View_Helper_ProductTableOptions extends Zend_View_Helper_Abstract
{

    public function ProductTableOptions(Products $Product)
    {
        $str = '';

        $options = $Product->getProps();

        if ( !empty($options) ) {
            $str .= '<ul>';
            $prevOption = [];

            foreach ($options as $option) {
                if ( $option['is_prime'] && !empty($option['value']) ) {

                    if ( @$prevOption['group'] != $option['group'] || empty($option['group']) ) {

                        $str .= '<li>';
                        $str .= '<span>'. $this->getTitle($option) . '</span>: ';
                    }

                    $str .= '<i>';
                    if ( !empty($option['param_brief']) && $this->getTitle($option) != $option['param_brief'] ) {
                        $str .= $option['param_brief'] . ' ';
                    }
                    $str .= $option['value'] . rtrim(' ' . $option['unit']);
                    $str .= '</i> ';
                    $prevOption = $option;
                }
            }

            $str .= '</ul>';
        }

        return $str;
    }

    private function getTitle($option)
    {
        if ( !empty($option['group']) ) {
            return !empty($option['group_brief']) ? $option['group_brief'] : $option['group'];
        } else {
            return !empty($option['param_brief']) ? $option['param_brief'] : $option['param'];
        }
    }
}
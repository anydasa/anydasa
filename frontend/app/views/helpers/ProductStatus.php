<?

class Site_View_Helper_ProductStatus extends Zend_View_Helper_Abstract
{
    public function ProductStatus(Products $Product)
    {
        return $Product->getStatusTitle();
    }
}
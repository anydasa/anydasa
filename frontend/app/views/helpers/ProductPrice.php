<?

class Site_View_Helper_ProductPrice extends Zend_View_Helper_Abstract
{
    public function ProductPrice(Products $Product)
    {
        $return = '';

        if (  Products::STATUS_NOT_AVAILABLE != $Product->status ) {

            $return .= '<div class="price_container">';

            $return .= '<span class="price">'. $Product->getFormatedPrice('retail', 'UAH').'</span>';

            if ( $Product->hasSticker('markdown') || $Product->hasSticker('sale') ) {
                if ( empty($Product->markdown_percent) ) {
                    $Product->markdown_percent = 25;
                }
                $return .= '<span class="old_price">'. $Product->getPreMarkdownPrice('UAH')->formatHtml().'</span>';
            }

            $return .= '</div>';
        }

        return $return;
    }
}
<?

class Brand_IndexController extends Controller_Main
{

    public function indexAction()
    {
        $params = $this->params;

        if ( !$brand = Doctrine::getTable('ProductsBrands')->findOneByAlias($params['brand-alias']) ) {
            throw new Site_Exception_NotFound;
        }

        $this->view->brand       = $brand;
        $query                   = Products_Query::create()->setOnlyPublic()->setParamsQuery($params);
        $countOnPage             = ('all' == $params['page']) ? 999999 : 40;

        $pager                   = new Doctrine_Pager($query, $params['page'], $countOnPage);
        $this->view->ProductList = $pager->execute();
        $this->view->pagerRange  = $pager->getRange('Sliding', array('chunk' => 9));

    }


}
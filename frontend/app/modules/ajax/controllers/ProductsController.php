<?

class Ajax_ProductsController extends Controller_Main
{

    public function setSortAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->_request->isPost() ) {
            $_SESSION['products_sort'] = $this->_request->getPost('sort');
        }

    }

    public function setFirstAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->_request->isPost() ) {
            $_SESSION['products_first'] = $this->_request->getPost('first');
        }

    }

}
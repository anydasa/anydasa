<?

class Ajax_AuthController extends Controller_Main
{

    public function loginAction()
    {
        $result = [
            "success" => false,
            "data"    => [],
            "errors"  => [],
            "info"    => []
        ];

        if ( $this->_request->isPost() ) {
            $login    = $this->_request->getPost('login');
            $password = $this->_request->getPost('password');

            $login = (new Zend_Filter_StringToLower)->filter($login);
            $password = md5($password);

            if ( !(new Site_Validate_EmailBool)->isValid($login) ) {
                $result['errors']['login'] = 'Email указан не корректно';
            } elseif ( !$User = Doctrine::getTable('Users')->findOneByEmail($login) ) {
                $result['errors']['login'] = 'Вы не зарегистрированы';
            } elseif ( $User->password !== $password ) {
                $result['errors']['password'] = 'Не верно указан пароль';
            } elseif ( !$User->is_enabled ) {
                $result['errors']['login'] = 'Ваш аккаунт выключен';
            } else {
                Zend_Auth::getInstance()->getStorage()->write($User);
                $User->last_authorized = new Doctrine_Expression('NOW()');
                $User->save();
                $result['success'] = true;
            }
        }

        return $this->_helper->json->sendJson($result);
    }

}
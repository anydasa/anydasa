<?

class Ajax_RemindPasswordController extends Controller_Main
{

    public function indexAction()
    {

        $result = [
            "success" => false,
            "data"    => [],
            "errors"  => [],
            "info"    => []
        ];

        if ( $this->_request->isPost() ) {
            $login    = $this->_request->getPost('login');

            $login = (new Zend_Filter_StringToLower)->filter($login);

            if ( !(new Site_Validate_EmailBool)->isValid($login) ) {
                $result['errors']['login'] = 'Email указан не корректно';
            } elseif ( !$User = Doctrine::getTable('Users')->findOneByEmail($login) ) {
                $result['errors']['login'] = 'Вы не зарегистрированы';
            } else {
                //$this->_sendMail($User);
                $result['success'] = true;
            }
        }

        return $this->_helper->json->sendJson($result);
    }

    private function _sendMail($User)
    {
        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('recovery_password') ) {
            throw new Exception('Отсутсвует recovery password почтовый шаблон (MailTemplates)');
        }

        $userdata = $User->toArray();

        foreach ($userdata as $key=>$value) {
            $MailTemplate->body_html = str_replace("{{user_$key}}", $value, $MailTemplate->body_html);
            $MailTemplate->body_text = str_replace("{{user_$key}}", $value, $MailTemplate->body_text);
        }

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyText($MailTemplate->body_text);
        $mail->setBodyHtml($MailTemplate->body_html);
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($User->email, $User->name);
        $mail->setSubject($MailTemplate->subject);
        $mail->send();
    }

}
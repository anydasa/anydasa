<?

class Oauth_FbController extends Controller_Main
{

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $params = [
            'app_id'     => '1555315091389124',
            'scope'     => 'email,user_likes',
            'redirect_uri'  =>  Zend_Registry::getInstance()->config->url->domain . 'oauth/fb/callback',
            'response_type' => 'code',
        ];

        $this->redirect('https://www.facebook.com/dialog/oauth?' .  http_build_query($params));
    }

    public function callbackAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->getRequest()->getParam('code') ) {
            $params = [
                'client_id'     => '1555315091389124',
                'client_secret' => 'f43fa37b23b07d9335699ef32899c4ec',
                'code'          => $this->getRequest()->getParam('code'),
                'redirect_uri'  =>  Zend_Registry::getInstance()->config->url->domain . 'oauth/fb/callback',
            ];

            parse_str(file_get_contents('https://graph.facebook.com/oauth/access_token?' . urldecode(http_build_query($params))), $token);

            if ( isset($token['access_token']) ) {
                $params = array(
                    'access_token' => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me?' . urldecode(http_build_query($params))), true);

                if ( !empty($userInfo) ) {
                    if ( ! $User = Doctrine::getTable('Users')->findOneByEmail($userInfo['email']) ) {
                        $User =  new Users;
                        $User->name  = $userInfo['name'];
                        $User->email = $userInfo['email'];
                        $User->password = SF::make_password(6);
                    }

                    $User->last_authorized = new Doctrine_Expression('NOW()');
                    $User->save();

                    Zend_Auth::getInstance()->getStorage()->write($User);
                    $this->redirect($_SERVER['HTTP_REFERER']);
                }
            }

        }

    }

}
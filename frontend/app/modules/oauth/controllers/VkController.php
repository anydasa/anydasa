<?

class Oauth_VkController extends Controller_Main
{

    public function indexAction()
    {

        $this->_helper->viewRenderer->setNoRender(true);

        $params = [
            'client_id'     => '4801427',
            'scope'         => 'email',
            'redirect_uri'  =>  Zend_Registry::getInstance()->config->url->domain . 'oauth/vk/callback',
            'response_type' => 'code',
            'v'             => 'API_VERSION',
            'state'         => 'SESSION_STATE'
        ];

        $this->redirect('https://oauth.vk.com/authorize?' .  http_build_query($params));
    }

    public function callbackAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->getRequest()->getParam('code') ) {
            $params = [
                'client_id'     => '4801427',
                'client_secret' => 'DMCuYAbeq5QS7SaZG5Rd',
                'code'          => $this->getRequest()->getParam('code'),
                'redirect_uri'  =>  Zend_Registry::getInstance()->config->url->domain . 'oauth/vk/callback',
            ];

            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

            if ( isset($token['access_token']) ) {
                $params = array(
                    'user_id'         => $token['user_id'],
                    'fields'       => 'email,uid,first_name,last_name,screen_name,sex,bdate,photo_big',
                    'access_token' => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);

                if ( !empty($userInfo) ) {
                    if ( ! $User = Doctrine::getTable('Users')->findOneByEmail($token['email']) ) {
                        $User =  new Users;
                        $User->name  = $userInfo['name'];
                        $User->email = $token['email'];
                        $User->password = SF::make_password(6);
                    }

                    $User->last_authorized = new Doctrine_Expression('NOW()');
                    $User->save();

                    Zend_Auth::getInstance()->getStorage()->write($User);
                    $this->redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
        $this->redirect('/');
    }
}
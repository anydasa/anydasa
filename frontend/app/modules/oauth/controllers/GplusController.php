<?

class Oauth_GplusController extends Controller_Main
{
    public function init()
    {
        parent::init();

        $this->OauthParams = [
            'client_id'     => '976089991926-mntstk5pnbcdbmir5lo55t50ptcc7d2k.apps.googleusercontent.com',
            'redirect_uri'  => Zend_Registry::getInstance()->config->url->domain . 'oauth/gplus/callback',
            'client_secret' => 'VILc03Hxx8fDHJqDymj--vBx',
        ];

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {

        $params = [
            'client_id'     => $this->OauthParams['client_id'],
            'scope'         => 'https://www.googleapis.com/auth/userinfo.email',
            'redirect_uri'  => $this->OauthParams['redirect_uri'],
            'response_type' => 'code',
        ];

        $this->redirect('https://accounts.google.com/o/oauth2/auth?' .  http_build_query($params));
    }

    public function callbackAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->getRequest()->getParam('code') ) {

            $params = array(
                "code"          => $this->getRequest()->getParam('code'),
                "client_id"     => $this->OauthParams['client_id'],
                "client_secret" => $this->OauthParams['client_secret'],
                "redirect_uri"  => $this->OauthParams['redirect_uri'],
                "grant_type"    => "authorization_code"
            );


            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            );

            $token_responce = file_get_contents('https://accounts.google.com/o/oauth2/token', false, stream_context_create($opts));

            $token = json_decode($token_responce, true);

            if ( isset($token['access_token']) ) {
                $params = array(
                    'access_token' => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?' . urldecode(http_build_query($params))), true);

                if ( !empty($userInfo) ) {
                    if ( ! $User = Doctrine::getTable('Users')->findOneByEmail($userInfo['email']) ) {
                        $User =  new Users;
                        $User->name  = $userInfo['name'];
                        $User->email = $userInfo['email'];
                        $User->password = SF::make_password(6);
                    }

                    $User->last_authorized = new Doctrine_Expression('NOW()');
                    $User->save();

                    Zend_Auth::getInstance()->getStorage()->write($User);
                    $this->redirect($_SERVER['HTTP_REFERER']);
                }
            }

        }

    }

}

<?

class Oauth_YaController extends Controller_Main
{
    public function init()
    {
        parent::init();

        $this->OauthParams = [
            'client_id'     => '33cb5112f74d4f04909e4726762c8ce1',
            'redirect_uri'  => Zend_Registry::getInstance()->config->url->domain . 'oauth/ya/callback',
            'client_secret' => '4260ecc11ae744cc946083df569d50a2',
        ];

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {

        $params = [
            'client_id'     => $this->OauthParams['client_id'],
            'redirect_uri'  => $this->OauthParams['redirect_uri'],
            'response_type' => 'code',
        ];

        $this->redirect('https://oauth.yandex.ru/authorize?' .  http_build_query($params));
    }

    public function callbackAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->getRequest()->getParam('code') ) {

            $params = array(
                "code"          => $this->getRequest()->getParam('code'),
                "client_id"     => $this->OauthParams['client_id'],
                "client_secret" => $this->OauthParams['client_secret'],
                "redirect_uri"  => $this->OauthParams['redirect_uri'],
                "grant_type"    => "authorization_code"
            );


            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            );

            $token_responce = file_get_contents('https://oauth.yandex.ru/token', false, stream_context_create($opts));

            $token = json_decode($token_responce, true);


            if ( isset($token['access_token']) ) {
                $params = array(
                    'format'        => 'json',
                    'oauth_token'  => $token['access_token']
                );

                $userInfo = json_decode(file_get_contents('https://login.yandex.ru/info?' . urldecode(http_build_query($params))), true);

                if ( !empty($userInfo) ) {
                    if ( ! $User = Doctrine::getTable('Users')->findOneByEmail($userInfo['default_email']) ) {
                        $User =  new Users;
                        $User->name  = $userInfo['real_name'];
                        $User->email = $userInfo['default_email'];
                        $User->password = SF::make_password(6);
                    }

                    $User->last_authorized = new Doctrine_Expression('NOW()');
                    $User->save();

                    Zend_Auth::getInstance()->getStorage()->write($User);
                    $this->redirect($_SERVER['HTTP_REFERER']);
                }
            }

        }

    }

}

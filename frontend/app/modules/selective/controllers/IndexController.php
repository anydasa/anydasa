<?

class Selective_IndexController extends Controller_Main
{

    public function indexAction()
    {
        if ( false === ($Record = Doctrine::getTable('ProductsLists')->findOneByAlias($this->_request->getParam('alias'))) ) {
            throw new Site_Exception_NotFound;
        }

        $this->view->Record = $Record;
    }

}
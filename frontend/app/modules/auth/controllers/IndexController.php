<?

class Auth_IndexController extends Controller_Main
{

    public function loginAction()
    {
        $form = new Form_Auth('login');
        $form->setAction($this->view->url(array(), 'login'));
        $form->setDescription('Войти');

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $User = Doctrine::getTable('Users')->findOneByEmail($form->email->getValue());
                Zend_Auth::getInstance()->getStorage()->write($User);

                $User->last_authorized = new Doctrine_Expression('NOW()');
                $User->save();

                $this->redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}
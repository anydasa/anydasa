<?

class Auth_RecoveryPasswordController extends Controller_Main
{

    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $form = new Form_RecoveryPassword('recovery-password');
        $form->setAction($this->view->url(array(), 'recovery-password'));
        $form->setDescription('Восстановление пароля');

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $User = Doctrine::getTable('Users')->findOneByEmail($form->email->getValue());

                $newpass = SF::make_password(6);
                $User->password = md5($newpass);
                $User->save();

                $this->_sendMail($User, $newpass);

                echo Zend_Registry::getInstance()->spelling['success_recovery_password'];
                return;
            }
        }

        echo $form;
    }

    private function _sendMail($User, $newpass)
    {
        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('recovery_password') ) {
            throw new Exception('Отсутсвует recovery password почтовый шаблон (MailTemplates)');
        }

        $data = [
            'user' =>  $User->toArray(),
            'new_pass' => $newpass
        ];

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($MailTemplate->renderHTML($data));
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($User->email, $User->name);
        $mail->setSubject($MailTemplate->subject);
        $mail->send();


    }
}
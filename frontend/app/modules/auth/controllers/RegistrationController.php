<?

class Auth_RegistrationController extends Controller_Main
{
    public function indexAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $form = new Form_Registration('registration');
        $form->setAction($this->view->url(array(), 'registration'));
        $form->setDescription('Регистрация');

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $User = new Users;
                $User->fromArray($form->getValues());
                $User->save();

                $this->_sendMail($User);

                $this->_helper->flashMessenger->addMessage("<script>ga('send', 'pageview', '/reg-ok');</script>", 'registration_ga');

                return $this->_autoLogin();
            }
        }

        echo $form;
    }

    private function _sendMail($User)
    {
        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('post_registration') ) {
            throw new Exception('');
        }

        $data = ['user' => $User->toArray()];

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml( $MailTemplate->renderHTML($data) );
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($User->email, $User->name);
        $mail->setSubject($MailTemplate->subject);
        $mail->send();

        $mail2 = clone $mail;
        $mail2->clearRecipients()->addTo('at@anydasa.com', '');
        $mail2->send();

        $mail2 = clone $mail;
        $mail2->clearRecipients()->addTo('info@anydasa.com', '');
        $mail2->send();
    }

    private function _autoLogin()
    {
        $redirectRouteDefaults = Zend_Controller_Front::getInstance()
                    ->getRouter()
                    ->getRoute('login')
                    ->getDefaults();

        $this->_request->setPost($this->_request->getPost());

        return $this->forward(
                    $redirectRouteDefaults['action'],
                    $redirectRouteDefaults['controller'],
                    $redirectRouteDefaults['module']
        );
    }
}
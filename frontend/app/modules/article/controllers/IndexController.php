<?

class Article_IndexController extends Controller_Main
{

    public function indexAction()
    {
        /*$groups = Doctrine_Query::create()->from('ArticleGroup')->fetchArray();

        foreach ($groups as $key => $group){
            $query = Doctrine_Query::create()->from('ArticleItem')->where('id_group = ?', $group['id']);
            $groups[$key]['articles'] = $query->execute();
        }

        $this->view->groups = $groups;*/

        $this->view->articles = Doctrine_Query::create()->from('ArticleItem')->where('is_enabled = ?', '1')->orderBy('date_publication DESC')->execute();
    }

    public function viewAction()
    {
        /** @var ArticleItem $node */
        if ( !$node = Doctrine::getTable('ArticleItem')->findOneByAlias($this->params['alias']) ) {
            throw new Site_Exception_NotFound;
        }

        $this->view->node = $node;

        $this->view->headTitle( $node->meta_title.'.' );
        $this->view->headDescription( $node->meta_description.'.' );

    }
}
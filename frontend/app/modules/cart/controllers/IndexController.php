<?

class Cart_IndexController extends Controller_Main
{

    public function preDispatch()
    {
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function deleteItemAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        Cart::getInstance()->removeProduct($this->_request->getPost()['id']);

        echo '$("#head-cart").html("'.SF::escapeJavaScriptText($this->view->render('header-cart.phtml')) .'");';
        echo '$(".btn2cart_'.$this->_request->getPost()['id'].'").replaceWith("'.SF::escapeJavaScriptText($this->view->Product2Cart($this->_request->getPost()['id'])) .'");';

        if ( !Cart::getInstance()->isEmpty() ) {
            echo '$("#head-cart .cart_items").show(); mouseleaveTimerCart = setTimeout(function() { $("#head-cart .cart_items").hide(); }, 1000);';
        }
    }

    public function clearAction()
    {
        unset($_SESSION['cart']['id->count']);
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function setPaymentAction()
    {
        $response = [];

        if ( $this->_request->isPost() ) {
            Cart::getInstance()->setPayment($this->_request->getPost('payment'));

            if ( $this->_request->getPost('payment') == 'credit') {
                $Session = Zend_Session::namespaceGet('Order');
                if(empty($Session['steps']['contacts']['data']['email'])){
                    $this->view->form  = new Form_Order_Payment_Email();
                    $response['modal'] = $this->view->render('ckeck-email.phtml');
                }
            }


        }
        $response['append'] = $this->view->render('cart-items.phtml');
        echo json_encode($response);
    }

    public function setItemAction()
    {
        if ( $this->_request->isPost() ) {
            $post =  $this->_request->getPost();

            $Product = Products_Query::create()
                ->setOnlyPublic()
                ->setParamsQuery(array('id'=> $post['id']))
                ->fetchOne();

            if ( !$Product ) {
                require_once 'Site/Exception/NotFound.php';
                throw new Site_Exception_NotFound;
            }

            Cart::getInstance()->setProduct($Product->id, $post['count']);

            $this->view->Product = $Product;
            $this->view->Cart    = Cart::getInstance();

            $this->render('popup');
        }
    }

    public function setProductSetAction()
    {
        if ( $this->_request->isPost() ) {
            $post =  $this->_request->getPost();

            if ( empty($post['id']) || empty($post['id_owner'])) {
                require_once 'Site/Exception/NotFound.php';
                throw new Site_Exception_NotFound;
            }

            $Owner = Products_Query::create()
                ->setOnlyPublic()
                ->setParamsQuery(array('id'=> $post['id_owner']))
                ->fetchOne();

            $Products = Products_Query::create()
                ->setOnlyPublic()
                ->setParamsQuery(array('id'=> $post['id']))
                ->execute();

            if ( !$Products->count() || !$Owner->exists() ) {
                require_once 'Site/Exception/NotFound.php';
                throw new Site_Exception_NotFound;
            }

            Cart::getInstance()->setProductsSet($Owner, $Products);

            $this->view->Product = $Owner;
            $this->view->Owner = $Owner;
            $this->view->Products = $Products;
            $this->view->Cart    = Cart::getInstance();

            $this->render('popup-set');
        }
    }

    public function setCountInListAction()
    {
        if ( $this->_request->isPost() ) {
            $post =  $this->_request->getPost();

            $Product = Products_Query::create()
                ->setOnlyPublic()
                ->setParamsQuery(array('id'=> $post['id']))
                ->fetchOne();

            if ( !$Product ) {
                require_once 'Site/Exception/NotFound.php';
                throw new Site_Exception_NotFound;
            }

            Cart::getInstance()->setProduct($Product->id, $post['count']);
        }

        echo $this->view->render('cart-items.phtml');
    }

    public function deleteFromListAction()
    {
        if ( $this->_request->isPost() ) {
            $post =  $this->_request->getPost();
            Cart::getInstance()->removeProduct($post['id']);
        }

        echo $this->view->render('cart-items.phtml');
    }

}
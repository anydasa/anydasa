<?

class User_InfoController extends Controller_Account
{

    public function indexAction()
    {
        $form = new Form_User_Info('UserInfo');
        $form->setAction($_SERVER['REQUEST_URI']);

        if ( $this->_request->isPost() ) {
            if ( $form->isValid($this->_request->getPost()) ) {
                $User = $this->user;
                $User->fromArray($form->getValues());
                $User->save();

                if ( $this->_request->isXmlHttpRequest() ) {
                    $this->view->success = true;
                } else {
                    $this->redirect($_SERVER['REQUEST_URI']);
                }

            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        } else {
            $form->populate($this->user->toArray());
        }

        $this->view->user = $this->user;
        $this->view->form = $form;
    }
}
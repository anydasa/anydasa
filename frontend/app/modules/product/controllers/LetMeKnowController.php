<?

class Product_LetMeKnowController extends Controller_Main
{
    public function preDispatch()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction()
    {
        $form = new Form_Product_LetMeKnow('ProductLetMeKnow');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription('Сообщить, когда появится');
        $form->id_product->setValue($this->_request->getParam('id_product'));

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $ProductLetMeKnow = new ProductsLetMeKnow;
                $ProductLetMeKnow->fromArray($form->getValues());
                $ProductLetMeKnow->save();
                $this->view->ProductLetMeKnow = $ProductLetMeKnow;
                $this->render('success');
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        echo $form;
    }


}
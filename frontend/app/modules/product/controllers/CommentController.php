<?

class Product_CommentController extends Controller_Main
{
    public function preDispatch()
    {
        if ( !$this->_request->isXmlHttpRequest() ) {
            $this->forward('access-denied', 'error', 'default');
        }

        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function addCommentAction()
    {
        if ( !$Product = Doctrine::getTable('Products')->find($this->getParam('id_product')) ) {
            return $this->forward('product-not-found', 'index', 'product');
        }

        $form = new Form_Product_Comment('ProductAddComment');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription('Отзыв о товаре <div style="font-size: 13px; color: GrayText; text-transform: none">'.$Product->title.'</div>');
        $form->id_product->setValue($Product->id);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $ProductsComment = new ProductsComments;
                $ProductsComment->fromArray($form->getValues());
                $ProductsComment->save();
                $this->view->Product = $ProductsComment->Products;
                $this->render('post-add-comment');
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        echo $form;
    }


    public function addQuestionAction()
    {
        if ( !$Product = Doctrine::getTable('Products')->find($this->getParam('id_product')) ) {
            return $this->forward('product-not-found', 'index', 'product');
        }

        $form = new Form_Product_Question('ProductAddQuestion');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription('Вопрос о товаре <div style="font-size: 13px; color: GrayText; text-transform: none">'.$Product->title.'</div>');
        $form->id_product->setValue($Product->id);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $ProductsComment = new ProductsComments;
                $ProductsComment->fromArray($form->getValues());
                $ProductsComment->save();
                $this->view->Product = $ProductsComment->Products;
                $this->render('post-add-question');
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        echo $form;
    }

    public function addAnswerAction()
    {
        if ( !$Comment = Doctrine::getTable('ProductsComments')->find($this->getParam('id_parent')) ) {
            return $this->forward('product-not-found', 'index', 'product');
        }

        $form = new Form_Product_Answer('ProductAnswer');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription('Ответ для <span style="font-size: 13px; color: GrayText; text-transform: none">'.$Comment->author_name.'</span>');
        $form->id_product->setValue($Comment->id_product);
        $form->id_parent->setValue($Comment->id);

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $ProductsComment = new ProductsComments;
                $ProductsComment->fromArray($form->getValues());
                $ProductsComment->save();
                $this->view->Product = $ProductsComment->Products;
                $this->render('post-add-answer');
                return;
            } else {
                $this->getResponse()->setRawHeader('HTTP/1.1 422 Bad Request');
            }
        }

        echo $form;
    }

    public function voteAction()
    {
        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();

            $Comment = Doctrine_Query::create()
                ->from('ProductsComments')
                ->where('id = ? AND id_parent IS NULL', $post['id'])
                ->fetchOne();

            if ( $Comment->exists() AND 'positive' == $post['type'] ) {
                $Comment->positive_vote_count += 1;
                $Comment->save();
                echo $Comment->positive_vote_count;
            }

            if ( $Comment->exists() AND 'negative' == $post['type'] ) {
                $Comment->negative_vote_count += 1;
                $Comment->save();
                echo $Comment->negative_vote_count;
            }


        }

    }

    public function getCommentsAction(){
        $this->_helper->viewRenderer->setNoRender(false);
        $page = $this->params['page'];
        $product_id = $this->params['id_product'];

        $this->view->comments = ProductsComments::getLastCheckedTree($product_id, 'vote', $page);

        if(!empty(ProductsComments::getLastCheckedTree($product_id, 'vote', $page+1))){
            $this->view->show_more = true;
        }else{
            $this->view->show_more = false;
        }
    }

}
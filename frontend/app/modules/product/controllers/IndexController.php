<?

class Product_IndexController extends Controller_Main
{
    public function oldAction()
    {
        /** @var $Product Products */
        if (!$Product = Doctrine::getTable('Products')->findOneByAlias($this->params['alias'])) {
            return $this->forward('product-not-found');
        }

        $this->redirect($this->view->productUrl($Product), ['code' => 301]);
    }

    public function indexAction()
    {
        /** @var $Product Products */
        if (!$Product = Doctrine::getTable('Products')->find($this->params['id'])) {
            return $this->forward('product-not-found');
        }

        if ($Product->alias !== $this->params['alias']) {
            $this->redirect($this->view->productUrl($Product), ['code' => 301]);
        }

        if ( $Product->hasSticker('markdown') || $Product->hasSticker('sale') ) {
            if ( empty($Product->markdown_percent) ) {
                $Product->markdown_percent = 25;
            }
            if ( $Product->hasSticker('markdown') && empty($Product->markdown_note) ) {
                $Product->markdown_note = 'Витринный образец';
            }
        }

        if ( $Product->isAvailableForOrder() ) {
            $this->view->ga['dimension1'] = $Product->id;
            $this->view->ga['dimension2'] = 'offerdetail';
            $this->view->ga['dimension3'] = $Product->getPrice('retail', 'UAH');
        }

        $PopularProducts = Products_Query::create()->setOnlyPublic()
            ->setParamsQuery([
                'id_catalog' => $Product->Catalogs->getFirst()->id,
                'status'    => [Products::STATUS_AVAILABLE, Products::STATUS_ORDER],
                'excludeid'  => $Product->id
            ])
            ->orderBy('CASE WHEN p.id < '.$Product->id.' THEN 1 ELSE 0 END, p.id ASC')
            ->limit(5)
            ->execute();

        $this->view->PopularProducts = $PopularProducts;
        $this->view->Accessories = Products_Query::create()
            ->setParamsQuery(['id_accessory_ref' => $Product->id_accessory])
            ->setOnlyPublic()
//            ->setOnlyAvailableForOrder()
            ->execute()
        ;

        $this->assignGroups($Product);

        $this->view->headTitle( $this->view->ProductMetaTitle($Product), 'SET');
        $this->view->headDescription( $this->view->ProductMetaDescription($Product));

        $this->view->headMeta()->appendProperty('og:title', $this->view->headTitle()->getContainer());
        $this->view->headMeta()->appendProperty('og:description', $this->view->headDescription()->getContainer());
        $this->view->headMeta()->appendProperty('og:image', $Product->getImage()->getFirstImage()['middle']['url']);
        $this->view->headMeta()->appendProperty('og:type', 'product');
        $this->view->headMeta()->appendProperty('og:url', $this->view->productUrl($Product));
        $this->view->headMeta()->appendProperty('og:site_name', SITE_NAME);

        $this->view->headLink(array('rel' => 'canonical', 'href' => $this->view->productUrl($Product)), 'PREPEND');

        $this->view->Product = $Product;
    }

    public function productNotFoundAction()
    {

        $log = new NotFoundPages;
        $log->uri = $_SERVER['REQUEST_URI'];
        $log->referer = $_SERVER['HTTP_REFERER'];

        try {
            $log->save();
        } catch (Exception $e) {

        }

        $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');


        $this->_helper->layout->setLayout('empty');

        $words = preg_replace('/-/', ' ', $this->params['alias']);

        $this->view->Products = Products_Query::create()
            ->setParamsQuery(['orkey' => $words])
            ->setOnlyPublic()
            ->setOnlyAvailableForOrder()
            ->limit(10)
            ->execute();

    }

    private function assignGroups($Product)
    {
        $this->view->Groups = $Product->getGroupInfo();

        $this->view->GroupStateProducts = [];
        $this->view->GroupSizeProducts = [];


        foreach ($this->view->Groups as $key => $item) {
            $group_type = reset($item)['group_type_code'];

            if ('state' == $group_type) {
                $products_group_title = array_column($item, 'title', 'product_id');
                $Products =  Products_Query::create()->setParamsQuery(['id' => array_column($item, 'product_id'), 'sort' => 'price'])->execute();
                foreach ($Products as $P) {
                    $this->view->GroupStateProducts []= [
                        'title'   => $products_group_title[$P->id],
                        'Product' => $P,
                    ];
                }
                unset($this->view->Groups[$key]);
                unset($Products);
            } elseif ('size' == $group_type) {
                $this->view->GroupSizeProducts = $item;
                unset($this->view->Groups[$key]);
            }


        }


    }

}
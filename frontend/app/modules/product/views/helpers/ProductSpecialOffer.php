<?

class Site_View_Helper_ProductSpecialOffer extends Zend_View_Helper_Abstract
{
    public function ProductSpecialOffer(Products $Product)
    {
        $str = '';

        if ( $offer = $Product->getActualSpecialOffer() ) {
            $str .= '<div class="special_offer_block">';
            $str .= $offer;
            $str .= '</div>';
        }

        return $str;
    }
}
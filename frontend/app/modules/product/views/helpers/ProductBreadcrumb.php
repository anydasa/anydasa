<?

require_once 'Zend/View/Helper/Abstract.php';

class Site_View_Helper_ProductBreadcrumb extends Zend_View_Helper_Abstract
{

    public function productBreadcrumb($Product)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        foreach ($Product->Catalogs as $Catalog) {
            if ( $Catalog->CatalogMenu->count() ) {
                $parents = $Catalog->CatalogMenu->getFirst()->getNode()->getAncestors();
                $parents->add($Product->Catalogs->getFirst()->CatalogMenu->getFirst());
                break;
            }
        }

        $path = '';
        $urlOptions = array();

        if ( $parents ) {
            foreach ($parents as $item) {
                $path .= '/'.$item->alias;
                if ( $item->level > 0 ) {
                    $urlOptions []= array(
                        'url'   => $view->menuLink($item),
                        'title' => $item->title,
                    );
                }
            }
            $brand['title'] = $Product->Brand->title;
            $brand['url'] = end($urlOptions)['url'] . $Product->Brand->alias . '/';
            $urlOptions []= $brand;
        }

        return $view->breadcrumb(
            '',
            $urlOptions
        );
    }
}
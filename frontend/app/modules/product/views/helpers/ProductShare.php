<?

class Site_View_Helper_ProductShare extends Zend_View_Helper_Abstract
{
    public function ProductShare(Products $Product)
    {
        $url = urlencode(Zend_Layout::getMvcInstance()->getView()->productUrl($Product));
        $title = urlencode($Product->getTypedName());

        $str = '<div class="product_share">
            <span class="product_share_title">Расскажи друзьям:</span>
             <ul>
                <li class="facebook"><a href="#" onclick="window.open(\'https://www.facebook.com/sharer.php?u='.$url.'&t='.$title.'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=300, top=200, width=550, height=440, toolbar=0, status=0\');return false" rel="nofollow"></a></li>
                <li class="googleplus"><a href="#" onclick="window.open(\'https://plus.google.com/share?url='.$url.'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=300, top=200, width=550, height=440, toolbar=0, status=0\');return false" rel="nofollow"></a></li>
                <li class="twitter"><a href="#" onclick="window.open(\'https://twitter.com/share?text='.$title.'&url='.$url.'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=300, top=200, width=550, height=440, toolbar=0, status=0\');return false" rel="nofollow"></a></li>
                <li class="odnoklassniki"><a href="#" onclick="window.open(\'https://www.odnoklassniki.ru/dk?st.cmd=addShare&st._surl='.$url.'&title='.$title.'\', \'_blank\', \'scrollbars=0, resizable=1, menubar=0, left=300, top=200, width=550, height=440, toolbar=0, status=0\');return false" rel="nofollow"></a></li>
            </ul>
        </div>';

        $str .=
            '<script type="application/javascript">
                $(".product_share a").on("click", function () {
                    ga(\'send\', \'event\', \'Нажатие на кнопку\', \'Расскажи друзьям\', \''.addslashes($Product->getFullName()).'\');
                });
            </script>';

        return $str;
    }


}
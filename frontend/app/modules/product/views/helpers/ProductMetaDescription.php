<?

use Twig\Environment;
use Twig\Loader\ArrayLoader;

class Site_View_Helper_ProductMetaDescription extends Zend_View_Helper_Abstract
{
    public function ProductMetaDescription(Products $Product)
    {
        $Twig = new Environment(new ArrayLoader([
            'description' => "{{ productName }}"
        ]));

        return $Twig->render('description', [
            'productName' => $Product->getTypedName()
        ]);
    }
}

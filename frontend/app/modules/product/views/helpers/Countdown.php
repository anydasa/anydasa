<?

class Site_View_Helper_Countdown extends Zend_View_Helper_Abstract
{

    public function Countdown(Promotions $Promotion)
    {
        $view   = Zend_Layout::getMvcInstance()->getView();
        $view->headScript()->prependFile('/js/jquery.countdown.js', 'text/javascript');

        $str = '<script type="application/javascript">
                    $(function(){
                        $("#countdown").countdown({
                          image: "/img/countdown.png",
                          format: "dd:hh:mm:ss",
                          digitImages: 6,
                          digitWidth: 35,
                          digitHeight: 45,
                          endTime: new Date('.(strtotime($Promotion->time_off)*1000).')
                        });
                      });
                </script>';
        $str .= '<div class="countdown"><strong>До окончания акции осталось</strong><div id="countdown"></div></div><div style="clear: both"></div>';
        return $str;
    }


}
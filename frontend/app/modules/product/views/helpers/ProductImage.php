<?

require_once 'Zend/View/Helper/Abstract.php';

class Site_View_Helper_ProductImage extends Zend_View_Helper_Abstract
{
    public function productImage(Products $product)
    {
        $imgList    = $product->getImage()->getImagesList();
        $firstImage = reset($imgList);

        $str = '<div class="images">
                <div class="big">
                    <a href="'. $firstImage['big']['url'] .'" rel="product" title="">
                        <img itemprop="image" src="'. $firstImage['big']['url'] .'" width="400" height="400" alt="'.htmlspecialchars($product->getTypedName(), ENT_COMPAT).'" />
                    </a>
                </div>';

        if ( count($imgList) > 1 ) {
            $str .= '<div class="thumbs"><ul class="owl-carousel carousel" id="carousel-product-images" data-count="'. count($imgList) .'">';
            foreach ($imgList as $img) {
                $str .= '<li>
                    <a href="'. $img['big']['url'] .'" rel="{gallery: \'product\' , smallimage: \''. $img['big']['url'] .'\',  largeimage: \''. $img['big']['url'] .'\'}" title="">
                        <img itemprop="image" src="'. $img['thumb']['url'] .'" alt="" >
                    </a>
                </li>';
            }
            $str .= '</ul></div>';
        }

        $str .= '</div>';

        return $str;
    }
}
<?

class Site_View_Helper_ProductRating extends Zend_View_Helper_Abstract
{
    public function ProductRating($Product)
    {
        $str = '';
        if ( $Product->getRatingValue() > 0 ) {
            $rating = round($Product->getRatingValue());

            $active  = @array_fill(0, $rating, '<span class="active"></span>');
            $passive = @array_fill(0, 5-$rating, '<span class="passive"></span>');

            if ( $Product->rating ) {
                $str .= '<span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                            <meta itemprop="ratingValue" content="'.$rating.'" />
                            <meta itemprop="bestRating" content="5" />
                            <meta itemprop="ratingCount" content="'.$Product->getRatingCount().'" />
                        </span>';
            }

            $str .= '<span class="rating">' . @implode('', $active) . @implode('', $passive). '</span>';
            $str .= '<span class="rating_count">голосов: '.$Product->getRatingCount().'</span>';
        }
        return $str;
    }
}
<?

class Site_View_Helper_ProductSet extends Zend_View_Helper_Abstract
{
    public function ProductSet(Products $Product)
    {
        $str = '';

        if ( $Product->id_set && $Product->isAvailableForOrder() ) {

            $ProductsSetsProduct = $Product->Set->getProductsInComplect();
            if ( !$ProductsSetsProduct->count() ) {
                return;
            }

            $view = Zend_Layout::getMvcInstance()->getView();

            $ProductsSetsProduct->loadRelated('Products');

            $str .= '<section class="set" id="complect"><h3 class="tab_title">Вместе дешевле</h3>';
            $str .= '<table><tr><td class="owner" data-id="'.$Product->id.'">';
            $str .=     '<div class="img"><img src="'.$Product->getImage()->getFirstImage()['middle']['url'].'"></div>';
            $str .=     '<div class="product_set-price">' . $Product->getFormatedPrice('retail', $_SESSION['currency']) . '</div>';
            $str .=     '<div class="product_set-title">' . $Product->title . '</div>';
            $str .= '</td>';
            $str .= '<td class="plus">+</td>';
            $str .= '<td><ul class="products_group">';


            $group_num = $ProductsSetsProduct->getFirst()->group_num;

            foreach ($ProductsSetsProduct as $ProductSet) {

                if ( $group_num != $ProductSet->group_num ) {
                    $group_num = $ProductSet->group_num;
                    $str .= '</ul></td><td class="plus">+</td><td><ul class="products_group">';
                }

                $str .= '<li data-discount="'.htmlspecialchars(Zend_Json::encode($ProductSet->discount)).'">';
                $str .=     '<div class="img"><img src="'.$ProductSet->Products->getImage()->getFirstImage()['middle']['url'].'"></div>';
                $str .=     '<div class="product_set-price"><label>';
                $str .=         '<input type="checkbox" checked data-id="'.$ProductSet->Products->id.'"> ';
                $str .=         '<span class="new_price"></span> ';
                $str .=         '<span class="old_price">' . $ProductSet->Products->getFormatedPrice('retail', $_SESSION['currency']) . '</span>';
                $str .=     '</label></div>';
                $str .=     '<div class="product_set-title"><a href="'.$view->ProductUrl($ProductSet->Products).'">' . $ProductSet->Products->getFullName() . '</a></div>';
                $str .= '</li>';
            }

            $str .= '</ul></td></tr><tr>';
            $str .= '<td class="equal">=</td>';
            $str .= '<td colspan="10" class="setButtonRow">';
            $str .=     '<span
                            class="btn btn-important btn-large nowrap btn2cartSet"
                            onclick="ga(\'send\', \'event\', \'Нажатие на кнопку\', \'Купить комплект\', \''.addcslashes($Product->getFullName(), '"\'').'\');"
                         >Купить комплект</span>';

            $str .=     '<div class="old-price">'. $ProductSet->Products->getFormatedPrice('retail', $_SESSION['currency']) .'</div>';
            $str .=     '<div class="new-price">'. $ProductSet->Products->getFormatedPrice('retail', $_SESSION['currency']) .'</div>';
            $str .=     '<div class="diff">экономия <span></span></div>';
            $str .= '</td>';

            $str .= '</tr></table>';
            $str .= '<script> initProductSet("#complect"); </script>';
            $str .= '</section>';
        }

        return $str;
    }


}
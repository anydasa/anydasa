<?

use Twig\Environment;
use Twig\Loader\ArrayLoader;

class Site_View_Helper_ProductMetaTitle extends Zend_View_Helper_Abstract
{
    public function ProductMetaTitle(Products $Product)
    {
        $Twig = new Environment(new ArrayLoader([
            'title' => "{{ productName }} в Украине. Купить электронику в интернет-магазине в Киеве по низким ценам."
        ]));

        return $Twig->render('title', [
            'productName' => $Product->getTypedName()
        ]);
    }
}

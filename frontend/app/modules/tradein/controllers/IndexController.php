<?

class Tradein_IndexController extends Controller_Main
{


    public function indexAction()
    {
        $form = new Form_Tradein('Tradein');

        if ($this->_request->isPost()) {
            $postData = $this->_request->getPost();
            if ($form->isValid($postData)) {
                $Record = new Tradein;
                $Record->fromArray($form->getValues());
                $Record->save();

                $this->sendToPartner($Record);

                $this->_helper->flashMessenger->addMessage("asd");

                $this->redirect($this->getRequest()->getRequestUri());
            }
        }

        if ($this->_helper->FlashMessenger->hasMessages()) {
            $this->view->success = true;
        }

        $this->view->form = $form;

    }

    private function sendToPartner(Tradein $Tradein)
    {
        $this->view->Record = $Tradein;

        $htmlBody = $this->view->render('index/mail-tpl-to-partner.phtml');

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($htmlBody);
        $mail->setFrom('info@anydasa.com');
        $mail->addTo('info@anydasa.com');
        $mail->setSubject("Новая заявка TradeIn от Anydasa");

        $mail->send();

        if ( !$MailTemplate = Doctrine::getTable('MailTemplates')->findOneByCode('tradein_claim') ) {
            throw new Exception('Отсутсвует tradein_claim почтовый шаблон (MailTemplates)');
        }

        $data = [
            'user_name' =>  $Tradein->user_name,
            'id' => $Tradein->id,
            'product_brand' => $Tradein->product_brand,
            'product_model' => $Tradein->product_model,
            'product_can_enable' => $Tradein->product_can_enable,
            'product_working_state' => $Tradein->product_working_state,
            'product_buttons_available' => $Tradein->product_buttons_available,
            'product_has_body_damage' => $Tradein->product_has_body_damage,
            'product_has_screen_damage' => $Tradein->product_has_screen_damage,
            'want_buy' => $Tradein->want_buy,
        ];

        $mail = new Site_Mail('UTF-8');
        $mail->setBodyHtml($MailTemplate->renderHTML($data));
        $mail->setFrom($MailTemplate->MailSmtp->username, $MailTemplate->from_name);
        $mail->addTo($Tradein->user_email, $Tradein->user_name);
        $mail->setSubject($MailTemplate->subject);
        $mail->send();

    }
}
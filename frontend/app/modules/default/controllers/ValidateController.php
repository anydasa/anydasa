<?

class ValidateController extends Controller_Account
{

    function preDispatch()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();
    }

    public function formElementAction()
    {
        $query = $this->_request->getQuery();
        $post  = $this->_request->getPost();

        $form = new $query['form']($query['model']);
        if ( $form->getElement($query['field'])->isValid($post[$query['field']], $post) ) {
            echo Zend_Json::encode(true);
        } else {
            echo Zend_Json::encode($form->getMessages()[$query['field']]);
        }
    }

}

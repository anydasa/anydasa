<?

class ErrorController extends Controller_Main
{

    public function pageNotFoundAction()
    {
        $this->_helper->layout->disableLayout();

        $this->getResponse()->setRawHeader('HTTP/1.1 404 Not Found');

        $this->view->headTitle('Странца не найдена', 'PREPEND');
        $this->view->headDescription('Странца не найдена', 'PREPEND');


        $log = new NotFoundPages;
        $log->uri = $_SERVER['REQUEST_URI'];
        $log->referer = $_SERVER['HTTP_REFERER'];

        try {
            $log->save();
        } catch (Exception $e) {

        }


        $this->render('page-not-found');
    }

    public function accessDeniedAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setRawHeader('HTTP/1.1 401 Access denied');
        $this->render('access-denied');
    }

    public function error500Action()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setRawHeader('HTTP/1.1 500 Internal Server Error');
        $this->render('error500');
    }

    public function unauthorizedAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->getResponse()->setRawHeader('HTTP/1.1 401 Unauthorized');
    }

}


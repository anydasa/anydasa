<?

class IndexController extends Controller_Main
{
    public function indexAction()
    {
        $this->view->headTitle('anydasa.com - каталог товаров в интернет-магазинах', 'SET');

        $this->populateNewProducts();
        $this->populateBestsellers();
        $this->populateSale();

        $this->view->ga['dimension1'] = implode(', ', $this->view->Bestsellers->toKeyValueArray('id', 'id'));
        $this->view->ga['dimension2'] = 'home';


        $this->view->Banners = Banners::getBanners('main', $_SERVER['REQUEST_URI']);
    }

    private function populateNewProducts($count = 15)
    {
        $NewProducts = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery([
                'filter'=>['stickers'=>'new'],
                'is_empty_price' => 0
            ])
            ->orderBy('RANDOM()')
            ->limit($count)
            ->execute();

        $this->view->NewProducts =  $NewProducts;
    }

    private function populateBestsellers($count = 15)
    {
        $Bestsellers = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery([
                'filter'=>['stickers'=>'bestseller'],
            ])
            ->orderBy('RANDOM()')
            ->limit($count)
            ->execute();

        $this->view->Bestsellers =  $Bestsellers;
    }

    private function populateSale($count = 15)
    {
        $Sale = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery([
                'filter'=>['stickers'=>'sale'],
            ])
            ->orderBy('RANDOM()')
            ->limit($count)
            ->execute();

        $this->view->Sale = $Sale;
    }

    public function recordExistAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();

        try {
            $isset = (bool)Doctrine_Query::create()
                ->from($this->params['table'])
                ->where($this->params['field'] . ' ilike ?', $this->params[$this->params['field']])
                ->fetchOne();
            if (isset($this->params['not']))
                $isset = !$isset;
            echo Zend_Json::encode($isset);
        } catch (Exception $e) {
            //echo Zend_Json::encode($e->getMessage());
        }
    }

    public function setCurrencyAction()
    {
        $_SESSION['currency'] = $this->params['currency'];
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function setDisplayTypeListAction()
    {
        $_SESSION['display_type_list'] = $this->params['type'];
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
}
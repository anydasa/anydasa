<?

class Feedback_IndexController extends Controller_Main
{

    public function indexAction()
    {
        if ( ! $this->_request->isXmlHttpRequest() ) {
            throw new Site_Exception_AccessDenied;
        }

        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();


        $form = new Form_Feedback('Feedback');
        $form->setAction($_SERVER['REQUEST_URI']);
        $form->setDescription('Обратная связь');

        if ( $this->_request->isPost() ) {
            if ($form->isValid($this->_request->getPost())) {
                $Feedback = new Feedback;
                $Feedback->fromArray($form->getValues());
                $Feedback->save();
                $this->render('success');

                return;
            }
        }

        echo $form;
    }
}
<?

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Payment_ChastController extends Controller_Main
{
    const API_PASS = 'bfea12d982d4463e809a4f05a93eee65';
    const API_STORE_ID = '8A67F7798A494135B9E1';

    public function preDispatch()
    {
        $this->_helper->layout->setLayout('chast');
    }

    public function callbackAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_helper->layout->disableLayout();


        if ( $this->_request->isPost() ) {

            $result = json_decode(file_get_contents('php://input'), true);

            if ( $this->makeSignatureCallBack($result) == $result['signature'] ) {
                $orderId = preg_replace('/-.*$/', '', $result['orderId']);

                if ( $Order = Doctrine::getTable('Orders')->find($orderId) ) {
                    $msg  = "<div><strong>Ответ с приватбанка (". $Order->getSum() ."грн):</strong></div>";

                    $msg .= "<div>".($result['paymentState'] == 'SUCCESS' ? 'Успешно' : 'Ошибка')."</div>";
                    $msg .= "<div>".$result['message']."</div>";

                    $History = new OrdersHistory;
                    $History->id_order = $orderId;
                    $History->comment = $msg;

                    $History->save();

                    if ( $result['paymentState'] == 'SUCCESS' ) {
                        $Order->token_chast = null;
                        $Order->save();

                        $this->sendMailForUserOnSuccess($Order, $result['message']);
                    }
                }

            }

        }
    }

    private function sendMailForUserOnSuccess(Orders $Order, $msg)
    {
        if ( !empty($Order->email) ) {
            $data = [];
            $data['order'] = $Order->toArray();
            $data['msg'] = $msg;

            $twig = new Environment(new FilesystemLoader(
                Zend_Registry::getInstance()->config->path->root . 'templates/twig/'
            ));

            $body = $twig->render('mail/payment/user/chast/success.twig', $data);

            $mail = new Site_Mail('UTF-8');
            $mail->setBodyHtml($body);
            $mail->setFrom('info@anydasa.com');
            $mail->setSubject('Кредит оформлен');
            $mail->addTo($Order->email, $Order->name);
            $mail->addCc('info@anydasa.com');
            $mail->send();
        }

    }

    public function orderNotFoundAction() {}
    public function unknownErrorAction() {}

    public function indexAction()
    {
        $Order = Doctrine::getTable('Orders')->findOneBy('token_chast', $this->_request->getParam('token'));

        if ( !$Order ) {
            $this->forward('order-not-found');
        }

        $this->view->Order = $Order;

        if ( $this->_request->isPost() ) {
            $post = $this->_request->getPost();

            $data = [
                'storeId' => self::API_STORE_ID,
                "orderId"=> $Order->id .'-'. base64_encode(rand()),
                "amount"=> number_format($Order->getSum(), 2, '.', ''),
                "currency"=> "980",
                "partsCount"=> $post['partsCount'],
                "merchantType"=> "II",
                "products"=> [],
            ];

            foreach ($Order->Products as $item) {
                $data['products'] []= [
                    "name" => $item->title,
                    "count"=> $item->count,
                    "price"=> number_format($item->price, 2, '.', '')
                ];
            }

            $data['signature'] = $this->makeSignatureCreate($data);

            $options = array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Accept: application/json\r\n" .
                        "Accept-Encoding: UTF-8\r\n" .
                        "Content-Type: application/json; charset=UTF-8",
                    'content' => json_encode($data)
                )
            );
            $context = stream_context_create($options);


            $result = file_get_contents('https://payparts2.privatbank.ua/ipp/v2/payment/create', false, $context);

            $result = json_decode($result, true);

            if ( 'SUCCESS' == $result['state'] ) {
                $this->redirect('https://payparts2.privatbank.ua/ipp/v2/payment?token='. $result['token']);
                exit;
            } else {
                $this->forward('unknown-error');
            }
        }
    }


    private function makeSignatureCallBack($data)
    {
        $signature = self::API_PASS;

        $signature .= $data['storeId'];
        $signature .= $data['orderId'];
        $signature .= $data['paymentState'];
        $signature .= $data['message'];

        $signature .= self::API_PASS;

        return base64_encode( hex2bin(sha1($signature)) );
    }

    private function makeSignatureCreate($data)
    {
        $signature = self::API_PASS;

        $signature .= $data['storeId'];
        $signature .= $data['orderId'];
        $signature .= str_replace('.', '', $data['amount']);
        $signature .= $data['currency'];
        $signature .= $data['partsCount'];
        $signature .= $data['merchantType'];
        $signature .= $data['responseUrl'];
        $signature .= $data['redirectUrl'];

        foreach ($data['products'] as $product) {
            $signature .= $product['name'];
            $signature .= $product['count'];
            $signature .= str_replace('.', '', $product['price']);
        }

        $signature .= self::API_PASS;

        return base64_encode( hex2bin(sha1($signature)) );


    }
}
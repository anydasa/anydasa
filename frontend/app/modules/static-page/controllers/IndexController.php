<?

class StaticPage_IndexController extends Controller_Main
{

    public function indexAction()
    {
        if ( !$node = Doctrine::getTable('StaticPages')->findOneByAlias($this->params['alias']) ) {
            throw new Site_Exception_NotFound;
        }

        $this->view->node = $node;

        $this->view->headTitle( $node->title.'.' );
        $this->view->headDescription( $node->title.'.' );

    }
}
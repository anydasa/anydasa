<?

class Account_UnsubscribeController extends Controller_Main
{

    public function priceAction()
    {
        if ( !$User = Users::getByToken($this->params['token']) ) {
            $this->render('user-not-found');
        }

        if ($this->_request->isPost()) {
            $User->is_recive_price = 0;
            $User->save();
        }

        $this->view->subscribeUser = $User;
    }
}
<?

use Twig\Environment;
use Twig\Loader\ArrayLoader;

class Shop_IndexController extends Controller_Main
{
    public function oldAction()
    {
        /** @var $catalogMenu CatalogMenu */
        if (!$catalogMenu = Doctrine::getTable('CatalogMenu')->findOneByAlias($this->params['menu_alias'])) {
            throw new Site_Exception_NotFound;
        }

        if (empty($this->params['filter_params'])) {
            $redirectUrl = $this->view->menuLink($catalogMenu);
        } else {
            $redirectUrl = $this->view->url([
                'menu_alias'    => $catalogMenu->alias,
                'menu_id'    => $catalogMenu->id,
                'filter_params' => $this->params['filter_params']
            ], 'shop-filter-params', true);
        }

        $this->redirect($redirectUrl, ['code' => 301]);
    }

    public function indexAction()
    {
        if (false === ($Menu = Doctrine::getTable('CatalogMenu')->find($this->params['menu_id']))) {
            throw new Site_Exception_NotFound;
        }

        if ($Menu->alias !== $this->params['menu_alias']) {
            $this->redirect($this->view->menuLink($Menu), ['code' => 301]);
        }

        $Filter = new Products_Filter($Menu->Catalog);

        //----Перанаправить с набора фильтров на алиас индеска, если есть
        $index_alias = $Filter->getIndexAliasByPath($this->params['filter_params']);
        if (false !== $index_alias && $index_alias != $this->params['filter_params']) {
            $routeParams = [
                'menu_alias'    => $this->params['menu_alias'],
                'menu_id'    => $this->params['menu_id'],
                'filter_params' => $index_alias
            ];

            $this->redirect($this->view->url($routeParams, 'shop-filter-params', true), ['code' => 301]);
        }

        //------------------------------------------------------------------------

        if (false !== ($path = $Filter->getIndexPathByAlias($this->getParam('filter_params')))) {
            $this->setParam('filter_params', $path);
        }

        $Filter->markSelected($this->getParam('filter_params'));
        $Filter->populateCounters($this->getAllParams());

        $this->_checkFilters($Filter);

        $query = Products_Query::create()
            ->setOnlyPublic()
            ->setParamsQuery($this->getAllParams())
            ->addOrderBy('CASE WHEN p.status IN(1,2,3) THEN 1 ELSE 2 END'); //at first available, at second the rest

        if (!empty($_SESSION['products_first'])) {
            $query->first($_SESSION['products_first']);
        }

        if (!empty($_SESSION['products_sort'])) {
            $query->setParamsQuery(['sort' => $_SESSION['products_sort']]);
        } else {
            $query->addOrderBy('CASE WHEN status IN(1,2,3) THEN 1+p.sort ELSE 0 END DESC');
        }

        if (!empty($Menu->id_catalog) && $Menu->Catalog->is_group_by_series) {
            $query->groupBySeries();
        }

        $pager = new Doctrine_Pager($query, $this->getParam('page'), 30);

        $this->view->ProductList = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));
        $this->view->menu = $Menu;
        $this->view->Filter = $Filter;

        $this->_meta($Menu, $Filter);

        $this->view->headLink()->prependStylesheet('/css/pages/shop.css?' . ASSETS_ID);
        $this->view->headScript()->prependFile('/js/common/shop.js', 'text/javascript');
        $this->view->layout()->breadcrumb = $this->view->MenuBreadcrumb($Menu);

        if ($this->_request->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
        }
    }

    private function _checkFilters(Products_Filter $Filter)
    {
        $FilterData = $Filter->getAllData();

        $request_params = $this->getRequest()->getParams();

        if (empty($request_params['filter_params'])) {
            return;
        }

        $requestFilterParams = Products_Filter_Url::parsePath($request_params['filter_params']);
        $requestFilterParams['brands'] = array_intersect($requestFilterParams['brands'], array_column($FilterData['brands']['Values'], 'alias'));
        $requestFilterParams['stickers'] = array_intersect($requestFilterParams['stickers'], array_column($FilterData['stickers']['Values'], 'alias'));

        if (!empty($requestFilterParams['filters'])) {
            $filters = array_column($FilterData['filters']['Values'], 'alias', 'id');
            $missing = array_diff($requestFilterParams['filters'], $filters);

            if (!empty($missing)) { // проверяем и заменяем на старые алиасы
                $old_aliases = array_filter(
                    array_column($FilterData['filters']['Values'], 'alias_old', 'alias'),
                    function ($item) {
                        return !is_null($item);
                    }
                );

                $replaceable = array_flip(array_intersect($old_aliases, $missing));

                $temp = array_combine(
                    $requestFilterParams['filters'],
                    $requestFilterParams['filters']
                );

                $requestFilterParams['filters'] = array_values(array_replace($temp, $replaceable));
            }

            $requestFilterParams['filters'] = array_intersect($requestFilterParams['filters'], $filters);
        }

        if ($request_params['filter_params'] != Products_Filter_Url::buildPath($requestFilterParams)) {
            $routeParams = [
                'menu_alias'    => $request_params['menu_alias'],
                'menu_id'    => $request_params['menu_id'],
                'filter_params' => Products_Filter_Url::buildPath($requestFilterParams)
            ];
            $routeName = !empty($routeParams['filter_params']) ? 'shop-filter-params' : 'shop';
            $this->redirect($this->view->url($routeParams, $routeName, true), ['code' => 301]);
        }
    }


    private function _meta(CatalogMenu $Menu, Products_Filter $Filter)
    {
        $this->_MetaTitle($Menu, $Filter);
        $this->_MetaDescription($Menu, $Filter);
        $this->_MetaKeywords($Filter);
        $this->_H1($Menu, $Filter);
        $this->_Text($Menu, $Filter);
        $this->_MetaRobots($Menu, $Filter);
        $this->_MetaCanonical($Menu, $Filter);
    }


    private function _MetaRobots($Menu, $Filter)
    {
        $FilterData = $Filter->getAllData();
        $Index = $Filter->getData('indexes')['selected'];

        $params = $this->getAllParams();
        unset($params['menu_alias']);
        unset($params['menu_id']);
        unset($params['module']);
        unset($params['controller']);
        unset($params['action']);
        unset($params['page']);

        if (empty($Menu->id_catalog)) {
            //НУЖЕН NOINDEX
        } elseif (0 == count(array_filter($params))) {
            return;
        } elseif (1 == count(array_filter($params)) && 'filter_params' == key($params)) {

            if (!empty($Index)) {
                return;
            } elseif (
                1 == count($FilterData['brands']['selected']) &&
                1 >= count($FilterData['filters']['selected']) &&
                0 == count($FilterData['stickers']['selected'])
            ) {
                return;
            } elseif (
                0 == count($FilterData['brands']['selected']) &&
                1 == count($FilterData['filters']['selected']) &&
                0 == count($FilterData['stickers']['selected'])
            ) {
                return;
            } elseif (
                0 == count($FilterData['brands']['selected']) &&
                2 == count($FilterData['filters']['selected']) &&
                2 == count(array_filter(array_column($FilterData['filters']['Groups'], 'selected'))) && // кол-во выбранных групп
                0 == count($FilterData['stickers']['selected'])
            ) {
                return;
            }
        }

        $this->view->headMeta()->appendName('robots', 'noindex,nofollow');
    }

    private function _MetaCanonical($Menu, $Filter)
    {
        $robots = $this->view->headMeta()->getContentByTypeName('name', 'robots');

        if (preg_match('/noindex/is', $robots)) {
            $canonical = $this->view->menuLink($Menu);
            $FilterData = $Filter->getAllData();
            if (1 == count($FilterData['brands']['selected'])) {
                $routeParams = [
                    'menu_alias'    => $this->getRequest()->getParam('menu_alias'),
                    'menu_id'    => $this->getRequest()->getParam('menu_id'),
                    'filter_params' => Products_Filter_Url::buildPath(['brands' => $FilterData['brands']['selected']])
                ];
                $canonical = $this->view->url($routeParams, 'shop-filter-params', true);
            }
            $this->view->headLink(array('rel' => 'canonical', 'href' => $canonical), 'PREPEND');
        }
    }

    private function _H1($Menu, $Filter)
    {
        $FilterData = $Filter->getAllData();
        $Index = $Filter->getData('indexes')['selected'];

        if (!empty($Index['h1'])) {
            $h1 = $Index['h1'];
        } elseif (
            1 == count($FilterData['brands']['selected']) &&
            0 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $h1 = $Menu->title . ' ' . $FilterData['brands']['Values'][key($FilterData['brands']['selected'])]['title'];
        } elseif (
            0 == count($FilterData['brands']['selected']) &&
            1 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];
            $h1 = $Menu->title . ' — ' . $group['title'] . ' ' . $value['title'];
        } elseif (
            1 == count($FilterData['brands']['selected']) &&
            1 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];
            $brand = $FilterData['brands']['Values'][key($FilterData['brands']['selected'])];
            $menu_title = $Menu->title;

            $h1 = $menu_title . ' ' . $brand['title'] . ' — ' . $group['title'] . ' ' . $value['title'];

        } elseif (
            0 == count($FilterData['brands']['selected']) &&
            2 == count($FilterData['filters']['selected']) &&
            2 == count(array_filter(array_column($FilterData['filters']['Groups'], 'selected'))) && // кол-во выбранных групп
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];

            next($FilterData['filters']['selected']);

            $value2 = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group2 = $FilterData['filters']['Groups'][$value2['id_group']];

            $h1 = $Menu->title . ' — ' . $group['title'] . ' ' . $value['title'] . ' — ' . $group2['title'] . ' ' . $value2['title'];
        } else {
            $h1 = $Menu->title;
        }

        $this->view->h1 = $h1;
    }

    private function _Text($Menu, $Filter)
    {
        $Index = $Filter->getData('indexes')['selected'];

        $text = '';

        $params = $this->getRequest()->getParams();
        unset($params['menu_alias']);
        unset($params['menu_id']);
        unset($params['module']);
        unset($params['controller']);
        unset($params['action']);

        if (!empty(trim(strip_tags($Index['text']))) and empty($params['page'])) {
            $text = $Index['text'];
        } elseif (
            0 == count(array_filter($params)) and
            !empty($Menu->id_catalog) and
            !empty(trim(strip_tags($Menu->Catalog->text)))
        ) {
            $text = $Menu->Catalog->text;
        }

        $this->view->text = $text;
    }

    private function _MetaTitle($Menu, Products_Filter $Filter)
    {
        $FilterData = $Filter->getAllData();
        $Index = $Filter->getData('indexes')['selected'];

        $Twig = new Environment(new ArrayLoader([
            'title' => "{{ text }} в Украине{% if page > 1 %}, стр №{{page}}{% endif %}. Купить {{ text2|lower }} в интернет-магазине в Украине с доставкой по всему миру."
        ]));

        $page = $this->getRequest()->getParam('page') ?: 1;

        if (!empty($Index['meta_title'])) {
            $title = $Index['meta_title'];
        } elseif (
            1 == count($FilterData['brands']['selected']) &&
            0 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $text = ($Menu->title . ' ' . $FilterData['brands']['Values'][key($FilterData['brands']['selected'])]['title']);
            $text2 = ($Menu->title . ' ' . $FilterData['brands']['Values'][key($FilterData['brands']['selected'])]['title']);
            $title = $Twig->render('title', ['text' => $text, 'text2' => $text2, 'page' => $page]);
        } elseif (
            0 == count($FilterData['brands']['selected']) &&
            1 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];

            $text = $Menu->title . ' — ' . $group['title'] . ' ' . $value['title'];
            $text2 = $Menu->title;
            $title = $Twig->render('title', ['text' => $text, 'text2' => $text2, 'page' => $page]);
        } elseif (
            1 == count($FilterData['brands']['selected']) &&
            1 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];
            $brand = $FilterData['brands']['Values'][key($FilterData['brands']['selected'])];
            $menu_title = $Menu->title;

            $text = $menu_title . ' ' . $brand['title'] . ' — ' . $group['title'] . ' ' . $value['title'];
            $text2 = $menu_title . ' ' . $brand['title'];

            $title = $Twig->render('title', ['text' => $text, 'text2' => $text2, 'page' => $page]);
        } elseif (
            0 == count($FilterData['brands']['selected']) &&
            2 == count($FilterData['filters']['selected']) &&
            2 == count(array_filter(array_column($FilterData['filters']['Groups'], 'selected'))) && // кол-во выбранных групп
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];

            next($FilterData['filters']['selected']);

            $value2 = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group2 = $FilterData['filters']['Groups'][$value2['id_group']];

            $text = $Menu->title . ' — ' . $group['title'] . ' ' . $value['title'] . ' — ' . $group2['title'] . ' ' . $value2['title'];
            $text2 = $Menu->title;
            $title = $Twig->render('title', ['text' => $text, 'text2' => $text2, 'page' => $page]);
        } elseif (!empty($Menu->meta_title)) {
            $title = $Menu->meta_title;
        } else {
            $title = $Twig->render('title', ['text' => $Menu->title, 'text2' => $Menu->title, 'page' => $page]);
        }

        $this->view->headTitle($title, 'SET');
    }

    private function _MetaDescription($Menu, $Filter)
    {
        $FilterData = $Filter->getAllData();
        $Index = $Filter->getData('indexes')['selected'];

        $Twig = new Environment(new ArrayLoader([
            'description' => "{{ text }}{% if page > 1 %}, стр №{{page}}{% endif %}."
        ]));

        $page = $this->getRequest()->getParam('page') ?: 1;

        if (!empty($Index['meta_description'])) {
            $description = $Index['meta_description'];
            $this->view->headDescription($description);
        } elseif (
            1 == count($FilterData['brands']['selected']) &&
            0 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $text = $Menu->title . ' ' . $FilterData['brands']['Values'][key($FilterData['brands']['selected'])]['title'];
            $description = $Twig->render('description', ['text' => $text, 'page' => $page]);
            $this->view->headDescription($description);
        } elseif (
            0 == count($FilterData['brands']['selected']) &&
            1 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];
            $text = $Menu->title . ' — ' . $group['title'] . ' ' . $value['title'];
            $description = $Twig->render('description', ['text' => $text, 'page' => $page]);
            $this->view->headDescription($description);
        } elseif (
            1 == count($FilterData['brands']['selected']) &&
            1 == count($FilterData['filters']['selected']) &&
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];
            $brand = $FilterData['brands']['Values'][key($FilterData['brands']['selected'])];

            $text = $Menu->title . ' ' . $brand['title'] . ' — ' . $group['title'] . ' ' . $value['title'];
            $description = $Twig->render('description', ['text' => $text, 'page' => $page]);
            $this->view->headDescription($description);
        } elseif (
            0 == count($FilterData['brands']['selected']) &&
            2 == count($FilterData['filters']['selected']) &&
            2 == count(array_filter(array_column($FilterData['filters']['Groups'], 'selected'))) && // кол-во выбранных групп
            0 == count($FilterData['stickers']['selected'])
        ) {
            $value = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group = $FilterData['filters']['Groups'][$value['id_group']];

            next($FilterData['filters']['selected']);

            $value2 = $FilterData['filters']['Values'][key($FilterData['filters']['selected'])];
            $group2 = $FilterData['filters']['Groups'][$value2['id_group']];

            $text = $Menu->title . ' — ' . $group['title'] . ' ' . $value['title'] . ' — ' . $group2['title'] . ' ' . $value2['title'];

            $description = $Twig->render('description', ['text' => $text, 'page' => $page]);
            $this->view->headDescription($description);
        } else {
            $description = empty($Menu->meta_description) ? $Twig->render('description', ['text' => $Menu->title, 'page' => $page]) : $Menu->meta_description;
            $this->view->headDescription($description);
        }
    }

    private function _MetaKeywords($Filter)
    {
        $Index = $Filter->getData('indexes')['selected'];

        if (!empty($Index['meta_keywords'])) {
            $this->view->headMeta()->appendName('keywords', $Index['meta_keywords']);
        }
    }
}
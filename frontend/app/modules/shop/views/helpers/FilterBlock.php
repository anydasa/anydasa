<?

class Site_View_Helper_FilterBlock extends Zend_View_Helper_Abstract
{
    private $Filter;
    private $requestParams;
    private $filterParams;

    public function FilterBlock($Menu, Products_Filter $Filter)
    {
        $this->Filter = $Filter;

        $this->requestParams = Zend_Controller_Front::getInstance()->getRequest()->getParams();
        $this->filterParams  = Products_Filter_Url::parsePath($this->requestParams['filter_params']);

        $str = '';

        $str .= $this->getSelected();

        $str .= '<div class="filter_block">';
//        $str .= $this->getPrices();
        $str .= $this->getSubmenu($Menu);
        $str .= $this->getStickers();
        $str .= $this->getBrands();
        $str .= $this->getFilter();
        $str .= $this->getIndexes();
        $str .= '</div>';



        return $str;
    }

    private function getIndexes()
    {
        $Data = $this->Filter->getData('indexes');

        $str = '';

        foreach ($Data['Values'] as $Value) {
            if ( $Value['weight'] > 0 ) {
                $routeParams = [
                    'menu_alias'    => $this->requestParams['menu_alias'],
                    'menu_id'    => $this->requestParams['menu_id'],
                    'filter_params' => $Value['alias']
                ];
                $str .= '<a style="font-size: '.$Value['weight'].'em" href="'.Zend_Controller_Front::getInstance()->getRouter()->assemble($routeParams, 'shop-filter-params', true).'">'.$Value['tag_name'].'</a> ';
            }
        }

        if ( !empty($str) ) {
            $str = '<div class="filter_block_item tags">'.$str.'</div>';
        }

        return $str;
    }

    private function getPrices()
    {
        $Data = $this->Filter->getData('prices');

        $str = '<div class="filter_block_item price">';
            $str .= '<a class="search" href="#"></a>';
            $str .= '<input type="text" name="min" id="minPrice">';
            $str .= ' <span>—</span> ';
            $str .= '<input type="text" name="max" id="maxPrice">';
            $str .= ' <span>грн.</span> ';
            $str .= '<div id="price-range"></div>';

            $str .= '<script>
                        $(function() {
                            $( "#price-range" ).slider({
                                range: true,
                                min: '.$Data['Values']['min'].',
                                max: '.$Data['Values']['max'].',
                                values: [ '.$Data['selected']['min'].', '.$Data['selected']['max'].' ]
                            });
                        });
              </script>';

        $str .= '</div>';

        return $str;
    }

    private function getSelected()
    {
        $str = '';

        $Data = $this->Filter->getAllData();
        foreach ($Data['brands']['selected'] as $id => $alias) {
            $str .= '<a href="'. $this->buidHref('brands', $alias) .'">'.$Data['brands']['Values'][$id]['title'].'<i></i></a>';
        }
        foreach ($Data['stickers']['selected'] as $id => $alias) {
            $str .= '<a href="'. $this->buidHref('stickers', $alias) .'">'.$Data['stickers']['Values'][$id]['title'].'<i></i></a>';
        }
        foreach ($Data['filters']['selected'] as $id => $alias) {
            $str .= '<a href="'. $this->buidHref('filters', $alias) .'">'.$Data['filters']['Values'][$id]['title'].'<i></i></a>';
        }

        if ( !empty($str) ) {
            $resetUrl = Zend_Controller_Front::getInstance()->getRouter()->assemble($this->requestParams, 'shop', true);
            $str = '<div class="filter_block_accepted"><div class="accepted_container">' .
                        '<a class="remove_all" href="'. $resetUrl .'">сбросить<i></i></a>' .
                        $str .
                    '</div></div>';
        }

        return $str;
    }

    private function getSubmenu($Menu)
    {
        $str = '';
        $view = Zend_Layout::getMvcInstance()->getView();

         if ( $Menu->getNode()->hasChildren() ) {
             $str .= '<div class="filter_block_item submenu open cat">';
//             $str .= '<div class="submenu_title">'.$Menu->title.'</div>';
             $str .= '<div class="submenu_children">';
             foreach($Menu->getNode()->getChildren() as $MenuItem) {
                 if ( $MenuItem->count_products > 0 ) {
                     $str .= '<div class="cat_item"><a href="'. $view->menuLink($MenuItem) .'">';
//                     $str .=    '<img src="'. $MenuItem->getImageUrl() .'" />';
                     $str .=    '<h3>'.$MenuItem->title.'</h3>';
//                     $str .=    '<span class="count">'. $MenuItem->count_products .'</span>';
                     $str .= '</a></div>';
                 }
             }
             $str .= '</div>';
             $str .= '</div>';
         }

        return $str;
    }

    private function getBrands()
    {
        $Brands = $this->Filter->getData('brands');

        $str = '';

        if ( !empty($Brands) && $Brands['count_products'] ) {
            $str .= '<div class="filter_block_item '. (count($Brands['selected']) > 0 ? 'open' : 'open') .'">';
            $str .= '<div class="submenu_title">Бренды</div>';
            $str .= '<div class="submenu_children unic '. (count($Brands['selected']) > 0 ? 'selected' : '') .'">';
            foreach ($Brands['Values'] as $Value) {
                if ( $Value['count_products'] > 0 || $Value['is_selected'] ) {
                    $str .= '<div data-selected="'. intval($Value['is_selected']) .'" data-alias="'. $Value['alias'] .'">';
                    $str .= '<input type="checkbox" value="'. $Value['alias'] .'" '. (intval($Value['is_selected']) ? 'checked' : '') .'>';
                    $str .= '<a href="'. $this->buidHref('brands', $Value['alias']) .'">'.$Value['title'].'</a>';
                    $str .= '<span class="count">'. $Value['count_products'] .'</span>';
                    $str .= '</div>';
                }
            }
            $str .= '</div>';
            $str .= '</div>';
        }

        return $str;
    }

    private function getStickers()
    {
        $Stickers = $this->Filter->getData('stickers');

        $str = '';
        if ( !empty($Stickers) && $Stickers['count_products'] ) {
            $str .= '<div class="filter_block_item open">';
            $str .= '<div class="submenu_children '. (count($Stickers['selected']) > 0 ? 'selected' : '') .'">';
            foreach ($Stickers['Values'] as $Value) {
                if ( $Value['count_products'] > 0 || $Value['is_selected'] ) {
                    $str .= '<div data-selected="'. intval($Value['is_selected']) .'" data-alias="'. $Value['id'] .'">';
                    $str .= '<input type="checkbox" value="'. $Value['id'] .'" '. (intval($Value['is_selected']) ? 'checked' : '') .'>';
                    $str .= '<a href="'. $this->buidHref('stickers', $Value['alias']) .'">'.$Value['title'].'</a>';
                    $str .= '<span class="count">'. $Value['count_products'] .'</span>';
                    $str .= '</div>';
                }
            }
            $str .= '</div>';
            $str .= '</div>';
        }

        return $str;
    }

    private function getFilter()
    {
        $Filters = $this->Filter->getData('filters');

        $str = '';

        foreach ($Filters['Groups'] as $Group) {
            if ( $Group['count_products'] > 0 || count($Group['selected']) ) {
                $str .= '<div class="filter_block_item '. ($Group['is_open'] || count($Group['selected']) > 0 ? 'open' : '') .'">';
                $str .= '<div class="submenu_title">'. $Group['title'] .'</div>';
                $str .= '<div class="submenu_children '. ($Group['is_multi'] > 0 ? '' : 'unic') .' '. (count($Group['selected']) > 0 ? 'selected' : '') .'">';
                foreach ($Group['CatalogFilterValue'] as $Value) {
                    if ( $Value['count_products'] > 0 || $Value['is_selected'] ) {
                        $str .= '<div '
                                    . 'data-has-children="'.intval( !empty($Value['children']) ).'" '
                                    . 'data-has-parent="'.intval( (bool)$Value['id_parent'] ).'" '
                                    . 'data-selected="'. intval($Value['is_selected']) .'" '
                                    . 'data-alias="'. $Value['id']
                                .'">';
                        $str .= '<input type="checkbox" value="'. $Value['id'] .'" '. (intval($Value['is_selected']) ? 'checked' : '') .'>';
                        $str .= '<a href="'. $this->buidHref('filters', $Value['alias']) .'" title="'.htmlentities($Value['title']).'">'.$Value['title'].'</a>';
                        $str .= '<span class="count">'. $Value['count_products'] .'</span>';
                        if ( !empty($Value['children']) ) {
                            $str .= '<span class="show-children-filters"></span>';
                        }
                        $str .= '</div>';
                    }
                }
                $str .= '</div>';
                $str .= '</div>';
            }
        }

        return $str;
    }

    public function buidHref($group, $value)
    {
        $params = $this->filterParams;


        $Url = new Products_Filter_Url;
        $Url->setParams($params);
        $Url->togglePartParam($group, $value, $this->Filter->getAllData());

        $path = $Url->getPath();

        if ( false !== ($alias = $this->Filter->getIndexAliasByPath($path)) ) {
            $path = $alias;
        }

        $routeParams = [
            'menu_alias'    => $this->requestParams['menu_alias'],
            'menu_id'    => $this->requestParams['menu_id'],
            'filter_params' => $path
        ];

        $routeName = !empty($routeParams['filter_params']) ? 'shop-filter-params' : 'shop';

        $return = Zend_Controller_Front::getInstance()->getRouter()->assemble($routeParams, $routeName, true);

        if ( !empty($_SERVER['QUERY_STRING']) ) {
            $return .= '?' . $_SERVER['QUERY_STRING'];
        }

        return $return;

    }
}
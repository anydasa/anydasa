<?

require_once 'Zend/View/Helper/Abstract.php';

class Site_View_Helper_MenuBreadcrumb extends Zend_View_Helper_Abstract
{

    public function menuBreadcrumb($Menu)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $parents = $Menu->getNode()->getAncestors();
        $path = '';
        $urlOptions = array();

        if ( $parents ) {
            foreach ($parents as $item) {
                $path .= '/'.$item->alias;
                if ( $item->level > 0 ) {
                    $urlOptions []= array(
                        'url'   => $view->menuLink($item),
                        'title' => $item->title,
                    );
                }
            }
        }

        return $view->breadcrumb(
            $Menu->title,
            $urlOptions
        );
    }
}

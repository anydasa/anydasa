<?

class Export_IndexController extends Controller_Main
{

    public function preDispatch()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);


        $User = Doctrine::getTable('Users')->findOneByToken((string)$this->params['token']);

        if ( empty($User) ) {
            return;
        }

        $this->view->user = $User;
    }
    public function xmlAction()
    {
        $key_cache = 'productsXML_' . md5(http_build_query($this->params));

        if ( !$return = Site_Cache::load($key_cache) ) {
        //-------CACHE START
            $Query = Products_Query::create()
                        ->setOnlyPublic()
                        ->setParamsQuery($this->params)
                        ->setParamsQuery(array('status'=>Products::STATUS_AVAILABLE));
            $this->view->list = $Query->execute();
            $return = $this->view->render('index/xml.phtml');
            Site_Cache::save($return, $key_cache);
        //-------CACHE END
        }

        echo $return;

        $this->_response->setHeader('Content-Type', 'text/xml; charset=utf-8');
    }

    public function csvAction()
    {
        $key_cache = 'productsXML_' . md5(http_build_query($this->params));

        if ( !$Products = Site_Cache::load($key_cache) ) {
            //-------CACHE START
            $Query = Products_Query::create()
                ->setOnlyPublic()
                ->setParamsQuery($this->params)
                ->setParamsQuery(array('status'=>Products::STATUS_AVAILABLE));
            $Products = $Query->execute();
            Site_Cache::save($Products, $key_cache);
            //-------CACHE END
        }
        $return = array();



        $profiler = new Doctrine_Connection_Profiler();

        $conn = Doctrine_Manager::connection();
        $conn->setListener($profiler);
        $Products->loadRelated('ProductsBrands');
        foreach ($Products as $Product) {

            $temp['code'] = $Product->code;
            $temp['title'] = $Product->title;
            $temp['brand'] = $Product->ProductsBrands->title;
            $temp['price'] = $Product->getUserPrice($this->view->user, 'USD');

            $catalogs = array();
            foreach ($Product->Catalogs as $catalog) {
                $catalogs []= $catalog->title;
            }
            $temp['catalog'] = implode('; ', $catalogs);

            $images = array();
            if ( $Product->getImage()->getImagesCount() > 0 ) {
                foreach ($Product->getImage()->getImagesList() as $img) {
                    $images[]= $img['original']['url'];
                }
            }
            $temp['images'] = implode(',', $images);

            $temp['url'] = REQUEST_SCHEME . '://'.$_SERVER['HTTP_HOST'] . $this->view->url(array('code'=>$Product->code), 'product');

            $return []= $temp;
        }

        $time = 0;
        foreach ($profiler as $event) {
            $time += $event->getElapsedSecs();
        }


        echo Site_CSV::arrayToCsv($return, true);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', 'text/csv', true)
            ->setHeader('Content-Transfer-Encoding', 'binary')
            ->setHeader('Content-Disposition', 'attachment; filename=Прайс '.date('H,i Y.m.d').'.csv')
            ->clearBody();

        $this->getResponse()->sendHeaders();
    }
}

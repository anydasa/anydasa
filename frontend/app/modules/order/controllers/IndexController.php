<?
class Order_IndexController extends Controller_Main
{
    protected static $Session;

    protected $steps = [
        'contacts' => [
            'button-title' => 'Личные данные',
            'action' => 'contacts'
        ],
        'delivery' => [
            'button-title' => 'Доставка',
            'action' => 'delivery'
        ],
        'payment' => [
            'button-title' => 'Метод оплаты',
            'title' => '',
            'action' => 'payment'
        ],
        'comment' => [
            'button-title' => 'Проверьте ваш заказ',
            'action' => 'comment'
        ]
    ];

    protected function checkStep($action)
    {
        if ( Cart::getInstance()->isEmpty() ) {
            $this->redirect('/');
        }

        foreach ($this->steps as $step) {
            if ( $action == $step['action'] ) {
                break;
            }
            if ( !$step['isCompleted'] ) {
                $this->redirect( $this->view->url(['action' => $step['action']]) );
            }
        }
    }

    public function init()
    {
        parent::init();

        self::$Session = new Zend_Session_Namespace('Order');

        foreach ($this->steps as & $step) {
            $step['isActive']    = $this->getRequest()->getActionName() == $step['action'];
            $step['isCompleted'] = !empty(self::$Session->steps[$step['action']]['isCompleted']);
        }

        $this->view->steps = $this->steps;
    }

    public function preDispatch()
    {
        $this->_helper->layout->setLayout('order');
    }

    public function indexAction()
    {
        $this->forward('contacts');
    }

    public function contactsAction()
    {
        $this->checkStep( $this->_request->getActionName() );

        $form = new Form_Order_Contacts;

        if ( $this->_request->isPost() ) {
            if ( $form->isValid($this->_request->getPost()) ) {
                self::$Session->steps['contacts']['data'] = $form->getValues();
                self::$Session->steps['contacts']['isCompleted'] = true;
                $this->redirect( $this->view->url(['action' => 'delivery']) );
            } else {
                self::$Session->steps['contacts']['data'] = $form->getValues();
                self::$Session->steps['contacts']['isCompleted'] = false;
            }
        } elseif ( !empty($this->user) ) {
            self::$Session->steps['contacts']['data'] = $this->user->toArray();
        } elseif ( empty(self::$Session->steps['contacts']['data']) ) {
            self::$Session->steps['contacts']['data'] = [];
        }

        $form->populate( self::$Session->steps['contacts']['data'] );

        $this->view->ga['dimension1'] = implode(', ', array_keys(Cart::getInstance()->getProducts()));
        $this->view->ga['dimension2'] = 'conversionintent';

        $this->view->form = $form;
    }

    public function deliveryAction()
    {
        $this->checkStep( $this->_request->getActionName() );

        $form = new Form_Order_Delivery;

        if ( $this->_request->isPost() ) {
            if ( $form->isValid($this->_request->getPost()) ) {
                self::$Session->steps['delivery']['data'] = $form->getValues();
                self::$Session->steps['delivery']['isCompleted'] = true;
                $this->redirect( $this->view->url(['action' => 'payment']) );
            } else {
                self::$Session->steps['delivery']['data'] = $form->getValues();
                self::$Session->steps['delivery']['isCompleted'] = false;
            }
        } elseif ( !empty($this->user) ) {
            self::$Session->steps['delivery']['data'] = $this->user->toArray();
        } elseif ( empty(self::$Session->steps['delivery']['data']) ) {
            self::$Session->steps['delivery']['data'] = [];
        }

        $form->populate( self::$Session->steps['delivery']['data'] );

        $this->view->form = $form;
    }

    public function paymentAction()
    {
        $this->checkStep( $this->_request->getActionName() );


        $form = new Form_Order_Payment;

        $Delivery = Doctrine::getTable('OrdersDelivery')->find(self::$Session->steps['delivery']['data']['delivery']);
        $disabled = array_diff(
            array_keys($form->payment->getMultioptions()),
            $Delivery->Payments->toKeyValueArray('id', 'id')
        );
        $form->payment->setAttrib('disable', $disabled);

        if ( $this->_request->isPost() ) {
            if ( $form->isValid($this->_request->getPost()) ) {
                $formValues = $form->getValues();
                self::$Session->steps['payment']['data'] = $formValues;
                if ($formValues['payment'] === 'novapay') {
                    $Order = new Orders;
                    $Order->id_user = $this->user->id;

                    $Order->name  = self::$Session->steps['contacts']['data']['name'];
                    $Order->email = self::$Session->steps['contacts']['data']['email'];
                    $Order->phone = self::$Session->steps['contacts']['data']['phone'];

                    $Order->delivery_type = self::$Session->steps['delivery']['data']['delivery'];
                    $Order->payment_type  = self::$Session->steps['payment']['data']['payment'];

                    $Order->admin_payment_confirmed = false;

                    $Order->address = "Город: " . self::$Session->steps['delivery']['data']['city'] . "\n";

                    switch ($Order->delivery_type) {
                        case 'npcourier':
                            $Order->address .= self::$Session->steps['delivery']['data']['npcourier']['address'];
                            break;
                        case 'courier':
                            $Order->address .= self::$Session->steps['delivery']['data']['courier']['address'];
                            $Order->desired_time_delivery = self::$Session->steps['delivery']['data']['courier']['begin_time'] . '-' . self::$Session->steps['delivery']['data']['courier']['end_time'];
                            break;
                        case 'novaposhta':
                            $Order->address .= "Склад: " . Doctrine::getTable('NovaposhtaWarehouses')->findOneByCode( self::$Session->steps['delivery']['data']['novaposhta']['warehouse'] )->description_ru;
                            break;
                        case 'pickup':
                            $Order->desired_time_delivery = self::$Session->steps['delivery']['data']['pickup']['begin_time'] . '-' . self::$Session->steps['delivery']['data']['pickup']['end_time'];
                            break;
                    }

                    $cart = Cart::getInstance();

                    $Order->addProducts($cart);

                    $novapayService = new Novapay_NovapayService();
                    $paymentSessionId = $novapayService->beginSession(
                        $Order->phone ? $Order->phone : '+380500636363',
                        $this->view->url(['action' => 'novapay-postback']),
                        $this->view->url(['action' => 'comment'])
                    );

                    $Order->novapay_session_id = $paymentSessionId;
                    $Order->save();

                    $Order->refresh(true);

                    self::$Session->orderId = $Order->id;

                    /** @var OrdersPayment $Order */
                    $orderPaymentType = Doctrine::getTable('OrdersPayment')->find('novapay');

                    $sumAmount = 0.0;

                    foreach ($cart->getProducts() as $product) {
                        $sumAmount += (float)$product['total'];
                    }

                    $sumAmount = round($sumAmount, 2);

                    $comission = round($sumAmount * $orderPaymentType->commission / 100, 2);

                    $sumAmount = ceil($sumAmount + $comission);

                    $getPaymentUrl = $novapayService->payment($paymentSessionId, $sumAmount);

                    self::$Session->steps['payment']['isCompleted'] = true;
                    $this->redirect($getPaymentUrl);
                } else {
                    self::$Session->steps['payment']['isCompleted'] = true;
                }
                $this->redirect($this->view->url(['action' => 'comment']));
            } else {
                self::$Session->steps['payment']['data'] = $form->getValues();
                self::$Session->steps['payment']['isCompleted'] = false;
            }
        } elseif ( !empty($this->user) ) {
            self::$Session->steps['payment']['data'] = $this->user->toArray();
        } elseif ( empty(self::$Session->steps['payment']['data']) ) {
            if ( Credit::issetReadyOffer() ) {
                self::$Session->steps['payment']['data']['payment'] = 'credit';
            } else {
                self::$Session->steps['payment']['data'] = [];
            }

        }

        if ( self::$Session->steps['payment']['data']['payment'] == 'credit' && Credit::issetReadyOffer() ) {
            self::$Session->steps['payment']['data']['credit'] = [
                'offer' => Credit::getParam('Offer')->title,
                'id_offer' => Credit::getParam('Offer')->id
            ];
        }

        $form->populate( self::$Session->steps['payment']['data'] );

        $this->view->form = $form;
    }

    public function commentAction()
    {
        $this->checkStep( $this->_request->getActionName() );

        $form = new Form_Order_Comment;

        if ( $this->_request->isPost() ) {
            if ( $form->isValid($this->_request->getPost()) ) {

//                if (!isset(self::$Session->orderId)) {
                    $Order = new Orders;
                    $Order->id_user = $this->user->id;
                    $Order->name = self::$Session->steps['contacts']['data']['name'];
                    $Order->email = self::$Session->steps['contacts']['data']['email'];
                    $Order->phone = self::$Session->steps['contacts']['data']['phone'];
                    $Order->comment = $form->getValue("comment");
                    $Order->delivery_type = self::$Session->steps['delivery']['data']['delivery'];
                    $Order->payment_type = self::$Session->steps['payment']['data']['payment'];
                    $Order->address = "Город: " . self::$Session->steps['delivery']['data']['city'] . "\n";

                    switch ($Order->delivery_type) {
                        case 'npcourier':
                            $Order->address .= self::$Session->steps['delivery']['data']['npcourier']['address'];
                            break;
                        case 'courier':
                            $Order->address .= self::$Session->steps['delivery']['data']['courier']['address'];
                            $Order->desired_time_delivery = self::$Session->steps['delivery']['data']['courier']['begin_time'] . '-' . self::$Session->steps['delivery']['data']['courier']['end_time'];
                            break;
                        case 'novaposhta':
                            $Order->address .= "Склад: " . Doctrine::getTable('NovaposhtaWarehouses')->findOneByCode(self::$Session->steps['delivery']['data']['novaposhta']['warehouse'])->description_ru;
                            break;
                        case 'pickup':
                            $Order->desired_time_delivery = self::$Session->steps['delivery']['data']['pickup']['begin_time'] . '-' . self::$Session->steps['delivery']['data']['pickup']['end_time'];
                            break;
                    }

                    switch ($Order->payment_type) {
                        case 'credit':
                            $Order->credit = Credit::getSerializedData();
                            break;
                    }

                    $Order->addProducts(Cart::getInstance());
//                } else {
//                    /** @var Orders $Order */
//                    $Order = Doctrine::getTable('Orders')->find(self::$Session->orderId);
//                    $novapayService = new Novapay_NovapayService();
//                    $novapayStatus = $novapayService->getStatus($Order->novapay_session_id);
//                    $Order->novapay_payment_status = $novapayStatus;
//                }

                $Order->save();

                Cart::destroy();

                $Order->refresh(true);

                $Order->sendMessagesForCurrentStatus();

                self::$Session->orderId = $Order->id;

                $this->redirect( $this->view->url(['action' => 'success']) );
            }
        }

        $this->view->form = $form;
    }

    public function successAction()
    {
        $orderId = self::$Session->orderId;

        if ( empty($orderId) OR !($Order = Doctrine::getTable('Orders')->find($orderId)) ) {
            $this->redirect( '/' );
        }

        $this->view->ga['dimension1'] = implode(', ', $Order->Products->toKeyValueArray('id', 'id'));
        $this->view->ga['dimension2'] = 'conversion';

        $this->view->Order = $Order;
    }

    public function novapayPostbackAction()
    {
        $data = file_get_contents("php://input");;
        $dataDecoded = json_decode($data, true);

        $sessionId = $dataDecoded['id'];
        $status = $dataDecoded['status'];

        /** @var Orders $Order */
        $Order = Doctrine::getTable('Orders')->findBy('novapay_session_id', $sessionId);
        $Order->novapay_payment_status = $status;
        $Order->save();
    }

    public function oneClickAction()
    {
        $this->_helper->layout->disableLayout();

        if ( $this->_request->isPost() ) {
            $Order = new Orders;

            $Order->name  = '-';
            $Order->phone = $this->_request->getPost("phone");
            $Order->email = $this->_request->getPost("email");
            $Order->addProducts(Cart::getInstance());
            $Order->is_one_click = true;
            $Order->referer = $_SESSION['referer'];
            unset($_SESSION['referer']);
            $Order->save();
            $Order->refresh(true);


            $Order->sendMessagesForCurrentStatus();

            Cart::destroy();

            $this->view->Order = $Order;
        }

    }
    public function setEmailAction(){
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        if ( $this->_request->isPost() ) {
            self::$Session->steps['contacts']['data']['email'] = $this->_request->getPost("email");
        }
        echo '<script>
            $(\'#Form_Order_Payment\').find(\'input:radio[value=credit]\').prop(\'checked\', true);
            $(\'#Form_Order_Payment\').find("div.form div.form").show();
            $(\'#Form_Order_Payment\').find("div.form div.form").find(\'select,input,textarea\').prop(\'disabled\', false);
            $(\'#step-payment\').find("#payment-credit-text").show();
            closeModalWindow();
            
                </script>';
    }

}

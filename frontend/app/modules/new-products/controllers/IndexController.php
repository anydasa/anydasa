<?

class NewProducts_IndexController extends Controller_Main
{

    public function indexAction()
    {
        $params = $this->params;

        if (empty($params['sort'])) {
            $params['sort'] = 'id_desc';
        }

        $params['is_new'] = 1;

        $query                   = Products_Query::create()->setOnlyPublic()->setParamsQuery($params);
        $countOnPage             = ('all' == $params['page']) ? 999999 : 40;

        $pager                   = new Doctrine_Pager($query, $params['page'], $countOnPage);
        $this->view->ProductList = $pager->execute();
        $this->view->pagerRange  = $pager->getRange('Sliding', array('chunk' => 9));

    }
}
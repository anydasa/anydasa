<?

class Promotions_IndexController extends Controller_Main
{

    public function listAction()
    {
        $Query = Doctrine_Query::create()
            ->from('Promotions')
            ->addWhere('time_off::date >= CURRENT_DATE')
            ->addWhere('id in (SELECT p.id_promotion FROM products p)')
            ->addWhere('is_enabled = 1')
            ->orderBy('time_off ASC');

        if ( !empty($this->_request->getParam('type')) ) {
            $Query->addWhere('id_type = ?', $this->_request->getParam('type'));
        }
        if ( !empty($this->_request->getParam('category')) ) {
            $Query->addWhere('id_category = ?', $this->_request->getParam('category'));
        }

        $pager = new Doctrine_Pager($Query, $this->_request->getParam('page'), 15);
        $this->view->Promotions = $pager->execute();
        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $this->view->PromoCategoryList = Doctrine_Query::create()
            ->from('PromotionsCategories')
            ->addWhere('id IN (SELECT tt.id_category FROM promotions tt WHERE tt.is_enabled = 1)')
            ->orderBy('sort')
            ->execute();

        $this->view->PromoTypeList = Doctrine_Query::create()
            ->from('PromotionsTypes')
            ->addWhere('id IN (SELECT tt.id_type FROM promotions tt WHERE tt.is_enabled = 1)')
            ->orderBy('sort')
            ->execute();
    }

    public function itemAction()
    {
        $params = $this->params;

        $Promotion = Doctrine::getTable('Promotions')->find($this->params['id']);

        if (!$Promotion) {
            throw new Site_Exception_NotFound;
        }

        unset($params['id']);

        $params['id_promotion'] = $Promotion->id;
//        $params['balance_from'] = 1;

        $query                   = Products_Query::create()->setOnlyPublic()->setParamsQuery($params);
        $countOnPage             = ('all' == $params['page']) ? 999999 : 40;

        $pager                   = new Doctrine_Pager($query, $params['page'], $countOnPage);
        $this->view->ProductList = $pager->execute();
        $this->view->pagerRange  = $pager->getRange('Sliding', array('chunk' => 9));

        $this->view->Promotion    = $Promotion;
    }

}
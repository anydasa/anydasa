<?

class Search_IndexController extends Controller_Main
{
    public function indexAction()
    {
        $Query = Products_Query::create()
            ->setOnlyPublic()
//            ->setOnlyAvailableForOrder()
            ->setParamsQuery($this->params);

        $countOnPage = 'all' == $this->params['page'] ? 999999 : 16;
        $pager = new Doctrine_Pager($Query, $this->params['page'], $countOnPage);
        $this->view->ProductList = $pager->execute();

        $this->view->pagerRange = $pager->getRange('Sliding', array('chunk' => 9));

        $this->view->headTitle('Поиск', 'PREPEND');
        $this->view->headDescription('Поиск', 'PREPEND');
        $this->view->headMeta()->appendProperty('robots', 'noindex, follow');

        if ( $pager->getLastPage() < (int)$this->params['page'] || 0 == $pager->getNumResults() ) {
            $this->getResponse()->setHttpResponseCode(404);
        }

        /*if ($this->getRequest()->isXmlHttpRequest()) {
            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            echo Zend_Json::encode(array(
                'items'      => $this->view->render('goods-result-items.phtml'),
                'pagination' => $this->view->showPager($this->view->pagerRange, $this->view->url($this->params, Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName()))
            ));

        }*/
    }

    public function autocompleteAction()
    {
        $result = [];

        if ( !empty($this->params['k']) ) {

            $Query = Products_Query::create()
                ->setOnlyPublic()
                ->setOnlyAvailableForOrder()
                ->setParamsQuery($this->params)
                ->limit(10);

            foreach ($Query->execute() as $item) {
                $result[$item->id]['value'] = $item->title;
                $result[$item->id]['title'] = $item->title;
                $result[$item->id]['type']  = '';
                $result[$item->id]['img']   = $item->getImage()->getFirstImage()['thumb']['url'];
                $result[$item->id]['href']  = $this->view->ProductUrl($item);
                $result[$item->id]['price'] = $item->getFormatedPrice('retail', $_SESSION['currency']);

                $result[$item->id]['brand'] = '';
            }
        }

        return $this->_helper->json($result);
    }

}

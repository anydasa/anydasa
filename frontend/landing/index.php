<?php

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

error_reporting(E_ALL);

require_once '../../etc/functions.php';
require_once '../../etc/config.php';

$paths = implode(PATH_SEPARATOR, array(
        $config['path']['lib'],
        $config['path']['appLib'],
        $config['path']['doctrineModels'],
        $config['path']['src'],
    )
);

set_include_path($paths);
date_default_timezone_set($config['timezone']);

require_once 'Zend/Loader/Autoloader.php';
$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->setFallbackAutoloader(true);

$dns = sprintf('%s:dbname=%s;host=%s',
    $config['db']['adapter'],
    $config['db']['params']['dbname'],
    $config['db']['params']['host']
);

$connection = new Site_Db_Connection(
    $dns,
    $config['db']['params']['username'],
    $config['db']['params']['password']
);

Site_Db_Doctrine::init($connection, $config['path']['doctrineModels']);

$Page = Doctrine::getTable('LandingPage')->findOneBy('domain', $_SERVER['HTTP_HOST']);

if (!$Page) {
    echo 'Error not found';
    exit;
}

$twig = new Environment(new FilesystemLoader(__DIR__));
echo $twig->render('layout.html.twig', $Page->toArray());

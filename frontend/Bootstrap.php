<?

class Bootstrap
{

    public static $registry;
    public static $frontController;

    public function __construct()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->setFallbackAutoloader(true);
    }

    public static function run($config)
    {
        try {
            self::setupRegistry();
            self::setupLogger();
            self::setupConfiguration($config);
            self::checkDomain();
            self::setupLog();

            self::setupView();
            self::setupDbAdapter();
            self::setupSession();
            self::setupFrontController();
            self::setupRouter();
            self::setupCache();
            self::$frontController->dispatch();
        } catch (Site_Exception_NotFound $e) {
            self::_goToPage404();
        } catch (Site_Exception_AccessDenied $e) {
            self::_goToPage403();
        }
        catch (\Throwable $e) {
            Sentry\captureException($e);
            self::_goToPage500();
        }
    }

    private static function checkDomain()
    {

        if ($_SERVER['HTTP_HOST'] != parse_url(self::$registry->config->url->domain)['host']) {

            $s = &$_SERVER;
            $ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
            $sp = strtolower($s['SERVER_PROTOCOL']);
            $protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
            $port = $s['SERVER_PORT'];
            $port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
            $url = $protocol . '://' . parse_url(self::$registry->config->url->domain)['host'] . $port . $s['REQUEST_URI'];

            header ('HTTP/1.1 301 Moved Permanently');
            header ('Location: '.$url);

            exit;
        }
    }

    private static function _goToPage404()
    {
        $request = new Zend_Controller_Request_Http;
        $response = new Zend_Controller_Response_Http;

        $request->setModuleName('default')->setControllerName('error')->setActionName('page-not-found');

        $dispatcher = self::$frontController->getDispatcher();
        $dispatcher->dispatch($request, $response);

        $response->sendHeaders();
        $response->outputBody();

    }

    private static function _goToPage403()
    {
        $request = new Zend_Controller_Request_Http;
        $response = new Zend_Controller_Response_Http;

        $request->setModuleName('default')->setControllerName('error')->setActionName('access-denied');

        $dispatcher = self::$frontController->getDispatcher();
        $dispatcher->dispatch($request, $response);

        $response->sendHeaders();
        $response->outputBody();

    }

    private static function _goToPage500()
    {
        header('HTTP/1.1 500 Bad Request');
        exit;

        $request = new Zend_Controller_Request_Http;
        $response = new Zend_Controller_Response_Http;

        $request->setModuleName('default')->setControllerName('error')->setActionName('error500');

        $dispatcher = self::$frontController->getDispatcher();
        $dispatcher->dispatch($request, $response);

        $response->sendHeaders();
        $response->outputBody();

    }

    public static function setupCache()
    {
        Site_Cache::init();
    }

    public static function setupFrontController()
    {
        self::$frontController = Zend_Controller_Front::getInstance();
        self::$frontController->setBaseUrl('/');
        self::$frontController->addModuleDirectory(self::$registry->config->path->app . 'modules');
        self::$frontController->throwExceptions(true);
        self::$frontController->setParam('noErrorHandler', true);
    }

    public static function registerAdminLogPlugin()
    {
        self::$frontController->registerPlugin(new Site_Controller_Plugin_AdminLog());
    }

    public static function setupRegistry()
    {
        self::$registry = new Zend_Registry();
        Zend_Registry::setInstance(self::$registry);
    }

    public static function setupSession()
    {
        Zend_Session::start(self::$registry->config->session->toArray());
    }

    public static function setupLog()
    {
//        Site_Log::init(self::$registry->config->path->logs, self::$registry->config->debug->on);
    }

    public static function setupConfiguration($config)
    {
        self::$registry->config = new Zend_Config($config);
    }

    public static function setupView()
    {
        Zend_Layout::startMvc(array('layoutPath' => self::$registry->config->path->app . 'views/layouts', 'layout' => 'main'));

        $layout = Zend_Layout::getMvcInstance();

        $view = $layout->getView()->setBasePath(self::$registry->config->path->app . 'views/');

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewDoctype = new Zend_View_Helper_Doctype();
        $viewDoctype->setDoctype('HTML5');

        $viewDoctype->setView($view);
        $viewRenderer->setView($view);

        $layout->setView($view);

        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        $view->addHelperPath(self::$registry->config->path->app . 'views/helpers', 'Site_View_Helper');
        $view->addHelperPath('Glitch/View/Helper', 'Glitch_View_Helper_');
    }

    public static function setupDbAdapter()
    {
        $db = self::$registry->config->db;
        $connection = new Site_Db_Connection($db->dsn, $db->username, $db->password);
        Site_Db_Doctrine::init($connection, self::$registry->config->path->doctrineModels);
    }

    public static function setupRouter()
    {
        $router = self::$frontController->getRouter();
        $router->removeDefaultRoutes();

        $router->addConfig(new Zend_Config_Ini(self::$registry->config->path->app . 'etc/routers/core.ini', 'core'), 'routes');

        foreach (glob(self::$registry->config->path->app . 'etc/routers/modules/*.ini') as $filename) {
            $router->addConfig(new Zend_Config_Ini($filename, basename($filename, '.ini')), 'routes');
        }

        self::$frontController->setRouter($router);
    }

    public static function setupLogger()
    {
        Sentry\init(['dsn' => getenv('SENTRY_DSN') ]);

        $sentryClient = \Sentry\ClientBuilder::create(['dsn' => getenv('SENTRY_DSN')])->getClient();

        $logger = new \Monolog\Logger('app');
        $sentryHandler = new \Sentry\Monolog\Handler(new \Sentry\State\Hub($sentryClient));
        $logger->pushHandler($sentryHandler);

        Zend_Registry::set('logger', $logger);
    }
}

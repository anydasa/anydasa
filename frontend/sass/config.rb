require 'compass/import-once/activate'

http_path = "/img"
css_dir = "./../public/css"
sass_dir = "./"
images_dir = "./../public/img"
javascripts_dir = "./../public/js"
generated_images_dir = "./../public/img/sprites"
#sourcemap = true

cache = false

output_style = :compressed

add_import_path "./"

relative_assets = true
#line_comments = false

<?

error_reporting(0);

require_once '../../vendor/autoload.php';
require_once '../../etc/functions.php';
require_once '../../etc/config.php';
require_once '../app/etc/config.php';
require_once '../Bootstrap.php';

$config = array_join($config, $appConfig);

$paths = implode(PATH_SEPARATOR, array(
        $config['path']['lib'],
        $config['path']['appLib'],
        $config['path']['doctrineModels'],
        $config['path']['src'],
        SRC_PATH,
    )
);

$paths = get_include_path() . ':' . $paths;

set_include_path($paths);
date_default_timezone_set($config['timezone']);

$bootstrap = new Bootstrap($config);
$bootstrap->run($config);
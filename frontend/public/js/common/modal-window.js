$(function () {
    $(document).on('click', ".mw-close", closeModalWindow);
    $(document).bind('keydown', function (event) {if (27 == event.which) closeModalWindow()});

    $(document).on('click', '[data-modal-link]', function() {
        var targetIndex = getTargetIndex($(this).attr('data-target'), 'blank')

        if ( targetIndex < 1 ) {
            $('body').addClass('wait')
            document.location = $(this).attr('data-modal-link');
            return false;
        }

        loadModalWindow($(this).attr('data-modal-link'), targetIndex );
        return false;
    });

    $(document).on('submit', '.mw-content form', function (e) {
        $form = $(this);

        $(this).find('[type=submit]')
            .attr('data-text', $(this).find('[type=submit]').html())
            .prop('disabled', true)
            .css({height: $(this).find('[type=submit]').outerHeight(true), width: $(this).find('[type=submit]').outerWidth(true)})
            .html('<i class="bar"></i>')
            ;

        var type     =  'POST' == $form.attr('method').toUpperCase() ? 'POST' : 'GET';
        var formData = ('POST' == type) ? new FormData(this) : $form.serialize() ;

        $.ajax({
            url:         $form.attr('action'),
            type:        type,
            data:        formData,
            mimeType:    $form.attr('enctype'),
            contentType: false,
            cache:       false,
            processData: false
        }).error(function(data) {
            openModalWindow(data.responseText, this.url, getTargetIndex('self', 'self'))
        }).success(function(data) {
            if ( checkAndEval(data) ) {
                closeModalWindow()
                return;
            }

            var redirectUrl = getUrlIfRedirect(data);
            var targetIndex = getTargetIndex($form.attr('data-target'), 'prev');
            if ( targetIndex < 1 && redirectUrl ) {
                document.location = redirectUrl;
                return;
            }
            if ( redirectUrl ) {
                loadModalWindow(redirectUrl, targetIndex)
            } else {
                openModalWindow(data, this.url, targetIndex)
            }
        }).always(function () {
            $(this)
                .find('[type=submit]')
                .html( $(this).find('[type=submit]').attr('data-text') )

                .prop('disabled', false) ;
        });

        return false;
    })
})

function checkAndEval(data)
{
    if ( 'Eval:' == data.substr(0,5) ) {
        eval(data.substr(5));
        return true;
    }
    return false;
}

function getTargetIndex(target, def)
{
    if ( undefined == target ) {
        target = def
    }
    if ( 'self' == target ) {
        var targetIndex = $(".mw-wrap").length;
    } else if ( 'prev' == target ) {
        var targetIndex = $(".mw-wrap").length - 1;
    } else {
        var targetIndex = $(".mw-wrap").length + 1;
    }

    return targetIndex;
}

function getUrlIfRedirect(data)
{
    if ( 'Location:' == data.substr(0,9) ) {
        return data.substr(9)
    }
    return false
}

function loadModalWindow(url, targetIndex)
{
    $('body').addClass('wait');

    $.ajax({
        url:         url,
        type:        'GET',
        async:       false,
        contentType: false,
        cache:       false,
        processData: false
    }).error(function(data) {
        openModalWindow(data.responseText, this.url, getTargetIndex('new', 'new'))
    }).success(function(data) {
        var redirectUrl = getUrlIfRedirect(data);
        if ( redirectUrl ) {
            if ( targetIndex < 1 ) {
                document.location = redirectUrl;
                return;
            } else {
                loadModalWindow(redirectUrl, targetIndex)
            }
        } else {
            openModalWindow(data, this.url, targetIndex)
        }
    });

}

function openModalWindow(data, url, targetIndex)
{
    $('body').removeClass('wait')

    var lastIndex = $(".mw-wrap").length + 1;

    if ( 0 === $("#mw-fade").length ) {//Если нет полупрозрачного слоя, показываем
        $("<div />").attr('id', 'mw-fade').appendTo('body')
        $("body").css({overflow: 'hidden', paddingRight: $.position.scrollbarWidth()})
    }

    for (let $i=lastIndex-targetIndex; $i>0; $i--) {
        $('.mw-wrap.mIndex'+$(".mw-wrap").length).remove();
    }

    $("<div />").addClass('mw-wrap mIndex'+targetIndex).attr('data-url', url).appendTo('body')
    $("<div />").addClass('mw-content').hide().appendTo('.mw-wrap.mIndex'+targetIndex)
    $("<div />").addClass('mw-move').appendTo('.mw-wrap.mIndex'+targetIndex+' .mw-content')
    $("<div />").addClass('mw-close').html('<i></i>').appendTo('.mw-wrap.mIndex'+targetIndex+' .mw-content')
    $('<div class="mw-container-content">'+data+'</div>').appendTo('.mw-wrap.mIndex'+targetIndex+' .mw-content')

    $('.mw-wrap.mIndex'+targetIndex+' .mw-content').css({position:'relative', top: ((targetIndex-1)*40), left: ((targetIndex-1)*20)}).show()
    $('.mw-wrap.mIndex'+targetIndex+' .mw-content').draggable({handle: '.mw-move'});

    $('.mw-wrap').removeClass('last').last().addClass('last');

    $("form input[type=submit]").click(function() {
        $("<input />")
            .attr('type', 'hidden')
            .attr('name', $(this).attr('name'))
            .attr('value', $(this).attr('value'))
            .appendTo(this)
    });
}


function closeModalWindow()
{
    var onclose = $('.mw-wrap.mIndex'+$(".mw-wrap").length).data('onclose');

    $('.mw-wrap.mIndex'+$(".mw-wrap").length).remove();
    $('.mw-wrap').removeClass('last').last().addClass('last')

    if ( 0 == $(".mw-wrap").length ) {
        $("#mw-fade").remove();
        $("body").css({overflow: 'auto', paddingRight: '0'})
    }

    if ( 'reload-prev' == onclose ) {
        reloadModalWindow( getTargetIndex('self') )
    }
}

function reloadModalWindow(index)
{
    if ( undefined == index ) {
        index = $(".mw-wrap").length;
    }

    if ( index < 1 ) {
        window.location.reload();
    } else {
        var url = $('.mw-wrap.mIndex'+index).data('url')

        $('.mw-wrap.mIndex'+index).remove();
        loadModalWindow(url, index)
        $('.mw-wrap').removeClass('last').last().addClass('last')
    }
}

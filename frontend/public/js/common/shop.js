
$(function () {
    setupFilterBlock();

    var stateObjDefault = {
        container: $("#shop-container").html()
    };

    window.addEventListener('popstate', function(e) {
        if (e.state) {
            populateProducts(e.state);
        } else {
            populateProducts(stateObjDefault);
        }
    }, false);



    $(document).on('click', '.submenu_children input', function () {
        $(this).parent().find('a')[0].click();
    })

    $(document).on('click', '.submenu_children a, .pagination a, .filter_block_accepted a', function (e) {
        if ($(this).parents('.submenu').length) {
            return
        }

        e.preventDefault();

        $('body').addClass('wait');
        var url = $(this).attr('href');

        $.get(url, function (data) {
            window.history.pushState({container: data}, '' , url)
            populateProducts({container: data});
            $('body').removeClass('wait')
            $('html, body').animate({scrollTop: 0}, 300);
        }, null, 'html' )
    });

});

var changeRange = function () {
    var $slider = $("#price-range");

    var min = ($slider.slider("values", 0) == $slider.slider("option", "min")) ? null : $slider.slider("values", 0);
    var max = ($slider.slider("values", 1) == $slider.slider("option", "max")) ? null : $slider.slider("values", 1);

    var uri = window.location.href;
    uri = updateQueryStringParameter(uri, "min", min);
    uri = updateQueryStringParameter(uri, "max", max);
    $(".price .search").attr("href", uri).show();
};

$(document).on('slide', '#price-range', function( event, ui ) {
    $("input#minPrice").val( ui.values[0] );
    $("input#maxPrice").val( ui.values[1] );
});
$(document).on('slidecreate', '#price-range', function( event, ui ) {
    $("input#minPrice").val( $(this).slider("values")[0] );
    $("input#maxPrice").val( $(this).slider("values")[1] );
});

$(document).on('slidechange', '#price-range', function () { changeRange(); });

$(document).on('change', 'input#minPrice, input#maxPrice', function(){
    var $slider = $("#price-range");
    var $minPrice = $("input#minPrice");
    var $maxPrice = $("input#maxPrice");

    var min = $slider.slider("option", "min");
    var max = $slider.slider("option", "max");

    if ( $minPrice.val() < min ) {
        $minPrice.val(min);
    }

    if ( $maxPrice.val() > max ) {
        $maxPrice.val(max)
    }

    $slider.slider("values", 0, $minPrice.val());
    $slider.slider("values", 1, $maxPrice.val());

    changeRange();
});



$(document).on('click', '#sortProductsType li', function (e) {
    $.post('/ajax/products/set-sort/', {
        sort: $(this).attr('data-sort')
    }, function () {
        $('body').addClass('wait');
        window.location.reload()
    });
});

$(document).on('click', '#sortProductsFirst li', function (e) {
    $.post('/ajax/products/set-first/', {
        first: $(this).attr('data-first')
    }, function () {
        $('body').addClass('wait');
        window.location.reload()
    });
});

$(document).on('click', '.submenu_title', function (e) {
    $block = $(this).closest('.filter_block_item');

    if ( $block.hasClass('open') ) {
        $block.removeClass('open');
    } else {
        $block.addClass('open');
    }
});

/* --------------------------------------------------------------- */

function setupFilterBlock()
{
    //$('.submenu_children').jScrollPane();
    $(".product_list").css({minHeight: $(".left_block").outerHeight()});
}

function populateProducts (stateObj){
    $("#shop-container").html(stateObj.container)
    setupFilterBlock();
    imgLazyLoad();
}


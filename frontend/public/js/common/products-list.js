$(function () {
    $(".styled-select").on('mouseenter', function () {
        $("#status_list_box").slideDown(300)
    })
    $(".styled-select").on('mouseleave', function () {
        $("#status_list_box").slideUp(300)
    })

    $("#status_list_box").submit(function () {
        $(".styled-select").off('mouseleave')
    })
    if ( $("#status_list_box input:checked").length ) {
        $("#status_list_chkb").css({color: 'red'})
    }

    $(".products-list.display-as-list tr td:nth-child(2)").click(function (e) {
        if ('IMG' != e.target.tagName.toUpperCase()) {
            $(".products-list tr td:nth-child(2)").not(this).find(".detail-info:visible").hide()
            $(this).find('.detail-info').toggle()
        }
    })

})


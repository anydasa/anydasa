$(document).on('click', '.slide-next', function(){
    item = $(this).prev();
    $(item).parent().find('.slide-prev').show();
    $(item).find('div:visible:last').next().show(300);
    $(item).find('div:visible:first').hide(300);
    if($(item).find('div:visible:last').index() == ($(item).find('div').length - 1)) $(this).hide(300);
});

$(document).on('click', '.slide-prev', function(){
    item = $(this).next();
    $(item).parent().find('.slide-next').show();
    $(item).find('div:visible:first').prev().show(300);
    $(item).find('div:visible:last').hide(300);
    if($(item).find('div:visible:first').index() == 0) $(this).hide(300);
});



$(document).on('click', 'a[href^="mailto:"]', function () {
    ga('send', 'event', 'Нажатие на ссылку', 'Email', document.title);
});

$(document).on('mouseenter', '.tooltip', function(event) {

    if ( 0 == $(this).attr('title').length ) {
        return;
    }

    $(this).renameAttr('title', 'data-title');

    $("<div />").css({
        left: $(this).offset().left + $(this).outerWidth() + 2,
        top: $(this).offset().top - 15
    }).addClass('tooltip_message')
        .text($(this).attr('data-title'))
        .hide()
        .appendTo($('body'))
        .show();
});

$(document).on('mouseleave', '.tooltip', function() {
    $(this).renameAttr('data-title', 'title');
    $(".tooltip_message").remove();
});


jQuery.fn.extend({
    renameAttr: function( name, newName ) {
        var val;
        return this.each(function() {
            val = jQuery.attr( this, name );
            jQuery.attr( this, newName, val );
            jQuery.removeAttr( this, name );
        });
    }
});

function imgLazyLoad(a) {
    var b = $("img[data-original]:visible");
    if (a) {
        b = $("img[data-original]:visible", a)
    }
    b.each(function () {
        var d = $(this), c = $(window).height(), e = $(window).scrollTop(), i = $(this).height(), g = $(this).offset().top;
        if ((e < (g + i)) && (g < (e + c))) {
            var f = $(new Image()), h = d.attr("data-original");
            f.attr("src", h);
            f.load(function () {
                d.css({opacity: 0}).attr("src", h).removeAttr("data-original").animate({opacity: 1}, 500)
            })
        }
    })
}

window.onscroll = function () {
    imgLazyLoad();
};

function updateQueryStringParameter(url, key, value) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null)
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null)
                url += '#' + hash[1];
            return url;
        }
        else
            return url;
    }
}

function intInput(b) {
    var a = (typeof b.charCode == "undefined" ? b.keyCode : b.charCode);
    if (a < 32) {
        return true
    }
    a = String.fromCharCode(a);
    return /[\d]/.test(a)
}

$(function () {
    imgLazyLoad();
});


var mouseleaveTimerHintHelp;
var mouseenterTimerHintHelp;

$(document).on({
    mouseenter: function (event) {
        if (mouseleaveTimerHintHelp) clearTimeout(mouseleaveTimerHintHelp);
        mouseenterTimerHintHelp = setTimeout(function() {
            $(event.currentTarget).find('.hint-text').show();
        }, 10);
    },
    mouseleave: function (event) {
        if (mouseenterTimerHintHelp) clearTimeout(mouseenterTimerHintHelp);
        mouseleaveTimerHintHelp = setTimeout(function() {
            $(event.currentTarget).find('.hint-text').hide();
        }, 300);
    }
}, ".hint-help");


$(document).on({
    mouseenter: function (event) {
        $(event.currentTarget).prevAll().addBack().addClass('hover');
    },
    mouseleave: function (event) {
        $(event.currentTarget).prevAll().addBack().removeClass('hover');
    },
    click: function (event) {
        $(event.currentTarget).nextAll().removeClass('active');
        $(event.currentTarget).prevAll().addBack().addClass('active');
    }
}, ".rating_vote > *");

$(document).ready(function(){
    $(".tabs").lightTabs();
});

$(function () {
    if( $('.mainslider .mainslider-items').length > 1 ) {
		var owl = $('.mainslider-block');
		owl.owlCarousel({
			autoplay:true,
			autoplayTimeout:3000,
			smartSpeed:1000,
			autoplayHoverPause:true,
			loop:true,
			margin:0,
			nav:false,
			singleItem: true,
			items:1,
			responsive:true
		});
	}
});
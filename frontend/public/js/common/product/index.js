var tabs_positions;
var tab_active;

$(function() {
    var stateObjDefault = {content: $("<div />").append($('section.product').clone()).html()};
    populateProductPage(stateObjDefault);
    window.history.replaceState(stateObjDefault, '', window.location);

    window.addEventListener('popstate', function(e) {
        populateProductPage(e.state);
    }, false);

});

$(function() {
    $(document).on('scroll', function (e) {
        $.each(window.tabs_positions, function (key, value) {
            if ( $(document).scrollTop() < value ) {
                if ( key !== window.tab_active ) {
                    window.tab_active = key;
                    $(".scroll_pop .tabs a").removeClass("active");
                    $(".scroll_pop .tabs a[href='#"+key+"']").addClass("active");
                }
                return false;
            }
        });
    }).scroll();
});

$(document).on('click', '.group-products a', function (e) {
    e.preventDefault();
    $('body').addClass('wait');
    var url = $(this).attr('href');
    $.get(url, function (data) {
        var stateObj = {content: data};
        window.history.pushState(stateObj, '' , url);
        populateProductPage(stateObj);
        $('body').removeClass('wait');
    });
});

$(document).on('click', '.tabs a, .tabs-nav a', function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top - $(".scroll_pop").height()
    }, 300);
    return false;
});

$(document).on('scroll', function (e) {
    if ( $("#tabs-content").offset().top < $(document).scrollTop() ) {
        $(".scroll_pop").slideDown(100);
    } else {
        $(".scroll_pop").slideUp(100);
    }
});

$(document).on('click', '.images .thumbs a', function (e) {
    $(".images .thumbs a").removeClass("zoomThumbActive");
    $(this).addClass("zoomThumbActive");
    $(".images .big a").attr("href", $(this).attr("href"));
    $(".images .big a img").attr("src", $(this).attr("href"));
    return false;
});

$(document).on('click', '.images .big a', function (e) {
    $(this).jqzoom( {
        title: false,
        xOffset: 28,
        yOffset: -20,
        zoomWidth: 593,
        zoomHeight: 400,
        showEffect: 'fadein',
        fadeinSpeed: 'fast',
        fadeoutSpeed: 'fast'
    } );
    return false;
});


function populateProductPage(stateObj)
{
    $("section.product").replaceWith(stateObj.content);

    imgLazyLoad();

    window.tabs_positions = {};
    $("#tabs-content>section").each(function () {
        window.tabs_positions[$(this).attr("id")] = $(this).offset().top + $(this).height() - $(".scroll_pop").height() - 50;
        $(".scroll_pop .tabs ul").append(
            '<li>' +
            '<a class="btn btn-small btn-normal" href="#'+$(this).attr('id')+'">' +
            $(this).find('.tab_title').clone().children().remove().end().text() +
            '</a>' +
            '</li> '
        );
    });

    $(".images .thumbs li:first a:first").addClass("zoomThumbActive");

    ul = $('.product .images .thumbs ul');
    if( $(ul).length ) {
        if( $(ul).children('li').length > 1 ) {
            ul.show()
        }
        var show_count = 100;
        if( $(ul).children('li').length > show_count ) {
            $(ul).children('li').each(function(){
                if($(this).index() >= show_count) $(this).hide();
            });
            $(ul).before('<div id="nextSlide"><i></i></div>\n<div id="prevSlide"><i></i></div>');

            $('body').on('click', '#nextSlide', function(){
                $('#prevSlide').show();
                $(ul).children('li').eq($(ul).children('li:visible:last').index()+1).show();
                $(ul).children('li:visible:first').hide();
                if($(ul).children('li:visible:last').index() == ($(ul).children('li').length - 1)) $(this).hide();
            });

            $('body').on('click', '#prevSlide', function(){
                $('#nextSlide').show();
                $(ul).children('li').eq($(ul).children('li:visible:first').index()-1).show();
                $(ul).children('li:visible:last').hide();
                if($(ul).children('li:visible:first').index() == 0) $(this).hide();
            });
        }
    }
}

$(document).ready(function() {
    $(document).on('click', '#comments .show-more', function () {
        $(this).remove();
        $('.comments_hidden').fadeIn();
    });

    $(document).on('click', '#comments .get-more', function () {
        $(this).remove();

        page = $('#comments').data('page');
        page++;
        product_id = $('#comments').data('product');
        $.get( '/product-get-comments/'+product_id+'/'+page+'/' ,  function (data) {
            $(this).remove();
            $('#comments').data('page', page);
            $('#comments').append(data);
        });
    });
    
});

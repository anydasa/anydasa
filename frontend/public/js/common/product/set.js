function initProductSet(container)
{
    $(container).on('update', function () {

        var group_num   = $(container).find('.products_group input[type="checkbox"]:checked:visible').length - 1
        owner_price = accounting.unformat($(".owner .product_set-price").text()),
            new_price   = owner_price,
            old_price   = owner_price
        products    = [];

        $(this).find('.products_group>li:visible input[type="checkbox"]:checked').each(function () {
            var discount = $.parseJSON($(this).closest('li').attr('data-discount'))[group_num],
                price    = accounting.unformat($(this).closest('li').find('.product_set-price .old_price').text());
            products.push( $(this).attr('data-id') );
            old_price += price;
            new_price += price - (price * Number(discount) / 100);
            $(this).closest('li').find('.new_price').html(accounting.formatMoney(price - (price * Number(discount) / 100)));
        });

        $(this).find('.setButtonRow .btn').attr('data-id', products);
        $(this).find('.setButtonRow .btn').attr('data-id-owner', $(container).find(".owner").attr('data-id'));

        $(this).find('.setButtonRow .old-price').html(accounting.formatMoney(old_price));
        $(this).find('.setButtonRow .new-price').html(accounting.formatMoney(new_price));
        $(this).find('.setButtonRow .diff span').html(accounting.formatMoney(old_price - new_price));
    });

    $(container).on('click', '.products_group .next-product', function () {
        $(this).closest('li').hide().next().show();
        $(this).closest(container).trigger('update');
    });

    $(container).on('click', '.products_group .prev-product', function () {
        $(this).closest('li').hide().prev().show();
        $(this).closest(container).trigger('update');
    });

    $(container).on('click', '.products_group input[type="checkbox"]', function () {
        if ( !$(this).prop('checked') ) {
            $(this).parents('.products_group:first')
                .attr('data-disabled', true)
                .css({opacity:0.5})
                .find('.prev-product, .next-product').hide()
            ;
        } else {
            $(this).parents('.products_group:first')
                .removeAttr('data-disabled')
                .css({opacity:1})
                .find('.prev-product, .next-product').show()
            ;
        }
        $(this).closest(container).trigger('update');
    });

    $(container).find(".products_group").each(function () {
        $(this).find('>:not(:last)').each(function () {
            var title = $(this).next().find('.product_set-title').text();
            $(this).find('img').after('<div class="btn btn-important next-product tooltip" title="'+title+'"><i></i></div>');
        })
        $(this).find('>:not(:first)').each(function () {
            var title = $(this).prev().find('.product_set-title').text();
            $(this).find('img').before('<div class="btn btn-important prev-product tooltip" title="'+title+'"><i></i></div>');
        })
    });

    $(container).trigger('update');

}


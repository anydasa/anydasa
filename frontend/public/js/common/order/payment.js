$(function () {
    var $container = $('#step-payment');
    var $form = $('#Form_Order_Payment');
    var $radio = $form.find('input:radio[name=payment]');

    var radioToggle = function () {
        var checked = $radio.filter(':checked').val();
        $form.find("div.form div.form").hide();
        $form.find("div.form div.form").find('select,input,textarea').prop('disabled', true);
        $container.find(".payment-text").hide();

        $container.find("#payment-"+ checked +"-text").show();
        $form.find("div.form div.form").filter("#form_"+ checked).show();
        $form.find("div.form div.form").filter("#form_"+ checked).find('select,input,textarea').prop('disabled', false);
    };

    radioToggle();

    $radio.filter(':disabled').parent().addClass('disabled');

    $radio.on('change', function () {
        var targetIndex = getTargetIndex($(this).attr('data-target'), 'blank');
        radioToggle();
        $("body").addClass("wait");
        $.post('/cart/set-payment/', {payment:$(this).val()}, function (data) {
            response =  $.parseJSON(data);
            if(response['modal']){
                $radio.prop('checked', false);
                radioToggle();
                openModalWindow(response['modal'], this.url, targetIndex)
            }
            $("#cart-items-container").html(response['append']);
            $("body").removeClass("wait");
        });
    });

    $.validator.prototype.elements = function() {
        var validator = this,
            rulesCache = {};

        return $( this.currentForm )
            .find( "input, select, textarea" )
            .not( ":submit, :reset, :image, [disabled]") // changed from: .not( ":submit, :reset, :image, [disabled], [readonly]" )
            .not( this.settings.ignore )
            .filter( function() {
                if ( !this.name && validator.settings.debug && window.console ) {
                    console.error( "%o has no name assigned", this );
                }

                if ( this.name in rulesCache || !validator.objectLength( $( this ).rules() ) ) {
                    return false;
                }

                rulesCache[ this.name ] = true;
                return true;
            });
    };

    $radio.filter(':checked').change();
});

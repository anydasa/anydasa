$(function () {

    var $form = $('#Form_Order_Delivery');
    var $cityElement = $form.find("#city");
    var $deliveryTypeElements = $form.find('input:radio[name=delivery]');

    var deliveryTypeToggle = function () {
        var checked = $deliveryTypeElements.filter(':checked').val();
        $form.find("div.form div.form").hide();
        $form.find("div.form div.form").find('select,input,textarea').prop('disabled', true);
        $form.find("div.form div.form").filter("#form_"+ checked).show();
        $form.find("div.form div.form").filter("#form_"+ checked).find('select,input,textarea').prop('disabled', false);
    };

    $cityElement.autocomplete({
        autoFocus: true,
        minLength: 2,
        messages: {
            noResults: '',
            results: function() {}
        },
        source: function( request, response ) {
            $.getJSON('/ajax/np/get-cities/', {q:request.term}, function( data, status, xhr ) {
               var newData = $.map(data, function (value, key) {
                    return {
                        value: value.description_ru,
                        area: value.NovaposhtaAreas.description_ru,
                        code: value.code
                    };
                });
                response(newData);
            });
        },
        change: function (event, ui) {
            if( !ui.item ){
                $cityElement.val("");
                $form.find("#city_code").val("").change();
            }
        },
        select: function (event, ui) {
            $form.find("#city_code").val(ui.item.code).change();
        }
    }).data( "uiAutocomplete" )._renderItem = function( ul, item ) {
        return $( "<li class=\""+item.value+"\"></li>" )
            .data( "item.autocomplete", item )
            .append( item.value + " <span>" + item.area + "</span>" )
            .appendTo( ul );
    };

    $cityElement.on('keyup', function () {
        if ( $(this).val().length == 0 ) {
            $form.find("#city_code").val("").change();
        }
    });

    $form.find("#city_code").on('change', function () {

        $deliveryTypeElements.prop('disabled', true);

        if ('8d5a980d-391c-11dd-90d9-001a92567626' === $(this).val() ) { // Это Киев
            $deliveryTypeElements.prop('disabled', false);
        } else if ( '' !== $(this).val() ) {
            $deliveryTypeElements.filter('[value=novaposhta]').prop('disabled', false);
            $deliveryTypeElements.filter('[value=npcourier]').prop('disabled', false);
        }

        $deliveryTypeElements.parent().removeClass('disabled');
        $deliveryTypeElements.filter(':disabled').parent().addClass('disabled');

        if ( $deliveryTypeElements.filter(':enabled').length == 1 ) {
            $deliveryTypeElements.filter(':enabled').prop('checked', true);
        }

        $deliveryTypeElements.filter(':checked').change();

        deliveryTypeToggle();

    }).change();



    $deliveryTypeElements.on('change', function () {
        deliveryTypeToggle();
        if (
            'novaposhta' === $(this).filter(":checked").val() &&
            $form.find("#city_code").val() !== $form.find('#novaposhta-warehouse').attr('data-city_code')
        ) {
            $form.find('#novaposhta-warehouse option[value!=""]').remove();
            $form.find('#novaposhta-warehouse').attr('data-city_code', $form.find("#city_code").val());
            $.get('/ajax/np/get-warehouses/', { city_code: $form.find("#city_code").val() }, function (data) {
                $.each( data, function( key, val ) {
                    $form.find('#novaposhta-warehouse').append(
                        $("<option />")
                            .val(val.code)
                            .text(val.description_ru)
                    );
                });
            }, 'json');
        }
    });

    $deliveryTypeElements.filter(':checked').change();

    $form.find("#element-city").after(
        $("<div />")
            .addClass('city-presets')
            .append(
                $("<span />")
                    .addClass('link link-green')
                    .attr('data-city_code', '8d5a980d-391c-11dd-90d9-001a92567626')
                    .text('Киев')
            )
            .append(
                $("<span />")
                    .addClass('link link-green')
                    .attr('data-city_code', 'db5c88d0-391c-11dd-90d9-001a92567626')
                    .text('Одесса')
            )
            .append(
                $("<span />")
                    .addClass('link link-green')
                    .attr('data-city_code', 'db5c88f0-391c-11dd-90d9-001a92567626')
                    .text('Днепропетровск')
            )
    );

    $form.find(".city-presets span").on('click', function () {
        $form.find("input[name='city']").val( $(this).text() ).keyup();
        $form.find("input[name='city_code']").val( $(this).attr('data-city_code') ).change();
    });
});

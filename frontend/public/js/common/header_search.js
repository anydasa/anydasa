$(function () {
    var cachesearch = {};
    $('.head input[type="search"]').autocomplete({
        autoFocus: true,
        minLength: 2,
        messages: {
            noResults: '',
            results: function() {}
        },
        source: function( request, response ) {
            var term = request.term;

            if ( term in cachesearch ) {
                response( cachesearch[ term ] );
                return;
            }

            $.getJSON('/search/autocomplete/', {k:request.term}, function( data, status, xhr ) {
                cachesearch[ term ] = data;
                response(data);
            });
        },
        change: function (event, ui) {
            return false;
        },
        select: function (event, ui) {
            return false;
        }
    }).data("uiAutocomplete")._renderItem = function( ul, item ) {
        ul.addClass('autocomplete_header'); //Ul custom class here
        var str = '<a href="'+item.href+'">'
                + '<img src="'+item.img+'">'
                + '<span class="price">'+ item.price+'</span>'
                + item.title
                +'</a>';
        return $("<li></li>").data( "item.autocomplete", item).append(str).appendTo(ul);
    };
});
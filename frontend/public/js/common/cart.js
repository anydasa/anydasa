var mouseenterTimerCart;
var mouseleaveTimerCart;

$(document).on({
    mouseenter: function () {
        if (mouseleaveTimerCart) clearTimeout(mouseleaveTimerCart);
        mouseenterTimerCart = setTimeout(function() {
            $("#head-cart .cart_items").fadeIn()
        }, 10);
    },
    mouseleave: function () {
        if (mouseenterTimerCart) clearTimeout(mouseenterTimerCart);
        mouseleaveTimerCart = setTimeout(function() {
            $("#head-cart .cart_items").hide()
        }, 200);
    }
}, "#head-cart");

$(document).on('click', '.cart_items li .del[data-id]', function () {
    $.post('/cart/delete-item', {id: $(this).attr('data-id')}, function () {
        setTimeout(function () {
            $(window).scroll()
        }, 100)
    }, 'script');

});

//--------------------------------------------------------------------------------------------------------------------
$(document).on("click", ".btn2cartSet", function () {
    $.post(
        '/cart/set-product-set/', {
        id: $(this).attr("data-id").split(','),
        id_owner: $(this).attr("data-id-owner"),
        count: 1
    }, function (data) {
        openModalWindow(data, '/', 1);
    });
});

$(document).on("click", ".btn2cart", function () {
    var count = $(this).attr("data-count").length ? $(this).attr("data-count") : 1;

    // fbq('track', 'AddToCart', {
    //     content_ids: [$(this).data('id')],
    //     content_type: 'product',
    //     value: $(this).data('price'),
    //     currency: 'UAH'
    // });

    $.post('/cart/set-item/', {id: $(this).attr("data-id"), count: count}, function (data) {
        openModalWindow(data, '/', 1);
    });
});

$(document).on("click", ".count_choice .plus", function () {
    var $input = $(this).parents(".count_choice").find("input");
    var currentVal = parseInt($input.val());
    $input.val( currentVal + 1).change();
});
$(document).on("click", ".count_choice .minus", function () {
    var $input = $(this).parents(".count_choice").find("input");
    var currentVal = parseInt($input.val());
    $input.val( currentVal - 1 ).change();
});

$(document).on("change", ".count_choice input", function () {

    if ( $(this).val() < 1 ) {
        $(this).val(1);
        return;
    }

    if ( parseInt($(this).attr("data-available")) < $(this).val() ) {
        var available = parseInt($(this).attr("data-available"));

        $(this).val(available);

        $(this).after(
            $("<div />")
                .addClass("not-available-count")
                .text("Доступно всего "+available)
        );

        $.post($(this).attr('data-action'), {id:$(this).attr('data-id'), count: $(this).val()}, function (data) {
            $(".popup_product").replaceWith(data);
            $("#cart-items-container").html(data);
        });

        setTimeout(function () {
            $(".not-available-count").fadeOut(500, function () {
                $(this).remove();
            });
        }, 1000);
        return;
    }

    $("body").addClass("wait");
    $.post($(this).attr('data-action'), {id:$(this).attr('data-id'), count: $(this).val()}, function (data) {
        $(".popup_product").replaceWith(data);
        $("#cart-items-container").html(data);
        $("body").removeClass("wait");
    });
});

$(document).on("click", ".cart-item-delete", function () {
    $("body").addClass("wait");
    $.post("/cart/delete-from-list/", {id: $(this).attr('data-id')}, function (data) {
        if ( data.trim() === '' ) {
            window.location = '/';
        } else {
            $("#cart-items-container").html(data);
            $("body").removeClass("wait");
        }
    });
});
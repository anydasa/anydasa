SELECT * FROM products WHERE catalogs = '{243}';

DO $$
    DECLARE r record;
        DECLARE _title varchar;
    BEGIN
        FOR r IN SELECT * FROM products WHERE catalogs = '{243}'
            LOOP
                _title := r.title;
                SELECT regexp_replace(_title, '(\d) мм', '\1 mm') INTO _title;
                SELECT regexp_replace(_title, '(\d) m', '\1 m') INTO _title;
                SELECT regexp_replace(_title, '(\d) гр', '\1 g') INTO _title;
                SELECT regexp_replace(_title, '(\d)гр', '\1g') INTO _title;
                SELECT regexp_replace(_title, '(\d)\s+(g|m|mm)(\W)', '\1\2\3', 'g') INTO _title;
                SELECT regexp_replace(_title, '(\d+),(\d+)(g|m|mm)', '\1.\2\3', 'g') INTO _title;
                SELECT regexp_replace(_title, '\s+', ' ', 'g') INTO _title;
                SELECT regexp_replace(_title, '(\d)\s+(SF|F|FF|S|SS|FS|SP|SR|SSR|MDR)(\W)', '\1\2\3') INTO _title;

                UPDATE products
                SET title = _title,
                    alias = getslug(title)
                WHERE id = r.id;
--             RAISE NOTICE '% - %', r.id, _title;
            END LOOP;
    END$$;

SELECT regexp_replace('Воблер Megabite  Hydro Jack 50 F (50 мм,  9,01гр,  3m) (S023)', '(\d) мм', '\1mm');
SELECT regexp_replace('Воблер Megabite  Hydro Jack 50 F (50mm,  9,01гр,  3m) (S023)', '(\d)гр', '\1g');
SELECT regexp_replace('Воблер Megabite  Hydro Jack 50 F (50mm,  9,01g,  3m) (S023)', '(\d+),(\d+g)', '\1.\2');
SELECT regexp_replace('Воблер Megabite  Hydro Jack 50 F (50mm,  9.01g,  3m) (S023)', '\s+', ' ', 'g');
SELECT regexp_replace('Воблер Megabite Hydro Jack 50 F (50mm, 9.01g, 3m) (S023)', '(\d)\s+(SF|F|FF|S|SS|FS|SP|SR|SSR|MDR)', '\1\2');
SELECT regexp_replace('Воблер Raid Level Vib B.I.G. 50 F(64mm, 18g) (001 Shimanashi Tiger)', '(\d)\s+(SF|F|FF|S|SS|FS|SP|SR|SSR|MDR)(\W)', '\1\2\3');
SELECT regexp_replace('Воблер Megabite Dandy 125SP (125mm, 28,2 g, 1,2 m) (217)', '(\d)\s+(g|m|mm)(\W)', '\1\2\3', 'g');


SELECT getslug('Воблер Megabite Dandy 125SP (125mm, 28,2g, 1,2m) (217)')
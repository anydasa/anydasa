CREATE TABLE public.products_state (
  id VARCHAR(20) NOT NULL,
  title VARCHAR NOT NULL,
  sort SMALLINT DEFAULT 0 NOT NULL,
  PRIMARY KEY(id)
) ;

COMMENT ON COLUMN public.products_state.id
IS 'ID';

COMMENT ON COLUMN public.products_state.title
IS 'Заголовок';

COMMENT ON COLUMN public.products_state.sort
IS 'Сортировка';

CREATE OR REPLACE FUNCTION public.sort_trigger ()
RETURNS trigger AS
$body$
DECLARE

BEGIN
  EXECUTE format('SELECT COALESCE(MAX(sort), 0)+1 FROM %I', TG_TABLE_NAME) INTO  NEW.sort;
  RETURN NEW;
END;
$body$
LANGUAGE 'plpgsql'
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER
COST 100;

CREATE TRIGGER products_state_sort_tr
  BEFORE INSERT
  ON public.products_state FOR EACH ROW
  EXECUTE PROCEDURE public.sort_trigger();

INSERT INTO products_state (id,title) VALUES('new', 'Новый');

ALTER TABLE public.products
  ADD COLUMN id_state VARCHAR(20);

COMMENT ON COLUMN public.products.id_state
IS 'Состояние';

ALTER TABLE public.products
ADD CONSTRAINT products_fk7 FOREIGN KEY (id_state)
REFERENCES public.products_state(id)
ON DELETE SET NULL
ON UPDATE CASCADE
NOT DEFERRABLE;

-- anydasa 11.06.2016
ALTER TABLE public.products_state
ADD COLUMN hotline_value VARCHAR DEFAULT '0' NOT NULL;

COMMENT ON COLUMN public.products_state.hotline_value
IS 'Для хотлайна';

-- anydasa 12.06.2016
INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'order/report/products', E'order/report/products', E'order', E'report', E'products', E'{}', E'order-report-products', E'Отчет по товарам', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'order/report/product/(\\d+)', E'order/report/product/%s', E'order', E'report', E'product', E'{id}', E'order-report-product', E'Статистика по товару', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'order/report/product-orders/(\\d+)', E'order/report/product-orders/%s', E'order', E'report', E'product-orders', E'{id}', E'order-report-product-orders', E'Заказы по товару', True);

--anydasa 19.06.2016
INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'products/list/profit', E'products/list/profit', E'products', E'list', E'profit', E'{}', E'products-list-profit', E'Список товаров Profit', True);

--anydasa 22.06.2016
INSERT INTO public.vendors ("name", "site", "description", "price_config", "url_file", "is_auto_upload", "fileext", "interval_process", "interval_actual", "google_drive_key", "handler_class", "is_enabled")
VALUES (E'dako', NULL, NULL, NULL, E'http://b2b.dako.ua/price_server.php?token=c22aa9436d588b317af7489afb719491', 1, NULL, 86400, 259200, NULL, E'Vendors_PriceHandler_Dako', True);

--anydasa 28.06.2016
CREATE TABLE public.landing_page (
  id SERIAL,
  domain VARCHAR NOT NULL,
  html_title VARCHAR NOT NULL,
  html_description VARCHAR NOT NULL,
  body TEXT NOT NULL,
  CONSTRAINT landing_page_domain_key UNIQUE(domain),
  CONSTRAINT landing_page_pkey PRIMARY KEY(id)
)
WITH (oids = false);

COMMENT ON COLUMN public.landing_page.id
IS 'ID';

COMMENT ON COLUMN public.landing_page.domain
IS 'Домен';

COMMENT ON COLUMN public.landing_page.html_title
IS 'Заголовок страницы (title)';

COMMENT ON COLUMN public.landing_page.html_description
IS 'Описание страницы (description)';

COMMENT ON COLUMN public.landing_page.body
IS 'Содержание (html)';

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'landing-page/index/list', E'landing-page/index/list', E'landing-page', E'index', E'list', E'{}', E'landing-page-index-list', E'Список Landing Page', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'landing-page/index/add', E'landing-page/index/add', E'landing-page', E'index', E'add', E'{}', E'landing-page-index-add', E'Добавить Landing Page', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'landing-page/index/edit/(\\d+)', E'landing-page/index/edit/%s', E'landing-page', E'index', E'edit', E'{id}', E'landing-page-index-edit', E'Редактировать Landing Page', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'landing-page/index/delete/(\\d+)', E'landing-page/index/delete/%s', E'landing-page', E'index', E'delete', E'{id}', E'landing-page-index-delete', E'Удалить Landing Page', True);

--anydasa 09.07.2016
ALTER TABLE public.products
ADD COLUMN special_offer_expiry TIMESTAMP WITHOUT TIME ZONE;
COMMENT ON COLUMN public.products.special_offer_expiry
IS 'Время окончания';

ALTER TABLE public.products
ADD COLUMN special_offer_text TEXT;
COMMENT ON COLUMN public.products.special_offer_text
IS 'Специальное предложение';

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'products/product/special-offer/(\\d+)', E'products/product/special-offer/%s', E'products', E'product', E'special-offer', E'{id}', E'products-product-special-offer', E'Специальное предложение', True);

--anydasa 11.07.2016
INSERT INTO public.vendors ("name", "site", "description", "price_config", "url_file", "is_auto_upload", "fileext", "interval_process", "last_time_processed", "interval_actual", "google_drive_key", "handler_class", "is_enabled")
VALUES (E'kin.dp.ua', NULL, NULL, NULL, E'http://xmlex.kin.dp.ua/11kmb/priceA00003578.xml', 1, NULL, 86400, E'2016-06-22 18:39:55', 259200, NULL, E'Vendors_PriceHandler_Kin', True);

--anydasa 22.07.2016
INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'order/report/report-count-per-day', E'order/report/report-count-per-day', E'order', E'report', E'report-count-per-day', E'{}', E'order-report-count-per-day', E'Заказы по товару', True);

-- 29.08.2016
ALTER TABLE "public"."orders"
ADD COLUMN "sent_invoice_html" text COLLATE "default";

COMMENT ON COLUMN "public"."orders"."sent_invoice_html" IS 'Письмо счет фактуры';

-- 02.09.2016
UPDATE orders SET phone = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(phone, '-', ''), ' ', ''), '(',''), ')', ''),'+','') WHERE phone != '-'

-- 07.09.2016
ALTER TABLE "public"."orders"
ADD COLUMN "delivery_cost" numeric(10,2);

COMMENT ON COLUMN "public"."orders"."delivery_cost" IS 'Стоимость доставки';

CREATE TABLE "public"."feedback_answer" (
"id" int4 NOT NULL,
"feedback_id" int4 NOT NULL,
"message" varchar NOT NULL,
"created_at" timestamp NOT NULL,
"user_id" int4 NOT NULL,
PRIMARY KEY ("id"),
CONSTRAINT "feedback_answer_fk" FOREIGN KEY ("feedback_id") REFERENCES "public"."feedback" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "feedback_answer_fk1" FOREIGN KEY ("user_id") REFERENCES "public"."admin_users" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;

COMMENT ON COLUMN "public"."feedback_answer"."feedback_id" IS 'Номер обратной связи';

COMMENT ON COLUMN "public"."feedback_answer"."message" IS 'Сообщение';

COMMENT ON COLUMN "public"."feedback_answer"."created_at" IS 'Время отправления';

COMMENT ON COLUMN "public"."feedback_answer"."user_id" IS 'Отправитель';

ALTER TABLE "public"."feedback_answer"
ALTER COLUMN "created_at" SET DEFAULT now();

CREATE SEQUENCE "public"."feedback_answer_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1
 OWNED BY "public"."feedback_answer"."id";

ALTER TABLE "public"."feedback_answer_id_seq" OWNER TO "megabite";

ALTER TABLE "public"."feedback_answer"
ALTER COLUMN "id" SET DEFAULT nextval('feedback_answer_id_seq'::regclass);


--23.09.2016--- ANYDASA ---

CREATE TABLE public.series (
  id SERIAL,
  title VARCHAR(255) NOT NULL,
  CONSTRAINT series_pkey PRIMARY KEY(id),
  CONSTRAINT series_title_key UNIQUE(title)
);

COMMENT ON COLUMN public.series.id IS 'ID';
COMMENT ON COLUMN public.series.title IS 'Название';

CREATE TABLE public.series_params (
  id SERIAL,
  id_series INTEGER NOT NULL,
  title VARCHAR(255) NOT NULL,
  sort SMALLINT NOT NULL,
  CONSTRAINT series_params_pkey PRIMARY KEY(id),
  CONSTRAINT series_params_fk FOREIGN KEY (id_series)
  REFERENCES public.series(id)
  ON DELETE CASCADE
  ON UPDATE RESTRICT
);

COMMENT ON COLUMN public.series_params.id IS 'ID';
COMMENT ON COLUMN public.series_params.id_series IS 'Серия';
COMMENT ON COLUMN public.series_params.title IS 'Название';
COMMENT ON COLUMN public.series_params.sort IS 'Сортировка';


CREATE FUNCTION series_params_tr() RETURNS trigger AS
$body$
BEGIN
  IF NEW.sort IS NULL THEN
    NEW.sort = (SELECT COALESCE(MAX(sort), 0)+1 FROM series_params WHERE id_series = NEW.id_series);
  END IF;

  RETURN NEW;
END;
$body$
LANGUAGE 'plpgsql';


CREATE TRIGGER series_params_tr
BEFORE INSERT ON public.series_params FOR EACH ROW
EXECUTE PROCEDURE public.series_params_tr();

CREATE TABLE public.series_values (
  id SERIAL,
  id_param INTEGER NOT NULL,
  title VARCHAR(255) NOT NULL,
  sort SMALLINT NOT NULL,
  CONSTRAINT series_values_pkey PRIMARY KEY(id),
  CONSTRAINT series_values_fk FOREIGN KEY (id_param)
  REFERENCES public.series_params(id)
  ON DELETE CASCADE
  ON UPDATE RESTRICT
);

COMMENT ON COLUMN public.series_values.id IS 'ID';
COMMENT ON COLUMN public.series_values.id_param IS 'Параметр';
COMMENT ON COLUMN public.series_values.title IS 'Значение';
COMMENT ON COLUMN public.series_values.sort IS 'Сортировка';


CREATE FUNCTION series_values_tr() RETURNS trigger AS
$body$
BEGIN
  IF NEW.sort IS NULL THEN
    NEW.sort = (SELECT COALESCE(MAX(sort), 0)+1 FROM series_values WHERE id_param = NEW.id_param);
  END IF;

  RETURN NEW;
END;
$body$
LANGUAGE 'plpgsql';

CREATE TRIGGER series_values_tr
BEFORE INSERT ON public.series_values FOR EACH ROW
EXECUTE PROCEDURE public.series_values_tr();

CREATE TABLE public.series_products (
  id_series INTEGER NOT NULL,
  id_product INTEGER NOT NULL,
  sort SMALLINT NOT NULL,
  CONSTRAINT series_products_pkey PRIMARY KEY(id_product),
  CONSTRAINT series_products_fk FOREIGN KEY (id_series)
  REFERENCES public.series(id)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
  CONSTRAINT series_products_fk1 FOREIGN KEY (id_product)
  REFERENCES public.products(id)
  ON DELETE CASCADE
  ON UPDATE NO ACTION
);

COMMENT ON COLUMN public.series_products.id_series IS 'Серия';
COMMENT ON COLUMN public.series_products.id_product IS 'Товар';
COMMENT ON COLUMN public.series_products.sort IS 'Сортировка';

CREATE FUNCTION public.series_products_tr() RETURNS trigger AS
$body$
BEGIN
  IF NEW.sort IS NULL THEN
    NEW.sort = (SELECT COALESCE(MAX(sort), 0)+1 FROM series_products WHERE id_series = NEW.id_series);
  END IF;

  RETURN NEW;
END;
$body$
LANGUAGE 'plpgsql';

CREATE TRIGGER series_products_tr
BEFORE INSERT ON public.series_products FOR EACH ROW
EXECUTE PROCEDURE public.series_products_tr();

CREATE TABLE public.series_products_value (
  id_product INTEGER NOT NULL,
  id_value INTEGER NOT NULL,
  CONSTRAINT series_products_value_idx PRIMARY KEY(id_value, id_product),
  CONSTRAINT series_products_value_fk FOREIGN KEY (id_value)
  REFERENCES public.series_values(id)
  ON DELETE CASCADE
  ON UPDATE NO ACTION,
  CONSTRAINT series_products_value_fk1 FOREIGN KEY (id_product)
  REFERENCES public.series_products(id_product)
  ON DELETE CASCADE
  ON UPDATE NO ACTION
);

COMMENT ON COLUMN public.series_products_value.id_product IS 'Товар';
COMMENT ON COLUMN public.series_products_value.id_value IS 'Значение';


INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'series/index/list', E'series/index/list', E'series', E'index', E'list', E'{}', E'series-index-list', E'Список Серии', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'series/index/add', E'series/index/add', E'series', E'index', E'add', E'{}', E'series-index-add', E'Добавить Серии', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/index/edit/(\\d+)', E'series/index/edit/%s', E'series', E'index', E'edit', E'{id}', E'series-index-edit', E'Редактировать Серии', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/index/delete/(\\d+)', E'series/index/delete/%s', E'series', E'index', E'delete', E'{id}', E'series-index-delete', E'Удалить Серии', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/param/edit/(\\d+)', E'series/param/edit/%s', E'series', E'param', E'edit', E'{id}', E'series-param-edit', E'Редактировать Серия/параметр', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/param/delete/(\\d+)', E'series/param/delete/%s', E'series', E'param', E'delete', E'{id}', E'series-param-delete', E'Удалить Серия/параметр', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/value/delete/(\\d+)', E'series/value/delete/%s', E'series', E'value', E'delete', E'{id}', E'series-value-delete', E'Удалить Серия/Значение', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/param/add/(\\d+)', E'series/param/add/%s', E'series', E'param', E'add', E'{id_series}', E'series-param-add', E'Добавить Серия/параметр', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/param/list/(\\d+)', E'series/param/list/%s', E'series', E'param', E'list', E'{id_series}', E'series-param-list', E'Список Серия/параметр', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/value/edit/(\\d+)', E'series/value/edit/%s', E'series', E'value', E'edit', E'{id}', E'series-value-edit', E'Редактировать Серия/Значение', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/value/add/(\\d+)', E'series/value/add/%s', E'series', E'value', E'add', E'{id_param}', E'series-value-add', E'Добавить Серия/Значение', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/value/list/(\\d+)', E'series/value/list/%s', E'series', E'value', E'list', E'{id_param}', E'series-value-list', E'Список Серия/Значение', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/products/list/(\\d+)', E'series/products/list/%s', E'series', E'products', E'list', E'{id_series}', E'series-products-list', E'Список Серия/Товары', True);

-- 30.09.2016
CREATE TABLE "public"."banners_ref_promotion" (
"banner_id" int4 NOT NULL,
"promotion_id" int4 NOT NULL,
PRIMARY KEY ("banner_id", "promotion_id"),
CONSTRAINT "banner_ref_promotion_fk" FOREIGN KEY ("banner_id") REFERENCES "public"."banners" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "banner_ref_promotion_fk1" FOREIGN KEY ("promotion_id") REFERENCES "public"."promotions" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;
-- 30.09.2016
CREATE TABLE "public"."banners_ref_banner" (
"banner_id" int4 NOT NULL,
"banner_id2" int4 NOT NULL,
PRIMARY KEY ("banner_id", "banner_id2"),
CONSTRAINT "banners_ref_banners_fk" FOREIGN KEY ("banner_id") REFERENCES "public"."banners" ("id") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "banners_ref_banners_fk1" FOREIGN KEY ("banner_id2") REFERENCES "public"."banners" ("id") ON DELETE CASCADE ON UPDATE CASCADE
)
WITH (OIDS=FALSE)
;


COMMENT ON COLUMN public.series_groups.id IS 'ID';
COMMENT ON COLUMN public.series_groups.title IS 'Название';

ALTER TABLE public.series ADD COLUMN id_group INTEGER;
COMMENT ON COLUMN public.series.id_group IS 'Группа';

ALTER TABLE public.series
  ADD CONSTRAINT series_fk FOREIGN KEY (id_group)
REFERENCES public.series_groups(id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE public.series ADD COLUMN sort SMALLINT;
COMMENT ON COLUMN public.series.sort IS 'Сортировка';

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'series/group/list', E'series/group/list', E'series', E'group', E'list', E'{}', E'series-group-list', E'Список Серия/Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'series/group/add', E'series/group/add', E'series', E'group', E'add', E'{}', E'series-group-add', E'Добавить Серия/Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/group/edit/(\\d+)', E'series/group/edit/%s', E'series', E'group', E'edit', E'{id}', E'series-group-edit', E'Редактировать Серия/Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/group/delete/(\\d+)', E'series/group/delete/%s', E'series', E'group', E'delete', E'{id}', E'series-group-delete', E'Удалить Серия/Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'series/group/sort/(\\d+)', E'series/group/sort/%s', E'series', E'group', E'sort', E'{id}', E'series-group-sort', E'Сортировать Серия/Группа', True);

-- anydasa 10.10.2016
ALTER TABLE public.series_params ADD COLUMN view_type SMALLINT DEFAULT 1 NOT NULL;
COMMENT ON COLUMN public.series_params.view_type IS 'Тип отображения';
ALTER TABLE public.series_params ALTER COLUMN view_type DROP DEFAULT;

ALTER TABLE public.series_params ADD COLUMN view_position SMALLINT DEFAULT 1 NOT NULL;
COMMENT ON COLUMN public.series_params.view_position IS 'Место отображения';
ALTER TABLE public.series_params ALTER COLUMN view_position DROP DEFAULT;

-- anydasa  13.10.2016
ALTER TABLE public.series_params ADD COLUMN is_group BOOLEAN DEFAULT FALSE NOT NULL;
COMMENT ON COLUMN public.series_params.is_group IS 'Группировать в каталоге';

CREATE OR REPLACE FUNCTION public.series_params_tr1 () RETURNS trigger AS
$body$
BEGIN

  IF NEW.is_group = TRUE THEN
    UPDATE series_params SET is_group = FALSE WHERE id_series = NEW.id_series AND id <> NEW.id;
  END IF;

  RETURN NEW;

END;
$body$
LANGUAGE 'plpgsql';

CREATE TRIGGER series_params_tr1
AFTER INSERT OR UPDATE
  ON public.series_params FOR EACH ROW
EXECUTE PROCEDURE public.series_params_tr1();

-----------------------------
--- 18.10.2016 anydasa (blog)
-----------------------------
CREATE TABLE public.blog_group (
  id SERIAL,
  title VARCHAR NOT NULL,
  CONSTRAINT blog_group_pkey PRIMARY KEY(id),
  CONSTRAINT blog_group_title_key UNIQUE(title)
);

COMMENT ON COLUMN public.blog_group.id IS 'ID';
COMMENT ON COLUMN public.blog_group.title IS 'Название';


CREATE TABLE public.blog_item (
  id SERIAL,
  id_group INTEGER NOT NULL,
  alias VARCHAR NOT NULL,
  meta_title VARCHAR NOT NULL,
  meta_description VARCHAR NOT NULL,
  title VARCHAR NOT NULL,
  text TEXT NOT NULL,
  is_enabled BOOLEAN DEFAULT false NOT NULL,
  CONSTRAINT blog_item_alias_key UNIQUE(alias),
  CONSTRAINT blog_item_pkey PRIMARY KEY(id),
  CONSTRAINT blog_item_fk FOREIGN KEY (id_group)
  REFERENCES public.blog_group(id)
  ON DELETE NO ACTION
  ON UPDATE CASCADE
);

COMMENT ON COLUMN public.blog_item.id IS 'ID';
COMMENT ON COLUMN public.blog_item.id_group IS 'Группа';
COMMENT ON COLUMN public.blog_item.alias IS 'Алиас';
COMMENT ON COLUMN public.blog_item.meta_title IS 'META title';
COMMENT ON COLUMN public.blog_item.meta_description IS 'META description';
COMMENT ON COLUMN public.blog_item.title IS 'Название';
COMMENT ON COLUMN public.blog_item.text IS 'Текст';
COMMENT ON COLUMN public.blog_item.is_enabled IS 'Вкл./Выкл.';


INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'blog/group/list', E'blog/group/list', E'blog', E'group', E'list', E'{}', E'blog-group-list', E'Список Blog Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'blog/group/add', E'blog/group/add', E'blog', E'group', E'add', E'{}', E'blog-group-add', E'Добавить Blog Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'blog/group/edit/(\\d+)', E'blog/group/edit/%s', E'blog', E'group', E'edit', E'{id}', E'blog-group-edit', E'Редактировать Blog Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'blog/group/delete/(\\d+)', E'blog/group/delete/%s', E'blog', E'group', E'delete', E'{id}', E'blog-group-delete', E'Удалить Blog Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'blog/item/list', E'blog/item/list', E'blog', E'item', E'list', E'{}', E'blog-item-list', E'Список Блог статья', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'blog/item/add', E'blog/item/add', E'blog', E'item', E'add', E'{}', E'blog-item-add', E'Добавить Блог статья', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'blog/item/edit/(\\d+)', E'blog/item/edit/%s', E'blog', E'item', E'edit', E'{id}', E'blog-item-edit', E'Редактировать Блог статья', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'blog/item/delete/(\\d+)', E'blog/item/delete/%s', E'blog', E'item', E'delete', E'{id}', E'blog-item-delete', E'Удалить Блог статья', True);


--- anydasa 19.10.2016
DROP TABLE public.blog_item;
DROP TABLE public.blog_group;
DELETE FROM public.admin_routes WHERE module = 'blog';

CREATE TABLE public.article_group (
  id SERIAL,
  title VARCHAR NOT NULL,
  CONSTRAINT article_group_pkey PRIMARY KEY(id),
  CONSTRAINT article_group_title_key UNIQUE(title)
);

COMMENT ON COLUMN public.article_group.id IS 'ID';
COMMENT ON COLUMN public.article_group.title IS 'Название';


CREATE TABLE public.article_item (
  id SERIAL,
  id_group INTEGER NOT NULL,
  alias VARCHAR NOT NULL,
  meta_title VARCHAR NOT NULL,
  meta_description VARCHAR NOT NULL,
  title VARCHAR NOT NULL,
  text TEXT NOT NULL,
  is_enabled BOOLEAN DEFAULT false NOT NULL,
  CONSTRAINT article_item_alias_key UNIQUE(alias),
  CONSTRAINT article_item_pkey PRIMARY KEY(id),
  CONSTRAINT article_item_fk FOREIGN KEY (id_group)
  REFERENCES public.article_group(id)
  ON DELETE NO ACTION
  ON UPDATE CASCADE
);

COMMENT ON COLUMN public.article_item.id IS 'ID';
COMMENT ON COLUMN public.article_item.id_group IS 'Группа';
COMMENT ON COLUMN public.article_item.alias IS 'Алиас';
COMMENT ON COLUMN public.article_item.meta_title IS 'META title';
COMMENT ON COLUMN public.article_item.meta_description IS 'META description';
COMMENT ON COLUMN public.article_item.title IS 'Название';
COMMENT ON COLUMN public.article_item.text IS 'Текст';
COMMENT ON COLUMN public.article_item.is_enabled IS 'Вкл./Выкл.';


INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'article/group/list', E'article/group/list', E'article', E'group', E'list', E'{}', E'article-group-list', E'Список article Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'article/group/add', E'article/group/add', E'article', E'group', E'add', E'{}', E'article-group-add', E'Добавить article Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'article/group/edit/(\\d+)', E'article/group/edit/%s', E'article', E'group', E'edit', E'{id}', E'article-group-edit', E'Редактировать article Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'article/group/delete/(\\d+)', E'article/group/delete/%s', E'article', E'group', E'delete', E'{id}', E'article-group-delete', E'Удалить article Группа', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'article/item/list', E'article/item/list', E'article', E'item', E'list', E'{}', E'article-item-list', E'Список Блог статья', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'article/item/add', E'article/item/add', E'article', E'item', E'add', E'{}', E'article-item-add', E'Добавить Блог статья', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'article/item/edit/(\\d+)', E'article/item/edit/%s', E'article', E'item', E'edit', E'{id}', E'article-item-edit', E'Редактировать Блог статья', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'article/item/delete/(\\d+)', E'article/item/delete/%s', E'article', E'item', E'delete', E'{id}', E'article-item-delete', E'Удалить Блог статья', True);

--- 21.10.2016
INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'series/index/download-params', E'series/index/download-params', E'series', E'index', E'download-params', E'{}', E'series-index-download-params', E'Загрузить параметры в EXCEL', True);

--- 01.11.2016
ALTER TABLE "public"."orders"
ADD COLUMN "desired_time_delivery" varchar,
ADD COLUMN "desired_date_delivery" timestamp DEFAULT NULL,
ADD COLUMN "date_delivery" timestamp DEFAULT NULL;

COMMENT ON COLUMN "public"."orders"."desired_time_delivery" IS 'Желаемое время доставки';

COMMENT ON COLUMN "public"."orders"."date_delivery" IS 'Запланированная дата доставки';

COMMENT ON COLUMN "public"."orders"."desired_date_delivery" IS 'Желаемая дата доставки';

-- 02.11.2016
ALTER TABLE "public"."article_item"
ADD COLUMN "short_text" varchar,
ADD COLUMN "user_id" int4,
ADD COLUMN "author" varchar,
ADD COLUMN "date_created" timestamp DEFAULT now() NOT NULL,
ADD COLUMN "date_publication" timestamp;

COMMENT ON COLUMN "public"."article_item"."short_text" IS 'Краткое описание';

COMMENT ON COLUMN "public"."article_item"."user_id" IS 'Кто создал';

COMMENT ON COLUMN "public"."article_item"."author" IS 'Автор';

COMMENT ON COLUMN "public"."article_item"."date_created" IS 'Дата создания';

COMMENT ON COLUMN "public"."article_item"."date_publication" IS 'Дата публикации';

-- 03.11.2016
ALTER TABLE "public"."article_item"
DROP COLUMN "author";

ALTER TABLE "public"."article_item"
ADD CONSTRAINT "user_access_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "public"."admin_users" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

INSERT INTO "public"."admin_routes"(
    "id",
    "type",
    "route",
    "reverse",
    "module",
    "controller",
    "action",
    "map",
    "name",
    "title",
    "is_registered_in_log"
) VALUES (
    669,
    'Zend_Controller_Router_Route_Regex',
    'order/sent_message_to_client/(\d+)',
    'order/sent_message_to_client/%s',
    'order',
    'index',
    'sent-message-to-client',
    '{id_order}',
    'order-sent-message-to-client',
    'Отправить сообщение пользователю',
    '1'
 );

 -- CREATE FIELD "sold_phrases_hotline" -------------------------
ALTER TABLE "public"."products" ADD COLUMN "sold_phrases_hotline" Character Varying;
COMMENT ON COLUMN "public"."products"."sold_phrases_hotline" IS 'ПРОДАЮЩАЯ ФРАЗА ДЛЯ ХОТЛАЙНА'

CREATE SEQUENCE partial_payments_id_seq;

CREATE TABLE public.partial_payments (
  id int4 DEFAULT nextval('partial_payments_id_seq'::regclass) NOT NULL,
  month_count INTEGER NOT NULL,
  percent_value numeric(4,2),
  CONSTRAINT partial_payments_pkey PRIMARY KEY(id)
);

ALTER SEQUENCE partial_payments_id_seq OWNED BY "partial_payments"."id";

COMMENT ON COLUMN "public"."partial_payments"."id" IS 'ID';

COMMENT ON COLUMN "public"."partial_payments"."month_count" IS 'Количество месяцев';

COMMENT ON COLUMN "public"."partial_payments"."percent_value" IS '% от стоимости товара';

DELETE FROM public.admin_routes WHERE id = 669;

INSERT INTO "public"."admin_routes"(
    "type",
    "route",
    "reverse",
    "module",
    "controller",
    "action",
    "map",
    "name",
    "title",
    "is_registered_in_log"
) VALUES (
    'Zend_Controller_Router_Route_Regex',
    'order/sent_message_to_client/(\d+)',
    'order/sent_message_to_client/%s',
    'order',
    'index',
    'sent-message-to-client',
    '{id_order}',
    'order-sent-message-to-client',
    'Отправить сообщение пользователю',
    '1'
 );

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'products/partial-payments/list', E'products/partial-payments/list', E'products', E'partial-payments', E'list', E'{}', E'partial-payments-list', E'Список оплата частями', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Static', E'products/partial-payments/add', E'products/partial-payments/add', E'products', E'partial-payments', E'add', E'{}', E'partial-payments-add', E'Добавить оплату частями', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'products/partial-payments/edit/(\\d+)', E'products/partial-payments/edit/%s', E'products', E'partial-payments', E'edit', E'{id}', E'partial-payments-edit', E'Редактировать оплату частями', True);

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES (E'Zend_Controller_Router_Route_Regex', E'products/partial-payments/delete/(\\d+)', E'products/partial-payments/delete/%s', E'products', E'partial-payments', E'delete', E'{id}', E'partial-payments-delete', E'Удалить оплату частями', True);

ALTER TABLE public.products_margins ADD COLUMN id_partial_payments INTEGER;
ALTER TABLE public.products_margins ADD COLUMN price_pm numeric(10,2);

COMMENT ON COLUMN public.products_margins.id_partial_payments
IS 'Оплата частями';

COMMENT ON COLUMN public.products_margins.price_pm
IS 'Цена ПМ';

ALTER TABLE public.products_margins
  ADD CONSTRAINT products_fk2 FOREIGN KEY (id_partial_payments)
REFERENCES public.partial_payments(id)
ON DELETE SET NULL
ON UPDATE CASCADE
  NOT DEFERRABLE;

--20.01.2019
INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title")
VALUES (E'Zend_Controller_Router_Route_Regex', E'products/import-images-excel', E'products/import-images-excel', E'products', E'import-images-excel', E'import', E'{}', E'products-import-images-excel-import', E'Импорт картинок');

 -- добавление способа оплаты новапей -------------------------
INSERT INTO public.orders_payment ("id", "title", "commission", "is_enabled", "sort", "text", "title_in_product")
VALUES ('novapay', 'Оплата через NovaPay', '2.5', true, 5, '', 'Картой через NovaPay (Б/Н курс)');

UPDATE public.orders_payment SET sort = 6 WHERE id = 'cashondelivery'

INSERT INTO public.orders_delivery_ref_payment ("id_delivery", "id_payment")
VALUES ('pickup', 'novapay');

INSERT INTO public.orders_delivery_ref_payment ("id_delivery", "id_payment")
VALUES ('courier', 'novapay');

INSERT INTO public.orders_delivery_ref_payment ("id_delivery", "id_payment")
VALUES ('novaposhta', 'novapay');

INSERT INTO public.orders_delivery_ref_payment ("id_delivery", "id_payment")
VALUES ('npcourier', 'novapay');

ALTER TABLE public.orders
  ADD COLUMN novapay_payment_status Character Varying DEFAULT null;

ALTER TABLE public.orders
  ADD COLUMN admin_payment_confirmed BOOLEAN DEFAULT FALSE NOT NULL;

ALTER TABLE public.orders
  ADD COLUMN novapay_session_id Character Varying DEFAULT null;

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title")
VALUES (E'Zend_Controller_Router_Route_Regex', E'order/novapay_confirm', E'order/novapay_confirm', E'order', E'index', E'novapay-confirm', E'{}', E'order-novapay-confirm', E'Списание средств с карты покупателя (подтверждение)');

INSERT INTO public.admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title")
VALUES (E'Zend_Controller_Router_Route_Regex', E'order/novapay_refresh', E'order/novapay_refresh', E'order', E'index', E'novapay-refresh', E'{}', E'order-novapay-refresh', E'Обновить статус novapay');

--------------------
ALTER TABLE products_links
    ADD COLUMN html TEXT DEFAULT NULL;

COMMENT ON COLUMN products_links.html
    IS 'Html';

-----------------------------------

INSERT INTO public.admin_routes (type, route, reverse, module, controller, action, map, name, title, is_registered_in_log)
VALUES ('Zend_Controller_Router_Route_Regex', 'products/product/description/(\d+)', 'products/product/description/%s', 'products', 'product', 'description', '{id}', 'products-product-description', 'Редактирование описания товара', true);

-----------------------------------

ALTER TABLE series
    ADD COLUMN description TEXT DEFAULT NULL;

COMMENT ON COLUMN series.description IS 'Описание';


INSERT INTO public.admin_routes (type, route, reverse, module, controller, action, map, name, title, is_registered_in_log)
VALUES ('Zend_Controller_Router_Route_Regex', 'series/index/description/(\d+)', 'series/index/description/%s', 'series', 'index', 'description', '{id}', 'series-index-description', 'Редактирование описания', true);

-----------
INSERT INTO vendors (name, site, description, price_config, url_file, is_auto_upload, fileext, interval_process, last_time_processed, interval_actual, google_drive_key, handler_class, is_enabled)
VALUES ('fishingstock.ua', 'https://fishingstock.ua/', '', null, 'https://fishingstock.ua/upload/xmls/fishingstock.xml', 1, null, 86400, '2020-10-08 18:26:08', 86400, '', 'Vendors_PriceHandler_FishingStock', true);

-----------

INSERT INTO admin_routes ( type, route, reverse, module, controller, action, map, name, title, is_registered_in_log)
VALUES
('Zend_Controller_Router_Route_Regex', 'products/quick-options/(\d+)/list', 'products/quick-options/%s/list', 'products', 'quick-options', 'list', '{id_catalog}', 'products-quick-options-list', 'Параметры товара', true),
('Zend_Controller_Router_Route_Regex', 'products/quick-options/save/(\d+)', 'products/quick-options/save/%s', 'products', 'quick-options', 'save', '{id_product}', 'products-quick-options-save', 'Изменить параметры', true),
('Zend_Controller_Router_Route_Regex', 'products/quick-options/(\d+)/upload', 'products/quick-options/%s/upload', 'products', 'quick-options', 'upload', '{id_catalog}', 'products-quick-options-upload', 'Загрузить параметры', true),
('Zend_Controller_Router_Route_Regex', 'products/quick-options/(\d+)/download', 'products/quick-options/%s/download', 'products', 'quick-options', 'download', '{id_catalog}', 'products-quick-options-download', 'Скачать параметры', true)

-----------
create unique index products_option_id_id_catalog_uindex
    on products_option (id, id_catalog);

create table series_options
(
    id serial primary key,
    id_catalog int not null,
    id_product_option int not null,
    display_type smallint not null,
    sort int default 0 not null
);
comment on column series_options.id is 'ID';
comment on column series_options.id_product_option is 'Параметр продукта';
comment on column series_options.display_type is 'Тип отображения';
comment on column series_options.sort is 'Сортировка';

alter table series_options
    add constraint series_options_products_option_id_catalog_id_fk
        foreign key (id_catalog, id_product_option) references products_option (id_catalog, id);

create unique index series_options_id_catalog_id_product_option_uindex
    on series_options (id_catalog, id_product_option);

INSERT INTO admin_routes ("type", "route", "reverse", "module", "controller", "action", "map", "name", "title", "is_registered_in_log")
VALUES  (E'Zend_Controller_Router_Route_Regex', E'series/options/edit/(\\d+)', E'series/options/edit/%s', E'series', E'options', E'edit', E'{id}', E'series-options-edit', E'Редактировать Серия/параметр', True),
        (E'Zend_Controller_Router_Route_Regex', E'series/options/delete/(\\d+)', E'series/options/delete/%s', E'series', E'options', E'delete', E'{id}', E'series-options-delete', E'Удалить Серия/параметр', True),
        (E'Zend_Controller_Router_Route_Regex', E'series/options/add/(\\d+)', E'series/options/add/%s', E'series', E'options', E'add', E'{id_catalog}', E'series-options-add', E'Добавить Серия/параметр', True),
        (E'Zend_Controller_Router_Route_Regex', E'series/options/list/(\\d+)', E'series/options/list/%s', E'series', E'options', E'list', E'{id_catalog}', E'series-options-list', E'Список Серия/параметр', True);

DELETE FROM admin_routes WHERE name like 'series-param-%';
DELETE FROM admin_routes WHERE name like 'series-value-%';

---------------------------------
alter table catalog add is_group_by_series bool default false not null;
comment on column catalog.is_group_by_series is 'Группировать по сериям';

---------------------------------
INSERT INTO vendors (name, site, description, price_config, url_file, is_auto_upload, fileext,
                     interval_process, last_time_processed, interval_actual, google_drive_key, handler_class,
                     is_enabled)
VALUES ('ArmorsFormatRozetka', 'http://armors.com.ua/', '', '',
        'https://pix.armors.com.ua/files/export-products/WGZ15dE8ScpF.xml', 1, '.zip', 10800, '2020-06-11 16:30:11',
        259200, '', 'Vendors_PriceHandler_ArmorsFormatRozetka', true);

UPDATE products_margins SET
(id_currency, price_min, price_partner, price_trade, price_retail, type_min, type_partner, type_trade, type_retail, round_min, round_partner, round_trade, round_retail, use_retail, id_partial_payments, price_pm)
=
(2, 0.00, 0.00, 0.00, 0.00, 'present', 'present', 'present', 'present', 2, 2, 2, 2, true, null, null);
<?

require 'dotenv.php';

define('IS_DEV', false);
define('APPLICATION_DOMAIN', 'anydasa.com.ua');
define('REQUEST_SCHEME', 'https');
define('APPLICATION_PATH', dirname($_SERVER['DOCUMENT_ROOT']) . '/');
define('ROOT_PATH', dirname(dirname($_SERVER['DOCUMENT_ROOT'])) . '/');

define('SRC_PATH', ROOT_PATH . 'vendor/googleads/googleads-php-lib/src');
define('LIB_PATH', 'Google/Api/Ads/AdWords/Lib');
define('UTIL_PATH', 'Google/Api/Ads/Common/Util');
define('ADWORDS_UTIL_PATH', 'Google/Api/Ads/AdWords/Util');
define('ADWORDS_UTIL_VERSION_PATH', 'Google/Api/Ads/AdWords/Util/v201605');
define('ADWORDS_VERSION', 'v201605');
define('ASSETS_ID', 'v102');

define('SITE_NAME', 'Anydasa');


$config = [
    'siteName' => 'Anydasa',
    'url' => [
        'host' => APPLICATION_DOMAIN,
        'domain' => REQUEST_SCHEME . '://' . APPLICATION_DOMAIN . '/',
        'admin' => 'https://adm.' . APPLICATION_DOMAIN . '/',
        'pix' => REQUEST_SCHEME . '://pix.' . APPLICATION_DOMAIN . '/',
        'img' => [
            'products' => REQUEST_SCHEME . '://pix.' . APPLICATION_DOMAIN . '/img/products/',
            'products_description' => REQUEST_SCHEME . '://pix.' . APPLICATION_DOMAIN . '/img/content/products/description',
            'series_description' => REQUEST_SCHEME . '://pix.' . APPLICATION_DOMAIN . '/img/content/series/description',
        ]
    ],
    'session' => [
        'cookie_httponly' => 'on',
        'gc_maxlifetime' => 1209600,
        'save_path' => APPLICATION_PATH . 'sessions/',
        'cookie_domain' => '.' . APPLICATION_DOMAIN,
    ],
    'db' => [
        'dsn' => 'pgsql:host=private-anydasa-com-db-postgresql-fra1-do-user-1841641-0.a.db.ondigitalocean.com;port=25060;dbname=anydasa',
        'username' => 'anydasa',
        'password' => 'ztkjinfpsy9lskaw',
    ],

    'path' => [
        'root' => ROOT_PATH,
        'files' => ROOT_PATH . 'frontend/public/files/',
        'lib' => ROOT_PATH . 'lib/',
        'img' => ROOT_PATH . 'frontend/public/img/',
        'doctrineModels' => ROOT_PATH . 'models/',
        'pix' => ROOT_PATH . 'pix/',
        'tmp' => ROOT_PATH . 'tmp/',
        'src' => ROOT_PATH . 'src/',
        'img_product_description' => ROOT_PATH . 'pix/img/content/products/description',
        'img_series_description' => ROOT_PATH . 'pix/img/content/series/description',


        'public' => APPLICATION_PATH . 'public/',
        'appLib' => APPLICATION_PATH . 'app/lib/',
        'app' => APPLICATION_PATH . 'app/',
        'logs' => APPLICATION_PATH . 'logs/app/',
    ],
    'timezone' => 'Europe/Kiev',
    'common' => [
        'charset' => 'utf-8',
    ],
    'debug' => [
        'on' => true,
    ],
    'default_locale' => 'ru',
    'locales' => [
        'ru' => 'ru_RU',
    ],
    'cache' => [
        'core' => 'File',
//        'core' => 'Memcached',
        'backend' => [
            'servers' => [[
                'host' => '127.0.0.1',
                'port' => '11211'
            ]],
            'compression' => true,
            'cache_dir' => ROOT_PATH . 'cache/'
        ],
        'frontend' => [
            'lifetime' => 300,
            'caching' => true,
            'write_control' => true,
            'automatic_serialization' => true,
            'ignore_user_abort' => true
        ]
    ],
    'sms' => [
        'login' => '---megabite',
        'password' => '---EWxazP',
        'sender' => '---Megabite'
    ]
];
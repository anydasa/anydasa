<?

function array_join()
{
    $arrays = func_get_args();
    $original = array_shift($arrays);
    foreach ($arrays as $array)
        foreach ($array as $key => $value)
            $original[$key] = is_array($value) ? array_join(@$original[$key], @$array[$key]) : $value;
    return $original;
}

function getmicrotime()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float) $usec + (float) $sec);
}

function strpos_array($haystack, $needles, $offset = 0) {
    if (is_array($needles)) {
        foreach ($needles as $needle) {
            $pos = strpos_array($haystack, $needle);
            if ($pos !== false) {
                return $pos;
            }
        }
        return false;
    } else {
        return mb_strpos($haystack, $needles, $offset);
    }
}

function parse_words($str, $min_sym_in_word = 1) {
    preg_match_all('/(\S{'.$min_sym_in_word.',})/is', $str, $m);
    return $m[1];
}

function trunc_words($string, $max_chars)
{
    if ( mb_strlen($string) > $max_chars ) {
        $sub = mb_substr($string, 0, $max_chars);
        $pos = mb_strrpos($sub, ' ');
        $string = mb_substr($string, 0, $pos);
    }
    return trim($string);
}